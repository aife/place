package com.atexo.serveurCryptographique.utilitaire.smartCard;



import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.ProviderException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import com.atexo.serveurCryptographique.utilitaire.CertificatItem;
import com.atexo.serveurCryptographique.utilitaire.HashUtil;
import com.atexo.serveurCryptographique.utilitaire.I18nUtil;
import com.atexo.serveurCryptographique.utilitaire.KeyPair;
import com.atexo.serveurCryptographique.utilitaire.RecuperationCertificatException;
import com.atexo.serveurCryptographique.utilitaire.TypeOs;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.Util;
import com.atexo.serveurSignature.CertificatUtil;

import sun.security.pkcs11.SunPKCS11;

/**
 *
 */
public class SmartCardHandler {

	private static SmartCardHandler smartCardHandler;

	private TypeOs typeOs;

	private List<CardTerminal> cardTerminals;

	private int index;

	private CardTerminal cardTerminal;

	private Card card;

	private ATR atr;

	private String atrString;

	private List<SmartCardInformation> smartCardInformations;

	private File pkcs11Librairie;

	private SunPKCS11 sunPkcs11Provider;

	private KeyStore keyStore;

	public static SmartCardHandler getInstance() {
		if (smartCardHandler == null) {
			smartCardHandler = new SmartCardHandler();
		}

		return smartCardHandler;
	}

	public void initialiserDetection(TypeOs typeOs) {
		this.typeOs = typeOs;

		try {
			TerminalFactory factory = TerminalFactory.getDefault();
			cardTerminals = factory.terminals().list();
		} catch (CardException e) {
			e.printStackTrace();
		}
	}

	public void setSmartCardInformations(List<SmartCardInformation> smartCardInformations) {
		this.smartCardInformations = smartCardInformations;
	}

	public List<CardTerminal> getCardTerminals() {
		if (cardTerminals == null) {
			cardTerminals = new ArrayList<CardTerminal>();
		}
		return cardTerminals;
	}

	public int getNombreSmartCards() {
		return getCardTerminals().size();
	}

	public List<SmartCardInformation> getSmartCardInformations() {
		if (smartCardInformations == null) {
			smartCardInformations = new ArrayList<SmartCardInformation>();
		}
		return smartCardInformations;
	}

	private void connecter() throws CardException {
		System.out.println("Etablissement de la connexion à la smart card " + getNomTerminalCard());
		card = cardTerminal.connect("*");
	}

	private void deconnecter() throws CardException {
		if (card != null) {
			System.out.println("Fermeture de la connexion à la smart card " + getNomTerminalCard());
			card.disconnect(false);
		}
	}

	public boolean verifierCardTerminalSelectionnee() throws CardException {
		boolean smartCardPresente = false;
		if (typeOs == TypeOs.MacOs) {
			// http://stackoverflow.com/questions/13024062/mac-os-java-7-appletsmartcard-issue
			// (http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7195480)
			smartCardPresente = cardTerminal != null;
		} else {
			smartCardPresente = cardTerminal != null && cardTerminal.isCardPresent();
		}

		System.out.println("La smart card est-elle tjrs présente et connecté : " + smartCardPresente);
		return smartCardPresente;
	}

	public String getNomTerminalCard() {
		return cardTerminal.getName();
	}

	private void recupererATR() {
		atr = card.getATR();
		atrString = HashUtil.convertirBinaireEnHexadecimal(atr.getBytes());
	}

	public boolean selectionnerCardTerminal(int index) throws CardException {
		System.out.println("Sélection de la smart card ayant comme index :" + index);
		if (!cardTerminals.isEmpty()) {
			this.cardTerminal = cardTerminals.get(index);
			this.index = index;
			if (verifierCardTerminalSelectionnee()) {
				System.out.println("Smart card sélectionnée :" + getNomTerminalCard());
				return rechercherLibrairie();
			} else {
				System.out.println("Aucune Smart card n'est sélectionnée :" + getNomTerminalCard());
				pkcs11Librairie = null;
				return false;
			}

		} else {
			return false;
		}
	}

	private boolean rechercherLibrairie() throws CardException {
		System.out.println("Recherche de la librairie PKCS11 du fabriquant");
		if (typeOs != TypeOs.Indetermine && verifierCardTerminalSelectionnee() && !getSmartCardInformations().isEmpty()) {

			try {

				if (card == null) {
					connecter();
				}
				recupererATR();
				System.out.println("Valeur de l'atr de la carte " + getNomTerminalCard() + " : " + atrString);
				boolean smartCardTrouvee = false;
				int i = 0;
				SmartCardInformation smartCardSelectionne = null;

				for (SmartCardInformation smartCardInformation : getSmartCardInformations()) {
					System.out.println("Comparaison de l'atr avec ceux de la liste des smart cards enregistrés :  "
							+ smartCardInformation.getAtr());
					char[] smartCardAtrTemp = smartCardInformation.getAtr().toCharArray();

					if (atrString.length() == smartCardAtrTemp.length) {

						char c = "_".charAt(0);

						for (i = 0; i < atrString.length(); i++) {

							String lettreAtrString = Character.toString(atrString.charAt(i)).toUpperCase();
							String lettreSmartCardAtrTemp = Character.toString(smartCardAtrTemp[i]).toUpperCase();

							if (!lettreAtrString.equals(lettreSmartCardAtrTemp) && smartCardAtrTemp[i] != c) {
								break;
							}
						}

						if (i == atrString.length()) {
							System.out.println("Smart card trouvé");
							smartCardTrouvee = true;
							smartCardSelectionne = smartCardInformation;
							break;
						}
					}

					i = 0;
				}

				if (smartCardTrouvee && smartCardSelectionne != null && smartCardSelectionne.getLibrairie() != null
						&& smartCardSelectionne.getLibrairie().isFile()) {
					System.out.println("Initialisation de la librairie PKCS11 avec la correspondance trouvée");
					this.pkcs11Librairie = smartCardSelectionne.getLibrairie();
					System.out.println("Chemin vers la lib : " + this.pkcs11Librairie.getAbsolutePath());
					smartCardTrouvee = pkcs11Librairie.isFile();
				} else {
					smartCardTrouvee = false;
				}

				return smartCardTrouvee;

			} finally {
				deconnecter();
			}
		}

		return false;
	}

	private class PinPasswordCallbackHandler implements CallbackHandler {
	//	@Override
		public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
			for (int i = 0; i < callbacks.length; i++) {
				if (callbacks[i] instanceof PasswordCallback) {
					PasswordCallback passwordCallback = (PasswordCallback) callbacks[i];
					JPasswordField passwordField = new JPasswordField();
					JLabel label = new JLabel(I18nUtil.get("SWING_PKCS11_MOT_DE_PASSE"));
					JOptionPane.showConfirmDialog(null, new Object[] { label, passwordField }, I18nUtil.get("SWING_PKCS11_CODE_PIN") + " "
							+ getNomTerminalCard(), JOptionPane.OK_CANCEL_OPTION);
					passwordCallback.setPassword(new String(passwordField.getPassword()).toCharArray());
				} else {
					throw new UnsupportedCallbackException(callbacks[i], "Callback non reconnu");
				}
			}
		}
	}

	/**
	 * Retourne la liste des certificats du provider pkcs11 associé à la carte à
	 * puce sélectionné
	 *
	 * @return la liste des certifcats personnels de l'utilisateur.
	 * @throws fr.atexo.signature.commun.exception.certificat.RecuperationCertificatException
	 *
	 */
	public List<CertificatItem> recupererCertificats(TypeProvider typeProvider, Set<String> hashs) throws RecuperationCertificatException {

		if (hashs == null) {
			hashs = new HashSet<String>();
		}
		List<CertificatItem> listCertifItem = new ArrayList<CertificatItem>();

		// suppression du provider s'il est déjà présent
		if (sunPkcs11Provider != null) {
			try {
				sunPkcs11Provider.logout();
			} catch (LoginException e) {
				e.printStackTrace();
			} catch (ProviderException e) {
				e.printStackTrace();
			}
			Security.removeProvider(sunPkcs11Provider.getName());
		}
		keyStore = null;

		// ajout du provider
		String pkcs11Config = "name = SmartCard\n" + "library = " + pkcs11Librairie.getAbsolutePath() + "\nslotListIndex = " + index;
		System.out.println("Config PKCS11 : " + pkcs11Config);
		byte[] pkcs11ConfigBytes = pkcs11Config.getBytes();
		ByteArrayInputStream bais = new ByteArrayInputStream(pkcs11ConfigBytes);
		sunPkcs11Provider = new SunPKCS11(bais);
		Security.addProvider(sunPkcs11Provider);
		System.out.println("Ajout du provider PKCS11");

		// récupération des certificats
		boolean effectuerAuthentification = true;
		KeyStore.CallbackHandlerProtection callbackHandlerProtection = new KeyStore.CallbackHandlerProtection(
				new PinPasswordCallbackHandler());
		KeyStore.Builder keystoreBuilder = null;
		while (true) {

			if (effectuerAuthentification) {
				effectuerAuthentification = false;
				try {
					keystoreBuilder = KeyStore.Builder.newInstance(TypeProvider.PKCS11.getType(), sunPkcs11Provider,
							callbackHandlerProtection);
					keyStore = keystoreBuilder.getKeyStore();
					break;
				} catch (KeyStoreException e) {
					effectuerAuthentification = true;
				}
			}
		}

		try {

			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String aliasKey = aliases.nextElement();
				X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
				String usage = CertificatUtil.getUtilisablePour(certificat, true);
				// on rajoute a la liste uniquement s'il y a une utilité au
				// certificat listé
				if (usage != null) {
					String sujetCn = CertificatUtil.getCN(certificat.getSubjectDN());
					String issuerCn = CertificatUtil.getCN(certificat.getIssuerDN());
					String dateValidation = Util.formaterDate(certificat.getNotAfter());
					String hashCode = String.valueOf(certificat.hashCode());

					if (!hashs.contains(hashCode)) {
						CertificatItem item = new CertificatItem(aliasKey, sujetCn, issuerCn, dateValidation, certificat.getNotAfter(),
								usage, true, hashCode, typeProvider);
						listCertifItem.add(item);
						hashs.add(hashCode);
					}
				}
			}
		} catch (KeyStoreException e) {
			throw new RecuperationCertificatException(
					"Erreur lors de la récupération  des certificats se trouvant dans le key store du provider PKCS11", e);
		}

		return listCertifItem;
	}

	/**
	 * Retour le keyPair à partir du provider et de l'alias.
	 *
	 * @param alias
	 *            l'alias à recherche depuis le Keystore
	 * @return le keyPair
	 * @throws RecuperationCertificatException
	 *
	 */
	public KeyPair getKeyPair(String alias) throws RecuperationCertificatException {

		try {
			// parcourt de l'ensemble des occurances contenus dans le KeyStore
			// et récupération du bit-clé sélectionné
			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String aliasKey = aliases.nextElement();
				// System.out.println("aliasKey => " + aliasKey);
				if (aliasKey.equals(alias)) {
					X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
					// dans le cas d'apple il est nécessaire pour accéder à la
					// clef privée de mettre un mot de passe bidon
					PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, null);
					KeyPair keyPair = new KeyPair(certificat, privateKey, TypeProvider.PKCS11);
					return keyPair;
				}
			}

		} catch (KeyStoreException e) {
			throw new RecuperationCertificatException("Erreur lors de la récupération des alias se trouvant dans le key store du provider "
					+ TypeProvider.PKCS11, e);
		} catch (UnrecoverableKeyException e) {
			throw new RecuperationCertificatException(
					"Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + TypeProvider.PKCS11
							+ " pour l'alias " + alias, e);
		} catch (NoSuchAlgorithmException e) {
			throw new RecuperationCertificatException(
					"Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + TypeProvider.PKCS11
							+ " pour l'alias " + alias, e);
		}

		return null;
	}

	public File getPkcs11Librairie() {
		return pkcs11Librairie;
	}
}
