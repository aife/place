package com.atexo.serveurCryptographique.utilitaire.magasin;

import java.util.EventListener;
import java.util.List;

import com.atexo.serveurCryptographique.utilitaire.CertificatItem;
import com.atexo.serveurCryptographique.utilitaire.TypeOs;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurSignature.CertificatUtil;

/**
 * Interface générique d'intialisation de l'interface graphique de l'utilisateur.
 */
public interface CertificateSelectUiService<C extends EventListener> {

  /**
   * Permet d'initialiser l'interface graphique de l'utilisateur.
   * 
   * @param listener le type de listeneur
   * @param typeOs le type d'os
   * @param typeProvider le type de provider
   * @param filtrerSignatureModeRGS pour activer le filtre sur les roles pour le mode RGS ou non
   * @param typeCertificat l'ensemble des types de rôles a appliquer si mode RGS non active
   */
  void initUi(C listener, TypeOs typeOs, TypeProvider typeProvider, boolean filtrerSignatureModeRGS, boolean verificationCertificatPerime, CertificatUtil.TypeCertificat... typeCertificat);

}
