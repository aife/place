package com.atexo.serveurCryptographique.utilitaire;

import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
import com.atexo.serveurSignature.CertificatUtil;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Classe abstraite permettant de sélectionner le bon KeyStore à charger en
 * fonction du type de provider.
 */
public abstract class AbstractKeyStoreHandler {

	private static Boolean isPkcs11ProviderLoaded = false;

	public static Provider recupererProvider(String nomProvider) {
		Provider provider = Security.getProvider(nomProvider);
		return provider;
	}

	public static Provider recupererProvider(TypeProvider typeProvider) {
		Provider provider = Security.getProvider(typeProvider.getNom());
		return provider;
	}

	/**
	 * Récupére le KeyStore en fonction du type de provider sélectionné.
	 *
	 * @param typeProvider le type de provider
	 * @return le KeyStore
	 * @throws RecuperationCertificatException
	 */
	public static KeyStore getKeyStore(TypeProvider typeProvider) throws RecuperationCertificatException {
		KeyStore keyStore = null;
		try {

			switch (typeProvider) {
				case PKCS11:
					if (!isPkcs11ProviderLoaded) {
						Pkcs11Handler.getInstance().addProviders();
						AbstractKeyStoreHandler.isPkcs11ProviderLoaded = true;
					}
				case MSCAPI:
				case PKCS12:
					keyStore = KeyStore.getInstance(typeProvider.getType());
					break;
				case APPLE:
					keyStore = KeyStore.getInstance(typeProvider.getType(), typeProvider.getNom());
					break;
			}
		} catch (Exception e) {
			throw new RecuperationCertificatException("Impossible d'accèder au keysotre pour le provider " + typeProvider, e);
		}

		return keyStore;
	}

	/**
	 * Retourne la liste des certificats du Magasin de Windows / Mac Os
	 *
	 * @param typeProvider            le type de provider
	 * @param keyStore                le keystore
	 * @param hashCodes               liste des hashs de certificat déjà intégré
	 * @param filtrerSignatureModeRGS <code>true</code> si on est en mode RGS, sinon
	 *                                <code>false</code>
	 * @param typeCertificats         les type de rôles autorisés
	 * @return la liste des certifcats personnels de l'utilisateur.
	 * @throws RecuperationCertificatException
	 */
	protected List<CertificatItem> recupererCertificats(TypeProvider typeProvider, KeyStore keyStore, Set<String> hashCodes, boolean smartCard, boolean filtrerSignatureModeRGS,
														CertificatUtil.TypeCertificat... typeCertificats) throws RecuperationCertificatException {

		if (hashCodes == null) {
			hashCodes = new HashSet<String>();
		}
		List<CertificatItem> listCertifItem = new ArrayList<>();

		try {

			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String aliasKey = aliases.nextElement();
				boolean estClef = keyStore.isKeyEntry(aliasKey);
				if (estClef) {
					X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);

					boolean signatureElectroniqueActif = true;
					boolean chiffrementActif = true;
					boolean authentificationActif = true;

					if (!filtrerSignatureModeRGS && typeCertificats != null && typeCertificats.length > 0) {

						signatureElectroniqueActif = false;
						chiffrementActif = false;
						authentificationActif = false;

						for (CertificatUtil.TypeCertificat typeCertificat : typeCertificats) {
							switch (typeCertificat) {

								case SignatureElectronique:
									signatureElectroniqueActif = CertificatUtil.isUtilisablePourSignatureElectronique(certificat);
									break;
								case Chiffrement:
									chiffrementActif = CertificatUtil.isUtilisablePourChiffrement(certificat);
									break;
								case Authentification:
									authentificationActif = CertificatUtil.isUtilisablePourAuthentification(certificat);
									break;
							}
						}
					}

					boolean filtrerRGS = filtrerSignatureModeRGS && !CertificatUtil.isUtilisablePourChiffrement(certificat)
							&& CertificatUtil.isUtilisablePourSignatureElectronique(certificat) && CertificatUtil.isValideDate(certificat) && CertificatUtil.isConformite(certificat);
					if (filtrerRGS || (!filtrerSignatureModeRGS && (signatureElectroniqueActif || chiffrementActif || authentificationActif))) {

						// on rajoute a la liste uniquement s'il y a une utilité
						// au certificat listé
						String sujetCn = CertificatUtil.getCN(certificat.getSubjectDN());
						String issuerCn = CertificatUtil.getCN(certificat.getIssuerDN());
						String dateValidation = Util.formaterDate(certificat.getNotAfter());
						String usage = CertificatUtil.getUtilisablePour(certificat, true);
						String hashCode = String.valueOf(certificat.hashCode());

						// on vérifie de ne pas ajouter n fois le même
						// certificat
						if (!hashCodes.contains(hashCode)) {
							CertificatItem item = new CertificatItem(aliasKey, sujetCn, issuerCn, dateValidation, certificat.getNotAfter(), usage, smartCard, hashCode, TypeProvider.PKCS11);
							if (CertificatUtil.isConformite(certificat)) {
								item.setConformite("Adapt\u00E9e");
							} else {
								item.setConformite("Tol\u00E9r\u00E9e jusqu'en septembre 2017");
							}
							listCertifItem.add(item);
							hashCodes.add(hashCode);
						}
					}
				}

			}
		} catch (KeyStoreException e) {
			throw new RecuperationCertificatException("Erreur lors de la récupération  des certificats se trouvant dans le key store du provider " + typeProvider, e);
		}

		return listCertifItem;
	}

}
