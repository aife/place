package com.atexo.serveurCryptographique.utilitaire.logging;


import java.io.File;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atexo.serveurCryptographique.utilitaire.Util;


/**
 *
 */
public abstract class LoggerFactory {

    private static String nomLogger;

    private static File fichier;

    private LoggerFactory() {
    }

    private static File FILE_LOG;

    public static Logger getLogger(String nom) {

        if (!isExiste(nom)) {
            Logger logger = Logger.getLogger(nom);
            //logger.setUseParentHandlers(!activerConsoleHandler);
            java.util.logging.LogManager.getLogManager().reset();

            SingleLineLogFormatter singleLineLogFormatter = new SingleLineLogFormatter(true, true);
            ConsoleHandler consoleHandler = new ConsoleHandler();
            consoleHandler.setFormatter(singleLineLogFormatter);
            
            logger.addHandler(consoleHandler);

            try {
                FileHandler fileHandler = new FileHandler(FILE_LOG.getPath(), true);
                fileHandler.setFormatter(singleLineLogFormatter);
                logger.addHandler(fileHandler);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return logger;
        } else {
            return Logger.getLogger(nom);
        }
    }

    public static String getNomLogger() {
        return nomLogger;
    }

    public static void setNomLogger(String nomLogger) {
        LoggerFactory.nomLogger = nomLogger;
        try {
            File fichierTemporaire = File.createTempFile("fichiertemp", ".tmp");
            String cheminRepertoireTemporaireSysteme = fichierTemporaire.getParent();
            String date = Util.formaterDate(new Date(), Util.DATE_TIME_PATTERN_YYYMMDD);
            fichier = new File(new File(cheminRepertoireTemporaireSysteme), nomLogger + "_" + date + ".log");
            if (!fichier.exists()) {
                fichier.createNewFile();
            }
            fichierTemporaire.delete();
        } catch (Exception e) {
        }
        FILE_LOG = fichier;
    }

    public static File getFichier() {
        return fichier;
    }

    public static void changerLoggerLevel(String nom, Level levelConsole, Level levelFichier) {

        if (isExiste(nom) && (levelConsole != null || levelFichier != null)) {

            Logger logger = getLogger(nom);
            for (Handler handler : logger.getHandlers()) {

                if (levelConsole != null && handler instanceof ConsoleHandler) {
                    handler.setLevel(levelConsole);
                } else if (levelFichier != null && handler instanceof FileHandler) {
                    handler.setLevel(levelFichier);
                }
            }
        }
    }

    public static boolean isExiste(String nom) {
        return java.util.logging.LogManager.getLogManager().getLogger(nom) != null;
    }
}
