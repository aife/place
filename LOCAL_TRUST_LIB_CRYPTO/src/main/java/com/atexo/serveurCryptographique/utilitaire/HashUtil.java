package com.atexo.serveurCryptographique.utilitaire;

import java.io.File;
import java.io.IOException;

import org.bouncycastle.crypto.digests.GeneralDigest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;

/**
 * Classe utilitaire de génération de hashs
 */
public abstract class HashUtil {

    /**
     * Génére un hash en héxadécimal des données en fonction du type d'algorithm de
     * hash sélectionné.
     *
     * @param typeAlgorithmHash le type d'algorithm de hash à appliquer
     * @param donnees           les données qui seront hashés
     * @return le hash des données
     */
    public static String genererHashShaHexadecimal(TypeAlgorithmHash typeAlgorithmHash, byte[] donnees) {
        return typeAlgorithmHash == TypeAlgorithmHash.SHA1 ? genererHashSha1Hexadecimal(donnees) : genererHashSha256Hexadecimal(donnees);
    }

    /**
     * Génére un hash en héxadécimal du fichier en fonction du type d'algorithm de
     * hash sélectionné.
     *
     * @param typeAlgorithmHash le type d'algorithm de hash à appliquer
     * @param fichier           le fichier qui sera hashés
     * @return le hash du fichier
     */
    public static String genererHashShaHexadecimal(TypeAlgorithmHash typeAlgorithmHash, File fichier) throws IOException {
        return typeAlgorithmHash == TypeAlgorithmHash.SHA1 ? genererHashSha1Hexadecimal(fichier) : genererHashSha256Hexadecimal(fichier);
    }

    /**
     * Génére un hash binaire des données en fonction du type d'algorithm de
     * hash sélectionné.
     *
     * @param typeAlgorithmHash le type d'algorithm de hash à appliquer
     * @param donnees           les données qui seront hashés
     * @return le hash des données
     */
    public static byte[] genererHashShaBinaire(TypeAlgorithmHash typeAlgorithmHash, byte[] donnees) {
        return typeAlgorithmHash == TypeAlgorithmHash.SHA1 ? genererHashSha1Binaire(donnees) : genererHashSha256Binaire(donnees);
    }

    /**
     * Génére un hash binaire du fichier en fonction du type d'algorithm de
     * hash sélectionné.
     *
     * @param typeAlgorithmHash le type d'algorithm de hash à appliquer
     * @param fichier           le fichier qui sera hashés
     * @return le hash du fichier
     */
    public static byte[] genererHashShaBinaire(TypeAlgorithmHash typeAlgorithmHash, File fichier) throws IOException {
        return typeAlgorithmHash == TypeAlgorithmHash.SHA1 ? genererHashSha1Binaire(fichier) : genererHashSha256Binaire(fichier);
    }

    /**
     * Génére un hash sha1 en héxadécimal des données.
     *
     * @param donnees les données qui seront hashés
     * @return le hash des données
     */
    public static String genererHashSha1Hexadecimal(byte[] donnees) {
        return genererHashSha1Hexadecimal(donnees, donnees.length);
    }

    /**
     * Génére un hash sha1 en héxadécimal des données.
     *
     * @param donnees  les données qui seront hashés
     * @param longueur la taille max des données
     * @return le hash des données
     */
    public static String genererHashSha1Hexadecimal(byte[] donnees, int longueur) {
        byte[] hashResult = genererHashBinaire(new SHA1Digest(), donnees, longueur);
        return convertirBinaireEnHexadecimal(hashResult);
    }

    /**
     * Génére un hash sha1 binaire des données.
     *
     * @param donnees les données qui seront hashés
     * @return le hash des données
     */
    public static byte[] genererHashSha1Binaire(byte[] donnees) {
        return genererHashSha1Binaire(donnees, donnees.length);
    }

    /**
     * Génére un hash sha1 binaire des données.
     *
     * @param donnees  les données qui seront hashés
     * @param longueur la taille max des données
     * @return le hash des données
     */
    public static byte[] genererHashSha1Binaire(byte[] donnees, int longueur) {
        byte[] hashResult = genererHashBinaire(new SHA1Digest(), donnees, longueur);
        return hashResult;
    }

    /**
     * Génére un hash sha256 en héxadécimal des données.
     *
     * @param donnees les données qui seront hashés
     * @return le hash des données
     */
    public static String genererHashSha256Hexadecimal(byte[] donnees) {
        return genererHashSha256Hexadecimal(donnees, donnees.length);
    }

    /**
     * Génére un hash sha256 en héxadécimal des données.
     *
     * @param donnees  les données qui seront hashés
     * @param longueur la taille max des données
     * @return le hash des données
     */
    public static String genererHashSha256Hexadecimal(byte[] donnees, int longueur) {
        byte[] hashResult = genererHashBinaire(new SHA256Digest(), donnees, longueur);
        return convertirBinaireEnHexadecimal(hashResult);
    }

    /**
     * Génére un hash sha256 binaire des données.
     *
     * @param donnees les données qui seront hashés
     * @return le hash des données
     */
    public static byte[] genererHashSha256Binaire(byte[] donnees) {
        return genererHashSha256Binaire(donnees, donnees.length);
    }

    /**
     * Génére un hash sha256 binaire des données.
     *
     * @param donnees  les données qui seront hashés
     * @param longueur la taille max des données
     * @return le hash des données
     */
    public static byte[] genererHashSha256Binaire(byte[] donnees, int longueur) {
        byte[] hashResult = genererHashBinaire(new SHA256Digest(), donnees, longueur);
        return hashResult;
    }

    /**
     * Génére un hash binaire des données en fonction du type d'algorithm de sha selectionné.
     *
     * @param algorithmHash l'algorithm de hash
     * @param donnees       les données qui seront hashés
     * @param longueur      la taille max des données
     * @return le hash des données
     */
    public static byte[] genererHashBinaire(GeneralDigest algorithmHash, byte[] donnees, int longueur) {
        algorithmHash.update(donnees, 0, longueur);
        byte[] hashResult = new byte[algorithmHash.getDigestSize()];
        algorithmHash.doFinal(hashResult, 0);
        return hashResult;
    }

    /**
     * Génére un hash sha256 hexadecimal du fichier en fonction du type d'algorithm de sha selectionné.
     *
     * @param fichier le fichier qui sera hashé
     * @return le hash du fichier
     */
    public static String genererHashSha256Hexadecimal(File fichier) throws IOException {
        return genererHashHexadecimal(new SHA256Digest(), fichier);
    }

    /**
     * Génére un hash sha1 hexadecimal du fichier en fonction du type d'algorithm de sha selectionné.
     *
     * @param fichier le fichier qui sera hashé
     * @return le hash du fichier
     */
    public static String genererHashSha1Hexadecimal(File fichier) throws IOException {
        return genererHashHexadecimal(new SHA1Digest(), fichier);
    }

    /**
     * Génére un hash sha256 binaire du fichier en fonction du type d'algorithm de sha selectionné.
     *
     * @param fichier le fichier qui sera hashé
     * @return le hash du fichier
     */
    public static byte[] genererHashSha256Binaire(File fichier) throws IOException {
        return genererHashBinaire(new SHA256Digest(), fichier);
    }

    /**
     * Génére un hash sha1 binaire du fichier en fonction du type d'algorithm de sha selectionné.
     *
     * @param fichier le fichier qui sera hashé
     * @return le hash du fichier
     */
    public static byte[] genererHashSha1Binaire(File fichier) throws IOException {
        return genererHashBinaire(new SHA1Digest(), fichier);
    }

    /**
     * Génére un hash hexadécimal du fichier en fonction du type d'algorithm de sha selectionné.
     *
     * @param algorithmHash l'algorithm de hash
     * @param fichier       le fichier qui sera hashé
     * @return le hash du fichier
     */
    public static String genererHashHexadecimal(GeneralDigest algorithmHash, File fichier) throws IOException {
        byte[] hashResult = genererHashBinaire(algorithmHash, fichier);
        return convertirBinaireEnHexadecimal(hashResult);
    }

    /**
     * Génére un hash binaire du fichier en fonction du type d'algorithm de sha selectionné.
     *
     * @param algorithmHash l'algorithm de hash
     * @param fichier       le fichier qui sera hashé
     * @return le hash du fichier
     */
    public static byte[] genererHashBinaire(GeneralDigest algorithmHash, File fichier) throws IOException {

        int tailleMaximale = ((int) fichier.length());
        int fin = ReponseAnnonceConstantes.TAILLE_MAXIMALE_BLOC_CHIFFREMENT;

        int tailleATraiter = 0;
        int indexFichier = 0;
        for (indexFichier = 0; indexFichier * fin <= tailleMaximale; indexFichier++) {
            if ((indexFichier * fin + fin) <= tailleMaximale) {
                tailleATraiter = fin;
            } else {
                tailleATraiter = (int) tailleMaximale - (indexFichier * fin);
            }
            byte[] contentBytes = FileUtil.lire(fichier, indexFichier * fin, tailleATraiter);


            algorithmHash.update(contentBytes, 0, tailleATraiter);
        }
        byte[] hashResult = new byte[algorithmHash.getDigestSize()];
        algorithmHash.doFinal(hashResult, 0);

        return hashResult;
    }

    /**
     * Converti un hash binaire en hash héxadécimal.
     *
     * @param hashBinaire le hash binaire
     * @return le hash en héxadécimal
     */
    public static String convertirBinaireEnHexadecimal(byte[] hashBinaire) {
        if (hashBinaire == null)
            return "";
        char[] hexChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer sb = new StringBuffer(hashBinaire.length * 2);
        for (int i = 0; i < hashBinaire.length; i++) {
            sb.append(hexChar[(hashBinaire[i] & 0xf0) >>> 4]);
            sb.append(hexChar[hashBinaire[i] & 0x0f]);
        }
        return sb.toString();
    }

    /**
     * Converti un hash héxadécimal en hash binaire
     *
     * @param hashHexadecimal le hash en héxadécimal
     * @return le hash binaire
     */
    public static byte[] convertirHexadecimalEnBinaire(String hashHexadecimal) {
        int len = hashHexadecimal.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hashHexadecimal.charAt(i), 16) << 4) + Character.digit(hashHexadecimal.charAt(i + 1), 16));
        }
        return data;
    }
}
