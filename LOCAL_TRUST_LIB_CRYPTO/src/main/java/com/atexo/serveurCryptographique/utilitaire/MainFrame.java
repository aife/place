package com.atexo.serveurCryptographique.utilitaire;



import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateEvent;


/**
 * @author Thibaut Decaudain
 */
public class MainFrame extends AbstractMainFrame<MagasinCertificateEvent> implements ActionListener {

  private static final long serialVersionUID = 1L;

  private List<CertificatItem> certificateItems;
  boolean verificationCertificatPerime = true;
  public static final int width = 900;
  public static final int height = 300;

  private JTable table;
  private JButton validerButton;
  private JButton annulerButton;

  /**
   * CrÃ©e une nouvelle fenetre popup affichant un tableau de certificats Un clic sur Valider lance
   * la methode certificateChosenEvent() des listeners de cette popup
   * 
   * @param certificatItems
   */
  public MainFrame(List<CertificatItem> certificatItems, boolean verificationCertificatPerime) {
    super();
    setAlwaysOnTop(true);
    this.certificateItems = certificatItems;
    this.verificationCertificatPerime = verificationCertificatPerime;
    this.build();
    this.setVisible(true);

  }

  private void build() {
    setTitle(I18nUtil.get("SWING_MSCAPI_LISTE_TITRE"));
    setSize(width, height);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setContentPane(buildContentPane());
  }

  private JPanel buildContentPane() {
    JPanel mainPanel = new JPanel();
    mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    mainPanel.setLayout(new BorderLayout());

    // Ajout de la JTable des certificats
    TableModel tableModel = new TableModel(new String[] {I18nUtil.get("SWING_MSCAPI_LISTE_NOM_CERTIFICAT"),
            I18nUtil.get("SWING_MSCAPI_LISTE_NOM_EMETTEUR"), I18nUtil.get("SWING_MSCAPI_LISTE_DATE_EXPIRATION"),
            I18nUtil.get("SWING_MSCAPI_LISTE_UTILISATION_CLE"), I18nUtil.get("SWING_MSCAPI_LISTE_CONFORMITE")}, certificateItems);
    table = new JTable();
    table.setModel(tableModel);
    table.setRowSelectionAllowed(true);
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    TableColumn column = table.getColumnModel().getColumn(2);
    column.setPreferredWidth(100);
    TableColumn column1 = table.getColumnModel().getColumn(1);
    column1.setPreferredWidth(120);
    TableColumn column2 = table.getColumnModel().getColumn(0);
    column2.setPreferredWidth(170);
    TableColumn column3 = table.getColumnModel().getColumn(3);
    column3.setPreferredWidth(330);
    TableColumn column4 = table.getColumnModel().getColumn(4);
    column4.setPreferredWidth(190);
    
    JScrollPane scroller = new JScrollPane(table);
    scroller.setVerticalScrollBar(new JScrollBar());
    mainPanel.add(scroller, BorderLayout.CENTER);

    JPanel bottomPanel = new JPanel();
    bottomPanel.setLayout(new BorderLayout());
    validerButton = new JButton(I18nUtil.get("SWING_ACTION_VALIDER"));
    validerButton.addActionListener(this);
    annulerButton = new JButton(I18nUtil.get("SWING_ACTION_ANNULER"));
    annulerButton.addActionListener(this);
    bottomPanel.add(annulerButton, BorderLayout.WEST);
    bottomPanel.add(validerButton, BorderLayout.EAST);
    bottomPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    JLabel text = new JLabel("<html><p align=\"justify\">"
                              + "Si aucun certificat ne s'affiche, v\u00E9rifier si celui-ci est bien install\u00E9 dans le magasin des certificats du navigateur, ou v\u00E9rifier s'il est bien conforme au RGS.</p></html>");
    text.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    bottomPanel.add(text , BorderLayout.PAGE_END);
    mainPanel.add(bottomPanel, BorderLayout.PAGE_END);

    return mainPanel;
  }


  /**
   * Methode appelÃ© lors du clique sur le bouton valider Si validÃ© : on envoie a tous les
   * CertificateEventListener l'Ã©vÃ©nement CertificateEvent contenant le CertificateItem sÃ©lectionnÃ©
   */
 // @Override
  public void actionPerformed(ActionEvent evt) {
    Object source = evt.getSource();

    if (source == annulerButton) {
      this.dispose();
      return;
    }

    int index = table.getSelectedRow();
    if (index >= 0) {
      MagasinCertificateEvent event = new MagasinCertificateEvent(this, certificateItems.get(index));

      // VÃ©rification du certificat choisi
      CertificatItem item = certificateItems.get(index);
      if (item != null && Util.isExpire(item.getDateExpiration()) && verificationCertificatPerime) {
        int userClic = JOptionPane.showConfirmDialog(this, I18nUtil.get("SWING_ACTION_ALERTE_PAR_DEFAUT_CERTIFICAT_PERIME"), I18nUtil.get("SWING_ACTION_ALERTE"), JOptionPane.YES_NO_OPTION);
        if (userClic != JOptionPane.YES_OPTION) {
          return; // stop
        }
      }
      this.dispose();
      this.fireCertificateEvent(event);

    } else {
      JOptionPane.showMessageDialog(this, I18nUtil.get("SWING_ACTION_ALERTE_SELECTIONNER_CERTIFICAT"), I18nUtil.get("SWING_ACTION_ALERTE"), JOptionPane.WARNING_MESSAGE);
    }
  }
}
