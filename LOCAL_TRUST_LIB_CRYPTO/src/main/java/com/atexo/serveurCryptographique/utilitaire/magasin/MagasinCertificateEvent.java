package com.atexo.serveurCryptographique.utilitaire.magasin;
import java.util.EventObject;

import com.atexo.serveurCryptographique.utilitaire.CertificatItem;

/**
 * Evenement qui est lancé lors de la sélection d'un certificat depuis le magasin
 * de certificats. Cet évenement contient le certificat sélectionné.
 */
public class MagasinCertificateEvent extends EventObject {

    private static final long serialVersionUID = -5224024987900710300L;

    private CertificatItem certificateItem;
	
	public MagasinCertificateEvent(Object source, CertificatItem item) {
		super(source);
		this.certificateItem = item;
	}
	
	public CertificatItem getCertificateItem() {
		return this.certificateItem;
	}
}
