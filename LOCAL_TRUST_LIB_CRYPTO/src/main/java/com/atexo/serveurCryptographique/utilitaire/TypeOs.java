package com.atexo.serveurCryptographique.utilitaire;

/**
 * Le type d'os
 */
public enum TypeOs {
    Windows, MacOs, Linux, Indetermine;
}
