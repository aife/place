package com.atexo.serveurCryptographique.utilitaire.magasin;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.atexo.serveurCryptographique.utilitaire.AbstractKeyStoreHandler;
import com.atexo.serveurCryptographique.utilitaire.CertificatItem;
import com.atexo.serveurCryptographique.utilitaire.KeyPair;
import com.atexo.serveurCryptographique.utilitaire.RecuperationCertificatException;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import com.atexo.serveurSignature.CertificatUtil;

/**
 * Classe permettant de gérer les certificats se trouvant directement dans le
 * Magasin de certificat de Windows (provider SunMSCAPI) ou bien de Mac Os
 * (KeyChainStore)
 */
public class MagasinHandler extends AbstractKeyStoreHandler {


    private static MagasinHandler magasinHandler;

    public static MagasinHandler getInstance() {
        if (magasinHandler == null) {
            magasinHandler = new MagasinHandler();
        }
        return magasinHandler;
    }

    /**
     * Retourne la liste des certificats du Magasin de Windows / Mac Os
     *
     * @param typeProvider
     *            le type de provider
     * @param hashCodes
     *            liste des hashs de certificat déjà intégré
     * @return la liste des certifcats personnels de l'utilisateur.
     * @throws RecuperationCertificatException
     *
     */
    public List<CertificatItem> recupererCertificats(TypeProvider typeProvider, Set<String> hashCodes, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificats)
            throws RecuperationCertificatException {

        // chargement depuis le KeyStore
        KeyStore keyStore = getKeyStore(typeProvider, true);

        return super.recupererCertificats(typeProvider, keyStore, hashCodes, false, filtrerSignatureModeRGS, typeCertificats);
    }

    /**
     * Retour le keyPair à partir du provider et de l'alias.
     *
     * @param typeProvider
     *            le type de provider.
     * @param alias
     *            l'alias à recherche depuis le Keystore
     * @return le keyPair
     * @throws RecuperationCertificatException
     *
     */
    public KeyPair getKeyPair(TypeProvider typeProvider, String alias) throws RecuperationCertificatException {

        try {
            // chargement depuis le KeyStore
            KeyStore keyStore = getKeyStore(typeProvider, true);

            // parcourt de l'ensemble des occurances contenus dans le KeyStore
            // et récupération du bit-clé sélectionné
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                String aliasKey = aliases.nextElement();
                // System.out.println("aliasKey => " + aliasKey);
                if (aliasKey.equals(alias)) {
                    X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
                    // dans le cas d'apple il est nécessaire pour accéder à la
                    // clef privée de mettre un mot de passe bidon
                    PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, typeProvider == TypeProvider.APPLE ? "convergence".toCharArray() : null);
                    KeyPair keyPair = new KeyPair(certificat, privateKey, typeProvider);
                    return keyPair;
                }
            }

        } catch (KeyStoreException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération des alias se trouvant dans le key store du provider " + typeProvider, e);
        } catch (UnrecoverableKeyException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + typeProvider + " pour l'alias " + alias, e);
        } catch (NoSuchAlgorithmException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + typeProvider + " pour l'alias " + alias, e);
        }

        return null;
    }

    /**
     * Retourne un bi-clé à partir du serial et de l'issuer.
     *
     * @param typeProvider
     *            le type de provider
     * @param serial
     *            le serial
     * @param emetteur
     *            l'issuer.
     * @return le keyPair
     * @throws RecuperationCertificatException
     *
     */
    public KeyPair getKeyPair(TypeProvider typeProvider, BigInteger serial, String emetteur) throws RecuperationCertificatException {
        try {
            // chargement depuis le KeyStore
            KeyStore keyStore = getKeyStore(typeProvider, true);

            // parcourt de l'ensemble des occurances contenus dans le KeyStore
            // et récupération du bi-clé sélectionné
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                String aliasKey = aliases.nextElement();
                X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
                BigInteger certificatSerial = certificat.getSerialNumber();
                String certificatEmetteur = certificat.getIssuerDN().toString();
                boolean serialsEgaux = certificatSerial.equals(serial);
                boolean emetteursEgaux = certificatEmetteur.equals(emetteur);
                System.out.println("Alias : " + aliasKey);
                System.out.println("Emetteur : " + certificatEmetteur + " => Emetteurs égaux : " + emetteursEgaux);
                System.out.println("Serial : " + certificatSerial + " => Serials égaux : " + serialsEgaux);
                if (serialsEgaux && emetteursEgaux) {
                    PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, null);
                    KeyPair keyPair = new KeyPair(certificat, privateKey, typeProvider);

                    return keyPair;
                }
            }
        } catch (Exception e) {
            throw new RecuperationCertificatException("Erreur de recupération du certificat personnel avec comme Serial " + serial + " et comme Issuer " + emetteur, e);
        }

        return null;
    }

    /**
     * Retourne un bi-clé à partir du serial et de l'issuer.
     *
     * @param keyStore
     *            le keystore
     * @param typeProvider
     *            le type de provider
     * @param serial
     *            le serial
     * @param issuer
     *            l'issuer.
     * @return le keyPair
     * @throws RecuperationCertificatException
     *
     */
    public KeyPair getKeyPair(KeyStore keyStore, TypeProvider typeProvider, BigInteger serial, String issuer) throws RecuperationCertificatException {
        try {

            // parcourt de l'ensemble des occurances contenus dans le KeyStore
            // et récupération du bit-clé sélectionné
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                String aliasKey = aliases.nextElement();
                X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);

                if (certificat.getSerialNumber().equals(serial) && certificat.getIssuerDN().toString().equals(issuer)) {

                    PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, null);
                    KeyPair keyPair = new KeyPair(certificat, privateKey, typeProvider);

                    return keyPair;
                }
            }
        } catch (Exception e) {
            throw new RecuperationCertificatException("Erreur de recupération du certificat personnel avec comme Serial " + serial + " et comme Issuer " + issuer, e);
        }

        return null;
    }

    /**
     * Récupére le KeyStore chargé avec les alias des certificats contenu dedans
     * qui ont été parsés afin d'être rendu unique.
     *
     * @param typeProvider
     *            le type de provider
     * @param reparerAlias
     *            doit reparer ou non les alias
     * @return le KeyStore initialisé
     * @throws RecuperationCertificatException
     *
     */
    public KeyStore getKeyStore(TypeProvider typeProvider, boolean reparerAlias) throws RecuperationCertificatException {
        try {
            // chargement depuis le KeyStore
            KeyStore keyStore = getKeyStore(typeProvider);
            keyStore.load(null, null);
            if (reparerAlias) {
                reparerAlias(typeProvider, keyStore);
            }
            return keyStore;

        } catch (NoSuchAlgorithmException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + typeProvider, e);
        } catch (CertificateException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + typeProvider, e);
        } catch (IOException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + typeProvider, e);
        }
    }

    /**
     * Permet d'ajouter un suffixe unique directement à l'ensemble des alias se
     * trouvant dans l'instance du KeyStore afin de corriger le souci de
     * dupplicat.
     *
     * @param typeProvider
     *            le type de provider
     * @param keyStore
     *            le KeyStore à mettre à jour
     */

    private void reparerAlias(TypeProvider typeProvider, KeyStore keyStore) {
        // Pour décrire un attribut, la classe java.lang.reflect.Field est
        // utilisée.
        Field field;
        // This class defines the Service Provider Interface (SPI) for the
        // KeyStore class
        KeyStoreSpi keyStoreVeritable;

        try {
            field = keyStore.getClass().getDeclaredField("keyStoreSpi");
            /***
             * getClass() : Returns the runtime class of this Object.
             * getDeclaredField():Returns a Field object that reflects the
             * specified declared field of the class or interface represented by
             * this Class object. The name parameter is a String that specifies
             * the simple name of the desired field. Note that this method will
             * not reflect the length field of an array class.
             ***/
            field.setAccessible(true);
            /**
             * KeyStoreSpi : This class defines the Service Provider Interface
             * (SPI) for the KeyStore class. get(ks) : Returns the value of the
             * field represented by this Field, on the specified object. The
             * value is automatically wrapped in an object if it has a primitive
             * type.
             */

            keyStoreVeritable = (KeyStoreSpi) field.get(keyStore);// ksv
            if (typeProvider.getKeyStoreVeritable() != null && typeProvider.getKeyStoreVeritable().equals(keyStoreVeritable.getClass().getName())) {
                Collection entries;
                String alias, hashCode;
                X509Certificate[] certificates;

                field = keyStoreVeritable.getClass().getEnclosingClass().getDeclaredField("entries");
                field.setAccessible(true);
                if (field.get(keyStoreVeritable) instanceof HashMap) {
                    entries = ((HashMap) field.get(keyStoreVeritable)).values();
                } else {
                    entries = (Collection) field.get(keyStoreVeritable);
                }

                for (Object entry : entries) {
                    field = entry.getClass().getDeclaredField("certChain");
                    field.setAccessible(true);
                    certificates = (X509Certificate[]) field.get(entry);

                    if (certificates != null && certificates.length > 0) {

                        hashCode = Integer.toString(certificates[0].hashCode());

                        field = entry.getClass().getDeclaredField("alias");
                        field.setAccessible(true);
                        alias = (String) field.get(entry);

                        if (!alias.equals(hashCode)) {
                            field.set(entry, alias.concat(" - ").concat(hashCode));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }
}
