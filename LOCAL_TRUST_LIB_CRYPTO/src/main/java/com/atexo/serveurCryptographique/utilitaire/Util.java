package com.atexo.serveurCryptographique.utilitaire;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


import com.atexo.serveurCryptographique.utilitaire.commons.ExceptionUtils;

import javax.xml.bind.DatatypeConverter;


/**
 * Classe d'utilitaire.
 */
public abstract class Util {

    final static int[] CARACTERES_ILLEGAUX_NOM_FICHIER = {34, 60, 62, 124, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 58, 42, 63, 92, 47};

    public static final String DATE_PATTERN_SLASH = "dd/MM/yyyy";

    public static final String DATE_TIME_PATTERN_TIRET = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_TIME_PATTERN_TIRET_SANS_SECONDES = "yyyy-MM-dd HH:mm";

    public static final String DATE_TIME_PATTERN_YYYMMDD = "yyyyMMdd";

    public static final String DATE_TIME_PATTERN_YYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static final String DATE_TIME_PATTERN_YYYMMDDHHMMSSSS = "yyyyMMddHHmmssSS";

    public static String creerISO8601DateTime(Date date) {
        DateFormat ISO8601Local = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        TimeZone timeZone = TimeZone.getDefault();
        ISO8601Local.setTimeZone(timeZone);
        int offset = timeZone.getOffset(date.getTime());

        String sign = "+";
        if (offset < 0) {
            offset = -offset;
            sign = "-";
        }

        int hours = offset / 3600000;
        int minutes = (offset - hours * 3600000) / 60000;
        if (offset != hours * 3600000 + minutes * 60000) {
            // E.g. TZ=Asia/Riyadh87
            throw new RuntimeException("TimeZone offset (" + sign + offset + " ms) is not an exact number of minutes");
        }

        DecimalFormat twoDigits = new DecimalFormat("00");
        String ISO8601Now = ISO8601Local.format(date) + sign + twoDigits.format(hours) + ":" + twoDigits.format(minutes);

        return ISO8601Now;
    }

    public static Date convertirISO8601DateTime(String dateISO8601) {
        Date date = null;
        try {
            Calendar calendar = DatatypeConverter.parseDateTime(dateISO8601);
            if (calendar != null) {
                date = calendar.getTime();
            }
        } catch (IllegalArgumentException e) {

        }
        return date;
    }

    /**
     * Converti une date au format dd/MM/yyyy
     *
     * @param date la date à convertir
     * @return la date au format String convertir selon le pattern dd/MM/yyyy
     */
    public static String formaterDate(Date date) {
        return formaterDate(date, DATE_PATTERN_SLASH);
    }

    /**
     * Converti une date selon le format specifié.
     *
     * @param date    la date à convertir
     * @param pattern le pattern de formatage à appliquer
     * @return la date au format String convertir selon le pattern spécifié
     */
    public static String formaterDate(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    /**
     * Converti une String selon le format specifié.
     *
     * @param date    la date à convertir
     * @param pattern le pattern de formatage à appliquer
     * @return la date converti à partir du pattern spécifié
     */
    public static Date convertirDate(String date, String pattern) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(date);
    }

    /**
     * Verifie si la date est expirée par rapport à maintenant.
     *
     * @param dateExpiration la date à controler
     * @return true si la date est expirée, sinon false.
     */
    public static boolean isExpire(Date dateExpiration) {
        if (dateExpiration.before(new Date())) {
            return true;
        }
        return false;
    }

    /**
     * Permet de vérifier si la version de java est la même ou bien si il ne
     * s'agit pas d'une version inférieur.
     *
     * @param version
     * @return
     */
    public static boolean verifierJavaVersion(int version) {
        String javaVersion = System.getProperty("java.version");
        int iJavaVersion = Integer.parseInt(javaVersion.replace(".", "").replace("_", "").substring(0, 2));
        return iJavaVersion >= version;
    }

    /**
     * Permet de déterminer quel provider utiliser en fonction de
     * l'environnement de l'utilisateur. MSCAPI : - Windows + JRE 6 et 7 en 32
     * bit - Windows + JRE 7 en 64 bit
     * <p/>
     * PKCS12: - l'ensemble des autres cas
     *
     * @return le type de provider à utiliser
     */
    public static TypeProvider determinerProvider() {
        String architecture = System.getProperty("os.arch");
        String nomOs = System.getProperty("os.name");
        String version = System.getProperty("java.specification.version");
        String bits = System.getProperty("sun.arch.data.model");
        return determinerProvider(architecture, nomOs, version, bits);
    }

    /**
     * Permet de déterminer quel provider utiliser en fonction de
     * l'environnement de l'utilisateur. MSCAPI : - Windows + JRE 6 et 7 en 32
     * bit - Windows + JRE 7 en 64 bit
     * <p/>
     * PKCS12: - l'ensemble des autres cas
     *
     * @param architecture le type d'architecture
     * @param nomOs        le nom du système d'exploitation
     * @param version      la version de la jre
     * @param bits         la version 32 ou 64 bits
     * @return le type de provider à utiliser
     */
    public static TypeProvider determinerProvider(String architecture, String nomOs, String version, String bits) {

        Double versionParse = Double.parseDouble(version);

        if (nomOs.startsWith("Windows") && versionParse > Double.parseDouble("1.5")) {

            if (bits.equals("32")) {
                return TypeProvider.MSCAPI;
            } else {
                return Double.parseDouble(version) > Double.parseDouble("1.6") ? TypeProvider.MSCAPI : TypeProvider.PKCS12;
            }

        } else if (nomOs.startsWith("Mac")) {
            return TypeProvider.APPLE;
        } else if (nomOs.startsWith("Linux")) {
            return TypeProvider.PKCS11;
        } else {
            return TypeProvider.PKCS12;
        }
    }

    public static TypeOs determinerOs() {
        String nomOs = System.getProperty("os.name");
        if (nomOs.startsWith("Windows")) {
            return TypeOs.Windows;
        } else if (nomOs.startsWith("Mac")) {
            return TypeOs.MacOs;
        } else if (nomOs.startsWith("Linux")) {
            return TypeOs.Linux;
        } else {
            return TypeOs.Indetermine;
        }
    }

    /**
     * Vérifie si une chaine de caractère est vide.
     *
     * @param string
     * @return
     */
    public static boolean estVide(String string) {
        return string == null || string.trim().length() == 0;
    }

    /**
     * Permet d'arrondir une valeur en spécifiant la précision voulue
     *
     * @param valeur    la valeur à arrondir
     * @param precision la précision de l'arrondissement à effectuer
     * @return
     */
    public static double arrondir(double valeur, int precision) {
        return ((int) (valeur * Math.pow(10, precision) + .5)) / Math.pow(10, precision);
    }

    public static String remplacerPar(String chaine, String[] occurances, String valeur) {
        if (chaine == null) {
            return null;
        }

        String nouvelleChaine = chaine;

        for (String occurance : occurances) {
            nouvelleChaine = nouvelleChaine.replaceAll(occurance, valeur);
        }

        return nouvelleChaine;
    }

    public static String remplacerPar(String chaine, String occurance, String valeur) {
        return remplacerPar(chaine, new String[]{occurance}, valeur);
    }

    public static String getFullStacktraceException(String message, Throwable cause) {
        String stacktrace = ExceptionUtils.getStackTrace(cause);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(message);
        stringBuffer.append("\n");
        stringBuffer.append(stacktrace);
        return stringBuffer.toString();
    }

    public static String supprimerNomFichierCaracteresNonSupportes(String nomFichier) {
        StringBuilder nomFichierModifie = new StringBuilder();
        for (int i = 0; i < nomFichier.length(); i++) {
            int c = nomFichier.charAt(i);
            if (Arrays.binarySearch(CARACTERES_ILLEGAUX_NOM_FICHIER, c) < 0) {
                nomFichierModifie.append((char) c);
            }
        }
        return nomFichierModifie.toString();
    }

    public static boolean isDateCompriseEntre(Date dateComparaitre, Date dateDebut, Date dateFin) {
        return dateDebut.before(dateComparaitre) && dateFin.after(dateComparaitre);
    }
}
