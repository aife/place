package com.atexo.serveurCryptographique.utilitaire.pkcs11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pkcs11LibType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="pkcs11LibType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="libsWindows" type="{}libsType" minOccurs="0"/>
 *         &lt;element name="libsMacOs" type="{}libsType" minOccurs="0"/>
 *         &lt;element name="libsLinux" type="{}libsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="fournisseur" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pkcs11LibType", propOrder = {
    "description",
    "libsWindows",
    "libsMacOs",
    "libsLinux"
})
public class Pkcs11LibType {

    protected String description;
    protected LibsType libsWindows;
    protected LibsType libsMacOs;
    protected LibsType libsLinux;
    @XmlAttribute(required = true)
    protected String fournisseur;

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the libsWindows property.
     *
     * @return
     *     possible object is
     *     {@link LibsType }
     *
     */
    public LibsType getLibsWindows() {
        return libsWindows;
    }

    /**
     * Sets the value of the libsWindows property.
     *
     * @param value
     *     allowed object is
     *     {@link LibsType }
     *
     */
    public void setLibsWindows(LibsType value) {
        this.libsWindows = value;
    }

    /**
     * Gets the value of the libsMacOs property.
     *
     * @return
     *     possible object is
     *     {@link LibsType }
     *
     */
    public LibsType getLibsMacOs() {
        return libsMacOs;
    }

    /**
     * Sets the value of the libsMacOs property.
     *
     * @param value
     *     allowed object is
     *     {@link LibsType }
     *
     */
    public void setLibsMacOs(LibsType value) {
        this.libsMacOs = value;
    }

    /**
     * Gets the value of the libsLinux property.
     *
     * @return
     *     possible object is
     *     {@link LibsType }
     *
     */
    public LibsType getLibsLinux() {
        return libsLinux;
    }

    /**
     * Sets the value of the libsLinux property.
     *
     * @param value
     *     allowed object is
     *     {@link LibsType }
     *
     */
    public void setLibsLinux(LibsType value) {
        this.libsLinux = value;
    }

    /**
     * Gets the value of the fournisseur property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFournisseur() {
        return fournisseur;
    }

    /**
     * Sets the value of the fournisseur property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFournisseur(String value) {
        this.fournisseur = value;
    }

}
