package com.atexo.serveurCryptographique.utilitaire.smartCard;

import java.io.File;

/**
 *
 */
public interface SmartCardInformation {

    String getAtr();

    File getLibrairie();

    String getNomFabriquant();
}
