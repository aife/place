package com.atexo.serveurCryptographique.utilitaire.pkcs11;


import com.atexo.serveurCryptographique.utilitaire.KeyPair;
import com.atexo.serveurCryptographique.utilitaire.*;
import com.atexo.serveurCryptographique.utilitaire.logging.LogManager;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.smartCard.SmartCardUtil;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import com.atexo.serveurSignature.CertificatUtil;
import com.sun.security.auth.callback.DialogCallbackHandler;
import sun.security.pkcs11.SunPKCS11;

import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Classe permettant de gérer les certificats se trouvant directement
 * dans une smartcard et nécessitant d'utiliser des librairies du fournisseur de la
 * smartcard qui implémentent la norme pkcs11.
 */
public class Pkcs11Handler extends AbstractKeyStoreHandler {
	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";

	private static Pkcs11Handler pkcs11Handler;

	private MultiKeyStore keyStore;
	private static Pkcs11LibsType pkcs11Libs = null;

	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11Libs = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Pkcs11Handler getInstance() {
		if (pkcs11Handler == null) {
			pkcs11Handler = new Pkcs11Handler();
		}
		return pkcs11Handler;
	}

	public void addProviders() throws RecuperationCertificatException {
		TypeOs typeOs = Util.determinerOs();
		try {
			// dans le cas de mac os ou linux, on charge en premier les
			// certificats se trouvant sur clé
			// usb
			if (pkcs11Libs != null) {
				if (typeOs != TypeOs.Windows) {
					Map<String, List<File>> providers = SmartCardUtil.getPkcs11Providers(pkcs11Libs, typeOs);
					if (providers != null && providers.size() > 0) {
						Map<String, KeyStore> keyStores = new TreeMap<String, KeyStore>();
						CallbackHandler callbackHandler = new DialogCallbackHandler();
						for (Map.Entry<String, List<File>> entry : providers.entrySet()) {
							String name = entry.getKey();

							for (File library : entry.getValue()) {
								LogManager.getInstance().afficherMessageInfo("Recherche de la librairie : " + library.getAbsolutePath(), this.getClass());
								if (library.exists()) {
									AuthProvider provider;
									try {
										provider = new SunPKCS11(new ByteArrayInputStream(("name=" + name + "\n" + "library=" + library + "\n" + "attributes=compatibility").getBytes()));
									} catch (ProviderException e) {
										continue;
									}
									provider.setCallbackHandler(callbackHandler);
									AuthProvider ancienProvider = (AuthProvider) Security.getProvider(provider.getName());
									if (ancienProvider != null) {
										LogManager.getInstance().afficherMessageInfo("Suppression du provider : " + ancienProvider.getName(), this.getClass());
										try {
											ancienProvider.logout();
										} catch (LoginException e) {
											e.printStackTrace();
										}
										Security.removeProvider(ancienProvider.getName());
									}
									int numero = Security.addProvider(provider);
									LogManager.getInstance().afficherMessageInfo("Ajout du provider : " + provider.getName() + " en position : " + numero, this.getClass());
									try {
										KeyStore keyStore = KeyStore.getInstance(TypeProvider.PKCS11.getType(), provider);
										keyStores.put(name, keyStore);
									} catch (KeyStoreException e) {
										e.printStackTrace();
										continue;
									}
									break;
								}
							}
						}
					} else {
						LogManager.getInstance().afficherMessageWarning(
								"Aucune librairie pkcs11 n'a été trouvé au niveau de l'installation du poste client => pas de dialogue avec de potentiel smartcard", this.getClass());
					}
				}

			} else {
				LogManager.getInstance().afficherMessageWarning("Aucune librairie téléchargeable vu que l'url de référence est injoignable ou incorrect", this.getClass());
			}

		} catch (Exception e) {
			LogManager.getInstance().afficherMessageErreur("Affichage magasin certificat", e.getCause(), MagasinCertificateUiService.class);
		}
	}

	/**
	 * Retourne la liste des certificats du Magasin de Windows / Mac Os
	 *
	 * @param providers l'ensemble des providers pkcs11 trouvés
	 * @param hashCodes liste des hashs de certificat déjà intégré
	 * @return la liste des certifcats personnels de l'utilisateur.
	 * @throws RecuperationCertificatException
	 */
	public List<CertificatItem> recupererCertificats(Map<String, List<File>> providers, Set<String> hashCodes, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificats) throws RecuperationCertificatException {

		// chargement depuis le KeyStore
		keyStore = getKeyStore(providers);

		return this.recupererCertificatsPkcs11(TypeProvider.PKCS11, keyStore, hashCodes, true, filtrerSignatureModeRGS, typeCertificats);
	}

	public MultiKeyStore getKeyStore(Map<String, List<File>> providers) throws RecuperationCertificatException {

		Map<String, KeyStore> keyStores = new TreeMap<String, KeyStore>();
		CallbackHandler callbackHandler = new DialogCallbackHandler();
		for (Map.Entry<String, List<File>> entry : providers.entrySet()) {
			String name = entry.getKey();

			for (File library : entry.getValue()) {
				LogManager.getInstance().afficherMessageInfo("Recherche de la librairie : " + library.getAbsolutePath(), this.getClass());
				if (library.exists()) {
					AuthProvider provider;
					try {
						provider = new SunPKCS11(new ByteArrayInputStream(("name=" + name + "\n" + "library=" + library + "\n" + "attributes=compatibility").getBytes()));
					} catch (ProviderException e) {
						continue;
					}
					provider.setCallbackHandler(callbackHandler);
					AuthProvider ancienProvider = (AuthProvider) Security.getProvider(provider.getName());
					if (ancienProvider != null) {
						LogManager.getInstance().afficherMessageInfo("Suppression du provider : " + ancienProvider.getName(), this.getClass());
						try {
							ancienProvider.logout();
						} catch (LoginException e) {
							e.printStackTrace();
						}
						Security.removeProvider(ancienProvider.getName());
					}
					int numero = Security.addProvider(provider);
					LogManager.getInstance().afficherMessageInfo("Ajout du provider : " + provider.getName() + " en position : " + numero, this.getClass());
					try {
						KeyStore keyStore = KeyStore.getInstance(TypeProvider.PKCS11.getType(), provider);
						keyStores.put(name, keyStore);
					} catch (KeyStoreException e) {
						e.printStackTrace();
						continue;
					}
					break;
				}
			}
		}

		MultiKeyStore keyStore = new MultiKeyStore(keyStores);

		try {
			keyStore.load(null, null);
			return keyStore;
		} catch (NoSuchAlgorithmException e) {
			throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + TypeProvider.PKCS11, e);
		} catch (CertificateException e) {
			throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + TypeProvider.PKCS11, e);
		} catch (IOException e) {
			if (e.getCause() instanceof FailedLoginException) {
				throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider  " + TypeProvider.PKCS11 + " car le pin est incorrect", e);
			} else {
				throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider" + TypeProvider.PKCS11, e);
			}

		}
	}

	/**
	 * Retour le keyPair à partir du provider et de l'alias.
	 *
	 * @param alias l'alias à recherche depuis le Keystore
	 * @return le keyPair
	 * @throws RecuperationCertificatException
	 */
	public KeyPair getKeyPair(String alias) throws RecuperationCertificatException {

		try {
			// parcourt de l'ensemble des occurances contenus dans le KeyStore et récupération du bit-clé sélectionné
			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String aliasKey = aliases.nextElement();
				//System.out.println("aliasKey => " + aliasKey);
				if (aliasKey.equals(alias)) {
					X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
					// dans le cas d'apple il est nécessaire pour accéder à la clef privée de mettre un mot de passe bidon
					PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, " ".toCharArray());
					String providerName = keyStore.getProviderName(aliasKey);
					KeyPair keyPair = new KeyPair(certificat, privateKey, TypeProvider.PKCS11, providerName);
					return keyPair;
				}
			}

		} catch (KeyStoreException e) {
			throw new RecuperationCertificatException("Erreur lors de la récupération des alias se trouvant dans le key store du provider " + TypeProvider.PKCS11, e);
		} catch (UnrecoverableKeyException e) {
			throw new RecuperationCertificatException("Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + TypeProvider.PKCS11 + " pour l'alias " + alias, e);
		} catch (NoSuchAlgorithmException e) {
			throw new RecuperationCertificatException("Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + TypeProvider.PKCS11 + " pour l'alias " + alias, e);
		}

		return null;
	}

	protected List<CertificatItem> recupererCertificatsPkcs11(TypeProvider typeProvider, KeyStore keyStore, Set<String> hashCodes, boolean smartCard, boolean filtrerSignatureModeRGS,
														CertificatUtil.TypeCertificat... typeCertificats) throws RecuperationCertificatException {

		if (hashCodes == null) {
			hashCodes = new HashSet<String>();
		}
		List<CertificatItem> listCertifItem = new ArrayList<>();

		try {

			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String aliasKey = aliases.nextElement();
				boolean estClef = keyStore.isKeyEntry(aliasKey);
				if (estClef) {
					X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);

					boolean signatureElectroniqueActif = true;
					boolean chiffrementActif = true;
					boolean authentificationActif = true;

					if (!filtrerSignatureModeRGS && typeCertificats != null && typeCertificats.length > 0) {

						signatureElectroniqueActif = false;
						chiffrementActif = false;
						authentificationActif = false;

						for (CertificatUtil.TypeCertificat typeCertificat : typeCertificats) {
							switch (typeCertificat) {

								case SignatureElectronique:
									signatureElectroniqueActif = CertificatUtil.isUtilisablePourSignatureElectronique(certificat);
									break;
								case Chiffrement:
									chiffrementActif = CertificatUtil.isUtilisablePourChiffrement(certificat);
									break;
								case Authentification:
									authentificationActif = CertificatUtil.isUtilisablePourAuthentification(certificat);
									break;
							}
						}
					}

					boolean filtrerRGS = filtrerSignatureModeRGS && !CertificatUtil.isUtilisablePourChiffrement(certificat)
							&& CertificatUtil.isUtilisablePourSignatureElectronique(certificat) && CertificatUtil.isValideDate(certificat) && CertificatUtil.isConformite(certificat);
					if (filtrerRGS || (!filtrerSignatureModeRGS && (signatureElectroniqueActif || chiffrementActif || authentificationActif))) {

						// on rajoute a la liste uniquement s'il y a une utilité
						// au certificat listé
						String sujetCn = CertificatUtil.getCN(certificat.getSubjectDN());
						String issuerCn = CertificatUtil.getCN(certificat.getIssuerDN());
						String dateValidation = Util.formaterDate(certificat.getNotAfter());
						String usage = CertificatUtil.getUtilisablePour(certificat, true);
						String hashCode = String.valueOf(certificat.hashCode());

						// on vérifie de ne pas ajouter n fois le même
						// certificat
						if (!hashCodes.contains(hashCode)) {
							CertificatItem item = new CertificatItem(aliasKey, sujetCn, issuerCn, dateValidation, certificat.getNotAfter(), usage, smartCard, hashCode, TypeProvider.PKCS11);
							if (CertificatUtil.isConformite(certificat)) {
								item.setConformite("Adapt\u00E9e");
							} else {
								item.setConformite("Tol\u00E9r\u00E9e jusqu'en septembre 2017");
							}
							listCertifItem.add(item);
							hashCodes.add(hashCode);
						}
					}
				}

			}
		} catch (KeyStoreException e) {
			throw new RecuperationCertificatException("Erreur lors de la récupération  des certificats se trouvant dans le key store du provider " + typeProvider, e);
		}

		return listCertifItem;
	}

}
