package com.atexo.serveurCryptographique.utilitaire.pkcs11;

import javax.xml.bind.JAXBElement;

import com.atexo.serveurCryptographique.utilitaire.FileUtil;
import com.atexo.serveurCryptographique.utilitaire.JAXBService;
import com.atexo.serveurCryptographique.utilitaire.TransfertExecutionException;

/**
 *
 */
public class JaxbPkcs11Util {

    private final static ObjectFactory factory = new ObjectFactory();

    public static Pkcs11LibsType getPkcs11Libs(String pkcs11LibsXML) {
        return getPkcs11Libs(pkcs11LibsXML, FileUtil.ENCODING_UTF_8);
    }

    public static Pkcs11LibsType getPkcs11Libs(String pkcs11LibsXML, String encoding) {
        String context = Pkcs11LibsType.class.getPackage().getName();
        Object resultatObject = JAXBService.instance().getAsObject(pkcs11LibsXML, context, encoding);
        return ((JAXBElement<Pkcs11LibsType>) resultatObject).getValue();
    }

    public static Pkcs11LibsType getPkcs11LibsType(byte[] contenuFichierXML) throws TransfertExecutionException {
        return JaxbPkcs11Util.getPkcs11Libs(new String(contenuFichierXML));
    }
}
