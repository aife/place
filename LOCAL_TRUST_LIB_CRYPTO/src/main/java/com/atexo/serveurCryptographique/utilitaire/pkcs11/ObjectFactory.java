package com.atexo.serveurCryptographique.utilitaire.pkcs11;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the fr.atexo.signature.xml.pkcs11 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Pkcs11Libs_QNAME = new QName("", "pkcs11Libs");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.atexo.signature.xml.pkcs11
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Pkcs11LibType }
     *
     */
    public Pkcs11LibType createPkcs11LibType() {
        return new Pkcs11LibType();
    }

    /**
     * Create an instance of {@link LibType }
     *
     */
    public LibType createLibType() {
        return new LibType();
    }

    /**
     * Create an instance of {@link LibsType }
     *
     */
    public LibsType createLibsType() {
        return new LibsType();
    }

    /**
     * Create an instance of {@link Pkcs11LibsType }
     *
     */
    public Pkcs11LibsType createPkcs11LibsType() {
        return new Pkcs11LibsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pkcs11LibsType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "pkcs11Libs")
    public JAXBElement<Pkcs11LibsType> createPkcs11Libs(Pkcs11LibsType value) {
        return new JAXBElement<Pkcs11LibsType>(_Pkcs11Libs_QNAME, Pkcs11LibsType.class, null, value);
    }

}
