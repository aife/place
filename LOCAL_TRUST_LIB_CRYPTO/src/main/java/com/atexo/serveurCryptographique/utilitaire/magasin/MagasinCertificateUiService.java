package com.atexo.serveurCryptographique.utilitaire.magasin;

import com.atexo.serveurCryptographique.utilitaire.CertificatItem;
import com.atexo.serveurCryptographique.utilitaire.MainFrame;
import com.atexo.serveurCryptographique.utilitaire.TypeOs;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.logging.LogManager;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.smartCard.SmartCardUtil;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import com.atexo.serveurSignature.CertificatUtil;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * Singleton permettant d'initialiser l'interfage graphique de l'utilisateur
 * ainsi que d'associer le listener qui sera lancé lors de la sélection du
 * certificat depuis le magasin de certificats par l'utilisateur.
 */
public class MagasinCertificateUiService implements CertificateSelectUiService<MagasinCertificateListener> {

	private static MagasinCertificateUiService uiService;

	private static Pkcs11LibsType pkcs11Libs;
	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";


	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11Libs = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static MagasinCertificateUiService getInstance() {
		if (uiService == null) {
			uiService = new MagasinCertificateUiService();
		}
		return uiService;
	}

	public List<CertificatItem> getCertificat(TypeProvider typeProvider, TypeOs typeOs, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificat) {
		List<CertificatItem> certificatItems = new ArrayList<>();
		try {
			Set<String> hashCodes = new HashSet<>();
			// dans le cas de mac os ou linux, on charge en premier les
			// certificats se trouvant sur clé
			// usb
			if (pkcs11Libs != null) {
				if (typeOs != TypeOs.Windows) {
					Map<String, List<File>> providers = SmartCardUtil.getPkcs11Providers(pkcs11Libs, typeOs);
					if (providers != null && providers.size() > 0) {
						List<CertificatItem> certificatsPkcs11 = Pkcs11Handler.getInstance().recupererCertificats(providers, hashCodes, filtrerSignatureModeRGS, typeCertificat);
						certificatItems.addAll(certificatsPkcs11);
					} else {
						LogManager.getInstance().afficherMessageWarning(
								"Aucune librairie pkcs11 n'a été trouvé au niveau de l'installation du poste client => pas de dialogue avec de potentiel smartcard", this.getClass());
					}
				}

			} else {
				LogManager.getInstance().afficherMessageWarning("Aucune librairie téléchargeable vu que l'url de référence est injoignable ou incorrect", this.getClass());
			}

			if (typeOs == TypeOs.Windows || typeOs == TypeOs.MacOs) {
				List<CertificatItem> certificatsMagasin = MagasinHandler.getInstance().recupererCertificats(typeProvider, hashCodes, filtrerSignatureModeRGS, typeCertificat);
				certificatItems.addAll(certificatsMagasin);
			}
		} catch (Exception e) {
			LogManager.getInstance().afficherMessageErreur("Affichage magasin certificat", e.getCause(), MagasinCertificateUiService.class);
		}
		return certificatItems;
	}

	@Override
	public void initUi(MagasinCertificateListener listener, TypeOs typeOs, TypeProvider typeProvider, boolean filtrerSignatureModeRGS, boolean verificationCertificatPerime,
					   CertificatUtil.TypeCertificat... typeCertificat) {
		List<CertificatItem> certificatsItems = getCertificat(typeProvider, typeOs, filtrerSignatureModeRGS, typeCertificat);
		if (certificatsItems == null) {
			certificatsItems = new ArrayList<CertificatItem>();
		}
		MainFrame mainFrame = new MainFrame(certificatsItems, verificationCertificatPerime);
		mainFrame.addListener(listener);
	}


}
