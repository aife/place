package com.atexo.serveurCryptographique.utilitaire;

import java.util.concurrent.ExecutionException;

/**
 *  Exception pour signaler un souci lors du transfert d'informations
 */
@SuppressWarnings("serial")
public class TransfertExecutionException extends ExecutionException {

    public TransfertExecutionException(String message) {
        super(message);
    }

    public TransfertExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
