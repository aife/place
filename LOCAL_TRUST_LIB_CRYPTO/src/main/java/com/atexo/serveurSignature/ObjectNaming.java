package com.atexo.serveurSignature;

import java.io.Serializable;

public class ObjectNaming implements Serializable {
    private static final long serialVersionUID = -3053470707341285977L;
    private String alternativeName;
    private String cn;
    private String t;
    private String e;
    private String ou;
    private String o;
    private String c;

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou;
    }

    public String getO() {
        return o;
    }

    public void setO(String o) {
        this.o = o;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    public String getT() {
        return t;
    }
    
    public void setT(String t) {
        this.t = t;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((alternativeName == null) ? 0 : alternativeName.hashCode());
        result = prime * result + ((c == null) ? 0 : c.hashCode());
        result = prime * result + ((cn == null) ? 0 : cn.hashCode());
        result = prime * result + ((e == null) ? 0 : e.hashCode());
        result = prime * result + ((o == null) ? 0 : o.hashCode());
        result = prime * result + ((ou == null) ? 0 : ou.hashCode());
        result = prime * result + ((t == null) ? 0 : t.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObjectNaming other = (ObjectNaming) obj;
        if (alternativeName == null) {
            if (other.alternativeName != null)
                return false;
        } else if (!alternativeName.equals(other.alternativeName))
            return false;
        if (c == null) {
            if (other.c != null)
                return false;
        } else if (!c.equals(other.c))
            return false;
        if (cn == null) {
            if (other.cn != null)
                return false;
        } else if (!cn.equals(other.cn))
            return false;
        if (e == null) {
            if (other.e != null)
                return false;
        } else if (!e.equals(other.e))
            return false;
        if (o == null) {
            if (other.o != null)
                return false;
        } else if (!o.equals(other.o))
            return false;
        if (ou == null) {
            if (other.ou != null)
                return false;
        } else if (!ou.equals(other.ou))
            return false;
        if (t == null) {
            if (other.t != null)
                return false;
        } else if (!t.equals(other.t))
            return false;
        return true;
    }
}
