package com.atexo.serveurSignature;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.security.auth.x500.X500Principal;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atexo.serveurCryptographique.utilitaire.BouncyCaslteHandler;
import com.atexo.serveurCryptographique.utilitaire.I18nUtil;
import com.atexo.serveurCryptographique.utilitaire.KeyPair;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.Util;

import net.iharder.Base64;
import sun.security.x509.AlgorithmId;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateIssuerName;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateSubjectName;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;

/**
 * Classe d'utilitaire d'extraction / manipulation des informations d'un
 * certificat.
 */
@SuppressWarnings("restriction")
public abstract class CertificatUtil {
    private static final Logger logger = LoggerFactory.getLogger(CertificatUtil.class);
    public static final String CHAINE_CERTIFICAT_BASE64 = "MIIFYjCCA0qgAwIBAgIBATANBgkqhkiG9w0BAQsFADBrMQswCQYDVQQGEwJGUjEOMAwGA1UEBxMFUEFSSVMxDjAMBgNVBAoTBUFURVhPMRYwFAYDVQQLEw1BVEVYTyBBQyBERU1PMSQwIgYDVQQDExtBVEVYTyBBQyBERU1PIC0gRW50cmVwcmlzZXMwHhcNMTMwNDE3MTkxNzU3WhcNMTYwNDE3MTkxNzU3WjCBlDELMAkGA1UEBhMCRlIxDjAMBgNVBAoTBUFURVhPMSYwJAYDVQQLEx1NQVJDSEVTIFBVQkxJQ1MgRUxFQ1RST05JUVVFUzEXMBUGA1UEAxMOQ2hhcmxlcyBDSVZFVEUxFzAVBgNVBCkTDkNoYXJsZXMgQ0lWRVRFMRswGQYJKoZIhvcNAQkBFgxjY0BhdGV4by5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDovtQHb+g22rU1S+BIdnfIdDF+XZgV7RdRMPzBjTFqQFxWVCa9lZx/OWLqBnUgB9J/9CzYPW5bpG55UUlkCbe66iFGIdLCMKIk4WOlWmxz4Zq0Pphr/Np4rS0UKLFh170r01ezmlRvlFesmYDI/XIVnjo8v+VGdoyiSzgqo2WgtonklTGkfePpUGllMMAp3v0rLJOe55bWFTnXSl85ndzZyIr4XL6VA1y6ev/LCxqYPzZKFMgbRZB13Nb6CDRF8hJWa0hEA2w3t6xvLYZ+4oKu8A5+WXEIo35baWaAPgxH9H5GbOmWnCxzs7+0luV5GdejEWgV+xpL2+rMbMzngjzjAgMBAAGjgeYwgeMwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBsAwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMB0GA1UdDgQWBBSD4uxZAuEc8eAIhQEWcwZNdujmQjAfBgNVHSMEGDAWgBSNms8QxETmT9eP+6SW/mqMyrFsZTBOBgNVHR8ERzBFMEOgQaA/hj1odHRwczovL3BraS5sb2NhbC10cnVzdC5jb20vY3JsL0FURVhPX0FDX0RFTU8tRW50cmVwcmlzZXMuY3JsMBcGA1UdEQQQMA6BDGNjQGF0ZXhvLmNvbTANBgkqhkiG9w0BAQsFAAOCAgEAj8KSlFQ37eDMNxfTmyjG+845i3ucT4dua6HvW3jJWHCwzqCB6dV/0zMzGN5CabE8qnBp1Sv8E3N+JH0VzCG1uPHtfQ1un3ydGYf7c/+hUmLGq0GRc/dDIqjS8VocHQ71GM60YrS0mp0brXJMbUr4TxU0elV760Uvr0l2a3Mu8yZ8x/NwSEoLTPZiTWCwwuA2l2Qk2e4XeGKpyRsYLZmXkYdSGveag/9Y0/gAfAoApLozYhbCaSIZuyPKLjHQa1LfuGL881xxsNlVx4d6JckM2k1Krzs2PaShAMNwONJgUA1aJdGIoDmjYuO9oAiAL1bvZEj3YHXUvHFiTlKP/87kCinS6EHulWq7mEZdMzqcSJUJGpR09Tr51NdwnQI97Pa+ASoPdGeFOjENyFzYLD9XbOqy523/Ok0tlVoBhjIjaokg2Ov60QYMShO8ynDukX0h3iqx3MYu6RpTGaIJBZw75oLHshrrP4Ojzt5bNjRpPw/GKrAbpxO6IpjgiJXVRdtc6hfqOZxAgWGJ5wL6LgAk+fGvC8X5PmsEDhtGuCYxj3FjxL+OG7pSS6/kz2678asbcC8U+qwfAqdJWtdj1s7eBHbgw6Qn9wpX4UPJS8t85DBVvU4z+p7dTToHbghqic/zm7z7ymWshFqlLeTb1jFtjTRXat2TRfhfGANBN78H8pI=";
    private static final String CHAINE_CLEF_PRIVEE_BASE64 = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDovtQHb+g22rU1S+BIdnfIdDF+XZgV7RdRMPzBjTFqQFxWVCa9lZx/OWLqBnUgB9J/9CzYPW5bpG55UUlkCbe66iFGIdLCMKIk4WOlWmxz4Zq0Pphr/Np4rS0UKLFh170r01ezmlRvlFesmYDI/XIVnjo8v+VGdoyiSzgqo2WgtonklTGkfePpUGllMMAp3v0rLJOe55bWFTnXSl85ndzZyIr4XL6VA1y6ev/LCxqYPzZKFMgbRZB13Nb6CDRF8hJWa0hEA2w3t6xvLYZ+4oKu8A5+WXEIo35baWaAPgxH9H5GbOmWnCxzs7+0luV5GdejEWgV+xpL2+rMbMzngjzjAgMBAAECggEAcX91eDb7P5zB1z6sHcofuZHn/N55Zt/aig5gg6Zt3YmLPdIFnlgSG/yJHuSNQ1RtM1aIc97pLSlvchvQtUcD4NOB7GhcFbSPrXp4FE+XKZ9vyMvpmmeQxl506Cq77aG+L1v9naj52fu2EYY9xkXJ1370mWFe1lDDXfVea3SI/6h2Rcrhd63eoF7Wz7xYTPNdWmRGFXW2kt/FTWteETlXihVYHEVKyiOqcrklVVrv2n4gqXwkHLMXUlpCP5/Cwb7CRv3L0sFTUuG5Hk+kPgA/XHtPLRjY0VVsbyDmDOB/hiilYGq+epAbMZJqISAKunTDGNEScK2NEM7cEu6rHbk0eQKBgQD7HMzHcYTwQ2zuXHaOopPxbJ60xZs0u7/f9hXiL2MYnqa06hmW+WVj+cuY0oPdimIiyaGOJPpIvd877wHSeDZYcHCTZRYlYvDi/Y56Y6uVY3ovkENxaHo1cCCnyeC7uY7cudoe4eIThPfOrtpVxKAgmtyzhVONVECwxPmG/LZTPwKBgQDtRoMTb3qzFVb0gVAEY2bfAD/UqXJaAf7Wvf6bhbRZI2w3yLS7VGb149A8YFCps7Ez5ftU/3z70WL0ZgNwcT4gA+OZMODwtAGS/2/YrN6H4QXPWtVErj/X9kFsEoCOfo0kMi026gXFtWAzzLXLOaqKhbu9g7fON0fRFpuWoB1BXQKBgQDkSKNVsTq0nedaro5NTzmFokSJfJeDkid1+Cae0IubJyfQkn2fBa0J/V382Wxtq89ZZspLCgzKsmpN9xNF/mkRb264YI6IgHETOoUbeJ7VGXFL6i+V2vA2wm+9ecangqKAwSitKJa5Pgl2SPlOPYmA+qgnP87cTbrduMRL1lP+SwKBgFZmLqSNG2jKhHdT/wcaCwN428VcTj9oKpuXY3bOIPW0aFuHwgfUnAk9gz349GZXAFedjv9MaP5pKFdGLkprcevGfsHxgncjlM8qJFgJ2p4v2iW/NZlkueqxyPDJa/Z3Ln3Xp48veBqeCQHTsP7naKo4ODyyEeQNTSFd0hnvA7ghAoGBAIRyhMMXLLuqjvPcDnXsN0BsCb+2NJ/ILcY19kVhYe+R8U8zYr+PVI33reqTSGdlMHKhzhZZKK99hLwhpLpZssJ+FoJOkTktzS/GMQeaGI6Dj4hGtKs2qI37cWjjduMTlZgP2zgWNaT0FJIngUxiaL1ssrCTjlOG+tvIzkk3DmWC";

    public static String[] KEY_USAGE_FLAG_NAMES = new String[] { "digitalSignature", "nonRepudiation", "keyEncipherment", "dataEncipherment", "keyAgreement", "keyCertSign", "cRLSign", "encipherOnly", "decipherOnly" };

    public enum TypeCertificat {
        SignatureElectronique("SIGNATURE_NUMERIQUE"), Chiffrement("CHIFFREMENT"), Authentification("AUTHENTIFICATION");

        private TypeCertificat(String labelKey) {
            this.labelKey = labelKey;
        }

        private String labelKey;

        public String getLabelKey() {
            return labelKey;
        }
    }

    private static final BouncyCastleProvider PROVIDER = new BouncyCastleProvider();

    /**
     * Méthode qui génére un certicat ( pour les test )
     * 
     * @param dn
     * @param pair
     * @param days
     * @param algorithm
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public static X509Certificate generateCertificate(String dn, java.security.KeyPair pair, int days, String algorithm) throws GeneralSecurityException, IOException {
        PrivateKey privkey = pair.getPrivate();
        X509CertInfo info = new X509CertInfo();
        Date from = new Date();
        Date to = new Date(from.getTime() + days * 86400000l);
        CertificateValidity interval = new CertificateValidity(from, to);
        BigInteger sn = new BigInteger(64, new SecureRandom());
        X500Name owner = new X500Name(dn);

        info.set(X509CertInfo.VALIDITY, interval);
        info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(sn));
        info.set(X509CertInfo.SUBJECT, new CertificateSubjectName(owner));
        info.set(X509CertInfo.ISSUER, new CertificateIssuerName(owner));
        info.set(X509CertInfo.KEY, new CertificateX509Key(pair.getPublic()));
        info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
        AlgorithmId algo = new AlgorithmId(AlgorithmId.md5WithRSAEncryption_oid);
        info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algo));

        // Sign the cert to identify the algorithm that's used.
        X509CertImpl cert = new X509CertImpl(info);
        cert.sign(privkey, algorithm);

        // Update the algorith, and resign.
        algo = (AlgorithmId) cert.get(X509CertImpl.SIG_ALG);
        info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, algo);
        cert = new X509CertImpl(info);
        cert.sign(privkey, algorithm);
        return cert;
    }

    public static X509Certificate getFromP12(String p12File, String p12Pin) {
        X509Certificate certificate = null;

        KeyStore keystore;
        try {
            keystore = KeyStore.getInstance("PKCS12", PROVIDER);
            ClassLoader classLoader = CertificatUtil.class.getClassLoader();
            keystore.load(classLoader.getResourceAsStream(p12File), p12Pin.toCharArray());
            Enumeration<String> aliases;
            aliases = keystore.aliases();
            String alias = null;

            if (aliases.hasMoreElements()) {
                alias = aliases.nextElement();
            }

            certificate = (X509Certificate) keystore.getCertificate(alias);
        } catch (NoSuchAlgorithmException e) {
            logger.error("", e.fillInStackTrace());
        } catch (CertificateException e) {
            logger.error("", e.fillInStackTrace());
        } catch (IOException e) {
            logger.error("", e.fillInStackTrace());
        } catch (KeyStoreException e) {
            logger.error("", e.fillInStackTrace());
        }
        return certificate;
    }

    public static PrivateKey getPrivateKeyFromP12(String p12File, String p12Pin) {
        ClassLoader classLoader = CertificatUtil.class.getClassLoader();
        PrivateKey privateKey = null;
        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12", PROVIDER);
            keystore.load(classLoader.getResourceAsStream(p12File), p12Pin.toCharArray());
            Enumeration<String> aliases = keystore.aliases();
            String alias = null;
            if (aliases.hasMoreElements()) {
                alias = aliases.nextElement();
            }
            privateKey = (PrivateKey) keystore.getKey(alias, p12Pin.toCharArray());
        } catch (NoSuchAlgorithmException e) {
            logger.error("", e.fillInStackTrace());
        } catch (KeyStoreException e) {
            logger.error("", e.fillInStackTrace());
        } catch (UnrecoverableKeyException e) {
            logger.error("", e.fillInStackTrace());
        } catch (CertificateException e) {
            logger.error("", e.fillInStackTrace());
        } catch (IOException e) {
            logger.error("", e.fillInStackTrace());
        }
        return privateKey;
    }

    /**
     * Récupére l'attribut CN d'un certificat
     * 
     * @param principal
     *            le certificat
     * @return la valeur du CN
     */
    public static String getCN(Principal principal) {

        if (principal != null) {
            String[] principalDecompose = principal.toString().split(",");

            for (String valeur : principalDecompose) {
                String valeurTrim = valeur.trim();
                if (valeurTrim.startsWith("CN=")) {
                    return valeurTrim.substring(3);
                }
            }
        }

        return null;
    }

    /**
     * Cette fonction permet de récupérer un certificat depuis une chaine de
     * caractére encode en base 64.
     * 
     * @param chaineDeCertificatEnBase64
     *            la chaine correspondant au certificat encodé en base 64.
     * @return X509Certificate le certificat X509.
     */
    public static X509Certificate getX509Certificate(String chaineDeCertificatEnBase64) throws IOException, CertificateException {

        if (chaineDeCertificatEnBase64 == null) {
            return null;
        }
        X509Certificate certificate = null;
        InputStream inputStream = null;
        CertificateFactory certificateFactory = null;

        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("-----BEGIN CERTIFICATE-----", "");
        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("-----END CERTIFICATE-----", "");
        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("\n", "");
        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("\r", "");

        byte[] chaineDeCertificatDecode = Base64.decode(chaineDeCertificatEnBase64);
        inputStream = new ByteArrayInputStream(chaineDeCertificatDecode);
        certificateFactory = CertificateFactory.getInstance("X.509");
        certificate = (X509Certificate) certificateFactory.generateCertificate(inputStream);
        inputStream.close();

        return certificate;
    }

    public static String reconstuireChaineDeCertificat(X509Certificate certificat) throws CertificateEncodingException {

        String issuer = getCN(certificat.getIssuerX500Principal()).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String date = Util.formaterDate(certificat.getNotAfter(), Util.DATE_TIME_PATTERN_TIRET).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String subject = getCN(certificat.getSubjectX500Principal()).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String certificatEnBase64 = new String(Base64.encodeBytes(certificat.getEncoded()));
        String chaineCertificat = issuer + "_" + date + "_" + subject + "|" + certificatEnBase64 + "##";

        return chaineCertificat;
    }

    public static boolean isValideDate(X509Certificate x509Certificate) {
        return x509Certificate.getNotAfter().after(new Date());
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de l'authentification.
     * on se base sur les flags digitalSignature ou nonRepudiation
     * 
     * @param x509Certificate
     *            le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de
     *         l'authentification, sinon false.
     */
    public static boolean isUtilisablePourAuthentification(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[0] || x509Certificate.getKeyUsage()[1]);
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de la signature
     * électronique.
     * 
     * on se base sur les flags digitalSignature ou nonRepudiation
     * 
     * @param x509Certificate
     *            le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de la signature
     *         électronique, sinon false.
     */
    public static boolean isUtilisablePourSignatureElectronique(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[0] || x509Certificate.getKeyUsage()[1]);
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de le chiffrement on
     * se base sur les flags keyEncipherment ou dataEncipherment
     * 
     * @param x509Certificate
     *            le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de le chiffrement,
     *         sinon false.
     */
    public static boolean isUtilisablePourChiffrement(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[2] || x509Certificate.getKeyUsage()[3]);

    }

    public static boolean isConformite(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && x509Certificate.getKeyUsage()[1]  && !x509Certificate.getKeyUsage()[2]
            && !x509Certificate.getKeyUsage()[3] && !x509Certificate.getKeyUsage()[4] && !x509Certificate.getKeyUsage()[5]
            && !x509Certificate.getKeyUsage()[6] && !x509Certificate.getKeyUsage()[7] && !x509Certificate.getKeyUsage()[8];

    }

    /**
     * @param x509Certificate
     * @return
     */
    public static String getUtilisablePour(X509Certificate x509Certificate, boolean traduction) {
        // on détermine à quel usage peut être utilisé le certificat
        StringBuilder utilisationPossible = new StringBuilder();
        if (x509Certificate.getKeyUsage() != null) {
            boolean[] keyUsage = x509Certificate.getKeyUsage();
            for (int i = 0; i < keyUsage.length; i++) {
                if (keyUsage[i]) {
                    if (utilisationPossible.length() != 0) {
                        utilisationPossible.append(", ");
                    }
                    if (traduction) {
                        utilisationPossible.append(I18nUtil.get("keyUsage_" + KEY_USAGE_FLAG_NAMES[i]));
                    } else {
                        utilisationPossible.append(KEY_USAGE_FLAG_NAMES[i]);
                    }
                }
            }
        }
        return utilisationPossible.toString();
    }

    public static KeyPair getKeyPairTestChiffrement() throws IOException, CertificateException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {

        BouncyCaslteHandler.verifierPresenceEtRecupererProvider();

        X509Certificate certificat = getX509Certificate(CHAINE_CERTIFICAT_BASE64);
        byte[] clefPriveeDecode = Base64.decode(CHAINE_CLEF_PRIVEE_BASE64);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA", TypeProvider.BC.getType());
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clefPriveeDecode);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        KeyPair keyPair = new KeyPair(certificat, privateKey, TypeProvider.BC);

        return keyPair;
    }

    public static InfosVerificationCertificat extraireInformations(X509Certificate certificat, InfosVerificationCertificat infosComplementairesCertificat) throws CertificateParsingException {

        // propriétaire du certificat
        X500Principal proprietaireCertificat = certificat.getSubjectX500Principal();
        infosComplementairesCertificat.setSignatairePartiel(CertificatUtil.getCN(proprietaireCertificat));
        infosComplementairesCertificat.setSignataireComplet(proprietaireCertificat.toString());
        // issuer du certificat
        X500Principal issuerCertificat = certificat.getIssuerX500Principal();
        infosComplementairesCertificat.setEmetteur(issuerCertificat.toString());
        infosComplementairesCertificat.setSerialNumber(certificat.getSerialNumber().toString());

        // periodicité du certificat
        String dateValiditeDu = Util.creerISO8601DateTime(certificat.getNotBefore());
        infosComplementairesCertificat.setDateValiditeDu(dateValiditeDu);
        String dateValiditeAu = Util.creerISO8601DateTime(certificat.getNotAfter());
        infosComplementairesCertificat.setDateValiditeAu(dateValiditeAu);

        boolean datePeriodiciteValide = Util.isDateCompriseEntre(new Date(), certificat.getNotBefore(), certificat.getNotAfter());
        infosComplementairesCertificat.setPeriodiciteValide(datePeriodiciteValide);
        infosComplementairesCertificat.setSignataireDetails(getObjectNaming(proprietaireCertificat.toString(), certificat.getSubjectAlternativeNames()));
        infosComplementairesCertificat.setEmetteurDetails(getObjectNaming(issuerCertificat.toString(), certificat.getSubjectAlternativeNames()));
        return infosComplementairesCertificat;
    }

    public static ObjectNaming getObjectNaming(String proprietaireCertificat, Collection<List<?>> alternativeName) {
        ObjectNaming object = new ObjectNaming();
        if (alternativeName != null) {
            Iterator<List<?>> it = alternativeName.iterator();
            if (it != null) {
                List<?> result = it.next();
                if (result != null) {
                    if (result.get(1) instanceof String) {
                        object.setAlternativeName((String) result.get(1));
                    }
                }
            }
        }
        try {
            LdapName names = new LdapName(proprietaireCertificat);
            for (Rdn tmp : names.getRdns()) {
                String cle = tmp.getType();
                String valeur = tmp.getValue().toString();
                if (cle.equalsIgnoreCase("CN")) {
                    if (object.getCn() != null) {
                        object.setCn(object.getCn() + ", " + valeur);
                    } else {
                        object.setCn(valeur);
                    }
                } else if (cle.equalsIgnoreCase("T")) {
                    if (object.getT() != null) {
                        object.setT(object.getT() + ", " + valeur);
                    } else {
                        object.setT(valeur);
                    }
                } else if (cle.equalsIgnoreCase("OU")) {
                    if (object.getOu() != null) {
                        object.setOu(object.getOu() + ", " + valeur);
                    } else {
                        object.setOu(valeur);
                    }
                } else if (cle.equalsIgnoreCase("E")) {
                    if (object.getE() != null) {
                        object.setE(object.getE() + ", " + valeur);
                    } else {
                        object.setE(valeur);
                    }
                } else if (cle.equalsIgnoreCase("O")) {
                    if (object.getO() != null) {
                        object.setO(object.getO() + ", " + valeur);
                    } else {
                        object.setO(valeur);
                    }
                } else if (cle.equalsIgnoreCase("C")) {
                    if (object.getC() != null) {
                        object.setC(cle.equals("C") + ", " + valeur);
                    } else {
                        object.setC(valeur);
                    }
                }
            }
        } catch (InvalidNameException e) {
            logger.error("Probleme a la lecture du certificat", e.fillInStackTrace());
        }
     
        return object;
    }

}
