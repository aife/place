package com.atexo.serveurSignature;

import java.io.Serializable;

/**
 * Représente un répertoire pour effectuer la reponse json.
 */
public class Repertoire implements Serializable {

    private String nom;
    
    private String statut;

    public Repertoire() {
    }

    public Repertoire(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Repertoire that = (Repertoire) o;

        if (!nom.equals(that.nom)) return false;

        return true;
    }
    
    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Override
    public int hashCode() {
        return nom.hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Repertoire");
        sb.append("{nom='").append(nom).append('\'').append(",");
        sb.append("statut='").append(statut).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

