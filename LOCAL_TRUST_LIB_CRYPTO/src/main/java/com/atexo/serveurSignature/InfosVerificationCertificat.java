package com.atexo.serveurSignature;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 */
public class InfosVerificationCertificat implements Serializable {
    private static final long serialVersionUID = 5443861700601895753L;

    private int etat = -1;

    @JsonIgnore
    private boolean dssValidationEffectuee = false;

    @JsonIgnore
    private boolean atexoValidationEffectuee = false;

    private String signatairePartiel;

    private String signataireComplet;

    private String emetteur;

    private String dateValiditeDu;

    private String dateValiditeAu;

    private Boolean periodiciteValide;

    private int chaineDeCertificationValide;

    private int absenceRevocationCRL;

    private int dateSignatureValide;

    private Boolean signatureValide;

    private ObjectNaming signataireDetails;

    private ObjectNaming emetteurDetails;

    private Set<Repertoire> repertoiresChaineCertification;

    private Set<Repertoire> repertoiresRevocation;

    private String signatureXadesServeurEnBase64;

    private String typeSignature = "INCONNUE";

    @JsonIgnore
    private String reportsDiagnostic;

    @JsonIgnore
    private String reportsDetailed;

    @JsonIgnore
    private String serialNumber;

    public String getSignataireComplet() {
        return signataireComplet;
    }

    public void setSignataireComplet(String signataireComplet) {
        this.signataireComplet = signataireComplet;
    }

    public String getSignatairePartiel() {
        return signatairePartiel;
    }

    public void setSignatairePartiel(String signatairePartiel) {
        this.signatairePartiel = signatairePartiel;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    public String getDateValiditeDu() {
        return dateValiditeDu;
    }

    public void setDateValiditeDu(String dateValiditeDu) {
        this.dateValiditeDu = dateValiditeDu;
    }

    public String getDateValiditeAu() {
        return dateValiditeAu;
    }

    public void setDateValiditeAu(String dateValiditeAu) {
        this.dateValiditeAu = dateValiditeAu;
    }

    public int getDateSignatureValide() {
        return dateSignatureValide;
    }

    public void setDateSignatureValide(int dateSignatureValide) {
        this.dateSignatureValide = dateSignatureValide;
    }

    public Boolean getPeriodiciteValide() {
        return periodiciteValide;
    }

    public void setPeriodiciteValide(Boolean periodiciteValide) {
        this.periodiciteValide = periodiciteValide;
    }

    public int getChaineDeCertificationValide() {
        return chaineDeCertificationValide;
    }

    public void setChaineDeCertificationValide(int chaineDeCertificationValide) {
        this.chaineDeCertificationValide = chaineDeCertificationValide;
    }

    public int getAbsenceRevocationCRL() {
        return absenceRevocationCRL;
    }

    public void setAbsenceRevocationCRL(int absenceRevocationCRL) {
        this.absenceRevocationCRL = absenceRevocationCRL;
    }

    public Boolean getSignatureValide() {
        return signatureValide;
    }

    public void setSignatureValide(Boolean signatureValide) {
        this.signatureValide = signatureValide;
    }

    public Set<Repertoire> getRepertoiresChaineCertification() {
        return repertoiresChaineCertification;
    }

    public void setRepertoiresChaineCertification(Set<Repertoire> repertoiresChaineCertification) {
        this.repertoiresChaineCertification = repertoiresChaineCertification;
    }

    public Set<Repertoire> getRepertoiresRevocation() {
        return repertoiresRevocation;
    }

    public void setRepertoiresRevocation(Set<Repertoire> repertoiresRevocation) {
        this.repertoiresRevocation = repertoiresRevocation;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public ObjectNaming getSignataireDetail() {
        return signataireDetails;
    }

    public ObjectNaming getEmetteurDetails() {
        return emetteurDetails;
    }

    public void setSignataireDetails(ObjectNaming signataire) {
        this.signataireDetails = signataire;
    }

    public void setEmetteurDetails(ObjectNaming emetteur) {
        this.emetteurDetails = emetteur;
    }

    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("InfosVerificationCertificat");
        sb.append("{etat=").append(etat);
        sb.append(", signatairePartiel='").append(signatairePartiel).append('\'');
        sb.append(", signataireComplet='").append(signataireComplet).append('\'');
        sb.append(", emetteur='").append(emetteur).append('\'');
        sb.append(", dateValiditeDu='").append(dateValiditeDu).append('\'');
        sb.append(", dateValiditeAu='").append(dateValiditeAu).append('\'');
        sb.append(", dateSignatureValide='").append(dateSignatureValide);
        sb.append(", periodiciteValide=").append(periodiciteValide);
        sb.append(", chaineDeCertificationValide=").append(chaineDeCertificationValide);
        sb.append(", absenceRevocationCRL=").append(absenceRevocationCRL);
        sb.append(", signatureValide=").append(signatureValide);
        sb.append(", typeSignature=").append(typeSignature);
        sb.append('}');
        return sb.toString();
    }

    public void setSignatureXadesServeurEnBase64(String signatureXadesServeurEnBase64) {
        this.signatureXadesServeurEnBase64 = signatureXadesServeurEnBase64;
    }

    public String getSignatureXadesServeurEnBase64() {
        return signatureXadesServeurEnBase64;
    }

    public void ajouterRepertoireChaineCertification(String nomRepertoire) {
        if (repertoiresChaineCertification == null) {
            repertoiresChaineCertification = new HashSet<Repertoire>();
        }
        repertoiresChaineCertification.add(new Repertoire(nomRepertoire));
    }

    public void ajouterRepertoireRevocation(String nomRepertoire) {
        if (repertoiresRevocation == null) {
            repertoiresRevocation = new HashSet<Repertoire>();
        }
        repertoiresRevocation.add(new Repertoire(nomRepertoire));
    }

    public String getTypeSignature() {
        return typeSignature;
    }

    public void setTypeSignature(String typeSignature) {
        this.typeSignature = typeSignature;
    }

    public boolean isDssValidationEffectuee() {
        return dssValidationEffectuee;
    }

    public void setDssValidationEffectuee(boolean dssValidationEffectuee) {
        this.dssValidationEffectuee = dssValidationEffectuee;
    }

    public boolean isAtexoValidationEffectuee() {
        return atexoValidationEffectuee;
    }

    public void setAtexoValidationEffectuee(boolean atexoValidationEffectuee) {
        this.atexoValidationEffectuee = atexoValidationEffectuee;
    }

    public String getReportsDiagnostic() {
        return reportsDiagnostic;
    }

    public void setReportsDiagnostic(String reportsDiagnostic) {
        this.reportsDiagnostic = reportsDiagnostic;
    }

    public String getReportsDetailed() {
        return reportsDetailed;
    }

    public void setReportsDetailed(String reportsDetailed) {
        this.reportsDetailed = reportsDetailed;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;

    }

    public String getSerialNumber() {
        return serialNumber;

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + absenceRevocationCRL;
        result = prime * result + (atexoValidationEffectuee ? 1231 : 1237);
        result = prime * result + chaineDeCertificationValide;
        result = prime * result + dateSignatureValide;
        result = prime * result + ((dateValiditeAu == null) ? 0 : dateValiditeAu.hashCode());
        result = prime * result + ((dateValiditeDu == null) ? 0 : dateValiditeDu.hashCode());
        result = prime * result + (dssValidationEffectuee ? 1231 : 1237);
        result = prime * result + ((emetteur == null) ? 0 : emetteur.hashCode());
        result = prime * result + ((emetteurDetails == null) ? 0 : emetteurDetails.hashCode());
        result = prime * result + etat;
        result = prime * result + ((periodiciteValide == null) ? 0 : periodiciteValide.hashCode());
        result = prime * result + ((reportsDetailed == null) ? 0 : reportsDetailed.hashCode());
        result = prime * result + ((reportsDiagnostic == null) ? 0 : reportsDiagnostic.hashCode());
        result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
        result = prime * result + ((signataireComplet == null) ? 0 : signataireComplet.hashCode());
        result = prime * result + ((signataireDetails == null) ? 0 : signataireDetails.hashCode());
        result = prime * result + ((signatairePartiel == null) ? 0 : signatairePartiel.hashCode());
        result = prime * result + ((signatureValide == null) ? 0 : signatureValide.hashCode());
        result = prime * result + ((signatureXadesServeurEnBase64 == null) ? 0 : signatureXadesServeurEnBase64.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InfosVerificationCertificat other = (InfosVerificationCertificat) obj;
        if (absenceRevocationCRL != other.absenceRevocationCRL)
            return false;
        if (chaineDeCertificationValide != other.chaineDeCertificationValide)
            return false;
        if (dateSignatureValide != other.dateSignatureValide)
            return false;
        if (dateValiditeAu == null) {
            if (other.dateValiditeAu != null)
                return false;
        } else if (!dateValiditeAu.equals(other.dateValiditeAu))
            return false;
        if (dateValiditeDu == null) {
            if (other.dateValiditeDu != null)
                return false;
        } else if (!dateValiditeDu.equals(other.dateValiditeDu))
            return false;
        if (emetteur == null) {
            if (other.emetteur != null)
                return false;
        } else if (!emetteur.equals(other.emetteur))
            return false;
        if (emetteurDetails == null) {
            if (other.emetteurDetails != null)
                return false;
        } else if (!emetteurDetails.equals(other.emetteurDetails))
            return false;
//        if (etat != other.etat)
//            return false;
        if (periodiciteValide == null) {
            if (other.periodiciteValide != null)
                return false;
        } else if (!periodiciteValide.equals(other.periodiciteValide))
            return false;
        if (signataireComplet == null) {
            if (other.signataireComplet != null)
                return false;
        } else if (!signataireComplet.equals(other.signataireComplet))
            return false;
        if (signataireDetails == null) {
            if (other.signataireDetails != null)
                return false;
        } else if (!signataireDetails.equals(other.signataireDetails))
            return false;
        if (signatairePartiel == null) {
            if (other.signatairePartiel != null)
                return false;
        } else if (!signatairePartiel.equals(other.signatairePartiel))
            return false;
        if (signatureValide == null) {
            if (other.signatureValide != null)
                return false;
        } else if (!signatureValide.equals(other.signatureValide))
            return false;
        return true;
    }

}
