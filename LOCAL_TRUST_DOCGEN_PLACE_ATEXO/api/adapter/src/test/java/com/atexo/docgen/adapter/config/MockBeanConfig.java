package com.atexo.docgen.adapter.config;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.dao.repository.*;
import com.atexo.docgen.ws.clients.onlyoffice.OnlyOfficeClientWS;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


@SpringBootTest(classes = MapperScanConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MockBeanConfig {


    @MockBean
    protected DocumentRepository documentRepository;


    @MockBean
    protected IDocumentConfigurationService iDocumentConfigurationService;

    @MockBean
    protected OnlyOfficeClientWS onlyOfficeClientWS;
    @MockBean
    protected UserRepository userRepository;
    @MockBean
    protected RoleRepository roleRepository;
    @MockBean
    protected SurchargeLibelleRepository surchargeLibelleRepository;
    @MockBean
    protected ChampFusionRacourciRepository champFusionRacourciRepository;
}
