package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.adapter.config.MockBeanConfig;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.domain.monitoring.model.MonitoringModel;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.dao.entity.DocumentEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class DocumentAdapterTest extends MockBeanConfig {


    @Autowired
    private IDocumentAdapter iDocumentAdapter;

    private DocumentEntity savedDocument;

    @BeforeEach
    public void setUp() {
        savedDocument = DocumentEntity.builder()
                .fileType("xlsx")
                .key("-1342744491")
                .creationDate(Timestamp.valueOf(LocalDateTime.of(2020, 12, 2, 12, 45, 20)))
                .status("REQUEST_TO_OPEN")
                .plateformeId("plateformeId").documentId("documentId")
                .id("id")
                .title("test-template (2) (2) (1).xlsx")
                .build();

        when(documentRepository.save(any())).thenReturn(savedDocument);
        when(iDocumentConfigurationService.generateDocumentKey(any())).thenReturn("generated-key");
    }

    @Test
    public void testAddDocumentIfNotExist() {
        //Given
        DocumentEditorRequest fileModel = DocumentEditorRequest.builder()
                .mode("edit")
                .callback("http://callback.com")
                .user(User.builder().id("id").name("name").build())
                .build();
        when(documentRepository.findById(any())).thenReturn(Optional.empty());

        //When

        String docx = "docx";
        String plateformeId = "plateformeId";
        String idFileSystem = "idFileSystem";
        long md5 = 1L;
        DocumentModel result = iDocumentAdapter.addDocumentIfNotExist(fileModel, docx, plateformeId, idFileSystem, md5, 0);
        //Then
        assertNotNull(result);
        verify(documentRepository, times(1)).findById(any());
        verify(documentRepository, times(1)).save(any());
        verify(iDocumentConfigurationService, times(1)).generateDocumentKey(any());

    }

    @Test
    public void testModifyDocumentIfExist() {
        //Given
        DocumentEditorRequest fileModel = DocumentEditorRequest.builder()
                .mode("edit")
                .callback("http://callback.com")
                .user(User.builder().id("id").name("name").build())
                .build();
        when(documentRepository.findById(any())).thenReturn(Optional.of(savedDocument));
        String fileName = "test.docx";
        String fileExtension = "docx";
        String key = "key";

        //When

        String docx = "docx";
        String plateformeId = "plateformeId";
        String idFileSystem = "idFileSystem";
        long md5 = 1L;
        DocumentModel result = iDocumentAdapter.addDocumentIfNotExist(fileModel, docx, plateformeId, idFileSystem, md5, 5);

        //Then
        assertNotNull(result);
        verify(documentRepository, times(1)).findById(any());
        verify(documentRepository, times(1)).save(any());
        verify(iDocumentConfigurationService, times(1)).generateDocumentKey(any());

    }

    @Test
    public void testModifyDocumentStatus() {
        String test = "test";
        when(documentRepository.findById(test)).thenReturn(Optional.of(new DocumentEntity()));
        when(documentRepository.save(any())).thenReturn(new DocumentEntity());

        DocumentModel result = iDocumentAdapter.modifyDocumentStatusById(test, test);
        assertEquals(new DocumentModel(), result);
        verify(documentRepository, times(1)).save(any());
        verify(documentRepository, times(1)).findById(test);

    }

    @Test
    public void shouldReturnNullWhenGetDocumentByKeyIsNull() {
        String test = "test";
        when(documentRepository.findById(test)).thenReturn(Optional.empty());

        DocumentModel result = iDocumentAdapter.getById(test);
        assertNull(result);
        verify(documentRepository, times(1)).findById(test);

    }

    @Test
    public void shouldReturnFileModelWhenGetDocumentByKeyIsNotNull() {
        String test = "test";
        when(documentRepository.findById(test)).thenReturn(Optional.of(savedDocument));

        DocumentModel result = iDocumentAdapter.getById(test);
        assertNotNull(result);
        verify(documentRepository, times(1)).findById(test);

    }

    @Test
    public void shouldReturnEmptyListWhenGetAll() {
        //TODO : correct
        when(documentRepository.findAll(any())).thenReturn(null);
        List<MonitoringModel> result = iDocumentAdapter.getAll();
        assertNotNull(result);
        assertEquals(0, result.size());
        verify(documentRepository, times(1)).findAll(any());

    }

    @Test
    public void shouldReturnNotEmptyListWhenGetAll() {
        when(documentRepository.findAll(any())).thenReturn(Collections.singletonList(savedDocument));
        List<MonitoringModel> result = iDocumentAdapter.getAll();
        assertNotNull(result);
        assertEquals(1, result.size());
        verify(documentRepository, times(1)).findAll(any());

    }

    @Test
    public void shouldReturnNotEmptyListWhenDocumentByPlateformeId() {
        when(documentRepository.findByPlateformeId(savedDocument.getPlateformeId())).thenReturn(Collections.singletonList(savedDocument));
        List<DocumentModel> result = iDocumentAdapter.getDocumentsByPlateformeId(savedDocument.getPlateformeId());
        assertNotNull(result);
        assertEquals(1, result.size());
        verify(documentRepository, times(1)).findByPlateformeId(savedDocument.getPlateformeId());

    }

    @Test
    public void shouldReturnEmptyListWhenDocumentByPlateformeId() {
        //TODO correct
        when(documentRepository.findByPlateformeId(savedDocument.getPlateformeId())).thenReturn(null);
        List<DocumentModel> result = iDocumentAdapter.getDocumentsByPlateformeId(savedDocument.getPlateformeId());
        assertNotNull(result);
        assertEquals(0, result.size());
        verify(documentRepository, times(1)).findByPlateformeId(savedDocument.getPlateformeId());

    }

    @Test
    public void shouldReturnNotEmptyListWhenDocumentByPlateformeIdAndStatus() {
        when(documentRepository.findByPlateformeId(savedDocument.getPlateformeId())).thenReturn(Collections.singletonList(savedDocument));
        List<DocumentModel> result = iDocumentAdapter.getDocumentsByPlateformeId(savedDocument.getPlateformeId());
        assertNotNull(result);
        assertEquals(1, result.size());
        verify(documentRepository, times(1)).findByPlateformeId(savedDocument.getPlateformeId());

    }

    @Test
    public void shouldReturnEmptyListWhenDocumentByPlateformeIdAndStatus() {
        //TODO correct

        when(documentRepository.findByPlateformeIdAndStatus(savedDocument.getPlateformeId(), savedDocument.getStatus())).thenReturn(null);
        List<DocumentModel> result = iDocumentAdapter.getDocumentsByPlateformeIdAndStatus(savedDocument.getPlateformeId(), FileStatusEnum.valueOf(savedDocument.getStatus()));
        assertNotNull(result);
        assertEquals(0, result.size());
        verify(documentRepository, times(1)).findByPlateformeIdAndStatus(savedDocument.getPlateformeId(), savedDocument.getStatus());

    }


}
