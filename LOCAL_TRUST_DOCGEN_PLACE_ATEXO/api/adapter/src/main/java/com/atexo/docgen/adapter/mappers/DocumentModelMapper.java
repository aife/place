package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.dao.entity.DocumentEntity;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING, uses = {IntAndFileRedacStatusMapper.class})
public interface DocumentModelMapper extends Mapper<DocumentEntity, DocumentModel> {

    DocumentModel mapToModel(DocumentEntity entity);

    DocumentEntity mapToEntity(DocumentModel model);
}
