package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.adapter.mappers.DocumentModelMapper;
import com.atexo.docgen.adapter.mappers.MonitoringDocumentModelMapper;
import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.monitoring.model.MonitoringModel;
import com.atexo.docgen.core.enums.FileModeEnum;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.port.IDocumentPort;
import com.atexo.docgen.dao.entity.DocumentEntity;
import com.atexo.docgen.dao.repository.DocumentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class IDocumentAdapter implements IDocumentPort {

    private final DocumentRepository documentRepository;
    private final DocumentModelMapper documentModelMapper;
    private final MonitoringDocumentModelMapper monitoringDocumentModelMapper;
    private final IDocumentConfigurationService iDocumentConfigurationService;

    public IDocumentAdapter(DocumentRepository documentRepository, DocumentModelMapper documentModelMapper, IDocumentConfigurationService iDocumentConfigurationService, MonitoringDocumentModelMapper monitoringDocumentModelMapper) {
        this.documentRepository = documentRepository;
        this.documentModelMapper = documentModelMapper;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.monitoringDocumentModelMapper = monitoringDocumentModelMapper;
    }

    @Override
    public DocumentModel addDocumentIfNotExist(DocumentEditorRequest editorRequest, String curExt, String plateformeId, String id, long size, int version) {

        log.info("Ajout de la demande de du document {} avec la plateforme : {}", editorRequest.getDocumentId(), plateformeId);
        DocumentEntity documentEntity = documentRepository.findById(id).map(document -> {
            document.setTitle(editorRequest.getDocumentTitle());
            document.setFileType(curExt);
            document.setMode(editorRequest.getMode());
            document.setModificationDate(new Timestamp(System.currentTimeMillis()));
            document.setRedacAction(null);
            return document;
        }).orElse(DocumentEntity.builder()
                .fileType(curExt)
                .id(id)
                .title(editorRequest.getDocumentTitle())
                .documentId(editorRequest.getDocumentId())
                .plateformeId(plateformeId)
                .mode(editorRequest.getMode())
                .saved(true)
                .creationDate(new Timestamp(System.currentTimeMillis()))
                .modificationDate(new Timestamp(System.currentTimeMillis()))
                .build());
        String key = iDocumentConfigurationService.generateDocumentKey(documentEntity.getCompositeNewKey());
        documentEntity.setKey(key);
        documentEntity.setVersion(version);
        if (FileModeEnum.VIEW.getValue().equals(documentEntity.getMode())) {
            documentEntity.setStatus(FileStatusEnum.CLOSED_WITHOUT_EDITING.name());
        } else {
            documentEntity.setStatus(FileStatusEnum.REQUEST_TO_OPEN.name());
        }
        return documentModelMapper.mapToModel(documentRepository.save(documentEntity));
    }

    @Override
    public void addNewVersion(String id, int newVersion, FileStatusEnum statusEnum) {
        documentRepository.addVersionById(id, newVersion, statusEnum.name());
    }

    @Override
    public void modifyDocumentRedacStatusById(String id, String status) {
        documentRepository.modifyDocumentRedacStatusById(id, status);
    }

    @Override
    public String setNewKey(String id, String compositeKey) {
        String key = iDocumentConfigurationService.generateDocumentKey(compositeKey);
        documentRepository.updateKeyAndSizeById(id, key);
        return key;
    }

    @Override
    public void setNewVersion(String id, boolean newVersion) {
        documentRepository.updateNewVersionById(id, newVersion);
    }

    @Override
    public DocumentModel modifyDocumentStatusById(String id, String status) {
        log.info("Modification du statut de {} à {}", id, status);
        DocumentEntity documentEntity = documentRepository.findById(id).orElse(null);
        if (null == documentEntity) {
            throw new DocGenException(TypeExceptionEnum.EDITOR, "Le document n'existe pas");
        }
        documentEntity.setStatus(status);
        documentEntity.setModificationDate(new Timestamp(System.currentTimeMillis()));
        return documentModelMapper.mapToModel(documentRepository.save(documentEntity));

    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public DocumentModel getById(String id) {
        return documentModelMapper.mapToModel(documentRepository.findById(id).orElse(null));
    }

    @Override
    public List<MonitoringModel> getAll() {
        List<MonitoringModel> documentModels = new ArrayList<>();
        Pageable page = PageRequest.of(0, 20, Sort.by("creationDate").descending());

        Iterable<DocumentEntity> documentEntities = documentRepository.findAll(page);
        if (documentEntities != null) {
            documentEntities.forEach(documentEntity -> documentModels.add(monitoringDocumentModelMapper.mapToModel(documentEntity)));
        }
        return documentModels;
    }

    @Override
    public List<DocumentModel> getDocumentsByPlateformeId(String plateformeId) {
        List<DocumentModel> documentModels = new ArrayList<>();
        Iterable<DocumentEntity> documentEntities = documentRepository.findByPlateformeId(plateformeId);
        if (documentEntities != null) {
            documentEntities.forEach(documentEntity -> documentModels.add(documentModelMapper.mapToModel(documentEntity)));
        }
        return documentModels;
    }

    @Override
    public List<DocumentModel> getDocumentsByPlateformeIdAndStatus(String plateformeId, FileStatusEnum status) {
        List<DocumentModel> documentModels = new ArrayList<>();
        Iterable<DocumentEntity> documentEntities = documentRepository.findByPlateformeIdAndStatus(plateformeId, status.name());
        if (documentEntities != null) {
            documentEntities.forEach(documentEntity -> documentModels.add(documentModelMapper.mapToModel(documentEntity)));
        }
        return documentModels;
    }

    @Override
    public void deleteById(String id) {
        documentRepository.deleteById(id);
    }
}
