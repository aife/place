package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.domain.convertor.model.analyse.Soumissionnaire;
import com.atexo.docgen.core.domain.extractor.model.analyse.SoumissionnaireAnalyse;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING, uses = {IntAndFileRedacStatusMapper.class})
public interface SoumissionnaireMapper extends Mapper<Soumissionnaire, SoumissionnaireAnalyse> {

    SoumissionnaireAnalyse mapToModel(Soumissionnaire entity);

    Soumissionnaire mapToEntity(SoumissionnaireAnalyse model);
}
