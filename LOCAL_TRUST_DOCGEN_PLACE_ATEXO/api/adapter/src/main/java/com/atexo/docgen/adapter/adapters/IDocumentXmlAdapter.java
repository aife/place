package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.adapter.mappers.DocumentXmlModelMapper;
import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.model.redac.DocumentXmlModel;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.enums.FileRedacStatusEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.port.IDocumentXmlPort;
import com.atexo.docgen.dao.entity.DocumentEntity;
import com.atexo.docgen.dao.repository.DocumentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.getObjectMapper;

@Component
@Slf4j
public class IDocumentXmlAdapter implements IDocumentXmlPort {

    private final DocumentRepository documentRepository;
    private final DocumentXmlModelMapper documentXmlModelMapper;
    private final IDocumentConfigurationService iDocumentConfigurationService;

    public IDocumentXmlAdapter(DocumentRepository documentRepository, DocumentXmlModelMapper documentXmlModelMapper, IDocumentConfigurationService iDocumentConfigurationService) {
        this.documentRepository = documentRepository;
        this.documentXmlModelMapper = documentXmlModelMapper;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
    }

    @Override
    public DocumentXmlModel modifyDocumentXml(String id, DocxDocument document) {
        try {
            String json = getObjectMapper().writeValueAsString(document);
            DocumentEntity xmlEntity = documentRepository.findById(id).map(documentXmlEntity -> {
                documentXmlEntity.setUpdatedJson(json);
                if (document.getLastAction() != null) {
                    documentXmlEntity.setRedacAction(document.getLastAction());
                }
                if (document.getStatus() != null) {
                    FileRedacStatusEnum fileRedacStatusEnum = FileRedacStatusEnum.fromValue(document.getStatus());
                    documentXmlEntity.setRedacStatus(fileRedacStatusEnum.name());
                }
                String newKey = iDocumentConfigurationService.generateDocumentKey(documentXmlEntity.getCompositeNewKey());
                if (newKey != null) {
                    documentXmlEntity.setKey(newKey);
                }
                return documentXmlEntity;
            }).orElse(null);
            return xmlEntity == null ? null : documentXmlModelMapper.mapToModel(documentRepository.save(xmlEntity));
        } catch (Exception e) {
            log.error("Erreur lors de la modification du document xml =>  {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage(), e);
        }

    }

    @Override
    public void addDocumentIfNotExist(DocumentXmlModel xmlModel) {
        documentRepository.save(documentXmlModelMapper.mapToEntity(xmlModel));
    }

    @Override
    public DocumentXmlModel getDocumentById(String id) {
        return documentXmlModelMapper.mapToModel(documentRepository.findById(id).orElse(null));
    }


}
