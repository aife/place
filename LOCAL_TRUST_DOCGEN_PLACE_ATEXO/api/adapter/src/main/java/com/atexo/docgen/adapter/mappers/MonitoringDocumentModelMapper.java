package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.domain.monitoring.model.MonitoringModel;
import com.atexo.docgen.dao.entity.DocumentEntity;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING, uses = {IntAndFileRedacStatusMapper.class})
public interface MonitoringDocumentModelMapper extends Mapper<DocumentEntity, MonitoringModel> {

    MonitoringModel mapToModel(DocumentEntity entity);

    DocumentEntity mapToEntity(MonitoringModel model);
}
