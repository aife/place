package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.core.domain.security.model.AtexoUserDetails;
import com.atexo.docgen.core.domain.security.model.MessageResponse;
import com.atexo.docgen.core.domain.security.model.SignupRequest;
import com.atexo.docgen.core.port.IUserPort;
import com.atexo.docgen.dao.entity.ERole;
import com.atexo.docgen.dao.entity.RoleEntity;
import com.atexo.docgen.dao.entity.UserEntity;
import com.atexo.docgen.dao.repository.RoleRepository;
import com.atexo.docgen.dao.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class IUserAdapter implements IUserPort {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public IUserAdapter(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public AtexoUserDetails findByUsername(String username) {
        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return this.build(user);
    }

    @Override
    public MessageResponse registerUser(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new MessageResponse(HttpStatus.BAD_REQUEST, "Error: Username is already taken!");

        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new MessageResponse(HttpStatus.BAD_REQUEST, "Error: Email is already in use!");
        }

        // Create new user's account
        UserEntity user = UserEntity.builder().username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .password(new BCryptPasswordEncoder().encode(signUpRequest.getPassword()))
                .build();

        Set<String> strRoles = signUpRequest.getRoles();
        Set<RoleEntity> roles = new HashSet<>();

        final String message = "Error: Role is not found.";
        if (strRoles == null) {
            RoleEntity userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException(message));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                if ("admin".equals(role)) {
                    RoleEntity adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException(message));
                    roles.add(adminRole);
                } else {
                    RoleEntity userRole = roleRepository.findByName(ERole.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException(message));
                    roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return new MessageResponse(HttpStatus.OK, "User registered successfully!");
    }


    public AtexoUserDetails build(UserEntity user) {
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        return new AtexoUserDetails(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities);
    }
}
