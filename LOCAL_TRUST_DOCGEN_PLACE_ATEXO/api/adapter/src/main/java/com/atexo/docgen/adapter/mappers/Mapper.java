package com.atexo.docgen.adapter.mappers;

import java.util.Collection;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.CollectionUtils.isEmpty;

public interface Mapper<E, M> {

    E mapToEntity(M model);

    M mapToModel(E entity);

    default List<E> mapToEntities(Collection<M> models) {
        if (isEmpty(models)) {
            return emptyList();
        }
        return models.stream()
                .map(this::mapToEntity)
                .collect(toList());
    }

    default List<M> mapToModels(Collection<E> entities) {
        if (isEmpty(entities)) {
            return emptyList();
        }
        return entities.stream()
                .map(this::mapToModel)
                .collect(toList());
    }
}
