package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.adapter.mappers.ConsultationMapper;
import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConsultationConfig;
import com.atexo.docgen.core.port.IAnalysePort;
import org.springframework.stereotype.Component;

@Component
public class IAnalyseAdapter implements IAnalysePort {

    private final ConsultationMapper consultationMapper;

    public IAnalyseAdapter(ConsultationMapper consultationMapper) {
        this.consultationMapper = consultationMapper;
    }

    @Override
    public ConsultationConfig map(Consultation consultation) {
        return consultationMapper.mapToModel(consultation);
    }
}
