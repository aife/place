package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.core.port.IChampFusionRacourciPort;
import com.atexo.docgen.dao.repository.ChampFusionRacourciRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class IChampFusionRacourciAdapter implements IChampFusionRacourciPort {

    private final ChampFusionRacourciRepository champFusionRacourciRepository;


    public IChampFusionRacourciAdapter(ChampFusionRacourciRepository champFusionRacourciRepository) {
        this.champFusionRacourciRepository = champFusionRacourciRepository;
    }

    @Override
    public String getChampFusionFromRacourci(String racourci) {
        return champFusionRacourciRepository.findByRacourciEquals(racourci)
                .map(champFusionRacourciEntity -> {
                    if (champFusionRacourciEntity.getRacourci().equals(racourci)) {
                        return champFusionRacourciEntity.getChampFusion();
                    } else {
                        return null;
                    }
                }).orElse(null);
    }
}
