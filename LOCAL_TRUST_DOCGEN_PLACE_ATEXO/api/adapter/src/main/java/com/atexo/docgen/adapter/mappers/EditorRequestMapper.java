package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING)
public interface EditorRequestMapper {


    DocumentEditorRequest mapToModel(DocumentEditorRequest model);
}
