package com.atexo.docgen.adapter.adapters;

import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.OnlyOfficeException;
import com.atexo.docgen.core.port.IOnlyOfficePort;
import com.atexo.docgen.dao.repository.SurchargeLibelleRepository;
import com.atexo.docgen.ws.clients.onlyoffice.OnlyOfficeClientWS;
import com.atexo.docgen.ws.clients.onlyoffice.model.CommandRequest;
import com.atexo.docgen.ws.clients.onlyoffice.model.CommandResponse;
import com.atexo.docgen.ws.clients.onlyoffice.model.ConvertRequest;
import com.atexo.docgen.ws.clients.onlyoffice.model.ConvertResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class IOnlyOfficeAdapter implements IOnlyOfficePort {

    private final OnlyOfficeClientWS onlyOfficeClient;
    private final SurchargeLibelleRepository surchargeLibelleRepository;

    public IOnlyOfficeAdapter(OnlyOfficeClientWS onlyOfficeClient, SurchargeLibelleRepository surchargeLibelleRepository) {
        this.onlyOfficeClient = onlyOfficeClient;
        this.surchargeLibelleRepository = surchargeLibelleRepository;
    }


    @Override
    public String convertFile(String outputFormat, String url, String key, String filetype) {
        ConvertRequest convertRequest = ConvertRequest.builder()
                .outputtype(outputFormat)
                .url(url)
                .key(key)
                .filetype(filetype)
                .build();

        ConvertResponse response = onlyOfficeClient.convertFile(convertRequest);
        if (response == null) {
            throw new OnlyOfficeException(TypeExceptionEnum.CONVERSION, "Pas de réponse");

        }
        if (!response.isEndConvert()) {
            switch (response.getError()) {

                case -2:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Erreur timeout de conversion");
                case -3:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Erreur de conversion");
                case -4:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Erreur lors du téléchargement du fichier document à convertir.");
                case -5:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Mot de passe incorrect.");
                case -6:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Erreur lors de l'accès à la base de données des résultats de conversion.");
                case -7:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Erreur d'input");
                case -8:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Token invalide");
                default:
                    throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Erreur Inconnu");
            }
        }

        return response.getFileUrl();
    }

    @Override
    public String getApis() {
        return onlyOfficeClient.apis();
    }

    @Override
    public Integer forceSave(String key) {
        return onlyofficeCommand(key, "forcesave");
    }


    @Override
    public Integer info(String key) {
        return onlyofficeCommand(key, "info");
    }

    @Override
    public Map<String, String> getSurchargesLibelles() {
        Map<String, String> map = new HashMap<>();
        surchargeLibelleRepository.findAll().forEach(refSurchargeLibelleEntity -> map.put(refSurchargeLibelleEntity.getCode(), refSurchargeLibelleEntity.getLibelle()));
        return map;
    }

    private Integer onlyofficeCommand(String key, String command) {
        CommandResponse result;
        try {
            result = onlyOfficeClient.command(CommandRequest.builder().c(command).key(key).build());
        } catch (HttpStatusCodeException e) {
            log.error("handleClientError {}", e.getMessage());
            throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, e.getResponseBodyAsString());
        }

        log.info(result.toString());
        switch (result.getError()) {

            case 1:
                throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "La clé de document est manquante ou aucun document avec une telle clé n'a pu être trouvé.");
            case 2:
                throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "L'URL de rappel n'est pas correcte.");
            case 3:
                throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Internal server error.");
            case 5:
                throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Commande incorrecte.");
            case 6:
                throw new OnlyOfficeException(TypeExceptionEnum.ONLYOFFICE, "Jeton invalide.");
            default:
                return result.getError();
        }
    }
}
