package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.domain.convertor.model.redac.DocumentXmlModel;
import com.atexo.docgen.dao.entity.DocumentEntity;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING, uses = {IntAndFileRedacStatusMapper.class})
public interface DocumentXmlModelMapper extends Mapper<DocumentEntity, DocumentXmlModel> {

    DocumentXmlModel mapToModel(DocumentEntity entity);

    DocumentEntity mapToEntity(DocumentXmlModel model);
}
