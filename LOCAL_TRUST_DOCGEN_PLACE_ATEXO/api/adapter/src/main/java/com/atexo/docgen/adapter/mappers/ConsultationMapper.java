package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConsultationConfig;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING, uses = {IntAndFileRedacStatusMapper.class})
public interface ConsultationMapper extends Mapper<Consultation, ConsultationConfig> {

    ConsultationConfig mapToModel(Consultation entity);

    Consultation mapToEntity(ConsultationConfig model);
}
