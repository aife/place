package com.atexo.docgen.adapter.mappers;

import com.atexo.docgen.adapter.utils.MapperConstants;
import com.atexo.docgen.core.enums.FileRedacStatusEnum;

import java.util.Objects;

@org.mapstruct.Mapper(componentModel = MapperConstants.SPRING)
public interface IntAndFileRedacStatusMapper extends Mapper<String, Integer> {

    @Override
    default String mapToEntity(Integer model) {
        return Objects.nonNull(model) ? FileRedacStatusEnum.fromValue(model).name() : FileRedacStatusEnum.NOT_DEFINIED.name();
    }

    @Override
    default Integer mapToModel(String entity) {
        return Objects.nonNull(entity) ? FileRedacStatusEnum.valueOf(entity).getCode() : 0;
    }

}
