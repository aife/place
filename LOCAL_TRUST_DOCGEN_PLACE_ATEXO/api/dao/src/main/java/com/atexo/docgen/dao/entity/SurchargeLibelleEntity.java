package com.atexo.docgen.dao.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "SURCHARGE_LIBELLE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SurchargeLibelleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "code")
    protected String code;

    @Column(name = "libelle")
    protected String libelle;


}
