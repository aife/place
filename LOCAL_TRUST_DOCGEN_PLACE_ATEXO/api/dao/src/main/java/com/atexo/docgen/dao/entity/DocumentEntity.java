package com.atexo.docgen.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "DOCUMENT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentEntity {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "DOCUMENT_ID")
    private String documentId;

    @Column(name = "MODE")
    private String mode;

    @Column(name = "PLATEFORME_ID")
    private String plateformeId;

    @Column(name = "NEW_VERSION")
    private boolean newVersion;

    @Column(name = "EDITION_KEY")
    private String key;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "CREATION_DATE")
    private Timestamp creationDate;

    @Column(name = "MODIFICATION_DATE")
    private Timestamp modificationDate;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "FILE_TYPE")
    private String fileType;

    @Lob
    @Column(name = "JSON_XML")
    private String json;
    @Lob
    @Column(name = "UPDATED_JSON_XML")
    private String updatedJson;

    @Column(name = "REDAC_STATUS")
    private String redacStatus;

    @Column(name = "REDAC_ACTION")
    private String redacAction;

    @Column(name = "VERSION")
    private int version;
    @Column(name = "SAVED")
    private boolean saved;

    public String getCompositeNewKey() {
        return this.getPlateformeId() + "/" +
                this.getDocumentId() + "/" + System.currentTimeMillis();
    }
}
