package com.atexo.docgen.dao.repository;

import com.atexo.docgen.dao.entity.SurchargeLibelleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SurchargeLibelleRepository extends JpaRepository<SurchargeLibelleEntity, Integer> {

}
