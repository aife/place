package com.atexo.docgen.dao.repository;

import com.atexo.docgen.dao.entity.ChampFusionRacourciEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChampFusionRacourciRepository extends JpaRepository<ChampFusionRacourciEntity, Integer> {

    Optional<ChampFusionRacourciEntity> findByRacourciEquals(String racourci);
}
