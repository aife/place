package com.atexo.docgen.dao.repository;

import com.atexo.docgen.dao.entity.DocumentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DocumentRepository extends CrudRepository<DocumentEntity, String> {

    Iterable<DocumentEntity> findAll(Pageable page);

    Iterable<DocumentEntity> findByPlateformeId(String plateformeId);

    Iterable<DocumentEntity> findByPlateformeIdAndStatus(String plateforme, String status);

    @Transactional
    @Modifying
    @Query("UPDATE DocumentEntity doc SET doc.key = :key WHERE doc.id = :id")
    void updateKeyAndSizeById(@Param("id") String id, @Param("key") String key);

    @Transactional
    @Modifying
    @Query("UPDATE DocumentEntity doc SET doc.newVersion= :newVersion, doc.saved=false WHERE doc.id = :id")
    void updateNewVersionById(@Param("id") String id, @Param("newVersion") boolean newVersion);

    @Transactional
    @Modifying
    @Query("UPDATE DocumentEntity doc SET doc.saved= true , doc.version= :newVersion," +
            "doc.status =:status ,doc.modificationDate=current_timestamp WHERE doc.id = :id")
    void addVersionById(@Param("id") String id, @Param("newVersion") int newVersion, @Param("status") String status);

    @Transactional
    @Modifying
    @Query("UPDATE DocumentEntity doc SET doc.redacStatus =:status WHERE doc.id = :id")
    void modifyDocumentRedacStatusById(String id, String status);
}
