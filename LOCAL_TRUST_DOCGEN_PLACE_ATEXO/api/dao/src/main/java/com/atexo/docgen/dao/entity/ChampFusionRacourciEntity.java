package com.atexo.docgen.dao.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "CHAMP_FUSION_RACOURCI")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChampFusionRacourciEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "RACOURCI")
    protected String racourci;

    @Column(name = "CHAMP_FUSION")
    protected String champFusion;


}
