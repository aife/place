package com.atexo.docgen.dao.config;

import org.testcontainers.containers.MariaDBContainer;

public class ITCustomMariaDBContainer extends MariaDBContainer<ITCustomMariaDBContainer> {
    private static ITCustomMariaDBContainer ITCustomMariaDBContainer;

    private ITCustomMariaDBContainer() {
        super();
    }

    public static ITCustomMariaDBContainer getInstance(String databaseName) {
        if (ITCustomMariaDBContainer != null) {
            ITCustomMariaDBContainer.stop();
        }
        ITCustomMariaDBContainer = new ITCustomMariaDBContainer().withDatabaseName(databaseName);

        return ITCustomMariaDBContainer;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", ITCustomMariaDBContainer.getJdbcUrl());
        System.setProperty("DB_USERNAME", ITCustomMariaDBContainer.getUsername());
        System.setProperty("DB_PASSWORD", ITCustomMariaDBContainer.getPassword());
    }

    @Override
    public void stop() {
        // do nothing, JVM handles shut down
    }
}
