package com.atexo.docgen.dao.repository;

import com.atexo.docgen.dao.config.AtexoMariaDBExtension;
import com.atexo.docgen.dao.config.DatabaseAbstractStarter;
import com.atexo.docgen.dao.entity.DocumentEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class DocumentRepositoryTest extends DatabaseAbstractStarter {

    @RegisterExtension
    public static final AtexoMariaDBExtension MARIA_DB_EXTENSION = new AtexoMariaDBExtension("dao");

    @Autowired
    private DocumentRepository documentRepository;

    private DocumentEntity savedDocument;
    private DocumentEntity savedDocument2;

    @BeforeEach
    public void setUp() {
        savedDocument = DocumentEntity.builder()
                .fileType("xlsx")
                .key("-1342744491")
                .creationDate(Timestamp.valueOf(LocalDateTime.of(2020, 12, 2, 12, 45, 20)))
                .status("REQUEST_TO_OPEN")
                .plateformeId("plateformeId").documentId("documentId")
                .title("test-template (2) (2) (1).xlsx")
                .id("id1")
                .build();

        savedDocument = documentRepository.save(savedDocument);
        savedDocument2 = DocumentEntity.builder()
                .fileType("xlsx")
                .key("-1342744492")
                .creationDate(Timestamp.valueOf(LocalDateTime.of(2020, 12, 2, 12, 45, 20)))
                .status("SAVED_AND_CLOSED")
                .plateformeId("plateformeId2").documentId("documentId2")
                .id("id2")
                .title("test-template (2) (2) (1).xlsx")
                .build();

        savedDocument2 = documentRepository.save(savedDocument2);
    }

    @Test
    public void testFindByPlateformeId() {
        //Given
        //When
        Iterable<DocumentEntity> result = documentRepository.findByPlateformeId(savedDocument.getPlateformeId());
        //Then
        AtomicInteger i = new AtomicInteger();
        result.forEach(documentEntity -> {
            i.getAndIncrement();
            assertNotNull(documentEntity);
            assertEqualsDocumentEntity(savedDocument, documentEntity);
        });
        assertEquals(1, i.get());

    }

    @Test
    public void testFindByPlateformeIdAndStatus() {
        //Given
        //When
        Iterable<DocumentEntity> result = documentRepository.findByPlateformeIdAndStatus(savedDocument2.getPlateformeId(), savedDocument2.getStatus());
        //Then
        AtomicInteger i = new AtomicInteger();
        result.forEach(documentEntity -> {
            i.getAndIncrement();
            assertNotNull(documentEntity);
            assertEqualsDocumentEntity(savedDocument2, documentEntity);
        });
        assertEquals(1, i.get());

    }


    private void assertEqualsDocumentEntity(DocumentEntity actual, DocumentEntity expected) {
        assertNotNull(actual);

        assertEquals(expected.getFileType(), actual.getFileType());
        assertEquals(expected.getKey(), actual.getKey());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getTitle(), actual.getTitle());
        assertEquals(expected.getPlateformeId(), actual.getPlateformeId());
        assertEquals(expected.getDocumentId(), actual.getDocumentId());
        assertEquals(expected.getCreationDate(), actual.getCreationDate());

    }

    @AfterEach
    public void tearDown() {
        documentRepository.deleteAll();
    }
}
