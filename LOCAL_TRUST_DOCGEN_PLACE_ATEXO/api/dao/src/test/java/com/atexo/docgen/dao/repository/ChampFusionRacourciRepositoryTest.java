package com.atexo.docgen.dao.repository;

import com.atexo.docgen.dao.config.AtexoMariaDBExtension;
import com.atexo.docgen.dao.config.DatabaseAbstractStarter;
import com.atexo.docgen.dao.entity.ChampFusionRacourciEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class ChampFusionRacourciRepositoryTest extends DatabaseAbstractStarter {

    @RegisterExtension
    public static final AtexoMariaDBExtension MARIA_DB_EXTENSION = new AtexoMariaDBExtension("dao");

    @Autowired
    private ChampFusionRacourciRepository champFusionRacourciRepository;

    @Test
    public void testFindByRacourciEqualsThenReturnRacourci() {
        //Given
        //When
        Optional<ChampFusionRacourciEntity> champFusionRacourciEntity = champFusionRacourciRepository.findByRacourciEquals("LOT");
        assertThat(champFusionRacourciEntity).isPresent();
        assertThat(champFusionRacourciEntity.get().getChampFusion()).isEqualTo("consultation.lot.donneesComplementaires");
    }

    @Test
    public void testFindByRacourciEqualsThenReturnNull() {
        //Given
        //When
        Optional<ChampFusionRacourciEntity> champFusionRacourciEntity = champFusionRacourciRepository.findByRacourciEquals("lot");
        assertThat(champFusionRacourciEntity).isPresent();
        assertThat(champFusionRacourciEntity.get().getChampFusion()).isEqualTo("consultation.lot.donneesComplementaires");
    }


}
