package com.atexo.docgen.dao.config;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = {DatabaseScanConfig.class})
@ActiveProfiles("db")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
public abstract class DatabaseAbstractStarter {


}
