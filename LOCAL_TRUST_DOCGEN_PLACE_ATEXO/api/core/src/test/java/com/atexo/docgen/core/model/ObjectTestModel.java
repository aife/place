package com.atexo.docgen.core.model;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ObjectTestModel implements Serializable {
	private String simpleString;
	private SubObject subObject;
	private List<SubObject> subObjects;


	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	@ToString
	public static class SubObject implements Serializable {
		private String simpleString;
	}

}
