package com.atexo.docgen.core.utilitaire;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DocumentUtilsTest {

	@Test
	public void shouldReturnNullWhenNameIsNull() {
		String result = DocumentUtils.getExtension(null);
		assertNull(result);
	}

	@Test
	public void shouldReturnNullWhenExtensionIsNotDefined() {
		String result = DocumentUtils.getExtension("test");
		assertNull(result);
	}

	@Test
	public void shouldReturnExtension() {
		String extension = "extension";
		String result = DocumentUtils.getExtension("test." + extension);
		assertNotNull(result);
		assertEquals(result, extension);
	}
}
