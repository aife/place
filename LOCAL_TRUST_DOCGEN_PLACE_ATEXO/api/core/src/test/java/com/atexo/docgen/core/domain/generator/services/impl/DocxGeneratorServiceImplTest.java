package com.atexo.docgen.core.domain.generator.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.model.ObjectTestModel;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.getObjectMapper;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


public class DocxGeneratorServiceImplTest extends PortMockBeanConfig {

    @Autowired
    private DocxGeneratorServiceImpl docxGeneratorService;

    private FileGeneratorRequest fileGeneratorRequest;
    private final String path = "src/test/resources/";
    private static XWPFDocument docx;
    private static List<XWPFParagraph> paragraphList;

    @BeforeEach
    public void setUp() throws IOException {
        File file = new File("src/test/resources/test-template.docx");
        FileInputStream input = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        HashMap<String, Object> keyValues = new HashMap<>();

        keyValues.put("Header2", "test [ ] test & é # ç è > >");
        keyValues.put("objectTest",
                ObjectTestModel.builder()
                        .simpleString("simpleString")
                        .subObject(ObjectTestModel.SubObject
                                .builder()
                                .simpleString("simpleTest")
                                .build())
                        .subObjects(Arrays.asList(
                                ObjectTestModel.SubObject
                                        .builder()
                                        .simpleString("simpleTest1")
                                        .build(),
                                ObjectTestModel.SubObject
                                        .builder()
                                        .simpleString("simpleTest2")
                                        .build(),
                                ObjectTestModel.SubObject
                                        .builder()
                                        .simpleString("simpleTest3")
                                        .build(),
                                ObjectTestModel.SubObject
                                        .builder()
                                        .simpleString("simpleTest4")
                                        .build()))
                        .build());
        File json = new File("src/test/resources/generator/concentrateur.json");
        List<KeyValueRequest> newValues = List.of(objectMapper.readValue(json, KeyValueRequest[].class));

        keyValues.putAll(newValues.stream()
                .filter(keyValueRequest -> Objects.nonNull(keyValueRequest.getKey()) &&
                        Objects.nonNull(keyValueRequest.getValue()))
                .collect(Collectors.toMap(KeyValueRequest::getKey, KeyValueRequest::getValue)));


        fileGeneratorRequest = FileGeneratorRequest.builder()
                .keyValues(keyValues)
                .stream(template.getInputStream())
                .extension("docx")
                .build();
    }


    @Test
    public void shouldReturnListeDesVariables() throws Exception {
        //Given
        InputStream request = fileGeneratorRequest.getStream();
        //When
        final Map<String, Set<String>> result = docxGeneratorService.getFileVariable(request);
        //Then
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(result.get("SIMPLE"),
                new TreeSet<>(Arrays.asList("Body", "Footer", "objectTest.subObject.simpleString", "Header2", "PouvoirAdjudicateur", "NumeroConsultation")));
    }

    @Test
    public void shouldReturnListeDesVariablesBlocTemplate() throws Exception {
        //Given
        File file = new File("src/test/resources/template/bloc-template.docx");
        FileInputStream input = new FileInputStream(file);
        //When
        final Map<String, Set<String>> result = docxGeneratorService.getFileVariable(input);
        //Then
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(result.get("SIMPLE"),
                new TreeSet<>(Collections.singletonList("TITLE")));
        assertEquals(result.get("BLOC_TEMPLATE_objectTest.subObjects"),
                new TreeSet<>(Collections.singletonList("simpleString")));
    }


    @Test
    public void shouldGenerateDocument() throws Exception {
        //Given
        FileGeneratorRequest request = fileGeneratorRequest;
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(request, null, null);
        //Then
        assertNotNull(result);
        String name = path + "generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }
        File actualFile = new File(name);
        FileInputStream input = new FileInputStream(actualFile);
        MultipartFile template = new MockMultipartFile("template",
                actualFile.getName(), "text/plain", IOUtils.toByteArray(input));
        //When
        final Map<String, Set<String>> variables = docxGeneratorService.getFileVariable(template.getInputStream());
        //Then
        assertNotNull(variables);
        assertEquals(1, variables.size());
        assertEquals(new HashSet<>(), variables.get("SIMPLE"));

        File expectedFile = new File(path + "expected/expected-test.docx");
        assertDocxEquals(expectedFile, actualFile);
    }


    @Test
    void shouldGenerateBlocTemplateTableDocument() throws Exception {
        //Given
        String s = "src/test/resources/template/";
        File file = new File(s + "bloc-template.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + "generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }

    }

    @Test
    void shouldGenerateBlocTemplateDynamiqueDocument() throws Exception {
        //Given
        String s = "src/test/resources/template/";
        File file = new File(s + "bloc-template-dynamique.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + "generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }

    }



    @Test
    void shouldGenerateBlocTableDocument() throws Exception {
        //Given
        String s = "src/test/resources/template/";
        File file = new File(s + "bloc-table.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + "generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }

    }


    @Test
    void shouldGenerateBlocTemplateTextDocument() throws Exception {
        //Given
        //File file = new File(path + "/generator/bloc-template-text.docx");
        File file = new File(path + "/generator/modele_exe10.docx");
        //File file = new File(path + "/generator/nombre_candidats.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");
        fileGeneratorRequest.setKeyValues(getObjectMapper().readValue(new File(path + "/generator/bloc-allotie2-text.json"),
                Map.class));
        when(iChampFusionRacourciPort.getChampFusionFromRacourci("LOT")).thenReturn("consultation.lot.donneesComplementaires");
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + ".generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }
    }
    @Test
    void shouldGenerateBlocTableTextDocument() throws Exception {
        //Given
        //File file = new File(path + "/generator/bloc-template-text.docx");
        File file = new File(path + "/generator/table_place.docx");
        //File file = new File(path + "/generator/nombre_candidats.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");

        fileGeneratorRequest.setKeyValues(getObjectMapper().readValue(new File(path + "/generator/bloc-table.json"),
                Map.class));        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + ".generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }
    }

    @Test
    void shouldGenerateExecDocument() throws Exception {
        //Given
        //File file = new File(path + "/generator/bloc-template-text.docx");
        File file = new File(path + "/generator/modele_exe10.docx");
        //File file = new File(path + "/generator/nombre_candidats.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template", file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");
        fileGeneratorRequest.setKeyValues(getObjectMapper().readValue(new File(path + "/generator/exec.json"), Map.class));
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + ".generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }
    }

    @Test
    public void shouldGenerateComplexeDocument() throws Exception {
        //Given
        File file = new File(path + "/generator/complexe-document.docx");
        FileInputStream inputTemplate = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(inputTemplate));
        fileGeneratorRequest.setStream(template.getInputStream());
        fileGeneratorRequest.setExtension("docx");
        fileGeneratorRequest.setKeyValues(getObjectMapper().readValue(new File(path + "/generator/complexe-document.json"),
                Map.class));
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(fileGeneratorRequest, null, null);
        //Then
        assertNotNull(result);
        String name = file.getAbsolutePath() + ".generated-test.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }

    }

    public static void assertDocxEquals(File expectedFile, File actualFile) throws IOException {
        assertTrue(compare(getDocData(expectedFile.getAbsolutePath()), getDocData(actualFile.getAbsolutePath())));
    }


    private static List<String> getDocData(String filePath)
            throws IOException {
        List<String> listName = new ArrayList<>();
        File file = new File(filePath);
        FileInputStream fis = new FileInputStream(file);
        docx = new XWPFDocument(fis);

        paragraphList = docx.getParagraphs();

        for (int i = 0; i <= paragraphList.size() - 1; i++) {
            listName.add(paragraphList.get(i).getText());
        }
        fis.close();
        return listName;
    }


    private static boolean compare(List<String> expectedList,
                                   List<String> actualList) {
        boolean result = simpleCheck(expectedList, actualList);
        if (result) {
            int size = expectedList.size();
            for (int i = 0; i < size; i++) {
                result = paragraphCheck(expectedList.get(i).split(" "),
                        actualList.get(i).split(" "), i);
                if (!result) {
                    break;
                }

            }
        }
        return result;
    }

    private static boolean paragraphCheck(String[] firstParaArray,
                                          String[] secondParaArray, int paraNumber) {

        if (firstParaArray.length != secondParaArray.length) {
            return false;
        }
        TreeMap<String, Integer> firstDocPara = getOccurence(firstParaArray);
        TreeMap<String, Integer> secondDocPara = getOccurence(secondParaArray);
        ArrayList<String> keyData = new ArrayList<>(firstDocPara.keySet());
        for (String keyDatum : keyData) {
            if (!firstDocPara.get(keyDatum).equals(secondDocPara.get(keyDatum))) {
                System.out
                        .println("The following word is missing in actual document : "
                                + keyDatum);
                return false;
            }
        }

        return true;
    }

    private static TreeMap<String, Integer> getOccurence(String[] paraArray) {
        TreeMap<String, Integer> paragraphStringCountHolder = new TreeMap<>();
        for (String a : paraArray) {
            int count = 1;
            if (paragraphStringCountHolder.containsKey(a)) {
                count = paragraphStringCountHolder.get(a) + 1;
                paragraphStringCountHolder.put(a, count);
            } else {
                paragraphStringCountHolder.put(a, count);
            }
        }
        return paragraphStringCountHolder;
    }

    private static boolean simpleCheck(List<String> firstList,
                                       List<String> secondList) {
        boolean flag = false;
        if (firstList.size() > secondList.size()) {
            System.out
                    .println("There are more paragraph in Expected document than in Actual document");
        } else if (firstList.size() < secondList.size()) {
            System.out
                    .println("There are more paragraph in Actual document than in Expected document");
        } else {
            flag = true;
        }
        return flag;
    }

    @Test
    void shouldGenerateAvis() throws Exception {
        //Given

        File file = new File("src/test/resources/generator/avis-marche.docx");
        File json = new File("src/test/resources/generator/concentrateur.json");
        FileInputStream input = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        List<KeyValueRequest> keyValues = List.of(objectMapper.readValue(json, KeyValueRequest[].class));

        FileGeneratorRequest request = FileGeneratorRequest.builder()
                .keyValues(keyValues.stream()
                        .filter(keyValueRequest -> Objects.nonNull(keyValueRequest.getKey()) &&
                                Objects.nonNull(keyValueRequest.getValue()))
                        .collect(Collectors.toMap(KeyValueRequest::getKey, KeyValueRequest::getValue)))
                .stream(template.getInputStream())
                .extension("docx")
                .build();
        //When
        final ByteArrayOutputStream result = docxGeneratorService.generate(request, null, null);
        //Then
        assertNotNull(result);
        String name = "src/test/resources/generator/avis-marche.docx.generated.docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }

    }


}
