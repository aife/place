package com.atexo.docgen.core.domain.convertor.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConsultationConfig;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConvertResult;
import com.atexo.docgen.core.domain.convertor.services.IExcelConvertorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;



public class ExcelConvertorServiceImplTest extends PortMockBeanConfig {

    @Autowired
    private IExcelConvertorService iExcelConvertorService;

    private final String path = "src/test/resources/convertor/";

    @BeforeEach
    public void setUp() throws IOException {
        super.setUp();
    }

    @Test
    public void shouldConvertConsultationNonAllotieDocument() throws Exception {
        //Given
        String prefix = "-non-allotie";
        File fileTemplate = new File(path + "template-analyse-des-offres.xlsx");
        FileInputStream inputTemplate = new FileInputStream(fileTemplate);
        File src = new File(path + "/consultation" + prefix + ".json");
        Consultation consultation = objectMapper.readValue(src, Consultation.class);
        ConsultationConfig consultationConfig = objectMapper.readValue(src, ConsultationConfig.class);
        when(iAnalysePort.map(consultation)).thenReturn(consultationConfig);
        final ConvertResult result = iExcelConvertorService.convertAnalyse(inputTemplate, consultation);
        //Then
        assertEqualsExcelConvertor(prefix, fileTemplate, result);

    }

    @Test
    public void shouldConvertConsultationAllotieDocument() throws Exception {
        //Given
        String prefix = "-allotie";
        File fileTemplate = new File(path + "template-analyse-des-offres.xlsx");
        FileInputStream inputTemplate = new FileInputStream(fileTemplate);
        File src = new File(path + "/consultation" + prefix + ".json");
        Consultation consultation = objectMapper.readValue(src, Consultation.class);
        ConsultationConfig consultationConfig = objectMapper.readValue(src, ConsultationConfig.class);
        when(iAnalysePort.map(consultation)).thenReturn(consultationConfig);
        final ConvertResult result = iExcelConvertorService.convertAnalyse(inputTemplate, consultation);
        //Then
        assertEqualsExcelConvertor(prefix, fileTemplate, result);

    }

    @Test
    public void shouldConvertConsultationNonAllotieDocumentWithEmptySoumissionnaires() throws Exception {
        //Given
        String prefix = "-non-allotie-without-soumissionnaires";
        File fileTemplate = new File(path + "template-analyse-des-offres.xlsx");
        FileInputStream inputTemplate = new FileInputStream(fileTemplate);
        File src = new File(path + "/consultation" + prefix + ".json");
        Consultation consultation = objectMapper.readValue(src, Consultation.class);
        ConsultationConfig consultationConfig = objectMapper.readValue(src, ConsultationConfig.class);
        when(iAnalysePort.map(consultation)).thenReturn(consultationConfig);
        final ConvertResult result = iExcelConvertorService.convertAnalyse(inputTemplate, consultation);
        //Then
        assertEqualsExcelConvertor(prefix, fileTemplate, result);

    }

    @Test
    public void shouldConvertConsultationAllotieDocumentWithEmptySoumissionnaires() throws Exception {
        //Given
        String prefix = "-allotie-without-soumissionnaires";
        File fileTemplate = new File(path + "template-analyse-des-offres.xlsx");
        FileInputStream inputTemplate = new FileInputStream(fileTemplate);
        File src = new File(path + "/consultation" + prefix + ".json");
        Consultation consultation = objectMapper.readValue(src, Consultation.class);
        ConsultationConfig consultationConfig = objectMapper.readValue(src, ConsultationConfig.class);
        when(iAnalysePort.map(consultation)).thenReturn(consultationConfig);
        final ConvertResult result = iExcelConvertorService.convertAnalyse(inputTemplate, consultation);
        //Then
        assertEqualsExcelConvertor(prefix, fileTemplate, result);

    }

    private void assertEqualsExcelConvertor(String prefix, File fileTemplate, ConvertResult result) throws IOException {
        assertNotNull(result);
        final String s = fileTemplate.getName() + prefix;
        String name = path + s + "-generated.xlsx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.getConvertedDocument().writeTo(outputStream);
        }
        String nameJson = path + fileTemplate.getName() + prefix + "-generated.json";
        try (OutputStream outputStream = new FileOutputStream(nameJson)) {
            outputStream.write(objectMapper.writeValueAsString(result.getConfiguration()).getBytes(StandardCharsets.UTF_8));
        }

        String expectednameJson = path + fileTemplate.getName() + prefix + "-expected.json";
        assertEquals(objectMapper.readTree(new File(expectednameJson)), objectMapper.readTree(objectMapper.writeValueAsString(result.getConfiguration())));
    }


    public static void main(String[] args) throws Exception {
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");

        engine.eval("value = 10");
        Boolean greaterThan5 = (Boolean) engine.eval("value > 5");
        Boolean lessThan5 = (Boolean) engine.eval("value < 5");

        System.out.println("10 > 5? " + greaterThan5); // true
        System.out.println("10 < 5? " + lessThan5); // false
    }

}
