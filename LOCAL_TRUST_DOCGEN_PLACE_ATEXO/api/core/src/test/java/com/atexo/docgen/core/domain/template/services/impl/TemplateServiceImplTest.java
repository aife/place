package com.atexo.docgen.core.domain.template.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.template.services.ITemplateService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.junit.Assert.assertNotNull;


public class TemplateServiceImplTest extends PortMockBeanConfig {

	@Autowired
	private ITemplateService iTemplateService;

	private final String path = "src/test/resources/template/";

	@Test
	public void shouldCreateTemplateWithStyle() throws Exception {
		//Given
		final ByteArrayOutputStream result = iTemplateService.applyStyle(null, null);
		assertNotNull(result);
		String name = path + "default-template.docx";
		try (OutputStream outputStream = new FileOutputStream(name)) {
			result.writeTo(outputStream);
		}


	}

	@Test
	public void shouldApplyDefaultTemplateToFile() throws Exception {
		//Given

		FileInputStream destTemplate = new FileInputStream(path + "dest-template.docx");
		final ByteArrayOutputStream result = iTemplateService.applyStyle(null, destTemplate);
		assertNotNull(result);
		String name = path + "dest-template_updated.docx";
		try (OutputStream outputStream = new FileOutputStream(name)) {
			result.writeTo(outputStream);
		}


	}

	@Test
	public void shouldApplySpecificTemplateToFile() throws Exception {
		//Given

		FileInputStream baseTemplate = new FileInputStream(path + "base-template.docx");
		FileInputStream destTemplate = new FileInputStream(path + "dest-template.docx");
		final ByteArrayOutputStream result = iTemplateService.applyStyle(baseTemplate, destTemplate);
		assertNotNull(result);
		String name = path + "dest-template_updated.docx";
		try (OutputStream outputStream = new FileOutputStream(name)) {
			result.writeTo(outputStream);
		}


	}


}
