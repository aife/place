package com.atexo.docgen.core.domain.scheduler.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.scheduler.services.DocumentSchedulerService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNull;


public class DocumentSchedulerServiceTest extends PortMockBeanConfig {

    @Autowired
    private DocumentSchedulerService documentSchedulerService;


    @Test
    @Disabled
    public void shouldUpdateDocumentsStatus() {
        //Given
        Exception exception = null;
        //When
        try {

            documentSchedulerService.updateDocumentsStatus();
        } catch (Exception e) {
            exception = e;
        }
        //Then
        assertNull(exception);
    }


    @Test
    @Disabled
    public void shouldDeleteFiles() {
        //Given
        Exception exception = null;
        //When
        try {
            documentSchedulerService.deleteFiles();
        } catch (IOException e) {
            exception = e;
        }
        //Then
        assertNull(exception);
    }


    @Test
    @Disabled
    public void shouldDeleteDocument() {
        //Given
        Exception exception = null;
        //When
        try {
            documentSchedulerService.deleteDocument();
        } catch (IOException e) {
            exception = e;
        }
        //Then
        assertNull(exception);
    }


}
