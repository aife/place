package com.atexo.docgen.core.utilitaire;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class XlsxConverterUtilsTest extends PortMockBeanConfig {

    List<MultipartFile> xlsxFiles;

    static String FILENAME1 = "Annexe Financière DPGF - BPU - DQE - Modèle ATEXO v7 2020 10 28(1).xlsx";

    @BeforeEach
    public void setUp() throws IOException {
        File file = new File("src/test/resources/extractor/" + FILENAME1);
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        xlsxFiles = Collections.singletonList(multipartFile);
    }

    @Test
    public void importOnlyAllXlsxFiles() {
       List<XSSFWorkbook> result = XlsxConverterUtils.readXlsxFiles(xlsxFiles);
        assertNotNull(result);
        assertNotNull(result.get(0));
    }

}
