package com.atexo.docgen.core.config;

import com.atexo.docgen.core.port.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.getObjectMapper;


@SpringBootTest(classes = CoreScanConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PortMockBeanConfig {

    @MockBean
    protected IDocumentPort iDocumentPort;
    @MockBean
    protected IChampFusionRacourciPort iChampFusionRacourciPort;
    @MockBean
    protected IDocumentXmlPort iDocumentXmlPort;
    @MockBean
    protected RestTemplate atexoRestTemplate;
    @MockBean
    protected IOnlyOfficePort iOnlyOfficePort;

    @MockBean
    protected IUserPort iUserPort;
    @MockBean
    protected IAnalysePort iAnalysePort;
    @MockBean
    protected AuthenticationManager authenticationManager;
    protected final ObjectMapper objectMapper = getObjectMapper();

    @BeforeEach
    public void setUp() throws IOException {

    }
}
