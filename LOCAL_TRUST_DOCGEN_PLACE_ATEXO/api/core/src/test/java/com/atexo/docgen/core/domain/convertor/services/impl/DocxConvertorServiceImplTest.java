package com.atexo.docgen.core.domain.convertor.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.security.model.AtexoUserDetails;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.io.*;
import java.nio.file.Files;
import java.util.Collections;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.sanitizeHtmlInput;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


public class DocxConvertorServiceImplTest extends PortMockBeanConfig {

    @Autowired
    private IDocumentConvertorService iDocumentConvertorService;

    private final String path = "src/test/resources/convertor/";

    @Test
    public void shouldConvertDocument() throws Exception {
        //Given
        File fileXml = new File(path + "output.xml");
        //File fileXml = new File(path + "test.xml");
        FileInputStream inputXml = new FileInputStream(fileXml);

        //File fileTemplate = new File(path + "test-rme.docx");
        File fileTemplate = new File(path + "new-gabarit.docx");
        //File fileTemplate = new File(path + "RC.docx");
        //File fileTemplate = new File(path + "RC-TEMPLATE.docx");
        FileInputStream inputTemplate = new FileInputStream(fileTemplate);

        //When
        DocxDocument document = iDocumentConvertorService.getDocxDocument(inputXml);

        final ByteArrayOutputStream result = iDocumentConvertorService.convert(inputTemplate, document);
        //Then
        assertNotNull(result);
        String name = path + fileXml.getName() + ".docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }


    }

    @Test
    public void shouldConvertDocumentWithPuce() throws Exception {
        //Given
        File fileXml = new File(path + "puce.xml");
        //File fileXml = new File(path + "test.xml");
        FileInputStream inputXml = new FileInputStream(fileXml);

        //File fileTemplate = new File(path + "test-rme.docx");
        File fileTemplate = new File(path + "RC-TEMPLATE/result.docx");
        //File fileTemplate = new File(path + "RC.docx");
        //File fileTemplate = new File(path + "RC-TEMPLATE.docx");
        FileInputStream inputTemplate = new FileInputStream(fileTemplate);

        //When
        DocxDocument document = iDocumentConvertorService.getDocxDocument(inputXml);

        final ByteArrayOutputStream result = iDocumentConvertorService.convert(inputTemplate, document);
        //Then
        assertNotNull(result);
        String name = path + fileXml.getName() + ".docx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }


    }

    @Test
    public void shouldSanitizeInputStream() throws Exception {
        //Given
        File fileXml = new File(path + "CCAP.xml");
        //File fileXml = new File(path + "test.xml");
        FileInputStream inputXml = new FileInputStream(fileXml);
        DocxDocument document = iDocumentConvertorService.getDocxDocument(inputXml);

        final DocxDocument result = iDocumentConvertorService.sanitizeDocxDocument(document);
        //Then
        assertNotNull(result);
        String name = path + fileXml.getName() + ".xml";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            iDocumentConvertorService.getXml(result).writeTo(outputStream);
        }


    }

    @Test
    public void shouldFinalizeDocumentXlsx() throws Exception {
        String originalType = "docx";
        String output = "docx";
        //Given
        String nameWithoutExtension = "output";
        //String nameWithoutExtension = "file2";
        File fileXml = new File(path + nameWithoutExtension + "." + originalType);
        FileInputStream inputXml = new FileInputStream(fileXml);
        //When

        final ByteArrayOutputStream result = iDocumentConvertorService.finalizeDocument(inputXml, originalType, output);
        //Then
        assertNotNull(result);
        String name = path  + nameWithoutExtension + "-without-mark-generated." + output;
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }


    }

    @Test
    @Disabled
    //TODO Generalize this test : Mock
    public void shouldFinalizeDocumentPdf() throws Exception {
        String originalType = "docx";
        String output = "pdf";
        //Given
        String nameWithoutExtension = "2020_VM_040_482.xml";
        //String nameWithoutExtension = "file2";
        File fileXml = new File(path + nameWithoutExtension + "." + originalType);
        FileInputStream inputXml = new FileInputStream(fileXml);
        AtexoUserDetails atexoUserDetails = new AtexoUserDetails(1, "Test", "Test", "Test", Collections.emptyList());
        Authentication authentication = new UsernamePasswordAuthenticationToken(atexoUserDetails, atexoUserDetails.getPassword(), atexoUserDetails.getAuthorities());
        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        //When
        final ByteArrayOutputStream result = iDocumentConvertorService.finalizeDocument(inputXml, originalType, output);
        //Then
        assertNotNull(result);
        String name = path + "final_" + nameWithoutExtension + "." + output;
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }


    }

    @Test
    void shouldGetTemplate() throws Exception {

        when(atexoRestTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenAnswer(invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            String[] split = ((String) args[0]).split("/");
            if (split.length < 6) return ResponseEntity.ok(new FileSystemResource(path + "bloc-template.docx"));
            String s = split[5];
            switch (s) {
                case "VARIANTE-EXIGEE":
                    Resource docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/varianteExigee.docx");
                    return ResponseEntity.ok(docxResource);
                case "VARIANTE-AUTORISEE":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/varianteAutorisee.docx");
                    return ResponseEntity.ok(docxResource);
                case "TRANCHES":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/tranches.docx");
                    return ResponseEntity.ok(docxResource);
                case "RECONDUCTION":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/reconduction.docx");
                    return ResponseEntity.ok(docxResource);
                case "PRESTATIONS-SUPPLEMENTAIRES":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/prestations.docx");
                    return ResponseEntity.ok(docxResource);
                case "NEGOCIATION":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/negociation.docx");
                    return ResponseEntity.ok(docxResource);
                case "NB-CANDIDATS":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/nombre_candidats.docx");
                    return ResponseEntity.ok(docxResource);
                case "FORME-PRIX":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/formePrix.docx");
                    return ResponseEntity.ok(docxResource);
                case "FICHE-CANDIDAT":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/ficheCandidat.docx");
                    return ResponseEntity.ok(docxResource);
                case "DELAI-VALIDITE":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/delaiValidite.docx");
                    return ResponseEntity.ok(docxResource);
                case "DUREE-MARCHE":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/dureeMarche.docx");
                    return ResponseEntity.ok(docxResource);
                case "CRITERES-ATTRIBUTION":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/criteresAttribution.docx");
                    return ResponseEntity.ok(docxResource);
                 case "CCAG":
                    docxResource = new FileSystemResource(path + "champs-fusions-complexes/mpe/ccag.docx");
                    return ResponseEntity.ok(docxResource);
                default:
                    docxResource = new FileSystemResource(path + "bloc-template.docx");
                    return ResponseEntity.ok(docxResource);

            }

        });
        File docAdministrable = new File(path + "template.docx");
        try {
            // Lire le contenu du fichier dans un tableau d'octets (byte array)
            byte[] fileContent = readBytesFromFile(docAdministrable);

            // Créer un ByteArrayResource à partir du contenu du fichier
            ByteArrayResource byteArrayResource = new ByteArrayResource(fileContent);

            final ByteArrayOutputStream result = iDocumentConvertorService.getTemplate(byteArrayResource.getInputStream(), DocumentEditorDetails.builder().build().getHeadersCallback());
            //Then

            String name = path + docAdministrable.getName() + "-generated.docx";
            try (OutputStream outputStream = new FileOutputStream(name)) {
                result.writeTo(outputStream);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test
    void shouldSanitizeHtml() {
        String untrustedHTML = "<p>test</p><script>alert('test')</script>";
        String result = sanitizeHtmlInput(untrustedHTML);
        assertNotNull(result);
        assertThat(result).isEqualTo("<p>test</p>");
    }

    @Test
    void shouldSanitizeHtmlFichier() throws IOException {
        File fileXml = new File(path + "test.html");
        String untrustedHTML = Files.readString(fileXml.toPath());
        String result = sanitizeHtmlInput(untrustedHTML);
        Assertions.assertNotNull(result);
        String name = path + fileXml.getName() + "-generated.html";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            outputStream.write(result.getBytes());
        }
    }

    @Test
    void shouldSanitizeJson() {
        String untrustedHTML = "test <p>test</p><script>alert('test')</script> jjfhh";
        String result = sanitizeHtmlInput(untrustedHTML);
        assertNotNull(result);
        assertThat(result).isEqualTo("test <p>test</p> jjfhh");
    }

    private static byte[] readBytesFromFile(File file) throws IOException {
        try (InputStream inputStream = new FileInputStream(file)) {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteStream.write(buffer, 0, bytesRead);
            }

            return byteStream.toByteArray();
        }
    }
}
