package com.atexo.docgen.core.domain.editor.services.impl;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.editor.model.*;
import com.atexo.docgen.core.domain.editor.services.IDocumentEditorService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.impl.JwtServiceImpl;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocumentNotExistantException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


public class DocumentEditorServiceImplTest extends PortMockBeanConfig {

    @Autowired
    private IDocumentEditorService iDocumentEditorService;

    @Autowired
    private JwtServiceImpl jwtServiceImpl;

    @Autowired
    private IDocumentConfigurationService iDocumentConfigurationService;
    @Value("${docgen.public-url}")
    private String publicUrl;
    @Value("${docgen.context-path}")
    private String contextePath;


    @Test
    public void shouldReturnMauvaisParametreExceptionWhenTokenIsNull() throws Exception {
        MauvaisParametreException exception =
                assertThrows(MauvaisParametreException.class, () -> iDocumentEditorService.getFileModel(null));
        assertEquals("Id ne doit pas être null", exception.getMessage());
        assertEquals(TypeExceptionEnum.EDITOR, exception.getType());
        verify(iDocumentPort, times(0)).getById(anyString());

    }


    @Test
    public void shouldReturnMauvaisParametreExceptionWhenDocumentNotFound() throws Exception {
        //Given
        String id = "id";
        String plateformeId = "plateformeId";
        String token = jwtServiceImpl.generateJwtToken("test_key", DocumentEditorRequest.builder()
                .user(User.builder().id("id").name("name").build())
                .callback("http://callback.com")
                .mode("edit")
                .build(), plateformeId, id);
        DocumentEditorDetails details = jwtServiceImpl.getEditorDetailsFromToken(token);
        when(iDocumentPort.getById(anyString())).thenReturn(null);
        DocumentNotExistantException exception =
                assertThrows(DocumentNotExistantException.class, () -> iDocumentEditorService.getFileModel(details));
        assertEquals("L'id " + id + " ne correspond pas à un document enregistré", exception.getMessage());
        assertEquals(TypeExceptionEnum.EDITOR, exception.getType());
        verify(iDocumentPort, times(1)).getById(anyString());

    }


    @Test
    public void shouldReturnFileModel() {
        //Given
        String id = "id";
        String plateformeId = "plateformeId";
        String token = jwtServiceImpl.generateJwtToken("test", DocumentEditorRequest.builder()
                .user(User.builder().id("id").name("name").build())
                .callback("http://callback.com")
                .mode("edit")
                .build(), plateformeId, id);
        DocumentModel documentModel = DocumentModel.builder()
                .status("status")
                .permissions(PermissionsModel.builder().review(true).comment(true).download(true).edit(true)
                        .fillForms(true).modifyContentControl(true).modifyFilter(true).build())
                .fileType("fileType")
                .key("key")
                .id(id)
                .title("test.docx")
                .build();
        DocumentEditorDetails details = jwtServiceImpl.getEditorDetailsFromToken(token);

        when(iDocumentPort.getById(anyString())).thenReturn(documentModel);
        //When
        final FileModel result = iDocumentEditorService.getFileModel(details);
        //Then
        assertNotNull(result);
        this.assertFileModelEquals(result, details, documentModel, publicUrl + contextePath);
        verify(iDocumentPort, times(1)).getById(anyString());
    }

    @Test
    public void shouldHideXlsx() {
        iDocumentEditorService.setXlsxFileWithConfiguration(new File("src/test/resources/convertor/template-analyse-des-offres.xlsx"),
                DocumentConfiguration.builder().defaultSheetName("Aide").activatedSheetNames(Arrays.asList("Aide","Analyse_lot")).build());
    }


    private void assertFileModelEquals(FileModel actual, DocumentEditorDetails editorDetails, DocumentModel documentModel, String serverUrl) {
        assertEquals(editorDetails.getMode(), actual.getMode());
        assertEquals("desktop", actual.getType());
        assertDocumentEquals(documentModel, actual.getDocument());
        assertEquals(actual.getDocument().getUrl(), serverUrl, documentModel.getId());
        assertEditorConfigEquals(editorDetails, actual.getEditorConfig());
    }

    private void assertEditorConfigEquals(DocumentEditorDetails editorDetails, EditorConfig actual) {
        assertEquals(editorDetails.getMode(), actual.getMode());
        assertEquals(editorDetails.getUser().getId(), actual.getUser().getId());
        assertEquals(editorDetails.getUser().getName(), actual.getUser().getName());
        assertEquals(editorDetails.getUser().getRoles(), actual.getUser().getRoles());

    }

    private void assertDocumentEquals(DocumentModel expected, DocumentModel actual) {
        assertEquals(expected.getFileType(), actual.getFileType());
        assertEquals(expected.getKey(), actual.getKey());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getTitle(), actual.getTitle());
        assertPermissionsEquals(expected.getPermissions(), actual.getPermissions());

    }

    private void assertPermissionsEquals(PermissionsModel expected, PermissionsModel actual) {
        assertEquals(expected.getComment(), actual.getComment());
        assertEquals(expected.getDownload(), actual.getDownload());
        assertEquals(expected.getEdit(), actual.getEdit());
        assertEquals(expected.getFillForms(), actual.getFillForms());
        assertEquals(expected.getModifyContentControl(), actual.getModifyContentControl());
        assertEquals(expected.getReview(), actual.getReview());
        assertEquals(expected.getModifyFilter(), actual.getModifyFilter());
    }


}
