package com.atexo.docgen.core.domain.convertor.services.impl;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;

public class ConditionalFormattingBeginsWith {

	public static void main(String[] args) throws Exception {
		Workbook workbook = new XSSFWorkbook();
		//Workbook workbook = new HSSFWorkbook();

		Sheet sheet = workbook.createSheet("new sheet");
		SheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();

		String text = "bla";

		ConditionalFormattingRule rule = sheetCF.createConditionalFormattingRule(
				"(ISEQUAL(" + text + ")");

		PatternFormatting fill = rule.createPatternFormatting();
		fill.setFillBackgroundColor(IndexedColors.YELLOW.index);
		fill.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule[] cfRules = new ConditionalFormattingRule[]{rule};

		CellRangeAddress[] regions = new CellRangeAddress[]{CellRangeAddress.valueOf("A1:B10")};

		sheetCF.addConditionalFormatting(regions, cfRules);

		if (workbook instanceof XSSFWorkbook) {
			workbook.write(new FileOutputStream("ConditionalFormattingBeginsWith.xlsx"));
		} else if (workbook instanceof HSSFWorkbook) {
			workbook.write(new FileOutputStream("ConditionalFormattingBeginsWith.xls"));
		}
		workbook.close();

	}
}
