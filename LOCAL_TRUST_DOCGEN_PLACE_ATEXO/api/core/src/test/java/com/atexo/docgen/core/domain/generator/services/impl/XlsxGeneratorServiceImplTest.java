package com.atexo.docgen.core.domain.generator.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.model.ObjectTestModel;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class XlsxGeneratorServiceImplTest extends PortMockBeanConfig {

    @Autowired
    private XlsxGeneratorServiceImpl xlsxGeneratorService;

    private FileGeneratorRequest fileGeneratorRequest;
    private final String path = "src/test/resources/";

    @BeforeEach
    public void setUp() throws IOException {
        File file = new File("src/test/resources/test-template.xlsx");
        FileInputStream input = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        HashMap<String, Object> keyValues = new HashMap<>();
        keyValues.put("NUMERO_CONSULTATION", "test");
        keyValues.put("objectTest",
                ObjectTestModel.builder().simpleString("simpleString")
                        .subObject(ObjectTestModel.SubObject.builder().simpleString("simpleTest")
                                .build()).subObjects(List.of(ObjectTestModel.SubObject.builder().simpleString("simpleTest2").build(), ObjectTestModel.SubObject.builder().simpleString("2test")
                                .build()))
                        .build()
        );
        fileGeneratorRequest = FileGeneratorRequest.builder()
                .keyValues(keyValues)
                .stream(template.getInputStream())
                .extension("xlsx")
                .build();
    }

    @Test
    public void shouldReturnListeDesVariables() throws Exception {
        //Given
        InputStream request = fileGeneratorRequest.getStream();
        //When
        final Map<String, Set<String>> result = xlsxGeneratorService.getFileVariable(request);
        //Then
        assertNotNull(result);
        assertEquals(1, result.size());

        assertEquals(result.get("SIMPLE"), new HashSet<>(Arrays.asList(
                "NUMERO_CONSULTATION", "DIRECTION_SERVICE", "INTITULE_CONSULTATION", "objectTest.simpleString", "PROCEDURE_CONSULTATION",
                "objectTest.subObjects(1).simpleString", "objectTest.absent", "TYPE_DOCUMENT", "POUVOIR_ADJUDICACTEUR", "objectTest.subObjects(0).simpleString")));
    }

    @Test
    public void shouldGenerateDocument() throws Exception {
        //Given
        FileGeneratorRequest request = fileGeneratorRequest;
        //When
        final ByteArrayOutputStream result = xlsxGeneratorService.generate(request, null, null);
        //Then
        assertNotNull(result);
        String name = path + "generated-test.xlsx";
        try (OutputStream outputStream = new FileOutputStream(name)) {
            result.writeTo(outputStream);
        }
        File actualFile = new File(name);
        FileInputStream input = new FileInputStream(actualFile);
        MultipartFile template = new MockMultipartFile("template",
                actualFile.getName(), "text/plain", IOUtils.toByteArray(input));
        //When
        final Map<String, Set<String>> variables = xlsxGeneratorService.getFileVariable(template.getInputStream());
        //Then
        assertNotNull(variables);
        assertEquals(1, variables.size());
        assertEquals(new HashSet<>(), variables.get("SIMPLE"));

        File expectedFile = new File(path + "expected/expected-test.xlsx");
        assertXlsxEquals(actualFile, expectedFile);
        Files.delete(actualFile.toPath());
    }

    public static void assertXlsxEquals(File actualFile, File expectedFile) throws IOException {
        XSSFWorkbook actualWorkbook = new XSSFWorkbook(new FileInputStream(actualFile));
        XSSFWorkbook expectedWorkbook = new XSSFWorkbook(new FileInputStream(expectedFile));
        assertEquals(expectedWorkbook.getNumberOfSheets(), actualWorkbook.getNumberOfSheets());
        for (int i = 0; i < expectedWorkbook.getNumberOfSheets(); i++) {
            XSSFSheet expectedSheet = expectedWorkbook.getSheetAt(i);
            XSSFSheet actualSheet = actualWorkbook.getSheetAt(i);
            int lastRow = expectedSheet.getLastRowNum();
            int firstRow = expectedSheet.getFirstRowNum();

            for (int rowIndex = firstRow; rowIndex < lastRow; rowIndex++) {
                XSSFRow expectedRow = expectedSheet.getRow(rowIndex);
                XSSFRow actualRow = actualSheet.getRow(rowIndex);
                if (expectedRow == null) {
                    assertNull(expectedRow);
                    assertNull(actualRow);
                    continue;
                }
                int firstColumns = expectedRow.getFirstCellNum();
                int lastColumns = expectedRow.getLastCellNum();

                for (int columnIndex = firstColumns; columnIndex < lastColumns; columnIndex++) {
                    XSSFCell expectedCell = expectedRow.getCell(columnIndex);
                    XSSFCell actualCell = actualRow.getCell(columnIndex);
                    if (expectedCell == null || !expectedCell.getCellType().equals(CellType.STRING)) {
                        if (expectedCell == null) {
                            assertNull(expectedCell);
                            assertNull(actualCell);
                        } else {
                            assertEquals(expectedCell.getCellType(), actualCell.getCellType());
                        }
                        continue;
                    }
                    FormulaEvaluator actualEvaluator = actualWorkbook.getCreationHelper().createFormulaEvaluator();
                    FormulaEvaluator expectedEvaluator = expectedWorkbook.getCreationHelper().createFormulaEvaluator();

                    String expectedContent = expectedEvaluator.evaluate(expectedCell).formatAsString();
                    String actualContent = actualEvaluator.evaluate(actualCell).formatAsString();
                    assertEquals(expectedContent, actualContent);

                }
            }
        }
    }

}
