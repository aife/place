package com.atexo.docgen.core.domain.generator.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.services.IDocumentGeneratorService;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class DocumentGeneratorFactoryTest extends PortMockBeanConfig {

    @Autowired
    private DocumentGeneratorFactory documentGeneratorFactory;

    private FileGeneratorRequest fileGeneratorRequest;

    @BeforeEach
    public void setUp() throws IOException {
        File file = new File("src/test/resources/test-template.docx");
        FileInputStream input = new FileInputStream(file);
        MultipartFile template = new MockMultipartFile("template",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        HashMap<String, Object> keyValues = new HashMap<>();
        keyValues.put("POUVOIR_ADJUDICACTEUR", "test");
        fileGeneratorRequest = FileGeneratorRequest.builder()
                .keyValues(keyValues)
                .stream(template.getInputStream())
                .extension("docx")
                .build();
    }

    @Test
    public void shouldReturnDocxInstantiation() {
        //Given
        String extension = "docx";
        //When
        IDocumentGeneratorService iDocumentGeneratorService = documentGeneratorFactory.getPrototype(extension);
        //Then
        assertTrue(iDocumentGeneratorService instanceof DocxGeneratorServiceImpl);
    }


    @Test
    public void shouldReturnXlsxInstantiation() {
        //Given
        String extension = "xlsx";
        //When
        IDocumentGeneratorService iDocumentGeneratorService = documentGeneratorFactory.getPrototype(extension);
        //Then
        assertTrue(iDocumentGeneratorService instanceof XlsxGeneratorServiceImpl);
    }


    @Test
    public void shouldTrowExtensionException() {
        //Given
        String extension = "test";
        //When => Then
        DocumentExtensionNotSupportedException exception = assertThrows(DocumentExtensionNotSupportedException.class, () -> documentGeneratorFactory.getPrototype(extension));
        assertEquals("L'extension du document n'est pas supportée. Les extensions supportées sont .docx et .xlsx", exception.getMessage());
        assertEquals(TypeExceptionEnum.GENERATION, exception.getType());

    }

    @Test
    public void shouldThrowExceptionWhenFileRequestIsNull() throws Exception {
        //Given
        FileGeneratorRequest request = null;
        //When
        //When => Then
        MauvaisParametreException exception = assertThrows(MauvaisParametreException.class, () -> documentGeneratorFactory.generate(request, null, null));
        assertEquals("Le fichier ou la liste des valeurs ne doivent pas être null", exception.getMessage());
        assertEquals(TypeExceptionEnum.GENERATION, exception.getType());
    }

    @Test
    public void shouldThrowExceptionWhenVariableErreurTechnique() throws Exception {
        //Given
        MultipartFile request = mock(MultipartFile.class);
        when(request.getInputStream())
                .thenThrow(new IOException());
        //When => Then
        DocGenException exception = assertThrows(DocGenException.class, () -> documentGeneratorFactory.getFileVariable("docx", request));
        assertNull(exception.getMessage());
        assertEquals(TypeExceptionEnum.CHAMPS_FUSION, exception.getType());
    }

    @Test
    public void shouldThrowExceptionDocInstantiation() throws Exception {
        //Given
        MultipartFile request = null;
        //When
        //When => Then
        MauvaisParametreException exception = assertThrows(MauvaisParametreException.class, () -> documentGeneratorFactory.getFileVariable("docx", request));
        assertEquals("Le fichier ne doit pas être null", exception.getMessage());
        assertEquals(TypeExceptionEnum.CHAMPS_FUSION, exception.getType());
    }
}
