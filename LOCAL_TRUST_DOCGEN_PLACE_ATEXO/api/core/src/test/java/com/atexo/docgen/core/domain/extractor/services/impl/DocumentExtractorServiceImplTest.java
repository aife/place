package com.atexo.docgen.core.domain.extractor.services.impl;

import com.atexo.docgen.core.config.PortMockBeanConfig;
import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.extractor.model.ExcelConfiguration;
import com.atexo.docgen.core.domain.extractor.model.PositionToExtract;
import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import com.atexo.docgen.core.domain.extractor.model.StaticData;
import com.atexo.docgen.core.domain.extractor.model.analyse.AnalyseOffre;
import com.atexo.docgen.core.domain.extractor.services.IDocumentExtractorService;
import com.atexo.docgen.core.domain.generator.services.impl.XlsxGeneratorServiceImplTest;
import junit.framework.TestCase;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class DocumentExtractorServiceImplTest extends PortMockBeanConfig {
	File template;
	List<MultipartFile> templates;
	private ExcelConfiguration configuration = ExcelConfiguration.builder().build();
	private File expectedFile;
	@Autowired
	IDocumentExtractorService documentExtractorService;

	private final String path = "src/test/resources/extractor/";

	@BeforeEach
	public void setUp() throws IOException {
		super.setUp();
		File file = new File(path, "Annexe Financière DPGF - BPU - DQE - Modèle ATEXO v7 2020 10 28(1).xlsx");
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile("template",
				file.getName(), "text/plain", IOUtils.toByteArray(input));

		File file1 = new File(path, "file2.xlsx");
		FileInputStream input1 = new FileInputStream(file1);
		MultipartFile multipartFile1 = new MockMultipartFile("template",
				"test-fichier2.xlsx", "text/plain", IOUtils.toByteArray(input1));

		template = new File(path, "Tableau comparatif des Annexes Financières - Modèle ATEXO v7 2020 10 28.xlsx");


		expectedFile = new File(path, "expected-test-result.xlsx");

		//soumissionaire
		List<PositionToExtract> positions = new ArrayList<>();
		positions.add(PositionToExtract.builder()
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(9)
						.build())
				.position(PositionValue.builder()
						.column("B")
						.row(8)
						.build())
				.sheetName("Synthèse").build());
		//Montant du forfait EUR HT
		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("C")
						.row(20)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(10)
						.build())
				.sheetName("Synthèse").build());

		//Montant du scénario de commandes EUR HT
		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("E")
						.row(20)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(11)
						.build())
				.sheetName("Synthèse").build());

		//Montant du scénario de commandes EUR TTC
		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("C")
						.row(21)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(12)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("E")
						.row(21)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(13)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("C")
						.row(22)
						.build())

				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(14)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("D")
						.row(22)
						.build())

				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(15)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("E")
						.row(22)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(16)
						.build())
				.sheetName("Synthèse").build());
		configuration.setPositions(positions);
		List<StaticData> staticData = new ArrayList<>();
		String pattern = "dd/MM/yyyy";

		DateFormat df = new SimpleDateFormat(pattern);

		Date date1 = Date.from(LocalDateTime.of(2020, 12, 2, 12, 45, 20).toInstant(ZoneOffset.UTC));
		Date date2 = Date.from(LocalDateTime.of(2020, 11, 2, 12, 45, 20).toInstant(ZoneOffset.UTC));

		String dateString1 = df.format(date1);
		String dateString2 = df.format(date2);
		staticData.add(StaticData.builder()
				.data(Arrays.asList(dateString1, dateString2))
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(8)
						.build())
				.build());
		configuration.setStaticData(staticData);
		templates = Arrays.asList(multipartFile, multipartFile1);
	}

	@Test
	public void shouldGenerateDocumentCorrectly() throws IOException {
		ByteArrayResource templateResources = new ByteArrayResource(Files.readAllBytes(template.toPath()));
		ByteArrayOutputStream result = documentExtractorService.extract(templates, templateResources, configuration);
		FileOutputStream fos = new FileOutputStream(new File("test.xlsx"));
		fos.write(result.toByteArray());
		File testResult = new File("test.xlsx");
		assertNotNull(testResult);
		assertTrue(testResult.exists());

		// process for checking all values
		XlsxGeneratorServiceImplTest.assertXlsxEquals(expectedFile, testResult);

		// delete of result files
		testResult.delete();
	}

	@Test
	public void shouldGenerateDocumentNotCorrectly() throws IOException {
		ByteArrayResource templateResources = new ByteArrayResource(Files.readAllBytes(template.toPath()));
		File file = new File(path, "test.docx");
		FileInputStream input = new FileInputStream(file);
		MultipartFile template3 = new MockMultipartFile("template",
				file.getName(), "text/plain", IOUtils.toByteArray(input));
		try {
			documentExtractorService.extract(Collections.singletonList(template3), templateResources, configuration);
			TestCase.fail();
		} catch (Exception e) {
			// Le cas d'erreur est bien traité
		}

		MultipartFile nullMultiPartFile = null;

		try {
			documentExtractorService.extract(Arrays.asList(null), templateResources, configuration);
			TestCase.fail();
		} catch (Exception e) {
			// Le cas d'erreur est bien traité
		}
	}

	@Test
	public void shouldExtractAnalyseConsultationAllotie() throws IOException {

		String prefix = "-allotie";
		File file = new File(path, "analyse" + prefix + ".xlsx");
		FileInputStream input = new FileInputStream(file);

		Configuration configuration = objectMapper.readValue(new File(path + "configuration" + prefix + ".json"), Configuration.class);

		AnalyseOffre result = documentExtractorService.extractAnalyseStatus(input, configuration);
		assertNotNull(result);

		assertEqualsAnalyse(file, result);

	}

	@Test
	public void shouldExtractAnalyseConsultationNonAllotie() throws IOException {
		String prefix = "-non-allotie";
		File file = new File(path, "analyse" + prefix + ".xlsx");
		FileInputStream input = new FileInputStream(file);

		Configuration configuration = objectMapper.readValue(new File(path + "configuration" + prefix + ".json"), Configuration.class);

		AnalyseOffre result = documentExtractorService.extractAnalyseStatus(input, configuration);
		assertNotNull(result);


		assertEqualsAnalyse(file, result);

	}


	@Test
	public void shouldExtractAnalyseConsultationAllotieWithoutSoumissionnaire() throws IOException {

		String prefix = "-allotie-without-soumissionnaires";
		File file = new File(path, "analyse" + prefix + ".xlsx");
		FileInputStream input = new FileInputStream(file);

		Configuration configuration = objectMapper.readValue(new File(path + "configuration" + prefix + ".json"), Configuration.class);

		AnalyseOffre result = documentExtractorService.extractAnalyseStatus(input, configuration);
		assertNotNull(result);

		assertEqualsAnalyse(file, result);

	}


	@Test
	public void shouldExtractAnalyseConsultationNonAllotieWithoutSoumissionnaire() throws IOException {
		String prefix = "-non-allotie-without-soumissionnaires";
		File file = new File(path, "analyse" + prefix + ".xlsx");
		FileInputStream input = new FileInputStream(file);

		Configuration configuration = objectMapper.readValue(new File(path + "configuration" + prefix + ".json"), Configuration.class);

		AnalyseOffre result = documentExtractorService.extractAnalyseStatus(input, configuration);
		assertNotNull(result);

		assertEqualsAnalyse(file, result);

	}


	private void assertEqualsAnalyse(File file, AnalyseOffre result) throws IOException {
		String generatedNameJson = path + file.getName() + "-generated.json";
		try (OutputStream outputStream = new FileOutputStream(generatedNameJson)) {
			outputStream.write(objectMapper.writeValueAsString(result).getBytes(StandardCharsets.UTF_8));
		}

		String nameJson = path + file.getName() + ".expected.json";
		assertEquals(objectMapper.readTree(new File(nameJson)), objectMapper.readTree(objectMapper.writeValueAsString(result)));
	}

}
