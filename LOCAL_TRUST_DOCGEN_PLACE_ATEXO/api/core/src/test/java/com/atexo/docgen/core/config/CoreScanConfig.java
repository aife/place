package com.atexo.docgen.core.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ComponentScan(basePackages = {"com.atexo.docgen"})
@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration
public class CoreScanConfig {

}
