package org.apache.poi.ooxml;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

public final class XWPFDocumentUtils {
    private XWPFDocumentUtils() {

    }

    public static void deleteComments(XWPFDocument newDoc) {
        newDoc.getRelationParts().stream().filter(relationPart -> relationPart.getRelationship().getRelationshipType().equals("http://schemas.openxmlformats.org/officeDocument/2006/relationships/comments"))
                .forEach(relationPart -> newDoc.removeRelation(relationPart.getRelationship().getId()));
    }
}
