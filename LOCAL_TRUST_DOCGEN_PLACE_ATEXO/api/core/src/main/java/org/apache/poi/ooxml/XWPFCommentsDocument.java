package org.apache.poi.ooxml;


import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTComments;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CommentsDocument;

import javax.xml.namespace.QName;
import java.io.IOException;
import java.io.OutputStream;

import static org.apache.poi.ooxml.POIXMLTypeLoader.DEFAULT_XML_OPTIONS;

//a wrapper class for the CommentsDocument /word/comments.xml in the *.docx ZIP archive
public class XWPFCommentsDocument extends POIXMLDocumentPart {

    private CTComments comments;

    public XWPFCommentsDocument(PackagePart part) {
        super(part);
        try {
            comments = CommentsDocument.Factory.parse(part.getInputStream(), DEFAULT_XML_OPTIONS).getComments();
        } catch (XmlException | IOException e) {
            comments = CommentsDocument.Factory.newInstance().addNewComments();
        }
    }

    public CTComments getComments() {
        return comments;
    }

    @Override
    protected void commit() throws IOException {
        XmlOptions xmlOptions = new XmlOptions(DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTComments.type.getName().getNamespaceURI(), "comments"));
        PackagePart part = super.getPackagePart();
        OutputStream out = part.getOutputStream();
        comments.save(out, xmlOptions);
        out.close();
    }

}
