package com.atexo.docgen.core.port;

import com.atexo.docgen.core.domain.security.model.AtexoUserDetails;
import com.atexo.docgen.core.domain.security.model.MessageResponse;
import com.atexo.docgen.core.domain.security.model.SignupRequest;

public interface IUserPort {


    AtexoUserDetails findByUsername(String username);

    MessageResponse registerUser(SignupRequest admin);
}
