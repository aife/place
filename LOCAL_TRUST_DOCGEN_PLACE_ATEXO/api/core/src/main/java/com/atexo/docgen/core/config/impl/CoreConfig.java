package com.atexo.docgen.core.config.impl;

import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import com.atexo.docgen.core.domain.convertor.services.impl.html.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.getObjectMapper;

@Configuration
public class CoreConfig {
    @Bean
    public ObjectMapper objectMapper() {
        return getObjectMapper();
    }

    @Bean
    public Map<String, TagFormatHandler> knownTags() {
        Map<String, TagFormatHandler> knownTags = new HashMap<>();
        knownTags.put("span", new SpanTagFormatHandler());
        knownTags.put("p", new PTagFormatHandler());
        knownTags.put("div", new DivTagFormatHandler());
        knownTags.put("br", new CarriageReturnTagFormatHandler());
        knownTags.put("li", new LiTagFormatHandler());
        knownTags.put("u", new UnderlineTagFormatHandler());
        knownTags.put("a", new ATagFormatHandler());
        knownTags.put("em", new GenericTagFormatHandler());
        knownTags.put("strong", new GenericTagFormatHandler());
        knownTags.put("b", new GenericTagFormatHandler());
        knownTags.put("i", new GenericTagFormatHandler());
        return knownTags;
    }

}
