package com.atexo.docgen.core.domain.extractor.model;

import lombok.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellReference;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExtractorBuilderModel {

    @NotEmpty
    private List<Cell> xssfCells = new ArrayList<>();
    @NotNull
    private CellReference cellReference;

}
