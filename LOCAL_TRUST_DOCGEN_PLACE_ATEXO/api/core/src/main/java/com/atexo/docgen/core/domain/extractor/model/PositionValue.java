package com.atexo.docgen.core.domain.extractor.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class PositionValue {
    @NotNull
    private String column;
    @NotNull
    private Integer row;

    public String getCellReference() {
        return column + row;
    }

    public String translateRowCellReference(int addRow) {
        return column + (row + addRow);
    }
}
