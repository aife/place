package com.atexo.docgen.core.domain.convertor.model.analyse;

import lombok.*;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCfvoType;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class XlsxColorCellConfig {
    private String value;
    private String color;
    private STCfvoType.Enum type = STCfvoType.NUM;
}
