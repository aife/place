package com.atexo.docgen.core.domain.extractor.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Feuille  {

    private String sheetName;

    private int sheetIndex;

    private boolean hidden;

}
