package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EditorConfig {
    private String mode;
    private String callbackUrl;
    private String lang;
    private User user;
    private Customization customization;
    private Embedded embedded;
    private Plugins plugins;

    private boolean chat;
    private boolean comments;
    private boolean leftMenu;
    private boolean rightMenu;
    private boolean toolbar;
    private boolean statusBar;


}
