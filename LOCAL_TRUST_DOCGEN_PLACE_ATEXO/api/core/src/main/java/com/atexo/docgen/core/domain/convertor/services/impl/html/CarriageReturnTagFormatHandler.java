package com.atexo.docgen.core.domain.convertor.services.impl.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;


public class CarriageReturnTagFormatHandler implements TagFormatHandler {

    @Override
    public TagFormat parse(XmlPullParser xpp) {
        return TagFormat.builder()
                .name(xpp.getName())
                .format("br")
                .build();
    }

    @Override
    public FormatedText closing(TagFormat tagFormat) {
        List<TagFormat> formatList = new ArrayList<>();
        formatList.add(tagFormat);
        return FormatedText.builder().format(formatList)
                .text(null)
                .isCarriageReturn(true).build();
    }
}
