package com.atexo.docgen.core.exceptions;

import com.atexo.docgen.core.enums.TypeExceptionEnum;

public class OnlyOfficeException extends RuntimeException {
    private static final long serialVersionUID = 100L;
    private final TypeExceptionEnum type;


    public OnlyOfficeException(TypeExceptionEnum type, String message) {
        super(message);
        this.type = type;
    }
    
    public TypeExceptionEnum getType() {
        return type;
    }
    
}
