

package com.atexo.docgen.core.config.impl;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentNotExistantException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class DocumentConfigurationServiceImpl implements IDocumentConfigurationService {

    @Value("${docgen.edited-docs}")
    private String editedExts;

    @Value("${docgen.convert-docs}")
    private String convertExts;
    @Value("${external-apis.onlyoffice.storage-folder}")
    private String storagePath;

    private final RestTemplate restTemplate;

    public DocumentConfigurationServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<String> getEditedExts() {
        return Arrays.asList(editedExts.split("\\|"));
    }

    @Override
    public List<String> getConvertExts() {
        return Arrays.asList(convertExts.split("\\|"));
    }


    @Override
    public String filesRootPath() {
        String directory = storagePath + File.separator;

        File file = new File(directory);

        if (!file.exists()) {
            file.mkdirs();
        }

        return directory;
    }

    @Override
    public String storagePath(String fileName) {
        String directory = filesRootPath();
        return directory + fileName;
    }

    @Override
    public String generateDocumentKey(String expectedKey) {
        log.info("Génération de la clé pour le document {}", expectedKey);
        expectedKey = Integer.toString(expectedKey.hashCode());

        String key = expectedKey.replace("[^0-9-.a-zA-Z_=]", "_");

        return key.substring(0, Math.min(key.length(), 20));
    }

    @Override
    public void downloadToFile(String fileUri, File file) {
        try {
            InputStream stream = getInputStreamFromUri(fileUri);
            DocumentUtils.saveFile(stream, file.getParent(), file.getName(), true);
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
            throw new DocumentNotExistantException(TypeExceptionEnum.ERREUR_FICHIER, e.getMessage());
        } catch (IOException e) {
            log.error("Erreur lors de la récupération du fichier depuis l'uri {}", fileUri);
            throw new DocGenException(TypeExceptionEnum.ERREUR_FICHIER, e.getMessage(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du fichier depuis l'uri {}", fileUri);
            throw new DocGenException(TypeExceptionEnum.ERREUR_INTERNE, e.getMessage(), e);

        }
    }

    @Override
    public InputStream getInputStreamFromUri(String fileUri) throws IOException {
        Resource resource = restTemplate.getForObject(fileUri, Resource.class);
        return resource != null ? resource.getInputStream() : null;
    }


}
