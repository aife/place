package com.atexo.docgen.core.port;

import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConsultationConfig;

public interface IAnalysePort {
    ConsultationConfig map(Consultation consultation);
}
