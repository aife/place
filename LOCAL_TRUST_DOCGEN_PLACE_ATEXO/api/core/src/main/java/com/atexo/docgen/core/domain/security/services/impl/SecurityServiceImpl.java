package com.atexo.docgen.core.domain.security.services.impl;

import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.ISecurityService;
import com.atexo.docgen.core.enums.FileModeEnum;
import org.springframework.stereotype.Component;

@Component
public class SecurityServiceImpl implements ISecurityService {
    @Override
    public boolean canEdit(DocumentEditorDetails editorDetails) {
        return hasAccess(editorDetails) && !FileModeEnum.VIEW.getValue().equals(editorDetails.getMode());
    }

    @Override
    public boolean hasAccess(DocumentEditorDetails documentEditorDetails) {
        return documentEditorDetails != null && documentEditorDetails.getMode() != null &&
                documentEditorDetails.getId() != null;
    }

    @Override
    public boolean hasMode(DocumentEditorDetails documentEditorDetails, String mode) {
        FileModeEnum.fromValue(mode);
        return documentEditorDetails != null && documentEditorDetails.getMode() != null && documentEditorDetails.getMode().equals(mode);
    }


}
