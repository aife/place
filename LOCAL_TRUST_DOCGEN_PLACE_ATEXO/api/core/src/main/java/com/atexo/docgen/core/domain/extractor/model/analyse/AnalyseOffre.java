package com.atexo.docgen.core.domain.extractor.model.analyse;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnalyseOffre {

    private String reference;
    private String intitule;
    private String objet;
    private List<AnalyseStatus> lots;
    private String entrepriseMieuxDisante;
    private String status;
    private List<SoumissionnaireAnalyse> soumissionnaires;
    private SoumissionnaireAnalyse attributaire;

}
