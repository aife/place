package com.atexo.docgen.core.domain.generator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AdministrationTypeEnum {
    CHAMP_FUSION_SIMPLE("CHAMP_FUSION_SIMPLE"),
    CHAMP_FUSION_COMPLEXE("CHAMP_FUSION_COMPLEXE");
    private final String value;

    AdministrationTypeEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static AdministrationTypeEnum fromValue(String value) {
        for (AdministrationTypeEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
