package com.atexo.docgen.core.utilitaire.word;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtBlock;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyXWPFDocument extends XWPFDocument {

    private static final POILogger LOG = POILogFactory.getLogger(XWPFDocument.class);

    public MyXWPFDocument(InputStream is) throws IOException {
        super(is);
    }

    public void createTOC(XmlCursor cursor, String headingStyleIdPrefix) {
        CTSdtBlock block = null;
        if (cursor != null && isCursorInBody(cursor)) {
            String uri = CTSdtBlock.type.getName().getNamespaceURI();
            String localPart = "sdt";
            cursor.beginElement(localPart, uri);
            cursor.toParent();
            block = (CTSdtBlock) cursor.getObject();
        } else {
            block = this.getDocument().getBody().addNewSdt();
        }
        TOC toc = new TOC(block);
        int bookmarkId = 0;
        String tocIdPrefix = "123456";
        for (XWPFParagraph par : paragraphs) {
            String parStyleId = par.getStyle();
            if (parStyleId != null && parStyleId.toLowerCase().startsWith(headingStyleIdPrefix.toLowerCase())) {
                try {
                    int level = Integer.parseInt(parStyleId.substring(headingStyleIdPrefix.length()));
                    par.getCTP().addNewBookmarkStart();
                    par.getCTP().getBookmarkStartArray(0).setId(BigInteger.valueOf(bookmarkId));
                    par.getCTP().getBookmarkStartArray(0).setName("_Toc" + tocIdPrefix + bookmarkId);
                    par.getCTP().addNewBookmarkEnd().setId(BigInteger.valueOf(bookmarkId));
                    toc.addRow(level, par.getText(), 1, "" + tocIdPrefix + bookmarkId);
                    bookmarkId++;
                } catch (Exception e) {
                    LOG.log(POILogger.ERROR, "can't create TOC item", e);
                }
            }
        }
        recreateBodyElementLists(-1, List.of());
    }

    protected void recreateBodyElementLists(Integer xmlCursor, List<IBodyElement> elements) {
        bodyElements = new ArrayList<>();
        paragraphs = new ArrayList<>();
        tables = new ArrayList<>();
        contentControls = new ArrayList<>();
        // parse the document with cursor and add
        // the XmlObject to its lists
        XmlCursor docCursor = this.getDocument().newCursor();
        docCursor.selectPath("./*");
        int i = 0;
        while (docCursor.toNextSelection()) {
            XmlObject o = docCursor.getObject();
            if (o instanceof CTBody) {
                XmlCursor bodyCursor = o.newCursor();
                bodyCursor.selectPath("./*");
                while (bodyCursor.toNextSelection()) {
                    if (i == xmlCursor) {
                        for (IBodyElement element : elements) {
                            if (element instanceof XWPFParagraph) {
                                final XWPFParagraph item = (XWPFParagraph) element;
                                bodyElements.add(item);
                                paragraphs.add(item);
                            } else if (element instanceof XWPFTable) {
                                final XWPFTable item = (XWPFTable) element;
                                bodyElements.add(item);
                                tables.add(item);
                            } else if (element instanceof XWPFSDT) {
                                final XWPFSDT item = (XWPFSDT) element;
                                bodyElements.add(item);
                                contentControls.add(item);
                            }
                        }

                    } else {
                        XmlObject bodyObj = bodyCursor.getObject();
                        if (bodyObj instanceof CTP) {
                            XWPFParagraph p = new XWPFParagraph((CTP) bodyObj, this);
                            bodyElements.add(p);
                            paragraphs.add(p);
                        } else if (bodyObj instanceof CTTbl) {
                            XWPFTable t = new XWPFTable((CTTbl) bodyObj, this);
                            bodyElements.add(t);
                            tables.add(t);
                        } else if (bodyObj instanceof CTSdtBlock) {
                            XWPFSDT c = new XWPFSDT((CTSdtBlock) bodyObj, this);
                            bodyElements.add(c);
                            contentControls.add(c);
                        }
                    }
                    i++;
                }
                bodyCursor.dispose();
            }
        }
        docCursor.dispose();
    }

    public List<XWPFSDT> getContentControls() {
        return Collections.unmodifiableList(contentControls);
    }

    private boolean isCursorInBody(XmlCursor cursor) {
        XmlCursor verify = cursor.newCursor();
        verify.toParent();
        boolean result = (verify.getObject() == getDocument().getBody());
        verify.dispose();
        return result;
    }
}
