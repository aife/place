package com.atexo.docgen.core.domain.convertor.model;

import lombok.*;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WordStyle {
    private String parentStyle;
    private boolean bold;
    private String color = "000000";
    private String background = "FFFFFF";
    private boolean italic;
    private UnderlinePatterns underline;
    private boolean strike;
    private boolean smallCaps;
    private boolean capitalized;
    private boolean shadow;
    private boolean imprinted;
    private boolean embossed;
    private FontFamily fontFamily = FontFamily.ARIAL;
    private Integer fontSize;
    private Integer textPosition;
    private String textAlign;
    private String horizontalTextAlign;
    private String verticalTextAlign;
    private String href;

}
