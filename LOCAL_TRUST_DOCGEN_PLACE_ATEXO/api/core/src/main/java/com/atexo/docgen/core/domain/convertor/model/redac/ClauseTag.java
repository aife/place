package com.atexo.docgen.core.domain.convertor.model.redac;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClauseTag {
    private List<String> roles;
    private String status;
}
