package com.atexo.docgen.core.domain.editor.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Goback {
    private String url;
}
