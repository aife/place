package com.atexo.docgen.core.domain.security.model;


import com.atexo.docgen.core.domain.editor.model.DocumentConfiguration;
import com.atexo.docgen.core.domain.editor.model.User;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentEditorDetails implements Serializable {
    @NotNull
    private String id;
    @NotNull
    private String documentId;
    @NotNull
    private String plateformeId;
    @NotNull
    private String mode;
    @NotNull
    private User user;
    private DocumentConfiguration configuration;
    @NotNull
    private List<String> users;
    private String callback;
    private Map<String, String> headersCallback;
}
