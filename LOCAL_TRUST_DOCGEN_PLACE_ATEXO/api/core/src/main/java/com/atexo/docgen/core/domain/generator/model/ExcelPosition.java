package com.atexo.docgen.core.domain.generator.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelPosition {
    private int sheetIndex;
    private int row;
    private int column;
    private String champsFusion;
}
