package com.atexo.docgen.core.domain.editor.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class Customer {
    private String address;
    private String info;
    private String logo;
    private String mail;
    private String name;
    private String www;

}
