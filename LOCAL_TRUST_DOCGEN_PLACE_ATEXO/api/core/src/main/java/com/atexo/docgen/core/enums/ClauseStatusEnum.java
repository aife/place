package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum ClauseStatusEnum {
    INITIALE("INITIALE"), INVALIDE("INVALIDE"), INVALIDE_OBLIGATOIRE("INVALIDE - SAISIE OBLIGATOIRE"), VALIDE("VALIDE");
    private final String value;

    ClauseStatusEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ClauseStatusEnum fromValue(String value) {
        for (ClauseStatusEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        throw new IllegalArgumentException("Cannot find ClauseStatusEnum from value: " + value);
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
