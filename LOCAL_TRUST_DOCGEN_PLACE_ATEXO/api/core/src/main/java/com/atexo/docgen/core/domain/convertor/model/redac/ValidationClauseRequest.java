package com.atexo.docgen.core.domain.convertor.model.redac;

import com.atexo.docgen.core.domain.convertor.model.ClauseType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ValidationClauseRequest {
    private DocxDocument document;
    private ClauseType clause;
}
