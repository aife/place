package com.atexo.docgen.core.domain.editor.model;

import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DonneeTest {
    private String libelle;
    private List<KeyValueRequest> requests;
}
