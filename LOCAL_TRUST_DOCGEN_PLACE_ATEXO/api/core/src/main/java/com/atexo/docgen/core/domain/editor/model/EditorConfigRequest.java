package com.atexo.docgen.core.domain.editor.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EditorConfigRequest {
    private FileModel config;
    private String apis;
}
