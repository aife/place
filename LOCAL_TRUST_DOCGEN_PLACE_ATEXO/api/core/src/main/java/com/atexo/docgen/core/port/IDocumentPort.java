package com.atexo.docgen.core.port;

import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.monitoring.model.MonitoringModel;
import com.atexo.docgen.core.enums.FileStatusEnum;

import java.util.List;

public interface IDocumentPort {


    String setNewKey(String id, String compositeKey);

    void setNewVersion(String id, boolean newVersion);

    DocumentModel modifyDocumentStatusById(String id, String status);

    DocumentModel getById(String id);

    List<MonitoringModel> getAll();

    List<DocumentModel> getDocumentsByPlateformeId(String plateforme);

    List<DocumentModel> getDocumentsByPlateformeIdAndStatus(String plateformeId, FileStatusEnum status);

    void deleteById(String key);

    DocumentModel addDocumentIfNotExist(DocumentEditorRequest editorRequest, String curExt, String plateformeId, String idFileSystem, long size, int version);

    void addNewVersion(String id, int newVersion, FileStatusEnum statusEnum);

    void modifyDocumentRedacStatusById(String id, String status);
}
