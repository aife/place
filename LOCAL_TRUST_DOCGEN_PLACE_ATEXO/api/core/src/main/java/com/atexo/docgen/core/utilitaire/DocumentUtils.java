package com.atexo.docgen.core.utilitaire;

import com.atexo.docgen.core.enums.FileTypeEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public final class DocumentUtils {
    private DocumentUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static String getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf('.') + 1)).orElse(null);
    }


    public static FileTypeEnum getFileTypeFromExtension(String ext) {
        if (ext.equals("docx"))
            return FileTypeEnum.TEXT;

        if (ext.equals("xlsx"))
            return FileTypeEnum.SPREADSHEET;

        if (ext.equals("pptx"))
            return FileTypeEnum.PRESENTATION;

        return FileTypeEnum.TEXT;
    }

    public static String getFileName(String url) {

        if (url == null) return null;
        return url.substring(url.lastIndexOf('/') + 1);
    }

    public static String getFileExtension(String url) {
        String fileName = getFileName(url);
        return getExtension(fileName);
    }

    public static File saveFile(InputStream file, String directory, String fileName, Boolean override) {
        try {
            File directoryFile = new File(directory);
            if (!directoryFile.isDirectory()) {
                Files.deleteIfExists(directoryFile.toPath());
            }
            if (!directoryFile.exists()) {
                log.info("Création des dossiers {}", directory);
                Files.createDirectories(directoryFile.toPath());
            }

            File convFile = new File(directory, fileName);
            if (convFile.exists() && (override == null || Boolean.FALSE.equals(override))) {
                log.info("Le fichier {} existe déjà", convFile.getAbsolutePath());
                return convFile;
            }
            Files.deleteIfExists(convFile.toPath());

            log.info("Création du fichier {}", convFile.getAbsolutePath());
            Files.createFile(convFile.toPath());
            try (FileOutputStream fos = new FileOutputStream(convFile)) {
                fos.write(IOUtils.toByteArray(file));
                log.info("Fichier {} est sauvegardé dans {}", fileName, directory);
            }
            return convFile;
        } catch (IOException e) {
            log.error("Erreur lors du sauvegarde {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.ERREUR_FICHIER, e.getMessage());
        }
    }

    public static JsonNode getReplacement(JsonNode jsonNode, String champsFusion) {
        String[] attributs = champsFusion.split("\\.");
        try {

            for (int i = 0; i < attributs.length; i++) {
                String attribut = attributs[i];
                String field = attribut;
                int index = -1;
                if (attribut.contains("(") && attribut.endsWith(")")) {
                    String[] split = attribut.split("\\(");
                    field = split[0];
                    index = Integer.parseInt(split[1]
                            .split("\\)")[0]);
                }
                JsonNode node = jsonNode.get(field);
                jsonNode = (index == -1 || !node.isArray() || node.size() < index + 1) ? node : node.get(index);
                if (jsonNode == null) {
                    break;
                } else if (jsonNode.isArray() && jsonNode.size() == 1 && i < attributs.length - 1) {
                    jsonNode = jsonNode.get(0);
                }
            }
            return jsonNode;
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de la valeur : {}", e.getMessage());
            return null;
        }

    }


    public static void deleteFolder(File folder) throws IOException {
        if (Files.exists(folder.toPath())) {
            try (Stream<Path> walk = Files.walk(folder.toPath())) {
                walk.sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::deleteOnExit);
                log.info("Suppression de {}", folder.getAbsolutePath());
            }
        }
    }

    public static File createFolder(String parent, String child) throws IOException {
        File folder = new File(parent, child);
        if (!Files.exists(folder.toPath()))
            Files.createDirectory(folder.toPath());

        return folder;
    }
}
