package com.atexo.docgen.core.domain.generator.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocxGenerationTag {
    private String condition;
    private GenerationTypeEnum type;
}
