package com.atexo.docgen.core.utilitaire;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDNamedDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class PdfDocumentUtils {
    private PdfDocumentUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static Map<String, Integer> getListAnchorAndPage(InputStream pdf) throws IOException {
        Map<String, Integer> contentsTablePages = new HashMap<>();
        try (PDDocument doc = PDDocument.load(pdf)) {
            for (PDPage page : doc.getPages()) {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                List<PDAnnotation> annotations = page.getAnnotations();
                for (int j = 0; j < annotations.size(); j++) {
                    PDAnnotation annot = annotations.get(j);
                    if (annot instanceof PDAnnotationLink) {
                        PDAnnotationLink link = (PDAnnotationLink) annot;
                        PDRectangle rect = link.getRectangle();
                        float x = 0.0f;
                        float y = rect.getUpperRightY();
                        float width = rect.getWidth();
                        float height = rect.getHeight();
                        int rotation = page.getRotation();
                        if (rotation == 0) {
                            PDRectangle pageSize = page.getMediaBox();
                            y = pageSize.getHeight() - y;
                            width = pageSize.getWidth();
                        }  //do nothing


                        Rectangle2D.Float awtRect = new Rectangle2D.Float(x, y, width, height);
                        stripper.addRegion("" + j, awtRect);
                    }
                }

                stripper.extractRegions(page);
                for (PDAnnotation annot : annotations) {
                    if (annot instanceof PDAnnotationLink) {
                        PDAnnotationLink link = (PDAnnotationLink) annot;
                        PDDestination pDestination = link.getDestination();
                        String urlText = stripper.getTextForRegion("" + annotations.indexOf(annot));

                        if (pDestination != null) {
                            PDPageDestination pageDestination;
                            if (pDestination instanceof PDPageDestination) {
                                pageDestination = (PDPageDestination) pDestination;
                            } else {
                                if (pDestination instanceof PDNamedDestination) {
                                    pageDestination = doc.getDocumentCatalog().findNamedDestinationPage((PDNamedDestination) pDestination);
                                } else {
                                    // error handling
                                    break;
                                }
                            }

                            if (pageDestination != null) {
                                contentsTablePages.put(urlText, pageDestination.retrievePageNumber() + 1);
                            }  //contentsTablePages.add(contentsTablePages.get(contentsTablePages.size() - 1));

                        }
                    }
                }
            }
        }

        return contentsTablePages;
    }


}
