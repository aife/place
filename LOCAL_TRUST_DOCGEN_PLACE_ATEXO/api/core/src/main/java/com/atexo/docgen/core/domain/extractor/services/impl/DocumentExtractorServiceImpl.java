package com.atexo.docgen.core.domain.extractor.services.impl;

import com.atexo.docgen.core.domain.convertor.model.analyse.*;
import com.atexo.docgen.core.domain.extractor.model.*;
import com.atexo.docgen.core.domain.extractor.model.analyse.AnalyseOffre;
import com.atexo.docgen.core.domain.extractor.model.analyse.AnalyseStatus;
import com.atexo.docgen.core.domain.extractor.model.analyse.CritereAnalyse;
import com.atexo.docgen.core.domain.extractor.model.analyse.SoumissionnaireAnalyse;
import com.atexo.docgen.core.domain.extractor.services.IDocumentExtractorService;
import com.atexo.docgen.core.domain.extractor.validator.ExtractorValidator;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.utilitaire.XlsxConverterUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.*;

@Service
@Slf4j
public class DocumentExtractorServiceImpl implements IDocumentExtractorService {

    private final ExtractorValidator extractorValidator;

    public DocumentExtractorServiceImpl(ExtractorValidator extractorValidator) {
        this.extractorValidator = extractorValidator;
    }

    @Override
    public ByteArrayOutputStream extract(@NotEmpty List<MultipartFile> fichiers, @NotNull ByteArrayResource template, @NotNull ExcelConfiguration configuration) {
        InputStream templateInputStream = new ByteArrayInputStream(template.getByteArray());
        log.info("Prise en compte des positions de chaque éléments");
        List<CellPosition> cellPositions = configuration
                .getPositions()
                .stream()
                .map(CellPosition::new)
                .collect(Collectors.toList());
        log.info("Lecture de chaque date avec son fichier");
        List<XSSFWorkbook> xssfWorkbooks = XlsxConverterUtils.readXlsxFiles(fichiers);
        log.info("Vérification de la validité des fichiers envoyés");
        extractorValidator.validate(fichiers);
        log.info("Lancement du process de lecture de données");
        List<ExtractorBuilderModel> cells = extractAllValues(xssfWorkbooks, cellPositions);
        log.info("Lancement de la création du résultat");
        return generateDocument(configuration.getStaticData(), cells, templateInputStream);
    }

    @Override
    public AnalyseOffre extractAnalyseStatus(InputStream inputStream, Configuration configuration) {
        try (XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {

            XSSFSheet generalSheet = workbook.getSheetAt(0);
            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
            ConsultationConfig meta = configuration.getMeta();

            if (meta == null) {
                throw new DocGenException(TypeExceptionEnum.EXTRACTION, "Meta de configuration ne peut pas être null");
            }

            List<LotConfig> lots = meta.getLots();
            if (!CollectionUtils.isEmpty(lots)) {
                List<AnalyseStatus> analyseStatuses = new ArrayList<>();
                for (int i = 0; i < lots.size(); i++) {

                    int generalRowIndex = configuration.getApercuDebut().getRow() - 1 + i;
                    int generalColumnIndex = CellReference.convertColStringToIndex(configuration.getApercuDebut().getColumn());

                    String value = getCellStringValue(evaluator, generalSheet, generalRowIndex, generalColumnIndex);
                    if (value == null) {
                        continue;
                    }
                    LotConfig lot = lots.stream().filter(lot1 -> value.equals(lot1.getNumero() + " - " + lot1.getIntitule())).findFirst().orElse(null);
                    if (null == lot) {
                        continue;
                    }
                    String entreprise = getCellStringValue(evaluator, generalSheet, generalRowIndex, generalColumnIndex + 1);

                    String calculatedStatus = getCellStringValue(evaluator, generalSheet, generalRowIndex, generalColumnIndex + 2);

                    XSSFSheet lotSheet = workbook.getSheet("Lot " + lot.getNumero());

                    List<SoumissionnaireAnalyse> soumissionnaires = this.getAnalyseStatus(evaluator, lotSheet, lot.getSoumissionnaires(), lot.getCriteres(), configuration.getSoumissionnaireDebut().getColumn());

                    SoumissionnaireAnalyse premierAttributaire = getSoumissionnaireAttributaire(soumissionnaires);
                    final AnalyseStatus status = AnalyseStatus.builder()
                            .numero(lot.getNumero())
                            .intitule(lot.getIntitule())
                            .soumissionnaires(soumissionnaires)
                            .attributaire(premierAttributaire)
                            .status(calculatedStatus)
                            .entrepriseMieuxDisante(premierAttributaire != null ? premierAttributaire.getRaisonSocial() : null)
                            .build();
                    analyseStatuses.add(status);
                }
                return AnalyseOffre.builder()
                        .objet(meta.getObjet())
                        .reference(meta.getReference())
                        .intitule(meta.getIntitule())
                        .lots(analyseStatuses).build();
            } else {

                int generalRowIndex = configuration.getApercuDebut().getRow() - 1;
                int generalColumnIndex = CellReference.convertColStringToIndex(configuration.getApercuDebut().getColumn());

                String entreprise = getCellStringValue(evaluator, generalSheet, generalRowIndex, generalColumnIndex + 1);

                String calculatedStatus = getCellStringValue(evaluator, generalSheet, generalRowIndex, generalColumnIndex + 2);

                XSSFSheet lotSheet = workbook.getSheet("Analyse");

                List<SoumissionnaireAnalyse> soumissionnaires = this.getAnalyseStatus(evaluator, lotSheet, meta.getSoumissionnaires(), meta.getCriteres(), configuration.getSoumissionnaireDebut().getColumn());
                SoumissionnaireAnalyse premierAttributaire = getSoumissionnaireAttributaire(soumissionnaires);

                return AnalyseOffre.builder()
                        .reference(meta.getReference())
                        .intitule(meta.getIntitule())
                        .soumissionnaires(soumissionnaires)
                        .attributaire(premierAttributaire)
                        .status(calculatedStatus)
                        .entrepriseMieuxDisante(premierAttributaire != null ? premierAttributaire.getRaisonSocial() : null)
                        .build();
            }
        } catch (Exception e) {
            log.error("Erreur lors de l'extraction de l'analyse {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.EXTRACTION, e.getMessage(), e);
        }
    }

    @Override
    public List<FeuillePositionValue> extractXlsx(InputStream inputStream, List<FeuillePositionValue> positions) {
        try (XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {
            Map<String, List<FeuillePositionValue>> map = positions.stream().collect(Collectors.groupingBy(FeuillePositionValue::getSheetName));
            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

            map.keySet().forEach(sheetName -> {
                List<FeuillePositionValue> values = map.get(sheetName);
                XSSFSheet generalSheet = workbook.getSheet(sheetName);
                int index = workbook.getSheetIndex(generalSheet);
                values.forEach(position -> {
                    CellReference cellReference = new CellReference(position.getColumn() + position.getRow());
                    String value = getCellStringValue(evaluator, generalSheet, cellReference.getRow(), cellReference.getCol());
                    position.setData(value);
                    position.setSheetIndex(index);
                    position.setType(getCellType(generalSheet, cellReference.getRow(), cellReference.getCol()));
                });
            });

            return map.values().stream().flatMap(List::stream).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Erreur lors de l'extraction de l'analyse {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.EXTRACTION, e.getMessage(), e);
        }
    }

    @Override
    public List<Feuille> getSheetNames(InputStream inputStream) {
        try (XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {
            List<Feuille> feuilles = new ArrayList<>();
            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                String sheetName = workbook.getSheetAt(i).getSheetName();
                feuilles.add(Feuille.builder().sheetIndex(i).sheetName(sheetName)
                        .hidden(workbook.getSheetVisibility(i) != SheetVisibility.VISIBLE)
                        .build());
            }
            return feuilles;
        } catch (Exception e) {
            log.error("Erreur lors de l'extraction de l'analyse {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.EXTRACTION, e.getMessage(), e);
        }
    }

    private SoumissionnaireAnalyse getSoumissionnaireAttributaire(List<SoumissionnaireAnalyse> soumissionnaires) {
        final List<SoumissionnaireAnalyse> soumissionnairesAttributaires = soumissionnaires.stream().filter(soumissionnaireAnalyse -> soumissionnaireAnalyse.getClassement() == 1).collect(Collectors.toList());
        SoumissionnaireAnalyse premierAttributaire = null;
        if (!CollectionUtils.isEmpty(soumissionnairesAttributaires) && soumissionnairesAttributaires.size() == 1) {
            premierAttributaire = soumissionnairesAttributaires.get(0);
            for (SoumissionnaireAnalyse soumissionnaireAnalyse : soumissionnaires) {
                soumissionnaireAnalyse.setNoteGlobaleAttributaire(premierAttributaire.getNoteGlobale());
                for (int j = 0; j < soumissionnaireAnalyse.getCriteres().size(); j++) {
                    CritereAnalyse critereAnalyse = soumissionnaireAnalyse.getCriteres().get(j);
                    critereAnalyse.setEvaluationNoteAttributaire(premierAttributaire.getCriteres().get(j).getEvaluationNote());
                    final List<CritereAnalyse> sousCriteres = critereAnalyse.getSousCriteres();
                    if (!CollectionUtils.isEmpty(sousCriteres)) {
                        for (int k = 0; k < sousCriteres.size(); k++) {
                            CritereAnalyse sousCritereAnalyse = sousCriteres.get(k);
                            sousCritereAnalyse.setEvaluationNoteAttributaire(premierAttributaire.getCriteres().get(j)
                                    .getSousCriteres().get(k).getEvaluationNote());
                        }
                    }
                }
            }
        }
        return premierAttributaire;
    }

    private List<SoumissionnaireAnalyse> getAnalyseStatus(FormulaEvaluator evaluator, XSSFSheet lotSheet, List<Soumissionnaire> metaSoumissionnaires, List<CritereConfig> critereConfigs,
                                                          String soumissionnaireStartColumn) {
        List<SoumissionnaireAnalyse> soumissionnaires = new ArrayList<>();
        int soumissionnaireStartColumnIndex = CellReference.convertColStringToIndex(soumissionnaireStartColumn);
        if (CollectionUtils.isEmpty(metaSoumissionnaires)) {
            return soumissionnaires;
        }
        for (int soumissionnaireIndex = 0; soumissionnaireIndex < metaSoumissionnaires.size(); soumissionnaireIndex++) {
            List<CritereAnalyse> criteres = new ArrayList<>();
            for (CritereConfig critere : critereConfigs) {

                criteres.add(getCriteres(evaluator, lotSheet, soumissionnaireStartColumnIndex + soumissionnaireIndex, critere));
            }
            PositionValue lastCriteriaPosition =
                    critereConfigs.stream().map(CritereConfig::getPosition).max(Comparator.comparing(PositionValue::getRow)).orElse(null);
            if (lastCriteriaPosition == null) {
                continue;
            }
            int row = lastCriteriaPosition.getRow();
            int column = CellReference.convertColStringToIndex(lastCriteriaPosition.getColumn()) + soumissionnaireIndex + 1;
            Double globalNote = getCellNumberValue(evaluator, lotSheet, row, column);
            Double classement = getCellNumberValue(evaluator, lotSheet, row + 1, column);
            final Soumissionnaire soumissionnaire = metaSoumissionnaires.get(soumissionnaireIndex);
            final SoumissionnaireAnalyse soumissionnaireAnalyse = SoumissionnaireAnalyse.builder()
                    .raisonSocial(soumissionnaire.getRaisonSocial())
                    .siret(soumissionnaire.getSiret())
                    .enveloppe(soumissionnaire.getEnveloppe())
                    .classement(classement == null ? 0 : classement.intValue())
                    .noteGlobale(globalNote == null ? 0 : globalNote.intValue())
                    .criteres(criteres)
                    .build();
            soumissionnaireAnalyse.setClassementString(getClassementString(soumissionnaireAnalyse.getClassement()));
            soumissionnaires.add(soumissionnaireAnalyse);
        }
        return soumissionnaires;
    }

    private String getClassementString(int classement) {
        return classement + (classement == 1 ? "ère" : "eme");
    }

    private CritereAnalyse getCriteres(FormulaEvaluator evaluator, XSSFSheet lotSheet, int soumissionnaireColumnIndex, CritereConfig critere) {
        PositionValue criterePosition = critere.getPosition();
        int critereRowIndex = criterePosition.getRow() - 1;

        Double extractedCritereNote = getCellNumberValue(evaluator, lotSheet, critereRowIndex, soumissionnaireColumnIndex);
        String extractedCritereCommentaire = null;
        List<CritereAnalyse> sousCriteres = null;
        List<CritereConfig> metaSousCriteres = critere.getSousCriteres();
        if (CollectionUtils.isEmpty(metaSousCriteres)) {
            extractedCritereCommentaire = getCellStringValue(evaluator, lotSheet, critereRowIndex - 2, soumissionnaireColumnIndex);
        } else {
            sousCriteres = new ArrayList<>();
            for (CritereConfig sousCritere : metaSousCriteres) {
                sousCriteres.add(getSousCritere(evaluator, lotSheet, soumissionnaireColumnIndex, sousCritere));
            }
        }
        return CritereAnalyse.builder()
                .commentaire(extractedCritereCommentaire)
                .description(critere.getDescription())
                .sousCriteres(sousCriteres)
                .evaluationNote(extractedCritereNote != null ? extractedCritereNote.intValue() : 0)
                .maximumNote(critere.getMaximumNote())
                .build();
    }

    private CritereAnalyse getSousCritere(FormulaEvaluator evaluator, XSSFSheet lotSheet, int sousCritereColumnIndex, CritereConfig sousCritere) {
        int sousCritereRowIndex = sousCritere.getPosition().getRow() - 1;

        String extractedSousCritereCommentaire = getCellStringValue(evaluator, lotSheet, sousCritereRowIndex, sousCritereColumnIndex);


        Double extractedSousCritereNote = getCellNumberValue(evaluator, lotSheet, sousCritereRowIndex + 1, sousCritereColumnIndex);

        return CritereAnalyse.builder()
                .commentaire(extractedSousCritereCommentaire)
                .description(sousCritere.getDescription())
                .evaluationNote(extractedSousCritereNote != null ? extractedSousCritereNote.intValue() : 0)
                .maximumNote(sousCritere.getMaximumNote())
                .build();
    }

    private List<ExtractorBuilderModel> extractAllValues(List<XSSFWorkbook> dateFiles, List<CellPosition> positions) {
        log.info("Extraction des données d'entré(s)");
        return positions.stream()
                .map(position -> ExtractorBuilderModel
                        .builder()
                        .xssfCells(dateFiles.stream().map(file -> {
                            XSSFSheet sheet = file.getSheet(position.getSheet());
                            FormulaEvaluator evaluator = file.getCreationHelper().createFormulaEvaluator();
                            return evaluator.evaluateInCell(sheet
                                    .getRow(position.getPosition().getRow()).getCell(position.getPosition().getCol()));
                        }).collect(Collectors.toList()))
                        .cellReference(position.getStartTargetPosition())
                        .build())
                .collect(Collectors.toList());

    }


    private ByteArrayOutputStream generateDocument(List<StaticData> staticDataList, List<ExtractorBuilderModel> values,
                                                   InputStream template) {
        XSSFWorkbook templateWorkBook;
        try {
            templateWorkBook = new XSSFWorkbook(template);
            templateWorkBook.setForceFormulaRecalculation(true);

            Sheet sheet = templateWorkBook.getSheetAt(0);

            setExtractedData(values, sheet);

            setStaticData(staticDataList, sheet);

            log.info("Création du fichier de sortie");
            XSSFFormulaEvaluator.evaluateAllFormulaCells(templateWorkBook);
            templateWorkBook.setForceFormulaRecalculation(true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            templateWorkBook.write(bos);
            return bos;
        } catch (IOException e) {
            log.error("Erreur lors de génération du fichier de résultat' : {}", e.getMessage());
        }
        return null;
    }

    private void setStaticData(List<StaticData> staticDataList, Sheet sheet) {
        for (StaticData data : staticDataList) {
            CellReference cellReference = new CellReference(data.getStartTargetPosition().getColumn() + data.getStartTargetPosition().getRow());
            Row targetRow = sheet.getRow(cellReference.getRow());
            if (targetRow == null)
                sheet.createRow(cellReference.getRow());
            for (int i = 0; i < data.getData().size() && targetRow != null; i++) {

                int targetColumn = cellReference.getCol() + i;
                Cell cell = targetRow.getCell(targetColumn);
                if (cell == null)
                    cell = targetRow.createCell(targetColumn);
                cell.setCellValue(data.getData().get(i));

            }
        }
    }

    private void setExtractedData(List<ExtractorBuilderModel> values, Sheet sheet) {
        log.info("Écriture des résultats");
        for (ExtractorBuilderModel builder : values) {
            int startTargetRow = builder.getCellReference().getRow();
            int startTargetColumn = builder.getCellReference().getCol();

            List<Cell> xssfCells = builder.getXssfCells();

            Row valuesRow = sheet.getRow(startTargetRow);
            if (valuesRow == null)
                sheet.createRow(startTargetRow);
            for (int j = 0; j < xssfCells.size() && valuesRow != null; j++) {
                int targetColumn = startTargetColumn + j;
                Cell cell = valuesRow.getCell(targetColumn);
                if (cell == null)
                    cell = valuesRow.createCell(targetColumn);
                setCellTypeAndValue(xssfCells.get(j), cell);
            }
        }
    }

    private void setCellTypeAndValue(Cell xssfCell, Cell cell) {

        String s = xssfCell.toString();
        switch (xssfCell.getCellType()) {
            case NUMERIC:
                cell.setCellValue(Double.parseDouble(s));
                break;
            case BOOLEAN:
                cell.setCellValue(Boolean.getBoolean(s));
                break;
            case _NONE:
            case BLANK:
            case ERROR:
            case STRING:
            case FORMULA:
            default:
                cell.setCellValue(s);

        }
    }


}
