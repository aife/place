package com.atexo.docgen.core.domain.convertor.services.impl.redac;

import com.atexo.docgen.core.domain.convertor.model.*;
import com.atexo.docgen.core.domain.convertor.services.html.IHtmlDocService;
import com.atexo.docgen.core.domain.convertor.services.redac.IClauseRenderer;
import com.atexo.docgen.core.enums.ClauseColorEnum;
import com.atexo.docgen.core.enums.ClauseTypeEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.utilitaire.word.StylesUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.impl.CTSdtBlockImpl;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.ClauseUtils.*;
import static com.atexo.docgen.core.utilitaire.StringUtils.getReplace;
import static com.atexo.docgen.core.utilitaire.word.NumeringUtils.getBulletStyle;
import static com.atexo.docgen.core.utilitaire.word.SdtUtils.addSdtToDocument;
import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.*;


@Slf4j
@Component
public class ClauseRendererImpl implements IClauseRenderer {

    private final ObjectMapper objectMapper;
    private final IHtmlDocService iHtmlDocService;

    protected String format = "%1$s";
    protected ArrayList<String> lines = null;
    protected String message = "La Clause %s dispose d'un contenu obligatoire, elle ne peut être validée sans contenu.";

    public ClauseRendererImpl(IHtmlDocService iHtmlDocService, ObjectMapper objectMapper) {
        this.iHtmlDocService = iHtmlDocService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void render(ClauseType clause, XWPFDocument document, ChapitreType chapitre) {
        try (XWPFDocument clauseDocument = new XWPFDocument()) {
            String tag = objectMapper.writeValueAsString(getClauseTag(clause));
            String title = "CHAPITRE " + chapitre.getNumero() + " CLAUSE " + clause.getRef() + " - " + getClauseStatus(clause).getValue();
            if ((clause.getInfoBulle() != null && Boolean.TRUE.equals(clause.getInfoBulle().isActif())) ||
                    (chapitre.getInfoBulle() != null && Boolean.TRUE.equals(chapitre.getInfoBulle().isActif()))) {
                title += " - (voir info bulle)";
            }
            BigInteger numID = getBulletStyle(document);
            String id = String.valueOf(chapitre.getId()) + clause.getId();
            clause.setChapitreNumero(chapitre.getNumero());
            clause.setChapitreTitre(chapitre.getTitre());
            double effectivePageWidth = getPageWidth(document);
            getXwpfRuns(clause, numID, effectivePageWidth, clauseDocument);
            ClauseTypeEnum clauseTypeEnum = ClauseTypeEnum.fromValue(clause.getType());
            switch (clauseTypeEnum) {
                case TYPE_CLAUSE_LISTE_EXCLUSIVE:
                case TYPE_CLAUSE_LISTE_MULTIPLE:
                    if (Boolean.TRUE.equals(clause.isCLAUSEVALIDE())) {
                        addSdtToDocument(document, clauseDocument, id, tag, title, false, true);
                    } else if (Boolean.TRUE.equals(clause.isInitialise())) {
                        addSdtToDocument(document, clauseDocument, id, tag, title, true, false);
                    } else {
                        addSdtToDocument(document, clauseDocument, id, tag, title, false, true);
                    }
                    break;
                case TYPE_CLAUSE_TEXTE_LIBRE:
                case TYPE_CLAUSE_TEXTE_PREVALORISE:
                    if (Boolean.TRUE.equals(clause.isCLAUSEVALIDE())) {
                        addSdtToDocument(document, clauseDocument, id, tag, title, false, true);
                    } else {
                        addSdtToDocument(document, clauseDocument, id, tag, title, true, false);
                    }
                    break;
                default:
                    Boolean editDelete = !Boolean.TRUE.equals(clause.isDerogationResume());
                    addSdtToDocument(document, clauseDocument, id, tag, title, editDelete, editDelete);
            }
            if (clauseDocument != null) {
                clauseDocument.close();
            }

        } catch (Exception e) {
            log.error("Erreur lors de l'ajout d'une clause {}", e.getMessage());
        }

    }

    @Override
    public void getXwpfRuns(ClauseType clause, BigInteger
            numID, Double effectivePageWidth, XWPFDocument document) {
        validateClause(clause);
        if (document == null) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, "Document ne doit pas être null");
        }
        if (numID == null) {
            numID = getBulletStyle(document);
        }
        String background = getClauseBackground(clause);
        getTexteFixe(document, clause.getTexteFixeAvant(), background, numID, clause.isTexteFixeAvantALaLigne());
        switch (ClauseTypeEnum.fromValue(clause.getType())) {
            case TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case TYPE_CLAUSE_LISTE_MULTIPLE:
                getFormulationsRuns(document, clause, numID, background);
                break;
            case TYPE_CLAUSE_TEXTE_LIBRE:
                ClauseType.Texte value = clause.getTexte() == null ? new ClauseType.Texte() : clause.getTexte();
                if (StringUtils.isBlank(value.getValue()))
                    value.setValue("[Inclure texte à champ libre]");
                clause.setTexte(value);
            case TYPE_CLAUSE_TEXTE_PREVALORISE:
                getTexteRuns(document, clause, clause.isTexteFixeAvantALaLigne(), background, numID);
                break;
            case TYPE_CLAUSE_VALEUR_HERITEE:
                getOngletXls(document, clause, effectivePageWidth, background, numID);
                break;
            case TYPE_CLAUSE_TEXTE_FIXE:
                if (clause.getTableauRedaction() != null) {
                    getTexteFixe(document, clause.getTableauRedaction().getTextAvantTableau(), background, numID, true);
                }
                if (clause.getTexte() != null) {
                    getTexteRuns(document, clause, clause.isTexteFixeAvantALaLigne(), background, numID);
                }
                break;
            default:
                log.warn("Clause n'est pas prise en compte pour la validation {}", clause.getType());

        }
        getTexteFixe(document, clause.getTexteFixeApres(), background, numID, clause.isTexteFixeAresALaLigne());
    }

    private void getOngletXls(XWPFDocument document, ClauseType clause,
                              Double effectivePageWidth, String background, BigInteger numID) {
        boolean hasTexte = false;
        if (clause == null) {
            return;
        }
        if (clause.getTableauRedaction() != null) {
            getTexteFixe(document, clause.getTableauRedaction().getTextAvantTableau(), background, numID, true);
            hasTexte = StringUtils.isNotBlank(clause.getTableauRedaction().getTextAvantTableau());
        } else if (clause.getTexte() != null && StringUtils.isNotBlank(clause.getTexte().getValue())) {
            getTexteRuns(document, clause, clause.isTexteFixeAvantALaLigne(), background, numID);
            hasTexte = true;
        }
        if (clause.getTableauRedaction() == null
                || clause.getTableauRedaction().getDocumentXLS() == null
                || CollectionUtils.isEmpty(clause.getTableauRedaction().getDocumentXLS().getOnglet())) {
            return;
        }
        List<Onglet> onglets = clause.getTableauRedaction().getDocumentXLS().getOnglet();

        for (Onglet onglet : onglets) {
            List<Cellule> cellules = onglet.getCellule();
            Optional<Cellule> maxRows = cellules.stream()
                    .max(Comparator.comparing(i -> i.getCoordonnee().getLigne()));
            if (maxRows.isEmpty()) {
                continue;
            }

            int rows = maxRows.get().getCoordonnee().getLigne() + 1;
            Optional<Cellule> maxCols = cellules.stream()
                    .max(Comparator.comparing(i -> i.getCoordonnee().getColonne()));
            if (maxCols.isEmpty()) {
                continue;
            }
            int cols = maxCols.get().getCoordonnee().getColonne() + 1;
            if (hasTexte)
                document.createParagraph();
            XWPFTable table = document.createTable(rows, cols);
            CTTblLayoutType type = table.getCTTbl().getTblPr().addNewTblLayout();
            type.setType(STTblLayoutType.FIXED);

            cellules.forEach(cellule -> {
                WordStyle wordStyle = getWordStyleFromCellule(cellule);
                XWPFTableRow row = table.getRow(cellule.getCoordonnee().getLigne());
                row.setCantSplitRow(true);
                XWPFTableCell cell = row.getCell(cellule.getCoordonnee().getColonne());
                XWPFStyle style = StylesUtils.getStyleByName(document, "Tableau");
                String styleId = style == null ? StylesUtils.getStyleByName(document, "Table", "Table") : style.getStyleId();
                setCellTable(cell, cellule.getValeur(), wordStyle, styleId);
            });

            List<Double> columnsWidth;
            if (onglet.getAttributs() != null && onglet.getAttributs().getLargeurColonne() != null) {
                columnsWidth = onglet.getAttributs().getLargeurColonne().stream()
                        .map(attributLargeurColonne -> attributLargeurColonne.getLargeur() / 100)
                        .collect(Collectors.toList());
            } else {
                columnsWidth = new ArrayList<>();
                for (int i = 0; i < cols; i++) {
                    columnsWidth.add((double) (i + 1) / cols);
                }
            }
            if (effectivePageWidth != null) {
                setTableColumnsWidth(table, effectivePageWidth, columnsWidth);
            }

            if (onglet.getRegionFusionnee() != null) {
                onglet.getRegionFusionnee().forEach(regionFusionnee -> {
                    for (int row = regionFusionnee.getLigneDebut(); row <= regionFusionnee.getLigneFin(); row++) {
                        mergeCellHorizontally(table, row, regionFusionnee.getColonneDebut(), regionFusionnee.getColonneFin());
                    }
                    for (int colonne = regionFusionnee.getColonneDebut(); colonne <= regionFusionnee.getColonneFin(); colonne++) {
                        mergeCellVertically(table, colonne, regionFusionnee.getLigneDebut(), regionFusionnee.getLigneFin());
                    }
                });
            }
            table.setCellMargins(0, 0, 0, 0);
            //set borders
            table.setTopBorder(XWPFTable.XWPFBorderType.SINGLE, 4, 0, "000000");
            table.setBottomBorder(XWPFTable.XWPFBorderType.SINGLE, 4, 0, "000000");
            table.setLeftBorder(XWPFTable.XWPFBorderType.SINGLE, 4, 0, "000000");
            table.setRightBorder(XWPFTable.XWPFBorderType.SINGLE, 4, 0, "000000");
            table.setInsideHBorder(XWPFTable.XWPFBorderType.SINGLE, 4, 0, "000000");
            table.setInsideVBorder(XWPFTable.XWPFBorderType.SINGLE, 4, 0, "000000");

        }
    }

    private WordStyle getWordStyleFromCellule(Cellule cellule) {
        if (cellule == null || cellule.getStyle() == null)
            return null;
        WordStyle wordStyle = new WordStyle();
        CelluleStyle style = cellule.getStyle();
        CelluleStyleFont font = style.getFont();
        if (font != null) {
            wordStyle.setBold(font.isGras());
            wordStyle.setItalic(font.isItalic());
        }
        CelluleStyleRemplissage remplissage = style.getRemplissage();
        if (remplissage != null) {
            EnumerationCouleur premierPlan = remplissage.getPremierPlan();
            if (premierPlan != null)
                wordStyle.setColor(convertColor(premierPlan.value(), HSSFColor.HSSFColorPredefined.BLACK.getColor()));

            EnumerationCouleur arrierePlan = remplissage.getArrierePlan();
            if (arrierePlan != null)
                wordStyle.setBackground(convertColor(arrierePlan.value(), HSSFColor.HSSFColorPredefined.WHITE.getColor()));

        }
        CelluleStyleAlignement alignement = cellule.getStyle().getAlignement();
        if (alignement != null) {
            wordStyle.setVerticalTextAlign(alignement.getVertical().value().replace("VERTICAL_", ""));
            wordStyle.setHorizontalTextAlign(alignement.getHorizontal().value().replace("HORIZONTAL_", ""));
        }
        wordStyle.setFontSize(9);
        return wordStyle;
    }

    private void setCellTable(XWPFTableCell cell, String title, WordStyle
            wordStyle, String styleId) {
        if (wordStyle != null) {
            if (wordStyle.getBackground() != null)
                cell.getCTTc().addNewTcPr().addNewShd().setFill(wordStyle.getBackground());

            if (wordStyle.getVerticalTextAlign() != null)
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.valueOf(wordStyle.getVerticalTextAlign().toUpperCase()));
        }
        cell.removeParagraph(0);
        XWPFParagraph paragraph = cell.addParagraph();
        paragraph.setStyle(styleId);
        paragraph.setAlignment(ParagraphAlignment.LEFT);
        XWPFRun run = paragraph.createRun();
        run.setText(title);
        paragraph.getCTP().addNewPPr().addNewKeepLines().setVal(STOnOff.ON);
        paragraph.getCTP().getPPr().addNewKeepNext().setVal(STOnOff.ON);
        applyStyleToRun(wordStyle, run);
    }


    private void getTexteFixe(XWPFDocument document, String texte, String
            background, BigInteger numID, boolean isNewParagraph) {
        this.getTexteFixe(document, texte, background, numID, false, isNewParagraph);
    }

    private void getTexteFixe(XWPFDocument document, String texte, String
            background, BigInteger numID, boolean cantSplit, boolean isNewParagraph) {
        if (StringUtils.isNotBlank(texte)) {
            iHtmlDocService.insertHtml(getReplace(texte), document, isNewParagraph, background, numID, cantSplit);
        }
    }

    private void getFormulationsRuns(XWPFDocument document, ClauseType
            clause, BigInteger numID, String background) {
        if (clause.getFormulairesListeType() == null)
            return;
        List<FormulairesListe.Formulation> formulations = clause.getFormulairesListeType().getFormulation();
        if (CollectionUtils.isEmpty(formulations)) {
            return;
        }
        if (clause.getFormulairesListeType().isExclusif().equals(Boolean.TRUE)) {
            FormulairesListe.Formulation procoche = formulations.stream().filter(FormulairesListe.Formulation::isPrecoche).findFirst().orElse(null);
            if (procoche == null && ClauseColorEnum.YELLEOW.getCode().equals(background)) {
                for (FormulairesListe.Formulation formulation : formulations) {
                    String text = getReplace(formulation.getValue());
                    iHtmlDocService.insertHtml(text, document, clause.isTexteFixeAvantALaLigne(), background, numID, false);
                }
            } else if (procoche != null) {
                iHtmlDocService.insertHtml(getReplace(procoche.getValue()), document, clause.isTexteFixeAvantALaLigne(), background, numID, false);
            }
        } else {
            clause.setTexteFixeAresALaLigne(clause.isTexteFixeAresALaLigne());
            List<FormulairesListe.Formulation> procoches = formulations.stream().filter(FormulairesListe.Formulation::isPrecoche)
                    .collect(Collectors.toList());
            if (CollectionUtils.isEmpty(procoches) && ClauseColorEnum.YELLEOW.getCode().equals(background)) {
                procoches.addAll(formulations);
            }
            for (FormulairesListe.Formulation formulation : procoches) {
                String text = getReplace(formulation.getValue());
                iHtmlDocService.insertHtml(text, document, clause.isTexteFixeAvantALaLigne(), background, numID, false);
            }
        }


    }


    protected void getTexteRuns(XWPFDocument document, ClauseType clause,
                                boolean isNewParagraph, String background, BigInteger numID) {
        validateClauseTexteRun(clause);
        if (clause.getTexte() != null)
            iHtmlDocService.insertHtml(getReplace(clause.getTexte().getValue()), document, isNewParagraph, background, numID, false);

        addTexteIfEmpty(document, clause.getTexte(), background, numID);
    }


    private void addTexteIfEmpty(XWPFDocument document, ClauseType.Texte texte, String background, BigInteger numID) {
        if (CollectionUtils.isEmpty(document.getParagraphs())) {
            if (texte != null && Boolean.TRUE.equals(texte.isObligatoire()))
                iHtmlDocService.insertHtml("[Article obligatoire]", document, true, background, numID, false);
            else {
                iHtmlDocService.insertHtml("[Article optionnel]", document, true, background, numID, false);
            }
        }
    }

    @Override
    public XWPFDocument getXwpfDocumentFromClause(ClauseType clause, XWPFDocument document, String chapitreNumero) throws IOException {
        CTSdtBlock clauseContentControl = document.getDocument().getBody().getSdtList().stream().filter(ctSdtBlock -> {
            String clauseReference = "Chapitre " + chapitreNumero + " Clause " + clause.getRef();
            return !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getAliasList()) &&
                    ctSdtBlock.getSdtPr().getAliasArray(0).getVal().toUpperCase()
                            .startsWith(clauseReference.toUpperCase());
        }).findFirst().orElse(null);

        if (clauseContentControl == null) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));
        }
        if ((ClauseTypeEnum.TYPE_CLAUSE_TEXTE_LIBRE.getCode() == clause.getType() || ClauseTypeEnum.TYPE_CLAUSE_TEXTE_PREVALORISE.getCode() == clause.getType())
                && Boolean.TRUE.equals(clause.getTexte().isObligatoire())
                && StringUtils.isWhitespace(((CTSdtBlockImpl) clauseContentControl).stringValue())) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));
        }
        XWPFDocument newDocument = new XWPFDocument();
        String background = getClauseBackground(clause);
        CTSdtContentBlock sdtContent = clauseContentControl.getSdtContent();
        if (!CollectionUtils.isEmpty(sdtContent.getPList()))
            sdtContent.getPList().forEach(ctp -> cloneParagraph(newDocument.createParagraph(), ctp, background));
        if (!CollectionUtils.isEmpty(sdtContent.getTblList()))
            sdtContent.getTblList().forEach(ctp -> cloneTable(newDocument.createTable(), ctp));
        return newDocument;
    }

}
