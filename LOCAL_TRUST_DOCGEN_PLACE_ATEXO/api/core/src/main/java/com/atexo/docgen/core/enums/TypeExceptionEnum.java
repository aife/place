package com.atexo.docgen.core.enums;

public enum TypeExceptionEnum {
    GENERATION(1), CHAMPS_FUSION(2), EDITOR(3), MAPPER(4), ERREUR_FICHIER(5), ERREUR_INTERNE(6), EXTRACTION(7), CONVERSION(8),
    ONLYOFFICE(9), TEMPLATE(10);
    private final int code;

    TypeExceptionEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
