package com.atexo.docgen.core.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface IDocumentConfigurationService {
    List<String> getConvertExts();

    List<String> getEditedExts();

    String filesRootPath();

    String storagePath(String fileName);

    String generateDocumentKey(String expectedKey);

    void downloadToFile(String downloadUri, File toSave);

    InputStream getInputStreamFromUri(String fileUri) throws IOException;

}
