package com.atexo.docgen.core.domain.template.services;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public interface ITemplateService {
    ByteArrayOutputStream applyStyle(FileInputStream baseTemplate, FileInputStream destTemplate);


}
