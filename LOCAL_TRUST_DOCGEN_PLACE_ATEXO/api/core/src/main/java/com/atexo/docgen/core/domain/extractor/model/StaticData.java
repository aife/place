package com.atexo.docgen.core.domain.extractor.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StaticData {

    private List<String> data;
    @NonNull
    private PositionValue startTargetPosition;

}
