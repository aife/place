package com.atexo.docgen.core.domain.convertor.services.impl.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import org.xmlpull.v1.XmlPullParser;

public class ImageTagFormatHandler implements TagFormatHandler {

    @Override
    public TagFormat parse(XmlPullParser xpp) {
        String src = xpp.getAttributeValue("", "src");
        String width = xpp.getAttributeValue("", "width");
        String height = xpp.getAttributeValue("", "height");
        TagFormat tf = TagFormat.builder()
                .name(xpp.getName())
                .format("img")
                .build();
        tf.addAttribute("src", src);
        tf.addAttribute("width", width);
        tf.addAttribute("height", height);
        return tf;
    }

    @Override
    public FormatedText closing(TagFormat tagFormat) {
        return null;
    }
}
