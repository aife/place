package com.atexo.docgen.core.domain.convertor.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public enum ColorLabelEnum {
    OK("F79649", "F79649"), KO("", "KO");
    private String color;
    private String label;
}
