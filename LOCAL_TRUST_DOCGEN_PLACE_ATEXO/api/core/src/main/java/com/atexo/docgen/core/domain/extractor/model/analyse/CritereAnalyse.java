package com.atexo.docgen.core.domain.extractor.model.analyse;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CritereAnalyse {
    private int maximumNote;
    private String description;
    private int evaluationNote;
    private int evaluationNoteAttributaire;
    private String commentaire;
    private List<CritereAnalyse> sousCriteres;

}
