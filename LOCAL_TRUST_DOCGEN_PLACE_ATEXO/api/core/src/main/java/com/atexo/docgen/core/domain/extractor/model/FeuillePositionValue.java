package com.atexo.docgen.core.domain.extractor.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FeuillePositionValue extends PositionValue {

    @NotNull
    private String sheetName;

    private int sheetIndex;

    private String data;

    private String type;

}
