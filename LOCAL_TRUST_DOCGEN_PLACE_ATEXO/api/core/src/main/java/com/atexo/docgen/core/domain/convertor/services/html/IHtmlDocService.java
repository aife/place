package com.atexo.docgen.core.domain.convertor.services.html;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.math.BigInteger;

public interface IHtmlDocService {
    void insertHtml(String htmlInput, XWPFDocument docX, boolean isNewParagraph, String background, BigInteger numID, boolean cantSplit);
}
