package com.atexo.docgen.core.domain.convertor.services.impl.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.*;
import com.atexo.docgen.core.domain.convertor.services.analyse.IAnalyseSheetService;
import com.atexo.docgen.core.domain.convertor.services.analyse.ICritereService;
import com.atexo.docgen.core.domain.convertor.services.analyse.ISoumissionaireService;
import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.adjustHeight;
import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.setPrintSetup;

@Component
public class AnalyseSheetServiceImpl implements IAnalyseSheetService {
    private final ISoumissionaireService iSoumissionaireService;
    private final ICritereService iCritereService;

    public AnalyseSheetServiceImpl(ISoumissionaireService iSoumissionaireService, ICritereService iCritereService) {
        this.iSoumissionaireService = iSoumissionaireService;
        this.iCritereService = iCritereService;
    }

    @Override
    public Map<String, Integer> setLotsSheets(ConsultationConfig consultation, XSSFWorkbook workbook, Configuration configuration) {
        Map<String, Integer> criteriaTotalLines = new HashMap<>();
        consultation.getLots().forEach(lot -> {
            final String sheetName = "Lot " + lot.getNumero();
            final String title = "Lot " + lot.getNumero() + " - " + lot.getIntitule();
            List<Soumissionnaire> soumissionnaires = lot.getSoumissionnaires();
            final List<CritereConfig> criteres = lot.getCriteres();
            criteriaTotalLines.put(sheetName, this.addCriteriaSheet(workbook, configuration, sheetName, title, criteres, soumissionnaires));
        });
        return criteriaTotalLines;
    }

    @Override
    public void setLotInGeneral(Configuration configuration, Map<String, Integer> totalLotRowLines, XSSFSheet sheet, FormulaEvaluator evaluator, int srcStartRow,
                                int cellnum, List<LotConfig> lots) {
        int size = totalLotRowLines.size();
        for (int i = 0; i < size; i++) {
            LotConfig lot = lots.get(i);
            final int lotRow = srcStartRow + i;
            final String sheetName = "Lot " + lot.getNumero();
            final String title = lot.getNumero() + " - " + lot.getIntitule();
            int totalLotRow = totalLotRowLines.get(sheetName) - 3;
            final List<Soumissionnaire> soumissionnaires = lot.getSoumissionnaires();
            if (i != 0)
                sheet.copyRows(srcStartRow, srcStartRow, lotRow, new CellCopyPolicy());
            setLotRowInGeneral(configuration, sheet, evaluator, srcStartRow, cellnum, lotRow, sheetName, title, totalLotRow, soumissionnaires);
        }

    }

    private void setLotRowInGeneral(Configuration configuration, XSSFSheet sheet, FormulaEvaluator evaluator, int srcStartRow, int cellnum, int lotRow, String sheetName, String title, int totalLotRow, List<Soumissionnaire> soumissionnaires) {
        XSSFRow row = sheet.getRow(lotRow);
        final XSSFCell cell = row.getCell(cellnum);
        cell.setCellValue(title);
        adjustHeight(cell);
        this.setLotStatut(configuration, totalLotRow, evaluator, srcStartRow, cellnum, row, soumissionnaires, sheetName);
        if (!CollectionUtils.isEmpty(soumissionnaires)) {
            String startLetter = CellReference.convertNumToColString(srcStartRow + 1);
            String endLetter = CellReference.convertNumToColString(srcStartRow + soumissionnaires.size());
            setEntrepriseDisante(configuration, totalLotRow, evaluator, cellnum, row, startLetter, endLetter, sheetName);
        }
    }

    @Override
    public Map<String, Integer> setNonAllotiSheet(ConsultationConfig consultation, XSSFWorkbook workbook, Configuration configuration) {
        final String title = "Analyse";
        Map<String, Integer> criteriaTotalLines = new HashMap<>();
        criteriaTotalLines.put(title, this.addCriteriaSheet(workbook, configuration, title, "", consultation.getCriteres(), consultation.getSoumissionnaires()));
        return criteriaTotalLines;
    }

    private int addCriteriaSheet(XSSFWorkbook workbook, Configuration configuration, String sheetName, String title,
                                 List<CritereConfig> criteres,
                                 List<Soumissionnaire> soumissionnaires
    ) {

        XSSFSheet sheet = workbook.cloneSheet(workbook.getSheetIndex("Analyse_lot"), sheetName);
        PositionValue titrePosition = configuration.getTitreLot();
        sheet.getRow(titrePosition.getRow() - 1)
                .getCell(CellReference.convertColStringToIndex(titrePosition.getColumn()))
                .setCellValue(title);
        iSoumissionaireService.setSoumissionnairesColumn(soumissionnaires, sheet, configuration);
        int size = CollectionUtils.isEmpty(soumissionnaires) ? 0 : soumissionnaires.size();
        int total = iCritereService.setCriteriaRows(workbook, criteres, sheet, size, configuration);
        final int colSplit = CellReference.convertColStringToIndex(configuration.getSoumissionnaireDebut().getColumn());
        final int rowSplit = configuration.getCritereDebut().getRow() - 1;
        XSSFSheet baseSheet = workbook.getSheet("Analyse_lot");
        setPrintSetup(baseSheet, sheet, rowSplit, colSplit);
        iCritereService.setCriteriaMergeColumn(criteres, configuration, sheet, soumissionnaires);
        return total;
    }

    @Override
    public void setNonAllotieGeneralSheet(ConsultationConfig consultation, Configuration configuration, Map<String, Integer> totalLotRowLines, XSSFSheet sheet, FormulaEvaluator evaluator, int srcStartRow, int cellnum) {
        final String sheetName = "Analyse";
        final String title = "Consultation non allotie";
        int totalLotRow = totalLotRowLines.get(sheetName) - 3;
        final List<Soumissionnaire> soumissionnaires = consultation.getSoumissionnaires();
        setLotRowInGeneral(configuration, sheet, evaluator, srcStartRow, cellnum, srcStartRow, sheetName, title, totalLotRow, soumissionnaires);
    }

    private void setLotStatut(Configuration configuration, int classmementColumn, FormulaEvaluator evaluator, int srcStartRow, int cellnum, XSSFRow row,
                              List<Soumissionnaire> soumissionnaires, String sheetName) {
        if (!CollectionUtils.isEmpty(soumissionnaires)) {
            String startLetter = CellReference.convertNumToColString(srcStartRow + 1);
            String endLetter = CellReference.convertNumToColString(srcStartRow + soumissionnaires.size());
            setEntrepriseDisante(configuration, classmementColumn, evaluator, cellnum, row, startLetter, endLetter, sheetName);
            String formula = "IF(COUNTBLANK('" + sheetName + "'!" + startLetter + configuration.getCritereDebut().getRow() + ":" + endLetter + "" + classmementColumn + ")=0,\"Terminé\",\"En cours\")";
            XSSFCell cellProgrss = row.getCell(srcStartRow);
            cellProgrss.setCellType(CellType.FORMULA);
            cellProgrss.setCellFormula(formula);
            evaluator.evaluateFormulaCell(cellProgrss);

        }
    }

    private void setEntrepriseDisante(Configuration configuration, int column, FormulaEvaluator evaluator, int cellnum, XSSFRow row, String startLetter, String endLetter,
                                      String sheetName) {
        String formula = "IFERROR(INDEX('" + sheetName + "'!" + startLetter + (configuration.getSoumissionnaireDebut().getRow())
                + ":" + endLetter + (configuration.getSoumissionnaireDebut().getRow()) + ",MATCH(1,'" + sheetName +
                "'!" + startLetter + column + ":" + endLetter + column + ",0)),\"\")";
        XSSFCell cellSoumissionaire = row.getCell(cellnum + 1);
        cellSoumissionaire.setCellType(CellType.FORMULA);
        cellSoumissionaire.setCellFormula(formula);
        evaluator.evaluateFormulaCell(cellSoumissionaire);
        adjustHeight(cellSoumissionaire);
    }


}
