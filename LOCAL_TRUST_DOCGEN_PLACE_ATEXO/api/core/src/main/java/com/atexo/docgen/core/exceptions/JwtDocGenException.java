package com.atexo.docgen.core.exceptions;

import com.atexo.docgen.core.enums.TypeExceptionEnum;

public class JwtDocGenException extends RuntimeException {
    private static final long serialVersionUID = 100L;
    private final TypeExceptionEnum type;


    public JwtDocGenException(String message) {
        super(message);
        this.type = TypeExceptionEnum.EDITOR;
    }

    public JwtDocGenException(String message, Throwable e) {
        super(message, e);
        this.type = TypeExceptionEnum.EDITOR;
    }

    public TypeExceptionEnum getType() {
        return type;
    }

}
