package com.atexo.docgen.core.domain.extractor.model;

import lombok.*;
import org.apache.poi.ss.util.CellReference;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CellPosition {
    private CellReference position;
    private CellReference startTargetPosition;
    private String sheet;

    public CellPosition(PositionToExtract position) {
        this.position = new CellReference(position.getPosition().getColumn() + position.getPosition().getRow());
        this.startTargetPosition = new CellReference(position.getStartTargetPosition().getColumn() + position.getStartTargetPosition().getRow());
        this.sheet = position.getSheetName();
    }
}
