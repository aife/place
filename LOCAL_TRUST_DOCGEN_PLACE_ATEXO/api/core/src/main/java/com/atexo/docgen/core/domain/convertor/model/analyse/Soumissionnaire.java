package com.atexo.docgen.core.domain.convertor.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Soumissionnaire {

    private String raisonSocial;
    private String siret;
    private Enveloppe enveloppe;


    public String getAnalyseOffreEntete() {
        return (enveloppe != null ? "Enveloppe #" + this.enveloppe.getNumero() + "\n" : "") + this.raisonSocial;
    }

}
