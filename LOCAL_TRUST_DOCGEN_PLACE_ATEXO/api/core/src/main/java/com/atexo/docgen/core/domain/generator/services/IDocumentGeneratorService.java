package com.atexo.docgen.core.domain.generator.services;

import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.SheetRequest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IDocumentGeneratorService {
    ByteArrayOutputStream generate(FileGeneratorRequest file, String replacement, String dateFormat) throws IOException;

    Map<String, Set<String>> getFileVariable(InputStream inputStream) throws IOException;

    ByteArrayOutputStream buildDocument(List<SheetRequest> xlsx) throws IOException;
}
