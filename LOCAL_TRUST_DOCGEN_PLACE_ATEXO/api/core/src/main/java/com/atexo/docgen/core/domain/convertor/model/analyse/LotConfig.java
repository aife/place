package com.atexo.docgen.core.domain.convertor.model.analyse;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LotConfig {
    private int numero;
    private String intitule;
    private List<CritereConfig> criteres;
    private List<Soumissionnaire> soumissionnaires;

}
