package com.atexo.docgen.core.domain.security.services;

import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;

public interface ISecurityService {
    boolean canEdit(DocumentEditorDetails editorDetails);

    boolean hasAccess(DocumentEditorDetails documentEditorDetails);

    boolean hasMode(DocumentEditorDetails documentEditorDetails, String mode);
}
