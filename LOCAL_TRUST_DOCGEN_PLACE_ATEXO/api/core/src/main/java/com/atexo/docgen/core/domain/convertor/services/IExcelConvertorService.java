package com.atexo.docgen.core.domain.convertor.services;

import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConvertResult;

import java.io.InputStream;

public interface IExcelConvertorService {

    ConvertResult convertAnalyse(InputStream template, Consultation consultation);

}
