package com.atexo.docgen.core.domain.convertor.model.analyse;

import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CritereConfig {
    private int maximumNote;
    private String description;
    private PositionValue position;
    private List<CritereConfig> sousCriteres;

}
