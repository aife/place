package com.atexo.docgen.core.domain.generator.services.impl;


import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.GenerationRequest;
import com.atexo.docgen.core.domain.generator.services.IDocumentGeneratorService;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class DocumentGeneratorFactory {

    private final DocxGeneratorServiceImpl docxGeneratorService;
    private final XlsxGeneratorServiceImpl xlsxGeneratorService;

    public DocumentGeneratorFactory(DocxGeneratorServiceImpl docxGeneratorService, XlsxGeneratorServiceImpl xlsxGeneratorService) {
        this.docxGeneratorService = docxGeneratorService;
        this.xlsxGeneratorService = xlsxGeneratorService;
    }


    public IDocumentGeneratorService getPrototype(String arg) {
        log.info("Génération du document à partir d'un template {}", arg);
        switch (arg) {
            case "docx":
                return docxGeneratorService;

            case "xlsx":
                return xlsxGeneratorService;
            default:
                log.error("{} n'est pas une extension connue par Document generator", arg);
                throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.GENERATION,
                        "L'extension du document n'est pas supportée. Les extensions supportées sont .docx et .xlsx");
        }
    }

    public ByteArrayOutputStream generate(FileGeneratorRequest file, String replacement, String dateFormat) {
        if (file == null || file.getKeyValues() == null || file.getStream() == null) {
            log.error("Le fichier ou la liste des valeurs ne doivent pas être null");
            throw new MauvaisParametreException(TypeExceptionEnum.GENERATION, "Le fichier ou la liste des valeurs ne doivent pas être null");
        }
        log.info("Début du génération du document à partir du template ");
        if (file.getKeyValues().get("dateGeneration") == null)
            file.getKeyValues().put("dateGeneration", ZonedDateTime.now());
        try {
            return this.getPrototype(file.getExtension()).generate(file, replacement, dateFormat);
        } catch (IOException e) {
            log.error("Erreur lors de la génération du document : {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.GENERATION, e.getMessage());

        }

    }

    public Map<String, Set<String>> getFileVariable(String extension, MultipartFile file) {
        log.info("Récupération de la liste des variables");
        if (file == null) {
            log.error("Le fichier ne doit pas être null");
            throw new MauvaisParametreException(TypeExceptionEnum.CHAMPS_FUSION, "Le fichier ne doit pas être null");
        }
        try {
            return this.getPrototype(extension).getFileVariable(file.getInputStream());

        } catch (IOException e) {
            log.error("Erreur lors de la récupération de la liste des variables : {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CHAMPS_FUSION, e.getMessage());

        }
    }

    public ByteArrayOutputStream buildDocument(String type, GenerationRequest request) throws IOException {
        return this.getPrototype(type).buildDocument(request.getSheets());
    }
}
