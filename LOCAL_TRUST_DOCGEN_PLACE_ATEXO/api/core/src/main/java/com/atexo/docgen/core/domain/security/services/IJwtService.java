package com.atexo.docgen.core.domain.security.services;

import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import org.springframework.security.core.Authentication;

public interface IJwtService {

    DocumentEditorDetails getEditorDetailsFromToken(String token);

    String generateJwtToken(Authentication authentication);

    String generateJwtToken(String key, DocumentEditorRequest editorRequest, String plateformeId, String id);

    boolean validateJwtToken(String authToken);

    String getUserNameFromJwtToken(String jwt);
}
