package com.atexo.docgen.core.domain.convertor.model;

public enum FontFamily {

    ARIAL("arial"), TIMES_NEW_ROMAN("Times New Roman");

    private final String font;

    FontFamily(String font) {
        this.font = font;
    }

    public String getValue() {
        return this.font;
    }

}
