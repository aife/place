package com.atexo.docgen.core.domain.convertor.services.impl.redac;

import com.atexo.docgen.core.domain.convertor.model.ChapitreType;
import com.atexo.docgen.core.domain.convertor.model.ClauseType;
import com.atexo.docgen.core.domain.convertor.services.html.IHtmlDocService;
import com.atexo.docgen.core.domain.convertor.services.redac.IChapitreRenderer;
import com.atexo.docgen.core.domain.convertor.services.redac.IClauseRenderer;
import com.atexo.docgen.core.enums.ClauseEtatEnum;
import com.atexo.docgen.core.utilitaire.word.StylesUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
@Slf4j
public class ChapitreRendererImpl implements IChapitreRenderer {

    private final IClauseRenderer clauseRenderer;
    private final IHtmlDocService iHtmlDocService;

    public ChapitreRendererImpl(IClauseRenderer clauseRenderer, IHtmlDocService iHtmlDocService) {
        this.clauseRenderer = clauseRenderer;
        this.iHtmlDocService = iHtmlDocService;
    }

    public void render(ChapitreType chapitre, XWPFDocument doc, BigInteger cId, int compteur) {
        StringBuilder chapitreTitle = new StringBuilder();
        String[] chapter = StringUtils.split(chapitre.getNumero(), ".");
        int level = chapter.length;

        XWPFParagraph title = doc.createParagraph();

        CTP ctP = title.getCTP();



        String styleId = StylesUtils.getStyleByName(doc, "Heading " + level, "Heading" + level);
        title.setStyle(styleId);
        title.setNumID(cId);
        if (level > 0) {
            if (ctP.getPPr().getNumPr() == null) {
                ctP.getPPr().addNewNumPr();
            }
            if (ctP.getPPr().getNumPr().getIlvl() == null) {
                ctP.getPPr().getNumPr().addNewIlvl();
            }
            ctP.getPPr().getNumPr().getIlvl().setVal(BigInteger.valueOf(level - 1));
        }
        if (compteur == 1) {
            title.setPageBreak(true);
        }

        title.createRun();
        CTBookmark bookmark = ctP.addNewBookmarkStart();
        bookmark.setId(BigInteger.valueOf(compteur));
        bookmark.setName("_Toc" + compteur);
        chapitreTitle.append(chapitre.getTitre());
        XWPFRun run = title.createRun();
        run.setText(chapitreTitle.toString());
        title.createRun();
        ctP.addNewBookmarkEnd().setId(BigInteger.valueOf(compteur));
        title.createRun();
        List<ClauseType> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(chapitre.getClause())) {
            list = chapitre.getClause().stream().filter(clauseType ->
                    !ClauseEtatEnum.ETAT_SUPPRIMER.equals(ClauseEtatEnum.fromValue(clauseType.getEtat())) &&
                            !ClauseEtatEnum.ETAT_CONDITIONNER.equals(ClauseEtatEnum.fromValue(clauseType.getEtat())))
                    .collect(Collectors.toList());
        }
        if (!CollectionUtils.isEmpty(list)) {
            for (ClauseType clauseType : list) {
                clauseRenderer.render(clauseType, doc, chapitre);
            }
        } else if (CollectionUtils.isEmpty(chapitre.getChapitre())) {
            iHtmlDocService.insertHtml("[Chapitre optionnel]", doc, true, null, null, false);
        }

    }


}
