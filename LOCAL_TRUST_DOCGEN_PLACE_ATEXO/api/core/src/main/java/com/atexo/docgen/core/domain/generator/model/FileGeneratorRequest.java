package com.atexo.docgen.core.domain.generator.model;

import lombok.*;

import java.io.InputStream;
import java.util.Map;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FileGeneratorRequest {

    private InputStream stream;
    private String extension;
    private Map<String, Object> keyValues;
}
