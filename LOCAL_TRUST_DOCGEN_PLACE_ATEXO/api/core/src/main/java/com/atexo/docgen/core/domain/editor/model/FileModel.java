package com.atexo.docgen.core.domain.editor.model;

import com.google.gson.Gson;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileModel {
    private String type;
    private String mode;
    private String documentType;
    private DocumentModel document;
    private EditorConfig editorConfig;
    private String token;


    public static String serialize(FileModel model) {
        model.getDocument().setJson(null);
        Gson gson = new Gson();
        return gson.toJson(model);
    }
}
