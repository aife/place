package com.atexo.docgen.core.domain.editor.services;

import com.atexo.docgen.core.domain.editor.model.*;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface IDocumentEditorService {
    DocumentToken getToken(InputStream file, DocumentEditorRequest editorRequest, String curExt, Boolean override) throws IOException;

    FileModel getFileModel(DocumentEditorDetails id);

    DocumentModel getById(String id);

    DocumentModel modifyDocumentStatusById(String id, String name, String mode);

    String setDocumentKey(String id, DocumentModel document);

    int setXlsxFileWithConfiguration(File file, DocumentConfiguration configuration);
}
