package com.atexo.docgen.core.domain.template.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BlocTemplateRequest implements Serializable {
    private String body;
    private String reference;
    private String champsFusion;
    private Integer id;
}
