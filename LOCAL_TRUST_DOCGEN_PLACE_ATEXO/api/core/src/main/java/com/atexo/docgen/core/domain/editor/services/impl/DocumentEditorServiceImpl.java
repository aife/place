package com.atexo.docgen.core.domain.editor.services.impl;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.model.redac.ClauseTag;
import com.atexo.docgen.core.domain.editor.model.*;
import com.atexo.docgen.core.domain.editor.services.IDocumentEditorService;
import com.atexo.docgen.core.domain.monitoring.services.IDocumentMonitoringService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.IJwtService;
import com.atexo.docgen.core.enums.*;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentNotExistantException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import com.atexo.docgen.core.port.IDocumentPort;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.SheetVisibility;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STLock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.List;

import static com.atexo.docgen.core.utilitaire.StringUtils.intersection;

@Component
@Slf4j
public class DocumentEditorServiceImpl implements IDocumentEditorService {

    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentMonitoringService iDocumentMonitoringService;
    private final IDocumentPort iDocumentPort;
    private final IJwtService iJwtService;
    @Value("${external-apis.onlyoffice.storage-folder}")
    private String storagePath;
    @Value("${docgen.public-url}")
    private String publicUrl;
    @Value("${docgen.context-path}")
    private String contextPath;
    private final ObjectMapper objectMapper;

    public DocumentEditorServiceImpl(IDocumentConfigurationService iDocumentConfigurationService, IDocumentPort iDocumentPort, IJwtService iJwtService, ObjectMapper objectMapper, IDocumentMonitoringService iDocumentMonitoringService) {
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentPort = iDocumentPort;
        this.iJwtService = iJwtService;
        this.objectMapper = objectMapper;
        this.iDocumentMonitoringService = iDocumentMonitoringService;
    }

    @Override
    public DocumentToken getToken(InputStream file, DocumentEditorRequest editorRequest, String curExt, Boolean override) {
        if (editorRequest.getLang() == null) {
            editorRequest.setLang("fr");
        }
        DocumentConfiguration configuration = editorRequest.getConfiguration();
        if (configuration != null && "xlsx".equals(curExt)) {
            if (!CollectionUtils.isEmpty(configuration.getActivatedSheetNames())
                    && StringUtils.hasText(configuration.getDefaultSheetName())
                    && !configuration.getActivatedSheetNames().contains(configuration.getDefaultSheetName())) {
                throw new DocGenException(TypeExceptionEnum.EDITOR, "La feuille par défaut n'est pas inclus dans la liste des onglets activés");
            }
        }

        String id = iDocumentConfigurationService.generateDocumentKey(editorRequest.getPlateformeId() + "/" +
                editorRequest.getDocumentId());

        if (editorRequest.getMode().equals(FileModeEnum.REDAC.getValue())) {
            file = setDocumentPermission(file, editorRequest.getUser().getRoles());
        }
        File savedFile = DocumentUtils.saveFile(file, storagePath, id, override);
        if (configuration != null && "xlsx".equals(curExt)) {
            editorRequest.getConfiguration().setInitialDefaultSheetIndex(setXlsxFileWithConfiguration(savedFile, configuration));
        }

        long length = savedFile.length();
        if (editorRequest.getVersion() == null) {
            editorRequest.setVersion(0);
        }
        int version = editorRequest.getVersion();
        DocumentModel document = iDocumentPort.addDocumentIfNotExist(editorRequest, curExt, editorRequest.getPlateformeId(), id, length, version);

        return DocumentToken.builder()
                .token(iJwtService.generateJwtToken(document.getKey(), editorRequest, editorRequest.getPlateformeId(), document.getId()))
                .key(document.getKey())
                .build();
    }

    private ByteArrayInputStream setDocumentPermission(InputStream file, List<String> roles) {
        ZipSecureFile.setMinInflateRatio(0);
        try (XWPFDocument document = new XWPFDocument(file); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            document.getDocument().getBody().getSdtList().stream()
                    .filter(ctSdtBlock -> ctSdtBlock.getSdtPr() != null &&
                            !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getTagList()))
                    .forEach(ctSdtBlock -> {
                        ClauseTag tag = null;
                        String tagValue = ctSdtBlock.getSdtPr().getTagList().get(0).getVal();
                        try {
                            tag = objectMapper.readValue(tagValue, ClauseTag.class);
                        } catch (JsonProcessingException e) {
                            log.error("Erreur lors de la récupération du tag {} => {}", tagValue, e.getMessage());
                        }
                        if (tag != null && roles != null && !CollectionUtils.isEmpty(intersection(roles, tag.getRoles()))) {
                            ctSdtBlock.getSdtPr().getLockList().forEach(ctLock -> ctLock.setVal(STLock.SDT_LOCKED));
                        } else {
                            ctSdtBlock.getSdtPr().getLockList().forEach(ctLock -> ctLock.setVal(STLock.SDT_CONTENT_LOCKED));
                        }
                    });
            document.write(out);
            document.close();
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            log.error("Erreur lors de la réinitialisation des droits -> {}", e.getMessage());
        }
        return null;
    }

    @Override
    public FileModel getFileModel(DocumentEditorDetails details) {
        if (details == null) {
            log.error("Id du document ne doit pas être null");
            throw new MauvaisParametreException(TypeExceptionEnum.EDITOR, "Id ne doit pas être null");
        }
        DocumentModel documentModel = iDocumentPort.getById(details.getId());
        if (documentModel == null) {
            log.error("L'id du document {} ne correspond pas à un document enregistré dans la plateforme {}", details.getDocumentId(), details.getPlateformeId());
            throw new DocumentNotExistantException(TypeExceptionEnum.EDITOR, "L'id " + details.getId() + " ne correspond pas à un document enregistré");
        }
        String mode = details.getMode();
        List<String> userRoles = details.getUser().getRoles();
        PermissionsModel permissions = this.buildPermissionsModel(mode, userRoles, documentModel.getRedacStatus());
        documentModel.setPermissions(permissions);
        String url = publicUrl + contextPath;
        documentModel.setUrl(url);
        User user = details.getUser();
        log.info("Création du Document {} de la plateforme {}", documentModel.getDocumentId(), documentModel.getPlateformeId());
        FileModel file = this.buildFileModel(mode, url, documentModel, user);
        log.info("End getFileModel");
        return file;
    }

    @Override
    public DocumentModel getById(String id) {
        return iDocumentPort.getById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DocumentModel modifyDocumentStatusById(String id, String status, String mode) {
        if (FileStatusEnum.SAVED_AND_CLOSED.equals(FileStatusEnum.valueOf(status))) {
            DocumentModel documentModel = iDocumentPort.getById(id);
            iDocumentMonitoringService.addNewVersion(id);
        }
        return iDocumentPort.modifyDocumentStatusById(id, status);

    }

    @Override
    public String setDocumentKey(String id, DocumentModel document) {
        iDocumentPort.setNewVersion(id, true);
        return iDocumentPort.setNewKey(id, document.getCompositeNewKey());
    }

    private FileModel buildFileModel(String mode,
                                     String callbackUrl, DocumentModel document, User user) {

        Customization customization = Customization.builder()
                .autosave(true)
                .customer(Customer.builder()
                        .address("63, boulevard Haussmann 75008 Paris FRANCE")
                        .info("ATEXO EDITION")
                        .name("ATEXO")
                        .logo("https://www.atexo.com/App/assets/img/atexo-logo.png")
                        .www("https://www.atexo.com/")
                        .mail("contact@atexo.com")
                        .build())
                .goback(new Goback())
                .build();

        EditorConfig editorConfig = EditorConfig.builder()
                .lang("fr")
                .mode(setEditionMode(mode))
                .callbackUrl(callbackUrl)
                .user(user)
                .customization(customization)
                .chat(false)
                .comments(false)
                .leftMenu(false)
                .rightMenu(false)
                .statusBar(false)
                .toolbar(false)
                .build();

        return FileModel.builder()
                .documentType(DocumentUtils.getFileTypeFromExtension(document.getFileType()).name().toLowerCase())
                .type("desktop")
                .mode(mode)
                .editorConfig(editorConfig)
                .document(document)
                .build();
    }

    private String setEditionMode(String mode) {
        return FileModeEnum.VIEW.getValue().equals(mode) ? "view" : "edit";
    }

    private PermissionsModel buildPermissionsModel(String mode, List<String> userRoles, int redacStatus) {
        return PermissionsModel.builder()
                .comment(getCommentsPerimissionFromModeAndUser(mode, userRoles, redacStatus))
                .download(download(mode))
                .edit(getPerimissionFromModeAndUser(mode, userRoles, redacStatus))
                .fillForms(getPerimissionFromModeAndUser(mode, userRoles, redacStatus))
                .modifyFilter(getPerimissionFromModeAndUser(mode, userRoles, redacStatus))
                .modifyContentControl(getPerimissionFromModeAndUser(mode, userRoles, redacStatus))
                .review(getPerimissionFromModeAndUser(mode, userRoles, redacStatus))
                .print(download(mode))
                .build();
    }

    private boolean getPerimissionFromModeAndUser(String mode, List<String> userRoles, int redacStatus) {
        return (mode.equals(FileModeEnum.REDAC.getValue()) && FileRedacStatusEnum.fromValue(redacStatus).getCode() < 2
                && userRoles != null && userRoles.contains(UserRoleEnum.MODIFICATION.getValue())) ||
                mode.equals(FileModeEnum.EDIT.getValue()) ||
                (mode.equals(FileModeEnum.VALIDATION.getValue()) && FileRedacStatusEnum.fromValue(redacStatus).getCode() < 2
                        && userRoles != null && userRoles.contains(UserRoleEnum.MODIFICATION.getValue())) ||
                mode.equals(FileModeEnum.TEMPLATE.getValue());
    }

    private boolean download(String mode) {
        return mode.equals(FileModeEnum.EDIT.getValue()) || mode.equals(FileModeEnum.VALIDATION.getValue());
    }

    private boolean getCommentsPerimissionFromModeAndUser(String mode, List<String> userRoles, int redacStatus) {
        return mode.equals(FileModeEnum.EDIT.getValue()) ||
                (mode.equals(FileModeEnum.VALIDATION.getValue()) && FileRedacStatusEnum.fromValue(redacStatus).getCode() < 2
                        && userRoles != null && userRoles.contains(UserRoleEnum.MODIFICATION.getValue())) ||
                mode.equals(FileModeEnum.TEMPLATE.getValue()) ||
                mode.equals(FileModeEnum.REDAC.getValue());
    }

    @Override
    public int setXlsxFileWithConfiguration(File file, DocumentConfiguration configuration) {
        List<String> activatedSheetNames = configuration.getActivatedSheetNames();
        String defaultSheetName = configuration.getDefaultSheetName();
        if (CollectionUtils.isEmpty(activatedSheetNames) && !StringUtils.hasText(defaultSheetName))
            return 0;
        try {
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            for (int i = workbook.getNumberOfSheets() - 1; i >= 0; i--) {
                String sheetName = workbook.getSheetAt(i).getSheetName();
                if (!activatedSheetNames.contains(sheetName)) {
                    workbook.setSheetVisibility(i, SheetVisibility.VERY_HIDDEN);
                }
            }
            int initialDefaultSheetIndex = workbook.getActiveSheetIndex();
            if (StringUtils.hasText(defaultSheetName)) {
                int indexDefault = workbook.getSheetIndex(defaultSheetName);
                if (indexDefault > -1 && initialDefaultSheetIndex != indexDefault) {
                    workbook.setActiveSheet(indexDefault);
                }
            } else {
                int indexDefault = workbook.getSheetIndex(activatedSheetNames.get(0));
                if (indexDefault > -1 && initialDefaultSheetIndex != indexDefault) {
                    workbook.setActiveSheet(indexDefault);
                }
            }
            inputStream.close();
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
            workbook.close();
            return initialDefaultSheetIndex;
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return 0;
    }


}
