package com.atexo.docgen.core.domain.editor.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Embedded {
    private String saveUrl;
    private String embedUrl;
    private String shareUrl;
    private String toolbarDocked;
}
