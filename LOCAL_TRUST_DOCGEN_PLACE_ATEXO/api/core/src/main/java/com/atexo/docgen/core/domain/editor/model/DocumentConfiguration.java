package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentConfiguration {

    private List<String> activatedSheetNames;

    private String defaultSheetName;

    private int initialDefaultSheetIndex;



}
