package com.atexo.docgen.core.domain.convertor.services.impl.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.model.WordStyle;
import com.atexo.docgen.core.domain.convertor.services.html.IHtmlDocService;
import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import com.atexo.docgen.core.exceptions.MalformedHtmlException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.springframework.stereotype.Service;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.*;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.writeFormatedText;

@Slf4j
@Service
public class HtmlDocServiceIml implements IHtmlDocService {
    private final WordStyle defaultStyle = new WordStyle();
    private final Map<String, TagFormatHandler> knownTags;

    public HtmlDocServiceIml(Map<String, TagFormatHandler> knownTags) {
        this.knownTags = knownTags;
    }

    @Override
    public void insertHtml(String htmlInput, XWPFDocument docX, boolean isNewParagraph, String background, BigInteger numID, boolean cantSplit) {
        if (StringUtils.isBlank(htmlInput) || docX == null) {
            return;
        }
        XWPFParagraph paragraph;
        if (isNewParagraph) {
            paragraph = docX.createParagraph();
        } else if (CollectionUtils.isNotEmpty(docX.getParagraphs())) {
            htmlInput = " " + htmlInput;
            paragraph = docX.getLastParagraph();
            List<XWPFParagraph> paragraphs = docX.getParagraphs();
            while (StringUtils.isBlank(paragraph.getText())) {
                int currentIndex = paragraphs.indexOf(paragraph);
                if (currentIndex > 0) {
                    paragraph = paragraphs.get(currentIndex - 1);
                } else {
                    break;
                }
            }
        } else {
            paragraph = docX.createParagraph();
        }
        if (cantSplit) {
            paragraph.getCTP().addNewPPr().addNewKeepLines().setVal(STOnOff.ON);
            paragraph.getCTP().getPPr().addNewKeepNext().setVal(STOnOff.ON);

        }
        insertHtmlInParagraph(docX, htmlInput, paragraph, background, numID, 0, isNewParagraph);
    }

    private void insertHtmlInParagraph(XWPFDocument document, String htmlInput, XWPFParagraph paragraph, String background, BigInteger numID, int level, Boolean isNewParagraph) {
        String htmlValue = "<html>" + htmlInput + "</html>";
        htmlValue = htmlValue.replace("&nbsp;", " ");
        htmlValue = htmlValue.replace("& ", "&#38; ");
        htmlValue = htmlValue.replace("<br>", "<br></br>");

        List<FormatedText> formattedTexts = this.parse(new StringReader(htmlValue));

        if (CollectionUtils.isNotEmpty(formattedTexts)) {
            for (int i = 0; i < formattedTexts.size(); i++) {
                FormatedText ft = formattedTexts.get(i);
                paragraph = writeFormatedText(document, paragraph, defaultStyle, ft, background, numID, level, i == 0 ? null : formattedTexts.get(i - 1));
                if (i != formattedTexts.size() - 1) {
                    if (ft.isCarriageReturn()) {
                        paragraph = document.createParagraph();
                        paragraph.setSpacingAfter(0);
                    }
                }

            }
        }
    }


    public List<FormatedText> parse(Reader r) {
        XmlPullParserFactory factory;
        try {
            factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(r);
            return processDocument(xpp);
        } catch (XmlPullParserException e) {
            log.error("Erreur lors du parsing du html : {}", e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<FormatedText> processDocument(XmlPullParser xpp) {
        ArrayDeque<TagFormat> formatterStack = new ArrayDeque<>();
        List<FormatedText> result = new ArrayList<>();
        int eventType = 0;
        UUID idLi = null;
        try {
            eventType = xpp.getEventType();
            do {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equals("li")) {
                        idLi = UUID.randomUUID();
                    }
                    processStartStyledTag(xpp, formatterStack);
                } else if (eventType == XmlPullParser.END_TAG) {
                    processEndStyledTag(xpp, formatterStack, result);
                    if (xpp.getName().equals("li")) {
                        idLi = null;
                    }
                } else if (eventType == XmlPullParser.TEXT) {

                    List<TagFormat> tagFormatList = new ArrayList<>(formatterStack);
                    String text = xpp.getText();
                    FormatedText ft = FormatedText.builder()
                            .text(text)
                            .format(tagFormatList)
                            .idLi(idLi == null ? null : idLi.toString())
                            .build();

                    result.add(ft);
                }
                eventType = xpp.next();
            } while (eventType != XmlPullParser.END_DOCUMENT);
        } catch (XmlPullParserException | MalformedHtmlException | IOException e) {
            log.error("html en erreur {}", e.getMessage());
        }
        return result;
    }

    public void processStartStyledTag(XmlPullParser xpp, Deque<TagFormat> stack) {
        String name = xpp.getName();
        TagFormatHandler handler = knownTags.get(name);
        if (handler != null) {
            TagFormat tf = handler.parse(xpp);
            if (tf != null) {
                stack.push(tf);
            }
        }
    }

    public void processEndStyledTag(XmlPullParser xpp, Deque<TagFormat> stack, List<FormatedText> result)
            throws MalformedHtmlException {
        String name = xpp.getName();
        TagFormatHandler handler = knownTags.get(name);
        if (handler != null) {
            TagFormat currentFormat = stack.peek();

            if (currentFormat != null) {
                if (name.equals(currentFormat.getName())) {
                    FormatedText ft = handler.closing(currentFormat);
                    if (currentFormat.getName().equals("p") && StringUtils.isBlank(result.get(result.size() - 1).getText())) {
                        ft.setCarriageReturn(false);
                    }
                    if (ft != null) {
                        result.add(ft);
                    }
                    stack.pop();
                } else {
                    throw new MalformedHtmlException();
                }
            }
        }
    }

}
