package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum FileRedacStatusEnum {
    NOT_DEFINIED(0), BROUILLON(1), A_VALIDER(2), VALIDER(3);
    private final Integer code;

    FileRedacStatusEnum(Integer code) {
        this.code = code;
    }

    @JsonCreator
    public static FileRedacStatusEnum fromValue(int value) {
        for (FileRedacStatusEnum operator : values()) {
            if (operator.getCode() == value) {
                return operator;
            }
        }
        log.error("Cannot find FileStatusEnum from value: {}", value);
        return NOT_DEFINIED;
    }

    @JsonValue
    public int getCode() {
        return code;
    }
}
