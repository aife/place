package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum RedacActionEnum {
    VALIDATATION, INVALIDATION, SAVE, SUBMIT;

    @JsonCreator
    public static RedacActionEnum fromValue(String value) {
        for (RedacActionEnum operator : values()) {
            if (operator.name().equals(value)) {
                return operator;
            }
        }
        log.error("Cannot find FileStatusEnum from value: {}", value);
        throw new IllegalArgumentException("Cannot find FileStatusEnum from value: " + value);
    }
}
