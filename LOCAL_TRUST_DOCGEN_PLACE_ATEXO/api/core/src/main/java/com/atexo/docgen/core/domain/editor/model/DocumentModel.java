package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class DocumentModel implements Serializable {
    private String title;
    private String documentId;
    private String id;
    private String plateformeId;
    private String json;
    private String updatedJson;
    private String url;
    private String fileType;
    private String key;
    private PermissionsModel permissions;
    private String status;
    private int redacStatus;
    private String redacAction;
    private int version;
    private Timestamp creationDate;
    private Timestamp modificationDate;
    private String mode;
    private boolean newVersion;
    private boolean saved;

    public String getCompositeNewKey() {
        return this.getPlateformeId() + "/" +
                this.getDocumentId() + "/" + System.currentTimeMillis();
    }

}
