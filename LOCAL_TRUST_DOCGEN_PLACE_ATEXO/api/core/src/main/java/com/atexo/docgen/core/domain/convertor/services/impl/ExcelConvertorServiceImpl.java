package com.atexo.docgen.core.domain.convertor.services.impl;

import com.atexo.docgen.core.domain.convertor.model.analyse.*;
import com.atexo.docgen.core.domain.convertor.services.IExcelConvertorService;
import com.atexo.docgen.core.domain.convertor.services.analyse.IAnalyseSheetService;
import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import com.atexo.docgen.core.domain.generator.model.ExcelPosition;
import com.atexo.docgen.core.domain.generator.services.impl.XlsxGeneratorServiceImpl;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.port.IAnalysePort;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.setConditionnalFormatting;

@Component
@Slf4j
public class ExcelConvertorServiceImpl implements IExcelConvertorService {


    private final IAnalysePort iAnalysePort;
    private final XlsxGeneratorServiceImpl xlsxGeneratorService;
    private final IAnalyseSheetService iAnalyseSheetService;

    public ExcelConvertorServiceImpl(IAnalysePort iAnalysePort, XlsxGeneratorServiceImpl xlsxGeneratorService, IAnalyseSheetService iAnalyseSheetService) {
        this.iAnalysePort = iAnalysePort;
        this.xlsxGeneratorService = xlsxGeneratorService;
        this.iAnalyseSheetService = iAnalyseSheetService;
    }

    @Override
    public ConvertResult convertAnalyse(InputStream template, Consultation meta) {
        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                XSSFWorkbook workbook = new XSSFWorkbook(template);
        ) {
            Configuration configuration = this.getAnalyseConfiguration(workbook);

            ConsultationConfig metaConfig = iAnalysePort.map(meta);
            Map<String, Integer> totalLigne;
            if (!CollectionUtils.isEmpty(meta.getLots())) {
                totalLigne = iAnalyseSheetService.setLotsSheets(metaConfig, workbook, configuration);
            } else {
                totalLigne = iAnalyseSheetService.setNonAllotiSheet(metaConfig, workbook, configuration);
            }
            workbook.removeSheetAt(workbook.getSheetIndex("Analyse_lot"));

            setGeneralSheet(metaConfig, workbook, totalLigne, configuration);

            workbook.write(out);
            configuration.setMeta(metaConfig);
            return ConvertResult.builder().convertedDocument(out).configuration(configuration).build();
        } catch (IOException e) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage());
        }
    }

    private Configuration getAnalyseConfiguration(XSSFWorkbook workbook) {
        Map<String, List<ExcelPosition>> variables = xlsxGeneratorService.getVariables(workbook);
        if (CollectionUtils.isEmpty(variables)) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, "le template ne contient pas la configuration");
        }
        return Configuration.builder()
                .apercuDebut(getExcelPosition(variables, "apercuDebut"))
                .celluleControle(getExcelPosition(variables, "controle"))
                .critereDebut(getExcelPosition(variables, "critereDebut"))
                .critereFin(getExcelPosition(variables, "critereFin"))
                .titreLot(getExcelPosition(variables, "titreLot"))
                .soumissionnaireDebut(getExcelPosition(variables, "soumissionnaires"))
                .build();
    }


    private PositionValue getExcelPosition(Map<String, List<ExcelPosition>> champsFusionConfiguration, String field) {

        if (CollectionUtils.isEmpty(champsFusionConfiguration)
                || CollectionUtils.isEmpty(champsFusionConfiguration.get(field)) ||
                champsFusionConfiguration.get(field).size() > 1) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, "Le document ne contient pas " + field);
        }
        ExcelPosition excelPosition = champsFusionConfiguration.get(field).get(0);
        return PositionValue.builder().column(CellReference.convertNumToColString(excelPosition.getColumn())).row(excelPosition.getRow() + 1).build();
    }

    private void setGeneralSheet(ConsultationConfig consultation, XSSFWorkbook workbook, Map<String, Integer> totalLigne, Configuration configuration) {
        XSSFSheet sheet = workbook.getSheetAt(0);
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        int srcStartRow = configuration.getApercuDebut().getRow() - 1;
        int cellnum = CellReference.convertColStringToIndex(configuration.getApercuDebut().getColumn());

        final List<LotConfig> lots = consultation.getLots();
        if (!CollectionUtils.isEmpty(lots)) {
            iAnalyseSheetService.setLotInGeneral(configuration, totalLigne, sheet, evaluator, srcStartRow, cellnum, lots);
        } else {
            iAnalyseSheetService.setNonAllotieGeneralSheet(consultation, configuration, totalLigne, sheet, evaluator, srcStartRow, cellnum);
        }

        int total = CollectionUtils.isEmpty(lots) ? 1 : lots.size();


        CellRangeAddress[] regions = new CellRangeAddress[]{CellRangeAddress.valueOf(CellReference.convertNumToColString(cellnum + 2) +
                (srcStartRow + 1) + ":" + CellReference.convertNumToColString(cellnum + 2) +
                (srcStartRow + total))};
        setConditionnalFormatting(sheet, regions);

    }


}
