


package com.atexo.docgen.core.enums;

public enum FileTypeEnum {
    TEXT,
    SPREADSHEET,
    PRESENTATION
}
