package com.atexo.docgen.core.domain.security.services.impl;


import com.atexo.docgen.core.port.IUserPort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("atexoUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    private final IUserPort iUserPort;

    public UserDetailsServiceImpl(IUserPort iUserPort) {
        this.iUserPort = iUserPort;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        return iUserPort.findByUsername(username);
    }

}
