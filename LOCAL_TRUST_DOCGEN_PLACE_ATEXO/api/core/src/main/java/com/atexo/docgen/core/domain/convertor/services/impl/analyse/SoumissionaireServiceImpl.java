package com.atexo.docgen.core.domain.convertor.services.impl.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.convertor.model.analyse.Soumissionnaire;
import com.atexo.docgen.core.domain.convertor.services.analyse.ISoumissionaireService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.*;

@Component
public class SoumissionaireServiceImpl implements ISoumissionaireService {
    @Override
    public void setSoumissionnairesColumn(List<Soumissionnaire> soumissionnaires, XSSFSheet sheet, Configuration configuration) {
        int soumissionnaireColumn = CellReference.convertColStringToIndex(configuration.getSoumissionnaireDebut().getColumn());
        int soumissionnaireStartRow = configuration.getSoumissionnaireDebut().getRow() - 1;
        if (!CollectionUtils.isEmpty(soumissionnaires)) {
            for (int i = soumissionnaireStartRow; i < configuration.getCelluleControle().getRow(); i++) {
                XSSFRow row = sheet.getRow(i);
                int soumissionaireIndex = 0;
                XSSFCell cell = row.getCell(soumissionnaireColumn);

                for (Soumissionnaire soumissionaire : soumissionnaires) {
                    sheet.setColumnWidth(soumissionnaireColumn + soumissionaireIndex, sheet.getColumnWidth(soumissionnaireColumn));
                    XSSFCell cell1 = row.getCell(soumissionnaireColumn + soumissionaireIndex);
                    if (cell1 == null) {
                        cell1 = row.createCell(soumissionnaireColumn + soumissionaireIndex);
                    }
                    copyCells(cell, cell1);
                    if (i == soumissionnaireStartRow) {
                        cell1.setCellValue(soumissionaire.getAnalyseOffreEntete());
                        adjustHeight(cell1);
                    }
                    soumissionaireIndex++;
                }

            }
        } else {
            for (int i = soumissionnaireStartRow; i < configuration.getCelluleControle().getRow(); i++) {
                XSSFRow row = sheet.getRow(i);
                if (row != null) {
                    Cell cell = row.getCell(soumissionnaireColumn);
                    if (cell != null) {
                        row.removeCell(cell);
                    }
                }
            }
        }
    }

    @Override
    public void setSoumissionnairesFormula(XSSFSheet sheet, int nbrSoumissionaires, int destStartRow, FormulaEvaluator evaluator, Configuration configuration, int soumissionaireStartColumn) {
        for (int i = 0; i < nbrSoumissionaires; i++) {
            final String message = "\"OK\",\"KO\"";
            XSSFCell remplissageFormula = setFormula(sheet, evaluator, Arrays.asList(configuration.getCelluleControle().getRow() + 1, destStartRow - 5),
                    soumissionaireStartColumn + i, destStartRow - 1,
                    "IF(COUNTBLANK(", ")=0," + message + ")", ":");

            String startLetter = CellReference.convertNumToColString(soumissionaireStartColumn);
            String currentLetter = CellReference.convertNumToColString(soumissionaireStartColumn + i);
            String endLetter = CellReference.convertNumToColString(soumissionaireStartColumn + nbrSoumissionaires - 1);
            int rowIndex = destStartRow - 4;
            XSSFRow row = sheet.getRow(rowIndex);
            XSSFCell cell = row.getCell(soumissionaireStartColumn + i);
            if (cell == null) {
                cell = row.createCell(soumissionaireStartColumn + i);
            }
            cell.setCellType(CellType.FORMULA);
            final String replacement = ",1,0";
            final String noteFormula = remplissageFormula.getCellFormula().replaceAll("," + message, replacement);
            String formula = "RANK(" + currentLetter + rowIndex + "," + startLetter + rowIndex + ":" + endLetter + rowIndex + ")*" + noteFormula;
            cell.setCellFormula(formula);
            evaluator.evaluateFormulaCell(cell);

        }
        this.setSoumissionnaireControlConditionnalFormatting(sheet, nbrSoumissionaires, destStartRow, soumissionaireStartColumn);
    }

    private void setSoumissionnaireControlConditionnalFormatting(XSSFSheet sheet, int nbrSoumissionaires, int destStartRow, int soumissionaireStartColumn) {
        String startLetter = CellReference.convertNumToColString(soumissionaireStartColumn - 1);
        String endLetter = CellReference.convertNumToColString(soumissionaireStartColumn + nbrSoumissionaires - 1);
        String ref = startLetter + (destStartRow - 8) + ":" + (endLetter) + (destStartRow - 8);


        CellRangeAddress[] regions = new CellRangeAddress[]{CellRangeAddress.valueOf(ref)};
        setConditionnalFormatting(sheet, regions);
    }


}
