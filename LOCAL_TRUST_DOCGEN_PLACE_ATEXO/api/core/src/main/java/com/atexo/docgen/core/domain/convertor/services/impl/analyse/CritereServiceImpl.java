package com.atexo.docgen.core.domain.convertor.services.impl.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.convertor.model.analyse.CritereConfig;
import com.atexo.docgen.core.domain.convertor.model.analyse.Soumissionnaire;
import com.atexo.docgen.core.domain.convertor.services.analyse.ICritereService;
import com.atexo.docgen.core.domain.convertor.services.analyse.ISoumissionaireService;
import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.adjustHeight;
import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.setFormula;

@Component
public class CritereServiceImpl implements ICritereService {
    private final ISoumissionaireService iSoumissionaireService;

    public CritereServiceImpl(ISoumissionaireService iSoumissionaireService) {
        this.iSoumissionaireService = iSoumissionaireService;
    }

    @Override
    public int setCriteriaRows(XSSFWorkbook workbook, List<CritereConfig> criteres, XSSFSheet sheet, int nbrSoumissionaires, Configuration configuration) {
        int srcStartRow = configuration.getCritereDebut().getRow() - 1;
        int srcEndRow = configuration.getCritereFin().getRow() - 1;
        int destStartRow = configuration.getCelluleControle().getRow();
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        List<Integer> rowNotes = new ArrayList<>();
        List<String> ranges = new ArrayList<>();
        for (CritereConfig critere : criteres) {
            int underCriteriaIndex = 0;
            List<Integer> rowsubNotes = new ArrayList<>();
            if (!CollectionUtils.isEmpty(critere.getSousCriteres())) {

                for (CritereConfig sousCritere : critere.getSousCriteres()) {
                    rowsubNotes.add(addSubCriteria(sheet, srcStartRow, srcEndRow, destStartRow, critere, underCriteriaIndex, sousCritere, configuration));
                    underCriteriaIndex++;
                    destStartRow += 2;
                }
            } else {
                rowsubNotes.add(addSubCriteria(sheet, srcStartRow, srcEndRow, destStartRow, critere, underCriteriaIndex, CritereConfig.builder()
                        .description("Pas de sous-critère défini")
                        .maximumNote(critere.getMaximumNote())
                        .build(), configuration));
                destStartRow += 2;

            }
            String range = addSumCriteriaRow(sheet, nbrSoumissionaires, srcEndRow, destStartRow, evaluator, rowNotes, critere, rowsubNotes, configuration);
            ranges.add(range);
            destStartRow += 1;


        }


        return this.addSummaryRows(sheet, nbrSoumissionaires, destStartRow, evaluator, rowNotes, configuration, ranges, criteres);

    }

    private int addSubCriteria(XSSFSheet sheet, int srcStartRow, int srcEndRow, int destStartRow, CritereConfig critere, int underCriteriaIndex, CritereConfig sousCritere, Configuration configuration) {
        sheet.copyRows(srcStartRow, srcEndRow - 1, destStartRow, new CellCopyPolicy());
        final int critereDescriptionColumn = CellReference.convertColStringToIndex(configuration.getCritereDebut().getColumn());
        int subCriteriaDescriptionColumnIndex = critereDescriptionColumn + 1;
        setRowDescription(sheet, destStartRow, subCriteriaDescriptionColumnIndex, sousCritere.getDescription(), critereDescriptionColumn, underCriteriaIndex == 0 ? critere.getDescription() : "");
        XSSFRow rowNote = sheet.getRow(destStartRow + 1);
        int additionalRows = configuration.getCelluleControle().getRow() - configuration.getCritereDebut().getRow();

        int noteColumn = critereDescriptionColumn + 2;
        rowNote.getCell(noteColumn).setCellValue("/" + sousCritere.getMaximumNote());

        sousCritere.setPosition(PositionValue.builder()
                .row(destStartRow - additionalRows)
                .column(configuration.getCelluleControle().getColumn()).build());
        return destStartRow + 2;
    }


    private void setRowDescription(XSSFSheet sheet, int rowIndex, int subCriteriaDescriptionIndex, String subCriteriaDescription, int criteriaDescriptionIndex, String criteriaDescription) {
        XSSFRow row = sheet.getRow(rowIndex);

        final XSSFCell descriptionCell = row.getCell(criteriaDescriptionIndex);
        descriptionCell.setCellValue(criteriaDescription);
        XSSFCell cell = row.getCell(subCriteriaDescriptionIndex);
        cell.setCellValue(subCriteriaDescription);
        adjustHeight(cell);

    }


    private String addSumCriteriaRow(XSSFSheet sheet, int nbrSoumissionaires, int srcEndRow, int destStartRow, FormulaEvaluator evaluator, List<Integer> rowNotes, CritereConfig critere, List<Integer> rowsubNotes, Configuration configuration) {
        sheet.copyRows(srcEndRow, srcEndRow, destStartRow, new CellCopyPolicy());
        setCriteriaNote(sheet, destStartRow, rowNotes, critere, configuration);


        PositionValue soumissionnairesPosition = configuration.getSoumissionnaireDebut();
        int soumissionaireStartColumn = CellReference.convertColStringToIndex(soumissionnairesPosition.getColumn());
        for (int i = 0; i < nbrSoumissionaires; i++) {
            setFormula(sheet, evaluator, rowsubNotes, soumissionaireStartColumn + i, destStartRow, "SUM(", ")", ",");
        }

        int controleSousNoteColumnIndex = CellReference.convertColStringToIndex(configuration.getSoumissionnaireDebut().getColumn()) + 1;
        int cellnum = controleSousNoteColumnIndex + nbrSoumissionaires;


        return CellReference.convertNumToColString(cellnum) + (destStartRow - 7);
    }

    private XSSFRow setCriteriaNote(XSSFSheet sheet, int destStartRow, List<Integer> rowsubNotes, CritereConfig critere, Configuration configuration) {
        XSSFRow rowNote = sheet.getRow(destStartRow);
        int additionalRows = configuration.getCelluleControle().getRow() - configuration.getCritereDebut().getRow();
        XSSFCell cell = rowNote.getCell(CellReference.convertColStringToIndex(configuration.getCritereDebut().getColumn()) + 2);
        cell.setCellValue("/" + critere.getMaximumNote());
        critere.setPosition(PositionValue.builder().column(configuration.getCelluleControle().getColumn())
                .row(rowNote.getRowNum() - additionalRows).build());
        rowsubNotes.add(rowNote.getRowNum() + 1);
        return rowNote;
    }

    @Override
    public void setCriteriaMergeColumn(List<CritereConfig> criteres, Configuration configuration, XSSFSheet sheet, List<Soumissionnaire> soumissionnaires) {
        criteres.forEach(critereConfig -> {
            if (!CollectionUtils.isEmpty(critereConfig.getSousCriteres()) && critereConfig.getSousCriteres().size() > 1) {
                final int lastMergedRow = critereConfig.getSousCriteres().get(critereConfig.getSousCriteres().size() - 1).getPosition().getRow() + 2;
                final Integer firstMergedRow = critereConfig.getSousCriteres().get(0).getPosition().getRow();
                final String mergeColumn = configuration.getCritereDebut().getColumn();
                sheet.addMergedRegion(CellRangeAddress.valueOf(mergeColumn + firstMergedRow + ":" + mergeColumn + lastMergedRow));
                for (int i = firstMergedRow; i <= lastMergedRow; i++) {
                    sheet.getRow(i).setHeight((short) -1);
                }

            }

        });
    }


    private int addSummaryRows(XSSFSheet sheet, int nbrSoumissionaires, int destStartRow, FormulaEvaluator evaluator, List<Integer> rowNotes, Configuration configuration, List<String> ranges, List<CritereConfig> criteres) {
        sheet.copyRows(configuration.getCritereFin().getRow(), configuration.getCelluleControle().getRow() - 1, destStartRow, new CellCopyPolicy());

        PositionValue soumissionnairesPosition = configuration.getSoumissionnaireDebut();
        int soumissionaireStartColumn = CellReference.convertColStringToIndex(soumissionnairesPosition.getColumn());
        for (int i = 0; i < nbrSoumissionaires; i++) {
            setFormula(sheet, evaluator, rowNotes, soumissionaireStartColumn + i, destStartRow, "SUM(", ")", ",");
        }
        destStartRow += 5;


        iSoumissionaireService.setSoumissionnairesFormula(sheet, nbrSoumissionaires, destStartRow, evaluator, configuration, soumissionaireStartColumn);

        sheet.copyRows(configuration.getCelluleControle().getRow(), destStartRow, configuration.getCritereDebut().getRow() - 1, new CellCopyPolicy());

        int additionalRows = configuration.getCelluleControle().getRow() - configuration.getCritereDebut().getRow() + 1;
        for (int i = 1; i <= additionalRows; i++) {
            XSSFRow row = sheet.getRow(destStartRow - i);
            if (row != null)
                sheet.removeRow(row);
        }
        setControlFormula(sheet, criteres, soumissionaireStartColumn - 1,
                destStartRow - additionalRows - 1);
        return destStartRow - additionalRows;
    }


    private XSSFCell setControlFormula(XSSFSheet sheet, List<CritereConfig> critereConfigs, int cellIndex, int rowIndex) {
        XSSFRow row = sheet.getRow(rowIndex);
        XSSFCell cell = row.getCell(cellIndex);
        if (cell == null) {
            cell = row.createCell(cellIndex);
        }
        cell.setCellType(CellType.FORMULA);
        String subFormula = critereConfigs.stream()
                .filter(critereConfig -> !CollectionUtils.isEmpty(critereConfig.getSousCriteres()))
                .map(critereConfig ->
                        critereConfig.getSousCriteres().stream().map(subCritereConfig ->
                                getRightNumVal(subCritereConfig.getPosition().translateRowCellReference(1))
                        ).collect(Collectors.joining("+")) + "=VALUE(" +
                                getRightNumVal(critereConfig.getPosition().getCellReference()) + ")"

                )
                .collect(Collectors.joining(","));

        String noteFormula = critereConfigs.stream()
                .map(critereConfig -> getRightNumVal(critereConfig.getPosition().getCellReference()))
                .collect(Collectors.joining("+")) + "=100";
        String noteControle = StringUtils.hasText(subFormula) ? "IF(AND(" + noteFormula + "," + subFormula + "),\"OK\",\"KO\")" : "IF(" + noteFormula + ",\"OK\",\"KO\")";
        cell.setCellFormula(noteControle);
        return cell;
    }

    private String getRightNumVal(String cellReference) {
        return "RIGHT(" + cellReference
                + ",LEN(" + cellReference + ")-1)";
    }

}
