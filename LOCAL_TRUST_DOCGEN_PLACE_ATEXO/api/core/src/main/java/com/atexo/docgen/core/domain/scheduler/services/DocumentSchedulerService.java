package com.atexo.docgen.core.domain.scheduler.services;

import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

public interface DocumentSchedulerService {

    @Scheduled(initialDelayString = "${docgen.scheduler.delete-document.delay}", fixedRateString = "${docgen.scheduler.delete-document.rate}")
    void deleteFiles() throws IOException;

    @Scheduled(initialDelayString = "${scheduler.offre.fixed.delay}", fixedRateString = "${scheduler.offre.fixed.rate}")
    void updateDocumentsStatus();

    @Scheduled(initialDelayString = "${docgen.scheduler.update-document-status.delay}", fixedRateString = "${docgen.scheduler.update-document-status.rate}")
    void deleteDocument() throws IOException;
}
