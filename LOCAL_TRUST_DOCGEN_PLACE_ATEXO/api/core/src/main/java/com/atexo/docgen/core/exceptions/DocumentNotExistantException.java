package com.atexo.docgen.core.exceptions;

import com.atexo.docgen.core.enums.TypeExceptionEnum;

public class DocumentNotExistantException extends DocGenException {
    private static final long serialVersionUID = 100L;

    public DocumentNotExistantException(TypeExceptionEnum type, String message) {
        super(type, message);
    }

}
