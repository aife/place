package com.atexo.docgen.core.domain.convertor.model.analyse;


import lombok.*;

import java.io.ByteArrayOutputStream;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConvertResult {
    private ByteArrayOutputStream convertedDocument;
    private Configuration configuration;
}
