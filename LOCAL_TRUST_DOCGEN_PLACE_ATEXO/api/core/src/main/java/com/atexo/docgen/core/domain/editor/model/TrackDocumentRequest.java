package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrackDocumentRequest {
    private String key;
    private int status;
    private String url;
    private String changesurl;
    private List<String> users;
    private ZonedDateTime lastsave;
    private Boolean notmodified;
    private List<DocumentAction> actions;
    private List<DocumentChanges> changes;
}
