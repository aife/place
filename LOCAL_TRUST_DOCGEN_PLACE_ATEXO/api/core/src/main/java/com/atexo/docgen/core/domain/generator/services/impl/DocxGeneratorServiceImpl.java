package com.atexo.docgen.core.domain.generator.services.impl;

import com.atexo.docgen.core.domain.convertor.services.html.IHtmlDocService;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.GenerationTypeEnum;
import com.atexo.docgen.core.domain.generator.model.SheetRequest;
import com.atexo.docgen.core.domain.generator.services.IConditionService;
import com.atexo.docgen.core.domain.generator.services.IDocumentGeneratorService;
import com.atexo.docgen.core.port.IChampFusionRacourciPort;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtBlock;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtContentBlock;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtRun;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atexo.docgen.core.utilitaire.DocumentUtils.getReplacement;
import static com.atexo.docgen.core.utilitaire.word.SdtUtils.getIsdtContents;
import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.*;

@Component
@Slf4j
public class DocxGeneratorServiceImpl implements IDocumentGeneratorService {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    private final IHtmlDocService iHtmlDocService;
    private final IChampFusionRacourciPort iChampFusionRacourciPort;
    @Value("${docgen.champs-fusion.default-null}")
    private String champsFusionDefaultNullValue;

    @Value("${docgen.champs-fusion.default-not-found}")
    private String champsFusionDefaultNotFoundValue;
    @Value("${docgen.champs-fusion.regex}")
    private String champsFusionRegex;
    private final ObjectMapper objectMapper;
    private final IConditionService iConditionService;

    public DocxGeneratorServiceImpl(IHtmlDocService iHtmlDocService, IChampFusionRacourciPort iChampFusionRacourciPort, ObjectMapper objectMapper, IConditionService iConditionService) {
        this.iHtmlDocService = iHtmlDocService;
        this.iChampFusionRacourciPort = iChampFusionRacourciPort;
        this.objectMapper = objectMapper;
        this.iConditionService = iConditionService;
    }


    @Override
    public ByteArrayOutputStream generate(FileGeneratorRequest file, String replacement, String dateFormat) throws IOException {

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        InputStream inputStream = file.getStream();

        XWPFDocument doc = new XWPFDocument(inputStream);
        generateXWPFDocument(file, doc, replacement, dateFormat);

        doc.write(out);
        out.close();
        return out;

    }

    private void generateXWPFDocument(FileGeneratorRequest file, XWPFDocument doc, String champsFusionDefault, String dateFormat) {
        Map<String, Object> keyValues = file.getKeyValues();
        JsonNode newNode = objectMapper.valueToTree(keyValues);
        replaceBody(doc, newNode, champsFusionDefault, dateFormat);

        doc.getHeaderList().forEach(xwpfHeader -> {
            xwpfHeader.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, newNode, champsFusionDefault, dateFormat));
            xwpfHeader.getTables().forEach(xwpfTable -> xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell ->
                    xwpfTableCell.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, newNode, champsFusionDefault, dateFormat)))));
        });
        doc.getFooterList().forEach(xwpfFooter -> {
            xwpfFooter.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, newNode, champsFusionDefault, dateFormat));
            xwpfFooter.getTables().forEach(xwpfTable -> xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell ->
                    xwpfTableCell.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, newNode, champsFusionDefault, dateFormat)))));

        });

    }

    private void replaceBody(XWPFDocument rootDocument, JsonNode jsonNode, String champsFusionDefault, String dateFormat) {
        Map<XmlCursor, XWPFDocument> draftDocumentMap = new HashMap<>();
        XmlCursor cursor;
        if (!rootDocument.getParagraphs().isEmpty()) {
            cursor = rootDocument.getDocument().newCursor();
        } else if (!rootDocument.getTables().isEmpty()) {
            cursor = rootDocument.getTables().get(0).getCTTbl().newCursor();
        } else
            cursor = rootDocument.createParagraph().getCTP().newCursor();

        for (IBodyElement bodyElement : rootDocument.getBodyElements()) {
            if (bodyElement instanceof XWPFParagraph xwpfParagraph) {
                replaceParagraph(xwpfParagraph, jsonNode, champsFusionDefault, dateFormat);
                cursor = xwpfParagraph.getCTP().newCursor();
            } else if (bodyElement instanceof XWPFSDT xwpfsdt) {
                XWPFDocument drafDocument = new XWPFDocument();
                ISDTContent content = xwpfsdt.getContent();
                final String tag = xwpfsdt.getTag();
                if (StringUtils.hasText(tag)) {
                    try {
                        List<IBodyElement> isdtContents = getIsdtContents(content);
                        GenerationTypeEnum type = GenerationTypeEnum.fromValue(tag);
                        //gestion des lots pour éviter problème de limitation à 64 caractères dans le title d'un sdt
                        String title = getTitle(xwpfsdt);
                        if (type == null) {
                            replaceBlocSampleBody(drafDocument, isdtContents, jsonNode, jsonNode, GenerationTypeEnum.BLOC_SIMPLE, champsFusionDefault, dateFormat);
                        } else {
                            switch (type) {
                                case BLOC_TEMPLATE -> {
                                    String[] relativeNodePath = title.split(" ");
                                    replaceBlocTemplateBody(drafDocument, isdtContents, jsonNode, jsonNode, GenerationTypeEnum.BLOC_SIMPLE, relativeNodePath, champsFusionDefault, dateFormat);
                                }
                                case BLOC_EXPLICATION -> log.debug("Bloc explication");
                                case BLOC_TEMPLATE_DYNAMIQUE -> {
                                    String[] relativeNodePath = title.split(" ");
                                    replaceBlocTemplateDynamiqueBody(drafDocument, isdtContents, jsonNode, jsonNode, GenerationTypeEnum.BLOC_SIMPLE, relativeNodePath, dateFormat);
                                }
                                case BLOC_TABLE -> {
                                    String[] relativeNodePath = title.split(" ");
                                    replaceBlocTableBody(drafDocument, isdtContents, jsonNode, jsonNode, GenerationTypeEnum.BLOC_SIMPLE, relativeNodePath, champsFusionDefault, dateFormat);
                                }
                                case BLOC_CONDITIONNEL -> {
                                    if (iConditionService.isVerified(jsonNode, title))
                                        replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, jsonNode, jsonNode, GenerationTypeEnum.BLOC_SIMPLE, champsFusionDefault, dateFormat);
                                }
                                default -> {
                                    replaceBlocSampleBody(drafDocument, isdtContents, jsonNode, jsonNode, GenerationTypeEnum.BLOC_SIMPLE, champsFusionDefault, dateFormat);
                                }

                            }
                        }

                        draftDocumentMap.computeIfPresent(cursor, (xmlCursor, xwpfDocument) -> {
                            cloneDocumentBody(drafDocument, xwpfDocument);

                            return xwpfDocument;
                        });
                        draftDocumentMap.putIfAbsent(cursor, drafDocument);


                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        log.error("Erreur lors de la récupération de éléments de SDT {} avec le tag {} et le contenu {}", xwpfsdt.getTitle(),
                                tag,
                                xwpfsdt.getContent().getText());
                    }
                }


            } else if (bodyElement instanceof XWPFTable xwpfTable) {
                xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell ->
                        xwpfTableCell.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, jsonNode, champsFusionDefault, dateFormat))));
                cursor = xwpfTable.getCTTbl().newCursor();
            }

        }

        for (XWPFDocument doc : draftDocumentMap.values()) {
            for (IBodyElement bodyElement : doc.getBodyElements()) {
                if (bodyElement instanceof XWPFTable xwpfTable) {
                    xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell ->
                            xwpfTableCell.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, jsonNode, champsFusionDefault, dateFormat))));
                    cursor = xwpfTable.getCTTbl().newCursor();
                }
            }
        }

        addDraftDocumentsToRootDocument(rootDocument, draftDocumentMap);
        Predicate<CTSdtBlock> predicateBody = s -> {
            if (!s.isSetSdtContent() || !s.isSetSdtPr()
                    || CollectionUtils.isEmpty(s.getSdtPr().getTagList()))
                return false;
            GenerationTypeEnum.fromValue(s.getSdtPr().getTagList().get(0).getVal());
            return true;
        };
        Predicate<CTSdtRun> predicateRun = s -> {
            if (!s.isSetSdtContent() || !s.isSetSdtPr() || CollectionUtils.isEmpty(s.getSdtPr().getTagList()))
                return false;
            GenerationTypeEnum.fromValue(s.getSdtPr().getTagList().get(0).getVal());
            return true;
        };
        deleteSdtBlocks(rootDocument, predicateBody, predicateRun);

    }


    private void addDraftDocumentsToRootDocument(XWPFDocument rootDocument, Map<XmlCursor, XWPFDocument> draftDocumentMap) {
        final Set<XmlCursor> keys = draftDocumentMap.keySet();
        new ArrayDeque<>(keys) // or LinkedList
                .descendingIterator()
                .forEachRemaining(xmlCursor -> {
                    final XWPFDocument xwpfDocument = draftDocumentMap.get(xmlCursor);
                    final List<IBodyElement> elements = xwpfDocument.getBodyElements();
                    xmlCursor.toNextSibling();
                    for (int i = elements.size() - 1; i >= 0; i--) {
                        IBodyElement iBodyElement = elements.get(i);
                        if (iBodyElement instanceof XWPFParagraph xwpfParagraph) {
                            XWPFParagraph dest = rootDocument.insertNewParagraph(xmlCursor);
                            if (dest == null) {
                                dest = rootDocument.createParagraph();
                            }
                            cloneParagraph(dest, xwpfParagraph.getCTP());
                            xmlCursor = dest.getCTP().newCursor();

                        } else if (iBodyElement instanceof XWPFTable xwpfTable) {
                            XWPFTable newTable = rootDocument.insertNewTbl(xmlCursor);
                            if (newTable == null) {
                                newTable = rootDocument.createTable();
                            }
                            cloneTable(newTable, xwpfTable.getCTTbl());
                            xmlCursor = newTable.getCTTbl().newCursor();

                        }
                    }
                    try {
                        xwpfDocument.close();
                    } catch (IOException e) {
                        log.error("Erreur lors de la fermerture du Draft document");
                    }

                });
    }

    private void replaceBlocSampleBody(XWPFDocument drafDocument, List<IBodyElement> bodyElements, JsonNode globalNode, JsonNode relativeNode, GenerationTypeEnum parentType, String champsFusionDefault, String dateFormat) {
        for (IBodyElement bodyElement : bodyElements) {
            if (bodyElement instanceof XWPFParagraph xwpfParagraph) {
                XWPFParagraph dest = drafDocument.createParagraph();
                cloneParagraph(dest, xwpfParagraph);
                replaceParagraph(dest, globalNode, champsFusionDefault, dateFormat);
            } else if (bodyElement instanceof XWPFSDT xwpfsdt) {

                ISDTContent content = xwpfsdt.getContent();
                //gestion des lots pour éviter problème de limitation à 64 caractères dans le title d'un sdt
                String title = getTitle(xwpfsdt);
                try {

                    List<IBodyElement> isdtContents = getIsdtContents(content);
                    GenerationTypeEnum type = GenerationTypeEnum.fromValue(xwpfsdt.getTag());
                    if (type == null) {
                        replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_SIMPLE, champsFusionDefault, dateFormat);
                    } else {
                        switch (type) {
                            case BLOC_TEMPLATE -> {
                                String[] suffix = title.split(" ");
                                replaceBlocTemplateBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_SIMPLE, suffix, champsFusionDefault, dateFormat);
                            }
                            case BLOC_EXPLICATION -> {
                                log.debug("Bloc explication");
                            }
                            case BLOC_TABLE -> {
                                String[] suffix = title.split(" ");
                                replaceBlocTableBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_SIMPLE, suffix, champsFusionDefault, dateFormat);
                            }
                            case BLOC_CONDITIONNEL -> {
                                if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                                    replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, champsFusionDefault, dateFormat);
                                if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                    replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, champsFusionDefault, dateFormat);

                            }
                            default ->
                                    replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_SIMPLE, champsFusionDefault, dateFormat);

                        }
                    }
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    log.error("Erreur lors de la récupération de éléments de SDT {} avec le tag {} et le contenu {}", title,
                            xwpfsdt.getTag(),
                            xwpfsdt.getContent().getText());
                }
            } else if (bodyElement instanceof XWPFTable) {
                cloneDraftTable(drafDocument, relativeNode, bodyElement, champsFusionDefault, dateFormat);
            }

        }

    }

    private void cloneDraftTable(XWPFDocument drafDocument, JsonNode globalNode, IBodyElement bodyElement, String champsFusionDefault, String dateFormat) {

        XWPFTable xwpfTable = (XWPFTable) bodyElement;
        XWPFTable table = drafDocument.createTable();
        cloneTable(table, xwpfTable.getCTTbl());
        table.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell ->
                xwpfTableCell.getParagraphs().forEach(xwpfParagraph -> replaceParagraph(xwpfParagraph, globalNode, champsFusionDefault, dateFormat))));

    }


    private void replaceBlocTemplateBody(XWPFDocument drafDocument, List<IBodyElement> bodyElements, JsonNode globalNode, JsonNode relativeNode,
                                         GenerationTypeEnum parentType, String[] relativeNodePath, String champsFusionDefault, String dateFormat) {

        JsonNode replacementArray = getJsonNode(globalNode, relativeNode, parentType, relativeNodePath);
        if (replacementArray == null || "null".equals(replacementArray.asText())) {
            log.warn("Le tableau de données est vide {}", Arrays.stream(relativeNodePath).map(s -> s + " ").reduce("", String::concat));
            return;
        }
        if (!replacementArray.isArray()) {
            replacementArray = objectMapper.createArrayNode().add(replacementArray);
        }
        for (int i = 0; i < replacementArray.size(); i++) {
            final JsonNode relativeIteration = replacementArray.get(i);
            if (relativeIteration instanceof ObjectNode)
                ((ObjectNode) relativeIteration).put("i", i + 1);
            for (IBodyElement bodyElement : bodyElements) {
                if (bodyElement instanceof XWPFParagraph xwpfParagraph) {

                    XWPFParagraph dest = drafDocument.createParagraph();

                    cloneParagraph(dest, xwpfParagraph);
                    replaceParagraph(dest, relativeIteration, champsFusionDefault, dateFormat);
                } else if (bodyElement instanceof XWPFSDT) {
                    final XWPFSDT xwpfsdt = (XWPFSDT) bodyElement;
                    ISDTContent content = xwpfsdt.getContent();
                    //gestion des lots pour éviter problème de limitation à 64 caractères dans le title d'un sdt
                    String title = getTitle(xwpfsdt);
                    try {

                        List<IBodyElement> isdtContents = getIsdtContents(content);
                        GenerationTypeEnum type = GenerationTypeEnum.fromValue(xwpfsdt.getTag());
                        if (type == null) {
                            if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                                replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                            if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                        } else {
                            switch (type) {
                                case BLOC_TEMPLATE:
                                    String[] suffix = title.split(" ");
                                    replaceBlocTemplateBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TEMPLATE, suffix, champsFusionDefault, dateFormat);
                                    break;
                                case BLOC_EXPLICATION:
                                    break;
                                case BLOC_TABLE:
                                    suffix = title.split(" ");
                                    replaceBlocTableBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TEMPLATE, suffix, champsFusionDefault, dateFormat);
                                    break;
                                case BLOC_TEMPLATE_DYNAMIQUE:
                                    relativeNodePath = title.split(" ");
                                    replaceBlocTemplateDynamiqueBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TEMPLATE, relativeNodePath, dateFormat);
                                    break;
                                case BLOC_CONDITIONNEL:
                                    if (iConditionService.isVerified(relativeIteration, title))
                                        replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                                    break;
                                default:
                                    if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                                        replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                                    if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                        replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);

                            }
                        }
                    } catch (Exception e) {
                        log.error("Erreur lors de la récupération de éléments de SDT {} avec le tag {} et le contenu {}", title,
                                xwpfsdt.getTag(),
                                xwpfsdt.getContent().getText());
                    }
                }
            }

        }

    }

    private static JsonNode getJsonNode(JsonNode globalNode, JsonNode relativeNode, GenerationTypeEnum parentType, String[] relativeNodePath) {
        JsonNode replacementArray = null;
        if (GenerationTypeEnum.BLOC_TABLE.equals(parentType) ||
                GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType) ||
                GenerationTypeEnum.BLOC_TEMPLATE_DYNAMIQUE.equals(parentType)) {
            for (String path : relativeNodePath) {
                replacementArray = getReplacement(relativeNode, path);
                if (replacementArray != null && !"null".equals(replacementArray.asText())) {
                    break;
                }
            }
        } else {
            for (String path : relativeNodePath) {
                replacementArray = getReplacement(globalNode, path);
                if (replacementArray != null && !"null".equals(replacementArray.asText())) {
                    break;
                }
            }
        }
        return replacementArray;
    }


    private void replaceBlocTemplateDynamiqueBody(XWPFDocument drafDocument, List<IBodyElement> bodyElements, JsonNode globalNode, JsonNode relativeNode,
                                                  GenerationTypeEnum parentType, String[] relativeNodePath, String dateFormat) {


        JsonNode replacementArray = getJsonNode(globalNode, relativeNode, parentType, relativeNodePath);
        if (replacementArray == null || "null".equals(replacementArray.asText())) {
            log.warn("Le tableau de données est vide {}", relativeNodePath);
            return;
        }

        for (IBodyElement bodyElement : bodyElements) {
            if (bodyElement instanceof XWPFParagraph) {
                XWPFParagraph xwpfParagraph = (XWPFParagraph) bodyElement;

                XWPFParagraph dest = drafDocument.createParagraph();

                cloneParagraph(dest, xwpfParagraph);
            }
        }

        insertParagraphInDocxWithKeyValues(drafDocument, replacementArray, dateFormat, "", false);
    }

    private void insertParagraphInDocxWithKeyValues(XWPFDocument drafDocument, JsonNode keyValues, String dateFormat, String prefix, boolean firstElementTab) {
        if (keyValues instanceof ObjectNode) {
            Iterator<Map.Entry<String, JsonNode>> iterator = keyValues.fields();
            Map.Entry<String, JsonNode> current;
            while (iterator.hasNext()) {
                current = iterator.next();
                String key = current.getKey();
                JsonNode value = current.getValue();
                if (value.fields().hasNext() || value.isArray()) {
                    iHtmlDocService.insertHtml("<li>" + prefix + (firstElementTab ? "-" : " ") + " <strong>" + key + " :</strong></li>", drafDocument, false, null, null, false);
                    insertParagraphInDocxWithKeyValues(drafDocument, value, dateFormat, prefix + "      ", true);
                } else {
                    String x = value.asText();
                    String dateFromString = getDateFromString(dateFormat, x);
                    x = dateFromString == null ? x : dateFromString;
                    if (x != null) {
                        iHtmlDocService.insertHtml("<li>" + prefix + (firstElementTab ? "-" : " ") + " <strong>" + key + " :</strong> " + x + "</li>", drafDocument, false, null, null, false);
                    } else {
                        iHtmlDocService.insertHtml("<li>" + prefix + (firstElementTab ? "-" : " ") + " <strong>" + key + " :</strong> " + value.asText() + "</li>", drafDocument, false, null, null, false);
                    }
                    firstElementTab = false;
                }


            }


        } else if (keyValues instanceof ArrayNode) {
            for (int i = 0; i < keyValues.size(); i++) {
                insertParagraphInDocxWithKeyValues(drafDocument, keyValues.get(i), dateFormat, prefix, true);
            }

        } else {
            iHtmlDocService.insertHtml("<li>" + prefix + (firstElementTab ? "-" : " ") + " " + keyValues.asText() + "</li>", drafDocument, false, null, null, false);

        }
    }


    private void replaceBlocTableBody(XWPFDocument drafDocument, List<IBodyElement> bodyElements, JsonNode globalNode, JsonNode relativeNode,
                                      GenerationTypeEnum parentType, String[] relativeNodePath, String champsFusionDefault, String dateFormat) {

        JsonNode replacementArray = getJsonNode(globalNode, relativeNode, parentType, relativeNodePath);
        if (replacementArray == null || "null".equals(replacementArray.asText())) {
            log.warn("Le tableau de données est vide {}", String.join(", ", relativeNodePath));
            return;
        }
        if (!replacementArray.isArray()) {
            replacementArray = objectMapper.createArrayNode().add(replacementArray);
        }
        for (int i = 0; i < replacementArray.size(); i++) {
            final JsonNode relativeIteration = replacementArray.get(i);
            ((ObjectNode) relativeIteration).put("i", i + 1);
            for (IBodyElement bodyElement : bodyElements) {
                if (bodyElement instanceof XWPFParagraph xwpfParagraph && i == 0) {
                    XWPFParagraph dest = drafDocument.createParagraph();
                    cloneParagraph(dest, xwpfParagraph);
                    replaceParagraph(dest, relativeIteration, champsFusionDefault, dateFormat);
                } else if (bodyElement instanceof XWPFSDT xwpfsdt && i == 0) {
                    ISDTContent content = xwpfsdt.getContent();
                    //gestion des lots pour éviter problème de limitation à 64 caractères dans le title d'un sdt
                    String title = getTitle(xwpfsdt);
                    try {

                        List<IBodyElement> isdtContents = getIsdtContents(content);
                        GenerationTypeEnum type = GenerationTypeEnum.fromValue(xwpfsdt.getTag());
                        if (type == null) {
                            if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                                replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TABLE, champsFusionDefault, dateFormat);
                            if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TABLE, champsFusionDefault, dateFormat);

                        } else {
                        switch (type) {
                            case BLOC_TEMPLATE:
                                String[] suffix = title.split(" ");
                                replaceBlocTemplateBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TABLE, suffix, champsFusionDefault, dateFormat);
                                break;
                            case BLOC_EXPLICATION:
                                break;
                            case BLOC_CONDITIONNEL:
                                if (iConditionService.isVerified(relativeIteration, title))
                                    replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TABLE, champsFusionDefault, dateFormat);
                                break;
                            case BLOC_TEMPLATE_DYNAMIQUE:
                                relativeNodePath = title.split(" ");
                                replaceBlocTemplateDynamiqueBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TABLE, relativeNodePath, dateFormat);
                                break;
                            case BLOC_TABLE:
                                suffix = title.split(" ");
                                replaceBlocTableBody(drafDocument, isdtContents, globalNode, relativeIteration, GenerationTypeEnum.BLOC_TABLE, suffix, champsFusionDefault, dateFormat);
                                break;
                            default:
                                if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                                    replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TABLE, champsFusionDefault, dateFormat);
                                if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                    replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TABLE, champsFusionDefault, dateFormat);

                        }
                        }
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        log.error("Erreur lors de la récupération de éléments de SDT {} avec le tag {} et le contenu {}", title,
                                xwpfsdt.getTag(),
                                xwpfsdt.getContent().getText());
                    }
                } else if (bodyElement instanceof XWPFTable xwpfTable && i == 0) {
                    XWPFTable table = drafDocument.createTable();
                    cloneTable(table, xwpfTable.getCTTbl());

                    populateTable((ArrayNode) replacementArray, table, champsFusionDefault, dateFormat);

                }
            }

        }

    }


    private void replaceBlocTemplateConditionnelBody(XWPFDocument drafDocument, List<IBodyElement> bodyElements, JsonNode globalNode, JsonNode relativeNode,
                                                     GenerationTypeEnum parentType, String champsFusionDefault, String dateFormat) {

        for (IBodyElement bodyElement : bodyElements) {
            if (bodyElement instanceof XWPFParagraph xwpfParagraph) {

                XWPFParagraph dest = drafDocument.createParagraph();

                cloneParagraph(dest, xwpfParagraph);
                replaceParagraph(dest, relativeNode, champsFusionDefault, dateFormat);
            } else if (bodyElement instanceof XWPFSDT) {

                final XWPFSDT xwpfsdt = (XWPFSDT) bodyElement;
                ISDTContent content = xwpfsdt.getContent();
                String title = getTitle(xwpfsdt);
                try {

                    List<IBodyElement> isdtContents = getIsdtContents(content);
                    GenerationTypeEnum type = GenerationTypeEnum.fromValue(xwpfsdt.getTag());
                    if (type == null) {
                        if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                            replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                        if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                            replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                    } else {
                        switch (type) {
                            case BLOC_TEMPLATE:
                                String[] suffix = title.split(" ");
                                replaceBlocTemplateBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, suffix, champsFusionDefault, dateFormat);
                                break;
                            case BLOC_EXPLICATION:
                                break;
                            case BLOC_TABLE:
                                suffix = title.split(" ");
                                replaceBlocTableBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, suffix, champsFusionDefault, dateFormat);
                                break;
                            case BLOC_TEMPLATE_DYNAMIQUE:
                                suffix = title.split(" ");
                                replaceBlocTemplateDynamiqueBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, suffix, dateFormat);
                                break;
                            case BLOC_CONDITIONNEL:
                                if ((parentType == null || GenerationTypeEnum.BLOC_SIMPLE.equals(parentType)) && iConditionService.isVerified(globalNode, title))
                                    replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, champsFusionDefault, dateFormat);
                                if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                    replaceBlocTemplateConditionnelBody(drafDocument, isdtContents, globalNode, relativeNode, parentType, champsFusionDefault, dateFormat);
                                break;
                            default:
                                if (GenerationTypeEnum.BLOC_SIMPLE.equals(parentType) && iConditionService.isVerified(globalNode, title))
                                    replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);
                                if ((GenerationTypeEnum.BLOC_TABLE.equals(parentType) || GenerationTypeEnum.BLOC_TEMPLATE.equals(parentType)) && iConditionService.isVerified(relativeNode, title))
                                    replaceBlocSampleBody(drafDocument, isdtContents, globalNode, relativeNode, GenerationTypeEnum.BLOC_TEMPLATE, champsFusionDefault, dateFormat);

                        }
                    }
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    log.error("Erreur lors de la récupération de éléments de SDT {} avec le tag {} et le contenu {}", title,
                            xwpfsdt.getTag(),
                            xwpfsdt.getContent().getText());
                }
            } else if (bodyElement instanceof XWPFTable) {
                cloneDraftTable(drafDocument, relativeNode, bodyElement, champsFusionDefault, dateFormat);
            }

        }
    }

    private String getTitle(XWPFSDT xwpfsdt) {
        //gestion des lots pour éviter problème de limitation à 64 caractères dans le title d'un sdt
        String title = xwpfsdt.getTitle();
        return Arrays.stream(title.split(" ")).map(clause -> Arrays.stream(clause.split("\\.")).map(racourci -> {
            if (!racourci.startsWith("#")) {
                return racourci;
            }
            String fusion = iChampFusionRacourciPort.getChampFusionFromRacourci(racourci.replace("#", ""));
            return StringUtils.hasText(fusion) ? racourci.replace(racourci, fusion) : racourci;
        }).collect(Collectors.joining("."))).collect(Collectors.joining(" "));
    }


    @Override
    public Map<String, Set<String>> getFileVariable(InputStream inputStream) throws IOException {
        Map<String, Set<String>> variables = new HashMap<>();
        XWPFDocument doc = new XWPFDocument(inputStream);
        addVariable(GenerationTypeEnum.BLOC_SIMPLE.getValue(), variables, doc);
        final List<CTSdtBlock> sdtList = doc.getDocument().getBody().getSdtList();
        getBlocTemplateVariable(sdtList, variables, "", GenerationTypeEnum.BLOC_SIMPLE);
        return variables;

    }

    @Override
    public ByteArrayOutputStream buildDocument(List<SheetRequest> xlsx) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XWPFDocument doc = new XWPFDocument();
        doc.write(out);
        out.close();
        return out;
    }

    private void getBlocTemplateVariable(List<CTSdtBlock> sdtList, Map<String, Set<String>> variables, String parentPath, GenerationTypeEnum type) {
        sdtList.stream()
                .filter(this::filterBlocTemplate)
                .forEach(ctSdtBlock -> {
                    extractVariableFromBlocs(variables, parentPath, ctSdtBlock, type);
                });

    }

    private void extractVariableFromBlocs(Map<String, Set<String>> variables, String parentPath, CTSdtBlock ctSdtBlock, GenerationTypeEnum parentType) {
        String tagValue = ctSdtBlock.getSdtPr().getTagList().get(0).getVal();
        try (XWPFDocument newDocument = new XWPFDocument()) {

            GenerationTypeEnum type = GenerationTypeEnum.fromValue(tagValue);

            String suffix = getRelativeNodePath(parentPath, ctSdtBlock, type, parentType);
            CTSdtContentBlock sdtContent = ctSdtBlock.getSdtContent();
            if (!CollectionUtils.isEmpty(sdtContent.getSdtList())) {
                this.getBlocTemplateVariable(sdtContent.getSdtList(), variables, suffix,
                        type.equals(GenerationTypeEnum.BLOC_CONDITIONNEL) ? parentType : type);
            }
            if (!CollectionUtils.isEmpty(sdtContent.getPList()))
                sdtContent.getPList().forEach(ctp -> cloneParagraph(newDocument.createParagraph(), ctp));
            if (!CollectionUtils.isEmpty(sdtContent.getTblList()))
                sdtContent.getTblList().forEach(ctp -> cloneTable(newDocument.createTable(), ctp));

            searchVariables(variables, parentType, newDocument, suffix, type);

        } catch (IOException e) {
            log.error("Erreur lors de l'extraction des variables {}", e.getMessage());
        }
    }

    private String getRelativeNodePath(String parentPath, CTSdtBlock ctSdtBlock, GenerationTypeEnum type, GenerationTypeEnum parentType) {

        final String title = ctSdtBlock.getSdtPr().getAliasList().get(0).getVal().replace(" ", "");
        String suffix = parentType == null || parentType.equals(GenerationTypeEnum.BLOC_SIMPLE) ? "" : parentPath;
        if (type.equals(GenerationTypeEnum.BLOC_TEMPLATE) || type.equals(GenerationTypeEnum.BLOC_TEMPLATE_DYNAMIQUE)) {
            if (!StringUtils.isEmpty(suffix)) {
                suffix += ".";
            }
            suffix += title;
        }
        return suffix;
    }


    private void searchVariables(Map<String, Set<String>> variables, GenerationTypeEnum parentType, XWPFDocument newDocument, String suffix, GenerationTypeEnum type) {
        final String key = type.getValue() + "_" + suffix;
        if (type.equals(GenerationTypeEnum.BLOC_TEMPLATE) || type.equals(GenerationTypeEnum.BLOC_TEMPLATE_DYNAMIQUE)) {
            addVariable(key, variables, newDocument);
        } else if (type.equals(GenerationTypeEnum.BLOC_SIMPLE)) {
            addVariable(GenerationTypeEnum.BLOC_SIMPLE.getValue(), variables, newDocument);
        } else if (parentType.equals(GenerationTypeEnum.BLOC_TEMPLATE) || parentType.equals(GenerationTypeEnum.BLOC_TEMPLATE_DYNAMIQUE)) {
            addVariable(key, variables, newDocument);
        } else if (parentType.equals(GenerationTypeEnum.BLOC_SIMPLE)) {
            addVariable(GenerationTypeEnum.BLOC_SIMPLE.getValue(), variables, newDocument);
        }
    }

    private void addVariable(String key, Map<String, Set<String>> variables, XWPFDocument newDocument) {

        variables.computeIfPresent(key, (s, strings) -> {
            strings.addAll(this.getSimpleDocVaribles(newDocument));
            return strings;
        });
        variables.putIfAbsent(key, this.getSimpleDocVaribles(newDocument));

    }

    private boolean filterBlocTemplate(CTSdtBlock ctSdtBlock) {
        if (!ctSdtBlock.isSetSdtContent() || !ctSdtBlock.isSetSdtPr()
                || CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getTagList())
                || CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getAliasList()))
            return false;
        GenerationTypeEnum.fromValue(ctSdtBlock.getSdtPr().getTagList().get(0).getVal());
        return true;
    }


    private void populateTable(ArrayNode replacementArray, XWPFTable table, String champsFusionDefault, String dateFormat) {
        addRows(replacementArray, table);
        replaceRows(replacementArray, table, champsFusionDefault, dateFormat);
    }

    private void replaceRows(ArrayNode replacementArray, XWPFTable table, String champsFusionDefault, String dateFormat) {
        int start = table.getRows().size() - replacementArray.size();
        for (int i = 0; i < replacementArray.size(); i++) {
            int finalI = i;
            table.getRow(i + start)
                    .getTableCells()
                    .forEach(xwpfTableCell ->
                            xwpfTableCell.getParagraphs()
                                    .forEach(xwpfParagraph -> {
                                        final JsonNode keyValues = replacementArray.get(finalI);
                                        ((ObjectNode) keyValues).put("i", finalI + 1);
                                        replaceParagraph(xwpfParagraph, keyValues, champsFusionDefault, dateFormat);
                                    }));

        }
    }

    private void addRows(ArrayNode replacementArray, XWPFTable table) {
        int lastRow = table.getRows().size() - 1;
        XWPFTableRow replaceRow = table.getRow(lastRow);

        for (int i = 0; i < replacementArray.size() - 1; i++) {
            XWPFTableRow row = table.createRow();
            cloneRow(replaceRow.getCtRow(), row);

        }
    }

    private Set<String> getSimpleDocVaribles(XWPFDocument doc) {
        Set<String> variables = new TreeSet<>();
        doc.getHeaderList().forEach(xwpfHeader -> {
            variables.addAll(getFileVariable(xwpfHeader.getParagraphs()));
            xwpfHeader.getTables().forEach(xwpfTable -> xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell -> variables.addAll(getFileVariable(xwpfTableCell.getParagraphs())))));
        });
        doc.getFooterList().forEach(xwpfFooter -> {
            variables.addAll(getFileVariable(xwpfFooter.getParagraphs()));
            xwpfFooter.getTables().forEach(xwpfTable -> xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell -> variables.addAll(getFileVariable(xwpfTableCell.getParagraphs())))));
        });
        variables.addAll(getFileVariable(doc.getParagraphs()));
        doc.getTables().forEach(xwpfTable -> xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell -> variables.addAll(getFileVariable(xwpfTableCell.getParagraphs())))));
        return variables;
    }

    private Set<String> getFileVariable(List<XWPFParagraph> paragraphs) {

        Set<String> templateVariable = new TreeSet<>();
        paragraphs.forEach(p -> {
            String content = p.getText();
            if (content.contains("[")) { // if paragraph does not include our pattern, ignore
                Pattern pat = Pattern.compile(champsFusionRegex);
                Matcher m = pat.matcher(content);
                while (m.find()) { // for all patterns in the paragraph
                    String text = m.group(1);
                    log.info("Variable trouvée : {}", text);
                    templateVariable.add(text);
                }
            }
        });
        log.info("Fin de la recherche de la liste des variables");
        return templateVariable;
    }


    private void replaceParagraph(XWPFParagraph p, JsonNode keyValues, String replacement, String dateFormat) {
        String pText = p.getText(); // complete paragraph as string
        if (pText.contains("[")) { // if paragraph does not include our pattern, ignore
            replaceSimpleChampsFusion(p, keyValues, pText, replacement, dateFormat);
        }

    }

    private void replaceSimpleChampsFusion(XWPFParagraph p, JsonNode keyValues, String pText, String champsFusionDefault, String dateFormat) {
        TreeMap<Integer, XWPFRun> posRuns = getPosToRuns(p);
        Pattern pat = Pattern.compile(champsFusionRegex);
        Matcher m = pat.matcher(pText);
        while (m.find()) { // for all patterns in the paragraph
            String champFusion = m.group(1);  // extract key start and end pos
            JsonNode replacement = getReplacement(keyValues, champFusion);
            String textToInsert = null;
            if (replacement == null) {
                textToInsert = champsFusionDefault != null ? champsFusionDefault : champsFusionDefaultNotFoundValue;
            } else {
                String[] split = champFusion.split("\\.");
                String lastSplit = split[split.length - 1];
                String start;
                String last;
                String[] variable;
                if (lastSplit.contains("_")) {
                    variable = lastSplit.split("_");
                } else {
                    variable = lastSplit.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
                }
                start = variable[0].toLowerCase();
                last = variable[variable.length - 1].toLowerCase();
                boolean isDate = "date".equals(start) || "date".endsWith(last);
                String text = replacement.asText();
                if (isDate) {
                    textToInsert = getDateFromString(dateFormat, text);
                    if (textToInsert == null && replacement.isNumber()) {
                        textToInsert = getDateFromNumber(dateFormat, replacement.asLong());
                    }
                    if (textToInsert == null && replacement.isArray()) {
                        textToInsert = getDateFromArrays(dateFormat, replacement);
                    }

                }
                if (textToInsert == null) {
                    if (replacement.isNumber()) {
                        textToInsert = getFormatedValue(BigDecimal.valueOf(replacement.asDouble()));
                    } else if (replacement.isBoolean()) {
                        textToInsert = replacement.asBoolean() ? "Oui" : "Non";
                    } else if (replacement.isArray()) {
                        // convert to string join with ,
                        textToInsert = StreamSupport.stream(replacement.spliterator(), false).map(JsonNode::asText).filter(Objects::nonNull).collect(Collectors.joining(", "));
                    } else {
                        String dateFromString = getDateFromString(dateFormat, text);
                        textToInsert = dateFromString == null ? text : dateFromString;
                    }
                }
            }
            if (textToInsert == null || textToInsert.equals("null")) {
                textToInsert = champsFusionDefault != null ? champsFusionDefault : champsFusionDefaultNullValue;
            }
            int debut = m.start(1);
            int fin = m.end(1);
            SortedMap<Integer, XWPFRun> range = posRuns.subMap(debut - 1, true, fin + 1, true); // get runs which contain the pattern
            boolean initialized = false;
            boolean foundPrefix = false;
            boolean foundSuffix = false;
            XWPFRun prevRun = null;
            XWPFRun found2Run = null;
            int found2Pos = -1;
            replaceRun(champFusion, textToInsert, range, initialized, foundPrefix, foundSuffix, prevRun, found2Run, found2Pos);
        }
    }

    private String getDateFromArrays(String dateFormat, JsonNode list) {
        try {
            if (list.isArray() && list.size() >= 6) {
                ZonedDateTime dateTime = ZonedDateTime.of(list.get(0).asInt(), list.get(1).asInt(), list.get(2).asInt(), list.get(3).asInt(), list.get(4).asInt(), list.get(5).asInt(), 0, ZoneId.systemDefault());
                if (dateFormat == null) {
                    return dateTime.format(dateTimeFormatter);
                } else {
                    return dateTime.format(DateTimeFormatter.ofPattern(dateFormat));
                }
            }
        } catch (Exception e) {
            log.debug("Erreur lors de la récupération de la date {}", e.getMessage());
        }
        return null;
    }

    private String getDateFromNumber(String dateFormat, Long textToInsert) {
        try {
            ZonedDateTime dateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(textToInsert), ZoneId.systemDefault());
            if (dateFormat == null) {
                return dateTime.format(dateTimeFormatter);
            } else {
                return dateTime.format(DateTimeFormatter.ofPattern(dateFormat));
            }
        } catch (Exception e) {
            log.debug("Erreur lors de la récupération de la date {}", e.getMessage());
        }
        try {
            LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(textToInsert), ZoneId.systemDefault());
            if (dateFormat == null) {
                return dateTime.format(dateTimeFormatter);
            } else {
                return dateTime.format(DateTimeFormatter.ofPattern(dateFormat));
            }
        } catch (Exception e) {
            log.debug("Erreur lors de la récupération de la date {}", e.getMessage());
        }
        return null;
    }

    private String getDateFromString(String dateFormat, String textToInsert) {
        try {
            ZonedDateTime dateTime = ZonedDateTime.parse(textToInsert);
            if (dateFormat == null) {
                return dateTime.format(dateTimeFormatter);
            } else {
                return dateTime.format(DateTimeFormatter.ofPattern(dateFormat));
            }
        } catch (Exception e) {
            log.debug("Erreur lors de la récupération de la date {}", e.getMessage());
        }
        try {
            LocalDateTime dateTime = LocalDateTime.parse(textToInsert);
            if (dateFormat == null) {
                return dateTime.format(dateTimeFormatter);
            } else {
                return dateTime.format(DateTimeFormatter.ofPattern(dateFormat));
            }
        } catch (Exception e) {
            log.debug("Erreur lors de la récupération de la date {}", e.getMessage());
        }
        return null;
    }

    private String getFormatedValue(BigDecimal value) {
        try {
            if (value == null) return null;
            NumberFormat numberFormat = NumberFormat.getInstance(java.util.Locale.FRENCH);
            numberFormat.setRoundingMode(RoundingMode.HALF_DOWN);
            return numberFormat.format(value);
        } catch (Exception e) {
            log.warn("Erreur du formatage de la valeur {} / {}", value, e.getMessage());
            return value.toPlainString();
        }
    }


    @SuppressWarnings("checkstyle:InnerAssignment")
    private void replaceRun(String champFusion, String textToInsert, SortedMap<Integer, XWPFRun> range, boolean initialized, boolean foundPrefix, boolean foundSuffix, XWPFRun prevRun, XWPFRun found2Run, int found2Pos) {
        if (!range.isEmpty()) {
            String toReplace = "[" + champFusion + "]";
            XWPFRun xwpfRun = range.get(range.firstKey());
            String text = xwpfRun.getText(0);
            if (text != null && text.contains(toReplace)) {
                text = text.replace(toReplace, textToInsert);
                xwpfRun.setText(text, 0);
                return;
            }
        }
        try {
            int nbSymbol = 1;
            for (int i = 0; i < textToInsert.length(); i++) {
                if (textToInsert.charAt(i) == ']') {
                    nbSymbol++;
                }
            }
            int progressNbSymbol = 0;
            int firstPosition = 0;

            for (XWPFRun r : range.values()) {
                if (r == prevRun || foundSuffix) {
                    continue;
                }
                prevRun = r;
                for (int k = 0; ; k++) {
                    String txt;
                    if (foundSuffix || (txt = extractTextFromRun(r, k)) == null) {
                        break;
                    }
                    if (!initialized) {
                        firstPosition = txt.indexOf('[') + 1;
                        txt = txt.replaceFirst("\\[", Matcher.quoteReplacement(textToInsert + "["));
                        initialized = true;
                    }
                    if (txt.contains("[") && !foundPrefix) {
                        found2Run = r;
                        found2Pos = firstPosition + textToInsert.length() - 1;
                        txt = txt.substring(0, found2Pos) + txt.substring(found2Pos + 1);
                        foundPrefix = true;
                    }
                    for (int i = 0; i < txt.length(); i++) {
                        if (txt.charAt(i) == ']') {
                            progressNbSymbol++;
                        }
                    }
                    if (foundPrefix) {
                        txt = getText(found2Run, found2Pos, r, txt, progressNbSymbol == nbSymbol);
                    }

                    if (progressNbSymbol == nbSymbol) {
                        int lastIndexOf = txt.lastIndexOf(']');
                        txt = txt.substring(0, lastIndexOf) + txt.substring(lastIndexOf + 1);
                        foundSuffix = true;
                    }
                    r.setText(txt, k);
                }
            }
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du text {}", e.getMessage());
        }
    }

    private String extractTextFromRun(XWPFRun r, int k) {
        try {
            return r == null || CollectionUtils.isEmpty(r.getCTR().getTList()) || (k >= r.getCTR().getTList().size()) ? null : r.getText(k);
        } catch (Exception ex) {
            log.error("Erreur lors de la récupération du text {}", ex.getMessage());
        }
        return null;
    }

    private String getText(XWPFRun found2Run, int found2Pos, XWPFRun r, String txt, boolean closing) {
        if (closing) {
            if (r == found2Run) {
                return txt.substring(0, found2Pos) + txt.substring(txt.lastIndexOf(']'));
            } else {
                return txt.substring(txt.lastIndexOf(']'));
            }
        } else if (r == found2Run) {
            return txt.substring(0, found2Pos);
        } else {
            return "";
        }
    }

    private TreeMap<Integer, XWPFRun> getPosToRuns(XWPFParagraph paragraph) {
        int pos = 0;
        TreeMap<Integer, XWPFRun> map = new TreeMap<>();
        for (XWPFRun run : paragraph.getRuns()) {
            String runText = run.text();
            if (runText != null && runText.length() > 0) {
                for (int i = 0; i < runText.length(); i++) {
                    map.put(pos + i, run);
                }
                pos += runText.length();
            }

        }
        return map;
    }


}
