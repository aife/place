package com.atexo.docgen.core.domain.convertor.model;

import com.atexo.docgen.core.domain.editor.model.ChampsFusion;
import com.atexo.docgen.core.domain.generator.model.AdministrationTypeEnum;
import lombok.*;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChampFusionTag {
    private AdministrationTypeEnum type;
    private ChampsFusion value;


}
