package com.atexo.docgen.core.domain.extractor.model;

import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExcelConfiguration {
    @NotEmpty
    private List<PositionToExtract> positions;
    private List<KeyValueRequest> keyValues;
    private List<StaticData> staticData;
}
