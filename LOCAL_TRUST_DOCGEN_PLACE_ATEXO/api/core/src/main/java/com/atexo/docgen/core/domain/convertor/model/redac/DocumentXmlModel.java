package com.atexo.docgen.core.domain.convertor.model.redac;

import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DocumentXmlModel implements Serializable {
    private String documentId;
    private String plateformeId;
    private String id;
    private String json;
    private String updatedJson;
    private int version;
    private Timestamp creationDate;
    private String fileType;
    private Integer redacStatus;

}
