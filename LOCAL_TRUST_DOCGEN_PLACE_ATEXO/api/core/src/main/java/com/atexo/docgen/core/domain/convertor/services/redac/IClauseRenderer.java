package com.atexo.docgen.core.domain.convertor.services.redac;

import com.atexo.docgen.core.domain.convertor.model.ChapitreType;
import com.atexo.docgen.core.domain.convertor.model.ClauseType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.math.BigInteger;

public interface IClauseRenderer {
    void render(ClauseType clauseType, XWPFDocument doc, ChapitreType chapitre);

    void getXwpfRuns(ClauseType clause, BigInteger numID, Double effectivePageWidth, XWPFDocument document);

    XWPFDocument getXwpfDocumentFromClause(ClauseType clause, XWPFDocument document, String chapitreNumero) throws IOException;

}
