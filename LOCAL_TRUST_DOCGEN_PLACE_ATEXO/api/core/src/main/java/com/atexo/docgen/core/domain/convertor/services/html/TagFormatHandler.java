package com.atexo.docgen.core.domain.convertor.services.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import org.xmlpull.v1.XmlPullParser;

public interface TagFormatHandler {

    FormatedText closing(TagFormat tagFormat);

    TagFormat parse(XmlPullParser xpp);

}
