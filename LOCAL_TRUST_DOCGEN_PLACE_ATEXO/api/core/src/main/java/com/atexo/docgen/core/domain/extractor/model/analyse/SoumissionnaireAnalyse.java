package com.atexo.docgen.core.domain.extractor.model.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.Soumissionnaire;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SoumissionnaireAnalyse extends Soumissionnaire {

    private List<CritereAnalyse> criteres;
    private int classement;
    private String classementString;
    private int noteGlobale;
    private int noteGlobaleAttributaire;

}
