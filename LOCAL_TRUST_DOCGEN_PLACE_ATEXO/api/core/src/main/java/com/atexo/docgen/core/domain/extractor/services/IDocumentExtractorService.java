package com.atexo.docgen.core.domain.extractor.services;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.extractor.model.ExcelConfiguration;
import com.atexo.docgen.core.domain.extractor.model.Feuille;
import com.atexo.docgen.core.domain.extractor.model.FeuillePositionValue;
import com.atexo.docgen.core.domain.extractor.model.analyse.AnalyseOffre;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface IDocumentExtractorService {

    ByteArrayOutputStream extract(List<MultipartFile> files, ByteArrayResource template, ExcelConfiguration configuration) throws IOException;

    AnalyseOffre extractAnalyseStatus(InputStream inputStream, Configuration configuration);

    List<FeuillePositionValue> extractXlsx(InputStream inputStream, List<FeuillePositionValue> positions);

    List<Feuille> getSheetNames(InputStream inputStream);
}
