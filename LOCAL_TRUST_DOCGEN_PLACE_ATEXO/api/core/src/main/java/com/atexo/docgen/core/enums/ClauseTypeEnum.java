package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ClauseTypeEnum {
    TYPE_CLAUSE_TEXTE_FIXE(2), TYPE_CLAUSE_TEXTE_LIBRE(3),
    TYPE_CLAUSE_TEXTE_PREVALORISE(4), TYPE_CLAUSE_LISTE_EXCLUSIVE(6), TYPE_CLAUSE_LISTE_MULTIPLE(7),
    TYPE_CLAUSE_VALEUR_HERITEE(9);
    private final int code;

    ClauseTypeEnum(int code) {
        this.code = code;
    }

    @JsonCreator
    public static ClauseTypeEnum fromValue(int value) {
        for (ClauseTypeEnum operator : values()) {
            if (operator.getCode() == value) {
                return operator;
            }
        }
        throw new IllegalArgumentException("Cannot find FileStatusEnum from value: " + value);
    }

    @JsonValue
    public int getCode() {
        return code;
    }
}
