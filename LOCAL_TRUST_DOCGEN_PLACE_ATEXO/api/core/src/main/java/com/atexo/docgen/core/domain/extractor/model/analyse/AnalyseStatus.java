package com.atexo.docgen.core.domain.extractor.model.analyse;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnalyseStatus {
    private int numero;
    private String intitule;
    private String entrepriseMieuxDisante;
    private String status;
    private List<SoumissionnaireAnalyse> soumissionnaires;
    private SoumissionnaireAnalyse attributaire;
}
