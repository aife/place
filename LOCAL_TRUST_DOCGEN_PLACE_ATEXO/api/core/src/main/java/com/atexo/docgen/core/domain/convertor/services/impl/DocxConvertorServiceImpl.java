package com.atexo.docgen.core.domain.convertor.services.impl;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.model.*;
import com.atexo.docgen.core.domain.convertor.model.redac.DocumentProgress;
import com.atexo.docgen.core.domain.convertor.model.redac.DocumentXmlModel;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.convertor.services.impl.redac.ChapitreRendererImpl;
import com.atexo.docgen.core.domain.convertor.services.redac.IClauseRenderer;
import com.atexo.docgen.core.domain.editor.model.DocumentAdministrableConfiguration;
import com.atexo.docgen.core.domain.generator.model.AdministrationTypeEnum;
import com.atexo.docgen.core.domain.security.services.IJwtService;
import com.atexo.docgen.core.enums.*;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.port.IDocumentXmlPort;
import com.atexo.docgen.core.port.IOnlyOfficePort;
import com.atexo.docgen.core.utilitaire.ClauseUtils;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import com.atexo.docgen.core.utilitaire.PdfDocumentUtils;
import com.atexo.docgen.core.utilitaire.word.StylesUtils;
import com.atexo.docgen.core.utilitaire.word.TocUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ooxml.XWPFDocumentUtils;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.impl.common.IOUtil;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.ChapitreUtils.isChapitresValide;
import static com.atexo.docgen.core.utilitaire.word.NumeringUtils.setDefaultDocumentNumering;
import static com.atexo.docgen.core.utilitaire.word.SdtUtils.copySdtContent;
import static com.atexo.docgen.core.utilitaire.word.SdtUtils.getIsdtContents;
import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.*;

@Component
@Slf4j
public class DocxConvertorServiceImpl implements IDocumentConvertorService {

    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentXmlPort iDocumentXmlPort;
    private final IClauseRenderer clauseRenderer;
    private final ChapitreRendererImpl chapitreRenderer;
    @Value("${docgen.public-url}")
    private String publicUrl;
    @Value("${docgen.context-path}")
    private String contextPath;
    private final IJwtService iJwtService;
    private final IOnlyOfficePort iOnlyOfficePort;
    private final AuthenticationManager authenticationManager;
    private static final String REQUEST_TO_OPEN = "request-to-open/";
    private static final String TOKEN = "?token=";
    private final RestTemplate atexoRestTemplate;


    @Value("${docgen.admin.login}")
    private String loginAdmin;

    @Value("${docgen.admin.password}")
    private String passwordAdmin;

    @Value("${external-apis.onlyoffice.storage-folder}")
    private String storagePath;

    public DocxConvertorServiceImpl(IDocumentXmlPort iDocumentXmlPort, IClauseRenderer clauseRenderer, IDocumentConfigurationService iDocumentConfigurationService, ChapitreRendererImpl chapitreRenderer, IOnlyOfficePort iOnlyOfficePort, IJwtService iJwtService, AuthenticationManager authenticationManager, RestTemplate atexoRestTemplate) {
        this.iDocumentXmlPort = iDocumentXmlPort;
        this.clauseRenderer = clauseRenderer;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.chapitreRenderer = chapitreRenderer;
        this.iOnlyOfficePort = iOnlyOfficePort;
        this.iJwtService = iJwtService;
        this.authenticationManager = authenticationManager;
        this.atexoRestTemplate = atexoRestTemplate;
    }

    @Override
    public DocumentXmlModel getById(String id) {
        return iDocumentXmlPort.getDocumentById(id);
    }


    @Override
    public ByteArrayOutputStream convert(@NotEmpty InputStream template, @NotNull DocxDocument docxDocument) {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            XWPFDocument document = new XWPFDocument(template);
            BigInteger cId = setDefaultDocumentNumering(document);
            CTSdtBlock block = TocUtils.buildToc(document);
            if (docxDocument.getType() != null && Arrays.asList(EnumerationDocumentType.CCAP, EnumerationDocumentType.CCP, EnumerationDocumentType.CCPAE).contains(docxDocument.getType())) {
                updateDocumentDerogation(docxDocument.getChapitre(), null);
                updateDerogationClause(docxDocument);
                docxDocument.setDerogeable(true);
            } else {
                docxDocument.setDerogeable(false);
            }
            int compteur = 1;
            this.processChapitres(docxDocument.getChapitre(), document, cId, compteur);
            TocUtils.populateToc(document, block);
            updateToc(document);
            document.enforceUpdateFields();
            document.write(stream);
            document.close();
            return stream;

        } catch (Exception e) {
            log.error("Erreur lors du build du docxDocument : {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage(), e);
        }


    }

    private void updateDerogationClause(DocxDocument docxDocument) {
        List<ChapitreType> chapitreWithDerogation = new ArrayList<>();
        getDerogationFromClauses(docxDocument.getChapitre(), chapitreWithDerogation);
        this.setDerogationChapitre(docxDocument, chapitreWithDerogation);
    }

    private void setDerogationChapitre(DocxDocument docxDocument, List<ChapitreType> clausesWithDerogation) {

        List<ChapitreType> deragationChapter = docxDocument.getChapitre().stream().filter(chapitreType -> chapitreType.getTitre().equalsIgnoreCase("Dérogation") || chapitreType.getTitre().equalsIgnoreCase("Dérogations") || chapitreType.getTitre().equalsIgnoreCase("Derogation") || chapitreType.getTitre().equalsIgnoreCase("Derogations")).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(deragationChapter)) {
            deragationChapter.forEach(chapitreType -> {
                docxDocument.getChapitre().remove(chapitreType);
            });
        }
        ChapitreType chapitre = new ChapitreType();
        chapitre.setValide(true);
        chapitre.setTitre("Dérogations");
        chapitre.setDerogationResume(true);
        chapitre.setId(13115);
        ClauseType clause = new ClauseType();
        clause.setType(ClauseTypeEnum.TYPE_CLAUSE_VALEUR_HERITEE.getCode());
        clause.setDerogationResume(true);
        clause.setEtat(ClauseEtatEnum.ETAT_DISPONIBLE.getCode());
        clause.setTexteFixeAvantALaLigne(false);
        clause.setTexteFixeAresALaLigne(false);
        clause.setCLAUSEVALIDE(true);
        clause.setRef("CLDR");
        clause.setId(1);
        clause.setActive(true);
        TableauRedactionType value = new TableauRedactionType();
        value.setTextAvantTableau("Vous trouverez, ci-dessous, la liste des dérogations au CCAG.");
        DocumentXLS value1 = new DocumentXLS();
        Onglet onglet = new Onglet();
        TableauAttributs attributs = new TableauAttributs();
        attributs.getLargeurColonne().add(getLargeur(0, 20.0));
        attributs.getLargeurColonne().add(getLargeur(1, 20.0));
        attributs.getLargeurColonne().add(getLargeur(2, 60.0));
        onglet.setAttributs(attributs);
        onglet.getCellule().add(getCellule("Article du CCAP concerné", 0, 0, getHeaderStyle()));
        onglet.getCellule().add(getCellule("Article du CCAG dérogé", 0, 1, getHeaderStyle()));
        onglet.getCellule().add(getCellule("Commentaire", 0, 2, getHeaderStyle()));
        if (!CollectionUtils.isEmpty(clausesWithDerogation)) {
            for (int i = 0; i < clausesWithDerogation.size(); i++) {
                ChapitreType type = clausesWithDerogation.get(i);
                onglet.getCellule().add(getCellule(type.getNumero(), i + 1, 0, null));
                onglet.getCellule().add(getCellule(type.getDerogation().getArticle(), i + 1, 1, null));
                onglet.getCellule().add(getCellule(type.getDerogation().getCommentaire(), i + 1, 2, null));

            }
        }
        value1.getOnglet().add(onglet);
        value.setDocumentXLS(value1);
        clause.setTableauRedaction(value);
        chapitre.getClause().add(clause);
        if (CollectionUtils.isEmpty(docxDocument.getChapitre())) {
            chapitre.setNumero("1");
        } else {
            chapitre.setNumero(String.valueOf(docxDocument.getChapitre().size() + 1));
        }
        docxDocument.getChapitre().add(chapitre);

    }

    private AttributLargeurColonne getLargeur(int colonne, double largeur) {
        AttributLargeurColonne attributLargeurColonne = new AttributLargeurColonne();
        attributLargeurColonne.setColonne(colonne);
        attributLargeurColonne.setLargeur(largeur);
        return attributLargeurColonne;
    }

    private Cellule getCellule(String value, int ligne, int colonne, CelluleStyle headerStyle) {
        Cellule celluleClauseRoot = new Cellule();
        CelluleCoordonnee coordonne = new CelluleCoordonnee();
        coordonne.setLigne(ligne);
        coordonne.setColonne(colonne);
        celluleClauseRoot.setCoordonnee(coordonne);
        celluleClauseRoot.setValeur(value);
        celluleClauseRoot.setStyle(headerStyle);
        return celluleClauseRoot;
    }

    private CelluleStyle getHeaderStyle() {
        CelluleStyle celluleStyle = new CelluleStyle();
        celluleStyle.setRetourLigne(false);
        CelluleStyleRemplissage remplissage = new CelluleStyleRemplissage();
        remplissage.setType(EnumerationRemplissage.SOLID_FOREGROUND);
        remplissage.setArrierePlan(EnumerationCouleur.GREY_25_PERCENT);
        celluleStyle.setRemplissage(remplissage);
        CelluleStyleFont font = new CelluleStyleFont();
        font.setGras(true);
        font.setItalic(false);
        celluleStyle.setFont(font);
        CelluleStyleAlignement alignement = new CelluleStyleAlignement();
        alignement.setHorizontal(EnumerationAlignementHorizontal.ALIGN_GENERAL);
        alignement.setVertical(EnumerationAlignementVertical.VERTICAL_TOP);
        celluleStyle.setAlignement(alignement);
        return celluleStyle;
    }


    private void getDerogationFromClauses(List<ChapitreType> chapitres, List<ChapitreType> chapitreWithDerogation) {
        if (chapitres != null) {
            for (ChapitreType chapitre : chapitres) {
                if (chapitre.getClause() != null && chapitre.getDerogation() != null && StringUtils.hasText(chapitre.getDerogation().getArticle())) {
                    List<ClauseType> list = chapitre.getClause().stream().filter(clauseType -> !ClauseEtatEnum.ETAT_SUPPRIMER.equals(ClauseEtatEnum.fromValue(clauseType.getEtat())) && !ClauseEtatEnum.ETAT_CONDITIONNER.equals(ClauseEtatEnum.fromValue(clauseType.getEtat()))).collect(Collectors.toList());
                    if (!CollectionUtils.isEmpty(list)) {
                        chapitreWithDerogation.add(chapitre);

                    }
                }
                List<ChapitreType> subChapitre = chapitre.getChapitre();
                if (subChapitre != null) {
                    this.getDerogationFromClauses(subChapitre, chapitreWithDerogation);
                }
            }
        }
    }


    private void updateDocumentDerogation(List<ChapitreType> chapitres, DerogationType derogation) {
        if (chapitres != null) {
            for (ChapitreType chapitre : chapitres) {

                if (chapitre.getDerogation() == null && derogation != null && StringUtils.hasText(derogation.getArticle())) {
                    chapitre.setDerogation(derogation);
                }
                if (chapitre.getClause() != null && chapitre.getDerogation() != null && StringUtils.hasText(chapitre.getDerogation().getArticle())) {
                    chapitre.getClause().forEach(clauseType -> clauseType.setDerogation(chapitre.getDerogation()));
                }
                if (chapitre.getChapitre() != null) {
                    updateDocumentDerogation(chapitre.getChapitre(), chapitre.getDerogation());
                }
            }
        }
    }

    @Override
    public DocxDocument getDocxDocument(InputStream xml) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(DocxDocument.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            DocxDocument docxDocument = (DocxDocument) unmarshaller.unmarshal(xml);
            docxDocument = sanitizeDocxDocument(docxDocument);
            setDocument(docxDocument);
            return docxDocument;
        } catch (JAXBException e) {
            log.error("Erreur lors de la conversion {}", e.getMessage());
            return null;
        }

    }

    @Override
    public DocxDocument sanitizeDocxDocument(DocxDocument docxDocument) {
        List<ChapitreType> chapitre = docxDocument.getChapitre();
        sanitizeChapitres(chapitre);
        return docxDocument;
    }

    private static void sanitizeChapitres(List<ChapitreType> chapitre) {
        if (!CollectionUtils.isEmpty(chapitre)) {
            chapitre.forEach(chapitreType -> {
                List<ClauseType> clause = chapitreType.getClause();
                if (!CollectionUtils.isEmpty(clause)) {
                    clause.forEach(clauseType -> {
                        ClauseType.Texte texte = clauseType.getTexte();
                        if (texte != null) {
                            texte.setValue(sanitizeHtmlInput(texte.getValue()));
                        }

                        clauseType.setTexteFixeApres(sanitizeHtmlInput(clauseType.getTexteFixeApres()));
                        clauseType.setTexteFixeAvant(sanitizeHtmlInput(clauseType.getTexteFixeAvant()));
                        FormulairesListe formulairesListeType = clauseType.getFormulairesListeType();
                        if (formulairesListeType != null && !CollectionUtils.isEmpty(formulairesListeType.getFormulation())) {
                            formulairesListeType.getFormulation().forEach(formulation -> formulation.setValue(sanitizeHtmlInput(formulation.getValue())));
                        }

                    });
                }
                sanitizeChapitres(chapitreType.getChapitre());

            });
        }
    }

    @Override
    public void addXmlDocument(InputStream xml, String documentId, String plateformeId) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(DocxDocument.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            String id = iDocumentConfigurationService.generateDocumentKey(plateformeId + "/" + documentId);
            DocxDocument docxDocument = (DocxDocument) unmarshaller.unmarshal(xml);
            docxDocument = sanitizeDocxDocument(docxDocument);
            String json = getObjectMapper().writeValueAsString(docxDocument);
            iDocumentXmlPort.addDocumentIfNotExist(DocumentXmlModel.builder().documentId(documentId).plateformeId(plateformeId).id(id).creationDate(new Timestamp(System.currentTimeMillis())).json(json).updatedJson(json).build());
        } catch (JAXBException | JsonProcessingException e) {
            log.error("Erreur lors de l'ajout du xml {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage());
        }

    }

    @Override
    public void addTemplateDocument(DocumentAdministrableConfiguration configuration, String documentId, String plateformeId) {
        try {
            String id = iDocumentConfigurationService.generateDocumentKey(plateformeId + "/" + documentId);
            String json = getObjectMapper().writeValueAsString(configuration);
            iDocumentXmlPort.addDocumentIfNotExist(DocumentXmlModel.builder().documentId(documentId).plateformeId(plateformeId).id(id).creationDate(new Timestamp(System.currentTimeMillis())).json(json).updatedJson(json).build());
        } catch (JsonProcessingException e) {
            log.error("Erreur lors de l'ajout du xml {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage());
        }

    }

    @Override
    public void addDocumentToValidate(String documentId, String plateformeId, Integer status) {
        try {
            String id = iDocumentConfigurationService.generateDocumentKey(plateformeId + "/" + documentId);
            DocxDocument docxDocument = new DocxDocument();
            docxDocument.setStatus(status);
            String json = getObjectMapper().writeValueAsString(docxDocument);
            iDocumentXmlPort.addDocumentIfNotExist(DocumentXmlModel.builder().documentId(documentId).plateformeId(plateformeId).id(id).redacStatus(status).creationDate(new Timestamp(System.currentTimeMillis())).json(json).updatedJson(json).build());
        } catch (JsonProcessingException e) {
            log.error("Erreur lors de l'ajout du xml {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage());
        }

    }

    @Override
    public ByteArrayOutputStream convertClause(ClauseType clause, boolean from, String chapitreNumero, String id) {
        if (!from) {
            return getDocumentFromClause(clause, id);
        } else {

            return getDocumentFromFile(clause, chapitreNumero, id);


        }

    }

    private ByteArrayOutputStream getDocumentFromClause(ClauseType clause, String id) {
        String storage = iDocumentConfigurationService.storagePath(id);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); XWPFDocument document = new XWPFDocument(); XWPFDocument documentRedac = new XWPFDocument(new FileInputStream(storage))) {
            double effectivePageWidth = getPageWidth(documentRedac);
            XWPFStyle xwpfStyle = StylesUtils.getStyleByName(documentRedac, "Tableau");
            if (xwpfStyle == null) {
                xwpfStyle = StylesUtils.getStyleByName(documentRedac, "Table");
            }
            if (xwpfStyle != null) {
                XWPFStyles styles = document.createStyles();
                styles.addStyle(xwpfStyle);
                CTFonts fonts = CTFonts.Factory.newInstance();
                fonts.setEastAsia("Arial");
                fonts.setHAnsi("Arial");

                styles.setDefaultFonts(fonts);
            }
            clauseRenderer.getXwpfRuns(clause, null, effectivePageWidth, document);
            document.write(out);
            log.info("Clause converti");
            return out;
        } catch (IOException e) {
            log.error("We had an error while creating the Word Doc {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage());
        }
    }

    private ByteArrayOutputStream getDocumentFromFile(ClauseType clause, String chapitreNumero, String id) {
        String storage = iDocumentConfigurationService.storagePath(id);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); XWPFDocument document = new XWPFDocument(new FileInputStream(storage))) {
            XWPFDocument clauseDocument = clauseRenderer.getXwpfDocumentFromClause(clause, document, chapitreNumero);
            clauseDocument.write(out);
            clauseDocument.close();
            log.info("Clause converti");
            return out;
        } catch (IOException e) {
            log.error("We had an error while creating the Word Doc {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage());
        }
    }

    @Override
    public ByteArrayOutputStream getXml(String document) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DocxDocument docxDocument = getObjectMapper().readValue(document, DocxDocument.class);
            sanitizeChapitres(docxDocument.getChapitre());
            setDocument(docxDocument);
            JAXBContext jaxbContext = JAXBContext.newInstance(DocxDocument.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(docxDocument, out);
            out.close();
            return out;
        } catch (JAXBException | IOException e) {
            log.error("Erreur lors de la récupération du xml {}", e.getMessage());
            return null;
        }

    }

    @Override
    public ByteArrayOutputStream getXml(DocxDocument document) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            JAXBContext jaxbContext = JAXBContext.newInstance(DocxDocument.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(document, out);
            out.close();
            return out;
        } catch (JAXBException | IOException e) {
            log.error("Erreur lors de la récupération du xml {}", e.getMessage());
            return null;
        }

    }

    private void setDocument(DocxDocument docxDocument) {
        docxDocument.setProgress(getProgress(docxDocument));
        docxDocument.setCompleted(docxDocument.getProgress() == 100.0);
        if (docxDocument.getStatus() == null) {
            docxDocument.setStatus(FileRedacStatusEnum.BROUILLON.getCode());
        }
    }

    @Override
    public DocumentXmlModel modifyDocumentXml(String id, DocxDocument document) {
        if (RedacActionEnum.VALIDATATION.name().equals(document.getLastAction())) {
            document.setStatus(FileRedacStatusEnum.VALIDER.getCode());
        } else if (RedacActionEnum.SUBMIT.name().equals(document.getLastAction())) {
            document.setStatus(FileRedacStatusEnum.A_VALIDER.getCode());
        } else {
            document.setStatus(FileRedacStatusEnum.BROUILLON.getCode());
        }
        document.setCompleted(isChapitresValide(document.getChapitre()));
        document.getChapitre().forEach(chapitre -> chapitre.setValide(isChapitresValide(chapitre.getChapitre()) && ClauseUtils.isClausesValide(chapitre.getClause())));
        return iDocumentXmlPort.modifyDocumentXml(id, document);
    }

    @Override
    public ByteArrayOutputStream convertDocument(InputStream inputStream, String originalFormat, String outputFormat) throws IOException {
        if ("docx".equalsIgnoreCase(originalFormat)) {
            try (XWPFDocument document = new XWPFDocument(inputStream)) {
                return convertDocx(outputFormat, document);
            }
        } else if ("xlsx".equalsIgnoreCase(originalFormat)) {
            return convertXlsx(outputFormat, inputStream);
        }
        throw new DocGenException(TypeExceptionEnum.EDITOR, "Extension " + outputFormat + " non reconnue");
    }

    @Override
    public ByteArrayOutputStream finalizeDocument(InputStream inputStream, String originalFormat, String outputFormat) throws IOException {
        if ("docx".equalsIgnoreCase(originalFormat)) {
            return convertDocx(inputStream, outputFormat);
        } else if ("xlsx".equalsIgnoreCase(originalFormat)) {
            return convertXlsx(outputFormat, inputStream);
        } else if (originalFormat.equals(outputFormat)) {
                ByteArrayOutputStream targetStream = new ByteArrayOutputStream();
                inputStream.transferTo(targetStream);
            return targetStream;
        } else {
            return convertFromExtension(outputFormat, inputStream, originalFormat);
        }

    }

    private ByteArrayOutputStream convertDocx(InputStream inputStream, String outputFormat) throws IOException {
        ByteArrayResource newInput = new ByteArrayResource(IOUtils.toByteArray(inputStream));
        try (ByteArrayInputStream srcInputStream = new ByteArrayInputStream(newInput.getByteArray()); ByteArrayInputStream destInputStream = new ByteArrayInputStream(newInput.getByteArray()); XWPFDocument srcDoc = new XWPFDocument(srcInputStream); XWPFDocument newDoc = new XWPFDocument(destInputStream);) {
            this.updateToc(srcDoc);

            List<IBodyElement> bodyElements = srcDoc.getBodyElements();
            List<CTSdtBlock> sdtList = newDoc.getDocument().getBody().getSdtList();
            for (IBodyElement iBodyElement : bodyElements) {
                finalyseSdt(srcDoc, newDoc, sdtList, iBodyElement);
            }
            removeSdt(newDoc, sdtList);
            XWPFDocumentUtils.deleteComments(newDoc);
            return convertDocx(outputFormat, newDoc);
        } catch (Exception e) {
            log.error("Erreur lors du finalize : {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.CONVERSION, e.getMessage(), e);
        }
    }

    private void finalyseSdt(XWPFDocument srcDoc, XWPFDocument newDoc, List<CTSdtBlock> sdtList, IBodyElement iBodyElement) {
        try {
            if (iBodyElement.getElementType().equals(BodyElementType.CONTENTCONTROL)) {
                XWPFSDT sdt = (XWPFSDT) iBodyElement;
                if (sdt.getTitle() != null && sdt.getTitle().startsWith("CHAPITRE")) {
                    CTSdtBlock clauseContentControl = srcDoc.getDocument().getBody().getSdtList().stream().filter(ctSdtBlock -> !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getAliasList()) && ctSdtBlock.getSdtPr().getAliasArray(0).getVal().equals(sdt.getTitle())).findFirst().orElse(null);
                    CTSdtBlock clauseContentControl2 = sdtList.stream().filter(ctSdtBlock -> ctSdtBlock != null && ctSdtBlock.getSdtPr() != null && !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getAliasList()) && ctSdtBlock.getSdtPr().getAliasArray(0).getVal().equals(sdt.getTitle())).filter(ctSdtBlock -> ctSdtBlock.getSdtContent() != null).findFirst().orElse(null);
                    if (clauseContentControl2 == null || clauseContentControl == null) return;
                    XmlCursor cursor = clauseContentControl2.getSdtContent().newCursor();
                    clauseContentControl2.unsetSdtContent();
                    copySdtContent(newDoc, cursor, clauseContentControl);
                }
            }
        } catch (Exception e) {
            log.error("Erreur lors du finalize : {}", e.getMessage());
        }
    }


    private ByteArrayOutputStream convertXlsx(String outputFormat, InputStream inputStream) {

        log.info("Conversion du document à {}", outputFormat);
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream(); XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {

            if ("xlsx".equalsIgnoreCase(outputFormat)) {
                workbook.write(outStream);
            } else {
                String key = UUID.randomUUID().toString();
                String child = "tmp_" + key;
                File folder = DocumentUtils.createFolder(storagePath, child);
                String child1 = key + ".xlsx";
                File file = new File(folder, child1);
                try (FileOutputStream out = new FileOutputStream(file)) {
                    workbook.write(out);
                }
                return getByteArrayOutputStreamFromOnlyOffice("xlsx", outputFormat, outStream, key, child, folder, child1);

            }
            return outStream;
        } catch (IOException e) {
            log.error("Erreur lors de la conversion du document {}", e.getMessage());
            return null;
        }
    }

    private ByteArrayOutputStream convertFromExtension(String outputFormat, InputStream inputStream, String extension) {

        log.info("Conversion du document à {}", outputFormat);
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {


            String key = UUID.randomUUID().toString();
            String child = "tmp_" + key;
            File folder = DocumentUtils.createFolder(storagePath, child);
            String child1 = key + "." + extension;
            File file = new File(folder, child1);
            try (FileOutputStream out = new FileOutputStream(file)) {
                IOUtils.copy(inputStream, out);
            }

            return getByteArrayOutputStreamFromOnlyOffice(extension, outputFormat, outStream, key, child, folder, child1);
        } catch (IOException e) {
            log.error("Erreur lors de la conversion du document {}", e.getMessage());
            return null;
        }
    }

    private ByteArrayOutputStream getByteArrayOutputStreamFromOnlyOffice(String filetype, String outputFormat, ByteArrayOutputStream outStream, String key, String child, File folder, String child1) throws IOException {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginAdmin, passwordAdmin));

        String jwt = iJwtService.generateJwtToken(authentication);

        String s = publicUrl + contextPath + REQUEST_TO_OPEN + child + "/" + child1 + TOKEN + jwt;
        String convertResponse = iOnlyOfficePort.convertFile(outputFormat, s, key, filetype);
        DocumentUtils.deleteFolder(folder);
        if (convertResponse != null) {
            InputStream convertedStream = iDocumentConfigurationService.getInputStreamFromUri(convertResponse);
            IOUtil.copyCompletely(convertedStream, outStream);
        } else {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, "Erreur lors de la conversion à " + outputFormat);
        }
        return outStream;
    }

    @Override
    public void updateToc(XWPFDocument srcDoc) {
        try {
            ByteArrayOutputStream stream = convertDocx("pdf", srcDoc);
            if (stream == null) {
                return;
            }
            ByteArrayInputStream pdf = new ByteArrayInputStream(stream.toByteArray());
            Map<String, Integer> listAnchorAndPage = PdfDocumentUtils.getListAnchorAndPage(pdf);
            if (!CollectionUtils.isEmpty(listAnchorAndPage)) {
                TocUtils.updateToc(srcDoc, listAnchorAndPage);
            }


        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour de la table de matière : {}", e.getMessage());
        }


    }

    @Override
    public ByteArrayOutputStream getTemplate(InputStream inputStream, Map<String, String> headersCallback) {
        Map<Integer, List<XWPFDocument>> positionBlocContainerCursorMap = new HashMap<>();
        Map<Integer, List<XWPFDocument>> positionParagraphCursorMap = new HashMap<>();

        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); XWPFDocument rootDocument = new XWPFDocument(inputStream)) {
            List<IBodyElement> bodyElements = rootDocument.getBodyElements();
            int position = rootDocument.getDocument().getBody().getSdtList().size() - 1;
            int positionParagraph = rootDocument.getParagraphs().size();
            for (int i = bodyElements.size() - 1; i >= 0; i--) {
                IBodyElement bodyElement = bodyElements.get(i);
                if (bodyElement instanceof XWPFSDT xwpfsdt) {
                    XWPFDocument drafDocument;
                    final String tag = xwpfsdt.getTag();
                    if (StringUtils.hasText(tag)) {
                        String text1 = xwpfsdt.getContent().getText();
                        try {
                            ChampFusionTag champFusionTag = getChampFusionTag(tag);
                            if (champFusionTag == null) continue;
                            AdministrationTypeEnum type = champFusionTag.getType();
                            if (type == null) continue;
                            switch (type) {
                                case CHAMP_FUSION_SIMPLE:
                                    drafDocument = new XWPFDocument();

                                    ISDTContent content = xwpfsdt.getContent();
                                    //récupérer le texte éventuel avant et après champ de fusion
                                    String debut;
                                    String fin;
                                    String[] tab1 = text1.split("\\[");
                                    if (tab1.length == 2) {
                                        debut = tab1[0];
                                    } else {
                                        debut = "";
                                    }
                                    String[] tab2 = text1.split("\\]");
                                    if (tab2.length == 2) {
                                        fin = tab2[1];
                                    } else {
                                        fin = "";
                                    }
                                    List<IBodyElement> isdtContents = getIsdtContents(content);
                                    String libelle = champFusionTag.getValue().getLibelle().replaceAll("\'", " ");
                                    log.info("traitement d'un champ de fusion simple: {}", libelle);
                                    isdtContents.stream().filter(XWPFParagraph.class::isInstance).map(XWPFParagraph.class::cast).filter(xwpfParagraph -> xwpfParagraph.getText().equals(debut + "[" + libelle + "]" + fin)).findFirst().ifPresent(paragraph -> {
                                        XWPFParagraph newParagraph = drafDocument.createParagraph();
                                        cloneParagraph(newParagraph, paragraph);
                                        newParagraph.getRuns().stream().filter(xwpfRun -> {
                                            String text = xwpfRun.getText(0);
                                            return text != null && text.contains(libelle);
                                        }).forEach(xwpfRun -> {
                                            String text = xwpfRun.getText(0);
                                            if (text != null) {
                                                xwpfRun.setText(debut + "[" + champFusionTag.getValue().getChampsFusion() + "]" + fin, 0);
                                            }
                                        });
                                    });
                                    if (!positionParagraphCursorMap.containsKey(positionParagraph)) {
                                        positionParagraphCursorMap.putIfAbsent(positionParagraph, new ArrayList<>());
                                    }
                                    positionParagraphCursorMap.get(positionParagraph).add(drafDocument);

                                    break;
                                case CHAMP_FUSION_COMPLEXE:
                                    log.info("traitement d'un champ de fusion complexe");
                                    HttpHeaders headers;
                                    if (headersCallback != null) headers = createHeaders(headersCallback);
                                    else headers = new HttpHeaders();
                                    ResponseEntity<Resource> response = atexoRestTemplate.exchange(champFusionTag.getValue().getBlocUrl(), HttpMethod.GET, new HttpEntity<>(headers), Resource.class);
                                    var ressource = response.getBody();
                                    if (ressource != null) {
                                        drafDocument = new XWPFDocument(ressource.getInputStream());
                                        positionBlocContainerCursorMap.putIfAbsent(position, new ArrayList<>());
                                        positionBlocContainerCursorMap.get(position).add(drafDocument);
                                        break;
                                    }
                                default:
                                    log.info("type non pris en compte");
                            }
                        } catch (Exception e) {
                            log.error("Erreur lors de la récupération de éléments de SDT {} avec le tag {} et le contenu {} => {}", xwpfsdt.getTitle(), tag, text1, e.getMessage());
                        }
                    }

                    position--;

                } else if (bodyElement instanceof XWPFParagraph paragraph) {
                    positionParagraph--;
                    addChampFusionParagraph(headersCallback, List.of(paragraph));
                } else if (bodyElement instanceof XWPFTable table) {
                    addChampFusionTable(headersCallback, List.of(table));
                }
            }
            rootDocument.getHeaderList().forEach(xwpfHeader -> {
                List<XWPFParagraph> paragraphs = xwpfHeader.getParagraphs();
                addChampFusionParagraph(headersCallback, paragraphs);
                List<XWPFTable> tables = xwpfHeader.getTables();
                addChampFusionTable(headersCallback, tables);
            });
            rootDocument.getFooterList().forEach(xwpfFooter -> {
                List<XWPFParagraph> paragraphs = xwpfFooter.getParagraphs();
                addChampFusionParagraph(headersCallback, paragraphs);
                List<XWPFTable> tables = xwpfFooter.getTables();
                addChampFusionTable(headersCallback, tables);
            });
            addDraftDocumentsToRootDocument(rootDocument, positionParagraphCursorMap);
            addSdt(rootDocument, positionBlocContainerCursorMap);
            Predicate<CTSdtBlock> predicateBody = s -> {

                if (!s.isSetSdtContent() || !s.isSetSdtPr() || CollectionUtils.isEmpty(s.getSdtPr().getTagList()))
                    return false;
                String tag = s.getSdtPr().getTagList().get(0).getVal();
                ChampFusionTag champFusionTag;
                try {
                    ObjectMapper objectMapper = getObjectMapper();
                    champFusionTag = objectMapper.readValue(tag, ChampFusionTag.class);
                } catch (Exception e) {
                    log.warn("Erreur lors de la récupération du tag {}", tag);
                    return false;
                }
                AdministrationTypeEnum generationTypeEnum = champFusionTag.getType();
                return generationTypeEnum.equals(AdministrationTypeEnum.CHAMP_FUSION_COMPLEXE) || generationTypeEnum.equals(AdministrationTypeEnum.CHAMP_FUSION_SIMPLE);
            };
            Predicate<CTSdtRun> predicateRun = s -> {

                if (!s.isSetSdtContent() || !s.isSetSdtPr() || CollectionUtils.isEmpty(s.getSdtPr().getTagList()))
                    return false;
                String tag = s.getSdtPr().getTagList().get(0).getVal();
                ChampFusionTag champFusionTag;
                try {
                    ObjectMapper objectMapper = getObjectMapper();
                    champFusionTag = objectMapper.readValue(tag, ChampFusionTag.class);
                } catch (Exception e) {
                    log.warn("Erreur lors de la récupération du tag {}", tag);
                    return false;
                }
                AdministrationTypeEnum generationTypeEnum = champFusionTag.getType();
                return generationTypeEnum.equals(AdministrationTypeEnum.CHAMP_FUSION_COMPLEXE) || generationTypeEnum.equals(AdministrationTypeEnum.CHAMP_FUSION_SIMPLE);
            };
            deleteSdtBlocks(rootDocument, predicateBody, predicateRun);
            rootDocument.write(out);
            log.info("Document converti en template");
            return out;
        } catch (IOException e) {
            log.error("erreur lors de la récupération du document");
            return null;
        }


    }

    private void addChampFusionTable(Map<String, String> headersCallback, List<XWPFTable> tables) {
        tables.forEach(xwpfTable -> {
            xwpfTable.getRows().forEach(xwpfTableRow -> {
                xwpfTableRow.getTableCells().forEach(xwpfTableCell -> {
                    List<XWPFParagraph> paragraphs1 = xwpfTableCell.getParagraphs();
                    addChampFusionParagraph(headersCallback, paragraphs1);
                });
            });
        });
    }

    private void addChampFusionParagraph(Map<String, String> headersCallback, List<XWPFParagraph> paragraphs) {
        paragraphs.forEach(xwpfParagraph -> {
            String text = xwpfParagraph.getText();
            log.info("traitement d'un paragraphe : {}", text);
            List<XWPFRun> runs = xwpfParagraph.getRuns();
            List<CTSdtRun> sdtList = xwpfParagraph.getCTP().getSdtList();
            if (CollectionUtils.isEmpty(runs) || !StringUtils.hasText(getRunText(runs))) {
                for (int j = sdtList.size() - 1; j >= 0; j--) {
                    CTSdtRun sdtRun = sdtList.get(j);
                    if (sdtRun != null && sdtRun.getSdtContent() != null) {
                        final String tag = sdtRun.getSdtPr().getTagList().get(0).getVal();
                        ChampFusionTag champFusionTag = getChampFusionTag(tag);
                        addChampFusion(headersCallback, sdtRun, champFusionTag, xwpfParagraph, null);
                    }
                }
            } else if (!CollectionUtils.isEmpty(sdtList)) {
                int indexRun = runs.size() - 1;
                int indexSdt = sdtList.size() - 1;
                do {
                    XWPFRun run = runs.get(indexRun);
                    String textRun = run.getText(0);
                    if (textRun != null && text.endsWith(textRun)) {
                        text = text.substring(0, text.length() - textRun.length());
                        indexRun--;
                    } else if (textRun != null) {
                        CTSdtRun ctSdtRun = sdtList.get(indexSdt);
                        String tag = ctSdtRun.getSdtPr().getTagList().get(0).getVal();
                        ChampFusionTag champFusionTag = getChampFusionTag(tag);
                        if (champFusionTag != null) {
                            String textSdt = "[" + champFusionTag.getValue().getLibelle() + "]";
                            text = text.substring(0, text.length() - textSdt.length());
                            addChampFusion(headersCallback, ctSdtRun, champFusionTag, xwpfParagraph, indexRun + 1);
                        }
                        indexSdt--;
                    } else {
                        indexRun--;
                    }
                } while (StringUtils.hasText(text) && indexRun >= 0 && indexSdt >= 0);

                for (int j = indexSdt; j >= 0; j--) {
                    CTSdtRun sdtRun = sdtList.get(j);
                    if (sdtRun != null && sdtRun.getSdtContent() != null) {
                        final String tag = sdtRun.getSdtPr().getTagList().get(0).getVal();
                        ChampFusionTag champFusionTag = getChampFusionTag(tag);
                        addChampFusion(headersCallback, sdtRun, champFusionTag, xwpfParagraph, 0);
                    }
                }
            }

        });
    }

    private String getRunText(List<XWPFRun> runs) {
        if (CollectionUtils.isEmpty(runs)) return null;
        return runs.stream().map(xwpfRun -> xwpfRun.getText(0)).filter(StringUtils::hasText).collect(Collectors.joining());
    }

    private void addChampFusion(Map<String, String> headersCallback, CTSdtRun ctSdtRun, ChampFusionTag champFusionTag, XWPFParagraph paragraph, Integer indexRun) {
        if (champFusionTag == null) return;
        try {
            AdministrationTypeEnum type = champFusionTag.getType();
            if (type == null) return;
            switch (type) {
                case CHAMP_FUSION_SIMPLE:
                    CTSdtContentRun content = ctSdtRun.getSdtContent();
                    List<CTR> isdtContents = content.getRList();
                    String libelle = champFusionTag.getValue().getLibelle();
                    log.info("traitement d'un champ de fusion simple: {}", libelle);
                    isdtContents.forEach(ctr -> {
                        XWPFRun run;
                        if (indexRun == null) {
                            run = paragraph.createRun();
                        } else {
                            run = paragraph.insertNewRun(indexRun);
                        }
                        cloneRun(run, ctr, null);
                        if (run.getText(0) != null && run.getText(0).contains(libelle)) {
                            run.setText("[" + champFusionTag.getValue().getChampsFusion() + "]", 0);
                        }
                    });


                    break;
                case CHAMP_FUSION_COMPLEXE:
                    log.info("traitement d'un champ de fusion complexe");
                    HttpHeaders headers;
                    if (headersCallback != null) headers = createHeaders(headersCallback);
                    else headers = new HttpHeaders();
                    ResponseEntity<Resource> response = atexoRestTemplate.exchange(champFusionTag.getValue().getBlocUrl(), HttpMethod.GET, new HttpEntity<>(headers), Resource.class);
                    if (response != null && response.getBody() != null) {
                        var ressource = response.getBody();
                        XWPFDocument drafDocument = new XWPFDocument(ressource.getInputStream());
                        // TODO CFC : ajouter les champs de fusion complexe
                        paragraph.getDocument().insertNewParagraph(paragraph.getCTP().newCursor()).createRun().setText(champFusionTag.getValue().getChampsFusion());
                        break;
                    }
                default:
                    log.info("type non pris en compte");
            }


        } catch (Exception e) {
            log.error("Erreur lors de la récupération de éléments avec le tag {}  => {}", champFusionTag.getValue().getLibelle(), e.getMessage());
        }

    }

    private static ChampFusionTag getChampFusionTag(String tag) {
        try {
            ObjectMapper objectMapper = getObjectMapper();
            return objectMapper.readValue(tag, ChampFusionTag.class);
        } catch (Exception e) {
            log.warn("Erreur lors de la récupération du tag {}", tag);
        }
        return null;
    }


    private ByteArrayOutputStream convertDocx(String outputFormat, XWPFDocument xwpfDocument) {
        log.info("Conversion du document à {}", outputFormat);
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {
            if ("docx".equalsIgnoreCase(outputFormat)) {
                xwpfDocument.write(outStream);
            } else {
                String key = UUID.randomUUID().toString();
                String child = "tmp_" + key;
                File folder = DocumentUtils.createFolder(storagePath, child);
                String child1 = key + ".docx";
                File file = new File(folder, child1);
                FileOutputStream out = new FileOutputStream(file);
                xwpfDocument.write(out);

                return getByteArrayOutputStreamFromOnlyOffice("docx", outputFormat, outStream, key, child, folder, child1);

            }
            return outStream;
        } catch (IOException e) {
            log.error("Erreur lors de la conversion du document {}", e.getMessage());
            return null;
        }

    }


    private void processChapitres(List<ChapitreType> chapitres, XWPFDocument doc, BigInteger cId, int compteur) {
        for (ChapitreType chapitre : chapitres) {
            chapitreRenderer.render(chapitre, doc, cId, compteur);
            compteur++;
            List<ChapitreType> subChapitre = chapitre.getChapitre();
            if (subChapitre != null) {
                this.processChapitres(subChapitre, doc, cId, compteur);
            }
        }

    }

    private float getProgress(DocxDocument document) {
        DocumentProgress documentProgress = DocumentProgress.builder().totalClause(0).clausesNonValides(0).build();

        List<ChapitreType> chapitres = document.getChapitre();
        getChapitresProgress(documentProgress, chapitres);
        return documentProgress.getProgress();

    }

    private void getChapitresProgress(DocumentProgress documentProgress, List<ChapitreType> chapitres) {
        if (chapitres != null) {
            for (ChapitreType chapitreType : chapitres) {
                if (chapitreType.getClause() != null) {
                    List<ClauseType> clauses = chapitreType.getClause().stream().filter(clauseType -> ClauseEtatEnum.fromValue(clauseType.getEtat()).getCode() < ClauseEtatEnum.ETAT_SUPPRIMER.getCode()).collect(Collectors.toList());
                    for (ClauseType clauseType : clauses) {
                        if (ClauseUtils.isClauseModifiable(clauseType) && !Boolean.TRUE.equals(clauseType.isCLAUSEVALIDE())) {
                            documentProgress.addClausesNonValides();
                        }
                        documentProgress.addTotalClause();

                    }
                }

                if (chapitreType.getChapitre() != null) {
                    this.getChapitresProgress(documentProgress, chapitreType.getChapitre());
                }
            }
        }
    }

    private void removeSdt(XWPFDocument newDoc, List<CTSdtBlock> sdtList) {
        int size = sdtList.size();
        for (int i = size - 1; i >= 0; i--) {
            CTSdtBlock ctSdtBlock = sdtList.get(i);
            if (ctSdtBlock != null && ctSdtBlock.getSdtPr() != null && !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getAliasList()) && ctSdtBlock.getSdtPr().getAliasArray(0).getVal().toUpperCase().startsWith("CHAPITRE")) {
                newDoc.getDocument().getBody().removeSdt(i);
            }
        }
    }

    private HttpHeaders createHeaders(Map<String, String> headers) {
        return new HttpHeaders() {
            {
                if (!CollectionUtils.isEmpty(headers)) headers.forEach((key, value) -> set(key, value));
            }
        };
    }

}
