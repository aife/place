package com.atexo.docgen.core.config.impl;

import ch.qos.logback.classic.sift.MDCBasedDiscriminator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.atexo.docgen.core.config.MDCService;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MDCDiscriminatorImpl extends MDCBasedDiscriminator implements MDCService {
    private static final String FILENAME = "fileName";

    @Override
    public String getDiscriminatingValue(ILoggingEvent event) {
        Map<String, String> mdcMap = event.getMDCPropertyMap();
        if (mdcMap.get(FILENAME) != null) {
            return mdcMap.get(FILENAME);
        }
        return "autres";

    }

    @Override
    public void putMDC(DocumentEditorDetails details) {
        if (details == null) {
            MDC.put(FILENAME, null);
        } else {
            String plateformeId = details.getPlateformeId();
            if (plateformeId != null) {
                MDC.put(FILENAME, plateformeId);
                MDC.put("pf", plateformeId);
            }
            String documentId = details.getDocumentId();
            if (documentId != null) {
                MDC.put("autres", "documentId=" + documentId);
            }
            User user = details.getUser();
            if (user != null) {
                MDC.put("utilisateur", user.getName());
            }
        }

    }

    @Override
    public void putErrorMDC() {
        MDC.put(FILENAME, "error");
    }
}
