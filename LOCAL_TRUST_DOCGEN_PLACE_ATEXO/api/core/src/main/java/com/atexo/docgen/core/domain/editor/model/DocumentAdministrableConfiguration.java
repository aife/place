package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentAdministrableConfiguration {

    private List<ChampsFusion> champsFusions;

    private List<DonneeTest> donneesTests;

}
