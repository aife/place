package com.atexo.docgen.core.domain.editor.model;


import com.atexo.docgen.core.domain.convertor.model.InfoBulle;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ChampsFusion {
    private String libelle;
    private String champsFusion;
    private String blocUrl;
    private String type;
    private InfoBulle infoBulle;
    private Boolean simple;
    private Boolean collection;
}
