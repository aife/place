package com.atexo.docgen.core.domain.monitoring.model;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class FileStatus {
    private String key;
    private String documentId;
    private String status;
    private String mode;
    private int redacStatus;
    private String redacAction;
    private int version;
    private Timestamp creationDate;
    private Timestamp modificationDate;
    private List<String> users;
}
