package com.atexo.docgen.core.domain.monitoring.model;

import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class MonitoringModel implements Serializable {
    private String title;
    private String id;
    private String documentId;
    private String plateformeId;
    private String status;
    private String updatedJson;
    private String fileType;
    private String redacAction;
    private int redacStatus;
    private Timestamp creationDate;
    private String mode;
    private boolean saved;

}
