package com.atexo.docgen.core.port;

public interface IChampFusionRacourciPort {


    String getChampFusionFromRacourci(String racourci);
}
