package com.atexo.docgen.core.domain.convertor.model.redac;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentProgress {

    private int totalClause;
    private int clausesNonValides;

    public void addClausesNonValides() {
        this.clausesNonValides++;
    }

    public void addTotalClause() {
        this.totalClause++;
    }

    public float getProgress() {
        if (this.totalClause == 0) return 100;
        return (float) (this.totalClause - this.clausesNonValides) / this.totalClause * 100;
    }
}
