package com.atexo.docgen.core.utilitaire;

import com.atexo.docgen.core.domain.convertor.model.ClauseType;
import com.atexo.docgen.core.domain.convertor.model.FormulairesListe;
import com.atexo.docgen.core.domain.convertor.model.redac.ClauseTag;
import com.atexo.docgen.core.enums.*;
import com.atexo.docgen.core.exceptions.DocGenException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.StringUtils.getReplace;

@Slf4j
public final class ClauseUtils {

    private ClauseUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    protected static String message = "La Clause %s dispose d'un contenu obligatoire, elle ne peut être validée sans contenu.";

    public static String getClauseBackground(ClauseType clause) {

        ClauseTypeEnum clauseTypeEnum = ClauseTypeEnum.fromValue(clause.getType());
        switch (clauseTypeEnum) {
            case TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case TYPE_CLAUSE_LISTE_MULTIPLE:
                if (Boolean.TRUE.equals(clause.isCLAUSEVALIDE())) {
                    return ClauseColorEnum.GREEN.getCode();
                } else if (Boolean.TRUE.equals(clause.isInitialise())) {
                    return ClauseColorEnum.RED.getCode();
                } else {
                    return ClauseColorEnum.YELLEOW.getCode();
                }
            case TYPE_CLAUSE_TEXTE_LIBRE:
            case TYPE_CLAUSE_TEXTE_PREVALORISE:
                if (Boolean.TRUE.equals(clause.isCLAUSEVALIDE())) {
                    return ClauseColorEnum.GREEN.getCode();
                } else {
                    return ClauseColorEnum.RED.getCode();
                }
            default:
                return null;


        }
    }


    public static ClauseStatusEnum getClauseStatus(ClauseType clause) {

        ClauseTypeEnum clauseTypeEnum = ClauseTypeEnum.fromValue(clause.getType());
        switch (clauseTypeEnum) {
            case TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case TYPE_CLAUSE_LISTE_MULTIPLE:
                if (Boolean.TRUE.equals(clause.isCLAUSEVALIDE())) {
                    return ClauseStatusEnum.VALIDE;
                } else if (Boolean.TRUE.equals(clause.isInitialise())) {
                    return ClauseStatusEnum.INVALIDE;
                } else {
                    return ClauseStatusEnum.INITIALE;
                }
            case TYPE_CLAUSE_TEXTE_LIBRE:
            case TYPE_CLAUSE_TEXTE_PREVALORISE:
                if (Boolean.TRUE.equals(clause.isCLAUSEVALIDE())) {
                    return ClauseStatusEnum.VALIDE;
                } else {
                    return Boolean.TRUE.equals(clause.getTexte().isObligatoire()) ? ClauseStatusEnum.INVALIDE_OBLIGATOIRE : ClauseStatusEnum.INVALIDE;
                }
            default:
                return ClauseStatusEnum.VALIDE;


        }
    }


    public static void validateClause(ClauseType clause) {
        switch (ClauseTypeEnum.fromValue(clause.getType())) {
            case TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case TYPE_CLAUSE_LISTE_MULTIPLE:
                validateFormulationsRuns(clause);
                break;
            case TYPE_CLAUSE_TEXTE_PREVALORISE:
            case TYPE_CLAUSE_TEXTE_LIBRE:
                validateClauseTexteRun(clause);
                break;
            default:
                log.warn("Clause n'est pas prise en compte pour la validation {}", clause.getType());

        }

    }


    public static void validateFormulationsRuns(ClauseType clause) {
        if (clause.isCLAUSEVALIDE().equals(Boolean.TRUE)) {

            if (clause.getFormulairesListeType() == null)
                throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));
            List<FormulairesListe.Formulation> formulations = clause.getFormulairesListeType().getFormulation();
            if (CollectionUtils.isEmpty(formulations)) {
                throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));
            }
            if (clause.getFormulairesListeType().isExclusif().equals(Boolean.TRUE)) {
                FormulairesListe.Formulation procoche = formulations.stream().filter(FormulairesListe.Formulation::isPrecoche).findFirst().orElse(null);
                if (procoche == null || org.apache.commons.lang.StringUtils.isBlank(procoche.getValue())) {
                    throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));
                }
            } else {
                List<FormulairesListe.Formulation> procoches = formulations.stream().filter(FormulairesListe.Formulation::isPrecoche)
                        .collect(Collectors.toList());
                for (FormulairesListe.Formulation formulation : procoches) {
                    String text = getReplace(formulation.getValue());
                    if (org.apache.commons.lang.StringUtils.isBlank(text)) {
                        throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));
                    }
                }
            }
        }
    }

    public static void validateClauseTexteRun(ClauseType clause) {
        if (clause.isCLAUSEVALIDE().equals(Boolean.TRUE) && ClauseUtils.isClauseModifiable(clause)) {
            if (clause.getTexte() == null || (org.apache.commons.lang.StringUtils.isBlank(clause.getTexte().getValue()) && clause.getTexte().isObligatoire().equals(Boolean.TRUE))) {
                throw new DocGenException(TypeExceptionEnum.CONVERSION, String.format(message, clause.getRef()));

            }
        }
    }

    public static boolean isClauseModifiable(ClauseType clause) {
        return clause != null && clause.isActive() && (
                (Boolean.TRUE.equals(clause.isDerogationResume())) ||
                        (clause.getFormulairesListeType() != null
                                && (!CollectionUtils.isEmpty(clause.getFormulairesListeType().getFormulation())) ||
                                clause.getTexte() != null && Boolean.TRUE.equals(clause.getTexte().isModifiable())));
    }

    public static ClauseTag getClauseTag(ClauseType clause) {
        List<String> roles = Collections.singletonList(isClauseModifiable(clause) ? UserRoleEnum.MODIFICATION.getValue() : UserRoleEnum.REPRENDRE.getValue());
        return ClauseTag.builder()
                .roles(roles)
                .status(getClauseStatus(clause).name())
                .build();
    }

    public static boolean isClausesValide(List<ClauseType> clauses) {
        if (clauses == null) {
            return true;
        }
        for (ClauseType clause : clauses) {
            if (clause.isCLAUSEVALIDE() == null || Boolean.FALSE.equals(clause.isCLAUSEVALIDE())) {
                return false;
            }
        }
        return true;
    }
}
