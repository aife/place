package com.atexo.docgen.core.domain.generator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum GenerationConditionEnum {
    NOT_EMPTY("isNotEmpty"),
    HAS_TEXT("hasText"),
    IS_BLANK("isBlank"),
    EMPTY("isEmpty"),
    EQUALS("="),
    NOT_EQUALS("!="),
    IS_CP ("isCP"),
    IS_CO ("isCO"),
    IS_CU ("isCU"),
    IS_PU ("isPU"),
    IS_DC ("isDC"),
    IS_NOT_NULL("isNotNull"),
    IS_NULL("isNull"),
    IS_TRUE("isTrue"),
    SUPERIEUR(">"),
    INFERIEUR("<"),
    SUPERIEUR_EQUAL(">="),
    INFERIEUR_EQUAL("<="),
    IS_FALSE("isFalse");;
    private final String value;

    GenerationConditionEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static GenerationConditionEnum fromValue(String value) {
        for (GenerationConditionEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
