package com.atexo.docgen.core.port;

import com.atexo.docgen.core.domain.convertor.model.redac.DocumentXmlModel;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;

public interface IDocumentXmlPort {


    void addDocumentIfNotExist(DocumentXmlModel xmlModel);

    DocumentXmlModel getDocumentById(String id);

    DocumentXmlModel modifyDocumentXml(String key, DocxDocument document);
}
