package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FileModeEnum {
    REDAC("redac"), EDIT("edit"), TEMPLATE("template"), VIEW("view"), VALIDATION("validation"), ANALYZE("analyse");
    private final String value;

    FileModeEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static FileModeEnum fromValue(String value) {
        for (FileModeEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        throw new IllegalArgumentException("Cannot find FileModeEnum from value: " + value);
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
