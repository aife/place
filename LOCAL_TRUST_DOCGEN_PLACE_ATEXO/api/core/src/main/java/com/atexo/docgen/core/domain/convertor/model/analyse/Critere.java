package com.atexo.docgen.core.domain.convertor.model.analyse;

import lombok.*;

import java.util.List;

@Getter
@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Critere {
    private int maximumNote;
    private String description;
    private List<Critere> sousCriteres;

}
