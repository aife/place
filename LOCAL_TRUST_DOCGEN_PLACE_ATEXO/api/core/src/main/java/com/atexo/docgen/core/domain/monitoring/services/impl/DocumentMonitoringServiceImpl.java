package com.atexo.docgen.core.domain.monitoring.services.impl;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.editor.model.DocumentConfiguration;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import com.atexo.docgen.core.domain.monitoring.services.IDocumentMonitoringService;
import com.atexo.docgen.core.enums.FileModeEnum;
import com.atexo.docgen.core.enums.FileRedacStatusEnum;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.port.IDocumentPort;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.SheetVisibility;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class DocumentMonitoringServiceImpl implements IDocumentMonitoringService {

    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentPort iDocumentPort;
    private final IDocumentConvertorService iDocumentConvertorService;

    @Value("${docgen.admin.password}")
    private String passwordAdmin;


    public DocumentMonitoringServiceImpl(IDocumentConfigurationService iDocumentConfigurationService, IDocumentPort iDocumentPort, IDocumentConvertorService iDocumentConvertorService) {
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentPort = iDocumentPort;
        this.iDocumentConvertorService = iDocumentConvertorService;
    }

    @Override
    public ByteArrayResource getLastRessourceVersion(DocumentModel document, DocumentConfiguration configuration) {

        ByteArrayResource resource = null;
        if (document.getRedacStatus() == FileRedacStatusEnum.VALIDER.getCode()) {
            resource = this.getNewVersionResource(document, ".brouillon", configuration);
        }
        if (resource == null) {
            String suffix = "." + document.getFileType() + "_" + document.getVersion();
            resource = this.getNewVersionResource(document, suffix, configuration);
        }
        return resource == null ? this.getNewVersionResource(document, "", configuration) : resource;
    }

    private ByteArrayResource getNewVersionResource(DocumentModel document, String suffix, DocumentConfiguration configuration) {

        return this.getResource(document, suffix, configuration);
    }


    @Override
    public ByteArrayResource getResource(DocumentModel document, String suffix, DocumentConfiguration configuration) {
        File fileToDownload = new File(iDocumentConfigurationService.storagePath(document.getId() + suffix));
        try {
            if (Files.exists(fileToDownload.toPath())) {
                if (configuration != null && !CollectionUtils.isEmpty(configuration.getActivatedSheetNames()) && "xlsx".equals(document.getFileType())) {
                    File tmp = getXlsxFileWithConfiguration(fileToDownload, configuration);
                    return new ByteArrayResource(Files.readAllBytes(tmp.toPath()));
                } else {
                    return new ByteArrayResource(Files.readAllBytes(fileToDownload.toPath()));
                }
            }
        } catch (IOException e) {
            log.error("Erreur lors de la récupération des resources {}", e.getMessage());
        }
        return null;
    }


    @Override
    public List<FileStatus> getFileStatus(List<DocumentModel> documents) {
        return documents.stream().map(documentModel -> FileStatus.builder()
                        .documentId(documentModel.getDocumentId())
                        .key(documentModel.getKey())
                        .status(documentModel.getStatus())
                        .modificationDate(documentModel.getModificationDate())
                        .mode(documentModel.getMode())
                        .creationDate(documentModel.getCreationDate())
                        .version(documentModel.getVersion())
                        .redacStatus(documentModel.getRedacStatus())
                        .redacAction(documentModel.getRedacAction())
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public DocumentModel getDocumentModelById(String id) throws FileNotFoundException {
        DocumentModel document = iDocumentPort.getById(id);
        if (document == null) {
            throw new FileNotFoundException();
        }
        return document;
    }

    @Override
    public List<DocumentModel> getDocumentModelByPlateforme(String plateforme) {
        return iDocumentPort.getDocumentsByPlateformeId(plateforme).stream()
                .filter(documentModel -> documentModel.getDocumentId() != null && documentModel.getPlateformeId() != null)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getDocumentModelByPlateformeAndStatus(String plateformeId, FileStatusEnum status) {
        return iDocumentPort.getDocumentsByPlateformeIdAndStatus(plateformeId, status)
                .stream()
                .map(DocumentModel::getDocumentId)
                .collect(Collectors.toList());
    }

    @Override
    public boolean addNewVersion(String id) {
        DocumentModel document = iDocumentPort.getById(id);

        FileStatusEnum statusEnum = FileStatusEnum.CLOSED_WITHOUT_EDITING;
        if (!document.isSaved()) {
            statusEnum = FileStatusEnum.EDITED_AND_SAVED;

            int newVersion = document.getVersion() + 1;
            try {
                if (FileModeEnum.REDAC.getValue().equals(document.getMode()) || FileModeEnum.VALIDATION.getValue().equals(document.getMode())) {
                    String storagePath = iDocumentConfigurationService.storagePath(id);
                    File originalFile = new File(storagePath);
                    File archiveFileDocx = new File(storagePath + "." + document.getFileType() + "_" + newVersion);
                    Files.deleteIfExists(archiveFileDocx.toPath());
                    Files.copy(originalFile.toPath(), archiveFileDocx.toPath());
                    String updatedJson = document.getUpdatedJson();
                    if (StringUtils.hasText(updatedJson)) {
                        File archiveFileXml = new File(storagePath + ".xml_" + newVersion);
                        Files.deleteIfExists(archiveFileXml.toPath());
                        byte[] bytes = iDocumentConvertorService.getXml(updatedJson).toByteArray();
                        Files.copy(new ByteArrayInputStream(bytes), archiveFileXml.toPath());
                    }

                    iDocumentPort.addNewVersion(id, newVersion, statusEnum);
                }
            } catch (IOException e) {
                log.error("Erreur lors de la création d'une version {} : {}", newVersion, e.getMessage());
                throw new DocGenException(TypeExceptionEnum.EDITOR, "Erreur lors de la création d'une version " + newVersion);
            }

        } else {
            iDocumentPort.modifyDocumentStatusById(id, statusEnum.name());
        }

        return statusEnum.name().equals(FileStatusEnum.EDITED_AND_SAVED.name());
    }

    @Override
    public void changeStatus(String id, String status) {
        iDocumentPort.modifyDocumentRedacStatusById(id, status);
    }


    public File getXlsxFileWithConfiguration(File file, DocumentConfiguration configuration) {
        List<String> activatedSheetNames = configuration.getActivatedSheetNames();
        String defaultSheetName = configuration.getDefaultSheetName();
        if (CollectionUtils.isEmpty(activatedSheetNames) && !StringUtils.hasText(defaultSheetName))
            return file;
        try (FileInputStream inputStream = new FileInputStream(file);
             XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {
            for (int i = workbook.getNumberOfSheets() - 1; i >= 0; i--) {
                String sheetName = workbook.getSheetAt(i).getSheetName();
                if (!activatedSheetNames.contains(sheetName) && workbook.getSheetVisibility(i) != SheetVisibility.VISIBLE) {
                    workbook.setSheetVisibility(i, SheetVisibility.VISIBLE);
                }
            }
            workbook.setActiveSheet(configuration.getInitialDefaultSheetIndex());
            inputStream.close();
            Path tmp = Files.createTempFile("original-", ".xlsx");
            FileOutputStream out = new FileOutputStream(tmp.toFile());
            workbook.write(out);
            out.close();
            workbook.close();
            return tmp.toFile();

        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return file;
    }

}
