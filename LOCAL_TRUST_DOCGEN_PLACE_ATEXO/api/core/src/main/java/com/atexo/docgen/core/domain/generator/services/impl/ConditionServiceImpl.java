package com.atexo.docgen.core.domain.generator.services.impl;

import com.atexo.docgen.core.domain.generator.model.GenerationConditionEnum;
import com.atexo.docgen.core.domain.generator.services.IConditionService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static com.atexo.docgen.core.utilitaire.DocumentUtils.getReplacement;

@Service
public class ConditionServiceImpl implements IConditionService {
    @Override
    public boolean isVerified(JsonNode jsonNode, String condition) {
        if (condition == null || condition.isEmpty()) {
            return true;
        }
        if (condition.contains(" et ")) {
            final String[] blocs = condition.split(" et ");
            boolean isVerified = true;
            for (String bloc : blocs) {
                isVerified = isVerified && isVerified(jsonNode, bloc);
            }
            return isVerified;

        } else if (condition.contains(" ou ")) {
            final String[] blocs = condition.split(" ou ");
            boolean isVerified = false;
            for (String bloc : blocs) {
                isVerified = isVerified || isVerified(jsonNode, bloc);
            }
            return isVerified;
        }
        String conditionField;
        //gestion des conditions >64 caracteres, avec remplacement de isFalse par !
        if (condition.charAt(0) == '!') {
            String[] tab = condition.split("!");
            conditionField = tab[1];
            final JsonNode result = getReplacement(jsonNode, conditionField);
            return result != null && !result.asText().equals("null") && result.isBoolean() && !result.asBoolean();
        }
        final List<String> split = Arrays.asList(condition.split(" "));
        conditionField = split.get(0);
        final JsonNode replacement = getReplacement(jsonNode, conditionField);
        if (split.size() == 1) {
            return replacement != null && replacement.isBoolean() && replacement.asBoolean();
        }
        if (split.contains(GenerationConditionEnum.NOT_EMPTY.getValue())) {
            return replacement != null && !replacement.asText().equals("null") && ((replacement.isArray() && !replacement.isEmpty()) || !replacement.isArray());
        }
        if (split.contains(GenerationConditionEnum.IS_TRUE.getValue())) {
            return replacement != null && !replacement.asText().equals("null") && replacement.isBoolean() && replacement.asBoolean();
        }
        if (split.contains(GenerationConditionEnum.IS_FALSE.getValue())) {
            return replacement != null && !replacement.asText().equals("null") && replacement.isBoolean() && !replacement.asBoolean();
        }
        if (split.contains(GenerationConditionEnum.EMPTY.getValue())) {
            return replacement == null || replacement.asText().equals("null") || replacement.isArray() && replacement.isEmpty();
        }
        if (split.contains(GenerationConditionEnum.EQUALS.getValue())) {
            return replacement != null && !replacement.asText().equals("null") && split.size() == 3 &&
                    replacement.asText().equals(split.get(2));
        }
        if (split.contains(GenerationConditionEnum.NOT_EQUALS.getValue())) {
            return replacement != null && !replacement.asText().equals("null") && split.size() == 3 &&
                    !replacement.asText().equals(split.get(2));
        }

        if (split.contains(GenerationConditionEnum.IS_NULL.getValue())) {
            return replacement == null || replacement.asText().equals("null");
        }
        if (split.contains(GenerationConditionEnum.IS_BLANK.getValue())) {
            return replacement == null || replacement.asText().equals("null") || replacement.asText().isEmpty();
        }

        if (split.contains(GenerationConditionEnum.IS_NOT_NULL.getValue())) {
            return replacement != null && !replacement.asText().equals("null");
        }
        if (split.contains(GenerationConditionEnum.HAS_TEXT.getValue())) {
            return replacement != null && !replacement.asText().equals("null") && !replacement.asText().isEmpty();
        }

        if (split.contains(GenerationConditionEnum.SUPERIEUR.getValue())) {
            if (replacement == null || !replacement.asText().equals("null") || split.size() != 3 || !replacement.isNumber()) {
                return false;
            }
            double value = replacement.asDouble();
            double valueToCompare = Double.parseDouble(split.get(2));
            return value > valueToCompare;
        }
        if (split.contains(GenerationConditionEnum.SUPERIEUR_EQUAL.getValue())) {
            if (replacement == null || !replacement.asText().equals("null") || split.size() != 3 || !replacement.isNumber()) {
                return false;
            }
            double value = replacement.asDouble();
            double valueToCompare = Double.parseDouble(split.get(2));
            return value >= valueToCompare;
        }
        if (split.contains(GenerationConditionEnum.INFERIEUR.getValue())) {
            if (replacement == null || !replacement.asText().equals("null") || split.size() != 3 || !replacement.isNumber()) {
                return false;
            }
            double value = replacement.asDouble();
            double valueToCompare = Double.parseDouble(split.get(2));
            return value < valueToCompare;
        }
        if (split.contains(GenerationConditionEnum.INFERIEUR_EQUAL.getValue())) {
            if (replacement == null || !replacement.asText().equals("null") || split.size() != 3 || !replacement.isNumber()) {
                return false;
            }
            double value = replacement.asDouble();
            double valueToCompare = Double.parseDouble(split.get(2));
            return value <= valueToCompare;
        }

        return false;
    }
}
