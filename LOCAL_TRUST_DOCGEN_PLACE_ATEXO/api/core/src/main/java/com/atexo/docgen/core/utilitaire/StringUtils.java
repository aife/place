package com.atexo.docgen.core.utilitaire;


import java.util.ArrayList;
import java.util.List;

public final class StringUtils {
    private StringUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static String getReplace(String ft) {
        return ft != null ? ft.trim().replaceAll(" +", " ").replaceAll("\n", "") : null;
    }

    public static List<String> intersection(List<String> list1, List<String> list2) {
        List<String> list = new ArrayList<>();
        if (list1 != null && list2 != null)
            for (String t : list1) {
                if (list2.contains(t)) {
                    list.add(t);
                }
            }

        return list;
    }
}
