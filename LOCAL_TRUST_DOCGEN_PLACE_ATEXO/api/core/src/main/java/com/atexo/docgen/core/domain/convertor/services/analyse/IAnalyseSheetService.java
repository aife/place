package com.atexo.docgen.core.domain.convertor.services.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConsultationConfig;
import com.atexo.docgen.core.domain.convertor.model.analyse.LotConfig;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;
import java.util.Map;

public interface IAnalyseSheetService {
    Map<String, Integer> setLotsSheets(ConsultationConfig metaConfig, XSSFWorkbook workbook, Configuration configuration);

    void setLotInGeneral(Configuration configuration, Map<String, Integer> totalLotRow, XSSFSheet sheet, FormulaEvaluator evaluator, int srcStartRow, int cellnum, List<LotConfig> lots);

    Map<String, Integer> setNonAllotiSheet(ConsultationConfig metaConfig, XSSFWorkbook workbook, Configuration configuration);

    void setNonAllotieGeneralSheet(ConsultationConfig consultation, Configuration configuration, Map<String, Integer> totalLotRow, XSSFSheet sheet, FormulaEvaluator evaluator, int srcStartRow, int cellnum);
}
