package com.atexo.docgen.core.domain.extractor.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PositionToExtract {
    @NonNull
    private PositionValue position;
    @NonNull
    private PositionValue startTargetPosition;
    @NonNull
    private String sheetName;
}
