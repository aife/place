package com.atexo.docgen.core.config;

import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;

public interface MDCService {

    void putMDC(DocumentEditorDetails details);

    void putErrorMDC();
}
