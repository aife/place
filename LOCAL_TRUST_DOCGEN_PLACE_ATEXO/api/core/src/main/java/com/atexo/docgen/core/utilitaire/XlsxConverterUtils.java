package com.atexo.docgen.core.utilitaire;

import com.atexo.docgen.core.domain.generator.model.ExcelPosition;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.text.AttributedString;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public final class XlsxConverterUtils {

    private XlsxConverterUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static List<XSSFWorkbook> readXlsxFiles(List<MultipartFile> multipartFiles) {
        return multipartFiles.stream()
                .filter(file -> file.getOriginalFilename() != null)
                .map(XlsxConverterUtils::readXlsxFile)
                .collect(Collectors.toList());
    }


    // methode for get only on cell by position file
    public static Cell getExcellCellByCellPosition(CellReference cellPosition, Sheet sheet) {
        return sheet
                .getRow(cellPosition
                        .getRow())
                .getCell(cellPosition
                        .getCol());
    }

    public static XSSFWorkbook readXlsxFile(MultipartFile file) {
        try {
            return new XSSFWorkbook(file.getInputStream());
        } catch (IOException e) {
            log.error("Erreur lors de l'importation d'un fichier au format xlsx' : {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.MAPPER, e.getMessage());
        }
    }


    static void copyStyles(Cell sourceCell, Cell targetCell) {
        CellStyle sourceCellStyle = sourceCell.getCellStyle();
        targetCell.setCellStyle(sourceCellStyle);
    }

    public static void copyCells(Cell cell1, Cell cell2) {
        if (cell1 == null || cell2 == null) {
            return;
        }
        switch (cell1.getCellType()) {
            case STRING:
                String string1 = cell1.getStringCellValue();
                cell2.setCellValue(string1);
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell1)) {
                    Date date1 = cell1.getDateCellValue();
                    cell2.setCellValue(date1);
                } else {
                    double cellValue1 = cell1.getNumericCellValue();
                    cell2.setCellValue(cellValue1);
                }
                break;
            case FORMULA:
                String formula1 = cell1.getCellFormula();
                cell2.setCellFormula(formula1);
                break;

            default:
                log.warn("Clause n'est pas prise en compte pour la validation {}", cell1.getCellType());

        }

        copyStyles(cell1, cell2);

    }

    public static String getCellStringValue(FormulaEvaluator evaluator, XSSFSheet sheet, int roxIndex, int columnIndex) {
        try {
            XSSFCell cellStatus = sheet.getRow(roxIndex).getCell(columnIndex);

            CellValue evaluate = evaluator.evaluate(cellStatus);
            String calculatedStatus = null;
            if (evaluate != null)
                calculatedStatus = evaluate.getStringValue();
            return calculatedStatus;
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de {}{}", CellReference.convertNumToColString(columnIndex), roxIndex + 1);
            return null;
        }
    }

    public static String getCellType(XSSFSheet sheet, int roxIndex, int columnIndex) {
        try {
            XSSFCell cellStatus = sheet.getRow(roxIndex).getCell(columnIndex);
            return cellStatus.getCellType().name();
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de {}{}", CellReference.convertNumToColString(columnIndex), roxIndex + 1);
            return null;
        }
    }

    public static Double getCellNumberValue(FormulaEvaluator evaluator, XSSFSheet sheet, int roxIndex, int columnIndex) {
        try {
            XSSFCell cellStatus = sheet.getRow(roxIndex).getCell(columnIndex);

            CellValue evaluate = evaluator.evaluate(cellStatus);
            Double calculatedStatus = null;
            if (evaluate != null)
                calculatedStatus = evaluate.getNumberValue();
            return calculatedStatus;
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de {}{}", CellReference.convertNumToColString(columnIndex), roxIndex + 1);
            return null;
        }
    }


    public static void adjustHeight(XSSFCell cell) {
        XSSFFont font = cell.getCellStyle().getFont();
        // Create Font object with Font attribute (e.g. Font family, Font size, etc) for calculation
        String fontName = font == null || font.getFontName() == null ? "Calibri" : font.getFontName();
        short fontHeight = (short) (font == null ? 12 : font.getFontHeightInPoints());
        String cellValue = cell.getStringCellValue();

        final XSSFSheet sheet = cell.getSheet();
        XSSFRow row = cell.getRow();
        int colwidthinchars = sheet.getColumnWidth(cell.getColumnIndex()) / 256;

        colwidthinchars = Math.round(colwidthinchars * 12f / fontHeight);

        int neededrows = getNeededrows(colwidthinchars, cellValue);

        log.info("needed rows " + neededrows);

        //get default row height
        float defaultrowheight = sheet.getDefaultRowHeightInPoints();
        float height = neededrows * defaultrowheight;
        if (neededrows <= 1) {
            return;
        }
        row.setHeightInPoints(height);
        XSSFCellStyle rowStyle = cell.getCellStyle();
        if (rowStyle == null) {
            rowStyle = sheet.getWorkbook().createCellStyle();
        }
        rowStyle.setWrapText(true);
        cell.setCellStyle(rowStyle);

        AttributedString attributedString = new AttributedString(cellValue);
        attributedString.addAttribute(TextAttribute.FAMILY, fontName, 0, cellValue.length());
        attributedString.addAttribute(TextAttribute.SIZE, (float) font.getFontHeightInPoints());
        if (font.getBold())
            attributedString.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD, 0, cellValue.length());
        if (font.getItalic())
            attributedString.addAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE, 0, cellValue.length());

        FontRenderContext fontRenderContext = new FontRenderContext(null, true, true);

        TextLayout layout = new TextLayout(attributedString.getIterator(), fontRenderContext);
        Rectangle2D bounds = layout.getBounds();

        log.info("height {} / calculated height {} ", bounds.getBounds2D().getHeight(), height);

    }

    private static int getNeededrows(int colwidthinchars, String cellValue) {
        String[] chars = cellValue.split("");
        int neededrows = 1;
        int counter = 0;
        for (int i = 0; i < chars.length; i++) {
            counter++;
            if (counter == colwidthinchars) {
                neededrows++;
                counter = 0;
            } else if ("\n".equals(chars[i])) {
                log.info("new line because of new line mark");
                neededrows++;
                counter = 0;
            } else if ("\t".equals(chars[i])) {
                counter += 2;
            }
        }
        return neededrows;
    }


    public static void replaceCell(ExcelPosition excelPosition, XSSFSheet sheetAt, String replacementString) {
        XSSFCell cell = sheetAt.getRow(excelPosition.getRow())
                .getCell(excelPosition.getColumn());
        String content = cell
                .getStringCellValue().replaceAll("\\[" + Pattern.quote(excelPosition.getChampsFusion()) + "]", replacementString);
        cell.setCellValue(content);
    }


    public static void setConditionnalFormatting(XSSFSheet sheet, CellRangeAddress[] regions) {
        XSSFSheetConditionalFormatting rules = sheet.getSheetConditionalFormatting();
        final int numConditionalFormattings = rules.getNumConditionalFormattings();
        for (int i = 0; i < numConditionalFormattings; i++) {
            log.info("{}", i);
            ConditionalFormatting rule = rules.getConditionalFormattingAt(i);
            final int numberOfRules = rule.getNumberOfRules();
            for (int j = 0; j < numberOfRules; j++) {
                log.info("{}", j);
                rules.addConditionalFormatting(regions, rule.getRule(j));

            }
        }
    }


    public static XSSFCell setFormula(XSSFSheet sheet, FormulaEvaluator evaluator, List<Integer> rowsIndex, int cellIndex, int rowIndex,
                                      String prefixFormula, String suffixFormula, String delimiter) {
        String letter = CellReference.convertNumToColString(cellIndex);
        XSSFRow row = sheet.getRow(rowIndex);
        XSSFCell cell = row.getCell(cellIndex);
        if (cell == null) {
            cell = row.createCell(cellIndex);
        }
        cell.setCellType(CellType.FORMULA);
        String formula = prefixFormula +
                rowsIndex.stream().map(integer -> letter + integer).collect(Collectors.joining(delimiter)) +
                suffixFormula;
        cell.setCellFormula(formula);
        evaluator.evaluateFormulaCell(cell);
        DataFormat dataFormat = sheet.getWorkbook().createDataFormat();
        CellStyle numberFormatStyle = cell.getCellStyle();
        if (numberFormatStyle == null)
            numberFormatStyle = sheet.getWorkbook().createCellStyle();
        numberFormatStyle.setDataFormat(dataFormat.getFormat("#,##0.00"));
        cell.setCellStyle(numberFormatStyle);
        return cell;
    }

    public static void setPrintSetup(XSSFSheet baseSheet, XSSFSheet sheet, int rowSplit, int colSplit) {

        XSSFPrintSetup printSetupBase = baseSheet.getPrintSetup();
        XSSFPrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setLandscape(printSetupBase.getLandscape());
        printSetup.setOrientation(printSetupBase.getOrientation());
        printSetup.setDraft(printSetupBase.getDraft());
        printSetup.setFitHeight(printSetupBase.getFitWidth());
        printSetup.setPaperSize(printSetupBase.getPaperSize());
        printSetup.setFitWidth(printSetupBase.getFitWidth());
        final CellRangeAddress repeatingRows = baseSheet.getRepeatingRows();
        sheet.setRepeatingRows(repeatingRows);
        final CellRangeAddress repeatingColumns = baseSheet.getRepeatingColumns();
        sheet.setRepeatingColumns(repeatingColumns);

        colSplit = repeatingColumns != null ? repeatingColumns.getLastColumn() + 1 :
                colSplit;
        rowSplit = repeatingRows != null ? repeatingRows.getLastRow() + 1 : rowSplit;
        sheet.createFreezePane(colSplit, rowSplit);
    }
}
