package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentAction {
    private int type;
    private String userid;
}
