package com.atexo.docgen.core.exceptions;

import com.atexo.docgen.core.enums.TypeExceptionEnum;

public class MauvaisParametreException extends DocGenException {
    private static final long serialVersionUID = 100L;


    public MauvaisParametreException(TypeExceptionEnum type, String message) {
        super(type, message);
    }

}
