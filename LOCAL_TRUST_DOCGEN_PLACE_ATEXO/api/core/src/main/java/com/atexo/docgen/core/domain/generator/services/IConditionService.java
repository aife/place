package com.atexo.docgen.core.domain.generator.services;

import com.fasterxml.jackson.databind.JsonNode;

public interface IConditionService {
    boolean isVerified(JsonNode jsonNode, String condition);
}
