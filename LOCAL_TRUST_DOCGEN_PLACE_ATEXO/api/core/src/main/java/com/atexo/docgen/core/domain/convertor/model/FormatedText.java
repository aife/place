package com.atexo.docgen.core.domain.convertor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class FormatedText {
    private String text;
    private boolean isCarriageReturn;
    private List<TagFormat> format;

    private String idLi;

    public boolean isBullet() {
        return this.format.stream().anyMatch(tagFormat -> tagFormat.getFormat().equalsIgnoreCase("Bullet"));
    }

}
