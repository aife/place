package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ClauseEtatEnum {
    ETAT_DISPONIBLE(0), ETAT_MODIFIER(1), ETAT_SUPPRIMER(2), ETAT_CONDITIONNER(3);
    private final int code;

    ClauseEtatEnum(int code) {
        this.code = code;
    }

    @JsonCreator
    public static ClauseEtatEnum fromValue(int value) {
        for (ClauseEtatEnum operator : values()) {
            if (operator.getCode() == value) {
                return operator;
            }
        }
        throw new IllegalArgumentException("Cannot find ClauseEtatEnum from value: " + value);
    }

    @JsonValue
    public int getCode() {
        return code;
    }
}

