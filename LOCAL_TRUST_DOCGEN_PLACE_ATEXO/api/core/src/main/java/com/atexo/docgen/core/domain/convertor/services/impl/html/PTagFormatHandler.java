package com.atexo.docgen.core.domain.convertor.services.impl.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import org.apache.commons.lang.StringUtils;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

public class PTagFormatHandler implements TagFormatHandler {

    @Override
    public TagFormat parse(XmlPullParser xpp) {
        String style = xpp.getAttributeValue("", "style");

        final TagFormat tf = TagFormat.builder()
                .name(xpp.getName())
                .format(xpp.getName())
                .build();
        if (StringUtils.isNotBlank(style)) {
            tf.setFormat("style");
            for (String st : style.split(";")) {
                String[] styleDef = st.split(":");
                String attribute = this.transcodeAttribute(styleDef[0].toLowerCase().trim());
                tf.addAttribute(attribute, styleDef[1].trim());
            }
        }

        return tf;
    }

    private String transcodeAttribute(String attrb) {
        if ("background".equals(attrb)) {
            return "background-color";
        } else {
            return attrb;
        }
    }

    @Override
    public FormatedText closing(TagFormat tagFormat) {
        List<TagFormat> formatList = new ArrayList<>();
        formatList.add(tagFormat);
        return FormatedText.builder().format(formatList)
                .text(null)
                .isCarriageReturn(true).build();
    }
}
