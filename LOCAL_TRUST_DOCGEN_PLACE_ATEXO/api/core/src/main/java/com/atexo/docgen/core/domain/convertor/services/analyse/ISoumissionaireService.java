package com.atexo.docgen.core.domain.convertor.services.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.convertor.model.analyse.Soumissionnaire;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;

public interface ISoumissionaireService {
    void setSoumissionnairesColumn(List<Soumissionnaire> soumissionnaires, XSSFSheet sheet, Configuration configuration);

    void setSoumissionnairesFormula(XSSFSheet sheet, int nbrSoumissionaires, int destStartRow, FormulaEvaluator evaluator, Configuration configuration, int soumissionaireStartColumn);
}
