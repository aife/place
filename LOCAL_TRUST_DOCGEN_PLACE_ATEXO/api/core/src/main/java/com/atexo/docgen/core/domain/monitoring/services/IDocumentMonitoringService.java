package com.atexo.docgen.core.domain.monitoring.services;

import com.atexo.docgen.core.domain.editor.model.DocumentConfiguration;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import com.atexo.docgen.core.enums.FileStatusEnum;
import org.springframework.core.io.ByteArrayResource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface IDocumentMonitoringService {


    ByteArrayResource getLastRessourceVersion(DocumentModel document, DocumentConfiguration configuration);

    ByteArrayResource getResource(DocumentModel document, String suffix, DocumentConfiguration configuration);

    List<FileStatus> getFileStatus(List<DocumentModel> documents) throws IOException;

    DocumentModel getDocumentModelById(String token) throws FileNotFoundException;

    List<DocumentModel> getDocumentModelByPlateforme(String plateforme);

    List<String> getDocumentModelByPlateformeAndStatus(String plateformeId, FileStatusEnum status);

    boolean addNewVersion(String id);

    void changeStatus(String id, String status);
}
