package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Plugins {
    private List<String> autostart = new ArrayList<>();
    private List<String> pluginsData = new ArrayList<>();
}
