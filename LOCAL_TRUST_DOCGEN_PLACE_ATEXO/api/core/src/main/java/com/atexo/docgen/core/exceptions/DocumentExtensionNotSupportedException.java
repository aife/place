package com.atexo.docgen.core.exceptions;

import com.atexo.docgen.core.enums.TypeExceptionEnum;

public class DocumentExtensionNotSupportedException extends DocGenException {
    private static final long serialVersionUID = 100L;

    public DocumentExtensionNotSupportedException(TypeExceptionEnum type) {
        super(type, "File type is not supported");
    }

    public DocumentExtensionNotSupportedException(TypeExceptionEnum type, String message) {
        super(type, message);
    }
}
