package com.atexo.docgen.core.domain.convertor.model.analyse;


import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties
public class Configuration {
    private PositionValue soumissionnaireDebut;
    private PositionValue apercuDebut;
    private PositionValue titreLot;
    private PositionValue critereDebut;
    private PositionValue critereFin;
    private PositionValue celluleControle;
    private ConsultationConfig meta;
}
