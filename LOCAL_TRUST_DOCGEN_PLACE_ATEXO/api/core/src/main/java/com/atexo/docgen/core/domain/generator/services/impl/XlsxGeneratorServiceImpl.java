package com.atexo.docgen.core.domain.generator.services.impl;

import com.atexo.docgen.core.domain.generator.model.ExcelPosition;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.SheetRequest;
import com.atexo.docgen.core.domain.generator.model.XlsxHeader;
import com.atexo.docgen.core.domain.generator.services.IDocumentGeneratorService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atexo.docgen.core.utilitaire.DocumentUtils.getReplacement;
import static com.atexo.docgen.core.utilitaire.XlsxConverterUtils.replaceCell;

@Component
@Slf4j
public class XlsxGeneratorServiceImpl implements IDocumentGeneratorService {

    private final ObjectMapper objectMapper;
    @Value("${docgen.champs-fusion.default-null}")
    private String champsFusionDefaultNullValue;

    @Value("${docgen.champs-fusion.default-not-found}")
    private String champsFusionDefaultNotFoundValue;
    @Value("${docgen.champs-fusion.regex}")
    private String champsFusionRegex;

    public XlsxGeneratorServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public ByteArrayOutputStream generate(FileGeneratorRequest file, String champsFusionDefault, String dateFormat) throws IOException {

        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream(); InputStream inputStream = file.getStream();
                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        ) {


            Map<String, List<ExcelPosition>> variables = new HashMap<>();

            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                XSSFSheet sheet = workbook.getSheetAt(i);
                log.info("Extraction de la liste des variables dans {}", sheet.getSheetName());
                getFileVariable(sheet, i, variables);
            }
            log.info("Remplacement des variables");
            Map<String, Object> keyValues = file.getKeyValues();
            JsonNode newNode = objectMapper.valueToTree(keyValues);
            variables.forEach((s, excelPositions) -> replaceTextFor(workbook, excelPositions, newNode, champsFusionDefault));
            workbook.write(out);
            return out;
        }

    }


    @Override
    public Map<String, Set<String>> getFileVariable(InputStream inputStream) throws IOException {
        Map<String, Set<String>> variables = new HashMap<>();
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        Map<String, List<ExcelPosition>> simpleVariables = new HashMap<>();
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            getFileVariable(sheet, i, simpleVariables);
        }
        variables.put("SIMPLE", simpleVariables.keySet());
        return variables;

    }

    @Override
    public ByteArrayOutputStream buildDocument(List<SheetRequest> xlsx) throws IOException {
        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                XSSFWorkbook workbook = new XSSFWorkbook();
        ) {
            xlsx.forEach(sheetRequest -> {
                XSSFSheet sheet = workbook.createSheet(sheetRequest.getName());
                //ligne des titres colonnes
                int rownum = 0;

                Cell cell;

                XSSFCellStyle titleStyle = this.createStyleForTitle(workbook);
                for (XlsxHeader value : sheetRequest.getColumns()) {
                    Row row = sheet.createRow(rownum);
                    cell = row.createCell(value.getColumnNum(), CellType.STRING);
                    cell.setCellValue(value.getTitle());
                    cell.setCellStyle(titleStyle);
                    rownum++;
                    for (String s : value.getValues()) {
                        row = sheet.createRow(rownum);
                        cell = row.createCell(value.getColumnNum(), value.getType());
                        cell.setCellValue(s);
                        rownum++;
                    }
                }

            });
            workbook.write(out);
            return out;
        }
    }


    public Map<String, List<ExcelPosition>> getVariables(XSSFWorkbook workbook) {
        Map<String, List<ExcelPosition>> simpleVariables = new HashMap<>();
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            getFileVariable(sheet, i, simpleVariables);
        }
        return simpleVariables;

    }

    private void getFileVariable(XSSFSheet sheet, int sheetIndex, Map<String, List<ExcelPosition>> variables) {

        int lastRow = sheet.getLastRowNum();
        int firstRow = sheet.getFirstRowNum();

        for (int rowIndex = firstRow; rowIndex <= lastRow && firstRow != -1; rowIndex++) {
            XSSFRow row = sheet.getRow(rowIndex);
            if (row == null) {
                continue;
            }
            int firstColumns = row.getFirstCellNum();
            int lastColumns = row.getLastCellNum();

            for (int columnIndex = firstColumns; columnIndex <= lastColumns && firstColumns != -1; columnIndex++) {
                XSSFCell cell = row.getCell(columnIndex);
                if (cell == null || !cell.getCellType().equals(CellType.STRING)) {
                    continue;
                }
                String content = cell.getStringCellValue();
                getContentVariable(sheetIndex, variables, rowIndex, columnIndex, content);

            }
        }
        log.info("Fin de la recherche de la liste des variables dans {}", sheet.getSheetName());
    }

    private void getContentVariable(int sheetIndex, Map<String, List<ExcelPosition>> variables, int rowIndex, int columnIndex, String content) {
        if (content.contains("[")) {
            Pattern pat = Pattern.compile(champsFusionRegex);
            Matcher m = pat.matcher(content);
            while (m.find()) {
                String text = m.group(1);
                log.info("Variable trouvée : {}", text);

                variables.computeIfAbsent(text, k -> new ArrayList<>());
                variables.get(text).add(ExcelPosition.builder().sheetIndex(sheetIndex).champsFusion(text).row(rowIndex).column(columnIndex).build());
            }
        }
    }


    private void replaceTextFor(XSSFWorkbook workbook, List<ExcelPosition> excelPositions, JsonNode jsonNode, String champsFusionDefault) {
        excelPositions.forEach(excelPosition -> {
            XSSFSheet sheetAt = workbook.getSheetAt(excelPosition.getSheetIndex());
            log.info("Remplacement de {}  dans le sheet {}", excelPosition.getChampsFusion(), sheetAt.getSheetName());
            JsonNode replacement = getReplacement(jsonNode, excelPosition.getChampsFusion());
            String replacementString;
            if (replacement == null) {
                replacementString = champsFusionDefault != null ? champsFusionDefault : champsFusionDefaultNotFoundValue;
            } else {
                replacementString = replacement.asText();
            }

            if (replacementString == null || replacementString.equals("null")) {
                replacementString = champsFusionDefault != null ? champsFusionDefault : champsFusionDefaultNullValue;
            }
            replaceCell(excelPosition, sheetAt, replacementString);
        });


    }

    private XSSFCellStyle createStyleForTitle(XSSFWorkbook workbook) {
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        return style;
    }

}
