package com.atexo.docgen.core.domain.generator.model;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String error;
    private String stack;


}
