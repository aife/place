package com.atexo.docgen.core.utilitaire.word;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.LocaleUtil;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atexo.docgen.core.utilitaire.word.SdtUtils.setSdtStyle;

@Slf4j
public final class TocUtils {

    private static final String TOC_HEADER = "Sommaire";

    private TocUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static CTSdtBlock buildToc(XWPFDocument doc) {
        CTBody body = doc.getDocument().getBody();
        CTSdtBlock block = getCtSdtBlock(body);

        if (block == null) {
            block = body.addNewSdt();
            CTSdtPr sdtPr = block.addNewSdtPr();
            CTDecimalNumber id = sdtPr.addNewId();
            id.setVal(new BigInteger("4844945"));
            CTPlaceholder ctPlaceholder = sdtPr.addNewPlaceholder();
            ctPlaceholder.addNewDocPart().setVal("DefaultPlaceholder_TEXT");
            CTSdtDocPart ctSdtDocPart = sdtPr.addNewDocPartObj();
            ctSdtDocPart.addNewDocPartGallery().setVal("Table of Contents");
            ctSdtDocPart.addNewDocPartUnique();
            sdtPr.addNewRPr();

        }
        String styleId = StylesUtils.getStyleByName(doc, "Contents Heading", "En-ttedetabledesmatires");

        initTocHeader(block, styleId);
        return block;
    }

    private static void initTocHeader(CTSdtBlock block, String styleId) {
        if (block.isSetSdtContent()) {
            block.unsetSdtContent();
        }

        CTSdtContentBlock content = block.addNewSdtContent();
        CTP p = content.addNewP();
        p.setRsidR("00EF7E24".getBytes(LocaleUtil.CHARSET_1252));
        p.setRsidRDefault("00EF7E24".getBytes(LocaleUtil.CHARSET_1252));
        p.addNewPPr().addNewPStyle().setVal(styleId);
        p.addNewR().addNewT().setStringValue(TocUtils.TOC_HEADER);
    }

    public static CTSdtBlock getCtSdtBlock(CTBody body) {
        return getCtSdtBlock(body, "Table of Contents");
    }

    public static CTSdtBlock getCtSdtBlock(CTBody body, String sdtType) {
        CTSdtBlock block = null;
        List<CTSdtBlock> sdtList = body.getSdtList();
        if (!CollectionUtils.isEmpty(sdtList)) {
            for (int i = sdtList.size() - 1; i >= 0; i--) {
                CTSdtBlock ctSdtBlock = sdtList.get(i);
                if (ctSdtBlock != null && ctSdtBlock.getSdtPr() != null && !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getDocPartObjList()) &&
                        ctSdtBlock.getSdtPr().getDocPartObjList().get(0).getDocPartGallery() != null &&
                        sdtType.equals(ctSdtBlock.getSdtPr().getDocPartObjList().get(0).getDocPartGallery().getVal())) {
                    if (block == null) {
                        block = ctSdtBlock;
                    } else {
                        body.removeSdt(sdtList.indexOf(block));
                        block = ctSdtBlock;

                    }
                }
            }
        }
        return block;
    }

    public static void addRow(CTSdtBlock block, String styleId, String title, int page, String bookmarkRef, XWPFDocument xwpfDocument, int compteur) {

        XWPFStyle style = StylesUtils.getStyleById(xwpfDocument, styleId);
        CTSdtContentBlock contentBlock = block.getSdtContent();
        CTP p = contentBlock.addNewP();
        CTStyle ctStyle = style.getCTStyle();
        if (ctStyle.getRsid() != null) {
            p.setRsidR(ctStyle.getRsid().getVal());
            p.setRsidRDefault(ctStyle.getRsid().getVal());
        } else {
            p.setRsidR("00EF7E24".getBytes(LocaleUtil.CHARSET_1252));
            p.setRsidRDefault("00EF7E24".getBytes(LocaleUtil.CHARSET_1252));
        }
        CTPPr pPr = p.addNewPPr();

        pPr.addNewPStyle().setVal(styleId);
        CTPPr pPr1 = ctStyle.getPPr();
        CTTabs tabs = pPr.addNewTabs();
        if (pPr1 != null && pPr1.getTabs() != null && pPr1.getTabs().getTabList() != null && !pPr1.getTabs().getTabList().isEmpty()) {
            tabs.setTabArray(pPr1.getTabs().getTabList().toArray(new CTTabStop[0]));
        } else {
            CTTabStop tab = tabs.addNewTab();
            tab.setVal(STTabJc.RIGHT);
            tab.setLeader(STTabTlc.DOT);
            tab.setPos(new BigInteger("8290"));
            tab = tabs.addNewTab();
            tab.setVal(STTabJc.LEFT);
            tab.setLeader(STTabTlc.NONE);
            tab.setPos(new BigInteger("685"));
        }
        if (compteur == 1) {
            p.addNewR();
            CTR ctr = p.addNewR();
            ctr.addNewFldChar().setFldCharType(STFldCharType.BEGIN);
            CTText text = ctr.addNewInstrText();
            text.setSpace(SpaceAttribute.Space.PRESERVE);
            text.setStringValue("TOC \\o &quot;1-9&quot; \\h ");
            ctr.addNewFldChar().setFldCharType(STFldCharType.SEPARATE);
            p.addNewR();
        }
        CTHyperlink cthyperLink = p.addNewHyperlink();
        cthyperLink.setAnchor(bookmarkRef);
        cthyperLink.setHistory(STOnOff.Enum.forInt(1));
        cthyperLink.setTooltip("Lien vers " + title);
        CTR run = cthyperLink.addNewR();
        run.addNewT().setStringValue(title);
        run = cthyperLink.addNewR();
        run.addNewTab();
        run = cthyperLink.addNewR();
        run.addNewFldChar().setFldCharType(STFldCharType.BEGIN);
        var text = run.addNewInstrText();
        text.setSpace(SpaceAttribute.Space.PRESERVE);
        text.setStringValue(" PAGEREF " + bookmarkRef + " \\h ");
        run.addNewFldChar().setFldCharType(STFldCharType.SEPARATE);
        CTText ctText = run.addNewT();
        ctText.setStringValue(Integer.toString(page));
        ctText.setSpace(SpaceAttribute.Space.PRESERVE);
        run.addNewFldChar().setFldCharType(STFldCharType.END);
        p.addNewR();
        log.info("Ajout de la ligne \"{}\" page \"{}\"", title, page);
    }



    public static void populateToc(XWPFDocument xwpfDocument, CTSdtBlock block) {
        populateToc(xwpfDocument, block, new HashMap<>());
    }

    public static void populateToc(XWPFDocument xwpfDocument, CTSdtBlock block, Map<String, Integer> listAnchorAndPage) {
        if (xwpfDocument == null || block == null) {
            return;
        }
        String styleId = StylesUtils.getStyleByName(xwpfDocument, "Contents Heading", "En-ttedetabledesmatires");

        initTocHeader(block, styleId);

        Map<Integer, Integer> levels = new HashMap<>();
        int compteur = 0;
        for (XWPFParagraph paragraph : xwpfDocument.getParagraphs()) {
            XWPFStyle parStyle = StylesUtils.getStyleById(xwpfDocument, paragraph.getStyle());
            if (parStyle != null && parStyle.getName().toLowerCase().startsWith("heading")) {
                compteur++;
                try {
                    final String substring = parStyle.getName().substring("Heading".length() + 1);
                    int level = Integer.parseInt(substring);
                    levels.keySet().stream().filter(integer -> integer > level).forEach(integer -> levels.put(integer, 0));

                    levels.merge(level, 1, Integer::sum);

                    StringBuilder levelBuilder = new StringBuilder();
                    if (level == 1) {
                        levelBuilder.append("Article ");
                    }
                    levelBuilder.append(levels.get(1));
                    if (level == 1) {
                        levelBuilder.append(" -");
                    } else {
                        for (int i = 2; i <= level; i++) {
                            levelBuilder.append(".").append(levels.get(i));
                        }
                    }

                    levelBuilder.append(" ").append(paragraph.getText());
                    int page = 3;
                    String pageAnchor = listAnchorAndPage.keySet().stream().filter(s -> s.replace(" ", "").toUpperCase().startsWith(levelBuilder.toString().toUpperCase().replace(" ", "")))
                            .findFirst().orElse(null);
                    if (pageAnchor != null) {
                        page = listAnchorAndPage.get(pageAnchor);
                    }
                    String tocStyleId = StylesUtils.getStyleByName(xwpfDocument, "Toc " + level, "TM" + level);

                    List<CTBookmark> bookmarkStartList = paragraph.getCTP().getBookmarkStartList();
                    if (!CollectionUtils.isEmpty(bookmarkStartList)) {
                        addRow(block, tocStyleId, levelBuilder.toString(), page, bookmarkStartList.get(0).getName(), xwpfDocument, compteur);
                    }
                } catch (Exception e) {
                    log.error("Erreur toc {}", e.getMessage());
                }
            }
        }
        setEndToc(block);
    }

    private static void setEndToc(CTSdtBlock block) {
        CTSdtContentBlock contentBlock = block.getSdtContent();
        CTP p = contentBlock.addNewP();
        p.addNewR();
        CTR ctr = p.addNewR();
        ctr.addNewFldChar().setFldCharType(STFldCharType.END);
        p.addNewR();
        p.addNewR();
    }


    public static void updateToc(XWPFDocument srcDoc, Map<String, Integer> listAnchorAndPage) {
        CTSdtBlock ctSdtBlock = getCtSdtBlock(srcDoc.getDocument().getBody());
        populateToc(srcDoc, ctSdtBlock, listAnchorAndPage);
    }

    public static void updateToc(XWPFDocument srcDoc) {
        CTSdtBlock ctSdtBlock = getCtSdtBlock(srcDoc.getDocument().getBody());
        populateToc(srcDoc, ctSdtBlock, new HashMap<>());
    }


}
