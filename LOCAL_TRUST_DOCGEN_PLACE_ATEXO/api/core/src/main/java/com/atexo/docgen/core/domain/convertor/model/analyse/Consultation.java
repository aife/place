package com.atexo.docgen.core.domain.convertor.model.analyse;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Consultation {
    private List<Lot> lots;
    private String reference;
    private String objet;
    private String intitule;
    private List<Critere> criteres;
    private List<Soumissionnaire> soumissionnaires;
}
