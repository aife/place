package com.atexo.docgen.core.domain.editor.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class Customization {
    private Goback goback;
    private boolean autosave;
    private boolean forcesave;
    private Customer customer;


}
