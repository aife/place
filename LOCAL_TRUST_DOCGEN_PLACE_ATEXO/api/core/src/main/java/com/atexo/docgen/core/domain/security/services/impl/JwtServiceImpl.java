package com.atexo.docgen.core.domain.security.services.impl;

import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.security.model.AtexoUserDetails;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.IJwtService;
import com.atexo.docgen.core.exceptions.JwtDocGenException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class JwtServiceImpl implements IJwtService {

    @Value("${docgen.jwt.expirationMs}")
    private int jwtExpirationMs;
    @Value("${docgen.jwt.secret}")
    private String jwtSecret;
    private final ObjectMapper objectMapper;

    public JwtServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public DocumentEditorDetails getEditorDetailsFromToken(String token) {
        Object claims = getTokenClaims(token).get("details");
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(claims), DocumentEditorDetails.class);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération des details de token =>  {}", e.getMessage());
            return null;
        }
    }

    @Override
    public String generateJwtToken(Authentication authentication) {

        AtexoUserDetails userPrincipal = (AtexoUserDetails) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    @Override
    public String generateJwtToken(String key, DocumentEditorRequest editorRequest, String plateformeId, String id) {
        DocumentEditorDetails editorDetails = DocumentEditorDetails.builder()
                .id(id)
                .documentId(editorRequest.getDocumentId())
                .plateformeId(plateformeId)
                .mode(editorRequest.getMode())
                .callback(editorRequest.getCallback())
                .headersCallback(editorRequest.getHeadersCallback())
                .user(editorRequest.getUser())
                .configuration(editorRequest.getConfiguration())
                .build();

        return Jwts.builder()
                .claim("details", editorDetails)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    @Override
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            throw new JwtDocGenException("Invalid JWT signature: " + e.getMessage());
        } catch (MalformedJwtException e) {
            throw new JwtDocGenException("Invalid JWT token: " + e.getMessage());
        } catch (ExpiredJwtException e) {
            throw new JwtDocGenException("JWT token is expired: " + e.getMessage());
        } catch (UnsupportedJwtException e) {
            throw new JwtDocGenException("JWT token is unsupported: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new JwtDocGenException("JWT claims string is empty: " + e.getMessage());
        }

    }

    @Override
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    private Claims getTokenClaims(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
    }

}
