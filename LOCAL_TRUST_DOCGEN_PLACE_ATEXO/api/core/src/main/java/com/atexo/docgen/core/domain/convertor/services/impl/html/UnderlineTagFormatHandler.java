package com.atexo.docgen.core.domain.convertor.services.impl.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import org.apache.commons.lang.StringUtils;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


public class UnderlineTagFormatHandler implements TagFormatHandler {

    @Override
    public TagFormat parse(XmlPullParser xpp) {
        final TagFormat tf = TagFormat.builder()
                .name(xpp.getName())
                .format("underline")
                .build();

        String style = xpp.getAttributeValue("", "style");
        if (StringUtils.isNotBlank(style)) {
            Stream.of(style.split(";")).forEach(st -> {
                String[] styleDef = st.split(":");
                tf.addAttribute(styleDef[0].toLowerCase().trim(), styleDef[1].trim());
            });
        }
        return tf;
    }

    @Override
    public FormatedText closing(TagFormat tagFormat) {
        List<TagFormat> formatList = new ArrayList<>();
        formatList.add(tagFormat);
        return FormatedText.builder().format(formatList)
                .text(null)
                .build();
    }
}
