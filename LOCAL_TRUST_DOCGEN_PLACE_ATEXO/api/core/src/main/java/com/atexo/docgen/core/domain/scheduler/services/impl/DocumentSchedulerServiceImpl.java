
package com.atexo.docgen.core.domain.scheduler.services.impl;


import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.monitoring.model.MonitoringModel;
import com.atexo.docgen.core.domain.scheduler.services.DocumentSchedulerService;
import com.atexo.docgen.core.enums.FileModeEnum;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.core.port.IDocumentPort;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atexo.docgen.core.enums.FileStatusEnum.*;

@Service
@Transactional
@Slf4j
public class DocumentSchedulerServiceImpl implements DocumentSchedulerService {

    @Value("${external-apis.onlyoffice.storage-folder}")
    private String storageFolder;

    @Value("${docgen.scheduler.clean-up-period}")
    private int cleanUpPeriod;

    private static final long DELAI_SUPPRESION_FICHIER_TMP = 1000L * 60L * 60L * 24L;
    private final IDocumentPort iDocumentPort;

    public DocumentSchedulerServiceImpl(IDocumentPort iDocumentPort) {
        this.iDocumentPort = iDocumentPort;
    }

    @Scheduled(initialDelayString = "${docgen.scheduler.delete-document.delay}", fixedRateString = "${docgen.scheduler.delete-document.rate}")
    @Override
    public void deleteFiles() throws IOException {
        File folder = new File(storageFolder);
        if (!folder.exists()) {
            return;
        }
        try (Stream<Path> pathStream = Files.list(folder.toPath())) {
            for (Path path : pathStream.collect(Collectors.toList())) {
                String fileNameWithOutExt = FilenameUtils.removeExtension(path.getFileName().toString());
                DocumentModel document = iDocumentPort.getById(fileNameWithOutExt);
                if (document == null && !Files.isDirectory(path)) {
                    log.info("Suppression du fichier {}", path);
                    Files.deleteIfExists(path);
                } else if (Files.isDirectory(path) && path.startsWith("tmp_")) {
                    try (Stream<Path> walk = Files.walk(folder.toPath())) {
                        walk.sorted(Comparator.reverseOrder())
                                .map(Path::toFile)
                                .forEach(File::deleteOnExit);
                    }
                }
            }
        }
    }



    @Scheduled(initialDelayString = "${docgen.scheduler.update-document-status.delay}", fixedRateString = "${docgen.scheduler.update-document-status.rate}")
    @Override
    public void updateDocumentsStatus() {
        List<MonitoringModel> modelList = iDocumentPort.getAll();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        for (MonitoringModel document : modelList) {
            long milliseconds = now.getTime() - document.getCreationDate().getTime();
            int seconds = (int) (milliseconds / 1000);
            int hours = (int) seconds / 3600;
            if (hours > 24 && document.getStatus() != null) {
                FileStatusEnum statusEnum = FileStatusEnum.valueOf(document.getStatus());
                if (OPENED.equals(statusEnum)) {
                    if (FileModeEnum.VIEW.equals(FileModeEnum.fromValue(document.getMode()))) {
                        iDocumentPort.modifyDocumentStatusById(document.getId(), CLOSED_WITHOUT_EDITING.name());
                    } else {
                        iDocumentPort.modifyDocumentStatusById(document.getId(), FileStatusEnum.CORRUPTED.name());
                    }
                } else if (EDITED_AND_SAVED.equals(statusEnum)) {
                    iDocumentPort.modifyDocumentStatusById(document.getId(), FileStatusEnum.SAVED_AND_CLOSED.name());
                }
            }
            if (seconds > 30 && document.getStatus() != null && REQUEST_TO_OPEN.equals(FileStatusEnum.valueOf(document.getStatus()))) {
                iDocumentPort.modifyDocumentStatusById(document.getId(), CLOSED_WITHOUT_EDITING.name());
            } else if (document.getStatus() == null) {
                iDocumentPort.modifyDocumentStatusById(document.getId(), CORRUPTED.name());
            }
        }
    }


    @Scheduled(initialDelayString = "${docgen.scheduler.delete-document.delay}", fixedRateString = "${docgen.scheduler.delete-document.rate}")
    @Override
    public void deleteDocument() throws IOException {
        List<MonitoringModel> modelList = iDocumentPort.getAll();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        for (MonitoringModel document : modelList) {
            long milliseconds = now.getTime() - document.getCreationDate().getTime();
            int days = (int) milliseconds / 1000 / 3600 / 24;
            if (days > cleanUpPeriod) {
                iDocumentPort.deleteById(document.getId());
                File file = new File(storageFolder, document.getId());
                Files.deleteIfExists(file.toPath());
                File xml = new File(storageFolder, document.getId() + ".xml");
                Files.deleteIfExists(xml.toPath());

                log.info("Suppression du document {} de la plateforme {} avec success (id ={})", document.getDocumentId(), document.getPlateformeId(), document.getId());
            }
        }
    }

    @Scheduled(cron = "${docgen.scheduler.cron.delete-tmp}")
    public void cleanTmpFiles() {
        File[] files = FileUtils.getTempDirectory().listFiles();
        long date = System.currentTimeMillis() - DELAI_SUPPRESION_FICHIER_TMP;
        for (File file : files) {
            if (file.getName().startsWith("javamelody")) {
                continue;
            }
            if (FileUtils.isFileOlder(file, date)) {

                log.info("fichier {} supprime", file.getAbsolutePath());
                    FileUtils.deleteQuietly(file);
            }
        }

    }

    @Scheduled(cron = "${docgen.scheduler.cron.delete-onlyoffice}")
    public void cleanOnlyofficeFiles() {
        File[] files = FileUtils.getFile(storageFolder).listFiles();
        long date = System.currentTimeMillis() - DELAI_SUPPRESION_FICHIER_TMP;
        for (File file : files) {
            if (FileUtils.isFileOlder(file, date)) {
                    log.info("fichier {} supprime", file.getAbsolutePath());
                    FileUtils.deleteQuietly(file);

            }
        }

    }
}
