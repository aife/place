package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ClauseColorEnum {
    YELLEOW("F5EBA4"), GREEN("D4EDDA"), RED("F8D7DA"), BLUE("D4E7ED");
    private final String code;

    ClauseColorEnum(String code) {
        this.code = code;
    }

    @JsonCreator
    public static ClauseColorEnum fromValue(String value) {
        for (ClauseColorEnum operator : values()) {
            if (operator.getCode().equals(value)) {
                return operator;
            }
        }
        throw new IllegalArgumentException("Cannot find ClauseColorEnum from value: " + value);
    }

    @JsonValue
    public String getCode() {
        return code;
    }
}

