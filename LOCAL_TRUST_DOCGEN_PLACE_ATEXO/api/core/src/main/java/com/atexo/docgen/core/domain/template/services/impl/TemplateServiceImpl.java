package com.atexo.docgen.core.domain.template.services.impl;

import com.atexo.docgen.core.domain.template.services.ITemplateService;
import com.atexo.docgen.core.utilitaire.word.StylesUtils;
import com.atexo.docgen.core.utilitaire.word.TocUtils;
import com.atexo.docgen.core.utilitaire.word.WordDocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import static com.atexo.docgen.core.utilitaire.word.NumeringUtils.setDefaultDocumentNumering;
import static com.atexo.docgen.core.utilitaire.word.SdtUtils.addEmptySdt;
import static com.atexo.docgen.core.utilitaire.word.StylesUtils.setDefaultDocumentStyle;

@Component
@Slf4j
public class TemplateServiceImpl implements ITemplateService {



    @Override
    public ByteArrayOutputStream applyStyle(FileInputStream baseTemplate, FileInputStream destTemplate) {
        if (destTemplate == null && baseTemplate == null) {
            return applyDefaultStyle();
        }
        if (baseTemplate == null) {
            return applyDefaultStyle(destTemplate);
        }
        return applyStyleFromDocument(baseTemplate, destTemplate);
    }
    private ByteArrayOutputStream applyDefaultStyle(FileInputStream destTemplate) {
        try (XWPFDocument destDocument = new XWPFDocument(destTemplate); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            setDefaultDocumentStyle(destDocument);
            destDocument.write(outputStream);
            return outputStream;
        } catch (IOException e) {
            log.error("Erreur lors de la copie des style : {}", e.getMessage());
        }
        return null;
    }

    private ByteArrayOutputStream applyDefaultStyle() {
        try (XWPFDocument destDocument = new XWPFDocument(); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            setDefaultDocumentStyle(destDocument);
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            String image = "logo.png";
            InputStream inputStream = contextClassLoader.getResourceAsStream(image);
            WordDocumentUtils.addHeaderImage(destDocument, inputStream, image);
            double pageWidth = WordDocumentUtils.getPageWidth(destDocument);
            log.info("Création du template avec largeur {}", pageWidth);
            BigInteger chapterId = setDefaultDocumentNumering(destDocument);

            addEmptyRun(destDocument);
            addEmptyRun(destDocument);
            addEmptyRun(destDocument);
            addEmptyRun(destDocument);
            addEmptyRun(destDocument);

            XWPFParagraph paragraph = destDocument.createParagraph();
            paragraph.setStyle("Titreprincipal");
            XWPFRun run = paragraph.createRun();
            run.setText("[TITLE]");
            TocUtils.buildToc(destDocument);

            for (int i = 0; i < 9; i++) {
                paragraph = destDocument.createParagraph();

                CTBookmark bookmark = paragraph.getCTP().addNewBookmarkStart();
                bookmark.setName("_Toc" + i);
                bookmark.setId(BigInteger.valueOf(i));
                paragraph.setNumID(chapterId);
                if (paragraph.getCTP().getPPr().getNumPr().getIlvl() == null) {
                    paragraph.getCTP().getPPr().getNumPr().addNewIlvl();
                }
                paragraph.getCTP().getPPr().getNumPr().getIlvl().setVal(BigInteger.valueOf(i));
                String styleId = StylesUtils.getStyleByName(destDocument, "Heading " + (i + 1), "Heading" + (i + 1));
                paragraph.setStyle(styleId);

                run = paragraph.createRun();
                if (i == 0)
                    run.addBreak(BreakType.PAGE);
                run.setText("TITRE " + (i + 1));
                paragraph.getCTP().addNewBookmarkEnd().setId(BigInteger.valueOf(i));
            }
            addEmptySdt(destDocument);
            TocUtils.updateToc(destDocument);

            destDocument.write(outputStream);
            return outputStream;
        } catch (IOException | InvalidFormatException e) {
            log.error("Erreur lors de la copie des style : {}", e.getMessage());
        }
        return null;
    }

    private void addEmptyRun(XWPFDocument destDocument) {
        XWPFParagraph paragraph = destDocument.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("");

    }


    private ByteArrayOutputStream applyStyleFromDocument(FileInputStream baseTemplate, FileInputStream destTemplate) {
        try (XWPFDocument srcDocument = new XWPFDocument(baseTemplate);
             XWPFDocument destDocument = new XWPFDocument(destTemplate);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            setDefaultDocumentStyle(destDocument);
            destDocument.write(outputStream);
            return outputStream;
        } catch (IOException e) {
            log.error("Erreur lors de la copie des style : {}", e.getMessage());
        }
        return null;
    }
}
