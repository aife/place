package com.atexo.docgen.core.utilitaire.word;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.model.WordStyle;
import com.atexo.docgen.core.domain.generator.model.GenerationTypeEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ooxml.XWPFCommentsDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.*;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlAnySimpleType;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.impl.CTPImpl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.impl.CTSdtBlockImpl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.impl.CTTblImpl;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URI;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.atexo.docgen.core.utilitaire.word.SdtUtils.setCTSdtPr;
import static com.atexo.docgen.core.utilitaire.word.SdtUtils.setSdtStyle;

@Slf4j
public final class WordDocumentUtils {
    private WordDocumentUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    private static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        //   OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        OBJECT_MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        OBJECT_MAPPER.registerModule(new JavaTimeModule());
    }

    public void copyTable(XWPFTable target, XWPFTable source) {
        cloneTable(target, source.getCTTbl());
    }

    public static void cloneTable(XWPFTable dest, CTTbl source) {
        if (dest == null || source == null) {
            log.info("Table est null : dest {}, source {}", dest, source);
            return;
        }
        dest.getCTTbl().setTblPr(source.getTblPr());
        dest.getCTTbl().setTblGrid(source.getTblGrid());
        for (CTRow row : source.getTrList()) {
            XWPFTableRow targetRow = dest.createRow();
            cloneRow(row, targetRow);
        }
        dest.removeRow(0);

    }

    public static void cloneRow(CTRow srcRow, XWPFTableRow targetRow) {
        targetRow.setCantSplitRow(true);
        targetRow.getCtRow().setTrPr(srcRow.getTrPr());
        for (int c = 0; c < srcRow.getTcList().size(); c++) {
            XWPFTableCell targetCell = c < targetRow.getTableCells().size() ? targetRow.getTableCells().get(c) : targetRow.createCell();
            CTTc cell = srcRow.getTcList().get(c);
            targetCell.getCTTc().setTcPr(cell.getTcPr());
            for (int p = 0; p < cell.getPList().size(); p++) {
                CTP elem = cell.getPList().get(p);
                XWPFParagraph targetPar = targetCell.addParagraph();
                if (targetPar == null) {
                    continue;
                }
                cloneParagraph(targetPar, elem, null);

                targetPar.getCTP().addNewPPr().addNewKeepLines().setVal(STOnOff.ON);
                targetPar.getCTP().getPPr().addNewKeepNext().setVal(STOnOff.ON);

            }
            targetCell.removeParagraph(0);
        }
    }


    public static void cloneParagraph(XWPFParagraph dest, XWPFParagraph source) {
        cloneParagraph(dest, source.getCTP(), "FFFFFF");

    }

    public static void cloneParagraph(XWPFParagraph dest, CTP source) {
        cloneParagraph(dest, source, "FFFFFF");
    }

    public static void cloneParagraph(CTP dest, CTP source) {
        cloneParagraph(dest, source, "FFFFFF");
    }

    public static void cloneParagraph(XWPFParagraph dest, CTP source, String background) {
        if (dest == null || source == null) {
            return;
        }
        CTPPr pPr = dest.getCTP().isSetPPr() ? dest.getCTP().getPPr() : dest.getCTP().addNewPPr();
        pPr.set(source.getPPr());
        Map<XmlCursor, String> xmlCursorHashMap = new HashMap<>();

        if (!CollectionUtils.isEmpty(source.getHyperlinkList())) {
            for (CTHyperlink hyperlink : source.getHyperlinkList()) {
                XmlCursor hyperlinkCursor = hyperlink.newCursor();
                xmlCursorHashMap.put(hyperlinkCursor, "hyperlink");
            }
        }
        List<CTR> textList = source.getRList();
        for (CTR ctr : textList) {
            XmlCursor textCursor = ctr.newCursor();
            xmlCursorHashMap.put(textCursor, "text");
        }
        List<Map.Entry<XmlCursor, String>> entryList = new ArrayList<>(xmlCursorHashMap.entrySet());
        sortCursorList(entryList);

        for (Map.Entry<XmlCursor, String> entry : entryList) {
            if (entry.getValue().equals("hyperlink")) {
                PackagePart targetPackagePart = dest.getDocument().getPackagePart();
                for (CTHyperlink hyperlink : source.getHyperlinkList()) {
                    if (entry.getKey().isAtSamePositionAs(hyperlink.newCursor())) {
                        PackageRelationship newExternalRelationShip = targetPackagePart.addRelationship(URI.create(hyperlink.getRArray(0).getTArray(0).getStringValue()), TargetMode.EXTERNAL, PackageRelationshipTypes.HYPERLINK_PART);
                        CTHyperlink newCTHyperLink = dest.getCTP().addNewHyperlink();
                        newCTHyperLink.setId(newExternalRelationShip.getId());
                        newCTHyperLink.addNewR();

                        XWPFHyperlinkRun hyperlinkRun = new XWPFHyperlinkRun(newCTHyperLink, newCTHyperLink.getRArray(0), dest);

                        cloneHyperLink(hyperlinkRun, hyperlink, background);
                        break;
                    }
                }
            } else if (entry.getValue().equals("text")) {
                for (CTR r : source.getRList()) {
                    if (entry.getKey().isAtSamePositionAs(r.newCursor())) {
                        XWPFRun nr = dest.createRun();
                        cloneRun(nr, r, background);
                        break;
                    }
                }
            } else {
                for (CTR r : source.getRList()) {
                    XWPFRun nr = dest.createRun();
                    cloneRun(nr, r, background);
                }
            }
        }
    }

    private static void sortCursorList(List<Map.Entry<XmlCursor, String>> cursorList) {
        for (int i = 0; i < cursorList.size() - 1; i++) {
            for (int j = 0; j < cursorList.size() - i - 1; j++) {
                if (cursorList.get(j).getKey().comparePosition(cursorList.get(j + 1).getKey()) == 1) {
                    Map.Entry<XmlCursor, String> temp = cursorList.get(j);
                    cursorList.set(j, cursorList.get(j + 1));
                    cursorList.set(j + 1, temp);
                }
            }
        }
    }

    public static void cloneParagraph(CTP dest, CTP source, String background) {
        if (dest == null || source == null) {
            return;
        }
        CTPPr pPr = dest.isSetPPr() ? dest.getPPr() : dest.addNewPPr();
        pPr.set(source.getPPr());
        for (CTR r : source.getRList()) {
            CTR nr = dest.addNewR();
            cloneRun(nr, r, background);
        }
    }

    public static void cloneRun(XWPFRun clone, CTR source, String background) {
        CTRPr rPr = clone.getCTR().isSetRPr() ? clone.getCTR().getRPr() : clone.getCTR().addNewRPr();
        rPr.set(source.getRPr());
        if (!CollectionUtils.isEmpty(source.getBrList())) {
            source.getBrList().forEach(br -> {
                if (br.isSetType()) {
                    BreakType type = BreakType.valueOf(br.getType().intValue());
                    clone.addBreak(type);
                } else {
                    clone.addBreak();
                }

            });
        }
        clone.setText(source.getTList().stream().map(XmlAnySimpleType::getStringValue).collect(Collectors.joining()));

        if (background != null) {
            CTShd cTShd = clone.getCTR().addNewRPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setColor("auto");
            cTShd.setFill(background);
        }
    }

    public static void cloneHyperLink(XWPFHyperlinkRun hyperlinkRun, CTHyperlink hyperlink, String background) {
        String anchorText = hyperlink.getRArray(0).getTArray(0).getStringValue();
        hyperlinkRun.setText(anchorText);
        hyperlinkRun.setColor("0000FF");
        hyperlinkRun.setUnderline(UnderlinePatterns.SINGLE);
        if (background != null) {
            CTShd cTShd = hyperlinkRun.getCTR().addNewRPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setColor("auto");
            cTShd.setFill(background);
        }
    }


    public static void cloneRun(CTR clone, CTR source, String background) {
        CTRPr rPr = clone.isSetRPr() ? clone.getRPr() : clone.addNewRPr();
        rPr.set(source.getRPr());
        source.getTList().forEach(ctText -> {
            CTText t = clone.addNewT();
            t.setSpace(ctText.getSpace());
            t.xsetSpace(ctText.xgetSpace());
        });
        if (background != null) {
            CTShd cTShd = clone.addNewRPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setColor("auto");
            cTShd.setFill(background);
        }
    }

    public static void setTableColumnsWidth(XWPFTable table, Double effectivePageWidth, List<Double> widthsInPercent) {
        CTTblGrid grid = table.getCTTbl().addNewTblGrid();
        for (Double widthInPercent : widthsInPercent) {
            int size = (int) (effectivePageWidth * widthInPercent);
            grid.addNewGridCol().setW(BigInteger.valueOf(size));
        }
    }

    public static double getPageWidth(XWPFDocument document) {
        CTBody body1 = document.getDocument().getBody();
        if (!body1.isSetSectPr()) {
            body1.addNewSectPr();
        }
        CTSectPr sectPr = body1.getSectPr();
        CTPageSz pageSize = sectPr.getPgSz();
        if (!sectPr.isSetPgSz()) {
            pageSize = sectPr.addNewPgSz();
            pageSize.setH(BigInteger.valueOf(16840));
            pageSize.setW(BigInteger.valueOf(11900));
        }

        double pageWidth = pageSize.getW().doubleValue();
        CTPageMar pageMargin = sectPr.getPgMar();
        if (!sectPr.isSetPgMar()) {
            pageMargin = sectPr.addNewPgMar();
            pageMargin.setLeft(BigInteger.valueOf(720L));
            pageMargin.setTop(BigInteger.valueOf(1440L));
            pageMargin.setRight(BigInteger.valueOf(720L));
            pageMargin.setBottom(BigInteger.valueOf(1440L));
        }
        pageSize.setOrient(STPageOrientation.PORTRAIT);

        double pageMarginLeft = pageMargin.getLeft().doubleValue();
        double pageMarginRight = pageMargin.getRight().doubleValue();
        return pageWidth - pageMarginLeft - pageMarginRight;
    }

    // Copy Styles of Table and Paragraph.
    public static void copyStyle(XWPFDocument srcDoc, XWPFDocument destDoc, XWPFStyle style) {
        if (destDoc == null || style == null) return;

        if (destDoc.getStyles() == null) {
            destDoc.createStyles();
        }

        List<XWPFStyle> usedStyleList = srcDoc.getStyles().getUsedStyleList(style);
        for (XWPFStyle xwpfStyle : usedStyleList) {
            destDoc.getStyles().addStyle(xwpfStyle);
        }
    }

    public static void copyLayout(XWPFDocument srcDoc, XWPFDocument destDoc) {
        CTPageMar pgMar = srcDoc.getDocument().getBody().getSectPr().getPgMar();

        BigInteger bottom = pgMar.getBottom();
        BigInteger footer = pgMar.getFooter();
        BigInteger gutter = pgMar.getGutter();
        BigInteger header = pgMar.getHeader();
        BigInteger left = pgMar.getLeft();
        BigInteger right = pgMar.getRight();
        BigInteger top = pgMar.getTop();

        CTPageMar addNewPgMar = destDoc.getDocument().getBody().addNewSectPr().addNewPgMar();

        addNewPgMar.setBottom(bottom);
        addNewPgMar.setFooter(footer);
        addNewPgMar.setGutter(gutter);
        addNewPgMar.setHeader(header);
        addNewPgMar.setLeft(left);
        addNewPgMar.setRight(right);
        addNewPgMar.setTop(top);

        CTPageSz pgSzSrc = srcDoc.getDocument().getBody().getSectPr().getPgSz();

        BigInteger code = pgSzSrc.getCode();
        BigInteger h = pgSzSrc.getH();
        STPageOrientation.Enum orient = pgSzSrc.getOrient();
        BigInteger w = pgSzSrc.getW();

        CTPageSz addNewPgSz = destDoc.getDocument().getBody().addNewSectPr().addNewPgSz();

        addNewPgSz.setCode(code);
        addNewPgSz.setH(h);
        addNewPgSz.setOrient(orient);
        addNewPgSz.setW(w);
    }


    public static CTComments getDocumentComments(XWPFDocument document) {

        OPCPackage oPCPackage = document.getPackage();
        PackagePartName partName;
        try {

            partName = PackagingURIHelper.createPartName("/word/comments.xml");

            PackagePart part = oPCPackage.getPart(partName);
            if (part == null) {
                part = oPCPackage.createPart(partName, "application/vnd.openxmlformats-officedocument.wordprocessingml.comments+xml");

            }
            XWPFCommentsDocument myXWPFCommentsDocument = new XWPFCommentsDocument(part);
            if (CollectionUtils.isEmpty(myXWPFCommentsDocument.getComments().getCommentList())) {
                String rId = "rId" + (document.getRelationParts().size() + 1);
                document.addRelation(rId, XWPFRelation.COMMENT, myXWPFCommentsDocument);
            }
            return myXWPFCommentsDocument.getComments();
        } catch (OpenXML4JException e) {
            log.error("Erreur lors de la création des commentaires : {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.GENERATION, e.getMessage());
        }
    }


    public static void addComment(String comment, CTComments comments, BigInteger cId) {

        CTComment ctComment = comments.addNewComment();
        ctComment.setAuthor("Info Bulle");
        ctComment.addNewP().addNewR().addNewT().setStringValue(comment);
        ctComment.setId(cId);
    }


    public static WordStyle generateWordStyle(List<TagFormat> format, XWPFRun run) {
        final WordStyle style = new WordStyle();
        BiConsumer<WordStyle, TagFormat> cstyle = (s, f) -> {
            String color = f.getAttribute("color");
            if (color != null) {
                s.setColor(convertColor(color, HSSFColor.HSSFColorPredefined.BLACK.getColor()));
            }
            String bgcolor = f.getAttribute("background-color");
            if (org.apache.commons.lang.StringUtils.isNotBlank(bgcolor) && (run.getCTR().getRPr() == null || run.getCTR().getRPr().getShd() == null)) {
                CTShd cTShd = CTShd.Factory.newInstance();
                cTShd.setVal(STShd.CLEAR);
                cTShd.setColor("auto");
                cTShd.setFill(convertColor(bgcolor, HSSFColor.HSSFColorPredefined.WHITE.getColor()));
                run.getCTR().addNewRPr().setShd(cTShd);
            }
            String textAlign = f.getAttribute("text-align");
            if (textAlign != null) {
                style.setTextAlign(textAlign);
            }

        };
        BiConsumer<WordStyle, TagFormat> cstrong = (s, f) -> {
            s.setBold(true);
            if (f.getAttributes() != null) {
                cstyle.accept(s, f);
            }
        };
        BiConsumer<WordStyle, TagFormat> clink = (s, f) -> {
            s.setUnderline(UnderlinePatterns.SINGLE);
            s.setColor("0000FF");
            if (f.getAttributes() != null) {
                cstyle.accept(s, f);
            }
        };
        BiConsumer<WordStyle, TagFormat> cem = (s, f) -> {
            s.setItalic(true);
            if (f.getAttributes() != null) {
                cstyle.accept(s, f);
            }
        };
        BiConsumer<WordStyle, TagFormat> cunderline = (s, f) -> {
            s.setUnderline(UnderlinePatterns.SINGLE);
            if (f.getAttributes() != null) {
                cstyle.accept(s, f);
            }
        };

        for (int j = format.size() - 1; j >= 0; j--) {
            TagFormat f = format.get(j);
            switch (f.getFormat()) {
                case "strong":
                    cstrong.accept(style, f);
                    break;
                case "em":
                    cem.accept(style, f);
                    break;
                case "underline":
                    cunderline.accept(style, f);
                    break;
                case "a":
                    clink.accept(style, f);
                    break;
                case "style":
                    cstyle.accept(style, f);
                    break;
                default:
                    log.warn("Clause n'est pas prise en compte pour la validation {}", f.getFormat());

            }
        }
        return style;
    }

    public static void applyStyleToRun(WordStyle style, XWPFRun run) {
        if (style == null) return;
        run.setBold(style.isBold());
        run.setItalic(style.isItalic());
        if (style.getFontSize() != null) {
            run.setFontSize(style.getFontSize());
        } else {
            run.setFontSize(10);
        }
        if (style.getTextPosition() != null) {
            run.setTextPosition(style.getTextPosition());
        }
        if (style.getFontFamily() != null) {
            run.setFontFamily(style.getFontFamily().getValue());
        } else {
            run.setFontFamily("Arial");
        }
        if (style.getUnderline() != null) {
            run.setUnderline(style.getUnderline());
        } else {
            run.setUnderline(UnderlinePatterns.NONE);
        }
        if (org.apache.commons.lang.StringUtils.isNotBlank(style.getColor())) {
            run.setColor(style.getColor());
        }

    }

    public static String convertColor(String attribute, HSSFColor defaut) {
        HSSFColor color = defaut;
        try {
            if (org.apache.commons.lang.StringUtils.isNotBlank(attribute)) {
                if (attribute.startsWith("#")) {
                    return attribute.substring(1);
                } else if (attribute.startsWith("rgb")) {
                    return rgbToHex(parseRGBString(attribute));
                } else {

                    color = HSSFColor.HSSFColorPredefined.valueOf(attribute.toUpperCase()).getColor();

                }
            }
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de la couleur {}", attribute);
        }
        short[] colors = color.getTriplet();
        StringBuilder sb = new StringBuilder();
        for (short bit : colors) {
            sb.append(StringUtils.leftPad(Integer.toHexString(bit).toUpperCase(), 2, '0'));
        }
        return sb.toString();
    }

    public static int[] parseRGBString(String rgbString) {
        int[] rgbValues = new int[3];

        Pattern pattern = Pattern.compile("rgb\\((\\d+),\\s*(\\d+),\\s*(\\d+)\\)");
        Matcher matcher = pattern.matcher(rgbString);

        if (matcher.matches()) {
            rgbValues[0] = Integer.parseInt(matcher.group(1)); // Red value
            rgbValues[1] = Integer.parseInt(matcher.group(2)); // Green value
            rgbValues[2] = Integer.parseInt(matcher.group(3)); // Blue value
        }

        return rgbValues;
    }

    public static String rgbToHex(int[] colors) {
        String hexRed = Integer.toHexString(colors[0]);
        String hexGreen = Integer.toHexString(colors[1]);
        String hexBlue = Integer.toHexString(colors[2]);

        hexRed = hexRed.length() == 1 ? "0" + hexRed : hexRed;
        hexGreen = hexGreen.length() == 1 ? "0" + hexGreen : hexGreen;
        hexBlue = hexBlue.length() == 1 ? "0" + hexBlue : hexBlue;

        String hexColor = hexRed + hexGreen + hexBlue;

        return hexColor.toUpperCase();
    }

    public static XWPFParagraph writeFormatedText(XWPFDocument document, XWPFParagraph paragraph, WordStyle defaultStyle, FormatedText ft, String background, BigInteger numID, int level, FormatedText previousFt) {
        String text = ft.getText();

        if (ft.isBullet() && ft.getIdLi() != null && (previousFt == null || !ft.getIdLi().equals(previousFt.getIdLi()))) {
            if (previousFt != null && !CollectionUtils.isEmpty(previousFt.getFormat()) && previousFt.getFormat().get(0).getFormat().equals("p")) {
                paragraph = document.getLastParagraph();
            } else {
                paragraph = document.createParagraph();
            }
            paragraph.setNumID(numID);

            TagFormat tf = ft.getFormat().get(0);
            int marginLeft = extractNumber(tf.getAttribute("margin-left"));
            if (marginLeft != 0 && previousFt != null && !CollectionUtils.isEmpty(previousFt.getFormat())) {
                TagFormat previousTf = previousFt.getFormat().get(0);
                int previousMarginLeft = extractNumber(previousTf.getAttribute("margin-left"));
                if (marginLeft > previousMarginLeft) {
                    tf.getAttributes().put("numILvl", String.valueOf(marginLeft / 2));
                } else if (marginLeft < previousMarginLeft && !previousTf.getAttribute("numILvl").equals("0")) {
                    tf.getAttributes().put("numILvl", String.valueOf(marginLeft / 2));
                } else {
                    tf.getAttributes().put("numILvl", String.valueOf(Integer.parseInt(previousTf.getAttribute("numILvl"))));
                }
            }
            if (tf.getAttribute("numILvl") != null) {
                paragraph.getCTP().getPPr().getNumPr().addNewIlvl();
                paragraph.getCTP().getPPr().getNumPr().getIlvl().setVal(BigInteger.valueOf(Integer.parseInt(tf.getAttribute("numILvl"))));
            }
        }

        if (text != null) {
            XWPFRun run = paragraph.createRun();
            if (ft.getFormat() != null && !ft.getFormat().isEmpty()) {
                List<TagFormat> tagFormatList = ft.getFormat().stream().filter(tagFormat -> tagFormat.getName().equals("a") && tagFormat.getAttributes() != null).toList();
                if (!tagFormatList.isEmpty()) {
                    TagFormat aTagFormat = tagFormatList.get(0);
                    if (aTagFormat != null) {
                        String url = aTagFormat.getAttributes().get("href=");
                        run = createHyperlinkRun(paragraph, url);
                        run.setText(text);
                    }
                } else {
                    run.setText(text);
                }
            } else {
                run.setText(text);
            }

            if (background != null) {
                CTShd cTShd = run.getCTR().addNewRPr().addNewShd();
                cTShd.setVal(STShd.CLEAR);
                cTShd.setColor("auto");
                cTShd.setFill(background);
            }

            if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(ft.getFormat())) {
                WordStyle style = generateWordStyle(ft.getFormat(), run);
                applyStyleToRun(style, run);
                if (style.getTextAlign() != null) {
                    if (style.getTextAlign().equals("justify")) {
                        style.setTextAlign("both");
                    }
                    paragraph.setAlignment(ParagraphAlignment.valueOf(style.getTextAlign().toUpperCase()));
                }
            } else if (defaultStyle != null) {
                applyStyleToRun(defaultStyle, run);
            }

        }
        return paragraph;
    }

    public static int extractNumber(String input) {
        if (input != null) {
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(input);

            if (matcher.find()) {
                String numberStr = matcher.group();
                return Integer.parseInt(numberStr);
            }

        }
        return 0;
    }

    static public XWPFHyperlinkRun createHyperlinkRun(XWPFParagraph paragraph, String uri) {
        String rId = paragraph.getPart().getPackagePart().addExternalRelationship(uri, XWPFRelation.HYPERLINK.getRelation()).getId();

        CTHyperlink cthyperLink = paragraph.getCTP().addNewHyperlink();
        cthyperLink.addNewR();
        cthyperLink.setId(rId);

        return new XWPFHyperlinkRun(cthyperLink, cthyperLink.getRArray(0), paragraph);
    }

    public static void mergeCellVertically(XWPFTable table, int col, int fromRow, int toRow) {
        if (fromRow < toRow) for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            if (rowIndex == fromRow) {
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    public static void mergeCellHorizontally(XWPFTable table, int row, int fromCol, int toCol) {
        if (fromCol < toCol) for (int cellIndex = fromCol; cellIndex <= toCol; cellIndex++) {
            XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
            if (cellIndex == fromCol) {
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.RESTART);
            } else {
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    public static void addHeaderImage(XWPFDocument doc, InputStream imgFile, String name) throws IOException, InvalidFormatException {
        // create header start
        CTBody body = doc.getDocument().getBody();
        CTSectPr sectPr;
        if (body.isSetSectPr()) {
            sectPr = doc.getDocument().getBody().getSectPr();
        } else {
            sectPr = doc.getDocument().getBody().addNewSectPr();
        }
        sectPr.addNewTitlePg();

        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(doc, sectPr);

        XWPFHeader header = headerFooterPolicy.getHeader(XWPFHeaderFooterPolicy.FIRST);
        if (header == null) {
            header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.FIRST);
        }
        XWPFParagraph paragraph = header.getParagraphArray(0);
        if (paragraph == null) {
            paragraph = header.createParagraph();
        }
        paragraph.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun run = paragraph.createRun();
        run.addPicture(imgFile, XWPFDocument.PICTURE_TYPE_PNG, name, Units.toEMU(1.5 * 1.9 * 28.6), Units.toEMU(1.5 * 1.9 * 16.9));
        paragraph = header.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        run = paragraph.createRun();
        run.setText("[Header]");

    }

    public static void addDraftDocumentsToRootDocument(XWPFDocument rootDocument, Map<Integer, List<XWPFDocument>> positionCursorMap) {

        positionCursorMap.keySet().stream().sorted((o1, o2) -> o2 - o1).forEach(position -> {
                    try {
                        new ArrayDeque<>(positionCursorMap.get(position)).descendingIterator().forEachRemaining(draftDocument -> {
                            XmlCursor draftXmlcursor = draftDocument.getDocument().getBody().newCursor();
                            List<XmlObject> xmlObjects = new ArrayList<>();
                            int niveau = 0;
                            while (draftXmlcursor.hasNextToken()) {
                                XmlCursor.TokenType tokentype = draftXmlcursor.toNextToken();
                                if (tokentype.isStart()) {
                                    niveau++;
                                    if (niveau == 1) {
                                        XmlObject object = draftXmlcursor.getObject();
                                        log.debug("object : {}", object.getClass().getName());
                                        if (object instanceof CTSdtBlockImpl ctSdtBlock) {
                                            if (ctSdtBlock.isSetSdtPr() && !CollectionUtils.isEmpty(ctSdtBlock.getSdtPr().getTagList()) && StringUtils.isNotBlank(ctSdtBlock.getSdtPr().getTagList().get(0).getVal())) {
                                                GenerationTypeEnum tag = GenerationTypeEnum.fromValue(ctSdtBlock.getSdtPr().getTagList().get(0).getVal());
                                                if (tag != null) xmlObjects.add(object);

                                            }
                                        } else if (object instanceof CTPImpl ctp && StringUtils.isNotBlank(ctp.getStringValue())) {
                                            xmlObjects.add(object);
                                        } else if (object instanceof CTTblImpl ctTbl && (StringUtils.isNotBlank(ctTbl.getStringValue()))) {
                                            xmlObjects.add(object);
                                        }
                                    }
                                } else if (tokentype.isEnd()) {
                                    niveau--;
                                }
                            }
                            Collections.reverse(xmlObjects);
                            for (XmlObject xmlObject : xmlObjects) {
                                if (xmlObject instanceof CTPImpl ctP) {
                                    if (position == -1 || position > rootDocument.getParagraphs().size() - 1) {
                                        CTP ctp = rootDocument.getDocument().getBody().addNewP();
                                        ctp.set(ctP);
                                    } else {
                                        XWPFParagraph xwpfParagraph = rootDocument.getParagraphArray(position);
                                        CTP ctp = rootDocument.insertNewParagraph(xwpfParagraph.getCTP().newCursor()).getCTP();
                                        ctp.set(ctP);
                                    }

                                } else if (xmlObject instanceof CTSdtBlockImpl ctSdtBlock) {
                                    CTSdtBlock block = rootDocument.getDocument().getBody().insertNewSdt(position);
                                    block.set(ctSdtBlock);
                                }
                            }

                            try {
                                draftDocument.close();
                            } catch (IOException e) {
                                log.error("Erreur lors de la fermerture du Draft document {}", e.getMessage());
                            }
                        });

                    } catch (Exception e) {
                        log.error("Erreur lors de l'insertion du draft document : {}", e.getMessage());
                    }

                });
    }

    public static void addSdt(XWPFDocument rootDocument, Map<Integer, List<XWPFDocument>> positionCursorMap) {

        positionCursorMap.keySet().stream().sorted((o1, o2) -> o2 - o1).forEach(position -> {
            try {
                new ArrayDeque<>(positionCursorMap.get(position)).descendingIterator().forEachRemaining(draftDocument -> {
                    if (draftDocument == null || (CollectionUtils.isEmpty(draftDocument.getParagraphs()) && CollectionUtils.isEmpty(draftDocument.getTables()))) {
                        return;
                    }
                    CTBody addBody = draftDocument.getDocument().getBody();
                    if (addBody == null || rootDocument == null) {
                        return;
                    }

                    PackagePart sourcePackagePart = draftDocument.getPackagePart();
                    PackagePart targetPackagePart = rootDocument.getPackagePart();

                    try {
                        for (PackageRelationship rel : sourcePackagePart.getRelationships()) {
                            if (rel.getTargetMode().equals(TargetMode.EXTERNAL)) {
                                PackageRelationship newExternalRelationShip = targetPackagePart.addRelationship(rel.getTargetURI(), TargetMode.EXTERNAL, rel.getRelationshipType());
                                for (XWPFParagraph paragraph : draftDocument.getParagraphs()) {
                                    for (CTHyperlink hyperlink : paragraph.getCTP().getHyperlinkList()) {
                                        if (hyperlink.getId().equals(rel.getId())) {
                                            hyperlink.setId(newExternalRelationShip.getId());
                                        }
                                    }
                                }
                            }
                        }
                    } catch (InvalidFormatException e) {
                        log.error("Erreur lors de la copie des relations externes", e);
                    }


                    CTSdtBlock block = rootDocument.getDocument().getBody().insertNewSdt(position);
                    CTSdtPr sdtPr = block.addNewSdtPr();
                    sdtPr.addNewDocPartObj().addNewDocPartGallery().setVal(position + GenerationTypeEnum.BLOC_SIMPLE.getValue());
                    CTSdtEndPr sdtEndPr = block.addNewSdtEndPr();
                    setSdtStyle(sdtEndPr);
                    CTSdtContentBlock content = block.addNewSdtContent();
                    content.set(addBody.changeType(CTSdtContentBlock.Factory.newInstance().schemaType()));
                    setCTSdtPr(sdtPr, String.valueOf(position), "", GenerationTypeEnum.BLOC_SIMPLE.getValue(), false, false);


                    try {
                        draftDocument.close();
                    } catch (IOException e) {
                        log.error("Erreur lors de la fermerture du Draft document {}", e.getMessage());
                    }
                });

            } catch (Exception e) {
                log.error("Erreur lors de l'insertion du draft document : {}", e.getMessage());
            }

        });
    }

    public static void deleteSdtBlocks(XWPFDocument rootDocument, Predicate<CTSdtBlock> predicate, Predicate<CTSdtRun> predicateRun) {

        List<CTSdtBlock> sdtList = rootDocument.getDocument().getBody().getSdtList();
        int size = sdtList.size();
        for (int i = size - 1; i >= 0; i--) {
            CTSdtBlock ctSdtBlock = sdtList.get(i);
            if (predicate.test(ctSdtBlock)) {
                rootDocument.getDocument().getBody().removeSdt(i);
            }
        }
        List<XWPFParagraph> paragraphs = rootDocument.getParagraphs();
        removeFromParagraph(predicateRun, paragraphs);
        rootDocument.getHeaderList().forEach(xwpfHeader -> {
            xwpfHeader.getParagraphs().forEach(xwpfParagraph -> removeFromParagraph(predicateRun, Collections.singletonList(xwpfParagraph)));
            xwpfHeader.getTables().forEach(xwpfTable -> removeFromTable(predicateRun, Collections.singletonList(xwpfTable)));
        });
        rootDocument.getFooterList().forEach(xwpfFooter -> {
            xwpfFooter.getParagraphs().forEach(xwpfParagraph -> removeFromParagraph(predicateRun, Collections.singletonList(xwpfParagraph)));
            xwpfFooter.getTables().forEach(xwpfTable -> removeFromTable(predicateRun, Collections.singletonList(xwpfTable)));
        });
        List<XWPFTable> tables = rootDocument.getTables();
        removeFromTable(predicateRun, tables);
    }

    private static void removeFromTable(Predicate<CTSdtRun> predicateRun, List<XWPFTable> tables) {
        if (CollectionUtils.isEmpty(tables)) {
            return;
        }
        tables.forEach(xwpfTable -> xwpfTable.getRows().forEach(xwpfTableRow -> xwpfTableRow.getTableCells().forEach(xwpfTableCell -> {
            removeFromTable(predicateRun, xwpfTableCell.getTables());
            removeFromParagraph(predicateRun, xwpfTableCell.getParagraphs());
        })));
    }

    private static void removeFromParagraph(Predicate<CTSdtRun> predicateRun, List<XWPFParagraph> paragraphs) {
        if (CollectionUtils.isEmpty(paragraphs)) {
            return;
        }
        paragraphs.stream().filter(xwpfParagraph -> !CollectionUtils.isEmpty(xwpfParagraph.getCTP().getSdtList())).forEach(xwpfParagraph -> {
            List<CTSdtRun> sdtParagraphList = xwpfParagraph.getCTP().getSdtList();
            int size = sdtParagraphList.size();
            for (int i = size - 1; i >= 0; i--) {
                CTSdtRun ctSdtBlock = sdtParagraphList.get(i);
                if (predicateRun.test(ctSdtBlock)) {
                    xwpfParagraph.getCTP().removeSdt(i);
                }
            }
        });
    }

    public static void cloneDocumentBody(XWPFDocument drafDocument, XWPFDocument xwpfDocument) {
        drafDocument.getBodyElements().forEach(iBodyElement -> {
            if (iBodyElement instanceof XWPFParagraph xwpfParagraph) {
                XWPFParagraph dest = xwpfDocument.createParagraph();
                cloneParagraph(dest, xwpfParagraph.getCTP());
            } else if (iBodyElement instanceof XWPFTable xwpfTable) {
                XWPFTable newTable = xwpfDocument.createTable();
                cloneTable(newTable, xwpfTable.getCTTbl());
            }
        });
        try {
            drafDocument.close();
        } catch (IOException e) {
            log.error("Erreur lors de la fermerture du Draft document");
        }
    }

    public static String sanitizeHtmlInput(String untrustedHTML) {
        if (StringUtils.isBlank(untrustedHTML)) return untrustedHTML;
        PolicyFactory policy = new HtmlPolicyBuilder().allowElements("a", "br", "div", "img", "ul", "li", "p", "span", "u", "em", "strong", "b", "i")
                .allowAttributes("href", "style").onElements("a")
                .allowAttributes("style").onElements("div")
                .allowAttributes("width", "src", "height").onElements("img")
                .allowAttributes("style").onElements("li")
                .allowAttributes("style").onElements("p")
                .allowAttributes("style").onElements("span")
                .allowAttributes("style").onElements("u")
                .allowAttributes("style").onElements("em")
                .allowAttributes("style").onElements("strong")
                .allowAttributes("style").onElements("b")
                .allowAttributes("style").onElements("i")
                .allowUrlProtocols("https", "http")
                .toFactory();
        return policy.sanitize(untrustedHTML);
    }


    public static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }


}
