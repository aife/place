package com.atexo.docgen.core.domain.generator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum GenerationTypeEnum {
    BLOC_TABLE("BLOC_TABLE"),
    BLOC_TEMPLATE("BLOC_TEMPLATE"),
    BLOC_TEMPLATE_DYNAMIQUE("BLOC_TEMPLATE_DYNAMIQUE"),
    BLOC_CONDITIONNEL("BLOC_TEMPLATE_CONDITIONNEL"),
    BLOC_EXPLICATION("BLOC_EXPLICATION"),
    BLOC_SIMPLE("SIMPLE");
    private final String value;

    GenerationTypeEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static GenerationTypeEnum fromValue(String value) {
        for (GenerationTypeEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
