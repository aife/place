package com.atexo.docgen.core.domain.convertor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;


@Builder
@Getter
@Setter
public class TagFormat {
    private String name;
    private String format;
    private Map<String, String> attributes;

    public void addAttribute(String key, String val) {
        if (attributes == null) {
            attributes = new HashMap<>();
        }
        attributes.put(key, val);
    }

    public String getAttribute(String key) {
        return (attributes == null) ? null : attributes.get(key);
    }

}
