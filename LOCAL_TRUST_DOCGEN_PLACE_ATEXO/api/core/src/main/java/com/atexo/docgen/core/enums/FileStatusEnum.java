package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum FileStatusEnum {
    REQUEST_TO_OPEN(0), OPENED(1), SAVED_AND_CLOSED(2), CORRUPTED(3),
    CLOSED_WITHOUT_EDITING(4), EDITED_AND_SAVED(6), ERROR_WHILE_SAVING(7), NOT_FOUND(8),
    DOWNLOADED(9);
    private final int code;

    FileStatusEnum(int code) {
        this.code = code;
    }

    @JsonCreator
    public static FileStatusEnum fromValue(int value) {
        for (FileStatusEnum operator : values()) {
            if (operator.getCode() == value) {
                return operator;
            }
        }
        log.error("Cannot find FileStatusEnum from value: {}", value);
        throw new IllegalArgumentException("Cannot find FileStatusEnum from value: " + value);
    }

    @JsonValue
    public int getCode() {
        return code;
    }
}
