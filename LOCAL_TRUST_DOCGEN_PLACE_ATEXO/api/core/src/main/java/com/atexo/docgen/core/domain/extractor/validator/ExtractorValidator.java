package com.atexo.docgen.core.domain.extractor.validator;

import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Component
public class ExtractorValidator {

    String format = "xlsx";

    public void validate(@NonNull List<MultipartFile> files) {
        log.info("Lancement du process de validation de fichiers");
        files.forEach(file -> {
            if (file == null) {
                log.error("Le fichier ne doit pas être null");
                throw new MauvaisParametreException(TypeExceptionEnum.GENERATION, "Le fichier ne doit pas être null");
            }
            String curExt = DocumentUtils.getFileExtension(file.getOriginalFilename());
            if (!format.equalsIgnoreCase(curExt)) {
                log.error("Le format de fichier est incorrect");
                throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.GENERATION);
            }
        });

        log.info("Validé !");
    }
}
