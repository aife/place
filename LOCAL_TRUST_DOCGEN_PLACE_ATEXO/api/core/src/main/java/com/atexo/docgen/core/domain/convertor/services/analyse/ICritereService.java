package com.atexo.docgen.core.domain.convertor.services.analyse;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.convertor.model.analyse.CritereConfig;
import com.atexo.docgen.core.domain.convertor.model.analyse.Soumissionnaire;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

public interface ICritereService {
    int setCriteriaRows(XSSFWorkbook workbook, List<CritereConfig> criteres, XSSFSheet sheet, int size, Configuration configuration);

    void setCriteriaMergeColumn(List<CritereConfig> criteres, Configuration configuration, XSSFSheet sheet, List<Soumissionnaire> soumissionnaires);
}
