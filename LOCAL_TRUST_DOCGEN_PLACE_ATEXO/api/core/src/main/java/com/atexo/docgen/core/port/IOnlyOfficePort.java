package com.atexo.docgen.core.port;

import java.util.Map;

public interface IOnlyOfficePort {


    String convertFile(String outputFormat, String s, String key, String filetype);

    String getApis();

    Integer forceSave(String key);

    Integer info(String key);

    Map<String, String> getSurchargesLibelles();
}
