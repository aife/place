package com.atexo.docgen.core.domain.generator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
@Setter
public class SheetRequest {
    private List<XlsxHeader> columns;
    private String name;
}
