package com.atexo.docgen.core.exceptions;

public class MalformedHtmlException extends Exception {

    private static final long serialVersionUID = 163612741297617L;

    public MalformedHtmlException() {
        super();
    }

    public MalformedHtmlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MalformedHtmlException(String message, Throwable cause) {
        super(message, cause);
    }

    public MalformedHtmlException(String message) {
        super(message);
    }

    public MalformedHtmlException(Throwable cause) {
        super(cause);
    }

}
