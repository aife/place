package com.atexo.docgen.core.utilitaire.word;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFAbstractNum;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFNumbering;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNumbering;

import java.math.BigInteger;

@Slf4j
public final class NumeringUtils {
    private NumeringUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    private static final String ARTICLE_NUMERING = "<w:abstractNum  xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:abstractNumId=\"7\">" +
            "<w:multiLevelType w:val=\"hybridMultilevel\"/>" +
            "<w:styleLink w:val=\"811\"/>" +
            "<w:lvl w:ilvl=\"0\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"799\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"Article %1 - \"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"0\" w:firstLine =\"283\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"1\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"800\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2 \"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"0\" w:firstLine =\"283\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"2\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"801\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3 \"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"0\" w:firstLine =\"283\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"3\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"802\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3.%4 \"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"0\" w:firstLine =\"283\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"4\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"803\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3.%4.%5 \"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"0\" w:firstLine =\"283\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"5\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"804\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3.%4.%5.%6 \"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"1152\" w:hanging=\"1152\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"6\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"805\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3.%4.%5.%6.%7\"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"1296\" w:hanging=\"1296\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"7\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"806\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3.%4.%5.%6.%7.%8\"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"1440\" w:hanging=\"1440\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "<w:lvl w:ilvl=\"8\">" +
            "<w:start w:val=\"1\"/>" +
            "<w:numFmt w:val=\"decimal\" />" +
            "<w:pStyle w:val=\"807\"/>" +
            "<w:isLgl w:val=\"false\"/>" +
            "<w:suff w:val=\"tab\"/>" +
            "<w:lvlText w:val=\"%1.%2.%3.%4.%5.%6.%7.%8.%9\"/>" +
            "<w:lvlJc w:val=\"left\" />" +
            "<w:pPr>" +
            "<w:ind w:left=\"1584\" w:hanging=\"1584\"/>" +
            "</w:pPr>" +
            "</w:lvl>" +
            "</w:abstractNum>";
    private static final String BULLET_NUMERING =
            "<w:abstractNum xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:abstractNumId=\"0\">"
                    + "<w:multiLevelType w:val=\"hybridMultilevel\"/>"
                    + "<w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Symbol\" w:hAnsi=\"Symbol\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"1\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"o\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Courier New\" w:hAnsi=\"Courier New\" w:cs=\"Courier New\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"2\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Wingdings\" w:hAnsi=\"Wingdings\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "</w:abstractNum>";

    public static BigInteger getBulletStyle(XWPFDocument doc) {
        return setNumeringStyle(doc, BULLET_NUMERING);
    }

    public static BigInteger setNumeringStyle(XWPFDocument doc, String numering) {
        try {


            CTNumbering cTNumbering = CTNumbering.Factory.parse(numering);

            CTAbstractNum cTAbstractNum = cTNumbering.getAbstractNumArray(0);

            XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);
            XWPFNumbering numbering = doc.getNumbering();
            if (numbering == null) {
                numbering = doc.createNumbering();
            }
            abstractNum.setNumbering(numbering);
            BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

            return numbering.addNum(abstractNumID);

        } catch (Exception e) {
            log.error("Erreur lors de l'ajout des numering du document");
        }
        return null;
    }

    public static BigInteger setDefaultDocumentNumering(XWPFDocument xwpfDocument) {
        //setNumeringStyle(xwpfDocument, BULLET_NUMERING);
        return setNumeringStyle(xwpfDocument, ARTICLE_NUMERING);
    }


}
