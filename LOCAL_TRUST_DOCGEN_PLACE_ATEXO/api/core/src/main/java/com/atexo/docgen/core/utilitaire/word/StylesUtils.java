package com.atexo.docgen.core.utilitaire.word;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyle;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyles;

import java.lang.reflect.Field;

@Slf4j
public final class StylesUtils {

    private static final String NORMAL = "<w:style  xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Normal\" w:default=\"1\">" +
            "<w:name w:val=\"Normal\"/>" +
            "<w:qFormat/>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\" w:cs=\"Tahoma\" w:eastAsia=\"Andale Sans UI\"/>" +
            "<w:color w:val=\"auto\"/>" +
            "<w:sz w:val=\"20\"/>" +
            "<w:szCs w:val=\"20\"/>" +
            "<w:lang w:val=\"fr-FR\" w:bidi=\"fa-IR\" w:eastAsia=\"ja-JP\"/>" +
            "</w:rPr>" +
            "<w:pPr>" +
            "<w:jc w:val=\"both\" />" +
            "<w:spacing w:after=\"0\" w:before=\"120\"/>" +
            "<w:widowControl/>" +
            "</w:pPr>" +
            "</w:style>";
    private static final String STANDARD = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            " w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"Standard\">" +
            "<w:name w:val=\"Standard\"/>" +
            "<w:link w:val=\"StandardCar\"/>" +
            "<w:autoRedefine/>" +
            "<w:rsid w:val=\"007A2D7F\"/>" +
            "<w:pPr>" +
            "<w:widowControl/>" +
            "<w:spacing w:before=\"120\"/>" +
            "<w:jc w:val=\"both\"/>" +
            "<w:textAlignment w:val=\"center\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"20\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            " w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"Titre\">" +
            "<w:name w:val=\"Heading\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:autoRedefine/>" +
            "<w:pPr>" +
            "<w:keepNext/>" +
            "<w:pBdr>" +
            "<w:top w:val=\"single\" w:sz=\"18\" w:space=\"1\" w:color=\"666666\"/>" +
            "<w:left w:val=\"single\" w:sz=\"18\" w:space=\"1\" w:color=\"666666\"/>" +
            "<w:bottom w:val=\"single\" w:sz=\"18\" w:space=\"1\" w:color=\"666666\"/>" +
            "<w:right w:val=\"single\" w:sz=\"18\" w:space=\"1\" w:color=\"666666\"/>" +
            "</w:pBdr>" +
            "<w:spacing w:before=\"567\" w:after=\"567\"/>" +
            "<w:jc w:val=\"center\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:b/>" +
            "<w:sz w:val=\"32\"/>" +
            "<w:szCs w:val=\"28\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TITRE_PRINCIPAL = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            " w:type=\"paragraph\" w:styleId=\"Title\">" +
            "<w:name w:val=\"Title\"/>" +
            "<w:basedOn w:val=\"Titre\"/>" +
            "<w:next w:val=\"Corpsdetexte\"/>" +
            "<w:qFormat/>" +
            "<w:pPr>" +
            "<w:jc w:val=\"center\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:b/>" +
            "<w:bCs/>" +
            "<w:sz w:val=\"56\"/>" +
            "<w:szCs w:val=\"56\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_1 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading1\">" +
            "<w:name w:val=\"Heading 1\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:next w:val=\"Standard\"/>" +
            "<w:uiPriority w:val=\"9\"/>" +
            "<w:qFormat/>" +
            "<w:rsid w:val=\"00932262\"/>" +
            "<w:pPr>" +
            "<w:keepNext/>" +
            "<w:numPr>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:spacing w:before=\"360\"/>" +
            "<w:ind w:left=\"1701\" w:hanging=\"1701\"/>" +
            "<w:outlineLvl w:val=\"0\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:b/>" +
            "<w:color w:val=\"666699\"/>" +
            "<w:sz w:val=\"28\"/>" +
            "<w:szCs w:val=\"36\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_2 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading2\">" +
            "<w:name w:val=\"Heading 2\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:next w:val=\"Standard\"/>" +
            "<w:uiPriority w:val=\"9\"/>" +
            "<w:unhideWhenUsed/>" +
            "<w:qFormat/>" +
            "<w:rsid w:val=\"00932262\"/>" +
            "<w:pPr>" +
            "<w:keepNext/>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"1\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:spacing w:before=\"240\"/>" +
            "<w:ind w:left=\"1701\" w:hanging=\"1701\"/>" +
            "<w:outlineLvl w:val=\"1\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:b/>" +
            "<w:bCs/>" +
            "<w:iCs/>" +
            "<w:color w:val=\"666699\"/>" +
            "<w:sz w:val=\"22\"/>" +
            "<w:szCs w:val=\"28\"/>" +
            "<w:u w:val=\"single\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_3 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading3\">" +
            "<w:name w:val=\"Heading 3\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:next w:val=\"Standard\"/>" +
            "<w:uiPriority w:val=\"9\"/>" +
            "<w:unhideWhenUsed/>" +
            "<w:qFormat/>" +
            "<w:rsid w:val=\"00582936\"/>" +
            "<w:pPr>" +
            "<w:keepNext/>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"2\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:spacing w:before=\"240\"/>" +
            "<w:ind w:left=\"1701\" w:hanging=\"1701\"/>" +
            "<w:outlineLvl w:val=\"2\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:color w:val=\"666699\"/>" +
            "<w:szCs w:val=\"26\"/>" +
            "<w:u w:val=\"single\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_4 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading4\">" +
            "<w:name w:val=\"Heading 4\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:uiPriority w:val=\"9\"/>" +
            "<w:semiHidden/>" +
            "<w:unhideWhenUsed/>" +
            "<w:qFormat/>" +
            "<w:rsid w:val=\"007A2D7F\"/>" +
            "<w:pPr>" +
            "<w:keepNext/>" +
            "<w:keepLines/>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"3\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:spacing w:before=\"240\"/>" +
            "<w:ind w:left=\"1701\" w:hanging=\"1701\"/>" +
            "<w:outlineLvl w:val=\"3\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:iCs/>" +
            "<w:color w:val=\"666699\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_5 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading5\">" +
            "<w:name w:val=\"Heading 5\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:uiPriority w:val=\"9\"/>" +
            "<w:semiHidden/>" +
            "<w:unhideWhenUsed/>" +
            "<w:qFormat/>" +
            "<w:pPr>" +
            "<w:keepNext/>" +
            "<w:keepLines/>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"4\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:spacing w:before=\"283\" w:after=\"57\"/>" +
            "<w:outlineLvl w:val=\"4\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:b/>" +
            "<w:bCs/>" +
            "<w:sz w:val=\"22\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_6 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading6\">" +
            "<w:name w:val=\"Heading 6\"/>" +
            "<w:basedOn w:val=\"Heading\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:uiPriority w:val=\"9\"/>" +
            "<w:semiHidden/>" +
            "<w:unhideWhenUsed/>" +
            "<w:qFormat/>" +
            "<w:pPr>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"5\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:pBdr>" +
            "<w:top w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:left w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:bottom w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:right w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "</w:pBdr>" +
            "<w:spacing w:before=\"283\" w:after=\"283\"/>" +
            "<w:jc w:val=\"both\"/>" +
            "<w:outlineLvl w:val=\"5\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:sz w:val=\"22\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_7 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading7\">" +
            "<w:name w:val=\"Heading 7\"/>" +
            "<w:basedOn w:val=\"Heading\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:pPr>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"6\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:pBdr>" +
            "<w:top w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:left w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:bottom w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:right w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "</w:pBdr>" +
            "<w:spacing w:before=\"283\" w:after=\"57\"/>" +
            "<w:jc w:val=\"both\"/>" +
            "<w:outlineLvl w:val=\"6\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:sz w:val=\"22\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_8 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading8\">" +
            "<w:name w:val=\"Heading 8\"/>" +
            "<w:basedOn w:val=\"Heading\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:pPr>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"7\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:pBdr>" +
            "<w:top w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:left w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:bottom w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:right w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "</w:pBdr>" +
            "<w:spacing w:before=\"283\" w:after=\"57\"/>" +
            "<w:jc w:val=\"both\"/>" +
            "<w:outlineLvl w:val=\"7\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:sz w:val=\"21\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String HEADING_9 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:styleId=\"Heading9\">" +
            "<w:name w:val=\"Heading 9\"/>" +
            "<w:basedOn w:val=\"Heading\"/>" +
            "<w:next w:val=\"Normal\"/>" +
            "<w:pPr>" +
            "<w:numPr>" +
            "<w:ilvl w:val=\"8\"/>" +
            "<w:numId w:val=\"1\"/>" +
            "</w:numPr>" +
            "<w:pBdr>" +
            "<w:top w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:left w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:bottom w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "<w:right w:val=\"none\" w:sz=\"0\" w:space=\"0\" w:color=\"auto\"/>" +
            "</w:pBdr>" +
            "<w:spacing w:before=\"283\" w:after=\"57\"/>" +
            "<w:jc w:val=\"both\"/>" +
            "<w:outlineLvl w:val=\"8\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:sz w:val=\"21\"/>" +
            "</w:rPr>" +
            "</w:style>";

    private static final String TOC_HEADING = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"En-ttedetabledesmatires\">" +
            "<w:name w:val=\"TOC Heading\"/>" +
            "<w:basedOn w:val=\"Heading\"/>" +
            "<w:pPr>" +
            "<w:pageBreakBefore/>" +
            "<w:suppressLineNumbers/>" +
            "<w:spacing w:before=\"0\" w:after=\"283\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:bCs/>" +
            "<w:szCs w:val=\"32\"/>" +
            "</w:rPr>" +
            "</w:style>";

    private static final String TOC_1 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM1\">" +
            "<w:name w:val=\"toc 1\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:autoRedefine/>" +
            "<w:rsid w:val=\"00882EFF\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9637\"/>" +
            "</w:tabs>" +
            "<w:spacing w:after=\"120\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial Gras\" w:hAnsi=\"Arial Gras\"/>" +
            "<w:b/>" +
            "<w:smallCaps/>" +
            "<w:sz w:val=\"18\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_2 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM2\">" +
            "<w:name w:val=\"toc 2\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"238\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"18\"/>" +
            "</w:rPr>" +
            "</w:style>";


    private static final String TOC_3 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM3\">" +
            "<w:name w:val=\"toc 3\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9241\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"482\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"16\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_4 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM4\">" +
            "<w:name w:val=\"toc 4\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9128\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"709\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"16\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_5 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM5\">" +
            "<w:name w:val=\"toc 5\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9015\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"851\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"16\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_6 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM6\">" +
            "<w:name w:val=\"toc 6\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9015\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"880\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"14\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_7 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM7\">" +
            "<w:name w:val=\"toc 7\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9015\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"900\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"14\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_8 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM8\">" +
            "<w:name w:val=\"toc 8\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9015\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"910\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"14\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TOC_9 = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"TM9\">" +
            "<w:name w:val=\"toc 9\"/>" +
            "<w:basedOn w:val=\"Index\"/>" +
            "<w:rsid w:val=\"00C95DC6\"/>" +
            "<w:pPr>" +
            "<w:tabs>" +
            "<w:tab w:val=\"right\" w:leader=\"dot\" w:pos=\"9015\"/>" +
            "</w:tabs>" +
            "<w:spacing w:before=\"0\"/>" +
            "<w:ind w:left=\"920\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:sz w:val=\"14\"/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String LGENDE = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:type=\"paragraph\"" +
            " w:styleId=\"Lgende\">" +
            "<w:name w:val=\"Lgende\"/>" +
            "<w:basedOn w:val=\"Standard\"/>" +
            "<w:rsid w:val=\"00567D31\"/>" +
            "<w:pPr>" +
            "<w:suppressLineNumbers/>" +
            "<w:spacing w:after=\"120\"/>" +
            "</w:pPr>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:i/>" +
            "<w:iCs/>" +
            "</w:rPr>" +
            "</w:style>";
    private static final String TABLE = "<w:style xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "w:type=\"paragraph\" w:customStyle=\"1\" w:styleId=\"Table\">" +
            "<w:name w:val=\"Table\"/>" +
            "<w:basedOn w:val=\"Lgende\"/>" +
            "<w:rPr>" +
            "<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\"/>" +
            "<w:i w:val=\"0\"/>" +
            "<w:sz w:val=\"17\"/>" +
            "</w:rPr>" +
            "</w:style>";

    private StylesUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    private static XWPFStyle createStyle(XWPFStyles styles, String tableStyleXML) throws Exception {

        if (styles == null || tableStyleXML == null) return null;

        CTStyles ctStyles = CTStyles.Factory.parse(tableStyleXML);
        CTStyle ctStyle = ctStyles.getStyleArray(0);

        XWPFStyle style = styles.getStyleWithName(ctStyle.getName().getVal());
        if (style == null) {
            style = new XWPFStyle(ctStyle, styles);
            styles.addStyle(style);
        } else {
            style.setStyle(ctStyle);
        }
        return style;
    }

    public static void setDefaultDocumentStyle(XWPFDocument document) {
        XWPFStyles styles = document.getStyles();
        if (styles == null) {
            styles = document.createStyles();
        }
        try {
            createStyle(styles, NORMAL);
            createStyle(styles, STANDARD);
            createStyle(styles, LGENDE);
            createStyle(styles, TITRE_PRINCIPAL);
            setDefaultDocumentHeadingStyle(styles);
            setDefaultDocumentTocStyle(styles);
            createStyle(styles, TABLE);

        } catch (Exception e) {
            log.error("Erreur lors de la creation du style : {}", e.getMessage());
        }
    }

    private static void setDefaultDocumentHeadingStyle(XWPFStyles styles) {
        try {
            addStyleWithHeading(styles, HEADING, HEADING_1, HEADING_2, HEADING_3, HEADING_4, HEADING_5, HEADING_6, HEADING_7, HEADING_8, HEADING_9);
        } catch (Exception e) {
            log.error("Erreur lors de la creation du style Heading : {}", e.getMessage());
        }
    }

    private static void addStyleWithHeading(XWPFStyles styles, String heading, String heading1, String heading2, String heading3, String heading4, String heading5, String heading6, String heading7, String heading8, String heading9) throws Exception {
        createStyle(styles, heading);
        createStyle(styles, heading1);
        createStyle(styles, heading2);
        createStyle(styles, heading3);
        createStyle(styles, heading4);
        createStyle(styles, heading5);
        createStyle(styles, heading6);
        createStyle(styles, heading7);
        createStyle(styles, heading8);
        createStyle(styles, heading9);
    }

    public static void setDefaultDocumentTocStyle(XWPFStyles styles) {
        try {
            addStyleWithHeading(styles, TOC_HEADING, TOC_1, TOC_2, TOC_3, TOC_4, TOC_5, TOC_6, TOC_7, TOC_8, TOC_9);
        } catch (Exception e) {
            log.error("Erreur lors de la creation du style TOC : {}", e.getMessage());
        }
    }

    public static String getStyleByName(XWPFDocument document, String name, String defaultStyleId) {
        XWPFStyles styles = document.getStyles();
        if (styles == null) {
            styles = document.createStyles();
        }
        XWPFStyle style = styles.getStyleWithName(name);
        if (style == null) {
            style = styles.getStyleWithName(name.toLowerCase());
        }
        if (style == null) {
            try {
                Field field = StylesUtils.class.getDeclaredField(name.toUpperCase().replace(' ', '_'));
                String tableStyleXML = field.get(null).toString();
                style = createStyle(styles, tableStyleXML);
                styles.addStyle(style);
            } catch (Exception e) {
                log.error("Erreur lors de l'ajout du style");
            }
        }
        return style != null ? style.getStyleId() : defaultStyleId;
    }

    public static XWPFStyle getStyleByName(XWPFDocument document, String name) {
        XWPFStyles styles = document.getStyles();
        if (styles == null) {
            styles = document.createStyles();
        }
        return styles.getStyleWithName(name);
    }

    public static XWPFStyle getStyleById(XWPFDocument document, String id) {
        XWPFStyles styles = document.getStyles();
        if (styles == null) {
            return null;
        }
        return styles.getStyle(id);
    }

}
