package com.atexo.docgen.core.domain.convertor.services.impl.html;

import com.atexo.docgen.core.domain.convertor.model.FormatedText;
import com.atexo.docgen.core.domain.convertor.model.TagFormat;
import com.atexo.docgen.core.domain.convertor.services.html.TagFormatHandler;
import org.apache.commons.lang.StringUtils;
import org.xmlpull.v1.XmlPullParser;

import java.util.stream.Stream;


public class SpanTagFormatHandler implements TagFormatHandler {


    @Override
    public FormatedText closing(TagFormat tagFormat) {
        return null;
    }

    @Override
    public TagFormat parse(XmlPullParser xpp) {
        String style = xpp.getAttributeValue("", "style");
        if (StringUtils.isNotBlank(style)) {
            final TagFormat tf = TagFormat.builder()
                    .name(xpp.getName())
                    .format("style")
                    .build();
            Stream.of(style.split(";")).forEach(st -> {
                String[] styleDef = st.split(":");
                String attribute = this.transcodeAttribute(styleDef[0].toLowerCase().trim());
                if (attribute.equals("text-decoration") && styleDef[1].trim().equals("underline")) {
                    tf.setFormat("underline");
                } else {
                    tf.addAttribute(attribute, styleDef[1].trim());
                }
            });
            return tf;
        }
        return null;
    }

    private String transcodeAttribute(String attrb) {
        if ("background".equals(attrb)) {
            return "background-color";
        } else {
            return attrb;
        }
    }
}
