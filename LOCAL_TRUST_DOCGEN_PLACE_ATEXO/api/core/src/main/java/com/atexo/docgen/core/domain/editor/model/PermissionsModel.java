package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class PermissionsModel implements Serializable {
    private Boolean comment;
    private Boolean download;
    private Boolean edit;
    private Boolean fillForms;
    private Boolean modifyFilter;
    private Boolean modifyContentControl;
    private Boolean review;
    private Boolean print;
}
