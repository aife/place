package com.atexo.docgen.core.domain.editor.model;

import lombok.*;

import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentChanges {
    private ZonedDateTime created;
    private User user;
}
