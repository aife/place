package com.atexo.docgen.core.utilitaire;

import com.atexo.docgen.core.domain.convertor.model.ChapitreType;

import java.util.List;

import static com.atexo.docgen.core.utilitaire.ClauseUtils.isClausesValide;

public final class ChapitreUtils {

    private ChapitreUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static boolean isChapitresValide(List<ChapitreType> chapitres) {
        boolean valid = true;
        if (chapitres == null) {
            return valid;
        }
        for (ChapitreType chapitre : chapitres) {
            if (!isClausesValide(chapitre.getClause())) {
                valid = false;
                break;
            }
            boolean sousChapitreValide = isChapitresValide(chapitre.getChapitre());
            chapitre.setValide(sousChapitreValide);
            if (!sousChapitreValide) {
                valid = false;
                break;
            }
        }
        return valid;
    }
}
