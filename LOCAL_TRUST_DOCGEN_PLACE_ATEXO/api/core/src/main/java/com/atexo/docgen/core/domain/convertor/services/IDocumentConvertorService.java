package com.atexo.docgen.core.domain.convertor.services;

import com.atexo.docgen.core.domain.convertor.model.ClauseType;
import com.atexo.docgen.core.domain.convertor.model.redac.DocumentXmlModel;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.domain.editor.model.DocumentAdministrableConfiguration;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface IDocumentConvertorService {

    DocumentXmlModel getById(String id);

    ByteArrayOutputStream convert(InputStream template, DocxDocument document);

    DocxDocument getDocxDocument(InputStream xml);


    DocxDocument sanitizeDocxDocument(DocxDocument docxDocument);

    void addXmlDocument(InputStream xml, String documentId, String plateformeId);

    void addTemplateDocument(DocumentAdministrableConfiguration configuration, String documentId, String plateformeId);

    void addDocumentToValidate(String documentId, String plateformeId, Integer status);

    ByteArrayOutputStream convertClause(ClauseType clause, boolean from, String chapitreNumero, String id);

    ByteArrayOutputStream getXml(String document);

    ByteArrayOutputStream getXml(DocxDocument document);

    DocumentXmlModel modifyDocumentXml(String id, DocxDocument document);

    ByteArrayOutputStream convertDocument(InputStream inputStream, String originalFormat, String outputFormat) throws IOException;

    ByteArrayOutputStream finalizeDocument(InputStream inputStream, String originalFormat, String outputFormat) throws IOException;

    void updateToc(XWPFDocument srcDoc);

    ByteArrayOutputStream getTemplate(InputStream inputStream, Map<String, String> headersCallback);
}
