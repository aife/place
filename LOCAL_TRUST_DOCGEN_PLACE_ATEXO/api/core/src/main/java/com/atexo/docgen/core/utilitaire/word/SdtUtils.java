package com.atexo.docgen.core.utilitaire.word;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.List;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.cloneParagraph;
import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.cloneTable;

@Slf4j
public final class SdtUtils {
    private SdtUtils() {
        throw new IllegalStateException("Classe utilitaire");
    }

    public static XmlCursor copySdtContent(XWPFDocument newDoc, XmlCursor cursor, CTSdtBlock clauseContentControl) {
        List<CTP> pList = clauseContentControl.getSdtContent().getPList();
        if (!CollectionUtils.isEmpty(pList)) {
            for (CTP ctp : pList) {
                cursor.toNextSibling();
                XWPFParagraph paragraph = newDoc.insertNewParagraph(cursor);
                if (paragraph != null) {
                    cloneParagraph(paragraph, ctp);
                    cursor = paragraph.getCTP().newCursor();
                }
            }
        }
        List<CTTbl> tList = clauseContentControl.getSdtContent().getTblList();
        if (!CollectionUtils.isEmpty(tList)) {
            for (CTTbl ctTbl : tList) {
                cursor.toNextSibling();
                XWPFTable table = newDoc.insertNewTbl(cursor);
                if (table != null) {
                    cloneTable(table, ctTbl);
                    cursor = table.getCTTbl().newCursor();
                }
            }
            cursor.toNextSibling();
            XWPFParagraph paragraph = newDoc.insertNewParagraph(cursor);
            if (paragraph != null) {
                XWPFRun run = paragraph.createRun();
                run.setText("");
                cursor = paragraph.getCTP().newCursor();
            }
        }
        return cursor;
    }


    public static void addSdtToDocument(XWPFDocument document, XWPFDocument generatedDocument, String id, String tag,
                                        String title, boolean notDel, boolean notEdit) {
        if (generatedDocument == null || (CollectionUtils.isEmpty(generatedDocument.getParagraphs()) &&
                CollectionUtils.isEmpty(generatedDocument.getTables()))) {
            return;
        }
        CTBody addBody = generatedDocument.getDocument().getBody();
        if (addBody == null || document == null) {
            return;
        }

        PackagePart sourcePackagePart = generatedDocument.getPackagePart();
        PackagePart targetPackagePart = document.getPackagePart();

        try {
            for (PackageRelationship rel : sourcePackagePart.getRelationships()) {
                if (rel.getTargetMode().equals(TargetMode.EXTERNAL)) {
                    PackageRelationship newExternalRelationShip = targetPackagePart.addRelationship(rel.getTargetURI(), TargetMode.EXTERNAL, rel.getRelationshipType());
                    for (XWPFParagraph paragraph : generatedDocument.getParagraphs()) {
                        for (CTHyperlink hyperlink : paragraph.getCTP().getHyperlinkList()) {
                            if (hyperlink.getId().equals(rel.getId())) {
                                hyperlink.setId(newExternalRelationShip.getId());
                            }
                        }
                    }
                }
            }
        } catch (InvalidFormatException e) {
            log.error("Erreur lors de la copie des relations externes", e);
        }


        CTSdtBlock block = document.getDocument().getBody().addNewSdt();
        CTSdtPr sdtPr = block.addNewSdtPr();
        sdtPr.addNewDocPartObj().addNewDocPartGallery().setVal(id + tag);
        CTSdtEndPr sdtEndPr = block.addNewSdtEndPr();
        setSdtStyle(sdtEndPr);
        CTSdtContentBlock content = block.addNewSdtContent();
        content.set(addBody.changeType(CTSdtContentBlock.Factory.newInstance().schemaType()));
        setCTSdtPr(sdtPr, id, title, tag, notDel, notEdit);

    }


    public static void setSdtStyle(CTSdtEndPr sdtEndPr) {

        CTRPr rPr = sdtEndPr.addNewRPr();
        CTFonts fonts = rPr.addNewRFonts();
        fonts.setAsciiTheme(STTheme.MINOR_H_ANSI);
        fonts.setEastAsiaTheme(STTheme.MINOR_H_ANSI);
        fonts.setHAnsiTheme(STTheme.MINOR_H_ANSI);
        fonts.setCstheme(STTheme.MINOR_BIDI);
        rPr.addNewB().setVal(STOnOff.OFF);
        rPr.addNewBCs().setVal(STOnOff.OFF);
        rPr.addNewColor().setVal("auto");
        rPr.addNewSz().setVal(BigInteger.valueOf(24));
        rPr.addNewSzCs().setVal(BigInteger.valueOf(20));
        CTLanguage lang = rPr.addNewLang();
        lang.setBidi("fa-IR");
        lang.setEastAsia("ja-JP");
    }

    public static void setCTSdtPr(CTSdtPr sdtPr, String id, String title, String tag,
                                  boolean notDel, boolean notEdit) {
        // Plain Text
        CTSdtText sdtText = sdtPr.addNewText();
        // allow multiple lines
        sdtText.setMultiLine(STOnOff.TRUE);


        // Set the id
        if (StringUtils.isNumeric(id)) {
            CTDecimalNumber sdtId = sdtPr.addNewId();
            sdtId.setVal(new BigInteger(id));
        }
        // set the title
        if (StringUtils.isNotBlank(title)) {
            CTString ctAlia = sdtPr.addNewAlias();
            ctAlia.setVal(title);
        }
        // set the tag
        if (StringUtils.isNotBlank(tag)) {
            CTString ctTag = sdtPr.addNewTag();
            ctTag.setVal(tag);
        }
        setSdtPermission(sdtPr, notDel, notEdit);


    }

    public static void setSdtPermission(CTSdtPr sdtPr, boolean notDel, boolean notEdit) {
        // Unable to delete content control, unable to edit content
        if (notDel && notEdit) {
            CTLock ctLock = sdtPr.addNewLock();
            ctLock.setVal(STLock.SDT_CONTENT_LOCKED);
        } else if (notDel) {
            // can't delete
            CTLock ctLock = sdtPr.addNewLock();
            ctLock.setVal(STLock.SDT_LOCKED);
        } else if (notEdit) {
            // can't edit
            CTLock ctLock = sdtPr.addNewLock();
            ctLock.setVal(STLock.CONTENT_LOCKED);
        }
    }

    public static void addEmptySdt(XWPFDocument destination) {
        XWPFDocument document = new XWPFDocument();
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("À remplacer avec le contenu de bloc template");
        addSdtToDocument(destination, document, "1", "BLOC_TEMPLATE", "consultation.test", true, false);
    }

    public static List<IBodyElement> getIsdtContents(ISDTContent content) throws NoSuchFieldException, IllegalAccessException {
        Field bodyElementsField = content.getClass().getDeclaredField("bodyElements");
        bodyElementsField.setAccessible(true);
        List<IBodyElement> isdtContents = (List) bodyElementsField.get(content);
        bodyElementsField.setAccessible(false);
        return isdtContents;
    }

    public static List<IBodyElement> getRunsdtContents(CTSdtContentRun content) throws NoSuchFieldException, IllegalAccessException {
        Field bodyElementsField = content.getClass().getDeclaredField("bodyElements");
        bodyElementsField.setAccessible(true);
        List<IBodyElement> isdtContents = (List) bodyElementsField.get(content);
        bodyElementsField.setAccessible(false);
        return isdtContents;
    }

}
