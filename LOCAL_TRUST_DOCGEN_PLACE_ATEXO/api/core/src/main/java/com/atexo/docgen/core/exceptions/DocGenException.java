package com.atexo.docgen.core.exceptions;

import com.atexo.docgen.core.enums.TypeExceptionEnum;

public class DocGenException extends RuntimeException {
    private static final long serialVersionUID = 100L;
    private final TypeExceptionEnum type;


    public DocGenException(TypeExceptionEnum type, String message) {
        super(message);
        this.type = type;
    }

    public DocGenException(TypeExceptionEnum type, String message, Throwable e) {
        super(message, e);
        this.type = type;
    }

    public TypeExceptionEnum getType() {
        return type;
    }

}
