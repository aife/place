package com.atexo.docgen.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UserRoleEnum {
    MODIFICATION("modification"),
    VALIDATION("validation"), REPRENDRE("reprendre");
    private final String value;

    UserRoleEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static UserRoleEnum fromValue(String value) {
        for (UserRoleEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        throw new IllegalArgumentException("Cannot find FileModeEnum from value: " + value);
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
