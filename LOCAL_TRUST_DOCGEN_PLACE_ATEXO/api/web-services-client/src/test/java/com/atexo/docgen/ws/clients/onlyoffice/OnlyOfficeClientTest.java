package com.atexo.docgen.ws.clients.onlyoffice;

import com.atexo.docgen.ws.clients.onlyoffice.model.ConvertRequest;
import com.atexo.docgen.ws.clients.onlyoffice.model.ConvertResponse;
import com.atexo.docgen.ws.config.WsScanConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;


@SpringBootTest(classes = WsScanConfig.class)
class OnlyOfficeClientTest {

    @Autowired
    OnlyOfficeClientWS onlyOfficeClient;

    @Test
    void shouldReturnErrorDocument() {
        //Given
        ConvertRequest convertRequest = ConvertRequest.builder()
                .outputtype("pdf")
                .url(null)
                .key("key")
                .filetype("docx")
                .build();
        //When
        final ConvertResponse result = onlyOfficeClient.convertFile(convertRequest);
        //Then
        assertNotNull(result);
        assertFalse(result.isEndConvert());
    }

    @Test
    void shouldReturnConvertDocument() throws IOException {
        //Given
        ConvertRequest convertRequest = ConvertRequest.builder()
                .outputtype("pdf")
                .url("https://ressources-release.local-trust.com/docs/modeles/atexo/gen_doc_mpe/annexe_financiere.xlsx")
                .key("key")
                .filetype("xlsx")
                .build();
        //When
        final ConvertResponse result = onlyOfficeClient.convertFile(convertRequest);
        //Then
        assertNotNull(result);
        assertTrue(result.isEndConvert());
        assertNull(result.getError());
        Resource resource = new RestTemplate().getForObject(result.getFileUrl(), Resource.class);
        try (FileOutputStream fos = new FileOutputStream("src/test/resources/expected/converted-annexe_financiere." + convertRequest.getOutputtype())) {
            fos.write(resource.getInputStream().readAllBytes());
        }
    }

    @Test
    void shouldReturnConvertDocumentDocx() throws IOException {
        //Given
        ConvertRequest convertRequest = ConvertRequest.builder()
                .outputtype("pdf")
                .url("https://ressources-release.local-trust.com/docs/modeles/atexo/gen_doc_mpe/Admission_candidatures.docx")
                .key("key")
                .filetype("docx")
                .build();
        //When
        final ConvertResponse result = onlyOfficeClient.convertFile(convertRequest);
        //Then
        assertNotNull(result);
        assertTrue(result.isEndConvert());
        assertNull(result.getError());
        Resource resource = new RestTemplate().getForObject(result.getFileUrl(), Resource.class);
        try (FileOutputStream fos = new FileOutputStream("src/test/resources/expected/converted-Admission_candidatures." + convertRequest.getOutputtype())) {
            fos.write(resource.getInputStream().readAllBytes());
        }
    }
}
