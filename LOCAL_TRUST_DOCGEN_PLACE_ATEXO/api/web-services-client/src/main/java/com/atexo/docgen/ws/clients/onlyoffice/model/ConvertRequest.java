package com.atexo.docgen.ws.clients.onlyoffice.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConvertRequest implements Serializable {

    private String outputtype;
    private String url;
    private String key;
    private String filetype;
}
