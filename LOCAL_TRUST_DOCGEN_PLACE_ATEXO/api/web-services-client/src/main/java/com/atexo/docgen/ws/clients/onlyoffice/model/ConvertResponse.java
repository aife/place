package com.atexo.docgen.ws.clients.onlyoffice.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConvertResponse implements Serializable {

    private boolean endConvert;
    private Integer error;
    private String fileUrl;
}
