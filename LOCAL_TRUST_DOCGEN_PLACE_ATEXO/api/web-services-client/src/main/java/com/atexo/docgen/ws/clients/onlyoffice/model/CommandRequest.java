package com.atexo.docgen.ws.clients.onlyoffice.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommandRequest implements Serializable {

    private String key;
    private String c;
}
