package com.atexo.docgen.ws.clients.onlyoffice;


import com.atexo.docgen.ws.clients.onlyoffice.model.CommandRequest;
import com.atexo.docgen.ws.clients.onlyoffice.model.CommandResponse;
import com.atexo.docgen.ws.clients.onlyoffice.model.ConvertRequest;
import com.atexo.docgen.ws.clients.onlyoffice.model.ConvertResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.primeframework.jwt.Signer;
import org.primeframework.jwt.Verifier;
import org.primeframework.jwt.domain.JWT;
import org.primeframework.jwt.hmac.HMACSigner;
import org.primeframework.jwt.hmac.HMACVerifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
@Slf4j
public class OnlyOfficeClientWS {

    private final String tokenSecret;
    private final ObjectMapper objectMapper;
    private final JSONParser parser;
    private final String baseUrl;
    private final String frontUrl;
    private final RestTemplate atexoRestTemplate;

    OnlyOfficeClientWS(@Value("${external-apis.onlyoffice.base-url}") String baseUrl, @Value("${external-apis.onlyoffice.front-url}") String frontUrl, @Value("${external-apis.onlyoffice.secret:null}") String tokenSecret, RestTemplate atexoRestTemplate, ObjectMapper objectMapper) {
        this.baseUrl = baseUrl;
        this.frontUrl = frontUrl;
        this.parser = new JSONParser();
        this.tokenSecret = tokenSecret;
        this.objectMapper = objectMapper;
        this.atexoRestTemplate = atexoRestTemplate;
    }

    public ConvertResponse convertFile(ConvertRequest convertRequest) {
        String url = baseUrl + "/ConvertService.ashx?outputtype=%s&url=%s&key=%s";
        String requestUrl = String.format(url, convertRequest.getOutputtype(), convertRequest.getUrl(), convertRequest.getKey());
        log.info("OnlyOfficeClientWS request URL {}", requestUrl);
        return atexoRestTemplate.getForObject(requestUrl, ConvertResponse.class);
    }

    public CommandResponse command(CommandRequest convertRequest) {
        String url = baseUrl + "/coauthoring/CommandService.ashx?key=%s&c=%s";
        String requestUrl = String.format(url, convertRequest.getKey(), convertRequest.getC());
        log.info("OnlyOfficeClientWS request URL {}", requestUrl);
        return atexoRestTemplate.getForObject(requestUrl, CommandResponse.class);
    }

    public String apis() {
        String requestUrl = frontUrl + "/web-apps/apps/api/documents/api.js";
        log.info("OnlyOfficeClientWS request URL {}", requestUrl);
        return requestUrl;
    }


    // create document token
    public String createToken(final Map<String, Object> payloadClaims) {
        try {
            // build a HMAC signer using a SHA-256 hash
            Signer signer = HMACSigner.newSHA256Signer(tokenSecret);
            JWT jwt = new JWT();
            for (String key : payloadClaims.keySet()) {  // run through all the keys from the payload
                jwt.addClaim(key, payloadClaims.get(key));  // and write each claim to the jwt
            }
            return JWT.getEncoder().encode(jwt, signer);  // sign and encode the JWT to a JSON string representation
        } catch (Exception e) {
            return "";
        }
    }

    // check if the token is enabled
    public boolean tokenEnabled() {
        return tokenSecret != null && !tokenSecret.isEmpty();
    }

    // read document token
    public JWT readToken(final String token) {
        try {
            // build a HMAC verifier using the token secret
            Verifier verifier = HMACVerifier.newVerifier(tokenSecret);

            // verify and decode the encoded string JWT to a rich object
            return JWT.getDecoder().decode(token, verifier);
        } catch (Exception exception) {
            return null;
        }
    }

    // parse the body
    public JSONObject parseBody(final String payload, final String header) {
        JSONObject body;
        try {
            Object obj = parser.parse(payload);  // get body parameters by parsing the payload
            body = (JSONObject) obj;
        } catch (Exception ex) {
            throw new RuntimeException("{\"error\":1,\"message\":\"JSON Parsing error\"}");
        }
        if (tokenEnabled()) {  // check if the token is enabled
            String token = (String) body.get("token");  // get token from the body
            if (token == null) {  // if token is empty
                if (header != null && !header.isBlank()) {  // and the header is defined

                    // get token from the header (it is placed after the Bearer prefix if it exists)
                    token = header.startsWith("Bearer ") ? header.substring("Bearer ".length()) : header;
                }
            }
            if (token == null || token.isBlank()) {
                throw new RuntimeException("{\"error\":1,\"message\":\"JWT expected\"}");
            }

            JWT jwt = readToken(token);  // read token
            if (jwt == null) {
                throw new RuntimeException("{\"error\":1,\"message\":\"JWT validation failed\"}");
            }
            if (jwt.getObject("payload") != null) {  // get payload from the token and check if it is not empty
                try {
                    @SuppressWarnings("unchecked") LinkedHashMap<String, Object> jwtPayload = (LinkedHashMap<String, Object>) jwt.getObject("payload");

                    jwt.claims = jwtPayload;
                } catch (Exception ex) {
                    throw new RuntimeException("{\"error\":1,\"message\":\"Wrong payload\"}");
                }
            }
            try {
                Object obj = parser.parse(objectMapper.writeValueAsString(jwt.claims));
                body = (JSONObject) obj;
            } catch (Exception ex) {
                throw new RuntimeException("{\"error\":1,\"message\":\"Parsing error\"}");
            }
        }

        return body;
    }

}
