package com.atexo.docgen.ws.clients.onlyoffice.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CommandResponse implements Serializable {

    private String version;
    private Integer error;
    private String key;
}
