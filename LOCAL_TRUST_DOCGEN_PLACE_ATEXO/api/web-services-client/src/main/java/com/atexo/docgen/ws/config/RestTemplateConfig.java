package com.atexo.docgen.ws.config;


import com.atexo.docgen.ws.utils.TrustAllManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;

@Configuration
@Slf4j
public class RestTemplateConfig {
    private static final String SSL_PROTOCOL = "TLS";
    private final int httpConnectTimeout = 60000;
    private final int httpReadTimeout = 30000;

    @Bean
    RestTemplate atexoRestTemplate() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(httpConnectTimeout);
        requestFactory.setReadTimeout(httpReadTimeout);
        try {
            requestFactory.setHttpClient(createHttpClient());
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            log.error(e.getMessage());
        }
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        // move XML converter to the end of list
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        for (int i = 0; i < messageConverters.size() - 1; i++) {
            if (messageConverters.get(i) instanceof MappingJackson2XmlHttpMessageConverter) {
                Collections.swap(messageConverters, i, messageConverters.size() - 1);
            }
        }

        restTemplate.setMessageConverters(messageConverters);

        return restTemplate;


    }

    private HttpClient createHttpClient() throws KeyManagementException, NoSuchAlgorithmException {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setSSLContext(createSSLContext());
        return httpClientBuilder.build();
    }

    private SSLContext createSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance(SSL_PROTOCOL);
        sslContext.init(null, new TrustManager[]{TrustAllManager.THE_INSTANCE}, new SecureRandom());
        return sslContext;
    }

}
