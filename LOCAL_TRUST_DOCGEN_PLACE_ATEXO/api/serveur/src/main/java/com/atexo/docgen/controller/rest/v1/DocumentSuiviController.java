package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.config.http_logging.LogEntryExit;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.editor.model.DocumentConfiguration;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import com.atexo.docgen.core.domain.monitoring.services.IDocumentMonitoringService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.enums.FileModeEnum;
import com.atexo.docgen.core.enums.FileRedacStatusEnum;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.core.port.IDocumentPort;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.annotation.MultipartConfig;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@MultipartConfig
@RequestMapping("api/v1/document-suivi")
@Slf4j
public class DocumentSuiviController {

    private final IDocumentPort iDocumentPort;
    private final IDocumentMonitoringService iDocumentMonitoringService;
    private final IDocumentConvertorService iDocumentConvertorService;

    public DocumentSuiviController(IDocumentPort iDocumentPort, IDocumentMonitoringService iDocumentMonitoringService, IDocumentConvertorService iDocumentConvertorService) {
        this.iDocumentPort = iDocumentPort;
        this.iDocumentMonitoringService = iDocumentMonitoringService;
        this.iDocumentConvertorService = iDocumentConvertorService;
    }

    @GetMapping("status")
    @PreAuthorize("hasAccess()")
    public ResponseEntity<ByteArrayResource> getStatus(@AuthenticationPrincipal DocumentEditorDetails details) throws IOException {

        DocumentModel document = iDocumentMonitoringService.getDocumentModelById(details.getId());
        ByteArrayResource out = null;
        ByteArrayResource file = iDocumentMonitoringService.getLastRessourceVersion(document, details.getConfiguration());
        String title = document.getDocumentId();
        MediaType mediaType = MediaType.APPLICATION_OCTET_STREAM;
        if (file != null) {
            if (document.getUpdatedJson() != null && (document.getMode().equals(FileModeEnum.REDAC.getValue()) || document.getMode().equals(FileModeEnum.VALIDATION.getValue()))) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
                ZipOutputStream zipOutputStream = addOriginalFile(document, file, title, bufferedOutputStream);

                ByteArrayResource xmlFile = new ByteArrayResource(iDocumentConvertorService.getXml(document.getUpdatedJson()).toByteArray());
                zipOutputStream.putNextEntry(new ZipEntry(title + ".xml"));
                IOUtils.copy(xmlFile.getInputStream(), zipOutputStream);
                zipOutputStream.closeEntry();
                if (document.getStatus() != null &&
                        FileRedacStatusEnum.VALIDER.equals(FileRedacStatusEnum.fromValue(document.getRedacStatus()))) {
                    addVersionToZip(document, title, zipOutputStream, "validate_" + document.getFileType(), details.getConfiguration());
                    addVersionToZip(document, title, zipOutputStream, "validate_pdf", details.getConfiguration());
                }

                out = getByteArrayResourceFromZip(out, byteArrayOutputStream, bufferedOutputStream, zipOutputStream);


                title += ".zip";
                mediaType = MediaType.parseMediaType("application/zip");
            } else if (document.getMode().equals(FileModeEnum.TEMPLATE.getValue())) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
                ZipOutputStream zipOutputStream = addOriginalFile(document, file, title, bufferedOutputStream);

                ByteArrayResource templateFile = new ByteArrayResource(iDocumentConvertorService.getTemplate(iDocumentMonitoringService.getLastRessourceVersion(document, details.getConfiguration()).getInputStream(), details.getHeadersCallback()).toByteArray());
                zipOutputStream.putNextEntry(new ZipEntry(title + "_template.docx"));
                IOUtils.copy(templateFile.getInputStream(), zipOutputStream);
                zipOutputStream.closeEntry();

                out = getByteArrayResourceFromZip(out, byteArrayOutputStream, bufferedOutputStream, zipOutputStream);


                title += ".zip";
                mediaType = MediaType.parseMediaType("application/zip");
            }

            else {
                out = file;
                title += "." + document.getFileType();
            }

            iDocumentPort.setNewVersion(document.getId(), false);
        }
        return out == null ? getNullBody(document, mediaType) : getBody(document, out, title, mediaType);

    }

    private ByteArrayResource getByteArrayResourceFromZip(ByteArrayResource out, ByteArrayOutputStream byteArrayOutputStream, BufferedOutputStream bufferedOutputStream, ZipOutputStream zipOutputStream) throws IOException {
        zipOutputStream.finish();
        zipOutputStream.flush();
        if (byteArrayOutputStream.size() > 0)
            out = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        IOUtils.closeQuietly(zipOutputStream);
        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);
        return out;
    }

    private ZipOutputStream addOriginalFile(DocumentModel document, ByteArrayResource file, String title, BufferedOutputStream bufferedOutputStream) throws IOException {
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
        zipOutputStream.putNextEntry(new ZipEntry(title + "." + document.getFileType()));
        IOUtils.copy(file.getInputStream(), zipOutputStream);
        zipOutputStream.closeEntry();
        return zipOutputStream;
    }

    @LogEntryExit(skip = true)
    @GetMapping("plateforme-status")
    public List<FileStatus> getPlateformeFileStatus(@RequestParam String plateformeId) throws IOException {

        List<DocumentModel> documents = iDocumentMonitoringService.getDocumentModelByPlateforme(plateformeId);
        return iDocumentMonitoringService.getFileStatus(documents);


    }

    @GetMapping("download-list")
    @LogEntryExit(skip = true)
    public List<String> getPlateformeFileToDownload(@RequestParam String plateformeId) {
        return iDocumentMonitoringService.getDocumentModelByPlateformeAndStatus(plateformeId, FileStatusEnum.SAVED_AND_CLOSED);
    }

    @PostMapping("modify-status")
    @PreAuthorize("hasAccess()")
    public String modifyStatus(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody String status) {
        iDocumentMonitoringService.changeStatus(details.getId(), status);
        return "OK";
    }


    private ResponseEntity<ByteArrayResource> getBody(DocumentModel document, ByteArrayResource out, String title, MediaType mediaType) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + title)
                .header("document-status", document.getStatus())
                .header("document-version", String.valueOf(document.getVersion()))
                .header("redac-status", String.valueOf(document.getRedacStatus()))
                .header("document-modifcation-date", document.getModificationDate().toString())
                .contentType(mediaType)
                .cacheControl(CacheControl.noCache())
                .contentLength(out.contentLength())
                .body(out);
    }

    private ResponseEntity<ByteArrayResource> getNullBody(DocumentModel document, MediaType mediaType) {
        return ResponseEntity.ok()
                .header("document-status", document.getStatus())
                .header("document-version", String.valueOf(document.getVersion()))
                .header("redac-status", String.valueOf(document.getRedacStatus()))
                .header("document-modifcation-date", document.getModificationDate().toString())
                .contentType(mediaType)
                .cacheControl(CacheControl.noCache())
                .body(null);
    }

    private ByteArrayResource addVersionToZip(DocumentModel document, String title, ZipOutputStream zipOutputStream, String suffix, DocumentConfiguration configuration) throws IOException {
        ByteArrayResource resource = iDocumentMonitoringService.getResource(document, "." + suffix, configuration);
        if (resource != null) {
            zipOutputStream.putNextEntry(new ZipEntry(title + "." + suffix));
            IOUtils.copy(resource.getInputStream(), zipOutputStream);
            zipOutputStream.closeEntry();
        }
        return resource;

    }


}
