package com.atexo.docgen.controller.config.security;

import com.atexo.docgen.core.config.MDCService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.impl.JwtServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class AuthTokenFilter extends OncePerRequestFilter {
    @Autowired
    private JwtServiceImpl jwtServiceImpl;

    @Autowired
    private MDCService mdcService;

    @Autowired
    private UserDetailsService atexoUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String jwt = parseJwt(request);
        try {
            if (jwt != null && jwtServiceImpl.validateJwtToken(jwt)) {
                DocumentEditorDetails details = jwtServiceImpl.getEditorDetailsFromToken(jwt);
                if (details != null) {
                    setDocumentToken(request, details);
                    mdcService.putMDC(details);
                } else {
                    String username = jwtServiceImpl.getUserNameFromJwtToken(jwt);
                    UserDetails userDetails = atexoUserDetailsService.loadUserByUsername(username);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                            userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    mdcService.putMDC(null);
                }
            } else {
                mdcService.putMDC(null);
            }
        } catch (Exception e) {
            log.error("Cannot set user authentication: {} => {}", jwt, e.getMessage());
        }

        filterChain.doFilter(request, response);
    }

    private void setDocumentToken(HttpServletRequest request, DocumentEditorDetails details) {
        if (details.getUser().getRoles() == null) {
            details.getUser().setRoles(new ArrayList<>());
        }
        List<String> userRoles = details.getUser().getRoles();

        List<GrantedAuthority> authorities = userRoles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                details, null, authorities);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }

        return request.getParameter("token");
    }
}
