package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.GenerationRequest;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.domain.generator.services.impl.DocumentGeneratorFactory;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@MultipartConfig
@RequestMapping("api/v1/document-generator")
@Slf4j
public class DocumentGeneratorController {

    private final DocumentGeneratorFactory documentGeneratorFactory;
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentConvertorService iDocumentConvertorService;

    public DocumentGeneratorController(DocumentGeneratorFactory documentGeneratorFactory,
                                       IDocumentConfigurationService iDocumentConfigurationService, IDocumentConvertorService iDocumentConvertorService) {
        this.documentGeneratorFactory = documentGeneratorFactory;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentConvertorService = iDocumentConvertorService;
    }

    @PostMapping("generate")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> generate(@RequestPart @NotNull MultipartFile template, @RequestPart @NotNull List<KeyValueRequest> keyValues,
                                                      @RequestParam(required = false) Boolean convertToPdf, @RequestParam(required = false) String defaultOnNull,
                                                      @RequestParam(required = false) String dateFormat,
                                                      @RequestParam(required = false) String outputFileName) {
        String originalFilename = template.getOriginalFilename();
        String extension = DocumentUtils.getFileExtension(originalFilename);
        if (!iDocumentConfigurationService.getEditedExts().contains(extension)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.GENERATION);

        }
        try (InputStream inputStream = template.getInputStream()) {


            FileGeneratorRequest request = FileGeneratorRequest.builder()
                    .stream(inputStream)
                    .extension(extension)
                    .keyValues(keyValues.stream()
                            .filter(keyValueRequest -> Objects.nonNull(keyValueRequest.getKey()) &&
                                    Objects.nonNull(keyValueRequest.getValue()))
                            .collect(Collectors.toMap(KeyValueRequest::getKey, KeyValueRequest::getValue)))
                    .build();

            assert extension != null;
            ByteArrayResource file = new ByteArrayResource(documentGeneratorFactory.generate(request, defaultOnNull, dateFormat).toByteArray(), dateFormat);
            String attachement;
            String contentType = template.getContentType();
            if (Boolean.TRUE.equals(convertToPdf)) {
                file = new ByteArrayResource(iDocumentConvertorService.finalizeDocument(file.getInputStream(), extension, "pdf").toByteArray());

                String baseName = FilenameUtils.getBaseName(outputFileName != null ? outputFileName : originalFilename);
                attachement = baseName + ".pdf";
                contentType = "application/pdf";
            } else {
                attachement = outputFileName != null ? outputFileName : originalFilename;
            }
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + attachement)
                    .contentType(MediaType.parseMediaType(Objects.requireNonNull(contentType)))
                    .cacheControl(CacheControl.noCache())
                    .contentLength(file.contentLength())
                    .body(file);
        } catch (IOException e) {
            log.error("Erreur lors de la génération du document {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.GENERATION, e.getMessage());
        }

    }

    @PostMapping("variables")
    public Map<String, Set<String>> getDocumentVariable(MultipartFile template) {
        if (template == null) {
            log.error("Le fichier ne doit pas être null");
            throw new MauvaisParametreException(TypeExceptionEnum.CHAMPS_FUSION, "Le fichier ne doit pas être null");
        }
        String curExt = DocumentUtils.getFileExtension(template.getOriginalFilename());
        if (!iDocumentConfigurationService.getEditedExts().contains(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CHAMPS_FUSION);
        }
        return documentGeneratorFactory.getFileVariable(curExt, template);

    }


    @PostMapping("generate/{type}")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> generateRandom(
            @RequestBody GenerationRequest request,
            @PathVariable String type,
            @RequestParam(required = false) String filename) throws IOException {
        if (!iDocumentConfigurationService.getEditedExts().contains(type)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.GENERATION);
        }

        String attachement = filename != null ? filename : "output";
        String contentType = "xlsx".equals(type) ? "application/vnd.openxmlformats" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        ByteArrayResource file = new ByteArrayResource(documentGeneratorFactory.buildDocument(type, request).toByteArray());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + attachement)
                .contentType(MediaType.parseMediaType(Objects.requireNonNull(contentType)))
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);

    }

}
