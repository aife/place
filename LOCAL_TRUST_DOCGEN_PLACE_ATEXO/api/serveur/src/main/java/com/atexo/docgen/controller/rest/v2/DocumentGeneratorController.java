package com.atexo.docgen.controller.rest.v2;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.services.impl.DocumentGeneratorFactory;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Objects;

@RestController("DocumentGeneratorControllerV2")
@MultipartConfig
@RequestMapping("api/v2/document-generator")
@Slf4j
public class DocumentGeneratorController {

    private final DocumentGeneratorFactory documentGeneratorFactory;
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentConvertorService iDocumentConvertorService;

    public DocumentGeneratorController(DocumentGeneratorFactory documentGeneratorFactory, IDocumentConfigurationService iDocumentConfigurationService, IDocumentConvertorService iDocumentConvertorService) {
        this.documentGeneratorFactory = documentGeneratorFactory;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentConvertorService = iDocumentConvertorService;
    }

    @PostMapping("generate")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> generate(@RequestPart @NotNull MultipartFile template, @RequestPart @NotNull Map<String, Object> keyValues, @RequestParam(required = false) Boolean convertToPdf, @RequestParam(required = false) String defaultOnNull, @RequestParam(required = false) String dateFormat) {
        String originalFilename = template.getOriginalFilename();
        String extension = DocumentUtils.getFileExtension(originalFilename);
        if (!iDocumentConfigurationService.getEditedExts().contains(extension)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.GENERATION);

        }
        try (InputStream inputStream = template.getInputStream()) {


            FileGeneratorRequest request = FileGeneratorRequest.builder()
                    .stream(inputStream)
                    .extension(extension)
                    .keyValues(keyValues)
                    .build();

            assert extension != null;
            ByteArrayResource file = new ByteArrayResource(documentGeneratorFactory.generate(request, defaultOnNull, dateFormat).toByteArray());
            String attachement = originalFilename;
            String contentType = template.getContentType();
            if (Boolean.TRUE.equals(convertToPdf)) {
                file = new ByteArrayResource(iDocumentConvertorService.finalizeDocument(file.getInputStream(), extension, "pdf").toByteArray());
                attachement += ".pdf";
                contentType = "application/pdf";
            }
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + attachement).contentType(MediaType.parseMediaType(Objects.requireNonNull(contentType))).cacheControl(CacheControl.noCache()).contentLength(file.contentLength()).body(file);
        } catch (IOException e) {
            log.error("Erreur lors de la génération du document {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.GENERATION, e.getMessage());
        }

    }


}
