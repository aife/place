package com.atexo.docgen.controller.rest.v1.edition;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.editor.model.ChampsFusion;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.domain.generator.services.IDocumentGeneratorService;
import com.atexo.docgen.core.domain.generator.services.impl.DocumentGeneratorFactory;
import com.atexo.docgen.core.domain.monitoring.services.IDocumentMonitoringService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.Get;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.annotation.MultipartConfig;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@MultipartConfig
@RequestMapping("api/v1/edition/document-template")
@Slf4j
public class DocumentTemplateController {

    private final IDocumentGeneratorService iDocumentGeneratorService;
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentMonitoringService iDocumentMonitoringService;
    private final IDocumentConvertorService iDocumentConvertorService;
    private final DocumentGeneratorFactory documentGeneratorFactory;
    private final RestTemplate atexoRestTemplate;

    public DocumentTemplateController(IDocumentGeneratorService docxGeneratorServiceImpl, IDocumentConfigurationService iDocumentConfigurationService, IDocumentMonitoringService iDocumentMonitoringService, IDocumentConvertorService iDocumentConvertorService, DocumentGeneratorFactory documentGeneratorFactory, RestTemplate atexoRestTemplate) {
        this.iDocumentGeneratorService = docxGeneratorServiceImpl;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentMonitoringService = iDocumentMonitoringService;
        this.iDocumentConvertorService = iDocumentConvertorService;
        this.documentGeneratorFactory = documentGeneratorFactory;
        this.atexoRestTemplate = atexoRestTemplate;
    }

    @GetMapping("variables")
    @PreAuthorize("hasAccess()")
    public Map<String, Set<String>> getTemplateVariable(@AuthenticationPrincipal DocumentEditorDetails details) {

        String storagePath = iDocumentConfigurationService.storagePath(details.getId());

        try (FileInputStream inputStream = new FileInputStream(storagePath)) {
            return iDocumentGeneratorService.getFileVariable(inputStream);

        } catch (IOException e) {
            log.error("Erreur lors de la récupération des variables du template {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.TEMPLATE, e.getMessage());
        }
    }

    @PostMapping("generate-test")
    @PreAuthorize("hasAccess()")
    public ResponseEntity<ByteArrayResource> generateTestDocument(@AuthenticationPrincipal DocumentEditorDetails details,
                                                                  @RequestBody List<KeyValueRequest> keyValues) {

        String storagePath = iDocumentConfigurationService.storagePath(details.getId());

        try (FileInputStream inputStream = new FileInputStream(storagePath)) {


            FileGeneratorRequest request = FileGeneratorRequest.builder().stream(inputStream)
                    .keyValues(keyValues.stream().filter(keyValueRequest -> Objects.nonNull(keyValueRequest.getKey()) && Objects.nonNull(keyValueRequest.getValue()))
                            .collect(Collectors.toMap(KeyValueRequest::getKey, KeyValueRequest::getValue)))
                    .build();
            ByteArrayResource file = new ByteArrayResource(iDocumentGeneratorService.generate(request, null, null).toByteArray());

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + details.getDocumentId()
                            + "-" + details.getPlateformeId() + "-" + details.getUser().getName() + ".docx")
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .cacheControl(CacheControl.noCache())
                    .contentLength(file.contentLength())
                    .body(file);
        } catch (Exception e) {
            log.error("Erreur lors de la génération des variables du template {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.TEMPLATE, e.getMessage(), e);
        }
    }

    @PostMapping("generate-pdf-test")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> generatePdfTestRendu(@AuthenticationPrincipal DocumentEditorDetails details,
                                                                  @RequestParam(required = false) String dateFormat,
                                                                  @RequestBody List<KeyValueRequest> keyValues,
                                                                  @RequestParam(required = false) String defaultOnNull) {

        try {
            //récupération du document original
            DocumentModel document = iDocumentMonitoringService.getDocumentModelById(details.getId());
            ByteArrayResource originalFile = iDocumentMonitoringService.getLastRessourceVersion(document, details.getConfiguration());
            //récupération du template correspondant
            ByteArrayResource templateFile = new ByteArrayResource(iDocumentConvertorService.getTemplate(iDocumentMonitoringService.getLastRessourceVersion(document, details.getConfiguration()).getInputStream(), details.getHeadersCallback()).toByteArray());
            if (templateFile == null) {
                templateFile = new ByteArrayResource(IOUtils.toByteArray(originalFile.getInputStream()));
            }
            //Remplacement des champs de fusion par valeurs de test et conversion en pdf

            FileGeneratorRequest request = FileGeneratorRequest.builder()
                    .stream(templateFile.getInputStream())
                    .extension("docx")
                    .keyValues(keyValues.stream()
                            .filter(keyValueRequest -> Objects.nonNull(keyValueRequest.getKey()) &&
                                    Objects.nonNull(keyValueRequest.getValue()))
                            .collect(Collectors.toMap(KeyValueRequest::getKey, KeyValueRequest::getValue)))
                    .build();
            ByteArrayResource file = new ByteArrayResource(documentGeneratorFactory.generate(request, defaultOnNull, dateFormat).toByteArray(), dateFormat);


            file = new ByteArrayResource(iDocumentConvertorService.finalizeDocument(file.getInputStream(), "docx", "pdf").toByteArray());


            String attachement = "test.pdf";
            String contentType = "application/pdf";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + attachement)
                    .contentType(MediaType.parseMediaType(Objects.requireNonNull(contentType)))
                    .cacheControl(CacheControl.noCache())
                    .contentLength(file.contentLength())
                    .body(file);
        } catch (Exception e) {
            log.error("Erreur lors de la génération du fichier test {}", e.getMessage());
            throw new DocGenException(TypeExceptionEnum.TEMPLATE, e.getMessage(), e);
        }
    }

    @PostMapping("preview-champ-complexe")
    @PreAuthorize("hasAccess()")
    public ResponseEntity<ByteArrayResource> previewChampComplexe(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody ChampsFusion champsFusion) {

        if (champsFusion.getBlocUrl() != null) {
            try {
                URI uri = new URI(champsFusion.getBlocUrl());
                HttpHeaders headers = createHeaders(details.getHeadersCallback());
                ResponseEntity<Resource> response = atexoRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), Resource.class);
                ByteArrayResource file = new ByteArrayResource(iDocumentConvertorService.finalizeDocument(response.getBody().getInputStream(), "docx", "pdf").toByteArray());
                String attachement = "test.pdf";
                String contentType = "application/pdf";


                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + attachement)
                        .contentType(MediaType.parseMediaType(Objects.requireNonNull(contentType)))
                        .cacheControl(CacheControl.noCache())
                        .contentLength(file.contentLength())
                        .body(file);
            } catch (Exception e) {
                log.error("Erreur lors de la prévisualisation du champ de fusion complexe {}", e.getMessage());
                throw new DocGenException(TypeExceptionEnum.TEMPLATE, e.getMessage(), e);
            }
        } else {
            throw new DocGenException(TypeExceptionEnum.TEMPLATE, "L'url d'un champ de fusion complexe est obligatoire");
        }


    }

    private HttpHeaders createHeaders(Map<String, String> headers) {
        return new HttpHeaders() {
            {
                if (!CollectionUtils.isEmpty(headers))
                    headers.forEach((key, value) -> set(key, value));
            }
        };
    }
}
