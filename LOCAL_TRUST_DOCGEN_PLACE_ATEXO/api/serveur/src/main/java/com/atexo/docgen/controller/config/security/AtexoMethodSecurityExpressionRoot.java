package com.atexo.docgen.controller.config.security;

import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.ISecurityService;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public class AtexoMethodSecurityExpressionRoot
        extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private final ISecurityService iSecurityService;

    public AtexoMethodSecurityExpressionRoot(Authentication authentication, ISecurityService iSecurityService) {
        super(authentication);
        this.iSecurityService = iSecurityService;
    }

    public boolean canEdit() {
        DocumentEditorDetails editorDetails = ((DocumentEditorDetails) this.getPrincipal());
        return iSecurityService.canEdit(editorDetails);
    }

    public boolean hasAccess() {
        DocumentEditorDetails documentEditorDetails = ((DocumentEditorDetails) this.getPrincipal());
        return iSecurityService.hasAccess(documentEditorDetails);

    }

    public boolean hasMode(String mode) {
        DocumentEditorDetails documentEditorDetails = ((DocumentEditorDetails) this.getPrincipal());
        return iSecurityService.hasMode(documentEditorDetails, mode);

    }


    @Override
    public void setFilterObject(Object o) {

    }

    @Override
    public Object getFilterObject() {
        return null;
    }

    @Override
    public void setReturnObject(Object o) {

    }

    @Override
    public Object getReturnObject() {
        return null;
    }

    @Override
    public Object getThis() {
        return null;
    }
}

