package com.atexo.docgen.controller.rest.v2.mock;

import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;

@RestController("MPEMockControllerV2")
@RequestMapping("api.php/v2")
public class MPEMockController {

    private final RestTemplate restTemplate;
    @Value("${docgen.private-url}")
    private String docgenMockUrl;

    @Value("${docgen.mock-folder}")
    private String docgenMockFolder;

    public MPEMockController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @PostMapping("editor/request")
    public ResponseEntity<String> getEditorToken(@RequestPart @NotNull MultipartFile file, @RequestPart @NotNull DocumentEditorRequest editorRequest) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("file", new FileSystemResource(DocumentUtils.saveFile(file.getInputStream(), docgenMockFolder + File.separator + "a_editer", file.getOriginalFilename(), true)));
        HttpEntity<MultiValueMap<String, Object>> requestEntity =
                com.atexo.docgen.controller.rest.v1.mock.MPEMockController.getEditorConfigMultipart(editorRequest, bodyMap);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v2/document-editor/request", requestEntity, String.class);

    }

}
