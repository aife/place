package com.atexo.docgen.controller.rest.v1.edition;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.model.redac.DocumentXmlModel;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.domain.convertor.model.redac.ValidationClauseRequest;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.monitoring.services.IDocumentMonitoringService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.enums.FileRedacStatusEnum;
import com.atexo.docgen.core.enums.RedacActionEnum;
import com.atexo.docgen.core.enums.UserRoleEnum;
import com.atexo.docgen.core.exceptions.JwtDocGenException;
import com.atexo.docgen.core.port.IDocumentPort;
import com.atexo.docgen.core.port.IDocumentXmlPort;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.annotation.MultipartConfig;
import java.io.*;
import java.nio.file.Files;

@RestController
@MultipartConfig
@RequestMapping("api/v1/edition/document-convertor")
@Slf4j
public class DocumentRedacController {

    private final IDocumentConvertorService iDocumentConvertorService;
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentMonitoringService iDocumentMonitoringService;
    private final IDocumentPort iDocumentPort;
    private final IDocumentXmlPort iDocumentXmlPort;
    @Value("${external-apis.plugins.storage-folder}")
    private String pluginsStoragePath;

    public DocumentRedacController(IDocumentConvertorService iDocumentConvertorService, IDocumentConfigurationService iDocumentConfigurationService, IDocumentPort iDocumentPort, IDocumentMonitoringService iDocumentMonitoringService, IDocumentXmlPort iDocumentXmlPort) {
        this.iDocumentConvertorService = iDocumentConvertorService;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentPort = iDocumentPort;
        this.iDocumentMonitoringService = iDocumentMonitoringService;
        this.iDocumentXmlPort = iDocumentXmlPort;
    }


    @PostMapping("document-configuration")
    @PreAuthorize("canEdit() and hasRole('modification')")
    public DocumentXmlModel updateConfiguration(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody DocxDocument document) {
        log.info("Modification de la configuration du document d'id {} suite à l'action {}", details.getId(), document.getLastAction());
        DocumentXmlModel documentXmlModel = iDocumentConvertorService.getById(details.getId());

        if (canEditDocument(details, document)) {
            String storagePath = iDocumentConfigurationService.storagePath(details.getId());
            File file = new File(storagePath);
            File fileAvanValidation = new File(storagePath + ".brouillon");
            File fileApresValidation = new File(storagePath + ".validate_" + documentXmlModel.getFileType().toLowerCase());
            File fileApresValidationPdf = new File(storagePath + ".validate_pdf");
            try {
                cleanAdditionalFiles(fileApresValidation, fileApresValidationPdf);
                if (document.getLastAction() != null &&
                        RedacActionEnum.VALIDATATION.equals(RedacActionEnum.fromValue(document.getLastAction()))) {
                    validateDocumentXml(details, document, documentXmlModel, file, fileAvanValidation, fileApresValidation, fileApresValidationPdf);
                    iDocumentPort.setNewVersion(details.getId(), true);
                } else if (Files.exists(fileAvanValidation.toPath()) && document.getLastAction() != null &&
                        RedacActionEnum.INVALIDATION.equals(RedacActionEnum.fromValue(document.getLastAction()))) {
                    replaceFile(details, file, fileAvanValidation);
                    iDocumentPort.setNewVersion(details.getId(), true);
                }
                documentXmlModel = iDocumentConvertorService.modifyDocumentXml(details.getId(), document);

            } catch (Exception e) {
                log.error("Erreur lors de la validation du document {} => {}", details.getId(), e.getMessage());
                throw new JwtDocGenException("La validation de document est impossible", e);
            }
        }
        return documentXmlModel;
    }

    private void replaceFile(DocumentEditorDetails details, File fileToReplace, File newFile) throws IOException {
        iDocumentMonitoringService.addNewVersion(details.getId());
        Files.deleteIfExists(fileToReplace.toPath());
        Files.move(newFile.toPath(), fileToReplace.toPath());
    }

    private void validateDocumentXml(DocumentEditorDetails details, DocxDocument document, DocumentXmlModel documentXmlModel, File file, File fileAvanValidation, File fileApresValidation, File fileApresValidationPdf) throws IOException {
        replaceFile(details, fileAvanValidation, file);
        try (FileInputStream inputStream = new FileInputStream(fileAvanValidation); ByteArrayOutputStream outputStream = iDocumentConvertorService.finalizeDocument(inputStream, documentXmlModel.getFileType(), documentXmlModel.getFileType())) {
            Files.deleteIfExists(fileApresValidation.toPath());
            Files.copy(new ByteArrayInputStream(outputStream.toByteArray()), fileApresValidation.toPath());
            Files.deleteIfExists(file.toPath());
            Files.copy(new ByteArrayInputStream(outputStream.toByteArray()), file.toPath());
            if ("pdf".equalsIgnoreCase(document.getOutputFormat())) {
                try (FileInputStream inputStreamDocx = new FileInputStream(file)) {
                    Files.copy(new ByteArrayInputStream(iDocumentConvertorService.convertDocument(inputStreamDocx, documentXmlModel.getFileType(), "pdf").toByteArray()),
                            fileApresValidationPdf.toPath());
                }
            }
        }
    }

    private void cleanAdditionalFiles(File fileApresValidation, File fileApresValidationPdf) throws IOException {
        Files.deleteIfExists(fileApresValidation.toPath());
        Files.deleteIfExists(fileApresValidationPdf.toPath());
    }

    @PostMapping("chapitre/{chapitreNumero}/convert-clause")
    @PreAuthorize("canEdit() and hasRole('modification')")
    public String getClauseDocument(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody ValidationClauseRequest clauseRequest,
                                    @PathVariable String chapitreNumero, @RequestParam boolean fromFile) {
        if (clauseRequest.getClause() != null) {
            ByteArrayOutputStream convertedClause = iDocumentConvertorService.convertClause(clauseRequest.getClause(), fromFile, chapitreNumero, details.getId());
            DocumentUtils.saveFile(new ByteArrayInputStream(convertedClause.toByteArray()), pluginsStoragePath + File.separatorChar + details.getId() + File.separatorChar + chapitreNumero, clauseRequest.getClause().getRef(), true);
        }
        DocxDocument document = clauseRequest.getDocument();
        if (document != null && canEditDocument(details, document)) {
            iDocumentConvertorService.modifyDocumentXml(details.getId(), document);
        }
        return "OK";
    }

    private boolean canEditDocument(DocumentEditorDetails details, DocxDocument document) {
        if (document.getStatus() != null &&
                FileRedacStatusEnum.VALIDER.equals(FileRedacStatusEnum.fromValue(document.getStatus()))
                && !details.getUser().getRoles().contains(UserRoleEnum.VALIDATION.getValue())) {
            log.error("La validation de document non autorisée par {} du document {}", details.getUser().getName(), details.getId());
            throw new JwtDocGenException("La validation de document non autorisée");
        }
        return true;
    }


    @GetMapping("chapitre/{chapitreNumero}/clause/{clauseRef}")
    @PreAuthorize("canEdit() and hasRole('modification')")
    public ResponseEntity<ByteArrayResource> getClauseDocument(@AuthenticationPrincipal DocumentEditorDetails details,
                                                               @PathVariable String chapitreNumero,
                                                               @PathVariable String clauseRef) throws IOException {
        File convertedClause = new File(pluginsStoragePath + File.separatorChar + details.getId() + File.separatorChar + chapitreNumero, clauseRef);
        ByteArrayResource file = new ByteArrayResource(IOUtils.toByteArray(Files.newInputStream(convertedClause.toPath())));
        Files.deleteIfExists(convertedClause.toPath());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + convertedClause.getName() + ".docx")
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);

    }

    @GetMapping("document/{format}")
    @PreAuthorize("hasAccess()")
    public ResponseEntity<ByteArrayResource> getDocument(@AuthenticationPrincipal DocumentEditorDetails details,
                                                         @PathVariable String format) throws IOException {
        String fileName = details.getId() + "." + format;
        File ressourcesFile = new File(iDocumentConfigurationService.storagePath(fileName));

        ByteArrayResource file = new ByteArrayResource(Files.readAllBytes(ressourcesFile.toPath()));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);

    }


    @PostMapping("add-version")
    @PreAuthorize("hasAccess()")
    public boolean addVersion(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody DocxDocument document) {
        boolean result = iDocumentMonitoringService.addNewVersion(details.getId());
        if (result) {
            iDocumentXmlPort.modifyDocumentXml(details.getId(), document);
        }
        return result;
    }

}
