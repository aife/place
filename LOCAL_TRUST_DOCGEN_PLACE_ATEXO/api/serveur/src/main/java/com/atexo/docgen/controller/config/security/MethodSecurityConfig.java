package com.atexo.docgen.controller.config.security;

import com.atexo.docgen.core.domain.security.services.ISecurityService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {


    private final ISecurityService iSecurityService;

    public MethodSecurityConfig(ISecurityService iSecurityService) {
        this.iSecurityService = iSecurityService;
    }


    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        return new AtexoMethodSecurityExpressionHandler(iSecurityService);
    }
}
