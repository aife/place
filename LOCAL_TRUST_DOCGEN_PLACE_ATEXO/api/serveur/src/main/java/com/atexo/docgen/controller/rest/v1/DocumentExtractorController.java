package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.extractor.model.ExcelConfiguration;
import com.atexo.docgen.core.domain.extractor.model.Feuille;
import com.atexo.docgen.core.domain.extractor.model.FeuillePositionValue;
import com.atexo.docgen.core.domain.extractor.model.analyse.AnalyseOffre;
import com.atexo.docgen.core.domain.extractor.services.IDocumentExtractorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@MultipartConfig
@RequestMapping("api/v1/document-extractor")
@Slf4j
public class DocumentExtractorController {

    private final IDocumentExtractorService documentExtractorService;

    private final DocumentGeneratorController documentGeneratorController;

    public DocumentExtractorController(IDocumentExtractorService documentExtractorService,
                                       DocumentGeneratorController documentGeneratorController) {
        this.documentGeneratorController = documentGeneratorController;
        this.documentExtractorService = documentExtractorService;
    }

    @PostMapping("extract-xlsx")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> extract(
            MultipartHttpServletRequest request,
            @RequestParam(required = false) String outputFileName,
            @RequestParam(required = false) String dateFormat,
            @RequestPart @NotNull ExcelConfiguration configuration,
            @RequestPart @NotNull MultipartFile template
    ) throws IOException {
        log.info("Lancement du process d'extraction de données");
        List<MultipartFile> fichiers = new ArrayList<>();
        try {
            Iterator<String> itr = request.getFileNames();
            while (itr.hasNext()) {
                String uploadedFile = itr.next();
                if (uploadedFile != null && uploadedFile.startsWith("fichiers[") && uploadedFile.endsWith("]")) {
                    fichiers.add(request.getFile(uploadedFile));
                }
            }
        } catch (Exception e) {
            log.error("Error lors de la récupération du fichiers[] => {}", e.getMessage());
        }

        ByteArrayResource templateResult = documentGeneratorController.generate(template, configuration.getKeyValues(), false, null, outputFileName, dateFormat).getBody();
        ByteArrayOutputStream result = documentExtractorService.extract(fichiers, templateResult, configuration);
        if (outputFileName == null)
            outputFileName = "output";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + outputFileName + ".xlsx")
                .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                .cacheControl(CacheControl.noCache())
                .contentLength(result.toByteArray().length)
                .body(new ByteArrayResource(result.toByteArray()));
    }

    @PostMapping("analyse-report")
    @ResponseBody
    public AnalyseOffre analyseReport(@RequestPart @NotNull Configuration configuration,
                                      @RequestPart @NotNull MultipartFile document
    ) throws IOException {
        log.info("Lancement du process d'extraction de l'analyse");
        return documentExtractorService.extractAnalyseStatus(document.getInputStream(), configuration);
    }

    @PostMapping("xlsx-data")
    @ResponseBody
    public List<FeuillePositionValue> extractData(@RequestPart @NotNull List<FeuillePositionValue> positions,
                                                  @RequestPart @NotNull MultipartFile document
    ) throws IOException {
        log.info("Lancement du process d'extraction du fichier à partir des positions");
        return documentExtractorService.extractXlsx(document.getInputStream(), positions);
    }

    @PostMapping("xlsx-sheet")
    @ResponseBody
    public List<Feuille> getSheetNames(@RequestPart @NotNull MultipartFile document) throws IOException {
        log.info("Lancement du process d'extraction des noms de feuilles");
        return documentExtractorService.getSheetNames(document.getInputStream());
    }

}
