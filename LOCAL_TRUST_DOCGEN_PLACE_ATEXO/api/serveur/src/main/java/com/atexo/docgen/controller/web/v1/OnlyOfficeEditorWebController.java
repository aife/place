package com.atexo.docgen.controller.web.v1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("editor")
@Slf4j
public class OnlyOfficeEditorWebController {
    @Value("${docgen.public-url}")
    private String publicUrl;

    @GetMapping
    public void getEditorPage(HttpServletResponse httpServletResponse, @RequestParam(required = false) String token) {
        log.info("Start getEditorPage avec le token {}", token);
        String url = publicUrl + "/edition-en-ligne/bienvenue";
        if (token != null) {
            url += "?token=" + token;
        }
        httpServletResponse.setHeader("Location", url);
        httpServletResponse.setStatus(302);
    }


}
