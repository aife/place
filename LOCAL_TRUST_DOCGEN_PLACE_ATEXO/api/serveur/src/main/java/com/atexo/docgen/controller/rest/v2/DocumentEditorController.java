package com.atexo.docgen.controller.rest.v2;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.services.IDocumentEditorService;
import com.atexo.docgen.core.enums.FileModeEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.exceptions.MauvaisParametreException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController("DocumentEditorControllerV2")
@MultipartConfig
@RequestMapping("api/v2/document-editor")
@Slf4j
public class DocumentEditorController {

    private final IDocumentEditorService iDocumentEditorService;
    private final IDocumentConfigurationService iDocumentConfigurationService;

    public DocumentEditorController(IDocumentConfigurationService iDocumentConfigurationService, IDocumentEditorService iDocumentEditorService) {
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentEditorService = iDocumentEditorService;
    }

    @PostMapping("request")
    public String requestToken(@RequestPart @NotNull MultipartFile file, @RequestPart @NotNull DocumentEditorRequest editorRequest,
                               @RequestParam(required = false) Boolean override) throws IOException {
        if (editorRequest.getMode() == null || editorRequest.getUser() == null) {
            log.error("Aucun attribut de la requete doit être null");
            throw new MauvaisParametreException(TypeExceptionEnum.EDITOR, "Aucun atribut de la requete doit être null");
        }

        if (FileModeEnum.REDAC.equals(FileModeEnum.fromValue(editorRequest.getMode()))) {
            log.error("Ce mode n'est pas autorisé avec ce service");
            throw new MauvaisParametreException(TypeExceptionEnum.EDITOR, "Mode Redac interdit avec ce service");
        }
        String curExt = DocumentUtils.getFileExtension(file.getOriginalFilename());
        if (!iDocumentConfigurationService.getEditedExts().contains(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.EDITOR);

        }
        if (editorRequest.getDocumentTitle() == null) {
            editorRequest.setDocumentTitle(file.getOriginalFilename());
        }
        if (editorRequest.getDocumentId() == null) {
            editorRequest.setDocumentId(file.getOriginalFilename());
        }
        return iDocumentEditorService.getToken(file.getInputStream(), editorRequest, curExt, override).getToken();

    }


}
