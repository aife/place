package com.atexo.docgen.config;

import com.atexo.docgen.controller.config.ExceptionEnum;
import com.atexo.docgen.controller.config.WsMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {


    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<WsMessage> handleClientError(HttpClientErrorException ex) {
        log.error("handleClientError {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getResponseBodyAsString())
                .code(ex.getStatusCode().value())
                .build();
        return new ResponseEntity<>(wsMessage, ex.getStatusCode());
    }


    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<WsMessage> handleResourceNotFoundException(MethodArgumentNotValidException e) {
        String message = e.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .sorted((s, anotherString) -> s != null ? s.compareTo(anotherString) : 0)
                .collect(Collectors.joining(", "));
        log.error("handleResourceNotFoundException {}", message);
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }

    @ExceptionHandler({MissingRequestHeaderException.class})
    public ResponseEntity<WsMessage> handleResourceMissingRequestHeaderException(MissingRequestHeaderException e) {

        final String message = e.getHeaderName() + " est manquant";
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        log.error("handleResourceMissingRequestHeaderException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }

    @ExceptionHandler({UnsatisfiedServletRequestParameterException.class})
    public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(UnsatisfiedServletRequestParameterException e) {

        final String message = String.join(", ", e.getParamConditions()) + " sont manquant(e)s";
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }
    @ExceptionHandler({MissingServletRequestPartException.class})
    public ResponseEntity<WsMessage> handleMissingServletRequestPartException(MissingServletRequestPartException e) {

        final String message = format("Le paramètre '%s' est obligatoire", e.getRequestPartName());
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(MissingServletRequestParameterException e) {

        final String message = format("Le paramètre '%s' est obligatoire", e.getParameterName());
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<WsMessage> handleException(Exception ex) {
        StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        ex.printStackTrace(pw);
        log.error("handleException {}", ex.getMessage());
        log.error("stackException {}", writer.toString());
        WsMessage wsMessage = WsMessage.builder()
                .message("Veuillez contacter un administrateur")
                .code(HttpStatus.BAD_REQUEST.value())
                .type(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }

}
