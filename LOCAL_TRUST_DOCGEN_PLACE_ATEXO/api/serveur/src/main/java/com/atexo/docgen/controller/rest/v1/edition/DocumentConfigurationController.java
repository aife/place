package com.atexo.docgen.controller.rest.v1.edition;

import com.atexo.docgen.core.domain.editor.model.EditorConfigRequest;
import com.atexo.docgen.core.domain.editor.services.IDocumentEditorService;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.port.IOnlyOfficePort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.annotation.MultipartConfig;
import java.util.Map;

@RestController
@MultipartConfig
@RequestMapping("api/v1/edition/document")
@Slf4j
public class DocumentConfigurationController {
    private final IDocumentEditorService iDocumentEditorService;
    private final IOnlyOfficePort iOnlyOfficePort;


    public DocumentConfigurationController(IDocumentEditorService iDocumentEditorService,
                                           IOnlyOfficePort iOnlyOfficePort) {
        this.iDocumentEditorService = iDocumentEditorService;
        this.iOnlyOfficePort = iOnlyOfficePort;
    }


    @GetMapping("configuration")
    @PreAuthorize("hasAccess()")
    public EditorConfigRequest getEditorPage(@AuthenticationPrincipal DocumentEditorDetails details) {
        log.info("Start getEditorPage avec l'utilisateur {} => id {}", details.getUser().getName(), details.getId());
        return EditorConfigRequest.builder().config(iDocumentEditorService.getFileModel(details))
                .apis(iOnlyOfficePort.getApis())
                .build();
    }


    @GetMapping("{key}/force-save")
    @PreAuthorize("canEdit() and (hasRole('modification') or " +
            "hasMode('edit'))")
    public Integer forceSave(@AuthenticationPrincipal DocumentEditorDetails details, @PathVariable String key) {
        log.info("Start forceSave avec l'utilisateur {} => token {}", details.getUser().getName(), details.getId());
        return iOnlyOfficePort.forceSave(key);
    }

    @GetMapping("surcharge-libelle")
    public Map<String, String> getSurchargesLibelles(@AuthenticationPrincipal DocumentEditorDetails details) {
        return iOnlyOfficePort.getSurchargesLibelles();
    }
}
