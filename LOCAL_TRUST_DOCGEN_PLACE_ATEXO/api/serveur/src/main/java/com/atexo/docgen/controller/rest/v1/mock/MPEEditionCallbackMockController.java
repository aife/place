package com.atexo.docgen.controller.rest.v1.mock;


import com.atexo.docgen.core.domain.editor.model.TrackDocumentResponse;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("api.php/v1/document-edition/callback")
public class MPEEditionCallbackMockController {

    @PostMapping
    public TrackDocumentResponse trackDocument(@RequestBody FileStatus status) throws IOException {
        log.info("callback document {}", status.toString());
        return TrackDocumentResponse.builder().error("0").build();
    }

}
