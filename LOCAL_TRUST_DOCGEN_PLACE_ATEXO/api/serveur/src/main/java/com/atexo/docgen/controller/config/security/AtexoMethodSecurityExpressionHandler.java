package com.atexo.docgen.controller.config.security;

import com.atexo.docgen.core.domain.security.services.ISecurityService;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

public class AtexoMethodSecurityExpressionHandler
        extends DefaultMethodSecurityExpressionHandler {
    private final AuthenticationTrustResolver trustResolver =
            new AuthenticationTrustResolverImpl();
    private final ISecurityService iSecurityService;

    public AtexoMethodSecurityExpressionHandler(ISecurityService iSecurityService) {
        this.iSecurityService = iSecurityService;
    }

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
            Authentication authentication, MethodInvocation invocation) {
        AtexoMethodSecurityExpressionRoot root =
                new AtexoMethodSecurityExpressionRoot(authentication, iSecurityService);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setDefaultRolePrefix("");
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
    }
}
