package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.analyse.ConvertResult;
import com.atexo.docgen.core.domain.convertor.model.analyse.Lot;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.convertor.services.IExcelConvertorService;
import com.atexo.docgen.core.domain.generator.model.FileGeneratorRequest;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.domain.generator.services.impl.XlsxGeneratorServiceImpl;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.getObjectMapper;

@RestController
@MultipartConfig
@RequestMapping("api/v1/document-convertor")
@Slf4j
public class DocumentConvertorController {

    private final IExcelConvertorService iExcelConvertorService;
    private final IDocumentConvertorService iDocumentConvertorService;
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final DocumentGeneratorController documentGeneratorController;
    private final XlsxGeneratorServiceImpl xlsxGeneratorService;

    public DocumentConvertorController(IExcelConvertorService iExcelConvertorService, IDocumentConvertorService iDocumentConvertorService, IDocumentConfigurationService iDocumentConfigurationService,
                                       DocumentGeneratorController documentGeneratorController, XlsxGeneratorServiceImpl xlsxGeneratorService) {
        this.iExcelConvertorService = iExcelConvertorService;
        this.iDocumentConvertorService = iDocumentConvertorService;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.documentGeneratorController = documentGeneratorController;
        this.xlsxGeneratorService = xlsxGeneratorService;
    }


    @PostMapping("convert-analyse")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> convertOffreToXlsx(@RequestParam(required = false) String outputFileName,
                                                                @RequestParam(required = false) String dateFormat,
                                                                @RequestPart @NotEmpty @NotNull MultipartFile template,
                                                                @RequestPart @NotNull Consultation meta) throws IOException {
        validateAnalyse(template, meta);
        ByteArrayResource templateResult = new ByteArrayResource(IOUtils.toByteArray(template.getInputStream()));

        ConvertResult convert = iExcelConvertorService.convertAnalyse(templateResult.getInputStream(), meta);


        if (convert == null || convert.getConvertedDocument() == null || convert.getConfiguration() == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        ByteArrayResource file = new ByteArrayResource(convert.getConvertedDocument().toByteArray());
        try {
            Map<String, Object> keyValues = getCreatingKeyValues(meta);
            ByteArrayOutputStream xlsx = xlsxGeneratorService.generate(FileGeneratorRequest.builder().keyValues(keyValues).extension("xlsx")
                    .stream(file.getInputStream()).build(), null, dateFormat);
            file = new ByteArrayResource(xlsx.toByteArray());

        } catch (Exception e) {
            log.error("Erreur lors du remplacement des variables {}", e.getMessage());
        }

        if (outputFileName == null)
            outputFileName = "output";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
        zipOutputStream.putNextEntry(new ZipEntry(outputFileName + ".xlsx"));
        IOUtils.copy(file.getInputStream(), zipOutputStream);
        zipOutputStream.closeEntry();

        String s = getObjectMapper().writeValueAsString(convert.getConfiguration());
        ByteArrayResource reportOutputStream = new ByteArrayResource(s.getBytes(StandardCharsets.UTF_8));

        zipOutputStream.putNextEntry(new ZipEntry(outputFileName + ".json"));
        IOUtils.copy(reportOutputStream.getInputStream(), zipOutputStream);
        zipOutputStream.closeEntry();


        zipOutputStream.finish();
        zipOutputStream.flush();
        ByteArrayResource out = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        return getByteArrayResourceResponseEntity(outputFileName, byteArrayOutputStream, bufferedOutputStream, zipOutputStream, out);
    }

    @PostMapping("convert-xml")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> convertXmlToDocx(@RequestParam(required = false) String outputFileName, @RequestPart @NotEmpty @NotNull MultipartFile xml,
                                                              @RequestParam(required = false) String dateFormat,
                                                              @RequestPart @NotEmpty @NotNull MultipartFile template,
                                                              @RequestPart @NotNull List<KeyValueRequest> keyValues,
                                                              @RequestParam(required = false) String defaultOnNull) throws IOException {
        validate(xml, template);
        ByteArrayResource templateResult = null;
        try {

            templateResult = documentGeneratorController.generate(template, keyValues, false, defaultOnNull, outputFileName, dateFormat).getBody();

        } catch (Exception e) {
            log.error("Erreur lors du remplacement des variables {}", e.getMessage());
        }
        if (templateResult == null) {
            templateResult = new ByteArrayResource(IOUtils.toByteArray(template.getInputStream()));
        }

        DocxDocument docxDocument = iDocumentConvertorService.getDocxDocument(xml.getInputStream());
        ByteArrayOutputStream convert = iDocumentConvertorService.convert(templateResult.getInputStream(), docxDocument);
        ByteArrayResource file = new ByteArrayResource(convert.toByteArray());
        ByteArrayResource out;

        if (outputFileName == null)
            outputFileName = "output";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
        zipOutputStream.putNextEntry(new ZipEntry(outputFileName + ".docx"));
        IOUtils.copy(file.getInputStream(), zipOutputStream);
        zipOutputStream.closeEntry();

        ByteArrayOutputStream xmlOutputStream = iDocumentConvertorService.getXml(docxDocument);
        if (xmlOutputStream == null) {
            throw new DocGenException(TypeExceptionEnum.CONVERSION, "Le fichier xml est vide");
        }
        ByteArrayResource xmlFile = new ByteArrayResource(xmlOutputStream.toByteArray());
        zipOutputStream.putNextEntry(new ZipEntry(outputFileName + ".xml"));
        IOUtils.copy(xmlFile.getInputStream(), zipOutputStream);
        zipOutputStream.closeEntry();


        zipOutputStream.finish();
        zipOutputStream.flush();
        out = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        return getByteArrayResourceResponseEntity(outputFileName, byteArrayOutputStream, bufferedOutputStream, zipOutputStream, out);

    }

    @PostMapping("convert-modele-administrable")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> finalizeDocument(@RequestPart @NotEmpty @NotNull MultipartFile modele,
                                                              @RequestPart @NotNull Map<String, String> headers) throws IOException {
        String inputFormat = DocumentUtils.getFileExtension(modele.getOriginalFilename());
        if (!inputFormat.equals("docx")) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le document doit être d\'extension docx");
        }

        ByteArrayResource file = new ByteArrayResource(iDocumentConvertorService.getTemplate(modele.getInputStream(), headers).toByteArray());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=template-" + modele.getOriginalFilename())
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);

    }


    @PostMapping("{outputFormat}/finalize")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> finalizeDocument(@RequestParam(required = false) String outputFileName,
                                                              @RequestParam(required = false) String inputFormat,
                                                              @PathVariable String outputFormat,
                                                              @RequestPart @NotEmpty @NotNull MultipartFile document) throws IOException {
        if (inputFormat == null)
            inputFormat = DocumentUtils.getFileExtension(document.getOriginalFilename());

        ByteArrayResource file = new ByteArrayResource(iDocumentConvertorService.finalizeDocument(document.getInputStream(), inputFormat, outputFormat).toByteArray());
        if (outputFileName == null)
            outputFileName = "output." + outputFormat;
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + outputFileName)
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);

    }


    private void validate(MultipartFile xml, MultipartFile template) {
        String curExt = DocumentUtils.getFileExtension(xml.getOriginalFilename());
        if (!iDocumentConfigurationService.getConvertExts().contains(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le fichier XML doit être d\'extension xml");
        }
        curExt = DocumentUtils.getFileExtension(template.getOriginalFilename());
        if (!"docx".equals(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le template doit être d\'extension docx");
        }
    }


    private void validateAnalyse(MultipartFile xlsx, Consultation consultation) {
        if (consultation == null || xlsx == null) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le fichier meta/xlsx ne doit pas être null");
        }
        String curExt = DocumentUtils.getFileExtension(xlsx.getOriginalFilename());
        if (!"xlsx".equals(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le fichier xlsx doit être d\'extension xlsx");
        }
        if (consultation.getIntitule() == null) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Intitulé est vide");
        }
        if (consultation.getReference() == null) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Référence est vide");
        }

        if (CollectionUtils.isEmpty(consultation.getLots()) && CollectionUtils.isEmpty(consultation.getCriteres())) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Il y a aucun critère défini");
        } else if (!CollectionUtils.isEmpty(consultation.getLots())) {
            for (Lot lot : consultation.getLots()) {
                if (CollectionUtils.isEmpty(lot.getCriteres()))
                    throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Il y a aucun critère défini pour le lot " + lot.getNumero());
            }
        }

    }


    private ResponseEntity<ByteArrayResource> getByteArrayResourceResponseEntity(String outputFileName, ByteArrayOutputStream byteArrayOutputStream, BufferedOutputStream bufferedOutputStream, ZipOutputStream zipOutputStream, ByteArrayResource out) {
        IOUtils.closeQuietly(zipOutputStream);
        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);
        outputFileName += ".zip";
        MediaType mediaType = MediaType.parseMediaType("application/zip");

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + outputFileName)
                .contentType(mediaType)
                .cacheControl(CacheControl.noCache())
                .contentLength(out.contentLength())
                .body(out);
    }


    private Map<String, Object> getCreatingKeyValues(Consultation consultation) {
        try {
            ObjectMapper objectMapper = getObjectMapper();
            String json = objectMapper.writeValueAsString(consultation);
            return getKeys(objectMapper.readTree(json));
        } catch (IOException e) {
            log.error("Erreur lors de l'extraction des variables {}", e.getMessage());
            return new HashMap<>();
        }
    }

    private Map<String, Object> getKeys(JsonNode jsonNode) {
        Map<String, Object> keyValueRequests = new HashMap<>();
        ObjectNode objectNode = (ObjectNode) jsonNode;
        Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
        while (iter.hasNext()) {
            Map.Entry<String, JsonNode> entry = iter.next();
            keyValueRequests.put(entry.getKey(), entry.getValue());
        }
        return keyValueRequests;
    }


}
