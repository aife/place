package com.atexo.docgen.controller.rest.v1.edition;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.editor.model.DocumentModel;
import com.atexo.docgen.core.domain.editor.model.TrackDocumentRequest;
import com.atexo.docgen.core.domain.editor.model.TrackDocumentResponse;
import com.atexo.docgen.core.domain.editor.services.IDocumentEditorService;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.core.exceptions.OnlyOfficeException;
import com.atexo.docgen.core.port.IOnlyOfficePort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.Map;

@RestController
@RequestMapping("api/v1/edition/callback")
@Slf4j
public class OnlyOfficeCallbackController {
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentEditorService iDocumentEditorService;
    private final RestTemplate restTemplate;

    private final IOnlyOfficePort iOnlyOfficePort;

    public OnlyOfficeCallbackController(IDocumentEditorService iDocumentEditorService, IDocumentConfigurationService iDocumentConfigurationService, IOnlyOfficePort iOnlyOfficePort, RestTemplate restTemplate) {
        this.iDocumentEditorService = iDocumentEditorService;
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iOnlyOfficePort = iOnlyOfficePort;
        this.restTemplate = restTemplate;
    }

    @PostMapping("track")
    @PreAuthorize("hasAccess()")
    public TrackDocumentResponse track(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody TrackDocumentRequest request) {

        try {
            String id = details.getId();

            DocumentModel document = iDocumentEditorService.getById(id);
            String storagePath = iDocumentConfigurationService.storagePath(document.getId());

            FileStatusEnum newStatusEnum = FileStatusEnum.fromValue(request.getStatus());
            log.info("Process track du document {}(key docgen:{} / key onlyoffice: {}) avec la plateforme {} et le status {}", document.getDocumentId(),
                    document.getKey(), request.getKey(),
                    document.getPlateformeId(), newStatusEnum.name());
            int saved = 0;
            if (mustSave(request)) {
                saved = saveReceivedFileFromOnlyOffice(id, request, document, storagePath, saved);
            }
            if (saved == 0) {
                if (!document.getKey().equals(request.getKey())) {
                    try {
                        iOnlyOfficePort.info(document.getKey());
                        if (FileStatusEnum.SAVED_AND_CLOSED.equals(newStatusEnum)) {
                            document = iDocumentEditorService.modifyDocumentStatusById(id, FileStatusEnum.EDITED_AND_SAVED.name(), document.getMode());
                        }
                    } catch (OnlyOfficeException e) {
                        log.info("Erreur lors de la récupération d'info pour le document {} (key :{})", document.getDocumentId(), document.getKey());
                        document = updateStatusFromOnlyoffice(id, document, newStatusEnum);
                    }
                } else {
                    document = updateStatusFromOnlyoffice(id, document, newStatusEnum);
                }


            } else {
                document = iDocumentEditorService.modifyDocumentStatusById(id, FileStatusEnum.ERROR_WHILE_SAVING.name(), document.getMode());

            }
            sendCallbackUri(details, document);
            return TrackDocumentResponse.builder().
                    error(String.valueOf(saved)).
                    build();
        } catch (Exception e) {
            log.error("Erreur lors du traitement du callback {}", e.getMessage());
            return TrackDocumentResponse.builder().
                    error(String.valueOf(-1)).
                    build();
        }

    }

    HttpHeaders createHeaders(Map<String, String> headers) {
        return new HttpHeaders() {
            {
                if (!CollectionUtils.isEmpty(headers))
                    headers.forEach((key, value) -> set(key, value));
            }
        };
    }

    private void sendCallbackUri(DocumentEditorDetails details, DocumentModel document) {
        String callback = details.getCallback();
        if (!StringUtils.isEmpty(callback)) {
            try {
                FileStatus fileStatus = FileStatus.builder()
                        .documentId(document.getDocumentId())
                        .key(document.getKey())
                        .status(document.getStatus())
                        .modificationDate(document.getModificationDate())
                        .creationDate(document.getCreationDate())
                        .version(document.getVersion())
                        .mode(document.getMode())
                        .redacStatus(document.getRedacStatus())
                        .redacAction(document.getRedacAction())
                        .users(details.getUsers())
                        .build();
                ResponseEntity<TrackDocumentResponse> responseEntity =
                        restTemplate.postForEntity(callback, new HttpEntity<>(fileStatus, createHeaders(details.getHeadersCallback())), TrackDocumentResponse.class);
                log.info("Callback return avec l'erreur {}", responseEntity.getStatusCode());
                if (responseEntity.getStatusCodeValue() == 200 && responseEntity.getBody() != null &&
                        "0".equals(responseEntity.getBody().getError())) {
                    log.info("Aquittement document {} de la plateforme {} avec succès ", details.getDocumentId(), details.getPlateformeId());

                } else {
                    log.info("Aquittement document {} de la plateforme {} avec erreur ", details.getDocumentId(), details.getPlateformeId());
                }
            } catch (Exception e) {
                log.error("Aquittement document {} de la plateforme {} avec erreur  {}", details.getDocumentId(), details.getPlateformeId(), e.getMessage());
            }
        }
    }


    private DocumentModel updateStatusFromOnlyoffice(String id, DocumentModel document, FileStatusEnum newStatusEnum) {
        if (document.isNewVersion() && FileStatusEnum.CLOSED_WITHOUT_EDITING.equals(newStatusEnum)) {
            log.info("le  statut du document passe de {} à SAVED_AND_CLOSED suite à une nouvelle version", newStatusEnum.name());
            return iDocumentEditorService.modifyDocumentStatusById(id, FileStatusEnum.SAVED_AND_CLOSED.name(), document.getMode());
        } else {
            return iDocumentEditorService.modifyDocumentStatusById(id, newStatusEnum.name(), document.getMode());
        }
    }

    private boolean mustSave(TrackDocumentRequest request) {
        return request.getStatus() == FileStatusEnum.EDITED_AND_SAVED.getCode() ||
                request.getStatus() == FileStatusEnum.SAVED_AND_CLOSED.getCode() ||
                request.getStatus() == FileStatusEnum.CORRUPTED.getCode();
    }

    private int saveReceivedFileFromOnlyOffice(String id, TrackDocumentRequest request, DocumentModel document, String storagePath, int saved) {
        try {
            File toSave = new File(storagePath);
            iDocumentConfigurationService.downloadToFile(request.getUrl(), toSave);
            log.info("Téléchargement du fichier a fini avec succès {}", request.getUrl());
            String key = iDocumentEditorService.setDocumentKey(id, document);
            log.info("Nouvelle key {} pour le document {} de la plateforme {}", key, document.getDocumentId(), document.getPlateformeId());
        } catch (Exception ex) {
            saved = 1;
            log.error("Erreur lors de la sauvegarde du document {} : {}", request.getKey(), ex.getMessage());
        }
        return saved;
    }


}
