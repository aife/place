package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.config.http_logging.LogEntryExit;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.util.List;

@RestController
@MultipartConfig
@RequestMapping("api/v1/document-monitoring")
@Slf4j
public class DocumentMonitoringController {

    private final DocumentSuiviController documentSuiviController;

    public DocumentMonitoringController(DocumentSuiviController documentSuiviController) {
        this.documentSuiviController = documentSuiviController;
    }

    @GetMapping("status")
    @PreAuthorize("hasAccess()")
    public ResponseEntity<ByteArrayResource> getStatus(@AuthenticationPrincipal DocumentEditorDetails details) throws IOException {
        return documentSuiviController.getStatus(details);
    }


    @LogEntryExit(skip = true)
    @GetMapping("plateforme-status")
    public List<FileStatus> getPlateformeFileStatus(@RequestParam String plateformeId) throws IOException {

        return documentSuiviController.getPlateformeFileStatus(plateformeId);

    }

    @GetMapping("download-list")
    @LogEntryExit(skip = true)
    public List<String> getPlateformeFileToDownload(@RequestParam String plateformeId) {
        return documentSuiviController.getPlateformeFileToDownload(plateformeId);
    }

    @PostMapping("modify-status")
    @PreAuthorize("hasAccess()")
    public String modifyStatus(@AuthenticationPrincipal DocumentEditorDetails details, @RequestBody String status) {
        return documentSuiviController.modifyStatus(details, status);
    }


}
