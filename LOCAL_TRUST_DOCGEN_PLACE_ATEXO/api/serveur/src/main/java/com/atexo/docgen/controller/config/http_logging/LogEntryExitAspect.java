package com.atexo.docgen.controller.config.http_logging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.boot.logging.LogLevel;
import org.springframework.core.io.AbstractResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Method;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import static com.atexo.docgen.core.utilitaire.word.WordDocumentUtils.getObjectMapper;

@Aspect
@Component
@Slf4j
public class LogEntryExitAspect {

    @Around("within(com.atexo.docgen.controller.rest..*)")
    public Object logMessage(ProceedingJoinPoint point) throws Throwable {
        var codeSignature = (CodeSignature) point.getSignature();
        var methodSignature = (MethodSignature) point.getSignature();
        Method method = methodSignature.getMethod();
        var annotation = method.getAnnotation(LogEntryExit.class);
        boolean skip = annotation != null && annotation.skip();
        if (!skip) {
            LogLevel level = annotation != null ? annotation.value() : LogLevel.INFO;
            ChronoUnit unit = annotation != null ? annotation.unit() : ChronoUnit.SECONDS;
            boolean showArgs = annotation == null || annotation.showArgs();
            boolean showResult = annotation != null && annotation.showResult();
            boolean showExecutionTime = annotation == null || annotation.showExecutionTime();
            String methodName = method.getName();
            Object[] methodArgs = point.getArgs();
            String[] methodParams = codeSignature.getParameterNames();
            logMessage(log, level, entry(methodName, showArgs, methodParams, methodArgs));
            var start = Instant.now();
            var response = point.proceed();
            var end = Instant.now();
            var duration = String.format("%s %s", unit.between(start, end), unit.name().toLowerCase());
            logMessage(log, level, exit(methodName, duration, response, showResult, showExecutionTime));
            return response;
        } else {
            return point.proceed();
        }


    }

    // implementations of log, entry, exit, methods, etc.
    static String entry(String methodName, boolean showArgs, String[] params, Object[] args) {
        ObjectMapper objectMapper = getObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, SerializationFeature.FAIL_ON_EMPTY_BEANS);
        objectMapper.registerModule(new JavaTimeModule());
        StringJoiner message = new StringJoiner(" ")
                .add("Début de la méthode").add(methodName).add("method");
        if (showArgs && Objects.nonNull(params) && Objects.nonNull(args) && params.length == args.length) {
            Map<String, Object> values = new HashMap<>(params.length);
            for (int i = 0; i < params.length; i++) {
                Object arg = args[i];
                if (arg instanceof MultipartFile) {
                    values.put(params[i], "Fichier(nom: " + ((MultipartFile) arg).getOriginalFilename() + ",type: "
                            + ((MultipartFile) arg).getContentType() + ")");
                }
                if (arg instanceof AbstractResource) {
                    values.put(params[i], "Ressources(nom: " + ((AbstractResource) arg).getFilename() + ")");
                } else {
                    try {
                        values.put(params[i], objectMapper.writeValueAsString(arg));
                    } catch (JsonProcessingException e) {
                        values.put(params[i], arg.toString());
                    }

                }
            }
            message.add("avec les arguments:")
                    .add(values.toString());
        }
        return message.toString();
    }

    static String exit(String methodName, String duration, Object result, boolean showResult, boolean showExecutionTime) {
        StringJoiner message = new StringJoiner(" ")
                .add("Fin de la méthode").add(methodName).add("method");
        if (showExecutionTime) {
            message.add("dans").add(duration);
        }
        if (showResult) {
            message.add("avec le retour:").add(result.toString());
        }
        return message.toString();
    }

    static void logMessage(Logger logger, LogLevel level, String message) {
        switch (level) {
            case DEBUG:
                logger.debug(message);
                break;
            case TRACE:
                logger.trace(message);
                break;
            case WARN:
                logger.warn(message);
                break;
            case ERROR:
            case FATAL:
                logger.error(message);
                break;
            default:
                logger.info(message);
        }
    }
}
