package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.core.config.IDocumentConfigurationService;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.DocumentToken;
import com.atexo.docgen.core.domain.editor.model.ValidationDocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.services.IDocumentEditorService;
import com.atexo.docgen.core.domain.template.services.ITemplateService;
import com.atexo.docgen.core.enums.FileModeEnum;
import com.atexo.docgen.core.enums.FileRedacStatusEnum;
import com.atexo.docgen.core.enums.TypeExceptionEnum;
import com.atexo.docgen.core.exceptions.DocGenException;
import com.atexo.docgen.core.exceptions.DocumentExtensionNotSupportedException;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;

@RestController
@MultipartConfig
@RequestMapping("api/v1/document-editor")
@Slf4j
public class DocumentEditorController {
    private final IDocumentEditorService iDocumentEditorService;
    private final IDocumentConfigurationService iDocumentConfigurationService;
    private final IDocumentConvertorService iDocumentConvertorService;
    private final ITemplateService iTemplateService;

    public DocumentEditorController(IDocumentConfigurationService iDocumentConfigurationService, IDocumentEditorService iDocumentEditorService, IDocumentConvertorService iDocumentConvertorService, ITemplateService iTemplateService) {
        this.iDocumentConfigurationService = iDocumentConfigurationService;
        this.iDocumentEditorService = iDocumentEditorService;
        this.iDocumentConvertorService = iDocumentConvertorService;
        this.iTemplateService = iTemplateService;
    }

    @PostMapping("request")
    public ResponseEntity<String> requestToken() {
        return ResponseEntity.status(HttpStatus.SC_MOVED_PERMANENTLY).body("Utilisez /v2/request");

    }

    @PostMapping("request-xml")
    public String requestXml(@RequestPart @NotNull @NotEmpty MultipartFile xml,
                             @RequestPart MultipartFile document,
                             @RequestPart @NotNull DocumentEditorRequest editorRequest,
                             @RequestParam(required = false) Boolean override
    ) throws IOException {
        if (!FileModeEnum.REDAC.getValue().equals(editorRequest.getMode())) {
            throw new DocGenException(TypeExceptionEnum.EDITOR, "Seul le mode redac est autorisé pour ce service. ");
        }
        this.validate(xml, document);
        iDocumentConvertorService.addXmlDocument(xml.getInputStream(), editorRequest.getDocumentId(), editorRequest.getPlateformeId());
        if (editorRequest.getDocumentTitle() == null) {
            editorRequest.setDocumentTitle(document.getOriginalFilename());
        }
        editorRequest.setMode(FileModeEnum.REDAC.getValue());
        DocumentToken docxToken = iDocumentEditorService.getToken(document.getInputStream(), editorRequest, "docx", override);
        return docxToken.getToken();
    }

    @PostMapping("request-validation")
    public String requestValidation(
            @RequestPart MultipartFile document,
            @RequestPart @NotNull ValidationDocumentEditorRequest editorRequest,
            @RequestParam(required = false) Boolean override) throws IOException {
        this.validate(document);
        if (editorRequest.getDocumentTitle() == null) {
            editorRequest.setDocumentTitle(document.getOriginalFilename());
        }
        if (editorRequest.getStatus() == null) {
            editorRequest.setStatus(FileRedacStatusEnum.BROUILLON.getCode());
        }
        String curExt = DocumentUtils.getFileExtension(document.getOriginalFilename());
        iDocumentConvertorService.addDocumentToValidate(editorRequest.getDocumentId(), editorRequest.getPlateformeId(), editorRequest.getStatus());

        editorRequest.setMode(FileModeEnum.VALIDATION.getValue());
        DocumentToken docxToken = iDocumentEditorService.getToken(document.getInputStream(), editorRequest, curExt, override);
        return docxToken.getToken();
    }

    @PostMapping("request-template")
    public String requestTemplate(@RequestPart(required = false) MultipartFile template,
                                  @RequestPart @NotNull @Valid DocumentEditorRequest editorRequest,
                                  @RequestParam(required = false) Boolean override) throws IOException {
        if (!FileModeEnum.TEMPLATE.getValue().equals(editorRequest.getMode())) {
            throw new DocGenException(TypeExceptionEnum.EDITOR, "Seul le mode template est autorisé pour ce service. ");
        }
        this.validateTemplate(template);
        InputStream inputStream;
        String name = "template.docx";
        String curExt = "docx";
        if (template != null) {
            inputStream = template.getInputStream();
            name = template.getOriginalFilename();
            curExt = DocumentUtils.getFileExtension(template.getOriginalFilename());
        } else {
            byte[] defaultFile = iTemplateService.applyStyle(null, null).toByteArray();
            inputStream = new ByteArrayResource(defaultFile).getInputStream();

        }
        iDocumentConvertorService.addTemplateDocument(editorRequest.getDocumentAdministrableConfiguration(), editorRequest.getDocumentId(), editorRequest.getPlateformeId());
        if (editorRequest.getDocumentTitle() == null) {
            editorRequest.setDocumentTitle(name);
        }
        editorRequest.setMode(FileModeEnum.TEMPLATE.getValue());
        DocumentToken docxToken = iDocumentEditorService.getToken(inputStream, editorRequest, curExt, override);
        return docxToken.getToken();
    }

    private void validate(MultipartFile xml, MultipartFile document) {
        String curExt = DocumentUtils.getFileExtension(xml.getOriginalFilename());
        if (!iDocumentConfigurationService.getConvertExts().contains(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le fichier XML doit être d'extension xml");
        }
        if (document != null) {
            curExt = DocumentUtils.getFileExtension(document.getOriginalFilename());
            if (!"docx".equals(curExt)) {
                throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le document doit être d'extension docx");
            }
        }
    }

    private void validateXlsx(MultipartFile document) {

        if (document != null) {
            String curExt = DocumentUtils.getFileExtension(document.getOriginalFilename());
            if (!"xlsx".equals(curExt)) {
                throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le document doit être d'extension xlsx");
            }
        }
    }

    private void validate(MultipartFile document) {
        String curExt = DocumentUtils.getFileExtension(document.getOriginalFilename());
        if (!iDocumentConfigurationService.getEditedExts().contains(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.EDITOR);
        }

    }

    private void validateTemplate(MultipartFile template) {
        if (template == null) {
            return;
        }
        String curExt = DocumentUtils.getFileExtension(template.getOriginalFilename());
        if (!iDocumentConfigurationService.getEditedExts().contains(curExt)) {
            throw new DocumentExtensionNotSupportedException(TypeExceptionEnum.CONVERSION, "Le fichier Template doit être d'extension xlsx/docx");
        }
    }

}
