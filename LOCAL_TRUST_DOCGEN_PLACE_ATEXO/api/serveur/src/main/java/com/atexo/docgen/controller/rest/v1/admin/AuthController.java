package com.atexo.docgen.controller.rest.v1.admin;

import com.atexo.docgen.core.domain.security.model.AtexoUserDetails;
import com.atexo.docgen.core.domain.security.model.JwtResponse;
import com.atexo.docgen.core.domain.security.model.LoginRequest;
import com.atexo.docgen.core.domain.security.model.SignupRequest;
import com.atexo.docgen.core.domain.security.services.IJwtService;
import com.atexo.docgen.core.port.IUserPort;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/v1/auth")
public class AuthController {
	private final AuthenticationManager authenticationManager;

	private final IJwtService jwtUtils;
	private final IUserPort iUserPort;

	@Value("${docgen.admin.login}")
	private String login;

	@Value("${docgen.admin.password}")
	private String password;

	@Value("${docgen.admin.email}")
	private String email;

	public AuthController(AuthenticationManager authenticationManager, IJwtService jwtUtils, IUserPort iUserPort) {
		this.authenticationManager = authenticationManager;
		this.jwtUtils = jwtUtils;
        this.iUserPort = iUserPort;
	}

	@PostConstruct
	public void initialize() {
		iUserPort.registerUser(SignupRequest.builder().username(login).password(password).email(email).roles(Collections.singleton("admin")).build());
	}


	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		AtexoUserDetails userDetails = (AtexoUserDetails) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList());

		return ResponseEntity.ok(JwtResponse.builder()
				.token(jwt)
				.id(userDetails.getId())
				.username(userDetails.getUsername())
				.email(userDetails.getEmail())
				.roles(roles).build());
	}


}
