package com.atexo.docgen.controller.rest.v1.mock;

import com.atexo.docgen.core.domain.convertor.model.analyse.Configuration;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.ValidationDocumentEditorRequest;
import com.atexo.docgen.core.domain.extractor.model.ExcelConfiguration;
import com.atexo.docgen.core.domain.extractor.model.Feuille;
import com.atexo.docgen.core.domain.extractor.model.analyse.AnalyseOffre;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.domain.monitoring.model.FileStatus;
import com.atexo.docgen.core.domain.monitoring.model.MonitoringModel;
import com.atexo.docgen.core.port.IDocumentPort;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api.php/v1")
public class MPEMockController {

    private final IDocumentPort iDocumentPort;
    private final RestTemplate restTemplate;
    @Value("${docgen.private-url}")
    private String docgenMockUrl;
    @Value("${external-apis.onlyoffice.storage-folder}")
    private String storagePath;
    @Value("${docgen.mock-folder}")
    private String docgenMockFolder;
    private final String template1 = "template";

    public MPEMockController(RestTemplate restTemplate, IDocumentPort iDocumentPort) {
        this.restTemplate = restTemplate;
        this.iDocumentPort = iDocumentPort;
    }

    @GetMapping("suivi/{id}/download/{format}")
    public ResponseEntity<ByteArrayResource> getDocument(@PathVariable String id, @PathVariable String format) throws IOException {
        File convertedClause = new File(storagePath + File.separatorChar + id);
        ByteArrayResource file = new ByteArrayResource(IOUtils.toByteArray(Files.newInputStream(convertedClause.toPath())));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output." + format)
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);

    }


    @PostMapping(value = "generator/generate", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> generate(@RequestPart MultipartFile template, @RequestParam(required = false) Boolean convertToPdf,
                                             @RequestPart("keyValues") List<KeyValueRequest> keyValues) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add(template1, new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "templates", template.getOriginalFilename(), true)));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("keyValues", new HttpEntity<>(keyValues, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenMockUrl + "/api/v1/document-generator/generate";
        if (Boolean.TRUE.equals(convertToPdf)) {
            url += "?convertToPdf=" + convertToPdf;
        }
        return restTemplate.postForEntity(url, requestEntity, Resource.class);
    }

    @PostMapping("generator/variables")
    public ResponseEntity<Map> getVariable(@RequestPart MultipartFile template) throws IOException {


        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add(template1, new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "templates", template.getOriginalFilename(), true)));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/document-generator/variables", requestEntity, Map.class);
    }

    @GetMapping(value = "suivi/status", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> getStatus(@RequestParam String token) {
        return restTemplate.getForEntity(docgenMockUrl + "/api/v1/document-suivi/status?token=" + token, Resource.class);
    }

    @GetMapping("suivi/plateforme-status")
    public List<FileStatus> getPlateformeFilesStatus(@RequestParam String plateformeId) {
        return Arrays.asList(restTemplate.getForObject(docgenMockUrl + "/api/v1/document-suivi/plateforme-status?plateformeId=" + plateformeId, FileStatus[].class));
    }

    @PostMapping("extractor/extract-xlsx")
    public ResponseEntity<Resource> extract(@RequestPart @NotNull ExcelConfiguration configuration,
                                            @RequestPart @NotNull MultipartFile template,
                                            @RequestPart @NotNull List<MultipartFile> fichiers
    ) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add(template1, new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "a_extracter", template.getOriginalFilename(), true)));

        for (MultipartFile fichier : fichiers) {
            FileSystemResource fileSystemResource = new FileSystemResource(DocumentUtils.saveFile(fichier.getInputStream(), docgenMockFolder + File.separator + "a_extracter", fichier.getOriginalFilename(), true));
            bodyMap.add("fichiers", fileSystemResource);
        }

        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("configuration", new HttpEntity<>(configuration, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/document-extractor/extract-xlsx", requestEntity, Resource.class);
    }


    @PostMapping("editor/request-xml")
    public ResponseEntity<String> getEditorRedacToken(@RequestPart MultipartFile document, @RequestPart MultipartFile xml, @RequestPart DocumentEditorRequest editorRequest) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("xml", new FileSystemResource(DocumentUtils.saveFile(xml.getInputStream(), docgenMockFolder + File.separator + "a_rediger", xml.getOriginalFilename(), true)));
        bodyMap.add("document", new FileSystemResource(DocumentUtils.saveFile(document.getInputStream(), docgenMockFolder + File.separator + "a_rediger", document.getOriginalFilename(), true)));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = getEditorConfigMultipart(editorRequest, bodyMap);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/document-editor/request-xml", requestEntity, String.class);
    }

    @PostMapping("editor/request-validation")
    public ResponseEntity<String> getEditorValidationToken(@RequestPart MultipartFile document, @RequestPart ValidationDocumentEditorRequest editorRequest) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("document", new FileSystemResource(DocumentUtils.saveFile(document.getInputStream(), docgenMockFolder + File.separator + "a_rediger", document.getOriginalFilename(), true)));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = getEditorConfigMultipart(editorRequest, bodyMap);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/document-editor/request-validation", requestEntity, String.class);
    }

    @PostMapping("editor/request-template")
    public ResponseEntity<String> getEditorXmlToken(@RequestPart MultipartFile template, @RequestPart DocumentEditorRequest editorRequest) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        if (template != null && !StringUtils.isEmpty(template.getOriginalFilename())) {
            bodyMap.add("template", new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "a_rediger", template.getOriginalFilename(), true)));
        }

        HttpEntity<MultiValueMap<String, Object>> requestEntity = getEditorConfigMultipart(editorRequest, bodyMap);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/document-editor/request-template", requestEntity, String.class);
    }


    @PostMapping("template-referential/upload")
    public ResponseEntity<Resource> uploadTemplate(
            @RequestPart @NotNull MultipartFile template,
            @RequestPart @NotNull List<MultipartFile> blocTemplates
    ) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add(template1, new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "template", template.getOriginalFilename(), true)));

        for (MultipartFile fichier : blocTemplates) {
            FileSystemResource fileSystemResource = new FileSystemResource(DocumentUtils.saveFile(fichier.getInputStream(), docgenMockFolder + File.separator + "template", fichier.getOriginalFilename(), true));
            bodyMap.add("blocTemplates", fileSystemResource);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/template-referential/default/upload",
                requestEntity, Resource.class);
    }

    public static HttpEntity<MultiValueMap<String, Object>> getEditorConfigMultipart(@RequestPart DocumentEditorRequest editorRequest, MultiValueMap<String, Object> bodyMap) {
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("editorRequest", new HttpEntity<>(editorRequest, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return new HttpEntity<>(bodyMap, headers);
    }


    @GetMapping("suivi/all")
    public Map<String, List<MonitoringModel>> getStatus() {
        return iDocumentPort.getAll().stream().filter(monitoringModel -> Objects.nonNull(monitoringModel.getPlateformeId()))
                .collect(Collectors.groupingBy(MonitoringModel::getPlateformeId));
    }


    @PostMapping("extractor/analyse-report")
    public ResponseEntity<AnalyseOffre> convertAnalyse(@RequestPart @NotNull Configuration configuration,
                                                       @RequestPart @NotNull MultipartFile document) throws IOException {

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("document", new FileSystemResource(DocumentUtils.saveFile(document.getInputStream(), docgenMockFolder + File.separator + "analyse", document.getOriginalFilename(), true)));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("configuration", new HttpEntity<>(configuration, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenMockUrl + "/api/v1/document-extractor/analyse-report";
        return restTemplate.postForEntity(url, requestEntity, AnalyseOffre.class);

    }


    @PostMapping("extractor/xlsx-sheet")
    public ResponseEntity<Feuille[]> extract(@RequestPart @NotNull MultipartFile document
    ) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("document", new FileSystemResource(DocumentUtils.saveFile(document.getInputStream(), docgenMockFolder + File.separator + "a_extracter", document.getOriginalFilename(), true)));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        return restTemplate.postForEntity(docgenMockUrl + "/api/v1/document-extractor/xlsx-sheet", requestEntity, Feuille[].class);
    }


}
