package com.atexo.docgen.controller.rest.v1.mock;

import com.atexo.docgen.core.domain.convertor.model.analyse.Consultation;
import com.atexo.docgen.core.domain.convertor.model.redac.DocxDocument;
import com.atexo.docgen.core.domain.convertor.services.IDocumentConvertorService;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.utilitaire.DocumentUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api.php/v1/convertor")
public class MPEMockConvertorController {
    private final IDocumentConvertorService iDocumentConvertorService;
    private final RestTemplate restTemplate;
    @Value("${docgen.private-url}")
    private String docgenMockUrl;

    @Value("${docgen.mock-folder}")
    private String docgenMockFolder;
    private static final String TEMPLATE = "template";

    public MPEMockConvertorController(RestTemplate restTemplate, IDocumentConvertorService iDocumentConvertorService) {
        this.restTemplate = restTemplate;
        this.iDocumentConvertorService = iDocumentConvertorService;
    }

    @PostMapping(value = "{outputFormat}/finalize", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> generate(@RequestParam(required = false) String outputFileName,
                                             @RequestParam(required = false) String inputFormat,
                                             @RequestPart MultipartFile document,
                                             @PathVariable String outputFormat) throws IOException {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("document", new FileSystemResource(DocumentUtils.saveFile(document.getInputStream(), docgenMockFolder + File.separator + "convertor", document.getOriginalFilename(), true)));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenMockUrl + "/api/v1/document-convertor/" + outputFormat + "/finalize";
        if (outputFileName != null) {
            url += "?outputFileName=" + outputFileName;
        }
        if (inputFormat != null) {
            url += url.contains("outputFileName") ? "&" : "?";
            url += "inputFormat=" + inputFormat;
        }
        return restTemplate.postForEntity(url, requestEntity, Resource.class);
    }


    @PostMapping("xml")
    public ResponseEntity<ByteArrayResource> convertXmlToDocx(@RequestBody DocxDocument document) {
        ByteArrayResource file = new ByteArrayResource(iDocumentConvertorService.getXml(document).toByteArray());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output.xml")
                .cacheControl(CacheControl.noCache())
                .contentLength(file.contentLength())
                .body(file);
    }


    @PostMapping(value = "convert-xml", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> generate(@RequestParam(required = false) String outputFileName,
                                             @RequestPart MultipartFile xml, @RequestPart MultipartFile template,
                                             @RequestPart("keyValues") List<KeyValueRequest> keyValues) throws IOException {


        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("xml", new FileSystemResource(DocumentUtils.saveFile(xml.getInputStream(), docgenMockFolder + File.separator + "redac", xml.getOriginalFilename(), true)));
        bodyMap.add(TEMPLATE, new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "redac", template.getOriginalFilename(), true)));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("keyValues", new HttpEntity<>(keyValues, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenMockUrl + "/api/v1/document-convertor/convert-xml";
        if (outputFileName != null) {
            url += "?outputFileName=" + outputFileName;
        }
        return restTemplate.postForEntity(url, requestEntity, Resource.class);

    }

    @PostMapping(value = "convert-analyse", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> convertAnalyse(@RequestParam(required = false) String outputFileName,
                                                   @RequestPart MultipartFile template,
                                                   @RequestPart Consultation meta) throws IOException {


        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("template", new FileSystemResource(DocumentUtils.saveFile(template.getInputStream(), docgenMockFolder + File.separator + "analyse", template.getOriginalFilename(), true)));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("meta", new HttpEntity<>(meta, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenMockUrl + "/api/v1/document-convertor/convert-analyse";
        if (outputFileName != null) {
            url += "?outputFileName=" + outputFileName;
        }
        return restTemplate.postForEntity(url, requestEntity, Resource.class);

    }


}
