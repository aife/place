package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.enums.FileStatusEnum;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DocumentMonitoringControllerTest extends TestAbstractResources {

	@RegisterExtension
	public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


	String token2;
	String token;

	@BeforeEach
	public void setup() throws Exception {
		super.setup();
		File file = new File("src/test/resources/test-template.xlsx");
		FileInputStream input = new FileInputStream(file);
		MockMultipartFile template = new MockMultipartFile("file",
				file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
		DocumentEditorRequest editorRequest = DocumentEditorRequest.builder()
				.plateformeId("plateformeId")
				.documentId("documentId")
				.mode("edit")
				.callback("http://callback.com")
				.user(User.builder().name("DocumentMonitoringControllerTest").id("id").roles(new ArrayList<>()).build())
				.build();
		MockMultipartFile keyValuesFile = new MockMultipartFile("editorRequest", "",
				"application/json", objectMapper.writeValueAsString(editorRequest).getBytes());


		token = mockMvc.perform(MockMvcRequestBuilders.multipart(path2 + "document-editor/request")
				.file(keyValuesFile)
				.file(template))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		File document = new File("src/test/resources/convertor/test-template.docx_converted.docx");
		FileInputStream inputDocument = new FileInputStream(document);
		MockMultipartFile documentMultipartFile = new MockMultipartFile("document",
				document.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(inputDocument));
		File xml = new File("src/test/resources/convertor/document-demo.xml");
		FileInputStream inputXml = new FileInputStream(xml);
		MockMultipartFile xmlMultipartFile = new MockMultipartFile("xml",
				xml.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(inputXml));


		DocumentEditorRequest editorRequest2 = DocumentEditorRequest.builder()
				.mode("redac")
				.callback("http://callback.com")
				.plateformeId("plateformeId2")
				.documentId("documentId2")
				.documentTitle("documentTitle2")
				.user(User.builder().name("DocumentMonitoringControllerTest2").id("id").roles(Arrays.asList("modification", "validation")).build())
				.build();
		MockMultipartFile keyValuesFile2 = new MockMultipartFile("editorRequest", "",
				"application/json", objectMapper.writeValueAsString(editorRequest2).getBytes());


		token2 = mockMvc.perform(MockMvcRequestBuilders.multipart(path + "document-editor/request-xml")
				.file(keyValuesFile2)
				.file(documentMultipartFile)
				.file(xmlMultipartFile))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

	}

	@Test
	public void getDocumentStatus() throws Exception {
		// Test Status Of token
		MvcResult resultStatus = mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/status?token=" + token))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
				.andReturn();
		assertNotNull(resultStatus);
		assertNotNull(resultStatus.getResponse());
		String resource = resultStatus.getResponse().getContentAsString();
		assertNotNull(resource);
		assertNotNull( resource);
		String status = resultStatus.getResponse().getHeader("document-status");
		assertNotNull(status);
		assertEquals(FileStatusEnum.REQUEST_TO_OPEN.name(), status);
	}

}
