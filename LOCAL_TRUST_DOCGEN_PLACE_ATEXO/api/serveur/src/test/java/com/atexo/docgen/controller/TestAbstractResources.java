package com.atexo.docgen.controller;

import com.atexo.docgen.controller.config.ServeurScanConfig;
import com.atexo.docgen.core.domain.security.model.JwtResponse;
import com.atexo.docgen.core.domain.security.model.LoginRequest;
import com.atexo.docgen.dao.repository.DocumentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = {ServeurScanConfig.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("it")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ContextConfiguration(initializers = {TestAbstractResources.Initializer.class})
@DirtiesContext
@AutoConfigureMockMvc
public abstract class TestAbstractResources {

    private static final String STUB_PATH = "src/test/resources/stub/";
    private static final String STUB_EXTENSION = ".json";

    @LocalServerPort
    protected int port;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected DocumentRepository documentRepository;
    protected String tokenMock;

    protected final String path = "/api/v1/";
    protected final String path2 = "/api/v2/";
    protected final String pathPhp = "/api.php/v1/";
    protected final String pathPhp2 = "/api.php/v2/";
    @Value("${docgen.admin.login}")
    private String loginAdmin;

    @Value("${docgen.admin.password}")
    private String passwordAdmin;

    @BeforeEach
    public void setup() throws Exception {
        documentRepository.deleteAll();
        String result = mockMvc.perform(MockMvcRequestBuilders.post(path + "auth/signin")
                .content(objectMapper.writeValueAsString(LoginRequest.builder()
                        .username(loginAdmin)
                        .password(passwordAdmin)
                        .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        JwtResponse jwtResponse = objectMapper.readValue(result, JwtResponse.class);
        tokenMock = jwtResponse.getToken();
    }


    protected static String readStub(String stubId) throws IOException {
        Path pathSource = Paths.get(STUB_PATH + stubId + STUB_EXTENSION).toAbsolutePath();
        List<String> lines = Files.readAllLines(pathSource, Charset.defaultCharset());
        return lines.stream().reduce((result, concatWith) -> (result + concatWith.trim()).replace("\": ", "\":")).orElse(null);
    }


    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @SneakyThrows
        @Override
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    //"docgen.public-url=http://192.168.10.70/",
                    "docgen.public-url=" + InetAddress.getLocalHost().getHostAddress() + ":" + ServeurScanConfig.port,
                    "docgen.private-url=http://127.0.0.1:" + ServeurScanConfig.port + "/docgen/",
                    "external-apis.plugins.base-url=" + InetAddress.getLocalHost().getHostAddress() + "/plugins/"
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
