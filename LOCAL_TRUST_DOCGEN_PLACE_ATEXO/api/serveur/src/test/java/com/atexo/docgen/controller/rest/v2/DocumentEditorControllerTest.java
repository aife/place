package com.atexo.docgen.controller.rest.v2;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.impl.JwtServiceImpl;
import com.atexo.docgen.core.enums.FileStatusEnum;
import com.atexo.docgen.core.port.IDocumentPort;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DocumentEditorControllerTest extends TestAbstractResources {
    @RegisterExtension
    public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


    private final String pathV2 = "/api/v2/";
    private final String path = "/api/v1/";
    @Autowired
    private IDocumentPort iDocumentPort;

    @Autowired
    private JwtServiceImpl jwtServiceImpl;

    @Test
    public void testGenerateToken() throws Exception {
        File file = new File("src/test/resources/test-template.xlsx");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile template = new MockMultipartFile("file",
                file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
        DocumentEditorRequest editorRequest = DocumentEditorRequest.builder()
                .mode("edit")
                .callback("http://callback.com")
                .plateformeId("plateformeId")
                .documentId("documentId")
                .user(User.builder().name("testGenerateToken").roles(new ArrayList<>()).id("id").build())
                .build();
        MockMultipartFile editorRequestFile = new MockMultipartFile("editorRequest", "",
                "application/json", objectMapper.writeValueAsString(editorRequest).getBytes());


        MvcResult resultToken = mockMvc.perform(MockMvcRequestBuilders.multipart(pathV2 + "document-editor/request")
                .file(editorRequestFile)
                .file(template))
                .andExpect(status().isOk()).andReturn();
        assertNotNull(resultToken);
        assertNotNull(resultToken.getResponse());
        String token = resultToken.getResponse().getContentAsString();
        assertNotNull(token);

        // Test Status Of plateforme
        mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/plateforme-status?plateformeId=" + editorRequest.getPlateformeId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(readStub("expectedGetAllPlateformeStatus")));

        DocumentEditorDetails editorDetailsFromToken = jwtServiceImpl.getEditorDetailsFromToken(token);
        iDocumentPort.modifyDocumentStatusById(editorDetailsFromToken.getId(), FileStatusEnum.SAVED_AND_CLOSED.name());


        // Test Status Of plateforme
        mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/download-list?plateformeId=" + editorRequest.getPlateformeId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(readStub("expectedGetAllPlateformeFilesToDownload")));

    }

}
