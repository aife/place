package com.atexo.docgen.controller.config;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.shaded.org.apache.commons.lang.NotImplementedException;

public class MariaDbExtension implements BeforeAllCallback, AfterAllCallback {
	public static ITCustomMariaDBContainer MARIA_DB_CONTAINER;

	private MariaDbExtension() {
		throw new NotImplementedException();
	}

	public MariaDbExtension(String databaseName) {
		MARIA_DB_CONTAINER = ITCustomMariaDBContainer.getInstance(databaseName);

	}

	@Override
	public void beforeAll(ExtensionContext context) {
		MARIA_DB_CONTAINER.start();
	}

	@Override
	public void afterAll(ExtensionContext context) {
		MARIA_DB_CONTAINER.stop();
	}
}
