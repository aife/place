package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.enums.FileStatusEnum;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DocumentEditorControllerTest extends TestAbstractResources {

	@RegisterExtension
	public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


	@Test
	@Order(1)
	public void testGenerateToken() throws Exception {
		File file = new File("src/test/resources/test-template.xlsx");
		FileInputStream input = new FileInputStream(file);
		MockMultipartFile template = new MockMultipartFile("file",
				file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
		DocumentEditorRequest editorRequest = DocumentEditorRequest.builder()
				.mode("edit")
				.callback("http://callback.com")
				.user(User.builder().name("testGenerateToken").id("id").roles(new ArrayList<>()).build())
				.build();
		MockMultipartFile keyValuesFile = new MockMultipartFile("editorRequest", "",
				"application/json", objectMapper.writeValueAsString(editorRequest).getBytes());


		MvcResult resultToken = mockMvc.perform(MockMvcRequestBuilders.multipart(path2 + "document-editor/request")
				.file(keyValuesFile)
				.file(template))
				.andExpect(status().isOk()).andReturn();
		assertNotNull(resultToken);
		assertNotNull(resultToken.getResponse());
		String token = resultToken.getResponse().getContentAsString();
		assertNotNull(token);

		// Test Status Of token
		MvcResult resultStatus = mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/status?token=" + token))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
				.andReturn();
		assertNotNull(resultStatus);
		assertNotNull(resultStatus.getResponse());
		String resource = resultStatus.getResponse().getContentAsString();
		assertNotNull(resource);
		String status = resultStatus.getResponse().getHeader("document-status");
		assertNotNull(status);
		assertEquals(FileStatusEnum.REQUEST_TO_OPEN.name(), status);

		// Test Status Of token V2
		MvcResult resultStatusV2 = mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/status?token=" + token))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
				.andReturn();
		assertNotNull(resultStatusV2);
		assertNotNull(resultStatusV2.getResponse());
		String resourceV2 = resultStatusV2.getResponse().getContentAsString();
		assertNotNull(resourceV2);
		String statusV2 = resultStatusV2.getResponse().getHeader("document-status");
		assertNotNull(statusV2);
		assertEquals(FileStatusEnum.REQUEST_TO_OPEN.name(), statusV2);


	}

	@Test
	@Order(2)
	public void testGenerateTokenWithXml() throws Exception {
		File document = new File("src/test/resources/convertor/converted-test.docx");
		FileInputStream input = new FileInputStream(document);
		MockMultipartFile documentMultipartFile = new MockMultipartFile("document",
				document.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
		File xml = new File("src/test/resources/convertor/document-demo.xml");
		FileInputStream inputXml = new FileInputStream(xml);
		MockMultipartFile xmlMultipartFile = new MockMultipartFile("xml",
				xml.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(inputXml));


		DocumentEditorRequest editorRequest = DocumentEditorRequest.builder()
				.plateformeId("plateformeId")
				.documentId("documentId")
				.mode("redac")
				.callback("http://callback.com")
				.plateformeId("plateformeId2")
				.documentId("documentId2")
				.documentTitle("documentTitle2")
				.user(User.builder().name("testGenerateTokenWithXml").roles(Arrays.asList("modification", "validation")).id("id").build())
				.build();
		MockMultipartFile keyValuesFile = new MockMultipartFile("editorRequest", "",
				"application/json", objectMapper.writeValueAsString(editorRequest).getBytes());


		MvcResult resultToken = mockMvc.perform(MockMvcRequestBuilders.multipart(path + "document-editor/request-xml")
				.file(keyValuesFile)
				.file(documentMultipartFile)
				.file(xmlMultipartFile))
				.andExpect(status().isOk()).andReturn();
		assertNotNull(resultToken);
		assertNotNull(resultToken.getResponse());
		String token = resultToken.getResponse().getContentAsString();
		assertNotNull(token);

		// Test Status Of token
		MvcResult resultStatus = mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/status?token=" + token))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/zip"))
				.andReturn();
		assertNotNull(resultStatus);
		assertNotNull(resultStatus.getResponse());
		String resource = resultStatus.getResponse().getContentAsString();
		assertNotNull(resource);
		String status = resultStatus.getResponse().getHeader("document-status");
		assertNotNull(status);
		assertEquals(FileStatusEnum.REQUEST_TO_OPEN.name(), status);

		// Test Status Of token V2
		MvcResult resultStatusV2 = mockMvc.perform(MockMvcRequestBuilders.get(path + "document-suivi/status?token=" + token))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/zip"))
				.andReturn();
		assertNotNull(resultStatusV2);
		assertNotNull(resultStatusV2.getResponse());
		String resourceV2 = resultStatusV2.getResponse().getContentAsString();
		assertNotNull(resourceV2);
		String statusV2 = resultStatusV2.getResponse().getHeader("document-status");
		assertNotNull(statusV2);
		assertEquals(FileStatusEnum.REQUEST_TO_OPEN.name(), statusV2);


	}

}
