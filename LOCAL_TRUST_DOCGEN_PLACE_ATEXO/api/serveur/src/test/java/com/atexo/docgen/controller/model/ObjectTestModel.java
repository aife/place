package com.atexo.docgen.controller.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ObjectTestModel {
	private String simpleString;
	private SubObject subObject;
	private List<SubObject> subObjects;


	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	public static class SubObject {
		private String simpleString;
	}

}
