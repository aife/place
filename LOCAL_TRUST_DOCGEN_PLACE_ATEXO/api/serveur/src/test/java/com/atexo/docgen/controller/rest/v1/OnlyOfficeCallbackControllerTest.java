package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.TrackDocumentRequest;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.domain.security.model.DocumentEditorDetails;
import com.atexo.docgen.core.domain.security.services.IJwtService;
import com.atexo.docgen.core.enums.FileStatusEnum;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OnlyOfficeCallbackControllerTest extends TestAbstractResources {

	@Autowired
	private IJwtService iJwtService;

	@RegisterExtension
	public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


	@Test
	public void testCallbackUri() throws Exception {
		File file = new File("src/test/resources/test-template.xlsx");
		FileInputStream input = new FileInputStream(file);
		MockMultipartFile template = new MockMultipartFile("file",
				file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
		DocumentEditorRequest editorRequest = DocumentEditorRequest.builder()
				.plateformeId("plateformeId")
				.documentId("documentId")
				.mode("edit")
				.user(User.builder().name("testCallbackUri").id("id").roles(new ArrayList<>()).build())
				.build();
		MockMultipartFile keyValuesFile = new MockMultipartFile("editorRequest", "",
				"application/json", objectMapper.writeValueAsString(editorRequest).getBytes());


		MvcResult resultToken = mockMvc.perform(MockMvcRequestBuilders.multipart(path2 + "document-editor/request")
				.file(keyValuesFile)
				.file(template))
				.andExpect(status().isOk()).andReturn();
		assertNotNull(resultToken);
		assertNotNull(resultToken.getResponse());
		String token = resultToken.getResponse().getContentAsString();
		assertNotNull(token);


		// Post
		DocumentEditorDetails detailsFromToken = iJwtService.getEditorDetailsFromToken(token);
		String id = detailsFromToken.getId();
		TrackDocumentRequest request = TrackDocumentRequest.builder()
				.status(10)
				.key("test")
				.build();

		// Expect Exception
		mockMvc.perform(MockMvcRequestBuilders.post(path + "edition/callback/track?token=" + token)
				.content(objectMapper.writeValueAsString(request))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json(readStub("expectedRuntimeException")));

		// Except Ok
		request.setStatus(FileStatusEnum.REQUEST_TO_OPEN.getCode());
		mockMvc.perform(MockMvcRequestBuilders.post(path + "edition/callback/track?token=" + token)
				.content(objectMapper.writeValueAsString(request))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json(readStub("expectedSuccesTrack")));

		// Except Download Failure When SAVED_AND_CLOSED
		request.setStatus(FileStatusEnum.SAVED_AND_CLOSED.getCode());
		mockMvc.perform(MockMvcRequestBuilders.post(path + "edition/callback/track?token=" + token)
				.content(objectMapper.writeValueAsString(request))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json(readStub("expectedFailureTrack")));
	}
}
