package com.atexo.docgen.controller.rest.v1.mock;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.core.domain.editor.model.DocumentEditorRequest;
import com.atexo.docgen.core.domain.editor.model.User;
import com.atexo.docgen.core.domain.extractor.model.ExcelConfiguration;
import com.atexo.docgen.core.domain.extractor.model.PositionToExtract;
import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import com.atexo.docgen.core.domain.extractor.model.StaticData;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import com.atexo.docgen.core.enums.FileStatusEnum;
import org.apache.poi.util.IOUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.atexo.docgen.controller.rest.v1.DocumentExtractorControllerTest.assertXlsxEquals;
import static com.atexo.docgen.controller.rest.v1.DocumentExtractorControllerTest.getTemplateFromFile;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MPEMockControllerTest extends TestAbstractResources {
    @RegisterExtension
    public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


    @Test
    public void testGenerateToken() throws Exception {
        File file = new File("src/test/resources/test-template.xlsx");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile template = new MockMultipartFile("file",
                file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
        DocumentEditorRequest editorRequest = DocumentEditorRequest.builder()
                .plateformeId("plateformeId")
                .documentId("documentId")
                .mode("edit")
                .callback(null)
                .user(User.builder().name("testGenerateToken").roles(new ArrayList<>()).id("id").build())
                .build();
        MockMultipartFile keyValuesFile = new MockMultipartFile("editorRequest", "",
                "application/json", objectMapper.writeValueAsString(editorRequest).getBytes());


        MvcResult resultToken = mockMvc.perform(MockMvcRequestBuilders.multipart(pathPhp2 + "editor/request?token=" + tokenMock)
                .file(keyValuesFile)
                .file(template))
                .andExpect(status().isOk()).andReturn();
        assertNotNull(resultToken);
        assertNotNull(resultToken.getResponse());
        String token = resultToken.getResponse().getContentAsString();
        assertNotNull(token);

        // Test Status Of token
        MvcResult resultStatus = mockMvc.perform(MockMvcRequestBuilders.get(pathPhp + "suivi/status?token=" + token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andReturn();
        assertNotNull(resultStatus);
        assertNotNull(resultStatus.getResponse());
        String resource = resultStatus.getResponse().getContentAsString();
        assertNotNull(resource);
        assertNotNull(resource);
        String status = resultStatus.getResponse().getHeader("document-status");
        assertNotNull(status);
        assertEquals(FileStatusEnum.REQUEST_TO_OPEN.name(), status);

        // Test All Status
        mockMvc.perform(MockMvcRequestBuilders.get(pathPhp + "suivi/all?token=" + token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(readStub("expectedGetMockAllStatus")));

    }


    @Test
    public void testGetVariables() throws Exception {
        File file = new File("src/test/resources/test-template.xlsx");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile template = new MockMultipartFile("template",
                file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));


        mockMvc.perform(MockMvcRequestBuilders.multipart(pathPhp + "generator/variables?token=" + tokenMock)
                .file(template))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(readStub("expectedGetVariables")));
    }

    @Test
    public void testGenerate() throws Exception {
        File file = new File("src/test/resources/test-template.xlsx");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile template = new MockMultipartFile("template",
                file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
        List<KeyValueRequest> keyValues = new ArrayList<>();
        keyValues.add(new KeyValueRequest("TYPE_DOCUMENT", "test"));
        MockMultipartFile keyValuesFile = new MockMultipartFile("keyValues", "",
                "application/json", objectMapper.writeValueAsString(keyValues).getBytes());


        mockMvc.perform(MockMvcRequestBuilders.multipart(pathPhp + "generator/generate?token=" + tokenMock)
                .file(keyValuesFile)
                .file(template))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    }

    @Test
    public void testExtract() throws Exception {
        MockMultipartFile soumissionaire1 = getTemplateFromFile("fichiers", "src/test/resources/extractor/soumissionaire1.xlsx");

        MockMultipartFile soumissionaire2 = getTemplateFromFile("fichiers", "src/test/resources/extractor/soumissionaire2.xlsx");


        MockMultipartFile template = getTemplateFromFile("template", "src/test/resources/extractor/Tableau-template-extractor.xlsx");
        MockMultipartFile configuration = new MockMultipartFile("configuration", "",
                "application/json", objectMapper.writeValueAsString(getConfiguration()).getBytes());
        MvcResult resultStatus = mockMvc.perform(MockMvcRequestBuilders.multipart(pathPhp + "extractor/extract-xlsx?token=" + tokenMock)
                .file(template)
                .file(soumissionaire1)
                .file(soumissionaire2)
                .file(configuration))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                .andReturn();
        assertNotNull(resultStatus);
        assertNotNull(resultStatus.getResponse());
        byte[] resource = resultStatus.getResponse().getContentAsByteArray();
        assertNotNull(resource);

        FileOutputStream fos = new FileOutputStream(new File("src/test/resources/extractor/test.xlsx"));
        fos.write(resource);
        File testResult = new File("src/test/resources/extractor/test.xlsx");
        Assert.assertNotNull(testResult);
        assertTrue(testResult.exists());

        // process for checking all values
        File expectedFile = new File("src/test/resources/extractor/expected-test-result.xlsx");

        assertXlsxEquals(expectedFile, testResult);

        // delete of result files
        Files.delete(testResult.toPath());
    }

    private ExcelConfiguration getConfiguration() {
        ExcelConfiguration configuration = ExcelConfiguration.builder().build();
        //soumissionaire
        List<PositionToExtract> positions = new ArrayList<>();
        positions.add(PositionToExtract.builder()
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(9)
                        .build())
                .position(PositionValue.builder()
                        .column("B")
                        .row(8)
                        .build())
                .sheetName("Synthèse").build());
        //Montant du forfait EUR HT
        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("C")
                        .row(20)
                        .build())
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(10)
                        .build())
                .sheetName("Synthèse").build());

        //Montant du scénario de commandes EUR HT
        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("E")
                        .row(20)
                        .build())
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(11)
                        .build())
                .sheetName("Synthèse").build());

        //Montant du scénario de commandes EUR TTC
        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("C")
                        .row(21)
                        .build())
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(12)
                        .build())
                .sheetName("Synthèse").build());

        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("E")
                        .row(21)
                        .build())
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(13)
                        .build())
                .sheetName("Synthèse").build());

        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("C")
                        .row(22)
                        .build())

                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(14)
                        .build())
                .sheetName("Synthèse").build());

        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("D")
                        .row(22)
                        .build())

                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(15)
                        .build())
                .sheetName("Synthèse").build());

        positions.add(PositionToExtract.builder()
                .position(PositionValue.builder()
                        .column("E")
                        .row(22)
                        .build())
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(16)
                        .build())
                .sheetName("Synthèse").build());
        configuration.setPositions(positions);
        List<StaticData> staticData = new ArrayList<>();
        String pattern = "dd/MM/yyyy";

        DateFormat df = new SimpleDateFormat(pattern);

        Date date1 = Date.from(LocalDateTime.of(2020, 12, 2, 12, 45, 20).toInstant(ZoneOffset.UTC));
        Date date2 = Date.from(LocalDateTime.of(2020, 11, 2, 12, 45, 20).toInstant(ZoneOffset.UTC));

        String dateString1 = df.format(date1);
        String dateString2 = df.format(date2);
        staticData.add(StaticData.builder()
                .data(Arrays.asList(dateString1, dateString2))
                .startTargetPosition(PositionValue.builder()
                        .column("c")
                        .row(8)
                        .build())
                .build());
        List<KeyValueRequest> keyValues = new ArrayList<>();
        keyValues.add(new KeyValueRequest("NumeroConsultation", "Consultation Démo"));
        keyValues.add(new KeyValueRequest("PouvoirAdjudicateur", "Pouvoir Adjudicateur Démo"));
        keyValues.add(new KeyValueRequest("ObjetConsultation", "Objet Consultation Démo"));
        configuration.setStaticData(staticData);
        configuration.setPositions(positions);
        configuration.setKeyValues(keyValues);
        return configuration;
    }


}
