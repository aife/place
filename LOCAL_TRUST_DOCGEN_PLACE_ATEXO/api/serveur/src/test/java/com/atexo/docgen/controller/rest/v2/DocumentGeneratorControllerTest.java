package com.atexo.docgen.controller.rest.v2;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.controller.model.ObjectTestModel;
import org.apache.poi.util.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DocumentGeneratorControllerTest extends TestAbstractResources {

    @RegisterExtension
    public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");

    @Test
    public void testGenerate() throws Exception {
        File file = new File("src/test/resources/test-template.xlsx");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile template = new MockMultipartFile("template", file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("POUVOIR_ADJUDICACTEUR", "test");
        keyValues.put("objectTest", ObjectTestModel.builder().simpleString("simpleString").subObject(ObjectTestModel.SubObject.builder().simpleString("simpleTest").build()).subObjects(Collections.singletonList(ObjectTestModel.SubObject.builder().simpleString("simpleTest2").build())).build());
        MockMultipartFile keyValuesFile = new MockMultipartFile("keyValues", "", "application/json", objectMapper.writeValueAsString(keyValues).getBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart(path2 + "document-generator/generate").file(keyValuesFile).file(template)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM));
    }

}
