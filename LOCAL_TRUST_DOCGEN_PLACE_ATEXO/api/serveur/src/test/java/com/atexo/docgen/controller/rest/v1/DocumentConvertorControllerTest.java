package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.controller.model.ObjectTestModel;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import org.apache.poi.util.IOUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DocumentConvertorControllerTest extends TestAbstractResources {


    @RegisterExtension
    public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


    @Test
    @Disabled
    public void testFinalizeDocument() throws Exception {
        this.testConvertDocument();
        this.convert("docx");
        this.convert("pdf");
    }

    private void convert(String outputFormat) throws Exception {
        //Convert xml
        File file = new File("src/test/resources/convertor/test-template.docx_converted.docx");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile document = new MockMultipartFile("document",
                file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(input));

        MvcResult resultFile = mockMvc.perform(MockMvcRequestBuilders.multipart(path + "document-convertor/" + outputFormat
                + "/finalize")
                .file(document))
                .andExpect(status().isOk()).andReturn();
        assertNotNull(resultFile);
        assertNotNull(resultFile.getResponse());
        byte[] resource = resultFile.getResponse().getContentAsByteArray();
        assertNotNull(resource);
        File testResult = new File(file.getAbsolutePath() + "_finalize." + outputFormat);
        FileOutputStream fos = new FileOutputStream(testResult);
        fos.write(resource);
        fos.close();
        Assert.assertNotNull(testResult);
        assertTrue(testResult.exists());
    }

    public void testConvertDocument() throws Exception {
        //Convert xml
        File templateFile = new File("src/test/resources/convertor/test-template.docx");
        MockMultipartFile template = new MockMultipartFile("template",
                templateFile.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(new FileInputStream(templateFile)));

        File xmlFile = new File("src/test/resources/convertor/test.xml");
        MockMultipartFile xml = new MockMultipartFile("xml",
                xmlFile.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(new FileInputStream(xmlFile)));

        List<KeyValueRequest> keyValues = new ArrayList<>();
        keyValues.add(KeyValueRequest.builder().key("POUVOIR_ADJUDICACTEUR").value("test").build());
        keyValues.add(KeyValueRequest.builder().key("objectTest").value(
                ObjectTestModel.builder().simpleString("simpleString")
                        .subObject(ObjectTestModel.SubObject.builder().simpleString("simpleTest").build())
                        .subObjects(Collections.singletonList(ObjectTestModel.SubObject.builder().simpleString("simpleTest2")
                                .build()))
                        .build()
        ).build());
        MockMultipartFile keyValuesFile = new MockMultipartFile("keyValues", "",
                "application/json", objectMapper.writeValueAsString(keyValues).getBytes());

        MvcResult resultFile = mockMvc.perform(MockMvcRequestBuilders.multipart(path + "document-convertor/convert-xml")
                .file(template)
                .file(xml)
                .file(keyValuesFile))
                .andExpect(status().isOk()).andReturn();
        assertNotNull(resultFile);
        assertNotNull(resultFile.getResponse());
        byte[] resource = resultFile.getResponse().getContentAsByteArray();
        assertNotNull(resource);
        File testResult = new File(templateFile.getAbsolutePath() + "_converted.zip");
        FileOutputStream fos = new FileOutputStream(testResult);
        fos.write(resource);
        fos.close();
        Assert.assertNotNull(testResult);
        assertTrue(testResult.exists());
    }


}
