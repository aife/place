package com.atexo.docgen.controller.rest.v1;

import com.atexo.docgen.controller.TestAbstractResources;
import com.atexo.docgen.controller.config.MariaDbExtension;
import com.atexo.docgen.core.domain.extractor.model.ExcelConfiguration;
import com.atexo.docgen.core.domain.extractor.model.PositionToExtract;
import com.atexo.docgen.core.domain.extractor.model.PositionValue;
import com.atexo.docgen.core.domain.extractor.model.StaticData;
import com.atexo.docgen.core.domain.generator.model.KeyValueRequest;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DocumentExtractorControllerTest extends TestAbstractResources {
	@RegisterExtension
	public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension("docgen");


	@Test
	public void testExtract() throws Exception {
		MockMultipartFile soumissionaire1 = getTemplateFromFile("fichiers", "src/test/resources/extractor/soumissionaire1.xlsx");

		MockMultipartFile soumissionaire2 = getTemplateFromFile("fichiers", "src/test/resources/extractor/soumissionaire2.xlsx");


		MockMultipartFile template = getTemplateFromFile("template", "src/test/resources/extractor/Tableau-template-extractor.xlsx");
		MockMultipartFile configuration = new MockMultipartFile("configuration", "",
				"application/json", objectMapper.writeValueAsString(getConfiguration()).getBytes());
		MvcResult resultStatus = mockMvc.perform(MockMvcRequestBuilders.multipart(path + "document-extractor/extract-xlsx")
				.file(template)
				.file(soumissionaire1)
				.file(soumissionaire2)
				.file(configuration))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.andReturn();
		assertNotNull(resultStatus);
		assertNotNull(resultStatus.getResponse());
		byte[] resource = resultStatus.getResponse().getContentAsByteArray();
		assertNotNull(resource);
		File testResult = new File("src/test/resources/extractor/test.xlsx");
		FileOutputStream fos = new FileOutputStream(testResult);
		fos.write(resource);
		fos.close();
		Assert.assertNotNull(testResult);
		assertTrue(testResult.exists());

		// process for checking all values
		File expectedFile = new File("src/test/resources/extractor/expected-test-result.xlsx");

		assertXlsxEquals(expectedFile, testResult);

		// delete of result files
		Files.delete(testResult.toPath());
	}

	public static  MockMultipartFile getTemplateFromFile(String name, String pathFile) throws IOException {
		File fileTemplate = new File(pathFile);
		FileInputStream inputTemplate = new FileInputStream(fileTemplate);
		return new MockMultipartFile(name,
				fileTemplate.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, IOUtils.toByteArray(inputTemplate));
	}

	private ExcelConfiguration getConfiguration() {
		ExcelConfiguration configuration = ExcelConfiguration.builder().build();
		//soumissionaire
		List<PositionToExtract> positions = new ArrayList<>();
		positions.add(PositionToExtract.builder()
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(9)
						.build())
				.position(PositionValue.builder()
						.column("B")
						.row(8)
						.build())
				.sheetName("Synthèse").build());
		//Montant du forfait EUR HT
		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("C")
						.row(20)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(10)
						.build())
				.sheetName("Synthèse").build());

		//Montant du scénario de commandes EUR HT
		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("E")
						.row(20)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(11)
						.build())
				.sheetName("Synthèse").build());

		//Montant du scénario de commandes EUR TTC
		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("C")
						.row(21)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(12)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("E")
						.row(21)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(13)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("C")
						.row(22)
						.build())

				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(14)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("D")
						.row(22)
						.build())

				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(15)
						.build())
				.sheetName("Synthèse").build());

		positions.add(PositionToExtract.builder()
				.position(PositionValue.builder()
						.column("E")
						.row(22)
						.build())
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(16)
						.build())
				.sheetName("Synthèse").build());
		configuration.setPositions(positions);
		List<StaticData> staticData = new ArrayList<>();
		String pattern = "dd/MM/yyyy";

		DateFormat df = new SimpleDateFormat(pattern);

		Date date1 = Date.from(LocalDateTime.of(2020, 12, 2, 12, 45, 20).toInstant(ZoneOffset.UTC));
		Date date2 = Date.from(LocalDateTime.of(2020, 11, 2, 12, 45, 20).toInstant(ZoneOffset.UTC));

		String dateString1 = df.format(date1);
		String dateString2 = df.format(date2);
		staticData.add(StaticData.builder()
				.data(Arrays.asList(dateString1, dateString2))
				.startTargetPosition(PositionValue.builder()
						.column("c")
						.row(8)
						.build())
				.build());
		List<KeyValueRequest> keyValues = new ArrayList<>();
		keyValues.add(new KeyValueRequest("NumeroConsultation", "Consultation Démo"));
		keyValues.add(new KeyValueRequest("PouvoirAdjudicateur", "Pouvoir Adjudicateur Démo"));
		keyValues.add(new KeyValueRequest("ObjetConsultation", "Objet Consultation Démo"));
		configuration.setStaticData(staticData);
		configuration.setPositions(positions);
		configuration.setKeyValues(keyValues);
		return configuration;
	}

	public static void assertXlsxEquals(File actualFile, File expectedFile) throws IOException {
		XSSFWorkbook actualWorkbook = new XSSFWorkbook(new FileInputStream(actualFile));
		XSSFWorkbook expectedWorkbook = new XSSFWorkbook(new FileInputStream(expectedFile));
		assertEquals(expectedWorkbook.getNumberOfSheets(), actualWorkbook.getNumberOfSheets());
		for (int i = 0; i < expectedWorkbook.getNumberOfSheets(); i++) {
			XSSFSheet expectedSheet = expectedWorkbook.getSheetAt(i);
			XSSFSheet actualSheet = actualWorkbook.getSheetAt(i);
			int lastRow = expectedSheet.getLastRowNum();
			int firstRow = expectedSheet.getFirstRowNum();

			for (int rowIndex = firstRow; rowIndex < lastRow; rowIndex++) {
				XSSFRow expectedRow = expectedSheet.getRow(rowIndex);
				XSSFRow actualRow = actualSheet.getRow(rowIndex);
				if (expectedRow == null) {
					assertNull(expectedRow);
					assertNull(actualRow);
					continue;
				}
				int firstColumns = expectedRow.getFirstCellNum();
				int lastColumns = expectedRow.getLastCellNum();

				for (int columnIndex = firstColumns; columnIndex < lastColumns; columnIndex++) {
					XSSFCell expectedCell = expectedRow.getCell(columnIndex);
					XSSFCell actualCell = actualRow.getCell(columnIndex);
					if (expectedCell == null || !expectedCell.getCellType().equals(CellType.STRING)) {
						if (expectedCell == null) {
							assertNull(expectedCell);
							assertNull(actualCell);
						} else {
							assertEquals(expectedCell.getCellType(), actualCell.getCellType());
						}
						continue;
					}
					FormulaEvaluator actualEvaluator = actualWorkbook.getCreationHelper().createFormulaEvaluator();
					FormulaEvaluator expectedEvaluator = expectedWorkbook.getCreationHelper().createFormulaEvaluator();

					String expectedContent = expectedEvaluator.evaluate(expectedCell).formatAsString();
					String actualContent = actualEvaluator.evaluate(actualCell).formatAsString();
					assertEquals(expectedContent, actualContent);

				}
			}
		}
	}
}
