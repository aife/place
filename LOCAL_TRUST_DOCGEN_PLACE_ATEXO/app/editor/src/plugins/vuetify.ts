import Vue from "vue";
import Vuetify from "vuetify";

Vue.use(Vuetify);

export default new Vuetify({
    defaultAssets: {
        font: true,
        icons: 'md'
    },
    icons: {
        iconfont: 'md',
    }
});
