import Vue from 'vue';
import Router from 'vue-router';
import {JwtUtils} from "./core/utils/jwt.utils";
import {isNullOrUndefined} from "./core/utils/technical.utils";

Vue.use(Router);

var router = new Router({
    mode: 'history', //removes # (hashtag) from url
    fallback: true, //router should fallback to hash (#) mode when the browser does not support history.pushState
    routes: [
        {
            path: '/editeur-en-ligne/bienvenue',
            name: "welcome",
            component: () => import('./pages/WelcomePage.vue')
        }, {
            path: '/editeur-en-ligne/view',
            name: "view",
            component: () => import('./pages/EditionPage.vue'),
            meta: {
                requiresAuth: true,
                modes: ['view']
            }
        }, {
            path: '/editeur-en-ligne/edition',
            name: "edit",
            component: () => import('./pages/EditionPage.vue'),
            meta: {
                requiresAuth: true,
                modes: ['edit']
            }
        }, {
            path: '/editeur-en-ligne/redac',
            name: "redac",
            component: () => import('./pages/RedacPage.vue'),
            meta: {
                requiresAuth: true,
                modes: ['redac']
            }
        }, {
            path: '/editeur-en-ligne/template',
            name: "template",
            component: () => import('./pages/TemplatePage.vue'),
            meta: {
                requiresAuth: true,
                modes: ['template']
            }
        }, {
            path: '/editeur-en-ligne/validation',
            name: "validation",
            component: () => import('./pages/ValidationPage.vue'),
            meta: {
                requiresAuth: true,
                modes: ['validation']
            }
        },
        {
            path: '/*',
            redirect: {name: "welcome"}
        }
    ]
});


router.beforeEach((to, from, next) => {
    if (!isNullOrUndefined(from.query.token) && from.name !== 'welcome') {
        next({name: 'welcome', query: {token: from.query.token}});
    } else if (!isNullOrUndefined(to.query.token) && to.name !== 'welcome') {
        next({name: 'welcome', query: {token: to.query.token}});
    } else if (to.matched.some(record => record.meta.requiresAuth)) {
        const decodedToken: any = JwtUtils.parseJwt(sessionStorage.getItem('token'));
        if (!isNullOrUndefined(decodedToken) && !isNullOrUndefined(decodedToken.details)
            && to.meta && to.meta.modes.includes(decodedToken.details.mode)) {
            next();
        } else {
            next({name: 'welcome'});
        }

    } else {
        next();
    }

});

export default router;
