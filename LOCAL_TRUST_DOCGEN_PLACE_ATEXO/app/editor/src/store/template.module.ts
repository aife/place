import TemplateService from '../core/services/template.service';
import downloadFileFromResponse from "../core/utils/http-response.util";
import {ContentContainerModel} from "../core/models/content-container.model";
import {BlocTemplateRequestModel} from "../core/models/bloc-template-request.model";
import {AxiosResponse} from "axios";
import downloadJson from "../core/utils/download-json.util";

const initialState = {}

export const template = {
    namespaced: true,
    state: initialState,
    actions: {
        getBlocTemplate(context: any, id: Number): Promise<AxiosResponse<BlocTemplateRequestModel>> {
            return TemplateService.getBlocTemplate(id).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getDocVariable(context: any) {
            return TemplateService.getDocVariable().then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        generateDoc(context: any, keyValues: any) {
            return TemplateService.generateDoc(keyValues).then(
                response => {
                    downloadJson(keyValues, "champs-fusion-test");
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        generateTestRendu(context: any, keyValues: any) {
            return TemplateService.generateTestRendu(keyValues).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getDownloadUrl(context: any, id: number): string {
            return TemplateService.getDownloadUrl(id)
        },
        addBlocTemplate(context: any, params: BlocTemplateRequestModel) {
            const formData = new FormData();
            formData.append("blocTemplate", params.bodyFile)
            formData.append("request", new Blob([JSON.stringify(params)], {
                type: "application/json"
            }))
            return TemplateService.addBlocTemplate(formData).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    return Promise.reject(error);
                }
            ).catch((error) => {
                return Promise.reject(error);
            });
        },
        deleteBlocTemplate(context: any, bloc: ContentContainerModel) {
            if (!bloc || !bloc.Id) {
                return Promise.reject({error: "Id est null"});
            }
            return TemplateService.deleteBlocTemplate(bloc.Id).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            ).catch((error) => {
                return Promise.reject(error);
            });
        },
    }, mutations: {}
};
