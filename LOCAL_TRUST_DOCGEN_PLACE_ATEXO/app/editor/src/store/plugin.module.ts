import {ContentContainerRequestModel} from '@/core/models/content-container-request.model';
import {ContentContainerModel} from '@/core/models/content-container.model';
import {InlineContentContainerModel} from '@/core/models/inline-content-container.model';

const initialState = {}

export const plugin = {
    namespaced: true,
    state: initialState,
    actions: {
        insertContentContainer(context: any, replaceContent: Array<ContentContainerRequestModel>) {
            console.log("plugin module called for insert CF" + replaceContent[0].Props.Tag)
            sessionStorage.setItem("docgen.InsertAndReplaceContentControls",
                JSON.stringify({
                    date: new Date(),
                    value: replaceContent
                }));
        },
        addContentControl(context: any, replaceContent: InlineContentContainerModel) {
            console.log('plugin module called for addContentControl', replaceContent);
            sessionStorage.setItem('docgen.AddContentControl',
                JSON.stringify({
                    date: new Date(),
                    value: replaceContent
                }));
        },
        removeContentContainer(context: any, content: Array<ContentContainerModel>) {
            console.log("removeContentContainer called1 for " + content[0].InternalId)
            sessionStorage.setItem("docgen.RemoveContentControls",
                JSON.stringify({
                    date: new Date(),
                    value: content
                }));
        },
        goToContentContainer(context: any, content: Array<ContentContainerModel>) {
            console.log("goToContentContainer for " + content[0].InternalId)
            sessionStorage.setItem("docgen.MoveCursorToContentControl",
                JSON.stringify({
                    date: new Date(),
                    value: content
                }));
        }

    }, mutations: {}
};
