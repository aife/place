import {ClauseConvertRequestModel} from '../core/models/clause-convert-request.model';
import DocgenService from '../core/services/docgen.service';
import {ClauseUtils} from '@/core/utils/clause.utils';
import {DocumentModel} from '@/core/models/document.model';
import {ContentContainerRequestModel} from '@/core/models/content-container-request.model';
import {ContentContainerModel} from '@/core/models/content-container.model';
import {isNullOrUndefined} from '@/core/utils/technical.utils';

const initialState = {}

export const document = {
    namespaced: true,
    state: initialState,
    actions: {
        convertClause(context: any, params: ClauseConvertRequestModel) {
            params.clause.clausevalide = params.valid;
            return DocgenService.convertClause(params.clause, params.clause.chapitreNumero, params.document, params.fromFile)
                .then(response => {
                    if (!!response && response.status === 200) {
                        let downloadUrl = DocgenService.getConvertedClause(params.clause.ref, params.clause.chapitreNumero);
                        const contentColor = ClauseUtils.getClauseBackground(params.clause);
                        const lock = 2;
                        const alias = ClauseUtils.getAlias(params.clause);
                        const tag = JSON.stringify(ClauseUtils.getClauseTag(params.clause));
                        const contentContainerRequest = new ContentContainerRequestModel(new ContentContainerModel(
                            params.clause.contentParam.Id, params.clause.contentParam.InternalId,
                                tag, lock, alias, 1, contentColor, 'Clause ' + params.clause.ref),
                            window.location.origin + downloadUrl,
                            "docx"
                        );
                        return Promise.resolve(contentContainerRequest);
                    } else {

                        let validationDeLaClause = params.valid ? 'Validation' : 'Invalidation';
                        context.dispatch("application/setAlert", {
                            message: response.data.error,
                            type: 'error',
                            icon: 'error',
                            header: validationDeLaClause + ' de la clause ' + params.clause.ref,
                            button: 'Retour',
                            show: false,
                        }, {root: true})
                        return Promise.reject(null);
                    }
                });

        }
        ,
        updateDocumentConfiguration(context: any, documentModel: DocumentModel) {
            return DocgenService.updateDocumentConfiguration(documentModel)
                .then((reponse) => {
                    if (!isNullOrUndefined(reponse)) {
                        return Promise.resolve("OK")
                    } else {
                        return Promise.reject(null);
                    }

                });

        },
        saveVersion(context: any, document: DocumentModel) {
            return DocgenService.addVersion(document)
                .then((response) => {
                    if (!!response && response.status === 200) {
                        return Promise.resolve(response.data)
                    } else {
                        return Promise.reject(null);
                    }
                })
                .catch(() => Promise.reject(null));
        }
    }, mutations: {}
};
