import EditorService from '../core/services/editor.service';

class State {
    config: any
}

const initialState: State = {
    config: null
}

export const editor = {
    namespaced: true,
    state: initialState,
    actions: {
        getEditorConfiguration(context: any) {
            return EditorService.getEditorConfiguration().then(
                response => {
                    context.commit('setConfig', response.data)
                    return Promise.resolve(response.data);
                }
            ).catch(() => {
                context.commit('setConfig', null)
                return Promise.reject(null);
            });
        },
        forceUpdate(context: any, key: string) {
            return EditorService.forceSave(key)
        },
        getSurchargeI18n(context: any) {
            return EditorService.getSurchargeI18n().then(
                response => {
                    return Promise.resolve(response.data);
                }
            ).catch(() => {
                return Promise.reject(null);
            });
        }
    }, mutations: {
        setConfig(state: State, config: any) {
            state.config = config;
        }
    }
};
