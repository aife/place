import Vue from 'vue';
import Vuex from 'vuex';

import {template} from "./template.module";
import {plugin} from "./plugin.module";
import {editor} from "./editor.module";
import {document} from "./document.module";
import {application} from "./application.module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        editor, document, application, plugin, template
    }
});
