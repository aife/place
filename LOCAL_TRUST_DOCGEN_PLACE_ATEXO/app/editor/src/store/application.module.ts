import {MessageModel} from "../core/models/message.model";
import {AlertModel} from "../core/models/alert.model";
import {JwtUtils} from "../core/utils/jwt.utils";
import {isNullOrUndefined} from "../core/utils/technical.utils";

const initialState = {
    message: {
        snackbar: false,
        color: 'success',
        message: ''
    }, alert: {},
    token: null,
    exp: null,
    editionDetails: null,
    loading: false,
    start: new Date()
}

export const application = {
    namespaced: true,
    state: initialState,
    actions: {
        setLoading(context: any, loading: boolean) {
            context.commit('setLoading', loading)

        }, setAlert(context: any, alert: AlertModel) {
            context.commit('setAlert', alert)

        }, setMessage(context: any, message: MessageModel) {
            context.commit('setMessage', message)

        }, closeMessage(context: any) {
            context.commit('closeMessage')

        }, closeAlert(context: any) {
            context.commit('closeAlert')

        }, showAlert(context: any) {
            context.commit('showAlert')

        },
        setToken(context: any, token: string) {
            context.commit('setToken', token);
        }
    }, mutations: {
        setAlert(state: any, alert: AlertModel) {
            if (!alert) {
                return;
            }
            state.alert = alert;
        }, showAlert(state: any) {
            state.alert.show = true;
        }, closeAlert(state: any) {
            state.alert.show = false;
        }, setLoading(state: any, loading: boolean) {
            state.loading = loading;
        },
        setMessage(state: any, message: MessageModel) {
            if (!message) {
                return;
            }
            state.message = message;
        },
        closeMessage(state: any) {
            state.message.snackbar = false;
        },
        setToken(state: any, token: string) {
            if (!isNullOrUndefined(token))
                sessionStorage.setItem('token', token);
            state.token = token;
            const decodedToken: any = JwtUtils.parseJwt(token);
            if (!isNullOrUndefined(decodedToken)) {
                state.exp = decodedToken.exp;
                state.editionDetails = decodedToken.details;
            }
        }
    }
};
