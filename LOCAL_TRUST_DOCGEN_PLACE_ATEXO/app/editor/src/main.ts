import 'babel-polyfill'
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// In main.js
import LoadScript from 'vue-plugin-load-script';
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
import 'primeflex/primeflex.css';

import vuetify from "./plugins/vuetify";
import 'bootstrap/dist/css/bootstrap.css'
import './assets/css/styles.css'
import './assets/css/atx-app.css'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import api from "./core/services/api";
import i18n from "./i18n";

Vue.use(LoadScript);
Vue.prototype.$http = api;
new Vue({
    router,
    i18n,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');

