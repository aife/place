import axios from "axios";

const fr = require("./assets/i18n/fr.json")

const messages = {
    fr: fr
}
import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
    locale: 'fr',
    messages,
})
const API_URL_V1 = '/api-edition/document/';
const loadedLanguages = []
export default i18n;
function setI18nLanguage (lang: string) {
    i18n.locale = lang;
    axios.defaults.headers.common['Accept-Language'] = lang;
    return lang;
}

function stringToNestedJSON (inputString: string, value: any) {
    const keys = inputString.split('.');
    const result: any = {};

    let currentLevel = result;
    for (let i = 0; i < keys.length - 1; i++) {
        currentLevel[keys[i]] = {};
        currentLevel = currentLevel[keys[i]];
    }

    currentLevel[keys[keys.length - 1]] = value;

    return result;
}
export function loadLanguageAsync (lang: string, value: any) {

    let result = {};

    Object.keys(value).forEach(key => {
        console.log(key)
        const object = stringToNestedJSON(key, value[key])
        result = {
            ...i18n.messages.fr,
            ...object
        };

    });
    loadedLanguages.push(lang);
    setI18nLanguage(lang);
    i18n.mergeLocaleMessage(lang, result);


}

