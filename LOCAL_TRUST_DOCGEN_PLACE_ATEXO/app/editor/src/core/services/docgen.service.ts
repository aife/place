import axios from './api';
import {ClauseModel} from "../models/clause.model";
import {DocumentModel} from "../models/document.model";
import downloadFileFromResponse from "../utils/http-response.util";

const SERVER_URL = '/api-edition';
const SERVER_URL_CONVERTOR = SERVER_URL + '/document-convertor';

class DocgenService {
    convertClause(clause: ClauseModel, chapitreNumero: string, document: DocumentModel, fromFile: boolean) {
        let url = SERVER_URL_CONVERTOR + '/chapitre/' + chapitreNumero + '/convert-clause?fromFile=' + fromFile;
        return axios({
            method: 'post',
            url: url,
            data: {clause, document},
        }).then((reponse) => {
            return reponse;
        }).catch(error => {
            if (error.response) {
                // Request made and server responded
                return error.response;
            } else if (error.request) {
                // The request was made but no response was received
                return {status: 400, data: error.request};
            } else {
                // Something happened in setting up the request that triggered an Error
                return {status: 400, data: error.message};
            }
        });
    }

    getConvertedClause(clauseRef: string, chapitreNumero: string) {
        return SERVER_URL_CONVERTOR + '/chapitre/' + chapitreNumero + '/clause/' + clauseRef
            + "?token=" + sessionStorage.getItem('token');
    }

    getDocument(format: string) {
        const url = SERVER_URL_CONVERTOR + '/document/' + format + '?token=' + sessionStorage.getItem('token')
        axios({
            url: url,
            method: 'GET',
            responseType: 'blob',
        }).then((response) => {
            downloadFileFromResponse(response)
        });

    }

    updateDocumentConfiguration(document: DocumentModel) {
        return axios({
            method: 'post',
            url: SERVER_URL_CONVERTOR + '/document-configuration',
            data: document
        }).then(response => {

            return response.data;
        }).catch(() => {
            return null;
        });
    }

    addVersion(document: DocumentModel) {
        return axios({
            method: 'post',
            url: SERVER_URL_CONVERTOR + '/add-version',
            data: document
        })
    }

}

export default new DocgenService();
