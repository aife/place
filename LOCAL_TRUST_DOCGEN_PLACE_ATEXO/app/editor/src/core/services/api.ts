import axios from 'axios'

function getAxiosInstance() {
    let axiosInstance = axios.create({
        timeout: 5 * 60000
    });
    axiosInstance.interceptors.request.use(
        config => {
            config.headers.authorization = 'Bearer ' + sessionStorage.getItem("token");
            return config;
        },
        error => Promise.reject(error)
    );
    return axiosInstance;
}

export default getAxiosInstance()
