import axios from './api';

const API_URL_V1 = '/api-edition/document/';

class EditorService {

    getEditorConfiguration() {
        return axios
            .get(API_URL_V1 + 'configuration')
    }
    forceSave(key:string) {
        return axios
            .get(API_URL_V1 +key +'/force-save')
    }

    getSurchargeI18n() {
        return axios
            .get(API_URL_V1 + 'surcharge-libelle')
    }

    loadApis() {
        return API_URL_V1 + 'api?token=' + sessionStorage.getItem("token");
    }
}

export default new EditorService();
