import axios from './api';
import {BlocTemplateRequestModel} from "../models/bloc-template-request.model";
import {AxiosResponse} from "axios";

const API_URL_V1 = '/api-edition/document-template/';

class TemplateService {
    addBlocTemplate(formData: FormData) {
        return axios
            .post(API_URL_V1 + 'bloc-template', formData)
    }

    getBlocTemplate(id: Number): Promise<AxiosResponse<BlocTemplateRequestModel>> {
        return axios
            .get<BlocTemplateRequestModel>(API_URL_V1 + 'bloc-template/' + id)
    }

    deleteBlocTemplate(id: Number) {
        return axios
            .delete(API_URL_V1 + 'bloc-template/' + id)
    }

    getDownloadUrl(id: any) {
        return window.location.origin + API_URL_V1 + 'bloc-template/' + id + '/download?token=' + sessionStorage.getItem('token');
    }

    getDocVariable() {
        return axios
            .get(API_URL_V1 + 'variables')
    }

    generateDoc(keyValues: any) {
        return axios
            .post(API_URL_V1 + 'generate-test', keyValues, {
                responseType: "arraybuffer",
            })
    }

    generateTestRendu(keyValues: any) {
        return axios
            .post(API_URL_V1 + 'generate-pdf-test', keyValues, {
                responseType: "arraybuffer",
            })
    }

    previewChampComplexe(champ: any) {
        return axios
            .post(API_URL_V1 + 'preview-champ-complexe', champ, {
                responseType: "arraybuffer",
            })

    }

}

export default new TemplateService();
