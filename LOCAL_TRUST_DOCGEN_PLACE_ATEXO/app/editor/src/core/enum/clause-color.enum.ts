import {ColorModel} from "../models/color.model";

const ClauseColorEnum = {
    RED: new ColorModel(255, 0, 0),
    YELLOW: new ColorModel(255, 255, 0),
    GREEN: new ColorModel(0, 255, 0),
    WHITE: new ColorModel(255, 255, 255),
    BLACK: new ColorModel(0, 0, 0)
}

export default ClauseColorEnum;
