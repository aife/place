const DocumentStatusEnum = {
    BROUILLON: 1,
    A_VALIDER: 2,
    VALIDER: 3
}

export default DocumentStatusEnum;
