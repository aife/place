import {FormulairesListeTypeModel} from "./formulaires-liste-type.model";
import {ContentContainerModel} from "./content-container.model";
import {TexteModel} from "./texte.model";
import {InfoBulleModel} from "./info-bulle.model";
import {DerogationModel} from "@/core/models/derogation.model";

export class ClauseModel {
    constructor(public chapitreTitre: string,
                public chapitreNumero: string,
                public ref: string,
                public type: Number,
                public etat: Number,
                public texteFixeAvant: string,
                public texteFixeApres: string,
                public value: Number,
                public clauseformulationsmodifiable: boolean,
                public clausevalide: boolean,
                public initialise: boolean,
                public active: boolean,
                public derogationResume: boolean,
                public texte: TexteModel,
                public formulairesListeType: FormulairesListeTypeModel,
                public contentParam: ContentContainerModel,
                public infoBulle: InfoBulleModel,
                public derogation: DerogationModel,
                public chaptersInfoBulle: Array<InfoBulleModel>) {
    }


}
