/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.18.565 on 2024-01-24 15:05:28.

export interface ClauseTag {
    roles: string[];
    status: string;
}

export interface DocumentProgress {
    totalClause: number;
    clausesNonValides: number;
    progress: number;
}

export interface DocxDocument extends Document {
}

export interface ValidationClauseRequest {
    document: DocxDocument;
    clause: ClauseType;
}

export interface ChapitreType {
    infoBulle: InfoBulle;
    clause: ClauseType[];
    chapitre: ChapitreType[];
    derogation: DerogationType;
    id: number;
    style: string;
    numero: string;
    ref: string;
    titre: string;
    valide: boolean;
    derogationResume: boolean;
    nombreSautLignes: string;
}

export interface Document {
    chapitre: ChapitreType[];
    ref: string;
    id: string;
    status: number;
    commentaire: string;
    completed: boolean;
    derogeable: boolean;
    progress: number;
    outputFormat: string;
    lastAction: string;
    type: EnumerationDocumentType;
}

export interface ClauseType {
    derogation: DerogationType;
    texteFixeAvant: string;
    texteFixeApres: string;
    infoBulle: InfoBulle;
    formulairesListeType: FormulairesListe;
    texte: Texte;
    idReferentielValeurTableau: string;
    tableauRedaction: TableauRedactionType;
    derogationResume: boolean;
    type: number;
    id: number;
    chapitreTitre: string;
    chapitreNumero: string;
    ref: string;
    etat: number;
    version: number;
    active: boolean;
    clausevalide: boolean;
    initialise: boolean;
    clauseformulationsmodifiable: boolean;
    duplique: boolean;
    texteFixeAvantALaLigne: boolean;
    texteFixeAresALaLigne: boolean;
}

export interface InfoBulle {
    description: string;
    lien: LienType;
    actif: boolean;
}

export interface DerogationType {
    article: string;
    commentaire: string;
}

export interface FormulairesListe {
    formulation: Formulation[];
    exclusif: boolean;
}

export interface Texte {
    value: string;
    modifiable: boolean;
    obligatoire: boolean;
    taille: string;
}

export interface TableauRedactionType {
    textAvantTableau: string;
    documentXLS: DocumentXLS;
}

export interface LienType {
    url: string;
    description: string;
}

export interface Formulation {
    value: string;
    precoche: boolean;
    taille: string;
}

export interface DocumentXLS {
    onglet: Onglet[];
    versionMoteur: number;
}

export interface Onglet {
    nom: string;
    attributs: TableauAttributs;
    ligne: Ligne[];
    regionFusionnee: RegionFusionnee[];
    cellule: Cellule[];
    tableau: Tableau;
    numeroOnglet: number;
}

export interface TableauAttributs {
    largeurColonne: AttributLargeurColonne[];
}

export interface Ligne {
    cellule: Cellule[];
    regionFusionnee: RegionFusionnee[];
    numeroLigneInsertion: number;
    duplicationStyleLigne: number;
}

export interface RegionFusionnee {
    ligneDebut: number;
    colonneDebut: number;
    ligneFin: number;
    colonneFin: number;
}

export interface Cellule {
    coordonnee: CelluleCoordonnee;
    valeur: string;
    valeurDate: number;
    valeurNumerique: string;
    valeurBooleenne: boolean;
    valeurFormule: string;
    valeurUrlImage: string;
    style: CelluleStyle;
    type: EnumerationCelluleType;
    hauteur: number;
    format: string;
    bordureHaut: string;
    bordureBas: string;
    bordureGauche: string;
    bordureDroite: string;
    couleurPolice: number;
    taillePolice: number;
    couleurFond: number;
    gras: boolean;
    retourLigne: string;
}

export interface Tableau {
    styleEntete: CelluleStyle;
    styleCorp: CelluleStyle;
    reference: string;
    numeroLigneInsertion: number;
    numeroColonneInsertion: number;
    masquerRepetition: boolean;
}

export interface AttributLargeurColonne {
    colonne: number;
    largeur: number;
}

export interface CelluleCoordonnee {
    ligne: number;
    colonne: number;
}

export interface CelluleStyle {
    remplissage: CelluleStyleRemplissage;
    font: CelluleStyleFont;
    alignement: CelluleStyleAlignement;
    bordure: CelluleStyleBordure;
    retourLigne: boolean;
}

export interface CelluleStyleRemplissage {
    type: EnumerationRemplissage;
    premierPlan: EnumerationCouleur;
    arrierePlan: EnumerationCouleur;
}

export interface CelluleStyleFont {
    police: string;
    gras: boolean;
    italic: boolean;
    souligne: EnumerationFontSousligne;
    couleur: EnumerationCouleur;
    taille: number;
}

export interface CelluleStyleAlignement {
    rotation: number;
    horizontal: EnumerationAlignementHorizontal;
    vertical: EnumerationAlignementVertical;
}

export interface CelluleStyleBordure {
    haut: EnumerationBordure;
    bas: EnumerationBordure;
    gauche: EnumerationBordure;
    droite: EnumerationBordure;
}

export type EnumerationDocumentType = "PG" | "PF" | "ATT_1" | "ATT_2" | "OUV_3" | "OUV_4" | "OUV_5" | "OUV_6" | "OUV_7" | "OUV_8" | "OUV_9" | "OUV_10" | "NOTI_1" | "NOTI_3" | "NOTI_4" | "NOTI_5" | "NOTI_7" | "NOTI_8" | "DC_1" | "DC_2" | "DC_4" | "RC" | "CCAP" | "CCP" | "CCPAE" | "AE" | "DL";

export type EnumerationCelluleType = "CELL_TYPE_NUMERIC" | "CELL_TYPE_STRING" | "CELL_TYPE_FORMULA" | "CELL_TYPE_BLANK" | "CELL_TYPE_BOOLEAN" | "CELL_TYPE_ERROR";

export type EnumerationRemplissage = "NO_FILL" | "SOLID_FOREGROUND" | "FINE_DOTS" | "ALT_BARS" | "SPARSE_DOTS" | "THICK_HORZ_BANDS" | "THICK_VERT_BANDS" | "THICK_BACKWARD_DIAG" | "THICK_FORWARD_DIAG" | "BIG_SPOTS" | "BRICKS" | "THIN_HORZ_BANDS" | "THIN_VERT_BANDS" | "THIN_BACKWARD_DIAG" | "THIN_FORWARD_DIAG" | "SQUARES" | "DIAMONDS";

export type EnumerationCouleur = "BLACK" | "BROWN" | "OLIVE_GREEN" | "DARK_GREEN" | "DARK_TEAL" | "DARK_BLUE" | "INDIGO" | "GREY_80_PERCENT" | "ORANGE" | "DARK_YELLOW" | "GREEN" | "TEAL" | "BLUE" | "BLUE_GREY" | "GREY_50_PERCENT" | "RED" | "LIGHT_ORANGE" | "LIME" | "SEA_GREEN" | "AQUA" | "LIGHT_BLUE" | "VIOLET" | "GREY_40_PERCENT" | "PINK" | "GOLD" | "YELLOW" | "BRIGHT_GREEN" | "TURQUOISE" | "DARK_RED" | "SKY_BLUE" | "PLUM" | "GREY_25_PERCENT" | "ROSE" | "LIGHT_YELLOW" | "LIGHT_GREEN" | "LIGHT_TURQUOISE" | "PALE_BLUE" | "LAVENDER" | "WHITE" | "CORNFLOWER_BLUE" | "LEMON_CHIFFON" | "MAROON" | "ORCHID" | "CORAL" | "ROYAL_BLUE" | "LIGHT_CORNFLOWER_BLUE" | "TAN";

export type EnumerationFontSousligne = "U_NONE" | "U_SINGLE" | "U_DOUBLE" | "U_SINGLE_ACCOUNTING" | "U_DOUBLE_ACCOUNTING";

export type EnumerationAlignementHorizontal = "ALIGN_GENERAL" | "ALIGN_LEFT" | "ALIGN_CENTER" | "ALIGN_RIGHT" | "ALIGN_FILL" | "ALIGN_JUSTIFY" | "ALIGN_CENTER_SELECTION";

export type EnumerationAlignementVertical = "VERTICAL_TOP" | "VERTICAL_CENTER" | "VERTICAL_BOTTOM" | "VERTICAL_JUSTIFY";

export type EnumerationBordure = "BORDER_NONE" | "BORDER_THIN" | "BORDER_MEDIUM" | "BORDER_DASHED" | "BORDER_HAIR" | "BORDER_THICK" | "BORDER_DOUBLE" | "BORDER_DOTTED" | "BORDER_MEDIUM_DASHED" | "BORDER_DASH_DOT" | "BORDER_MEDIUM_DASH_DOT" | "BORDER_DASH_DOT_DOT" | "BORDER_MEDIUM_DASH_DOT_DOT" | "BORDER_SLANTED_DASH_DOT";
