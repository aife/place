import {ChapitreModel} from "./chapitre.model";

export class DocumentModel {
    constructor(public outputFormat: string,
                public commentaire: string,
                public progress: number,
                public status: number,
                public lastAction: number,
                public derogeable: boolean,
                public chapitre: Array<ChapitreModel>) {
    }

}
