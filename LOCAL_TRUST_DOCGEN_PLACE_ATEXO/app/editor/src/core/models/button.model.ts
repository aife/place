export class ButtonModel {
    constructor(public label: string,
                id: string,
                public color: string,
                public icon: string,
                public show: boolean,
                public disabled: boolean,
                public clickChannel: string,
    ) {
    }


}
