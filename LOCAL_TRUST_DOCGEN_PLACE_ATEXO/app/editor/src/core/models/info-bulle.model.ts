export class InfoBulleModel {

    constructor(
        public description: string,
        public actif: boolean) {
    }
}
