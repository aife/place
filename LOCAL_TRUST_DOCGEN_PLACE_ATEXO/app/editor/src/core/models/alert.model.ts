export class AlertModel {
    constructor(public show: boolean,
                public message: string,
                public header: string,
                public icon: string,
                public button: string,
                public type: string) {
    }
}
