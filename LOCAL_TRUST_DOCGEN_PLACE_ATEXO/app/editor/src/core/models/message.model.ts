export class MessageModel {
    constructor(public snackbar: boolean,
                public color: string,
                public message: string) {
    }
}
