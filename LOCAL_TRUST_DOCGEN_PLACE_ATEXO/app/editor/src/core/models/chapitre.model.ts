import {ClauseModel} from "./clause.model";
import {InfoBulleModel} from "./info-bulle.model";

export class ChapitreModel {

    constructor(
        public clause: Array<ClauseModel>,
        public chapitre: Array<ChapitreModel>,
        public infoBulle: InfoBulleModel) {
    }
}
