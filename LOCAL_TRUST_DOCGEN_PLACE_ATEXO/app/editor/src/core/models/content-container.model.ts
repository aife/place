import {ColorModel} from './color.model';

export class ContentContainerModel {
    constructor(public Id: number, public InternalId: string,
                public Tag: string, public Lock: number, public Alias: string, public Appearance: number,
                public Color: ColorModel, public PlaceHolderText: string) {
    }
}
