import {ClauseModel} from "./clause.model";
import {DocumentModel} from "./document.model";

export class ClauseConvertRequestModel {
    constructor(
        public clause: ClauseModel,
        public document: DocumentModel,
        public valid: boolean,
        public fromFile: boolean) {
    }


}
