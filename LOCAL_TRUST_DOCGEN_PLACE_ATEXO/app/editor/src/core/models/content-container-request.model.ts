import {ContentContainerModel} from "./content-container.model";

export class ContentContainerRequestModel {
    constructor(public Props: ContentContainerModel, public Url?: string, public Format?: string, public Script?: string) {
    }
}
