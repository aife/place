export class TemplateRequestModel {
    public constructor(public body: string,
                       public bodyFile: File,
                       public reference: string,
                       public champsFusion: string,
                       public id: number) {
    }
}
