import {ContentContainerModel} from '@/core/models/content-container.model';

export class InlineContentContainerModel {
    constructor(public content: ContentContainerModel, public nContentControlType: number) {
    }
}
