import {ButtonModel} from "./button.model";

export class ButtonGroupModel {
    constructor(public show: boolean,
                id: string,
                buttons: Array<ButtonModel>
    ) {
    }


}
