export class BlocTemplateRequestModel {
    constructor(
        public id: number,
        public bodyText: string,
        public bodyFile: File,
        public reference: string,
        public champs: string
    ) {
    }


}
