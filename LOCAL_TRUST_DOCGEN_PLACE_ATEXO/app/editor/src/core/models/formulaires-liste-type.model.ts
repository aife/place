import {FormulaireModel} from "./formulaire.model";

export class FormulairesListeTypeModel {
    constructor(public formulation: Array<FormulaireModel>,
                public exclusif: boolean) {
    }
}
