

function downloadJson(response: any, filename:string) {

        let blob = new Blob([JSON.stringify(response)],
            {type: "text/plain"});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename+".json");
        document.body.appendChild(link);
        link.click();
        if (!!link.parentNode)
            link.parentNode.removeChild(link)

}

export default downloadJson;
