import Ramda from 'ramda';

export function isNullOrUndefined(obj: any) {
    return !obj

}

/**
 * get the property of an object according to the path supplied
 * @returns undefined if the path is wrong or the object is null or undefined
 * Usage :  dotPath('a.b', {a:{b:'hello'}}) -> 'hello'
 *          dotPath('a.b', {a:{b:null}}) -> null
 *          dotPath('a.b', {a:{b:undefined}}) -> undefined
 *          dotPath('a.b', {a:{b:0}}) -> 0
 *          dotPath('a.b', {a:{b:[]}}) -> []
 *          dotPath('a.b', null) -> undefined
 * @returns undefined if the path doesn't exist or if the object is null or undefined
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function dotPath(dottedPath: string, obj: object): any {
    return Ramda.useWith(Ramda.path, [Ramda.split('.')])(dottedPath, obj);
}

/**
 * Check if an object is falsy or if Ramda.all of its properties are empty  null or undefined
 * @returns true if the object is falsy or if Ramda.all of its properties are empty  null or undefined
 * @param obj the object to be checked
 */
export function isEmptyObject(obj: object): boolean {
    return !obj || Ramda.all(Ramda.either(Ramda.isNil, Ramda.isEmpty), Ramda.values(obj));
}

/**
 * Check if an object is truthy and if Ramda.all of its properties are truthy
 * @returns false if the object is falsy or if one of its properties is falsy
 * @param obj the object to be checked
 */
export function isTruthyObject(obj: object): boolean {
    return !!obj && Ramda.all(Ramda.identity, Ramda.values(obj));
}

/**
 * Check if a given property of an object is null or undefined
 * @returns true if the property doesn't exist or if the object is null or undefined
 * @param property the property to be checked
 * @param obj the object to check the property from
 */
export function isNilProp(property: string, obj: object): boolean {
    return Ramda.isNil(Ramda.prop(property, obj));
}

/**
 * Check if a property of an object is null or undefined according to the path supplied
 * Usage :  isNilDotPath('a.b', {a:{b:null}}) -> true
 *          isNilDotPath('a.b', {a:{b:undefined}}) -> true
 *          isNilDotPath('a.b', {a:{b:0}}) -> false
 *          isNilDotPath('a.b', {a:{b:[]}}) -> true
 *          isNilDotPath('a.b', null) -> true
 * @returns true if the path doesn't exist or if the object is null or undefined
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function isNilDotPath(dottedPath: string, obj: object): boolean {
    return Ramda.isNil(dotPath(dottedPath, obj));
}

/**
 * Check if a given property of an object is empty
 * @returns true if the given label is its type's empty label
 * @returns false if the property does not exist
 * @param property the property to be checked
 * @param obj the object to check the property from
 */
export function isEmptyProp(property: string, obj: object | Array<any>): boolean {
    return Ramda.isEmpty(Ramda.prop(property, obj));
}

/**
 * Check if a property of an object is empty according to the path supplied
 * Usage :  isEmptyDotPath('a.b', {a:{b:''}}) -> true
 *          isEmptyDotPath('a.b', {a:{b:[]}}) -> true
 *          isEmptyDotPath('a.b', {a:{b:{}}}) -> true
 *          isEmptyDotPath('a.b', {a:{b:null}}) -> false
 *          isEmptyDotPath('a.b', []) -> false
 * @returns true if the given label is its type's empty label
 * @returns false if the property does not exist
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function isEmptyDotPath(dottedPath: string, obj: any): boolean {
    return Ramda.isEmpty(dotPath(dottedPath, obj));
}

/**
 * Check if an object is empty, null or undefined
 * @returns true if the object is empty, null or undefined
 * @param obj the object to be checked
 */
export function isEmptyOrNil(obj: any): boolean {
    return Ramda.either(Ramda.isNil, Ramda.isEmpty)(obj);
}

/**
 * Check if a given property of an object is empty, null or undefined
 * @returns true if the property doesn't exist or if the object is null or undefined
 * @returns true if the object is empty, null or undefined
 * @param property the property to be checked
 * @param obj the object to check the property from

 */
export function isEmptyOrNilProp(property: string, obj: object): boolean {
    return Ramda.either(Ramda.isNil, Ramda.isEmpty)(Ramda.prop(property, obj));
}

/**
 * Check if a property of an object is empty, null or undefined according to the path supplied
 * Usage :  isEmptyOrNilDotPath('a.b', {a:{b:''}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:[]}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:{}}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:null}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:1}}) -> true
 * @returns true if the path doesn't exist or if the object is null or undefined
 * @returns true if the object is empty, null or undefined
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function isEmptyOrNilDotPath(dottedPath: string, obj: object): boolean {
    return Ramda.either(Ramda.isNil, Ramda.isEmpty)(dotPath(dottedPath, obj));
}

export function sleep(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
