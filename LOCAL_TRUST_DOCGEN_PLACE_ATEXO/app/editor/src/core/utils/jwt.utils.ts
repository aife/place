import {isNullOrUndefined} from "./technical.utils";
import jwt_decode from "jwt-decode";

export class JwtUtils {
    public static parseJwt(token: any) {
        if (isNullOrUndefined(token)) {
            return null;
        }
        return jwt_decode(token)
    }

    public static checkTokenValidity(exp: any) {
        if (isNullOrUndefined(exp))
            return false;
        const elapsed = exp * 1000 - Date.now();
        return elapsed > 0;
    }

    public static checkTokenWarning(exp: any) {
        if (isNullOrUndefined(exp))
            return false;
        const elapsed = exp * 1000 - Date.now();
        var difference = new Date(elapsed);
        console.log("Token encore valide pour", difference.getUTCHours(), "heure(s) et", difference.getMinutes(), "minute(s)")
        return difference.getUTCHours() === 0 && difference.getMinutes() < 30 && difference.getMinutes() !== 0;
    }

}
