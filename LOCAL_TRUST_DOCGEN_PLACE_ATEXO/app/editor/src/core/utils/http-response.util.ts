function downloadFileFromResponse(response: any) {
    console.log("entré dans download")
    if (response.data) {
        console.log("avec data")
        var filename = getFileName(response);
        console.log("name " + filename);
        if (!filename)
            return;
        let blob = new Blob([response.data],
            {type: response.headers['content-type']});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        link.click();
        if (!!link.parentNode)
            link.parentNode.removeChild(link)
    }
}
function getFileName(response: any) {
    let filename = null;
    const disposition = response.headers['content-disposition'];
    if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
    }
    return filename;
}

export default downloadFileFromResponse;
