import {ClauseModel} from "../models/clause.model";
import ClauseTypeEnum from "../enum/clause-type.enum";
import {ClauseStatusEnum} from "../enum/clause-status.enum";
import ClauseColorEnum from "../enum/clause-color.enum";
import {UserRoleEnum} from "../enum/user-role.enum";
import {ClauseTagModel} from "../models/clause-tag.model";
import {isNullOrUndefined} from "./technical.utils";

export class ClauseUtils {
    static getClauseStatus(clause: ClauseModel) {

        switch (clause.type) {
            case ClauseTypeEnum.TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case ClauseTypeEnum.TYPE_CLAUSE_LISTE_MULTIPLE:
                if (clause.clausevalide) {
                    return ClauseStatusEnum.VALIDE;
                } else if (clause.initialise) {
                    return ClauseStatusEnum.INVALIDE;
                } else {
                    return ClauseStatusEnum.INITIALE;
                }
            case ClauseTypeEnum.TYPE_CLAUSE_TEXTE_LIBRE:
            case ClauseTypeEnum.TYPE_CLAUSE_TEXTE_PREVALORISE:
                if (clause.clausevalide) {
                    return ClauseStatusEnum.VALIDE;
                } else {
                    return clause.texte.obligatoire ? ClauseStatusEnum.INVALIDE_OBLIGATOIRE : ClauseStatusEnum.INVALIDE;
                }
            default:
                return ClauseStatusEnum.VALIDE;
        }
    }

    static getClauseBackground(clause: ClauseModel) {

        switch (clause.type) {
            case ClauseTypeEnum.TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case ClauseTypeEnum.TYPE_CLAUSE_LISTE_MULTIPLE:
                if (clause.clausevalide) {
                    return ClauseColorEnum.GREEN;
                } else if (clause.initialise) {
                    return ClauseColorEnum.RED;
                } else {
                    return ClauseColorEnum.YELLOW;
                }
            case ClauseTypeEnum.TYPE_CLAUSE_TEXTE_LIBRE:
            case ClauseTypeEnum.TYPE_CLAUSE_TEXTE_PREVALORISE:
                if (clause.clausevalide) {
                    return ClauseColorEnum.GREEN;
                } else {
                    return ClauseColorEnum.RED;
                }
            default:
                return ClauseColorEnum.WHITE;
        }
    }

    static getClauseLock(clause: ClauseModel) {

        switch (clause.type) {
            case ClauseTypeEnum.TYPE_CLAUSE_LISTE_EXCLUSIVE:
            case ClauseTypeEnum.TYPE_CLAUSE_LISTE_MULTIPLE:
                if (clause.clausevalide) {
                    return 1;
                } else if (clause.initialise) {
                    return ClauseColorEnum.RED;
                } else {
                    return ClauseColorEnum.YELLOW;
                }
            case ClauseTypeEnum.TYPE_CLAUSE_TEXTE_LIBRE:
            case ClauseTypeEnum.TYPE_CLAUSE_TEXTE_PREVALORISE:
                if (clause.clausevalide) {
                    return ClauseColorEnum.GREEN;
                } else {
                    return ClauseColorEnum.RED;
                }
            default:
                return ClauseColorEnum.WHITE;
        }
    }

    static isFormultation(clauseModel: ClauseModel) {
        return !!clauseModel && !!clauseModel.formulairesListeType && !!clauseModel.formulairesListeType.formulation && clauseModel.formulairesListeType.formulation.length > 0;
    }

    static getAlias(clause: ClauseModel) {
        let alias = "CHAPITRE " + clause.chapitreNumero + ' CLAUSE ' + clause.ref + ' - ' +
            ClauseUtils.getClauseStatus(clause);
        if (!isNullOrUndefined(clause) && !isNullOrUndefined(clause.chaptersInfoBulle) &&
            clause.chaptersInfoBulle.length > 0) {
            alias += ' - (voir info bulle)'
        }
        return alias;
    }

    static getClauseTag(clause: ClauseModel) {
        const roles = this.isClauseModifiable(clause) ? [UserRoleEnum.MODIFICATION] : [UserRoleEnum.REPRENDRE];
        return new ClauseTagModel(roles, this.getClauseStatus(clause))
    }

    static isClauseModifiable(clause: ClauseModel) {
        return !!clause && clause.active &&
            clause.etat < 2 &&
            ((!!clause.formulairesListeType && !!clause.formulairesListeType.formulation && clause.formulairesListeType.formulation.length > 0)
                || (!!clause.texte && clause.texte.modifiable));
    }

    static isClauseDerogeable(clause: ClauseModel) {
        return !!clause && clause.active &&
            clause.etat < 2 && clause.derogation;
    }

    static canValidate(clause: ClauseModel) {
        if (isNullOrUndefined(clause)) {
            return false;
        }
        if (clause.type === ClauseTypeEnum.TYPE_CLAUSE_TEXTE_LIBRE && clause.texte.obligatoire && !clause.initialise && !clause.clausevalide) {
            return false;
        }
        return !clause.clausevalide;

    }
}
