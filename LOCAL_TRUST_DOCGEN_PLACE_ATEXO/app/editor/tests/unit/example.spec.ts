import {shallowMount} from '@vue/test-utils'
import Edition from '@/components/Edition.vue'

describe('Edition.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(Edition, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
