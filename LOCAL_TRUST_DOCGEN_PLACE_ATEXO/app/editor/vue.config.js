module.exports = {
    devServer: {
        proxy: {
            "/api-edition/": {
                target: "http://editeur-en-ligne-docker.local-trust.com/"
            }
        },
        port: 4200
    }
}
