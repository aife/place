import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

var router = new Router({
    mode: 'history', //removes # (hashtag) from url
    base: '/admin',
    fallback: true, //router should fallback to hash (#) mode when the browser does not support history.pushState

    routes: [

        {
            path: '/',
            redirect: {name: 'dashboard'}
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('./pages/Home.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/convertor',
            name: "convertor",
            component: () => import('./pages/Convertor.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/analyse',
            name: "analyse",
            component: () => import('./pages/Analyse.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/generator',
            name: "generator",
            component: () => import('./pages/Generator.vue'),
            meta: {
                requiresAuth: true
            }
        }, {
            path: '/editor',
            name: "editor",
            component: () => import('./pages/Editor.vue'),
            meta: {
                requiresAuth: true
            }
        }, {
            path: '/extractor',
            name: "extractor",
            component: () => import('./pages/Extractor.vue'),
            meta: {
                requiresAuth: true
            }
        }, {
            path: '/template',
            name: "template",
            component: () => import('./pages/Template.vue'),
            meta: {
                requiresAuth: true
            }
        }, {
            path: '/validation',
            name: "validation",
            component: () => import('./pages/Validation.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/swagger',
            component: () => import('./pages/Swagger.vue'),
            name: 'swagger',
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./pages/Login.vue')
        },
        {
            path: '*',
            name: 'dashboard'
        }
    ],
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!!localStorage.getItem('token')) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})
export default router;
