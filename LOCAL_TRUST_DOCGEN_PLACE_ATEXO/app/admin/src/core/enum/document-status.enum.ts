export enum DocumentStatusEnum {
    ND = 0,
    BROUILLON = 1,
    A_VALIDER = 2,
    VALIDER = 3
}

