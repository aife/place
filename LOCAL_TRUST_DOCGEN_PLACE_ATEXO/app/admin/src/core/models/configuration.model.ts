import PositionModel from "./position.model";
import ConsultationModel from "./consultation.model";

export default class ConfigurationModel {
    constructor(public soumissionnaireDebut?: PositionModel,
                public apercuDebut?: PositionModel,
                public titreLot?: PositionModel,
                public critereDebut?: PositionModel,
                public critereFin?: PositionModel,
                public celluleControle?: PositionModel,
                public meta?: ConsultationModel) {
    }
}
