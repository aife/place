export default class EditorConvertModel {
    public template: File;
    public xml: File;
    public keyValues: Array<any>;

    constructor() {
        this.template = new File([], "");
        this.xml = new File([], "");
        this.keyValues = [];

    }

}
