import {ChampsFusionsModel} from "@/core/models/champs-fusions.model";
import DonneTestModel from "@/core/models/donne-test.model";

export class ConfigurationAdminitrableRequestModel {

    public champsFusions?: ChampsFusionsModel[];
    public donneesTests?: DonneTestModel[];
    constructor() {

    }
}
