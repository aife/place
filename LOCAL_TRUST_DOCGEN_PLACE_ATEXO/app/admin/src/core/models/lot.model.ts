import CritereModel from "./critere.model";
import SoumissionnaireModel from "./soumissionnaire.model";

export default class LotModel {

    constructor(public intitule?: string, public numero?: number,
                public criteres?: Array<CritereModel>, public soumissionnaires?: Array<SoumissionnaireModel>) {
    }
}
