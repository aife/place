import generateUUID from "../utils/guid.util";

export default class EditorRequestModel {
    public file: File;
    public editorRequest: any;

    constructor() {
        this.file = new File([], "");
        this.editorRequest = {
            user: {id: generateUUID(), name: 'Atexo', roles: []},
            mode: 'edit',
            documentId: generateUUID(),
            callback: window.location.origin + "/docgen/api.php/v1/document-edition/callback",
            documentTitle: null,
            plateformeId: "mock-docgen",
            version: 500,
            configuration: {
                defaultSheetName: null,
                activatedSheetNames: null
            }

        };
    }

}
