export default class FileFinalizeModel {
    public document: File;
    public outputFormat: string;
    public outputFileName!: string;
    public inputFormat!: string;
    constructor() {
        this.document = new File([], "");
        this.outputFormat = "pdf";
    }

}
