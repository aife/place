import {ConfigurationAdminitrableRequestModel} from "@/core/models/configuration-adminitrable-request.model";
import {ChampsFusionsModel} from "@/core/models/champs-fusions.model";

export class RequestModel {
    public user?: any;
    public mode?: string;
    public documentId?: string;
    public callback?: string;
    public documentTitle?: string | null;
    public plateformeId?: string;
    public version?: number;
    public documentAdministrableConfiguration?: ConfigurationAdminitrableRequestModel;

    constructor() {

    }
}
