import generateUUID from "../utils/guid.util";
import {DocumentStatusEnum} from "../enum/document-status.enum";

export default class EditorValidationRequestModel {
    public document: File;
    public editorRequest: any;

    constructor() {
        this.document = new File([], "");
        this.editorRequest = {
            user: {id: generateUUID(), name: 'Atexo', roles: ['modification','validation']},
            mode: 'validation',
            documentId: generateUUID(),
            callback:window.location.origin+"/docgen/api.php/v1/document-edition/callback",
            documentTitle: null,
            plateformeId: "mock-docgen",
            status: DocumentStatusEnum.BROUILLON,
            version:50
        };
    }

}
