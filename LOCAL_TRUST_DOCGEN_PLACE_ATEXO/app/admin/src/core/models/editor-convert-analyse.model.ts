import ConsultationModel from "./consultation.model";
import LotModel from "./lot.model";
import CritereModel from "./critere.model";
import SoumissionnaireModel from "./soumissionnaire.model";

export default class EditorConvertAnalyseModel {
    public template: File;
    public meta: ConsultationModel;
    constructor() {
        this.template = new File([], "");
        this.meta = new ConsultationModel("Fourniture de papier à imprimante", "2020V09006012",
            [new LotModel("Menuiseries métalliques", 1,
                [
                    new CritereModel("critere 1", 10,
                        [
                            new CritereModel("Moyens mis en place pour le chantier :\n" +
                                "•\tEffectif mis à disposition, composition des équipes, qualification du personnel, équivalent temps plein, CV des encadrants.\n" +
                                "•\tDécomposition des travaux réalisés en propre et ceux sous traités ; critères sélections, maitrise, gestion et encadrement des sous–traitants ;  liste des sous-traitants déclarés ou pressentis (avec leur référence, qualification, moyens humains, moyens matériels, assurances et divers documents administratifs), méthodologie de déclaration des sous-traitants.\n" +
                                "•\tRespect des préconisations du CCTP ; niveau de précision des fiches techniques de compositions ou d’utilisations des produits et matériaux mis en œuvre ; conformité avec les différentes études et rapports fournis (étude thermique, diagnostic amiante …)  - Qualité du DPGF et niveau de détail correspondant aux postes repris. \n" +
                                "Toute remarque utile du candidat, les éventuelles incompréhensions du dossier, les sources d’erreurs ou de  supplément de prix.\n" +
                                "•\tMoyens mis en place pour assurer la sécurité et l’hygiène sur le chantier, la charte chantier propre.\n", 5),
                            new CritereModel("Sous critères 2", 5)
                        ]),
                    new CritereModel("Critère 2", 80)
                ], [new SoumissionnaireModel("32941716600017", "Gilles Menuiserie"),
                    new SoumissionnaireModel("43121846000027", "les petits ateliers du bois")]), new LotModel("autre lot", 2,
                [
                    new CritereModel("critere 1", 10,
                        [
                            new CritereModel("Moyens mis en place pour le chantier :\n" +
                                "•\tEffectif mis à disposition, composition des équipes, qualification du personnel, équivalent temps plein, CV des encadrants.\n" +
                                "•\tDécomposition des travaux réalisés en propre et ceux sous traités ; critères sélections, maitrise, gestion et encadrement des sous–traitants ;  liste des sous-traitants déclarés ou pressentis (avec leur référence, qualification, moyens humains, moyens matériels, assurances et divers documents administratifs), méthodologie de déclaration des sous-traitants.\n" +
                                "•\tRespect des préconisations du CCTP ; niveau de précision des fiches techniques de compositions ou d’utilisations des produits et matériaux mis en œuvre ; conformité avec les différentes études et rapports fournis (étude thermique, diagnostic amiante …)  - Qualité du DPGF et niveau de détail correspondant aux postes repris. \n" +
                                "Toute remarque utile du candidat, les éventuelles incompréhensions du dossier, les sources d’erreurs ou de  supplément de prix.\n" +
                                "•\tMoyens mis en place pour assurer la sécurité et l’hygiène sur le chantier, la charte chantier propre.\n", 5),
                            new CritereModel("Sous critères 2", 5)
                        ]),
                    new CritereModel("Critère 2", 80)
                ], [new SoumissionnaireModel("32941716600017", "Gilles Menuiserie"),
                    new SoumissionnaireModel("43121846000027", "les petits ateliers du bois")])],
        );

    }
}
