import LotModel from "./lot.model";
import CritereModel from "./critere.model";
import SoumissionnaireModel from "./soumissionnaire.model";

export default class ConsultationModel {

    constructor(public intitule?: string, public reference?: string,
                public lots?: Array<LotModel>,
                public criteres?: Array<CritereModel>,
                public soumissionnaires?: Array<SoumissionnaireModel>,
                public objet?: string
    ) {
    }
}
