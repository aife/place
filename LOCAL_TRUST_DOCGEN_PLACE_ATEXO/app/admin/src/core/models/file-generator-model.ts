export default class FileGeneratorModel {
    public template: File;
    public convertToPdf: boolean;
    public keyValues: Array<any>;

    constructor() {
        this.template = new File([], "");
        this.keyValues = [];
        this.convertToPdf = false;
    }

}
