export default class UserModel {

    constructor(public username: string, public  email: string, public  password: string
        , public  token: string) {
    }
}
