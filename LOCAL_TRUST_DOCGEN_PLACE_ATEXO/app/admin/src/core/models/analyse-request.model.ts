import ConfigurationModel from "./configuration.model";
import PositionModel from "./position.model";
import ConsultationModel from "./consultation.model";

export default class AnalyseRequestModel {
    public document: File;
    public outputFileName!: string;
    public configuration!: ConfigurationModel;

    constructor() {
        this.document = new File([], "");
        this.configuration = new ConfigurationModel(
            new PositionModel("E", 3), //soumissionnaireDébut
            new PositionModel("B", 4),
            new PositionModel("B", 3),
            new PositionModel("B", 5),
            new PositionModel("B", 7),
            new PositionModel("C", 12),
            new ConsultationModel());
    }

}
