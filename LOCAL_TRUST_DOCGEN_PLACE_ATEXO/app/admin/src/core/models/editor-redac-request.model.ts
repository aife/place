import generateUUID from "../utils/guid.util";

export default class EditorRedacRequestModel {
    public document: File;
    public xml: File;
    public editorRequest: any;
    constructor() {
        this.xml = new File([], "");
        this.document = new File([], "");
        this.editorRequest = {
            user: {id: generateUUID(), name: 'Atexo', roles: ['modification','validation']},
            mode: 'redac',
            documentId: generateUUID(),
            callback:window.location.origin+"/docgen/api.php/v1/document-edition/callback",
            documentTitle: null,
            plateformeId: "mock-docgen",
            version:5
        };
    }

}
