export default class TemplateModel {
    public template: File;
    public blocTemplates: any[];
    constructor() {
        this.template = new File([], "");
        this.blocTemplates = []
    }

}
