export default class FileExtractorModel {

    public template: File;
    public fichiers: Array<File>;
    public configuration: any;

    constructor() {
        this.template = new File([], "");
        this.configuration = {
            keyValues: [],
            positions: [{
                position: {column: "B", row: 8},
                startTargetPosition: {column: "C", row: 9},
                sheetName: "Synthèse"
            }, {
                position: {column: "C", row: 20},
                startTargetPosition: {column: "C", row: 10},
                sheetName: "Synthèse"
            }, {
                position: {column: "E", row: 20},
                startTargetPosition: {column: "C", row: 11},
                sheetName: "Synthèse"
            }, {
                position: {column: "C", row: 21},
                startTargetPosition: {column: "C", row: 12},
                sheetName: "Synthèse"
            }, {
                position: {column: "E", row: 21},
                startTargetPosition: {column: "C", row: 13},
                sheetName: "Synthèse"
            }, {
                position: {column: "C", row: 22},
                startTargetPosition: {column: "C", row: 14},
                sheetName: "Synthèse"
            }, {
                position: {column: "D", row: 22},
                startTargetPosition: {column: "C", row: 15},
                sheetName: "Synthèse"
            }, {
                position: {column: "E", row: 22},
                startTargetPosition: {column: "C", row: 16},
                sheetName: "Synthèse"
            }],
            staticData: []
        };
        this.fichiers = []
    }

}
