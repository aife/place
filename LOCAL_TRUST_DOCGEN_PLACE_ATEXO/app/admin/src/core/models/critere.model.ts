import PositionModel from "./position.model";

export default class CritereModel {

    constructor(public description?: string, public maximumNote?: number, public sousCriteres?: Array<CritereModel>, public position?: PositionModel) {
    }
}
