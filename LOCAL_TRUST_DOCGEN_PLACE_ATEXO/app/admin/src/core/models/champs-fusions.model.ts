export class ChampsFusionsModel {
    public champsFusion?: string;
    public blocUrl?: string;
    public simple?: boolean;
    public libelle?: string;
    public infoBulle?: any;
}
