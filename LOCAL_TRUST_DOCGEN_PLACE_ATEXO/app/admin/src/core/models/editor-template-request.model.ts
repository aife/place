import generateUUID from "../utils/guid.util";
import {RequestModel} from "@/core/models/request.model";

export default class EditorTemplateRequestModel {
    public template: File;
    public editorRequest: RequestModel;

    constructor() {
        this.template = new File([], "");
        this.editorRequest = {
            user: {id: generateUUID(), name: 'Atexo', roles: ['modification']},
            mode: 'template',
            documentId: generateUUID(),
            callback: window.location.origin + "/docgen/api.php/v1/document-edition/callback",
            documentTitle: null,
            plateformeId: "mock-docgen",
            version: 100,
            documentAdministrableConfiguration: {
                champsFusions: [
                    {
                        "champsFusion": "consultation.objet",
                        "simple": true,
                        "libelle": "Objet de la consultation",
                        "infoBulle": {
                            "description": "Objet de la consultation",
                            "lien": {
                                "url": "https://www.google.com",
                                "description": "Lien vers google"
                            }
                        }
                    },
                    {
                        "champsFusion": "consultation.titre",
                        "simple": true,
                        "libelle": "Titre de la consultation",
                        "infoBulle": {
                            "description": "Titre de la consultation"
                        }
                    },
                    {
                        "champsFusion": "consultation.lots",
                        "simple": false,
                        "blocUrl": "http://docgen-docker.local-trust.com/docgen/request-to-open/-826587693?token=eyJhbGciOiJIUzUxMiJ9.eyJkZXRhaWxzIjp7ImlkIjoiLTgyNjU4NzY5MyIsImRvY3VtZW50SWQiOiIzMTgyODk2My04MzMxLTQ0M2EtOTk5Yy02YTg3NWY2NjEzMTIiLCJwbGF0ZWZvcm1lSWQiOiJtb2NrLWRvY2dlbiIsIm1vZGUiOiJ0ZW1wbGF0ZSIsInVzZXIiOnsiaWQiOiI2MzU4NTUxMyIsIm5hbWUiOiJBdGV4byIsInJvbGVzIjpbIm1vZGlmaWNhdGlvbiJdfSwiY29uZmlndXJhdGlvbiI6bnVsbCwidXNlcnMiOm51bGwsImNhbGxiYWNrIjoiaHR0cDovL2RvY2dlbi1kb2NrZXIubG9jYWwtdHJ1c3QuY29tL2RvY2dlbi9hcGkucGhwL3YxL2RvY3VtZW50LWVkaXRpb24vY2FsbGJhY2siLCJoZWFkZXJzQ2FsbGJhY2siOm51bGx9LCJpYXQiOjE2OTI4ODAyNjUsImV4cCI6MTY5Mjk2NjY2NX0._jfTwnC3IJV-Lzv3Ul3oPbRVvNEhftD3s2GAfNJspCA0S9KF24todtokp9oxrzZy6b19260KTdrSVHxf35Y8tw",
                        "libelle": "Lots de la consultation"
                    }, {
                        "champsFusion": "consultation.objet",
                        "simple": true,
                        "libelle": "Objet de la consultation",
                        "infoBulle": {
                            "description": "Objet de la consultation",
                            "lien": {
                                "url": "https://www.google.com",
                                "description": "Lien vers google"
                            }
                        }
                    },
                    {
                        "champsFusion": "candidat.contact.nom",
                        "simple": true,
                        "libelle": "Contact de l'offre : Nom",
                        "infoBulle": {
                            "description": "Nom du contact"
                        }
                    }
                ],
                donneesTests: [
                    {
                        libelle: "Donnée test 1",
                        requests: [
                            {
                                key: "consultation",
                                value: {
                                    objet: "Vraie valeur objet consultation",
                                    titre: "Vraie valeur titre consultation",
                                    lots: [
                                        {
                                            libelle: "Lot 1",
                                        },
                                        {
                                            libelle: "Lot 2",
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                ]
            }
        };
    }

}
