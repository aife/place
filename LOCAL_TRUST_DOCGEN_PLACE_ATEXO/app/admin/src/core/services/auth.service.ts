import axios from 'axios';
import UserModel from "../models/user-model";

const API_LOGIN = '/docgen/api/v1/auth/';

class AuthService {
    login(user: UserModel) {
        return axios
            .post(API_LOGIN + 'signin', {
                username: user.username,
                password: user.password
            })
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                    localStorage.setItem('token', JSON.stringify(response.data.token));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

}

export default new AuthService();
