import axios from './api';

const API_MOCK_URL_V1 = '/docgen/api.php/v1/';
const API_MOCK_URL_V2 = '/docgen/api.php/v2/';

class MpeMockService {


    convertDoc(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'convertor/convert-xml', file, {
                responseType: "arraybuffer",
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Accept": "application/octet-stream"
                }
            })
    }

    convertXlsx(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'convertor/convert-analyse', file, {
                responseType: "arraybuffer",
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Accept": "application/octet-stream"
                }
            })
    }


    extractXlsx(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'extractor/extract-xlsx', file, {
                responseType: "arraybuffer",
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Accept": "application/octet-stream"
                }
            })
    }

    getFeuilles(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'extractor/xlsx-sheet', file, {
                headers: {
                    "Content-Type": "multipart/form-data"}
            })
    }

    generateDoc(file: FormData, convertToPdf: boolean) {
        return axios
            .post(API_MOCK_URL_V1 + 'generator/generate', file, {
                responseType: "arraybuffer",
                params: {
                    convertToPdf
                },
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Accept": "application/octet-stream"
                }
            })
    }

    analyseReport(outputFileName: string, file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'extractor/analyse-report', file, {
                params: {
                    outputFileName

                }
            })
    }

    getDocVariable(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'generator/variables', file, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    }

    getEditorToken(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'editor/request', file, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    }

    getEditorTemplateToken(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'editor/request-template', file, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    }

    getEditorXmlToken(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'editor/request-xml', file, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    }


    getEditorTokenV2(file: FormData) {
        return axios
            .post(API_MOCK_URL_V2 + 'editor/request', file, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    }

    getFileStatus(token: string) {
        return axios.get(API_MOCK_URL_V1 + 'suivi/status', {
            params: {
                token
            },
            responseType: "arraybuffer"
        })
    }

    getPlateformeFilesStatus(plateformeId: string) {
        return axios
            .get(API_MOCK_URL_V1 + 'suivi/plateforme-status', {
                params: {
                    plateformeId
                }
            })
    }

    addTemplate(file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'template-referential/upload', file, {
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Accept": "application/octet-stream"
                }
            })
    }

    finalize(outputFormat: string, outputFileName: string, file: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'convertor/' + outputFormat + '/finalize', file, {
                responseType: "arraybuffer",
                params: {
                    outputFileName
                },
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Accept": "application/octet-stream"
                }
            })
    }

    getXml(document: any) {
        return axios
            .post(API_MOCK_URL_V1 + 'convertor/xml', document, {
                responseType: "arraybuffer",
            })
    }

    getFile(id: string, format: string) {
        return axios
            .get(API_MOCK_URL_V1 + 'suivi/' + id + '/download/' + format, {
                responseType: "arraybuffer"
            })
    }

    getEditorValidationToken(formData: FormData) {
        return axios
            .post(API_MOCK_URL_V1 + 'editor/request-validation', formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    }


    getAllFilesStatus() {
        return axios
            .get(API_MOCK_URL_V1 + 'suivi/all')
    }
}

export default new MpeMockService();
