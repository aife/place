import 'babel-polyfill'
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import 'primeflex/primeflex.css';

import vuetify from "./plugins/vuetify";
import './assets/css/atx-app.css'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import api from "./core/services/api";

const token = localStorage.getItem('token')
if (token) {
    api.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

api.interceptors.response.use(undefined, function (err) {
    return new Promise(function () {
        if (err.status === 401 && err.config) {
            store.dispatch('auth/logout').then(() => router.push('/login'));
        }
        throw err;
    });
});
Vue.prototype.$http = api;

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');

