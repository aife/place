import MpeMockService from '../core/services/mpe-mock.service';
import downloadFileFromResponse from "../core/utils/http-response.util";
import FileFinalizeModel from "../core/models/file-finalize-model";
import EditorTemplateRequestModel from "../core/models/editor-template-request.model";
import EditorValidationRequestModel from "../core/models/editor-validation-request.model";
import FileGeneratorModel from "../core/models/file-generator-model";
import EditorConvertAnalyseModel from "../core/models/editor-convert-analyse.model";
import AnalyseRequestModel from "../core/models/analyse-request.model";

const initialState = {
    filesStatus: []
}

export const mpeMock = {
    namespaced: true,
    state: initialState,
    actions: {

        getXml(context: any, document: any) {
            return MpeMockService.getXml(document).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getFile(context: any, param: any) {
            return MpeMockService.getFile(param.id, param.format).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        finalize(context: any, file: FileFinalizeModel) {
            const formData = new FormData();
            formData.append("document", file.document)

            return MpeMockService.finalize(file.outputFormat, file.outputFileName, formData).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        addTemplate(context: any, file: any) {
            const formData = new FormData();
            formData.append("template", file.template)
            file.blocTemplates.forEach((value: File) => {
                formData.append("blocTemplates", value)

            })

            return MpeMockService.addTemplate(formData).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        extractXlsx(context: any, file: any) {
            const formData = new FormData();
            formData.append("template", file.template)
            file.fichiers.forEach((value: File) => {
                formData.append("fichiers", value)

            })
            //formData.append("dateFichiers[0].date",  file.dateFichiers[0].date)
            formData.append("configuration", new Blob([JSON.stringify(file.configuration)], {
                type: "application/json"
            }))


            return MpeMockService.extractXlsx(formData).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        generateDoc(context: any, file: FileGeneratorModel) {

            var formData = new FormData();
            formData.append("template", file.template)

            formData.append("keyValues", new Blob([JSON.stringify(file.keyValues)], {
                type: "application/json"
            }))

            return MpeMockService.generateDoc(formData, file.convertToPdf).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getDocVariable(context: any, file: any) {

            var formData = new FormData();
            formData.append("template", file.template)
            return MpeMockService.getDocVariable(formData).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getEditorToken(context: any, file: any) {
            const formData = new FormData();
            formData.append("file", file.file)

            formData.append("editorRequest", new Blob([JSON.stringify(file.editorRequest)], {
                type: "application/json"
            }))

            let editorToken$ = file.editorRequest.plateformeId === null ? MpeMockService.getEditorToken(formData) : MpeMockService.getEditorTokenV2(formData);
            return editorToken$.then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getFeuilles(context: any, file: any) {
            const formData = new FormData();
            formData.append("document", file)
            return MpeMockService.getFeuilles(formData)
                .then(
                    response => {
                        return Promise.resolve(response.data);
                    },
                    error => {
                        return Promise.reject(error);
                    }
                );
        },
        getEditorTemplateToken(context: any, file: EditorTemplateRequestModel) {
            const formData = new FormData();
            formData.append("template", file.template)

            formData.append("editorRequest", new Blob([JSON.stringify(file.editorRequest)], {
                type: "application/json"
            }))

            return MpeMockService.getEditorTemplateToken(formData).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getEditorValidationToken(context: any, file: EditorValidationRequestModel) {
            const formData = new FormData();
            formData.append("document", file.document)

            formData.append("editorRequest", new Blob([JSON.stringify(file.editorRequest)], {
                type: "application/json"
            }))

            return MpeMockService.getEditorValidationToken(formData).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getEditorXmlToken(context: any, file: any) {
            const formData = new FormData();
            formData.append("xml", file.xml)
            formData.append("document", file.document)

            formData.append("editorRequest", new Blob([JSON.stringify(file.editorRequest)], {
                type: "application/json"
            }))

            return MpeMockService.getEditorXmlToken(formData).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        analyseReport(context: any, file: AnalyseRequestModel) {
            const formData = new FormData();
            formData.append("document", file.document)

            formData.append("configuration", new Blob([JSON.stringify(file.configuration)], {
                type: "application/json"
            }))
            return MpeMockService.analyseReport(file.outputFileName, formData).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getFileStatus(context: any, token: string) {
            return MpeMockService.getFileStatus(token).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response.headers['document-status']);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getPlateformeFilesStatus(context: any, plateformeId: string) {
            return MpeMockService.getPlateformeFilesStatus(plateformeId).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        convertDoc(context: any, file: any) {

            var formData = new FormData();
            formData.append("xml", file.xml)
            formData.append("template", file.template)
            formData.append("keyValues", new Blob([JSON.stringify(file.keyValues)], {
                type: "application/json"
            }))
            return MpeMockService.convertDoc(formData).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        convertXlsx(context: any, file: EditorConvertAnalyseModel) {

            var formData = new FormData();
            formData.append("template", file.template)
            formData.append("meta", new Blob([JSON.stringify(file.meta)], {
                type: "application/json"
            }))
            return MpeMockService.convertXlsx(formData).then(
                response => {
                    downloadFileFromResponse(response);
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        getAllFilesStatus(context: any) {
            return MpeMockService.getAllFilesStatus().then(
                response => {
                    context.commit('filesStateSuccess', response.data)
                    return Promise.resolve(response);
                },
                error => {
                    context.commit('filesStateFailure')
                    return Promise.reject(error);
                }
            );
        }
    }, mutations: {
        filesStateSuccess(state: any, list: Array<any>) {
            state.filesStatus = list;
        },
        filesStateFailure(state: any) {
            state.filesStatus = [];

        }
    }
};
