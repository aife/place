import axios from "../core/services/api";
import AuthService from '../core/services/auth.service';
import UserModel from "../core/models/user-model";

const initialState = {
    status: '',
    token: localStorage.getItem('token'),
    user: {}
}

export const auth = {
    namespaced: true,
    state: initialState,
    mutations: {
        auth_request(state: any) {
            state.status = 'loading'
        },
        auth_success(state: any, user: UserModel) {
            state.status = 'success'
            state.token = user.token
            state.user = user
        },
        auth_error(state: any) {
            state.status = 'error'
        },
        logout(state: any) {
            state.status = ''
            state.token = ''
        },
    },
    actions: {
        login(context: any, user: UserModel) {
            context.commit('auth_request')
            return AuthService.login(user)
                .then(resp => {
                    const token = resp.token
                    const user = resp
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                    context.commit('auth_success', user)
                    return Promise.resolve(resp)
                })
                .catch(err => {
                    context.commit('auth_error')
                    localStorage.removeItem('token')
                    return Promise.reject(err)
                })

        },
        logout(context: any) {
            context.commit('logout')
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            delete axios.defaults.headers.common['Authorization']
        }
    },
    getters: {
        isLoggedIn: (state: any) => !!state.token,
        authStatus: (state: any) => state.status,
    }
};
