module.exports = {
    devServer: {
        proxy: {
            "/docgen/": {
                target: "http://localhost:8081/"
            },
            "/onlyoffice/": {
                target: "http://192.168.10.105/"
            }
        },
        port: 4202
    },
    publicPath: 'admin'

}
