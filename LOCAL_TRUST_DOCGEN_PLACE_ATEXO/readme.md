# DocGen Application

Pre require before the instalation.

  - Java 11
  - Apache
  - Mysql
  - Docker

# Instalation

## Database
  - create a database with this configuration 
  - name : "docgen"
  - url : "jdbc:mariadb://localhost:3306/docgen?serverTimezone=UTC"
  - user : "root"
  - password : "admin"
  - Give all privilege of you user.

 ## Tomcat configuration
  - Download a tomcat server and open the file conf/context.xml.
  - Past on context.xml this code before the "</Context>" a the end file :
```xml
  <Resource
    driverClassName="org.mariadb.jdbc.Driver"
    type="javax.sql.DataSource"
    name="jdbc/docgen"
    username="root"
    password=""
    url="jdbc:mariadb://localhost:3306/docgen?serverTimezone=UTC"
    maxActive="20"
    maxIdle="10"
    maxWait="-1"
    >
	</Resource>
```
 - Configure you tomcat on inteleji by 
 - Configure le serveur Apache avec fichiers dans lt_doc_gen/config and copy those files to /etc/apache2/site-enabled/*
 - URL : http://{You_ip_adress}/admin
 - JRE : 11
 - and create you war file
 
 ## Docker
 - past on you terminal 
 - sudo chown your_user_name:your_user_name /var/run/docker.sock
 - sudo gedit /etc/hosts
 ajouter les lignes suivantes :
   127.0.0.1	docgen-docker.local-trust.com onlyoffice-docker.local-trust.com editeur-en-ligne-docker.local-trust.com
   127.0.0.1	atxpma-docker.local-trust.com

Pour lancer l'application, assurer que le port 80 est libre.
 - mvn clean install
 - docker-compose build
 - docker-compose up -d
 - vous pouvez naviguer via l'url http://docgen-docker.local-trust.com/admin/dashboard et vous connecter avec les identifiants suivants :
   - login : admin
   - password : password
 - pour acceder à la BDD : http://atxpma-docker.local-trust.com
   - user : root
   - password : admin
   - database : docgen
 - pour acceder à l'editeur en ligne : http://editeur-en-ligne-docker.local-trust.com
 - pour acceder à onlyoffice : http://onlyoffice-docker.local-trust.com
 - pour débugger l'application, il faut lancer le debug sur le port 8000
# Clean install the application
 - `mvn clean install`
