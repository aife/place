package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.hssf.util.HSSFColor;

import fr.atexo.commun.document.generation.service.tableaudef.CouleurTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCouleur;

public class CouleurTranslatorXls implements CouleurTranslator {

	@Override
	public Object transale(EnumerationCouleur couleur) {

		int translation = 0;

		switch (couleur) {
		case BLACK:
			translation = HSSFColor.HSSFColorPredefined.BLACK.getIndex();
			break;
		case BROWN:
			translation = HSSFColor.HSSFColorPredefined.BROWN.getIndex();
			break;
		case OLIVE_GREEN:
			translation = HSSFColor.HSSFColorPredefined.OLIVE_GREEN.getIndex();
			break;
		case DARK_GREEN:
			translation = HSSFColor.HSSFColorPredefined.DARK_GREEN.getIndex();
			break;
		case DARK_TEAL:
			translation = HSSFColor.HSSFColorPredefined.DARK_TEAL.getIndex();
			break;
		case DARK_BLUE:
			translation = HSSFColor.HSSFColorPredefined.DARK_BLUE.getIndex();
			break;
		case INDIGO:
			translation = HSSFColor.HSSFColorPredefined.INDIGO.getIndex();
			break;
		case GREY_80_PERCENT:
			translation = HSSFColor.HSSFColorPredefined.GREY_80_PERCENT.getIndex();
			break;
		case ORANGE:
			translation = HSSFColor.HSSFColorPredefined.ORANGE.getIndex();
			break;
		case DARK_YELLOW:
			translation = HSSFColor.HSSFColorPredefined.DARK_YELLOW.getIndex();
			break;
		case GREEN:
			translation = HSSFColor.HSSFColorPredefined.GREEN.getIndex();
			break;
		case TEAL:
			translation = HSSFColor.HSSFColorPredefined.TEAL.getIndex();
			break;
		case BLUE:
			translation = HSSFColor.HSSFColorPredefined.BLUE.getIndex();
			break;
		case BLUE_GREY:
			translation = HSSFColor.HSSFColorPredefined.BLUE_GREY.getIndex();
			break;
		case GREY_50_PERCENT:
			translation = HSSFColor.HSSFColorPredefined.GREY_50_PERCENT.getIndex();
			break;
		case RED:
			translation = HSSFColor.HSSFColorPredefined.RED.getIndex();
			break;
		case LIGHT_ORANGE:
			translation = HSSFColor.HSSFColorPredefined.LIGHT_ORANGE.getIndex();
			break;
		case LIME:
			translation = HSSFColor.HSSFColorPredefined.LIME.getIndex();
			break;
		case SEA_GREEN:
			translation = HSSFColor.HSSFColorPredefined.SEA_GREEN.getIndex();
			break;
		case AQUA:
			translation = HSSFColor.HSSFColorPredefined.AQUA.getIndex();
			break;
		case LIGHT_BLUE:
			translation = HSSFColor.HSSFColorPredefined.LIGHT_BLUE.getIndex();
			break;
		case VIOLET:
			translation = HSSFColor.HSSFColorPredefined.VIOLET.getIndex();
			break;
		case GREY_40_PERCENT:
			translation = HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex();
			break;
		case PINK:
			translation = HSSFColor.HSSFColorPredefined.PINK.getIndex();
			break;
		case GOLD:
			translation = HSSFColor.HSSFColorPredefined.GOLD.getIndex();
			break;
		case YELLOW:
			translation = HSSFColor.HSSFColorPredefined.YELLOW.getIndex();
			break;
		case BRIGHT_GREEN:
			translation = HSSFColor.HSSFColorPredefined.BRIGHT_GREEN.getIndex();
			break;
		case TURQUOISE:
			translation = HSSFColor.HSSFColorPredefined.TURQUOISE.getIndex();
			break;
		case DARK_RED:
			translation = HSSFColor.HSSFColorPredefined.DARK_RED.getIndex();
			break;
		case SKY_BLUE:
			translation = HSSFColor.HSSFColorPredefined.SKY_BLUE.getIndex();
			break;
		case PLUM:
			translation = HSSFColor.HSSFColorPredefined.PLUM.getIndex();
			break;
		case GREY_25_PERCENT:
			translation = HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex();
			break;
		case ROSE:
			translation = HSSFColor.HSSFColorPredefined.ROSE.getIndex();
			break;
		case LIGHT_YELLOW:
			translation = HSSFColor.HSSFColorPredefined.LIGHT_YELLOW.getIndex();
			break;
		case LIGHT_GREEN:
			translation = HSSFColor.HSSFColorPredefined.LIGHT_GREEN.getIndex();
			break;
		case LIGHT_TURQUOISE:
			translation = HSSFColor.HSSFColorPredefined.LIGHT_TURQUOISE.getIndex();
			break;
		case PALE_BLUE:
			translation = HSSFColor.HSSFColorPredefined.PALE_BLUE.getIndex();
			break;
		case LAVENDER:
			translation = HSSFColor.HSSFColorPredefined.LAVENDER.getIndex();
			break;
		case WHITE:
			translation = HSSFColor.HSSFColorPredefined.WHITE.getIndex();
			break;
		case CORNFLOWER_BLUE:
			translation = HSSFColor.HSSFColorPredefined.CORNFLOWER_BLUE.getIndex();
			break;
		case LEMON_CHIFFON:
			translation = HSSFColor.HSSFColorPredefined.LEMON_CHIFFON.getIndex();
			break;
		case MAROON:
			translation = HSSFColor.HSSFColorPredefined.MAROON.getIndex();
			break;
		case ORCHID:
			translation = HSSFColor.HSSFColorPredefined.ORCHID.getIndex();
			break;
		case CORAL:
			translation = HSSFColor.HSSFColorPredefined.CORAL.getIndex();
			break;
		case ROYAL_BLUE:
			translation = HSSFColor.HSSFColorPredefined.ROYAL_BLUE.getIndex();
			break;
		case LIGHT_CORNFLOWER_BLUE:
			translation = HSSFColor.HSSFColorPredefined.LIGHT_CORNFLOWER_BLUE.getIndex();
			break;
		case TAN:
			translation = HSSFColor.HSSFColorPredefined.TAN.getIndex();
			break;

		default:
			break;
		}

		return translation;
	}

}
