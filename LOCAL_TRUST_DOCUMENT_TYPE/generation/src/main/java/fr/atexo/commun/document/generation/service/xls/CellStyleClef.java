package fr.atexo.commun.document.generation.service.xls;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 * Commentaire
 * @author Louis Champion
 */
public class CellStyleClef {

	/**
	 * bordures
	 */
	private BorderStyle bordureBasse = null;

	private BorderStyle bordureHaute = null;

	private BorderStyle bordureGauche = null;

	private BorderStyle bordureDroite = null;

	private int bordureBasseCouleur = -1;

	private int bordureHauteCouleur = -1;

	private int bordureGaucheCouleur = -1;

	private int bordureDroiteCouleur = -1;

	/**
	 * remplissage
	 */
	private FillPatternType typeRemplissage = null;

	private int remplissagePremierPlan = -1;

	private int remplissageArrierePlan = -1;

	/**
	 * font
	 */
	private int policeCouleur = -1;

	private int policeTaille = -1;

	private int policeTailleEnPoints = -1;

	private boolean policeGras;

	private boolean policeItalique;

	private int policeSousligne = -1;

	private boolean policeBarre;

	private int policeDecalage = -1;

	private int policeEncoding = -1;

	private String policeNom = null;

	/**
	 * mise en page
	 */
	private boolean retourLigne;

	/**
	 * alignement
	 */
	private VerticalAlignment alignementVertical = null;

	private HorizontalAlignment alignementHorizontal = null;


	/**
	 * autre
	 */
	private boolean verrouillee;

	private boolean cachee;

	private String formatDonnee;

	private int formatDonneeIndex = -1;

	private int indentation;

	private int rotation;

	public boolean isVerrouillee() {
		return verrouillee;
	}

	public void setVerrouillee(boolean verrouillee) {
		this.verrouillee = verrouillee;
	}

	public boolean isCachee() {
		return cachee;
	}

	public void setCachee(boolean cachee) {
		this.cachee = cachee;
	}

	public int getFormatDonneeIndex() {
		return formatDonneeIndex;
	}

	public void setFormatDonneeIndex(int formatDonneeIndex) {
		this.formatDonneeIndex = formatDonneeIndex;
	}

	public int getIndentation() {
		return indentation;
	}

	public void setIndentation(int indentation) {
		this.indentation = indentation;
	}

	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	public BorderStyle getBordureBasse() {
		return bordureBasse;
	}

	public void setBordureBasse(BorderStyle bordureBasse) {
		this.bordureBasse = bordureBasse;
	}

	public BorderStyle getBordureHaute() {
		return bordureHaute;
	}

	public void setBordureHaute(BorderStyle bordureHaute) {
		this.bordureHaute = bordureHaute;
	}

	public BorderStyle getBordureGauche() {
		return bordureGauche;
	}

	public void setBordureGauche(BorderStyle bordureGauche) {
		this.bordureGauche = bordureGauche;
	}

	public BorderStyle getBordureDroite() {
		return bordureDroite;
	}

	public void setBordureDroite(BorderStyle bordureDroite) {
		this.bordureDroite = bordureDroite;
	}

	public int getBordureBasseCouleur() {
		return bordureBasseCouleur;
	}

	public void setBordureBasseCouleur(int bordureBasseCouleur) {
		this.bordureBasseCouleur = bordureBasseCouleur;
	}

	public int getBordureHauteCouleur() {
		return bordureHauteCouleur;
	}

	public void setBordureHauteCouleur(int bordureHauteCouleur) {
		this.bordureHauteCouleur = bordureHauteCouleur;
	}

	public int getBordureGaucheCouleur() {
		return bordureGaucheCouleur;
	}

	public void setBordureGaucheCouleur(int bordureGaucheCouleur) {
		this.bordureGaucheCouleur = bordureGaucheCouleur;
	}

	public int getBordureDroiteCouleur() {
		return bordureDroiteCouleur;
	}

	public void setBordureDroiteCouleur(int bordureDroiteCouleur) {
		this.bordureDroiteCouleur = bordureDroiteCouleur;
	}

	public FillPatternType getTypeRemplissage() {
		return typeRemplissage;
	}

	public void setTypeRemplissage(FillPatternType typeRemplissage) {
		this.typeRemplissage = typeRemplissage;
	}

	public int getRemplissagePremierPlan() {
		return remplissagePremierPlan;
	}

	public void setRemplissagePremierPlan(int remplissagePremierPlan) {
		this.remplissagePremierPlan = remplissagePremierPlan;
	}

	public int getRemplissageArrierePlan() {
		return remplissageArrierePlan;
	}

	public void setRemplissageArrierePlan(int remplissageArrierePlan) {
		this.remplissageArrierePlan = remplissageArrierePlan;
	}

	public int getPoliceCouleur() {
		return policeCouleur;
	}

	public void setPoliceCouleur(int policeCouleur) {
		this.policeCouleur = policeCouleur;
	}

	public int getPoliceTaille() {
		return policeTaille;
	}

	public void setPoliceTaille(int policeTaille) {
		this.policeTaille = policeTaille;
	}

	public boolean isPoliceGras() {
		return policeGras;
	}

	public void setPoliceGras(boolean policeGras) {
		this.policeGras = policeGras;
	}

	public boolean isPoliceItalique() {
		return policeItalique;
	}

	public void setPoliceItalique(boolean policeItalique) {
		this.policeItalique = policeItalique;
	}

	public int getPoliceSousligne() {
		return policeSousligne;
	}

	public void setPoliceSousligne(int policeSousligne) {
		this.policeSousligne = policeSousligne;
	}

	public boolean isRetourLigne() {
		return retourLigne;
	}

	public void setRetourLigne(boolean retourLigne) {
		this.retourLigne = retourLigne;
	}

	public VerticalAlignment getAlignementVertical() {
		return alignementVertical;
	}

	public void setAlignementVertical(VerticalAlignment alignementVertical) {
		this.alignementVertical = alignementVertical;
	}

	public HorizontalAlignment getAlignementHorizontal() {
		return alignementHorizontal;
	}

	public void setAlignementHorizontal(HorizontalAlignment alignementHorizontal) {
		this.alignementHorizontal = alignementHorizontal;
	}

	public int getPoliceTailleEnPoints() {
		return policeTailleEnPoints;
	}

	public void setPoliceTailleEnPoints(int policeTailleEnPoints) {
		this.policeTailleEnPoints = policeTailleEnPoints;
	}

	public boolean isPoliceBarre() {
		return policeBarre;
	}

	public void setPoliceBarre(boolean policeBarre) {
		this.policeBarre = policeBarre;
	}

	public int getPoliceDecalage() {
		return policeDecalage;
	}

	public void setPoliceDecalage(int policeDecalage) {
		this.policeDecalage = policeDecalage;
	}

	public int getPoliceEncoding() {
		return policeEncoding;
	}

	public void setPoliceEncoding(int policeEncoding) {
		this.policeEncoding = policeEncoding;
	}

	public String getPoliceNom() {
		return policeNom;
	}

	public void setPoliceNom(String policeNom) {
		this.policeNom = policeNom;
	}

	public String getFormatDonnee() {
		return formatDonnee;
	}

	public void setFormatDonnee(String formatDonnee) {
		this.formatDonnee = formatDonnee;
	}

	public boolean isBorduresDefinies() {
		boolean bordure = bordureBasse != null || bordureDroite != null || bordureGauche != null || bordureHaute != null;
		return bordure;
	}

	public boolean isRemplissageDefini() {
		boolean remplissage = typeRemplissage != null || remplissagePremierPlan != -1 || remplissageArrierePlan != -1;
		return remplissage;
	}

	public boolean isFontDefinie() {
		boolean miseEnPage = policeCouleur != -1 || policeTaille != -1 || policeTailleEnPoints != -1 || policeSousligne != -1 || policeGras || policeItalique;
		return miseEnPage;
	}

	public boolean isMiseEnPageDefinie() {
        return retourLigne;
	}

	public boolean isAlignementDefinie() {
        return alignementHorizontal != null || alignementVertical != null;
	}

	public boolean isFormatDefinie() {
		return formatDonnee != null || formatDonneeIndex != -1;
	}

	public boolean isStyleDefini() {
		return isAlignementDefinie() || isBorduresDefinies() || isFontDefinie() || isMiseEnPageDefinie() || isRemplissageDefini() || isFormatDefinie();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof CellStyleClef))
			return false;

		CellStyleClef that = (CellStyleClef) o;

		if (alignementHorizontal != that.alignementHorizontal)
			return false;
		if (alignementVertical != that.alignementVertical)
			return false;
		if (bordureBasse != that.bordureBasse)
			return false;
		if (bordureBasseCouleur != that.bordureBasseCouleur)
			return false;
		if (bordureDroite != that.bordureDroite)
			return false;
		if (bordureDroiteCouleur != that.bordureDroiteCouleur)
			return false;
		if (bordureGauche != that.bordureGauche)
			return false;
		if (bordureGaucheCouleur != that.bordureGaucheCouleur)
			return false;
		if (bordureHaute != that.bordureHaute)
			return false;
		if (bordureHauteCouleur != that.bordureHauteCouleur)
			return false;
		if (cachee != that.cachee)
			return false;
		if (formatDonneeIndex != that.formatDonneeIndex)
			return false;
		if (indentation != that.indentation)
			return false;
		if (policeBarre != that.policeBarre)
			return false;
		if (policeCouleur != that.policeCouleur)
			return false;
		if (policeDecalage != that.policeDecalage)
			return false;
		if (policeEncoding != that.policeEncoding)
			return false;
		if (policeGras != that.policeGras)
			return false;
		if (policeItalique != that.policeItalique)
			return false;
		if (policeSousligne != that.policeSousligne)
			return false;
		if (policeTaille != that.policeTaille)
			return false;
		if (policeTailleEnPoints != that.policeTailleEnPoints)
			return false;
		if (remplissageArrierePlan != that.remplissageArrierePlan)
			return false;
		if (remplissagePremierPlan != that.remplissagePremierPlan)
			return false;
		if (retourLigne != that.retourLigne)
			return false;
		if (rotation != that.rotation)
			return false;
		if (typeRemplissage != that.typeRemplissage)
			return false;
		if (verrouillee != that.verrouillee)
			return false;
		if (formatDonnee != null ? !formatDonnee.equals(that.formatDonnee) : that.formatDonnee != null)
			return false;
		if (policeNom != null ? !policeNom.equals(that.policeNom) : that.policeNom != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = bordureBasse != null ? bordureBasse.getCode() : 0;
		result = 31 * result + (bordureHaute != null ? bordureHaute.getCode() : 0);
		result = 31 * result + (bordureGauche != null ? bordureGauche.getCode() : 0);
		result = 31 * result + (bordureDroite != null ? bordureDroite.getCode() : 0);
		result = 31 * result + bordureBasseCouleur;
		result = 31 * result + bordureHauteCouleur;
		result = 31 * result + bordureGaucheCouleur;
		result = 31 * result + bordureDroiteCouleur;
		result = 31 * result + (typeRemplissage != null ? typeRemplissage.hashCode() : 0);
		result = 31 * result + remplissagePremierPlan;
		result = 31 * result + remplissageArrierePlan;
		result = 31 * result + policeCouleur;
		result = 31 * result + policeTaille;
		result = 31 * result + policeTailleEnPoints;
		result = 31 * result + (policeGras ? 1 : 0);
		result = 31 * result + (policeItalique ? 1 : 0);
		result = 31 * result + policeSousligne;
		result = 31 * result + (policeBarre ? 1 : 0);
		result = 31 * result + policeDecalage;
		result = 31 * result + policeEncoding;
		result = 31 * result + (policeNom != null ? policeNom.hashCode() : 0);
		result = 31 * result + (retourLigne ? 1 : 0);
		result = 31 * result + (alignementVertical != null ? alignementVertical.hashCode() : 0);
		result = 31 * result + (alignementHorizontal != null ? alignementHorizontal.hashCode() : 0);
		result = 31 * result + (verrouillee ? 1 : 0);
		result = 31 * result + (cachee ? 1 : 0);
		result = 31 * result + (formatDonnee != null ? formatDonnee.hashCode() : 0);
		result = 31 * result + formatDonneeIndex;
		result = 31 * result + indentation;
		result = 31 * result + rotation;
		return result;
	}
}