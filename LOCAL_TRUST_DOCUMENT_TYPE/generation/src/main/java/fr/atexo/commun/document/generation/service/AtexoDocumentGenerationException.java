/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service;

/**
 * Exception générique lancée par le module generation dès qu'un problème est
 * trouvé
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
public class AtexoDocumentGenerationException extends Exception {

	private static final long serialVersionUID = 3878511342043887268L;

	public AtexoDocumentGenerationException(String message, Throwable cause) {
		super(message, cause);
	}

	public AtexoDocumentGenerationException(String message) {
		super(message);
	}

	public AtexoDocumentGenerationException(ExceptionLogMessages e) {
		super(e.getLibelle());
	}
}
