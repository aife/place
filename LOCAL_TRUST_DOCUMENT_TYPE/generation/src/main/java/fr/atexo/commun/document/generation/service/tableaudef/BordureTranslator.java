package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationBordure;

public interface BordureTranslator {

	public Object transale(EnumerationBordure bordure);

}
