package fr.atexo.commun.document.generation.service;

import fr.atexo.commun.document.generation.service.AtexoDocumentGenerationException;
import fr.atexo.commun.document.generation.service.ExceptionLogMessages;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class AtexoDocumentConversionPdfException extends AtexoDocumentGenerationException {

    public AtexoDocumentConversionPdfException(ExceptionLogMessages e) {
        super(e);
    }

    public AtexoDocumentConversionPdfException(String message) {
        super(message);
    }
}
