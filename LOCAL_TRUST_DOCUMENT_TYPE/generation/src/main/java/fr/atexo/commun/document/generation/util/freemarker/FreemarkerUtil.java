package fr.atexo.commun.document.generation.util.freemarker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import freemarker.template.SimpleDate;

/**
 * Permet de faire des traitements dans un template FreeMarker (ex : tris sur
 * plusieurs parametres). Doit etre instancie dans le template avec
 * ObjectConstructor pour etre compatible.
 *
 * @author RVI
 */
public class FreemarkerUtil {

    /**
     * Permet de récupérer un timestamps à partir d'une date
     *
     * @param date la date
     * @return la date sous la forme d'un timestamps
     */
    public Long getTimeStamps(Date date) {

        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }

    /**
     * Permet de trier la liste paramList par rapport a certains champs des
     * objets qui la compose. Ces champs (et leur priorite de tri) sont definis
     * dans paramCmp.
     */
    public List<Object> tri(List<Object> paramList, String paramCmp[]) {

        GenericComparator comparator = new GenericComparator();
        comparator.setParametreCmp(paramCmp);

        // Creation d'une liste temporaire pour le tri (pour ne faire le sort()
        // directement sur la liste presente dans le template FreeMarker, ce qui
        // declenche une exception)
        List<Object> tmpList = new ArrayList<Object>();
        tmpList.addAll(paramList);

        Collections.sort(tmpList, comparator);

        // Reemcapsulation FreeMarker implicite au moment du return
        return tmpList;
    }

    /**
     * Permet de créer et de remplir une HashMap à partir d'un script FreeMarker
     */
    public HashMap<String, Object> hashMapPut(HashMap<String, Object> map, String key, Object value) {
        if (map == null) {
            map = new HashMap<String, Object>();
        }
        if (key != null && value != null) {
            map.put(key, value);
        }
        return map;
    }

    /**
     * Permet de créer et de remplir un ArrayList<String> à partir d'un script FreeMarker
     */
    public ArrayList<String> stringListAdd(ArrayList<String> list, String value) {
        if (list == null) {
            list = new ArrayList<String>();
        }
        list.add(value);
        return list;
    }

}
