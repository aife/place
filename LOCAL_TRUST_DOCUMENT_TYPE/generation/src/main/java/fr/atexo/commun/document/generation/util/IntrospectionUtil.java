/**
 * $Id$
 */
package fr.atexo.commun.document.generation.util;

import java.util.List;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class IntrospectionUtil {

    // déclaration du log
    private static final Log LOG = LogFactory.getLog(IntrospectionUtil.class);

    private Object object;

    public IntrospectionUtil(Object object) {
        this.object = object;
    }

    public void calculerValeurs(List<ChampFusionDTO> champs) {

        for (ChampFusionDTO cf : champs) {

            if (cf.getPath() != null) {
                try {
                    cf.setValeur(PropertyUtils.getProperty(object, cf.getPath()));

                } catch (NestedNullException e) {
                    LOG.warn("NestedNullException lors de l'introspection du champ " + cf.getNom() + " ayant comme chemin " + cf.getPath());
                } catch (Exception e) {
                    LOG.error("Erreur lors de l'introspection du champ " + cf.getNom() + " ayant comme chemin " + cf.getPath());
                    LOG.error(e.getMessage(), e);
                }
            } else {
                LOG.warn("Erreur lors de la récuperation de la valeur du ChampFusion " + cf.getNom() + ". Son path est null!");
            }
        }
    }
}
