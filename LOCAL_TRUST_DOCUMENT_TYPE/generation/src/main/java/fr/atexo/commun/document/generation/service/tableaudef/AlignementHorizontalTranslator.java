package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationAlignementHorizontal;

public interface AlignementHorizontalTranslator {

	public Object transale(EnumerationAlignementHorizontal alignementHorizontal);

}
