package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.ss.usermodel.CellType;

import fr.atexo.commun.document.generation.service.tableaudef.CelluleTypeTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCelluleType;

public class CelluleTypeTranslatorXls implements CelluleTypeTranslator {

	@Override
	public Object transale(EnumerationCelluleType celluleType) {

		CellType translation;

		switch (celluleType) {
		case CELL_TYPE_NUMERIC:
			translation = CellType.NUMERIC;
			break;
		case CELL_TYPE_STRING:
			translation = CellType.STRING;
			break;
		case CELL_TYPE_FORMULA:
			translation = CellType.FORMULA;
			break;
		case CELL_TYPE_BLANK:
			translation = CellType.BLANK;
			break;
		case CELL_TYPE_BOOLEAN:
			translation = CellType.BOOLEAN;
			break;
		case CELL_TYPE_ERROR:
			translation = CellType.ERROR;
			break;
		default:
			translation = CellType.STRING;
			break;
		}

		return translation;
	}

}
