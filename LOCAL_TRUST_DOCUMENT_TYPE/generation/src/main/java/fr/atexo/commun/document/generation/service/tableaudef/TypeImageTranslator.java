package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationTypeImage;

public interface TypeImageTranslator {

	public Object transale(EnumerationTypeImage typeImage);

}
