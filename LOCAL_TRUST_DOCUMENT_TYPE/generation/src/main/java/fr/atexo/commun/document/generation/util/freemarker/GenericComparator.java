package fr.atexo.commun.document.generation.util.freemarker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Permet d'effectuer differents tris par l'intermediaire de la methode
 * Collections.sort(List<?>, Comparator). La methode setParametreCmp("param0",
 * "param1", ...) permet de definir la priorite de tri des champs des objets
 * tries, tel que prio(param0) > prio(param1) > ... Si un des parametres est de
 * la forme "cond?param", alors le tri sur param a lieu uniquement si le champ
 * cond des deux objets compares est a true. Si un des parametres est de la
 * forme "cond?param1:param2", alors la comparaison a lieu entre param1/param2
 * de l'objet courant et de celui a comparer selon la valeur de leur champ cond
 * respectifs.
 * @author RVI
 */
public class GenericComparator implements Comparator<Object> {
    /**
     * Loggueur.
     */
    private final Log LOG = LogFactory.getLog(getClass());
    /**
     * Parametres de comparaison.
     */
    private String[] parametreCmp;
    /**
     * Indique si les parametres booleens doivent etre tries de maniere
     * ascendante ou descendante.
     */
    private boolean triBooleanAsc = false;

    public int compare(Object arg0, Object arg1) {

        Object objTmp0, objTmp1;
        Comparable cmpObj0, cmpObj1;
        String[] conditionTab = null;

        objTmp0 = objTmp1 = cmpObj0 = cmpObj1 = null;

        if ((arg0 == null) || (arg1 == null)) {
            ClassCastException e = new ClassCastException(
                    "Un des éléments de la liste a trier est null.");
            LOG.error(e.getMessage());
            throw e;
        }

        if (arg0.getClass() != arg1.getClass()) {
            ClassCastException e = new ClassCastException(
                    "La liste a trier contient des objets de types differents.");
            LOG.error(e.getMessage());
            throw e;
        }

        for (String itemCmp : parametreCmp) {
            String conditionCmp = null;
            String paramCmp0 = null;
            String paramCmp1 = null;
            String paramCmpElse = null;
            if (itemCmp.matches(".+\\?.+")) {
                conditionTab = itemCmp.split("[?:]");
                conditionCmp = conditionTab[0];
            } else {
                paramCmp0 = paramCmp1 = itemCmp;
            }

            // Comparaison conditionnelle = si les deux objets on conditionCmp a
            // true
            if (conditionCmp != null) {
                objTmp0 = getParam(arg0, conditionCmp);
                objTmp1 = getParam(arg1, conditionCmp);
                if (!(objTmp0 instanceof Boolean)) {
                    throw new ClassCastException(
                            "Le parametre de comparaison conditionnelle n'est pas de type boolean.");
                }
                Boolean conditionCmpVal0 = (Boolean) objTmp0;
                Boolean conditionCmpVal1 = (Boolean) objTmp1;
                if (conditionCmpVal0) {
                    paramCmp0 = conditionTab[1];
                } else if (conditionTab.length > 2) {
                    paramCmp0 = conditionTab[2];
                } else {
                    continue;
                }
                if (conditionCmpVal1) {
                    paramCmp1 = conditionTab[1];
                } else {
                    paramCmp1 = conditionTab[2];
                }
            }

            // Comparaison des champs
            objTmp0 = getParam(arg0, paramCmp0);
            objTmp1 = getParam(arg1, paramCmp1);

            //Gestion des valeurs NULL
            if (objTmp0 == null && objTmp1 == null){
                return 0;
            }
            else if (objTmp0 == null){
                return -1;
            }
            else if (objTmp1 == null){
                return 1;
            }

            if (objTmp0 instanceof Comparable) {
                cmpObj0 = (Comparable) objTmp0;
                cmpObj1 = (Comparable) objTmp1;

                int cmpRes = cmpObj0.compareTo(cmpObj1);
                if ((objTmp0 instanceof Boolean) && !triBooleanAsc) {
                    cmpRes = -cmpRes;
                }

                if (cmpRes != 0) {
                    return cmpRes;
                }
            }
        }

        return 0;
    }

    /**
     * Retourne la champ param de l'objet obj.
     */
    private Object getParam(Object obj, String param) {

        Object res = null;
        Method methode = null;
        boolean useSpecialGetter = false;

        String basicMethodName = "get" + param.substring(0, 1).toUpperCase(Locale.FRENCH)
                + param.substring(1, param.length());
        String bolleanMethodName = "is" + param.substring(0, 1).toUpperCase(Locale.FRENCH)
                + param.substring(1, param.length());

        try {
            methode = obj.getClass().getMethod(basicMethodName, null);
        } catch (SecurityException e1) {
            LOG.error("Probleme de securite lors de l'acces à la méthode " + basicMethodName, e1);
            return null;
        } catch (NoSuchMethodException e1) {
            try {
                methode = obj.getClass().getMethod(bolleanMethodName, null);
            } catch (SecurityException e2) {
                LOG.error("Probleme de securite lors de l'acces à la méthode " + bolleanMethodName,
                        e2);
                return null;
            } catch (NoSuchMethodException e2) {
                try {
                    methode = obj.getClass().getMethod("get", String.class);
                    useSpecialGetter = true;
                }
                catch (NoSuchMethodException e3) {
                    LOG.error("Méthode d'acces au paramètre " + param + " inexistante.", e2);
                    return null;
                }
            }
        }

        try {
            if (useSpecialGetter){
                 res = methode.invoke(obj, param);
            }
            else {
                res = methode.invoke(obj, null);
            }
        } catch (IllegalArgumentException e) {
            LOG.error("Argument invalide lors de l'appel à la méthode " + methode.getName(), e);
            return null;
        } catch (IllegalAccessException e) {
            LOG.error("Problème d'accès lors de l'appel à la méthode " + methode.getName(), e);
            return null;
        } catch (InvocationTargetException e) {
            LOG.error("Exception déclenchée lors de l'appel à la méthode " + methode.getName(), e);
            return null;
        }

        return res;
    }

    /**
     * @param valeur Parametres de comparaison.
     */
    public final void setParametreCmp(final String[] valeur) {
        this.parametreCmp = valeur;
    }

    /**
     * @param valeur Indique si les parametres booleens doivent etre tries de
     *            maniere ascendante ou descendante (par defaut false).
     */
    public final void setTriBooleanAsc(final boolean valeur) {
        this.triBooleanAsc = valeur;
    }
}
