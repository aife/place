package fr.atexo.commun.document.generation.service.openoffice.translator;

import com.sun.star.text.VertOrientation;

import fr.atexo.commun.document.generation.service.tableaudef.AlignementVerticalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationAlignementVertical;

public class AlignementVerticalTranslatorOdt implements AlignementVerticalTranslator {

	@Override
	public Short transale(EnumerationAlignementVertical alignementVertical) {

		short translation = VertOrientation.NONE;

		switch (alignementVertical) {
			case VERTICAL_TOP:
				translation = VertOrientation.TOP;
				break;

			case VERTICAL_CENTER:
				translation = VertOrientation.CENTER;
				break;

			case VERTICAL_BOTTOM:
				translation = VertOrientation.BOTTOM;
				break;

			case VERTICAL_JUSTIFY:
				translation = VertOrientation.NONE;
				break;

			default:
				break;
		}

		return translation;
	}

}
