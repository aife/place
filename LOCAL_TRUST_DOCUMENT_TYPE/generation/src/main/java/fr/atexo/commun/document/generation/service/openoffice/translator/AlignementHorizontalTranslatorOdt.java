package fr.atexo.commun.document.generation.service.openoffice.translator;

import com.sun.star.style.ParagraphAdjust;

import fr.atexo.commun.document.generation.service.tableaudef.AlignementHorizontalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationAlignementHorizontal;

public class AlignementHorizontalTranslatorOdt implements AlignementHorizontalTranslator {

	@Override
	public Object transale(EnumerationAlignementHorizontal alignementHorizontal) {

		ParagraphAdjust translation = ParagraphAdjust.getDefault();

		switch (alignementHorizontal) {
			case ALIGN_GENERAL:
				translation = ParagraphAdjust.getDefault();
				break;

			case ALIGN_LEFT:
				translation = ParagraphAdjust.LEFT;
				break;

			case ALIGN_CENTER:
				translation = ParagraphAdjust.CENTER;
				break;

			case ALIGN_RIGHT:
				translation = ParagraphAdjust.RIGHT;
				break;

			case ALIGN_FILL:
				translation = ParagraphAdjust.getDefault();
				break;

			case ALIGN_JUSTIFY:
				translation = ParagraphAdjust.getDefault();
				break;

			case ALIGN_CENTER_SELECTION:
				translation = ParagraphAdjust.getDefault();
				break;

			default:
				break;
		}

		return translation;
	}

}
