/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.freemarker;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.generation.service.TransformationService;
import fr.atexo.commun.document.generation.service.bean.ResultatCourrierFreeMarkerBean;
import fr.atexo.commun.freemarker.FreeMarkerUtil;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class FreeMarkerService {

	// déclaration du log
	private static final Log LOG = LogFactory.getLog(FreeMarkerService.class);

	/**
	 * Constructeur à utiliser lors de la génération d'un document
	 *
	 * @param documentDTO
	 *            DocumentDTO avec ses champsDTO et le contenu du Mail.
	 * @param freeMarkerMap
	 *            Map des valeurs à utiliser par défaut
	 * @throws Exception
	 *             lancée par {@link FreeMarkerUtil}
	 */
	public static ResultatCourrierFreeMarkerBean generateMail(DocumentDTO documentDTO, Map<String, Object> freeMarkerMap) throws Exception {
		LOG.info("Tentative de convertion du contenu d'un mail");
		ResultatCourrierFreeMarkerBean resultat = new ResultatCourrierFreeMarkerBean();
		TransformationService.ajouterChampFusionAFreeMarkerMap(documentDTO.getChampFusions(), freeMarkerMap);

		resultat.setMessage(FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, documentDTO.getDocumentModeleDTO().getContenuCourriel(), true));
		resultat.setObjet(FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, documentDTO.getDocumentModeleDTO().getObjetCourriel(), true));

		return resultat;
	}

	/**
	 * Constructeur à utiliser lors de la génération d'un document
	 *
	 * @param documentDTO
	 *            Le document à générer
	 * @throws fr.atexo.commun.document.generation.service.AtexoDocumentGenerationException
	 *             si le template n'existe pas
	 */
	public static ResultatCourrierFreeMarkerBean generateMail(DocumentDTO documentDTO) throws Exception {
		LOG.info("Tentative de convertion du contenu d'un mail");

		Map<String, Object> freeMarkerMap = new HashMap<String, Object>();

		return generateMail(documentDTO, freeMarkerMap);
	}

}
