package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationFontSousligne;

public interface FontSousligneTranslator {

	public Object transale(EnumerationFontSousligne fontSousligne);
}
