package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCouleur;

public interface CouleurTranslator {

	public Object transale(EnumerationCouleur couleur);

}
