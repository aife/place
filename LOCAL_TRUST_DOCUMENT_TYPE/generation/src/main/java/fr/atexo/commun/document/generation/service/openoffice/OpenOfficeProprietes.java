package fr.atexo.commun.document.generation.service.openoffice;

/**
 * Commentaire
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public enum OpenOfficeProprietes {
	FilterName,
	AsTemplate,
	DocumentTitle,
	Hidden,
	InstanceName,
	Name,
	Content,
	Value,
	IsVisible,
	AnchorType,
	GraphicURL,
	Width,
	Height,
	IsAutoHeight,
	CharFontName,
	CharWeight,
	CharPosture,
	CharHeight,
	CharBackColor,
	CharColor,
	CharUnderline,
	CharRotation,
	VertOrient,
	ParaAdjust,
	BackColor,
	BackTransparent,
	ParaStyleName
}
