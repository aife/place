/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.schlichtherle.io.File;
import de.schlichtherle.io.FileOutputStream;
import de.schlichtherle.util.zip.ZipEntry;
import de.schlichtherle.util.zip.ZipFile;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.generation.service.TransformationService;
import fr.atexo.commun.freemarker.FreeMarkerUtil;
import freemarker.cache.NullCacheStorage;

/**
 * Classe en charge de la transformation de documents Open Office contenant du Freemarker.
 * 
 * @author RVI
 * @version $Revision$ $Date$
 */
public class TemplateGenerateur {

  // déclaration du log
  private static final Log LOG = LogFactory.getLog(TemplateGenerateur.class);

  private static final String FICHIER_CONTENT_XML = "content.xml";

  private static File getFichierContent(java.io.File fichierODT, String dossierUnzipPath) throws IOException {

    LOG.info("Extraction du zip/odt dans le repertoire :" + dossierUnzipPath);

    // Extraction du zip/odt
    BufferedOutputStream out;
    InputStream input = null;
    File fichierContent = null;

    ZipFile fichierZip = new ZipFile(fichierODT);

    ZipEntry itemZip;
    for (Enumeration e = fichierZip.entries(); e.hasMoreElements();) {
      itemZip = (ZipEntry) e.nextElement();
      File itemFile = new File(dossierUnzipPath + File.separator + itemZip.getName());
      if (!itemFile.getParentFile().exists()) {
        itemFile.getParentFile().mkdirs();
      }
      if (itemZip.isDirectory()) {
        itemFile.mkdirs();
      } else {
        if (FICHIER_CONTENT_XML.equals(itemFile.getName())) {
          fichierContent = itemFile;
        }
        out = new BufferedOutputStream(new FileOutputStream(itemFile));
        input = fichierZip.getInputStream(itemZip);
        IOUtils.copyLarge(input, out);
        IOUtils.closeQuietly(out);
        IOUtils.closeQuietly(input);
      }
    }
    IOUtils.closeQuietly(input);
    fichierZip.close();

    return fichierContent;
  }

  /**
   * Méthode permettant générer un nouveau template à partir des information contenues dans
   * l'attribut documentDTO {@link DocumentDTO}
   * 
   * @throws Exception
   */
  public static String generateTemplateFromTemplate(List<ChampFusionDTO> champFusions, FichierDTO template, Map<String, Object> freeMarkerMap) throws java.lang.Exception {

    String cheminRepertoireDezippe = template.getFichier().getAbsolutePath() + "Unzip";
    File fichierContentCopie = getFichierContent(template.getFichier(), cheminRepertoireDezippe);

    // Transformation du fichier content.xml ( pour assurer la retro compatiblité )
    File contentRes = new File(TransformationService.executeTransformationFreemarker(fichierContentCopie.getParent() + File.separator, FICHIER_CONTENT_XML, Locale.FRENCH, freeMarkerMap));

    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(contentRes));
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fichierContentCopie));
    IOUtils.copyLarge(bis, bos);
    IOUtils.closeQuietly(bis);
    IOUtils.closeQuietly(bos);

    if (champFusions != null) {
      for (ChampFusionDTO champFusionXML : champFusions) {
        String resultatFreemarker = FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, champFusionXML.getScript());
        insererFreeMarkerATemplate(fichierContentCopie, champFusionXML, resultatFreemarker);
        FreeMarkerUtil.getConfiguration().setCacheStorage(new NullCacheStorage());
      }
    }

    // Recompression du resultat
    File dossierZipRes = new File(cheminRepertoireDezippe + ".res.zip");
    File dossierUnzip = new File(cheminRepertoireDezippe);

    dossierUnzip.copyAllTo(dossierZipRes);
    File.umount(dossierZipRes);
    FileUtils.deleteDirectory(new File(cheminRepertoireDezippe));

    return cheminRepertoireDezippe + ".res.zip";
  }

  /**
   * Méthode recursive pour trouver un pattern dans un fichier XML
   * 
   * @param nodeList la liste de nodes du fichier XML {@link NodeList}
   * @param pattern Le pattern à rechercher
   * @return Le node contenant le pattern comme valeur
   */
  private static Node rechercherNodeList(NodeList nodeList, String pattern) {
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node element = nodeList.item(i);
      if ((element.getChildNodes() == null || element.getChildNodes().getLength() == 0) && pattern.equals(element.getNodeValue())) {
        // Alors il s'agit d'un element sans enfant, et sa valeur est le pattern
        return element;
      } else if ((element = rechercherNodeList(element.getChildNodes(), pattern)) != null) {
        // Alors il s'agit d'une liste de nodes et une des valeur de ses nodes est le pattern
        return element;
      }
    }
    return null;
  }


  private static File insererFreeMarkerATemplate(File fichierContent, ChampFusionDTO champFusionXML, String xmlFreemarkerResultat) throws Exception {

    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    Document documentXML = documentBuilder.parse(new FileInputStream(fichierContent));


    NodeList nodeList = documentXML.getDocumentElement().getChildNodes();

    Node nodeElement = rechercherNodeList(nodeList, champFusionXML.getNom());
    if (nodeElement != null) {

      // Alors on a trouvé le pattern
      Node parentNodeLevel3 = nodeElement.getParentNode().getParentNode().getParentNode();
      Node parentNodeLevel2 = nodeElement.getParentNode().getParentNode();
      Node frereParentNodeLevel2 = parentNodeLevel2.getNextSibling();
      // String scriptXmlOoo = StringEscapeUtils.escapeXml(champFusionXML.getScript());
      Node contenu = documentXML.createTextNode(xmlFreemarkerResultat);
      parentNodeLevel3.insertBefore(contenu, frereParentNodeLevel2);
      parentNodeLevel3.removeChild(parentNodeLevel2);

      DOMSource source = new DOMSource(documentXML);
      StringWriter xmlWriter = new StringWriter();
      StreamResult result = new StreamResult(xmlWriter);
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      transformer.transform(source, result);

      // Malheuresement, la méthode StringEscapeUtils.escapeXml(...) semble ne pas éviter le
      // remplacement
      // des caractères (<,>,&,",') par ces équivalences "XML entities" (gt, lt, quot, amp, apos)
      // Du coup, il est nécessaire de faire les replace ci-dessous manuellement :
      String xml = xmlWriter.toString();
      xml = xml.replaceAll("&lt;", "<");
      xml = xml.replaceAll("&gt;", ">");
      xml = xml.replaceAll("&apos;", "\"");
      xml = xml.replaceAll("&quot;", "'");
      ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
      FileOutputStream fos = new FileOutputStream(fichierContent);
      IOUtils.copy(inputStream, fos);
      IOUtils.closeQuietly(inputStream);
      IOUtils.closeQuietly(fos);
    }

    return fichierContent;
  }
}
