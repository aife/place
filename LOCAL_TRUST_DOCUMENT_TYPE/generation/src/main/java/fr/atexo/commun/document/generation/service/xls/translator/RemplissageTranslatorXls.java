package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.ss.usermodel.FillPatternType;

import fr.atexo.commun.document.generation.service.tableaudef.RemplissageTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationRemplissage;

public class RemplissageTranslatorXls implements RemplissageTranslator {

	@Override
	public FillPatternType transale(EnumerationRemplissage remplissage) {

		FillPatternType translation;

		switch (remplissage) {
			case NO_FILL:
				translation = FillPatternType.NO_FILL;
				break;

			case SOLID_FOREGROUND:
				translation = FillPatternType.SOLID_FOREGROUND;
				break;

			case FINE_DOTS:
				translation = FillPatternType.FINE_DOTS;
				break;

			case ALT_BARS:
				translation = FillPatternType.ALT_BARS;
				break;

			case SPARSE_DOTS:
				translation = FillPatternType.SPARSE_DOTS;
				break;

			case THICK_HORZ_BANDS:
				translation = FillPatternType.THICK_HORZ_BANDS;
				break;

			case THICK_VERT_BANDS:
				translation = FillPatternType.THICK_VERT_BANDS;
				break;

			case THICK_BACKWARD_DIAG:
				translation = FillPatternType.THICK_BACKWARD_DIAG;
				break;

			case THICK_FORWARD_DIAG:
				translation = FillPatternType.THICK_FORWARD_DIAG;
				break;

			case BIG_SPOTS:
				translation = FillPatternType.BIG_SPOTS;
				break;

			case BRICKS:
				translation = FillPatternType.BRICKS;
				break;

			case THIN_HORZ_BANDS:
				translation = FillPatternType.THIN_HORZ_BANDS;
				break;

			case THIN_VERT_BANDS:
				translation = FillPatternType.THIN_VERT_BANDS;
				break;

			case THIN_BACKWARD_DIAG:
				translation = FillPatternType.THIN_BACKWARD_DIAG;
				break;

			case THIN_FORWARD_DIAG:
				translation = FillPatternType.THIN_FORWARD_DIAG;
				break;

			case SQUARES:
				translation = FillPatternType.SQUARES;
				break;

			case DIAMONDS:
				translation = FillPatternType.DIAMONDS;
				break;

			default:
				translation = FillPatternType.NO_FILL;
				break;
		}

		return translation;

	}

}
