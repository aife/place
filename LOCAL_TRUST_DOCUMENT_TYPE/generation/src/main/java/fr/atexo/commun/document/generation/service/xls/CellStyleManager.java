package fr.atexo.commun.document.generation.service.xls;

import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import fr.atexo.commun.document.generation.service.tableaudef.AlignementHorizontalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.AlignementVerticalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.BordureTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.CouleurTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.FontSousligneTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.RemplissageTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Cellule;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleHelper;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyle;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleAlignement;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleBordure;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleFont;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleRemplissage;
import fr.atexo.commun.document.generation.service.xls.translator.AlignementHorizontalTranslatorXls;
import fr.atexo.commun.document.generation.service.xls.translator.AlignementVerticalTranslatorXls;
import fr.atexo.commun.document.generation.service.xls.translator.BordureTranslatorXls;
import fr.atexo.commun.document.generation.service.xls.translator.CouleurTranslatorXls;
import fr.atexo.commun.document.generation.service.xls.translator.FontSousligneTranslatorXls;
import fr.atexo.commun.document.generation.service.xls.translator.RemplissageTranslatorXls;

/**
 * Commentaire
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class CellStyleManager {

	public static CellStyleManager cellStyleManager;

	private CellStyleManager() {
	}

	public static CellStyleManager getInstance() {

		if (cellStyleManager == null) {
			cellStyleManager = new CellStyleManager();
		}

		return cellStyleManager;
	}

	public void initialiserCellStyleClef(CellStyleClef cellStyleClef, Cellule cellule) {

		if (cellStyleClef == null) {
			cellStyleClef = new CellStyleClef();
		}

		if (CelluleHelper.isAppliquerAncienStyle(cellule)) {

			if (cellule.getBordureBas() != null) {
				cellStyleClef.setBordureBasse(BorderStyle.THIN);
			}
			if (cellule.getBordureGauche() != null) {
				cellStyleClef.setBordureGauche(BorderStyle.THIN);
			}
			if (cellule.getBordureDroite() != null) {
				cellStyleClef.setBordureDroite(BorderStyle.THIN);
			}
			if (cellule.getBordureHaut() != null) {
				cellStyleClef.setBordureHaute(BorderStyle.THIN);
			}

			if (cellule.getCouleurFond() != null) {
				cellStyleClef.setTypeRemplissage(FillPatternType.SOLID_FOREGROUND);
				cellStyleClef.setRemplissagePremierPlan((short) cellule.getCouleurFond().intValue());
			}
			if (cellule.getCouleurPolice() != null) {
				cellStyleClef.setPoliceCouleur((short) cellule.getCouleurPolice().intValue());
			}
			if (cellule.getTaillePolice() != null) {
				cellStyleClef.setPoliceTaille((short) cellule.getTaillePolice().intValue());
			}
			if (cellule.getGras() != null) {
				cellStyleClef.setPoliceGras(true);
			}
			if (cellule.getRetourLigne() != null) {
				cellStyleClef.setRetourLigne(true);
			}

		} else if (cellule.getStyle() != null) {
			CelluleStyle celluleStyle = cellule.getStyle();
			initialiserCellStyleClef(cellStyleClef, celluleStyle);
		}

		if (cellule.getFormat() != null) {
			cellStyleClef.setFormatDonnee(cellule.getFormat());
		}
	}

	public void initialiserCellStyleClef(CellStyleClef cellStyleClef, CelluleStyle celluleStyle) {

		if (celluleStyle != null) {

			CouleurTranslator ct = new CouleurTranslatorXls();

			CelluleStyleAlignement celluleStyleAlignement = celluleStyle.getAlignement();
			CelluleStyleBordure celluleStyleBordure = celluleStyle.getBordure();
			CelluleStyleFont celluleStyleFont = celluleStyle.getFont();
			CelluleStyleRemplissage celluleStyleRemplissage = celluleStyle.getRemplissage();

			// retour à la ligne du texte de la cellule
			cellStyleClef.setRetourLigne(celluleStyle.isRetourLigne());

			// alignement du texte de la cellule
			if (celluleStyleAlignement != null) {

				if (celluleStyleAlignement.getRotation() != null) {
					cellStyleClef.setRotation(celluleStyleAlignement.getRotation().shortValue());
				}

				if (celluleStyleAlignement.getHorizontal() != null) {
					AlignementHorizontalTranslator aht = new AlignementHorizontalTranslatorXls();
					cellStyleClef.setAlignementHorizontal((HorizontalAlignment) aht.transale(celluleStyleAlignement.getHorizontal()));
				}

				if (celluleStyleAlignement.getVertical() != null) {
					AlignementVerticalTranslator avt = new AlignementVerticalTranslatorXls();
					cellStyleClef.setAlignementVertical((VerticalAlignment) avt.transale(celluleStyleAlignement.getVertical()));
				}
			}

			// bordure de la cellule
			if (celluleStyleBordure != null) {
				BordureTranslator bt = new BordureTranslatorXls();
				// bordure haute
				if (celluleStyleBordure.getHaut() != null)
					cellStyleClef.setBordureHaute((BorderStyle) bt.transale(celluleStyleBordure.getHaut()));
				// bordure basse
				if (celluleStyleBordure.getBas() != null)
					cellStyleClef.setBordureBasse((BorderStyle) bt.transale(celluleStyleBordure.getBas()));
				// bordure droite
				if (celluleStyleBordure.getDroite() != null)
					cellStyleClef.setBordureDroite((BorderStyle) bt.transale(celluleStyleBordure.getDroite()));
				// bordure gauche
				if (celluleStyleBordure.getGauche() != null)
					cellStyleClef.setBordureGauche((BorderStyle) bt.transale(celluleStyleBordure.getGauche()));
			}

			// mise en page du texte (font)
			if (celluleStyleFont != null) {

				// gras
				cellStyleClef.setPoliceGras(celluleStyleFont.isGras());

				// italic
				cellStyleClef.setPoliceItalique(celluleStyleFont.isItalic());

				// sousligné
				if (celluleStyleFont.getSouligne() != null) {
					FontSousligneTranslator fst = new FontSousligneTranslatorXls();
					cellStyleClef.setPoliceSousligne((Integer) fst.transale(celluleStyleFont.getSouligne()));
				}

				// couleur
				if (celluleStyleFont.getCouleur() != null) {
					cellStyleClef.setPoliceCouleur((Integer) ct.transale(celluleStyleFont.getCouleur()));
				}

				// taille de la police
				if (celluleStyleFont.getTaille() != null) {
					cellStyleClef.setPoliceTailleEnPoints(celluleStyleFont.getTaille().shortValue());
				}
			}

			// remplissage de la cellule
			if (celluleStyleRemplissage != null) {

				// type de remplissage
				if (celluleStyleRemplissage.getType() != null) {
					RemplissageTranslator rt = new RemplissageTranslatorXls();
					cellStyleClef.setTypeRemplissage((FillPatternType) rt.transale(celluleStyleRemplissage.getType()));
				}

				// remplissage de premier plan
				if (celluleStyleRemplissage.getPremierPlan() != null) {
					cellStyleClef.setRemplissagePremierPlan((Integer) ct.transale(celluleStyleRemplissage.getPremierPlan()));
				}

				// remplissage d'arrière plan
				if (celluleStyleRemplissage.getArrierePlan() != null) {
					cellStyleClef.setRemplissageArrierePlan((Integer) ct.transale(celluleStyleRemplissage.getArrierePlan()));
				}

			}
		}
	}

	/**
	 * Crée un HSSFCellStyle à partir d'une Cellule
	 * @param cellule  la cellule contenant le style de la mise en page
	 * @param workbook le workbook nécessaire à la création d'un nouveau style
	 */
	public CellStyle creerOuRecupererStyle(Map<CellStyleClef, CellStyle> styles, Cellule cellule, HSSFWorkbook workbook, HSSFRow row) {

		HSSFCellStyle style = null;
		HSSFCellStyle styleActuel = row.getCell(CelluleHelper.getCelluleCoordonnee(cellule).getColonne()).getCellStyle();

		CellStyleClef cellStyleClef = new CellStyleClef();
		initialiserCellStyleClef(cellStyleClef, cellule);

		if (cellStyleClef.isStyleDefini()) {
			extraireStyleProprietes(cellStyleClef, styleActuel, workbook);
			initialiserCellStyleClef(cellStyleClef, cellule);

			CellStyle styleExistant = styles.get(cellStyleClef);

			if (styleExistant != null) {
				return styleExistant;
			} else {

				style = workbook.createCellStyle();

				duppliquerStyle(styleActuel, style);

				styles.put(cellStyleClef, style);

				HSSFFont font = workbook.createFont();

				if (cellStyleClef.isFontDefinie()) {
					duppliquerFont(styleActuel.getFont(workbook), font);
					style.setFont(font);
				}

				if (CelluleHelper.isAppliquerAncienStyle(cellule)) {

					if (cellule.getBordureBas() != null)
						style.setBorderBottom(BorderStyle.THIN);
					if (cellule.getBordureGauche() != null)
						style.setBorderLeft(BorderStyle.THIN);
					if (cellule.getBordureDroite() != null)
						style.setBorderRight(BorderStyle.THIN);
					if (cellule.getBordureHaut() != null)
						style.setBorderTop(BorderStyle.THIN);

					if (cellule.getCouleurFond() != null) {
						style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
						style.setFillForegroundColor((short) cellule.getCouleurFond().intValue());
					}
					if (cellule.getCouleurPolice() != null)
						font.setColor((short) cellule.getCouleurPolice().intValue());
					if (cellule.getTaillePolice() != null)
						font.setFontHeight((short) cellule.getTaillePolice().intValue());
					if (cellule.getGras() != null)
						font.setBold(true);
					if (cellule.getRetourLigne() != null)
						style.setWrapText(true);

				} else if (cellule.getStyle() != null || cellule.getFormat() != null) {

					creerStyle(cellule.getStyle(), style, font);

					// application du format
					if (cellule.getFormat() != null) {
						DataFormat format = workbook.createDataFormat();
						short referenceFormat = format.getFormat(cellule.getFormat());
						style.setDataFormat(referenceFormat);
					}
				}
			}
		}

		return style;
	}

	public CellStyle creerOuRecupererStyle(Map<CellStyleClef, CellStyle> styles, CelluleStyle celluleStyle, HSSFWorkbook workbook, HSSFRow row, int numeroColonne) {

		HSSFCellStyle style = null;
		HSSFCellStyle styleActuel = row.getCell(numeroColonne).getCellStyle();

		if (celluleStyle != null) {
			CellStyleClef cellStyleClef = new CellStyleClef();
			initialiserCellStyleClef(cellStyleClef, celluleStyle);
			if (cellStyleClef.isStyleDefini()) {
				extraireStyleProprietes(cellStyleClef, styleActuel, workbook);
				initialiserCellStyleClef(cellStyleClef, celluleStyle);

				CellStyle styleExistant = styles.get(cellStyleClef);

				if (styleExistant != null) {
					return styleExistant;
				} else {
					style = workbook.createCellStyle();
					duppliquerStyle(styleActuel, style);
					styles.put(cellStyleClef, style);
					HSSFFont font = workbook.createFont();
					if (cellStyleClef.isFontDefinie()) {
						duppliquerFont(styleActuel.getFont(workbook), font);
						style.setFont(font);
					}
					creerStyle(celluleStyle, style, font);
				}
			}
		}
		return style;

	}

	public CellStyle creerStyle(CelluleStyle celluleStyle, HSSFCellStyle style, HSSFFont font) {

		if (celluleStyle != null && style != null) {

			CouleurTranslator ct = new CouleurTranslatorXls();

			CelluleStyleAlignement celluleStyleAlignement = celluleStyle.getAlignement();
			CelluleStyleBordure celluleStyleBordure = celluleStyle.getBordure();
			CelluleStyleFont celluleStyleFont = celluleStyle.getFont();
			CelluleStyleRemplissage celluleStyleRemplissage = celluleStyle.getRemplissage();

			// retour à la ligne du texte de la cellule
			style.setWrapText(celluleStyle.isRetourLigne());

			// alignement du texte de la cellule
			if (celluleStyleAlignement != null) {

				if (celluleStyleAlignement.getRotation() != null) {
					style.setRotation(celluleStyleAlignement.getRotation().shortValue());
				}

				if (celluleStyleAlignement.getHorizontal() != null) {
					AlignementHorizontalTranslator aht = new AlignementHorizontalTranslatorXls();
					style.setAlignment((HorizontalAlignment) aht.transale(celluleStyleAlignement.getHorizontal()));
				}

				if (celluleStyleAlignement.getVertical() != null) {
					AlignementVerticalTranslator avt = new AlignementVerticalTranslatorXls();
					style.setVerticalAlignment((VerticalAlignment) avt.transale(celluleStyleAlignement.getVertical()));
				}
			}

			// bordure de la cellule
			if (celluleStyleBordure != null) {

				BordureTranslator bt = new BordureTranslatorXls();

				// bordure haute
				if (celluleStyleBordure.getHaut() != null) {
					style.setBorderTop((BorderStyle) bt.transale(celluleStyleBordure.getHaut()));
				}

				// bordure basse
				if (celluleStyleBordure.getBas() != null) {
					style.setBorderBottom((BorderStyle) bt.transale(celluleStyleBordure.getBas()));
				}

				// bordure droite
				if (celluleStyleBordure.getDroite() != null) {
					style.setBorderRight((BorderStyle) bt.transale(celluleStyleBordure.getDroite()));
				}

				// bordure gauche
				if (celluleStyleBordure.getGauche() != null) {
					style.setBorderLeft((BorderStyle) bt.transale(celluleStyleBordure.getGauche()));
				}
			}

			// mise en page du texte (font)
			if (celluleStyleFont != null) {

				// gras
				font.setBold(celluleStyleFont.isGras());

				// italic
				font.setItalic(celluleStyleFont.isItalic());

				// sousligné
				if (celluleStyleFont.getSouligne() != null) {
					FontSousligneTranslator fst = new FontSousligneTranslatorXls();
					font.setUnderline(((Integer) fst.transale(celluleStyleFont.getSouligne())).byteValue());
				}

				// couleur
				if (celluleStyleFont.getCouleur() != null) {
					font.setColor(((Integer) ct.transale(celluleStyleFont.getCouleur())).shortValue());
				}

				// taille de la police
				if (celluleStyleFont.getTaille() != null) {
					font.setFontHeightInPoints(celluleStyleFont.getTaille().shortValue());
				}
			}

			// remplissage de la cellule
			if (celluleStyleRemplissage != null) {

				// type de remplissage
				if (celluleStyleRemplissage.getType() != null) {
					RemplissageTranslator rt = new RemplissageTranslatorXls();
					style.setFillPattern((FillPatternType) rt.transale(celluleStyleRemplissage.getType()));
				}

				// remplissage de premier plan
				if (celluleStyleRemplissage.getPremierPlan() != null) {
					style.setFillForegroundColor(((Integer) ct.transale(celluleStyleRemplissage.getPremierPlan())).shortValue());
				}

				// remplissage d'arrière plan
				if (celluleStyleRemplissage.getArrierePlan() != null) {
					style.setFillBackgroundColor(((Integer) ct.transale(celluleStyleRemplissage.getArrierePlan())).shortValue());
				}

			}
		}

		return style;
	}

	private void extraireStyleProprietes(CellStyleClef cellStyleClef, CellStyle cellStyle, HSSFWorkbook workbook) {

		// retour à la ligne du texte de la cellule
		cellStyleClef.setRetourLigne(cellStyle.getWrapText());

		// alignement du texte de la cellule
		cellStyleClef.setAlignementVertical(cellStyle.getVerticalAlignment());
		cellStyleClef.setAlignementHorizontal(cellStyle.getAlignment());

		// bordure de la cellule
		cellStyleClef.setBordureBasse(cellStyle.getBorderBottom());
		cellStyleClef.setBordureDroite(cellStyle.getBorderRight());
		cellStyleClef.setBordureGauche(cellStyle.getBorderLeft());
		cellStyleClef.setBordureHaute(cellStyle.getBorderTop());

		// bordure de la cellule - couleur
		cellStyleClef.setBordureBasseCouleur(cellStyle.getBottomBorderColor());
		cellStyleClef.setBordureDroiteCouleur(cellStyle.getRightBorderColor());
		cellStyleClef.setBordureGaucheCouleur(cellStyle.getLeftBorderColor());
		cellStyleClef.setBordureHauteCouleur(cellStyle.getTopBorderColor());

		// mise en page du texte (font)
		Font font = workbook.getFontAt(cellStyle.getFontIndex());
		cellStyleClef.setPoliceGras(font.getBold());
		cellStyleClef.setPoliceItalique(font.getItalic());
		cellStyleClef.setPoliceSousligne(font.getUnderline());
		cellStyleClef.setPoliceCouleur(font.getColor());
		cellStyleClef.setPoliceTaille(font.getFontHeight());
		cellStyleClef.setPoliceTailleEnPoints(font.getFontHeightInPoints());
		cellStyleClef.setPoliceBarre(font.getStrikeout());
		cellStyleClef.setPoliceDecalage(font.getTypeOffset());
		cellStyleClef.setPoliceEncoding(font.getCharSet());
		cellStyleClef.setPoliceNom(font.getFontName());

		// remplissage de la cellule
		cellStyleClef.setTypeRemplissage(cellStyle.getFillPattern());
		cellStyleClef.setRemplissageArrierePlan(cellStyle.getFillBackgroundColor());
		cellStyleClef.setRemplissagePremierPlan(cellStyle.getFillForegroundColor());

		// autre
		cellStyleClef.setVerrouillee(cellStyle.getLocked());
		cellStyleClef.setCachee(cellStyle.getHidden());
		cellStyleClef.setFormatDonneeIndex(cellStyle.getDataFormat());
		cellStyleClef.setIndentation(cellStyle.getIndention());
		cellStyleClef.setRotation(cellStyle.getRotation());
	}

	private void duppliquerFont(Font original, Font copie) {
		copie.setBold(original.getBold());
		copie.setItalic(original.getItalic());
		copie.setUnderline(original.getUnderline());
		copie.setColor(original.getColor());
		copie.setFontHeight(original.getFontHeight());
		copie.setFontHeightInPoints(original.getFontHeightInPoints());
		copie.setCharSet(original.getCharSet());
		copie.setFontName(original.getFontName());
		copie.setStrikeout(original.getStrikeout());
		copie.setTypeOffset(original.getTypeOffset());
	}

	private void duppliquerStyle(CellStyle original, CellStyle copie) {
		copie.cloneStyleFrom(original);
	}

}
