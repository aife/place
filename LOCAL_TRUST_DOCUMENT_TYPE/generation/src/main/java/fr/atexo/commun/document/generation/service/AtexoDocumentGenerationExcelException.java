package fr.atexo.commun.document.generation.service;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class AtexoDocumentGenerationExcelException extends AtexoDocumentGenerationException {

    public AtexoDocumentGenerationExcelException(ExceptionLogMessages e) {
        super(e);
    }

    public AtexoDocumentGenerationExcelException(String message) {
        super(message);
    }
}
