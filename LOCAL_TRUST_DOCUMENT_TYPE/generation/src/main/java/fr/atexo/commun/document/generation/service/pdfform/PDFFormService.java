package fr.atexo.commun.document.generation.service.pdfform;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckbox;
import org.apache.pdfbox.pdmodel.interactive.form.PDChoiceField;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioCollection;

import de.schlichtherle.io.FileInputStream;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.TypeDonnee;
import fr.atexo.commun.document.generation.service.AtexoDocumentGenerationException;
import fr.atexo.commun.document.generation.service.openoffice.TemplateGenerateur;

public class PDFFormService {

	private static final Log LOG = LogFactory.getLog(TemplateGenerateur.class);

	/**
	 * @param champsFusion
	 * @param template
	 * @param dstFile
	 * @throws AtexoDocumentGenerationException
	 */
	public void fillForm(List<ChampFusionDTO> champsFusion, File template, File dstFile) throws AtexoDocumentGenerationException {
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(template);
			fos = new FileOutputStream(dstFile);
			fillForm(champsFusion, fis, fos);
		} catch (AtexoDocumentGenerationException e) {
			throw e;
		} catch (Exception e) {
			throw new AtexoDocumentGenerationException(e.getMessage());
		} finally {
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(fos);
		}
	}

	/**
	 * @param champsFusion
	 * @param template
	 * @param dstFile
	 * @throws AtexoDocumentGenerationException
	 */
	public void fillForm(List<ChampFusionDTO> champsFusion, InputStream template, File dstFile) throws AtexoDocumentGenerationException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(dstFile);
			fillForm(champsFusion, template, fos);
		} catch (AtexoDocumentGenerationException e) {
			throw e;
		} catch (Exception e) {
			throw new AtexoDocumentGenerationException(e.getMessage());
		} finally {
			IOUtils.closeQuietly(fos);
		}
	}

	/**
	 * @param champsFusion
	 * @param template
	 * @param dstFile
	 * @throws AtexoDocumentGenerationException
	 */
	public void fillForm(List<ChampFusionDTO> champsFusion, File template, OutputStream outputStream) throws AtexoDocumentGenerationException {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(template);
			fillForm(champsFusion, fis, outputStream);
		} catch (AtexoDocumentGenerationException e) {
			throw e;
		} catch (Exception e) {
			throw new AtexoDocumentGenerationException(e.getMessage());
		} finally {
			IOUtils.closeQuietly(fis);
		}
	}

	/**
	 * @param champsFusion
	 * @param template
	 * @param dstFile
	 * @throws AtexoDocumentGenerationException
	 */
	@SuppressWarnings("rawtypes")
	public void fillForm(List<ChampFusionDTO> champsFusion, InputStream template, OutputStream dstFile) throws AtexoDocumentGenerationException {
		try {
			PDDocument document = PDDocument.load(template);
			PDDocumentCatalog docCatalog = document.getDocumentCatalog();
			if (docCatalog == null) {
				return;
			}

			PDAcroForm acroForm = docCatalog.getAcroForm();
			if (acroForm == null) {
				return;
			}

			for (ChampFusionDTO champFusionDTO : champsFusion) {
				if (StringUtils.isNotBlank(champFusionDTO.getNom())) {
					List fields = acroForm.getFields();
					PDField field = null;
					for (Object field_ : fields) {
						if (field_ instanceof PDField) {
							field = (PDField) field_;
							if (field != null && field.getPartialName().equals(champFusionDTO.getNom())) {
								break;
							}
						}
					}

					if (field != null) {
						if (champFusionDTO.getTypeDonnee() == TypeDonnee.BOOLEAN) {
							if (champFusionDTO.getValeur() instanceof Boolean && field instanceof PDCheckbox) {
								Boolean value = (Boolean) champFusionDTO.getValeur();
								if (value) {
									((PDCheckbox) field).check();
								} else {
									((PDCheckbox) field).unCheck();
								}
							} else {
								LOG.warn("Le champs " + champFusionDTO.getNom() + " est incompatible avec le formualire");
							}
						} else if (champFusionDTO.getTypeDonnee() == TypeDonnee.LIST) {
							// PDFBox ne gère pas les Listbox multiselect .. ça
							// va pas marcher
							if (champFusionDTO.getValeur() instanceof List && field instanceof PDChoiceField) {
								PDChoiceField choiceField = (PDChoiceField) field;
								for (Object value : fields) {
									String valueAsStr = getStringValue(value);
									if (valueAsStr != null) {
										choiceField.setValue(value.toString());
									}
								}
							} else {
								LOG.warn("Le champs " + champFusionDTO.getNom() + " est incompatible avec le formualire");
							}
						} else {

							String valueAsStr = getStringValue(champFusionDTO.getValeur());
							if (valueAsStr != null) {
								field.setValue(valueAsStr);
							} else {
								LOG.warn("Le champs " + champFusionDTO.getNom() + " est incompatible avec le formualire");
							}
						}
					}
				}
			}

			document.save(dstFile);
			document.close();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new AtexoDocumentGenerationException(e.getMessage(), e);
		}
	}

	/**
	 * @param file
	 * @return
	 * @throws AtexoDocumentGenerationException
	 */
	public List<PDFFormField> getFormFieldsNames(File file) throws AtexoDocumentGenerationException {
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);
			return getFormFieldsNames(fis);

		} catch (AtexoDocumentGenerationException e) {
			throw e;
		} catch (Exception e) {
			throw new AtexoDocumentGenerationException(e.getMessage());
		} finally {
			IOUtils.closeQuietly(fis);
		}
	}

	/**
	 * @param templates
	 * @return
	 */
	public List<PDFFormField> getFormFieldsNames(InputStream template) throws AtexoDocumentGenerationException {

		List<PDFFormField> result = new ArrayList<PDFFormField>();

		try {

			PDDocument document = PDDocument.load(template);
			PDDocumentCatalog docCatalog = document.getDocumentCatalog();
			if (docCatalog == null) {
				return null;
			}

			PDAcroForm acroForm = docCatalog.getAcroForm();
			if (acroForm == null) {
				return null;
			}

			@SuppressWarnings("rawtypes")
			List fields = acroForm.getFields();

			for (Object field_ : fields) {

				if (field_ instanceof PDField) {

					PDField field = (PDField) field_;

					if (field instanceof PDCheckbox) {
						result.add(new PDFFormField(field.getPartialName(), TypeDonnee.BOOLEAN));

					} else {
						PDFFormField formField = new PDFFormField(field.getPartialName(), TypeDonnee.STRING);
						if (field instanceof PDRadioCollection) {
							PDRadioCollection radio = (PDRadioCollection) field;
							if (radio.getKids() != null) {
								for (Object kid_ : radio.getKids()) {
									if (kid_ instanceof PDCheckbox) {
										PDCheckbox checkbox = (PDCheckbox) kid_;
										formField.addValeurPossible(checkbox.getOnValue());
									}
								}
							}
						} else if (field instanceof PDChoiceField) {
							PDChoiceField choice = (PDChoiceField) field;

							COSArray options = (COSArray) choice.getDictionary().getDictionaryObject("Opt");
							for (int i = 0; i < options.size(); i++) {
								COSBase option = options.getObject(i);

								if (option instanceof COSArray) {
									COSArray keyValuePair = (COSArray) option;
									COSString key = (COSString) keyValuePair.getObject(0);
									formField.addValeurPossible(key.getString());
								}
							}
						}
						result.add(formField);
					}
				}
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new AtexoDocumentGenerationException(e.getMessage(), e);
		}

		return result;
	}

	/**
	 * @param obj
	 * @return
	 */
	private String getStringValue(Object obj) {
		if (obj instanceof String) {
			return (String) obj;
		} else if (obj instanceof Number) {
			return ((Number) obj).toString();
		} else if (obj instanceof Date) {
			Date date = (Date) obj;
			return new SimpleDateFormat().format(date);
		} else {
			return null;
		}
	}
}
