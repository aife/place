package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCelluleType;

public interface CelluleTypeTranslator {

	public Object transale(EnumerationCelluleType celluleType);

}
