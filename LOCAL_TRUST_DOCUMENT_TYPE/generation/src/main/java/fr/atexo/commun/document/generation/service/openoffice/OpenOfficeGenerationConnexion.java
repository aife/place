/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice;

import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class OpenOfficeGenerationConnexion {

    private static final Log LOG = LogFactory.getLog(OpenOfficeGenerationConnexion.class);

    private String host;

    private String port;

    // déclaration des propriétés du document OpenOffice
    private XComponentContext mxRemoteContext = null;
    private XMultiComponentFactory mxRemoteServiceManager;
    private XPropertySet xIndex;

    /*
    * Constructeur permettant de créer une connexion au serveur OpenOffice avec
    * host = localhost
    * port = 8100
    * */
    public OpenOfficeGenerationConnexion() throws Exception {
        this("localhost", "8100");
    }

    /*
   * Constructeur permettant de créer un une connexion au serveur OpenOffice avec le host et le port
   * définis
   *
   * */

    public OpenOfficeGenerationConnexion(String host, String port) throws Exception {
        this.host = host;
        this.port = port;
        initializeComponentContext();
    }

    /**
     * Méthode permettant d'initialiser la connection vers le service d'open
     * office
     *
     * @throws Exception
     */
    private void initializeComponentContext() throws java.lang.Exception {

        LOG.info("Tentative d'initialisation de la connection vers le service d'open office");

        // on initialise le XComponentContext et le XMultiComponentFactory
        mxRemoteContext = Bootstrap.createInitialComponentContext(null);

        mxRemoteServiceManager = mxRemoteContext.getServiceManager();
        Object urlResolver = mxRemoteServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", mxRemoteContext);
        XUnoUrlResolver xUnoUrlResolver = (XUnoUrlResolver) UnoRuntime.queryInterface(XUnoUrlResolver.class, urlResolver);

        // connection au service open office
        Object initialObject = xUnoUrlResolver.resolve("uno:socket,host=" + host + ",port=" + port + ";urp;StarOffice.ServiceManager");
        xIndex = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, initialObject);
        Object remoteContext = xIndex.getPropertyValue("DefaultContext");
        mxRemoteContext = (XComponentContext) UnoRuntime.queryInterface(XComponentContext.class, remoteContext);
        mxRemoteServiceManager = mxRemoteContext.getServiceManager();
    }

    private XComponentLoader getComponentLoaderImpl() throws Exception {
        // on récupère l'objet desktop afin de récupérer l'interface
        // XComponentLoader
        Object desktop = mxRemoteServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", mxRemoteContext);
        XComponentLoader xComponentLoader = (XComponentLoader) UnoRuntime.queryInterface(XComponentLoader.class, desktop);
        return xComponentLoader;
    }

    /**
     * Méthode permettant d'obtenir le XComponentLoader de la connexion OpenOffice
     * Si le service n'est pas accesible, la connexion est retentée une fois
     *
     * @throws Exception
     */
    public XComponentLoader getComponentLoader() throws Exception {
        try {
            return getComponentLoaderImpl();
        }
        catch (Exception e) {
            LOG.info(e.getMessage(), e);
            //retry the connection...
            initializeComponentContext();
            return getComponentLoaderImpl();
        }
    }
}
