package fr.atexo.commun.document.generation.service.tableaudef.bean;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;

/**
 * @author MZO
 * 
 */
public class CelluleHelper {

	/**
	 * Méthode utilisée pour récuperer les coordonnées d'une cellule. Elle prend
	 * en compte les anciennes versions du XML
	 * 
	 * @param cellule
	 * @return
	 */
	public static CelluleCoordonnee getCelluleCoordonnee(Cellule cellule) {
		if (cellule.getCoordonnee() != null) {
			return cellule.getCoordonnee();
		} else if (cellule.getCoordonne() != null) {
			return cellule.getCoordonne();
		} else {
			return null;
		}
	}

	/**
	 * @param cellule
	 * @return
	 */
	public static Double getValeurNumeriqueDouble(Cellule cellule) {
		char separateurFlotant = ',';
		if (cellule.getValeurNumerique() != null && StringUtils.contains(cellule.getValeurNumerique(), separateurFlotant)) {
			DecimalFormatSymbols symbols = new DecimalFormatSymbols();
			symbols.setDecimalSeparator(separateurFlotant);
			DecimalFormat format = new DecimalFormat("0.#");
			format.setDecimalFormatSymbols(symbols);
			try {
				Double valeur = format.parse(cellule.getValeurNumerique()).doubleValue();
				cellule.setValeurNumerique(valeur.toString());
				return valeur;
			} catch (ParseException e) {
				return null;
			}

		} else if (cellule.getValeurNumerique() != null) {
			return Double.valueOf(cellule.getValeurNumerique());
		} else {
			return null;
		}
	}

	/**
	 * @param cellule
	 * @return
	 */
	public static boolean isAppliquerAncienStyle(Cellule cellule) {
		if (cellule.getBordureBas() == null && cellule.getBordureDroite() == null && cellule.getBordureGauche() == null && cellule.getBordureHaut() == null
				&& cellule.getTaillePolice() == null && cellule.getCouleurPolice() == null && cellule.getCouleurPolice() == null && cellule.getGras() == null
				&& cellule.getRetourLigne() == null) {
			return false;
		} else {
			return true;
		}
	}
}
