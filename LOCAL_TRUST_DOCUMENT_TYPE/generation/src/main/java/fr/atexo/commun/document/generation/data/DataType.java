/**
 * $Id$
 */
package fr.atexo.commun.document.generation.data;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public enum DataType {

    STRING("Texte", "stringValue"), TEXT("Texte long", "textValue"), LIST("Liste d'éléments"),
    DATE("Date", "dateValue"), BOOLEAN("Booléen", "booleanValue"), LONG("Entier", "longValue"),
    DOUBLE("Décimal", "doubleValue"), SECURITE_SOCIALE("Numéro de Sécurité sociale"),
    RIB("RIB"), ADRESSE("Adresse"), FICHIER("Fichier"), OBJECT("Objet"), ENUM("Enumeration"),
    REFERENTIEL("Référentiel", "referentielValeur"), TABLE("Tableau d'éléments");

    public static final DataType[] XML_DATA_TYPES = {STRING, TEXT, LIST, DATE, BOOLEAN, LONG, DOUBLE, SECURITE_SOCIALE,
            RIB, ADRESSE, FICHIER, REFERENTIEL, TABLE};

    public static DataType[] getXmlDataTypes() {
        return XML_DATA_TYPES;
    }


    private String fieldName;
    private String label;

    DataType() {

    }

    DataType(String label) {
        this.label = label;
    }

    DataType(String label, String fieldName) {
        this(label);
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        if (fieldName == null) {
            throw new UnsupportedOperationException();
        }
        return fieldName;
    }

    public boolean isTextType() {
        return this == STRING || this == TEXT;
    }


    public String getLabel() {
        return label;
    }
}
