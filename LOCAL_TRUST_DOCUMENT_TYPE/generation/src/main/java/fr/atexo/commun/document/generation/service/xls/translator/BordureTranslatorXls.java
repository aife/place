package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.ss.usermodel.BorderStyle;

import fr.atexo.commun.document.generation.service.tableaudef.BordureTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationBordure;

public class BordureTranslatorXls implements BordureTranslator {

	@Override
	public BorderStyle transale(EnumerationBordure bordure) {

		BorderStyle translation;

		switch (bordure) {
			case BORDER_NONE:
				translation = BorderStyle.NONE;
				break;

			case BORDER_THIN:
				translation = BorderStyle.THIN;
				break;

			case BORDER_MEDIUM:
				translation = BorderStyle.MEDIUM;
				break;

			case BORDER_DASHED:
				translation = BorderStyle.DASHED;
				break;

			case BORDER_HAIR:
				translation = BorderStyle.HAIR;
				break;

			case BORDER_THICK:
				translation = BorderStyle.THICK;
				break;

			case BORDER_DOUBLE:
				translation = BorderStyle.DOUBLE;
				break;

			case BORDER_DOTTED:
				translation = BorderStyle.DOTTED;
				break;

			case BORDER_MEDIUM_DASHED:
				translation = BorderStyle.MEDIUM_DASHED;
				break;

			case BORDER_DASH_DOT:
				translation = BorderStyle.DASH_DOT;
				break;

			case BORDER_MEDIUM_DASH_DOT:
				translation = BorderStyle.MEDIUM_DASH_DOT;
				break;

			case BORDER_DASH_DOT_DOT:
				translation = BorderStyle.DASH_DOT_DOT;
				break;

			case BORDER_MEDIUM_DASH_DOT_DOT:
				translation = BorderStyle.MEDIUM_DASH_DOT_DOT;
				break;

			case BORDER_SLANTED_DASH_DOT:
				translation = BorderStyle.SLANTED_DASH_DOT;
				break;

			default:
				translation = BorderStyle.NONE;
				break;
		}

		return translation;
	}

}
