package fr.atexo.commun.document.generation.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public abstract class ImageUtil {

    private static final Log LOG = LogFactory.getLog(ImageUtil.class);

    public static Dimension getDimension(final String path) {
        Dimension result = null;
        String suffix = getFileSuffix(path);
        Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
        if (iter.hasNext()) {
            ImageReader reader = iter.next();
            try {
                ImageInputStream stream = new FileImageInputStream(new File(path));
                reader.setInput(stream);
                int width = reader.getWidth(reader.getMinIndex());
                int height = reader.getHeight(reader.getMinIndex());
                result = new Dimension(width, height);
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            } finally {
                reader.dispose();
            }
        } else {
            LOG.warn("Aucun lecteur trouvé pour le format : " + suffix);
        }
        return result;
    }

    private static String getFileSuffix(final String path) {

        String result = null;
        if (path != null) {
            result = "";
            if (path.lastIndexOf('.') != -1) {
                result = path.substring(path.lastIndexOf('.'));
                if (result.startsWith(".")) {
                    result = result.substring(1);
                }
            }
        }
        return result;
    }


    /**
     * Méthode permettant de récupérer une image à partir d'un url
     *
     * @param urlImage             le fichier
     * @param repertoireTemporaire le path du répertoire temporaire dans lequel sera téléchargé temporairement l'image
     * @param infosComplementaires si on veut les tailes (hauteur, longueur) renseignés en plus
     * @return le fichier et ses infos complémentaires
     */
    public static GraphicInfo getGraphicInfo(String urlImage, String repertoireTemporaire, boolean infosComplementaires) throws IOException {
        return getGraphicInfo(urlImage, new File(repertoireTemporaire), infosComplementaires);
    }

    /**
     * Méthode permettant de récupérer une image à partir d'un url
     *
     * @param urlImage             le fichier
     * @param repertoireTemporaire le répertoire temporaire dans lequel sera téléchargé temporairement l'image
     * @param infosComplementaires si on veut les tailes (hauteur, longueur) renseignés en plus
     * @return le fichier et ses infos complémentaires
     */
    public static GraphicInfo getGraphicInfo(String urlImage, File repertoireTemporaire, boolean infosComplementaires) {

        GraphicInfo graphicInfo = null;
        File file = null;
        try {
            URL url = new URL(urlImage);
            String extension = FilenameUtils.getExtension(urlImage);

            file = File.createTempFile("image_", "." + extension, repertoireTemporaire);

            FileUtils.copyURLToFile(url, file);

            if (infosComplementaires) {
                BufferedImage bufferedImage = ImageIO.read(file);
                int width = bufferedImage.getWidth();
                int height = bufferedImage.getHeight();
                graphicInfo = new GraphicInfo(file, extension, height, width);
            } else {
                graphicInfo = new GraphicInfo(file, extension);
            }
        } catch (IOException e) {
            FileUtils.deleteQuietly(file);
        }

        return graphicInfo;
    }

    public static class GraphicInfo {

        private File image;

        private String extension;

        private int width;

        private int height;

        public GraphicInfo(File image, String extension) {
            this.extension = extension;
            this.image = image;
        }

        private GraphicInfo(File image, String extension, int height, int width) {
            this(image, extension);
            this.height = height;
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public File getImage() {
            return image;
        }

        public int getWidth() {
            return width;
        }

        public String getExtension() {
            return extension;
        }
    }


}
