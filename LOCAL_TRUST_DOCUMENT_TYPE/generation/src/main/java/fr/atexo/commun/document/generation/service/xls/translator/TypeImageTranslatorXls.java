package fr.atexo.commun.document.generation.service.xls.translator;

import java.util.Arrays;

import org.apache.poi.ss.usermodel.Workbook;

import fr.atexo.commun.document.generation.service.tableaudef.TypeImageTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationTypeImage;

public class TypeImageTranslatorXls implements TypeImageTranslator {

	@Override
	public Object transale(EnumerationTypeImage typeImage) {

		int translation = 0;

		switch (typeImage) {
		case EMF:
			translation = Workbook.PICTURE_TYPE_EMF;
			break;

		case WMF:
			translation = Workbook.PICTURE_TYPE_WMF;
			break;

		case PICT:
			translation = Workbook.PICTURE_TYPE_PICT;
			break;

		case JPG:
			translation = Workbook.PICTURE_TYPE_JPEG;
			break;

		case PNG:
			translation = Workbook.PICTURE_TYPE_PNG;
			break;

		case DIB:
			translation = Workbook.PICTURE_TYPE_DIB;
			break;

		default:
			break;
		}

		return new Integer(translation);
	}

	public String[] transaleExtentions(EnumerationTypeImage typeImage) {

		String[] translation = null;

		switch (typeImage) {
		case EMF:
			translation = new String[] {};
			break;

		case WMF:
			translation = new String[] {};
			break;

		case PICT:
			translation = new String[] {};
			break;

		case JPG:
			translation = new String[] { "jpg,jpeg" };
			break;

		case PNG:
			translation = new String[] { "png" };
			break;

		case DIB:
			translation = new String[] {};
			break;

		default:
			break;
		}

		return translation;
	}

	public EnumerationTypeImage fromExtension(String extension) {
		if (extension != null) {
			for (EnumerationTypeImage enumerationTypeImage : EnumerationTypeImage.values()) {
				String[] extensions = transaleExtentions(enumerationTypeImage);
				if (extensions.length > 0 && Arrays.asList(extensions).contains(extension.toLowerCase())) {
					return enumerationTypeImage;
				}
			}
		}
		return null;
	}

}
