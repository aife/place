package fr.atexo.commun.document.generation.service.openoffice.translator;

import com.sun.star.awt.FontUnderline;

import fr.atexo.commun.document.generation.service.tableaudef.FontSousligneTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationFontSousligne;

public class FontSousligneTranslatorOdt implements FontSousligneTranslator {

	@Override
	public Object transale(EnumerationFontSousligne fontSousligne) {

		short translation = 0;

		switch (fontSousligne) {
		case U_NONE:
			translation = FontUnderline.NONE;
			break;

		case U_SINGLE:
			translation = FontUnderline.SINGLE;
			break;

		case U_DOUBLE:
			translation = FontUnderline.DOUBLE;
			break;

		case U_SINGLE_ACCOUNTING:
			translation = FontUnderline.SINGLE;
			break;

		case U_DOUBLE_ACCOUNTING:
			translation = FontUnderline.DOUBLE;
			break;

		default:
			break;
		}

		return new Integer(translation);

	}
}
