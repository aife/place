package fr.atexo.commun.document.generation.service.pdfform;

import java.util.ArrayList;
import java.util.List;

import fr.atexo.commun.document.administration.commun.TypeDonnee;

public class PDFFormField {

	private String nom;

	private TypeDonnee typeDonnee;

	private List<String> valeursPossibles;

	public PDFFormField(String nom, TypeDonnee typeDonnee) {
		super();
		this.nom = nom;
		this.typeDonnee = typeDonnee;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public TypeDonnee getTypeDonnee() {
		return typeDonnee;
	}

	public void setTypeDonnee(TypeDonnee typeDonnee) {
		this.typeDonnee = typeDonnee;
	}

	public List<String> getValeursPossibles() {
		return valeursPossibles;
	}

	public void setValeursPossibles(List<String> valeursPossibles) {
		this.valeursPossibles = valeursPossibles;
	}

	public void addValeurPossible(String valeurPossible) {
		if (valeursPossibles == null) {
			valeursPossibles = new ArrayList<String>();
		}
		valeursPossibles.add(valeurPossible);
	}
}
