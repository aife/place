package fr.atexo.commun.document.generation.service.openoffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.star.awt.FontSlant;
import com.sun.star.awt.FontWeight;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XIndexAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XNameContainer;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.table.XCell;
import com.sun.star.table.XCellRange;
import com.sun.star.text.TableColumnSeparator;
import com.sun.star.text.TextContentAnchorType;
import com.sun.star.text.XDependentTextField;
import com.sun.star.text.XDocumentIndex;
import com.sun.star.text.XDocumentIndexesSupplier;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextTableCursor;
import com.sun.star.uno.AnyConverter;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.CloseVetoException;
import com.sun.star.util.XRefreshable;

import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;
import fr.atexo.commun.document.generation.service.AtexoDocumentGenerationException;
import fr.atexo.commun.document.generation.service.ExceptionLogMessages;
import fr.atexo.commun.document.generation.service.TransformationService;
import fr.atexo.commun.document.generation.service.openoffice.translator.AlignementHorizontalTranslatorOdt;
import fr.atexo.commun.document.generation.service.openoffice.translator.AlignementVerticalTranslatorOdt;
import fr.atexo.commun.document.generation.service.openoffice.translator.CouleurTranslatorOdt;
import fr.atexo.commun.document.generation.service.openoffice.translator.FontSousligneTranslatorOdt;
import fr.atexo.commun.document.generation.service.tableaudef.AlignementHorizontalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.AlignementVerticalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.CouleurTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.FontSousligneTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.AttributLargeurColonne;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Cellule;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleCoordonnee;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleHelper;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyle;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleAlignement;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleBordure;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleFont;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyleRemplissage;
import fr.atexo.commun.document.generation.service.tableaudef.bean.DocumentXLS;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Ligne;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Onglet;
import fr.atexo.commun.document.generation.service.tableaudef.bean.RegionFusionnee;
import fr.atexo.commun.document.generation.service.tableaudef.bean.TableauAttributs;
import fr.atexo.commun.document.generation.util.ImageUtil;

/**
 * Class en charge de la génération des documents OpenOffice
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
public class OpenOfficeGenerateur {

  // déclaration du log
  private static final Log LOG = LogFactory.getLog(OpenOfficeGenerateur.class);

  private static final String RACINE_FIELD_MASTER = "com.sun.star.text.fieldmaster";

  private static final int TAILLE_MULTIPLICATEUR_IMAGE = 26;

  // Le fichier temp utilisé pour la création d'un fichier par OpenOffice
  private File tempFile;

  // Nom du répertoire dans le cas d'une création par lot
  private String lotDirectoryName;

  // Path du répertoire dans lequel OpenOffice va créer ses fichiers
  // temporaires
  private final String workDirectoryPath;

  // La connexion OpenOffice
  private final OpenOfficeGenerationConnexion openOfficeGenerationConnexion;

  // Le document à générer
  private final DocumentDTO documentDTO;

  // La template
  private final FichierDTO template;

  // La map de beans à transmettre au service freemarker en cas où
  private Map<String, Object> freeMarkerMap;

  // Ensemble d'attributes propres au Service OpenOffice
  private XComponent xTemplateComponent;
  private XTextDocument xDoc;
  private XMultiServiceFactory mxDocFactory;

  protected enum FieldMasterType {
    GetExpression, SetExpression, User;
  }

  /**
   * Constructeur utilisable pour utilise la calsse en tant que classe utilitaire ( notamment pour
   * l'insertion des tableaux )
   */
  public OpenOfficeGenerateur() {
    this.openOfficeGenerationConnexion = null;
    this.documentDTO = null;
    this.template = null;
    this.workDirectoryPath = null;
  }

  /**
   * Constructeur à utiliser lors de la génération d'un document
   * 
   * @param openOfficeGenerationConnexion La connexion OpenOffice
   *        {@link OpenOfficeGenerationConnexion}
   * @param documentDTO Le document à générer
   * @param template La template odt
   * @param workDirectoryPath Path du répertoire dans lequel OpenOffice va créer ses fichiers
   *        temporaires
   * @throws AtexoDocumentGenerationException si le template n'existe pas
   */
  public OpenOfficeGenerateur(OpenOfficeGenerationConnexion openOfficeGenerationConnexion, DocumentDTO documentDTO, FichierDTO template, String workDirectoryPath) throws Exception {

    if (template == null || template.getFichier() == null || !template.getFichier().exists()) {
      throw new AtexoDocumentGenerationException(ExceptionLogMessages.templateNonTrouvee);
    }

    this.openOfficeGenerationConnexion = openOfficeGenerationConnexion;
    this.documentDTO = documentDTO;
    this.template = template;
    this.workDirectoryPath = workDirectoryPath;
  }

  /**
   * Méthode permettant de créer un nouveau Xcomponent à partir du chemin vers un fichier template
   * de type odt
   * 
   * @param docTitle Le titre du document
   * @param loadUrl L'url du template 'odt'
   * @return XComponent
   * @throws Exception
   */
  private XComponent createComponentFromTemplate(String docTitle, String loadUrl) throws Exception {
    XComponentLoader xComponentLoader = openOfficeGenerationConnexion.getComponentLoader();

    // on définit les propriétés de chargement
    ArrayList<PropertyValue> props = new ArrayList<PropertyValue>();
    PropertyValue p = null;
    if (loadUrl != null) {
      // on précise que l'on va utiliser un template
      p = new PropertyValue();
      p.Name = OpenOfficeProprietes.AsTemplate.name();
      p.Value = true;
      props.add(p);
    }
    // on change le titre du document si celui-ci a été précisé
    if ((docTitle != null) && (docTitle.length() > 0)) {
      p = new PropertyValue();
      p.Name = OpenOfficeProprietes.DocumentTitle.name();
      p.Value = docTitle;
      props.add(p);
    }

    // on précise ces propriétés afin que lors de la construction du
    // document pour que cette construction se fasse de manière invisible
    // pour l'utilisateur.
    p = new PropertyValue();
    p.Name = OpenOfficeProprietes.Hidden.name();
    p.Value = true;
    props.add(p);

    PropertyValue[] properties = new PropertyValue[props.size()];
    props.toArray(properties);

    LOG.info("template path: " + loadUrl);

    return xComponentLoader.loadComponentFromURL(loadUrl, "_blank", 0, properties);
  }

  /**
   * Méthode permettant de retourner le chemin d'un fichier en file:///
   * 
   * @param file le fichier
   * @return String le path du fichier en forme de URL
   */
  private String getUnooURL(String file) {
    File f = new File(file);
    StringBuilder sb = new StringBuilder("file:///");
    try {
      sb.append(f.getCanonicalPath().replace('\\', '/'));
    } catch (IOException e) {
      LOG.error(e.getMessage(), e);
    }
    return sb.toString();
  }

  private XTextFieldsSupplier getXTextFieldsSupplier(XComponent xTemplateComponent) {

    // récupération des interfaces XTextFieldsSupplier et XBookmarksSupplier
    XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier) UnoRuntime.queryInterface(XTextFieldsSupplier.class, xTemplateComponent);
    return xTextFieldsSupplier;
  }

  private XEnumerationAccess getXEnumerationAccess(XTextFieldsSupplier xTextFieldsSupplier) {

    // accès aux TextFields
    XEnumerationAccess xEnumeratedFields = xTextFieldsSupplier.getTextFields();

    return xEnumeratedFields;
  }

  private XNameAccess getXNameAccess(XTextFieldsSupplier xTextFieldsSupplier) {

    // accès aux TextFields et aux collections du TextFieldMasters
    XNameAccess xNamedFieldMasters = xTextFieldsSupplier.getTextFieldMasters();

    return xNamedFieldMasters;
  }

  private void setXPropertySet(XPropertySet xPropertySet, String key, Object value) {
    try {
      xPropertySet.setPropertyValue(key, value);
    } catch (UnknownPropertyException e) {
      LOG.warn("Aucun champs nommé " + key + "  n'a été trouve dans le document");
    } catch (PropertyVetoException | WrappedTargetException | IllegalArgumentException e) {
      LOG.error(e.getMessage(), e);
    }
  }

  private void refreshFieldMasters(XEnumerationAccess xEnumeratedFields) {

    // refresh des textfields
    XRefreshable xRefreshable = (XRefreshable) UnoRuntime.queryInterface(XRefreshable.class, xEnumeratedFields);
    xRefreshable.refresh();
  }

  /**
   * Méthode permettant de remplacer la liste des champs se trouvant dans le document template
   */
  private void replaceFieldMastersByValues(XTextDocument xDoc, XMultiServiceFactory mxDocFactory, OpenOfficeValeurs openOfficeValeurs) {

    // on vérifie que la hashtable des pairs clef-valeur est non nulle ou
    // non vide
    if (openOfficeValeurs.getValeurs() != null && !openOfficeValeurs.getValeurs().isEmpty()) {

      // récupération des interfaces XTextFieldsSupplier et
      // XBookmarksSupplier
      XTextFieldsSupplier xTextFieldsSupplier = getXTextFieldsSupplier(this.xTemplateComponent);

      // accès aux TextFields et aux collections du TextFieldMasters
      XNameAccess xNamedFieldMasters = getXNameAccess(xTextFieldsSupplier);
      XEnumerationAccess xEnumeratedFields = getXEnumerationAccess(xTextFieldsSupplier);

      // récupération des clés de la Hashtable
      Set<String> set = openOfficeValeurs.getValeurs().keySet();
      Iterator<String> iterator = set.iterator();

      while (iterator.hasNext()) {

        String key = iterator.next();
        if (openOfficeValeurs.isObject(key)) {
          setFieldMasterValue(xNamedFieldMasters, FieldMasterType.User, key, openOfficeValeurs.getValeurs().get(key));
          setFieldMasterValue(xNamedFieldMasters, FieldMasterType.SetExpression, key, openOfficeValeurs.getValeurs().get(key));
        } else {
          setFieldMasterValue(xDoc, mxDocFactory, xNamedFieldMasters, FieldMasterType.SetExpression, key, openOfficeValeurs.getValeurs().get(key));
        }

        // refresh des textfields
        refreshFieldMasters(xEnumeratedFields);
      }
    }
  }

  private void setFieldMasterValue(XNameAccess xNamedFieldMasters, FieldMasterType type, String fieldname, OpenOfficeValeurs.Valeur value) {
    setFieldMasterValue(null, null, xNamedFieldMasters, type, fieldname, value);
  }

  private void setFieldMasterValue(XTextDocument xDoc, XMultiServiceFactory mxDocFactory, XNameAccess xNamedFieldMasters, FieldMasterType type, String fieldname, OpenOfficeValeurs.Valeur valeur) {

    String prefixFieldMasterType = RACINE_FIELD_MASTER + "." + type.name();
    String fieldMasterName = prefixFieldMasterType + "." + fieldname;

    try {
      Object fieldMaster = null;
      if (xNamedFieldMasters.hasByName(fieldMasterName)) {
        fieldMaster = xNamedFieldMasters.getByName(fieldMasterName);
      }

      if (fieldMaster != null) {

        XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, fieldMaster);

        if (xPropertySet != null) {

          switch (type) {
            case SetExpression:

              if (fieldMaster != null) {
                XDependentTextField[] xDependentTextFields = (XDependentTextField[]) xPropertySet.getPropertyValue("DependentTextFields");
                if (xDependentTextFields != null && xDependentTextFields.length > 0) {

                  for (XDependentTextField xDependentTextField : xDependentTextFields) {
                    if (valeur.getType() != TypeChamp.SCRIPT_TABLEAU && valeur.getType() != TypeChamp.URL_IMG) {
                      XPropertySet xPropertySetDependentTextField = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xDependentTextField);
                      if (xPropertySetDependentTextField != null) {
                        setXPropertySet(xPropertySetDependentTextField, OpenOfficeProprietes.Content.name(), valeur.getVal());
                      }
                    } else {
                      XTextRange xTextRange = xDependentTextField.getAnchor();
                      if (xTextRange != null) {

                        if (valeur.getType() == TypeChamp.URL_IMG) {

                          ImageUtil.GraphicInfo m_sGraphicInfo = null;
                          try {
                            m_sGraphicInfo = ImageUtil.getGraphicInfo(valeur.getVal().toString(), workDirectoryPath, true);

                            if (m_sGraphicInfo != null) {
                              replaceXTextRangeByImage(m_sGraphicInfo, xDoc, mxDocFactory, xTextRange);
                            }

                          } catch (IOException e) {
                            LOG.error(e.getMessage(), e);
                          } finally {
                            if (m_sGraphicInfo != null) {
                              FileUtils.deleteQuietly(m_sGraphicInfo.getImage());
                            }
                          }

                        } else if (valeur.getType() == TypeChamp.SCRIPT_TABLEAU) {
                          replaceXTextRangeByTable((DocumentXLS) valeur.getVal(), xDoc, mxDocFactory, xTextRange);
                        }

                      }
                    }
                  }
                }
              }
              break;

            case User:
              // insertion de la valeur dans le champ
              setXPropertySet(xPropertySet, OpenOfficeProprietes.Content.name(), valeur.getVal());
              break;
          }

        }
      }

    } catch (NoSuchElementException e) {
      LOG.warn("Aucun champs " + type + " nommé " + fieldname + "  n'a été trouve dans le document");
    } catch (Exception e) {
      LOG.error("Erreur lors de la récupération du champs " + fieldname + " : " + e);
    }
  }

  /**
   * Embeds the given Image into a Textdocument at the given cursor position (Anchored as character)
   * @param grProps OOo-style URL and width & height of the graphic
   */
  private boolean replaceXTextRangeByImage(ImageUtil.GraphicInfo grProps, XTextDocument xDoc, XMultiServiceFactory mxDocFactory, XTextRange xTextRange) {

    try {
      XNameContainer xBitmapContainer = (XNameContainer) UnoRuntime.queryInterface(XNameContainer.class, mxDocFactory.createInstance("com.sun.star.drawing.BitmapTable"));
      XTextContent xImage = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, mxDocFactory.createInstance("com.sun.star.text.TextGraphicObject"));
      XPropertySet xProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xImage);

      String name = "image_" + new Date().getTime();

      // helper-stuff to let OOo create an internal name of the graphic
      // that can be used later (internal name consists of various
      // checksums)
      String path = grProps.getImage().getAbsolutePath();
      String unoUrl = getUnooURL(path);
      xBitmapContainer.insertByName(name, unoUrl);
      String internalURL = AnyConverter.toString(xBitmapContainer.getByName(name));

      xProps.setPropertyValue(OpenOfficeProprietes.AnchorType.name(), com.sun.star.text.TextContentAnchorType.AS_CHARACTER);
      xProps.setPropertyValue(OpenOfficeProprietes.GraphicURL.name(), internalURL);

      xProps.setPropertyValue(OpenOfficeProprietes.Width.name(), grProps.getWidth() * TAILLE_MULTIPLICATEUR_IMAGE);
      xProps.setPropertyValue(OpenOfficeProprietes.Height.name(), grProps.getHeight() * TAILLE_MULTIPLICATEUR_IMAGE);

      // inser the graphic at the position
      xDoc.getText().insertTextContent(xTextRange, xImage, true);

      // remove the helper-entry
      xBitmapContainer.removeByName(name);

      return true;

    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      return false;
    }
  }

  public boolean replaceXTextRangeByTable(DocumentXLS documentXLS, XTextDocument xDocTextDocument, XMultiServiceFactory mxDocFactory, final XTextRange xTextRange) {
    TableInsertionCallback insertionCallback = new TableInsertionCallback() {
      @Override
      public void insertTable(XTextDocument xDocTextDocument, XTextTable xTextTable) throws Exception {
        xDocTextDocument.getText().insertTextContent(xTextRange, xTextTable, true);
      }
    };
    return replaceXTextRangeByTable(documentXLS, xDocTextDocument, mxDocFactory, insertionCallback);
  }

  public boolean insertTable(DocumentXLS documentXLS, XTextDocument xDocTextDocument, XMultiServiceFactory mxDocFactory, final XTextCursor mxDocCursor) {
    TableInsertionCallback insertionCallback = new TableInsertionCallback() {
      @Override
      public void insertTable(XTextDocument xDocTextDocument, XTextTable xTextTable) throws Exception {
        xDocTextDocument.getText().insertTextContent(mxDocCursor, xTextTable, false);
      }
    };
    return replaceXTextRangeByTable(documentXLS, xDocTextDocument, mxDocFactory, insertionCallback);
  }

  public boolean replaceXTextRangeByTable(DocumentXLS documentXLS, XTextDocument xDocTextDocument, XMultiServiceFactory mxDocFactory, TableInsertionCallback calllback) {

    try {
      Object oInt = mxDocFactory.createInstance("com.sun.star.text.TextTable");
      XTextTable xTextTable = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, oInt);


      // on utilise ne traite que le premier onglet
      Onglet onglet = documentXLS.getOnglet().get(0);

      int rowsCount = -1;
      int columnCount = -1;

      for (Cellule cell : onglet.getCellule()) {
        CelluleCoordonnee celluleCoordonnee = CelluleHelper.getCelluleCoordonnee(cell);
        if (celluleCoordonnee.getLigne() > rowsCount) {
          rowsCount = celluleCoordonnee.getLigne();
        }
        if (celluleCoordonnee.getColonne() > columnCount) {
          columnCount = celluleCoordonnee.getColonne();
        }
      }

      if (rowsCount != -1 && columnCount != -1) {
        rowsCount++;
        columnCount++;
      }

      // init du tableau
      xTextTable.initialize(rowsCount, columnCount);

      try {
        calllback.insertTable(xDocTextDocument, xTextTable);
      } catch (Exception e) {
        LOG.error(e.getMessage(), e);
        return false;
      }

      // on sette les largeurs de colonnes
      if (onglet.getAttributs() != null) {
        TableauAttributs attributs = onglet.getAttributs();

        if (attributs.getLargeurColonne() != null && columnCount > 1) {

          XPropertySet xPS = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xTextTable);
          Object xObj = xPS.getPropertyValue("TableColumnSeparators");
          TableColumnSeparator[] tableauSeparateurColonne = (TableColumnSeparator[]) UnoRuntime.queryInterface(TableColumnSeparator[].class, xObj);

          // nb cell = nb séparateurs + 1
          int largeurTotale = tableauSeparateurColonne[tableauSeparateurColonne.length - 1].Position + tableauSeparateurColonne[0].Position;

          Double[] layout = new Double[columnCount];
          int pourcentageUtilise = 0;

          for (AttributLargeurColonne attributLargeurColonne : attributs.getLargeurColonne()) {
            int colonne = attributLargeurColonne.getColonne();
            int largeurPourcentage = attributLargeurColonne.getLargeur();
            layout[colonne] = Double.valueOf(largeurPourcentage);
            pourcentageUtilise += largeurPourcentage;
          }

          if (columnCount - attributs.getLargeurColonne().size() != 0) {
            int pourcentageRestant = 100 - pourcentageUtilise;
            double pourcentageRestantParColonneNonAjustee = pourcentageRestant / (columnCount - attributs.getLargeurColonne().size());
            for (int i = 0; i < layout.length; i++) {
              if (layout[i] == null) {
                layout[i] = pourcentageRestantParColonneNonAjustee;
              }
            }
          }

          for (int i = 0; i < layout.length - 1; i++) {
            short offset = (i == 0) ? 0 : tableauSeparateurColonne[i - 1].Position;
            tableauSeparateurColonne[i].Position = (short) (offset + (double) largeurTotale * layout[i] / 100.0);
          }
          xPS.setPropertyValue("TableColumnSeparators", tableauSeparateurColonne);
        }
      }

      creerCellules(xTextTable, mxDocFactory, onglet.getCellule());

      for (Ligne ligne : onglet.getLigne()) {
        int numeroLigneInsertion = ligne.getNumeroLigneInsertion();
        xTextTable.getRows().insertByIndex(numeroLigneInsertion, 1);
        creerCellules(xTextTable, mxDocFactory, ligne.getCellule());
      }

      mergeCells(xTextTable, onglet.getRegionFusionnee());

    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      return false;
    }
    return true;
  }

  public static interface TableInsertionCallback {
    public void insertTable(XTextDocument xDocTextDocument, XTextTable xTextTable) throws Exception;
  }

  /**
   * 
   * Méthode qui crée les cellules
   */
  private void creerCellules(final XTextTable xTT, XMultiServiceFactory mxDocFactory, final List<Cellule> cellules) {

    for (Cellule cellule : cellules) {
      try {

        XCellRange xCellRange = (XCellRange) UnoRuntime.queryInterface(XCellRange.class, xTT);
        XCell cell;

        try {
          cell = xCellRange.getCellByPosition(cellule.getCoordonnee().getColonne(), cellule.getCoordonnee().getLigne());
        } catch (IndexOutOfBoundsException e) {
          LOG.warn(e.getMessage());
          continue;
        }

        XText cellText = (XText) UnoRuntime.queryInterface(XText.class, cell);
        applyStyleToCell(cellule, cell);

        if (cellule.getValeur() != null || cellule.getValeurNumerique() != null || cellule.isValeurBooleenne() != null || cellule.getValeurFormule() != null || cellule.getValeurDate() != null || cellule.getValeurUrlImage() != null) {

          if (cellule.getValeur() != null) {
            cellText.setString(cellule.getValeur());
          } else if (cellule.getValeurNumerique() != null) {

            if (StringUtils.isBlank(cellule.getFormat())) {
              cell.setValue(CelluleHelper.getValeurNumeriqueDouble(cellule));
            } else {
              NumberFormat numberFormat = new DecimalFormat(cellule.getFormat());
              numberFormat.format(CelluleHelper.getValeurNumeriqueDouble(cellule));
            }

          } else if (cellule.isValeurBooleenne() != null) {
            cellText.setString(String.valueOf(cellule.isValeurBooleenne()));

          } else if (cellule.getValeurFormule() != null) {

            cell.setFormula(cellule.getValeurFormule());
          } else if (cellule.getValeurDate() != null) {

            Date date = cellule.getValeurDate();
            DateFormat dateFormat = null;
            if (cellule.getFormat() != null) {
              dateFormat = new SimpleDateFormat(cellule.getFormat());
            } else {
              dateFormat = new SimpleDateFormat();
            }
            String dateAsStr = dateFormat.format(date);
            cellText.setString(dateAsStr);
          } else if (cellule.getValeurUrlImage() != null) {

            ImageUtil.GraphicInfo graphicInfo = null;

            graphicInfo = ImageUtil.getGraphicInfo(cellule.getValeurUrlImage(), workDirectoryPath, true);

            String path = graphicInfo.getImage().getAbsolutePath();
            String unoUrl = getUnooURL(path);

            XTextRange xTextRange = (XTextRange) UnoRuntime.queryInterface(XTextRange.class, cell);

            Object graphicObjet = mxDocFactory.createInstance("com.sun.star.text.TextGraphicObject");

            XTextContent xTextContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, graphicObjet);
            XPropertySet xProperties = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, graphicObjet);
            xProperties.setPropertyValue(OpenOfficeProprietes.AnchorType.name(), TextContentAnchorType.AS_CHARACTER);
            xProperties.setPropertyValue(OpenOfficeProprietes.GraphicURL.name(), unoUrl);

            xProperties.setPropertyValue(OpenOfficeProprietes.Width.name(), graphicInfo.getWidth() * TAILLE_MULTIPLICATEUR_IMAGE);
            xProperties.setPropertyValue(OpenOfficeProprietes.Height.name(), graphicInfo.getHeight() * TAILLE_MULTIPLICATEUR_IMAGE);

            cellText.insertTextContent(xTextRange, xTextContent, false);
          }
        }

        if (cellule.getHauteur() != null) {
          Float hauteur = new Float(cellule.getHauteur());
          Object objRow = xTT.getRows().getByIndex(cellule.getCoordonnee().getLigne());
          XPropertySet rowPropSet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, objRow);
          Integer hauteurActuelle = (Integer) rowPropSet.getPropertyValue(OpenOfficeProprietes.Height.name());
          if (hauteur > hauteurActuelle) {
            rowPropSet.setPropertyValue(OpenOfficeProprietes.Height.name(), new Integer((int) (hauteur * 100)));
            rowPropSet.setPropertyValue(OpenOfficeProprietes.IsAutoHeight.name(), Boolean.FALSE);
          }
        }

      } catch (Exception e) {
        CelluleCoordonnee celluleCoordonnee = CelluleHelper.getCelluleCoordonnee(cellule);
        LOG.error(MessageFormat.format("Problème recontré lors de la création de la cellule {0},{1}", celluleCoordonnee.getLigne(), celluleCoordonnee.getColonne()), e);
      }
    }
  }

  /**
   * Méthode qui applique le style à une cellule
   */
  private void applyStyleToCell(Cellule cellule, XCell xCell) throws Exception {
    try {

      CelluleStyle style = cellule.getStyle();

      CouleurTranslator couleurTranslator = new CouleurTranslatorOdt();

      if (style == null) {
        return;
      }

      XPropertySet cellPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xCell);
      XText cellText = (XText) UnoRuntime.queryInterface(XText.class, xCell);
      XPropertySet cellTextPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, cellText.createTextCursor());

      cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.ParaStyleName.name(), "Table");

      CelluleStyleAlignement celluleStyleAlignement = style.getAlignement();
      CelluleStyleBordure celluleStyleBordure = style.getBordure();
      CelluleStyleFont celluleStyleFont = style.getFont();
      CelluleStyleRemplissage celluleStyleRemplissage = style.getRemplissage();

      if (celluleStyleFont != null) {
        if (celluleStyleFont.getPolice() != null) {
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharFontName.name(), celluleStyleFont.getPolice());
        }

        if (celluleStyleFont.getTaille() != null) {
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharHeight.name(), (float) celluleStyleFont.getTaille().intValue());
        }
        if (celluleStyleFont.getCouleur() != null) {
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharColor.name(), couleurTranslator.transale(celluleStyleFont.getCouleur()));
        }
        if (celluleStyleFont.getSouligne() != null) {
          FontSousligneTranslator fst = new FontSousligneTranslatorOdt();
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharUnderline.name(), fst.transale(celluleStyleFont.getSouligne()));
        }
        if (celluleStyleFont.isGras()) {
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharWeight.name(), (FontWeight.BOLD));
        } else {
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharWeight.name(), (com.sun.star.awt.FontWeight.NORMAL));
        }
        if (celluleStyleFont.isItalic()) {
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharPosture.name(), FontSlant.ITALIC);
        }
      }

      if (celluleStyleBordure != null) {
        // Pas d'équivalent dans XTextTable
      }

      if (celluleStyleAlignement != null) {
        if (celluleStyleAlignement.getRotation() != null) {
          // open office accepte les valeurs suivantes 0 , 90 , 270
          // degres ( mult 10)
          int angle = celluleStyleAlignement.getRotation().shortValue();
          if (angle > 0 && angle <= 90) {
            angle = 90;
          } else if (angle > 90 && angle <= 270) {
            angle = 270;
          } else {
            angle = 0;
          }
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharRotation.name(), (short) (angle * 10));
        }

        if (celluleStyleAlignement.getHorizontal() != null) {
          AlignementHorizontalTranslator aht = new AlignementHorizontalTranslatorOdt();
          cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.ParaAdjust.name(), aht.transale(celluleStyleAlignement.getHorizontal()));
        }

        if (celluleStyleAlignement.getVertical() != null) {
          AlignementVerticalTranslator avt = new AlignementVerticalTranslatorOdt();
          cellPropertySet.setPropertyValue(OpenOfficeProprietes.VertOrient.name(), ((Integer) avt.transale(celluleStyleAlignement.getVertical())).shortValue());
        }
      }

      if (celluleStyleRemplissage != null) {

        // type de remplissage
        if (celluleStyleRemplissage.getType() != null) {
          // Pas d'équivalent dans XTextTable
        }

        // remplissage de premier plan
        if (celluleStyleRemplissage.getPremierPlan() != null) {
          if (celluleStyleRemplissage.getArrierePlan() == null) {
            cellPropertySet.setPropertyValue(OpenOfficeProprietes.BackTransparent.name(), false);
            cellPropertySet.setPropertyValue(OpenOfficeProprietes.BackColor.name(), couleurTranslator.transale(celluleStyleRemplissage.getPremierPlan()));

          } else {
            cellTextPropertySet.setPropertyValue(OpenOfficeProprietes.CharBackColor.name(), couleurTranslator.transale(celluleStyleRemplissage.getPremierPlan()));
          }
        }

        if (celluleStyleRemplissage.getArrierePlan() != null) {
          cellPropertySet.setPropertyValue(OpenOfficeProprietes.BackTransparent.name(), false);
          cellPropertySet.setPropertyValue(OpenOfficeProprietes.BackColor.name(), couleurTranslator.transale(celluleStyleRemplissage.getArrierePlan()));
        }
      }

    } catch (Exception e) {

      CelluleCoordonnee celluleCoordonnee = CelluleHelper.getCelluleCoordonnee(cellule);
      LOG.warn(MessageFormat.format("Problème recontré lors de l'application du style à la cellule {0},{1}", celluleCoordonnee.getLigne(), celluleCoordonnee.getColonne()), e);

      throw e;
    }
  }

  /**
   * Méthode qui fusionne des cellules
   */
  private void mergeCells(final XTextTable xTT, List<RegionFusionnee> regions) {
    for (RegionFusionnee region : regions) {

      String startCell = getColumnCharacter(region.getColonneDebut()) + (region.getLigneDebut() + 1);
      String endCell = getColumnCharacter(region.getColonneFin()) + (region.getLigneFin() + 1);
      XTextTableCursor cur = xTT.createCursorByCellName(startCell);
      cur.gotoCellByName(endCell, true);
      cur.mergeRange();
    }
  }

  /**
   * Méthode qui renvoie le nom d'une colonne à partir de son index
   * 
   * @param columnIndex index de la colonne
   */
  public static String getColumnCharacter(int columnIndex) {
    if (columnIndex < 26) {
      return String.valueOf((char) (columnIndex + 65));
    } else if (columnIndex < 52) {
      return String.valueOf((char) (columnIndex + 71));
    } else {
      char[] chars = new char[2];
      int firstIndex = columnIndex / 52;
      chars[0] = (char) (firstIndex + 64);
      int secondIndex = columnIndex - (firstIndex * 52);
      if (secondIndex < 26)
        chars[1] = (char) (secondIndex + 65);
      else
        chars[1] = (char) (secondIndex + 71);
      return String.copyValueOf(chars);
    }
  }

  /**
   * Permet de mettre à jour la table des matières du document.
   */
  private void updateIndexTables() {
    LOG.debug("Mise à jour du sommaire...");
    XDocumentIndexesSupplier xDocIndexSupp = (XDocumentIndexesSupplier) UnoRuntime.queryInterface(XDocumentIndexesSupplier.class, xDoc);

    XIndexAccess xIndexes = xDocIndexSupp.getDocumentIndexes();
    Object xIndexObjet = null;
    try {
      xIndexObjet = xIndexes.getByIndex(0);
    } catch (Exception e) {
      LOG.warn("Aucun Sommaine à générer...");
    }

    XDocumentIndex xDocIndex = null;
    if (xIndexObjet != null) {
      xDocIndex = (XDocumentIndex) UnoRuntime.queryInterface(XDocumentIndex.class, xIndexObjet);
      xDocIndex.update();
    }
  }

  /**
   * Méthode permettant générer un document à partir des information contenues dans l'attribut
   * documentDTO {@link DocumentDTO}
   * @param desactiverTransformations Utile pour quand on veut utiliser le service openoffice juste
   *        pour transformer les fichiers (ex. XLS -> PDF)
   */
  public void generateDocumentFromTemplate(Boolean desactiverTransformations) throws java.lang.Exception {

    String templatePath = template.getFichier().getAbsolutePath();

    OpenOfficeValeurs openOfficeValeurs = null;

    if (desactiverTransformations == null || !desactiverTransformations) {
      openOfficeValeurs = TransformationService.getValeursDesChampsFusion(documentDTO.getChampFusions(), freeMarkerMap);
    } else {
      openOfficeValeurs = OpenOfficeValeurs.makeEmpty();
    }

    // création d'un nouveau document basé sur le template
    String urlTemplate = getUnooURL(templatePath);
    xTemplateComponent = createComponentFromTemplate(documentDTO.getFileName(), urlTemplate);

    // instanciation des XTextDocument et XMultiServiceFactory
    xDoc = (XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, xTemplateComponent);
    mxDocFactory = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, xDoc);

    // insertion des champs de fusion qui se trouve dans le document
    // template et qui ne sont pas des images
    replaceFieldMastersByValues(xDoc, mxDocFactory, openOfficeValeurs);

    // insertion d'images
    // replaceTextsByImages(xDoc, mxDocFactory,
    // openOfficeValeurs.getValeurs(), openOfficeValeurs.getImages());

    // mise à jour du sommaire
    updateIndexTables();
  }

  /**
   * Méthode permettant de retourner un FileInputStream à partir du document qui sert de fichier
   * temporaire
   */
  public InputStream getDocumentToInputSteam() throws FileNotFoundException {

    String filePath;

    if (lotDirectoryName != null) {
      filePath = workDirectoryPath + lotDirectoryName + File.separator + documentDTO.getFileName() + documentDTO.getDocumentModeleDTO().getTypeFormat().getExtension();
    } else {
      filePath = workDirectoryPath + documentDTO.getFileName() + documentDTO.getDocumentModeleDTO().getTypeFormat().getExtension();
    }

    storeDocComponent(filePath);

    LOG.info("Création du fichier temporaire qui aura le flux de lecture vers le fichier généré : " + filePath);

    tempFile = new File(filePath);

    if (tempFile.exists()) {

      return new FileInputStream(tempFile);
    } else {
      return null;
    }
  }

  /**
   * Méthode permettant de sauvegarder sur disque le document préalablement construit
   */
  private void storeDocComponent(String filePath) {

    if (this.xDoc != null) {

      try {
        XStorable xStorable = (XStorable) UnoRuntime.queryInterface(XStorable.class, this.xDoc);
        PropertyValue[] storeProps = new PropertyValue[1];
        storeProps[0] = new PropertyValue();
        storeProps[0].Name = OpenOfficeProprietes.FilterName.name();
        storeProps[0].Value = documentDTO.getDocumentModeleDTO().getTypeFormat().getFormatCode();

        String pathToFile = getUnooURL(filePath);

        xStorable.storeToURL(pathToFile, storeProps);

        closeOpenedDocument(xStorable);

      } catch (CloseVetoException e) {
        LOG.error("Erreur lors de la tentative de cloture du fichier temporaire " + e);
        LOG.error(e.getMessage(), e);
      } catch (Exception e) {
        LOG.error("Erreur lors de la génération du document  par l'intermédiaire du service d'open office " + e);
        LOG.error(e.getMessage(), e);

      }
    }
  }

  private void closeOpenedDocument(XStorable xStorable) throws CloseVetoException {

    // cloture du document converti. Utilisation de XCloseable.close si
    // l'interface est supporté sinon on utilise XComponent.dispose
    com.sun.star.util.XCloseable xCloseable = (com.sun.star.util.XCloseable) UnoRuntime.queryInterface(com.sun.star.util.XCloseable.class, xStorable);

    if (xCloseable != null) {
      xCloseable.close(false);
    } else {
      com.sun.star.lang.XComponent xComp = (com.sun.star.lang.XComponent) UnoRuntime.queryInterface(com.sun.star.lang.XComponent.class, xStorable);
      xComp.dispose();
    }
  }

  /**
   * Méthode permettant de supprimer le fichier temporaire
   */
  public void deleteTempFile() {

    LOG.info("Supression du fichier temporaire");

    if (this.tempFile != null && this.tempFile.exists()) {
      this.tempFile.delete();
    }
  }

  /**
   * Méthode permettant de récuperer le fichier temporaire qui a été créé
   */
  public File getTempFile() {
    return tempFile;
  }

  /**
   * Méthode permettant de setter le répertoire où sera enregistré le document généré
   */
  public void setLotDirectoryName(String lotDirectoryName) {
    this.lotDirectoryName = lotDirectoryName;
  }

  /**
   * Méthode permettant de setter la map freeMarker à utiliser avec des entrées par défaut
   */
  public void setFreeMarkerMap(Map<String, Object> freeMarkerMap) {
    this.freeMarkerMap = freeMarkerMap;
  }
}
