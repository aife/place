package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.ss.usermodel.Font;

import fr.atexo.commun.document.generation.service.tableaudef.FontSousligneTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationFontSousligne;

public class FontSousligneTranslatorXls implements FontSousligneTranslator {

	@Override
	public Object transale(EnumerationFontSousligne fontSousligne) {

		byte translation = 0;

		switch (fontSousligne) {
		case U_NONE:
			translation = Font.U_NONE;
			break;

		case U_SINGLE:
			translation = Font.U_SINGLE;
			break;

		case U_DOUBLE:
			translation = Font.U_DOUBLE;
			break;

		case U_SINGLE_ACCOUNTING:
			translation = Font.U_SINGLE_ACCOUNTING;
			break;

		case U_DOUBLE_ACCOUNTING:
			translation = Font.U_DOUBLE_ACCOUNTING;
			break;

		default:
			break;
		}

		return new Integer(translation);

	}
}
