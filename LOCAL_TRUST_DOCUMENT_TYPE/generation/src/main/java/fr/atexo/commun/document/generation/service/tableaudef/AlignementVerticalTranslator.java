package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationAlignementVertical;

public interface AlignementVerticalTranslator {

	public Object transale(EnumerationAlignementVertical alignementVertical);

}
