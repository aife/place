/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.atexo.commun.document.administration.commun.TypeChamp;

/**
 * Class contenant les differents types de maps avec les valeur à fussioner
 * passée au Service OpenOffice
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
public class OpenOfficeValeurs {

	private Map<String, Valeur> valeurs;
	private Set<String> images;
	private Set<String> tableaux;

	public static class Valeur {

		private Object val;
		private TypeChamp type;

		public Valeur(TypeChamp type, Object val) {
			super();
			this.type = type;
			this.val = val;
		}

		public Object getVal() {
			return val;
		}

		public void setVal(Object val) {
			this.val = val;
		}

		public TypeChamp getType() {
			return type;
		}

		public void setType(TypeChamp type) {
			this.type = type;
		}

	}

	public static OpenOfficeValeurs makeEmpty() {
		OpenOfficeValeurs res = new OpenOfficeValeurs();
		res.setImages(new HashSet<String>());
		res.setTableaux(new HashSet<String>());
		res.setValeurs(new HashMap<String, OpenOfficeValeurs.Valeur>());
		return res;
	}

	public Map<String, Valeur> getValeurs() {
		return valeurs;
	}

	public Set<String> getImages() {
		return images;
	}

	public Set<String> getTableaux() {
		return tableaux;
	}

	public void setTableaux(Set<String> tableaux) {
		this.tableaux = tableaux;
	}

	public void setValeurs(Map<String, Valeur> valeurs) {
		this.valeurs = valeurs;
	}

	public void setImages(Set<String> images) {
		this.images = images;
	}

	public boolean isObject(String code) {
		return !isImage(code) && !isTableau(code);
	}

	public boolean isImage(String code) {
		return images != null && images.contains(code);
	}

	public boolean isTableau(String code) {
		return tableaux != null && tableaux.contains(code);
	}
}
