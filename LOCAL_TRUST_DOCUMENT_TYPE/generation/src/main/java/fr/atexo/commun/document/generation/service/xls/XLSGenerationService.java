package fr.atexo.commun.document.generation.service.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.artofsolving.jodconverter.document.DefaultDocumentFormatRegistry;
import org.artofsolving.jodconverter.document.DocumentFormat;

import com.googlecode.streamflyer.core.Modifier;
import com.googlecode.streamflyer.core.ModifyingReader;
import com.googlecode.streamflyer.regex.RegexModifier;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.administration.commun.TypeFormat;
import fr.atexo.commun.document.generation.service.AtexoDocumentConversionPdfException;
import fr.atexo.commun.document.generation.service.AtexoDocumentGenerationExcelException;
import fr.atexo.commun.document.generation.service.DocumentGenerationService;
import fr.atexo.commun.document.generation.service.ExceptionLogMessages;
import fr.atexo.commun.document.generation.service.TransformationService;
import fr.atexo.commun.document.generation.service.tableaudef.CelluleTypeTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Cellule;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleCoordonnee;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleHelper;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleStyle;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCelluleType;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationTypeImage;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Ligne;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Onglet;
import fr.atexo.commun.document.generation.service.tableaudef.bean.RegionFusionnee;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Tableau;
import fr.atexo.commun.document.generation.service.xls.translator.CelluleTypeTranslatorXls;
import fr.atexo.commun.document.generation.service.xls.translator.TypeImageTranslatorXls;
import fr.atexo.commun.document.generation.util.ImageUtil;
import fr.atexo.commun.document.openoffice.OpenOfficeDocumentConverteur;
import fr.atexo.commun.excel.ExcelManager;
import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeException;
import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import fr.atexo.commun.util.GeneriqueUtil;

/**
 * Le point d'entrée à l'ensemble du service XLSGenerateurService du module generation des Documents
 * @author JOR
 */
public class XLSGenerationService implements DocumentGenerationService {

    // déclaration du log
    private static final Log LOG = LogFactory.getLog(XLSGenerationService.class);

    private static final String NOM_FICHIER_TEMP = "fichierTemp_";

    private File fichierTemp;

    private File fichierSortiePdfTemp;

    private OpenOfficeDocumentConverteur openOfficeDocumentConverteur;

    private Integer versionMoteur = 1;

    public XLSGenerationService() {

    }

    public XLSGenerationService(OpenOfficeDocumentConverteur openOfficeDocumentConverteur) {
        this.openOfficeDocumentConverteur = openOfficeDocumentConverteur;
    }

    /**
     * Méthode d'accès au service génération d'un fichier XLS
     * @param documentDTO Définition du document à générer. Il contient notamment la définition XML du
     *        fichier à générer et les champsFusions
     * @param template Template XLS à modifier
     * @param freeMarkerMap Map freemarker d'initialisation (optionelle)
     * @return InputStream vers le fichier modifié
     */
    public InputStream genererDocument(DocumentDTO documentDTO, FichierDTO template, Map<String, Object> freeMarkerMap) throws ServiceOpenOfficeException, AtexoDocumentConversionPdfException, AtexoDocumentGenerationExcelException, FileNotFoundException {
        return genererDocument(documentDTO, template, freeMarkerMap, null, null);
    }

    public InputStream genererDocument(DocumentDTO documentDTO, FichierDTO template, Map<String, Object> freeMarkerMap, Map<String, List<LinkedHashMap<String, Object>>> donneesTableaux) throws ServiceOpenOfficeException, AtexoDocumentConversionPdfException, AtexoDocumentGenerationExcelException,
            FileNotFoundException {
        return genererDocument(documentDTO, template, freeMarkerMap, donneesTableaux, null);
    }

    /**
     * Méthode d'accès au service génération d'un fichier XLS
     *
     * @param documentDTO Définition du document à générer. Il contient notamment la définition XML du
     *        fichier à générer et les champsFusions
     * @param template Template XLS à modifier
     * @param freeMarkerMap map freemarker d'initialisation (optionelle)
     * @param donneesTableaux map des données qui doivent être intégrés sous la forme d'un tableau
     * @param nomRepertoireTemporaire Nom du répertoire temporaire utilisé
     * @return InputStream vers le fichier modifié
     */
    public InputStream genererDocument(DocumentDTO documentDTO, FichierDTO template, Map<String, Object> freeMarkerMap, Map<String, List<LinkedHashMap<String, Object>>> donneesTableaux, String nomRepertoireTemporaire) throws ServiceOpenOfficeException, AtexoDocumentConversionPdfException,
            AtexoDocumentGenerationExcelException, FileNotFoundException {

        // si Requête libre, alors créer une template minimale
        InputStream templateInput = null;
        if (template == null || template.getFichier() == null || !template.getFichier().exists()) {
            if (documentDTO.getDocumentModeleDTO().getTypeDocument() == TypeDocumentModele.RequeteLibre) {
                templateInput = creerTemplateLibre(documentDTO);
            } else {
                throw new AtexoDocumentGenerationExcelException(ExceptionLogMessages.templateNonTrouvee);
            }
        } else {
            templateInput = new FileInputStream(template.getFichier());
        }

        if (GeneriqueUtil.isEmpty(documentDTO.getDocumentModeleDTO().getXml())) {
            // Aucune transformation à faire sur la template
            return new FileInputStream(template.getFichier());
        }

        String nomFichier = GeneriqueUtil.isEmpty(documentDTO.getFileName()) ? NOM_FICHIER_TEMP : documentDTO.getFileName();
        // Copie du template XLS vers un fichier temporaire
        if (nomRepertoireTemporaire == null || nomRepertoireTemporaire.trim().length() == 0) {
            fichierTemp = FichierTemporaireUtil.creerFichierTemporaire(nomFichier, ".xls");
        } else {
            fichierTemp = FichierTemporaireUtil.creerFichierTemporaire(nomFichier, ".xls", FichierTemporaireUtil.getRepertoireTemporaire(nomRepertoireTemporaire, false));
        }

        // On copie la template vers un fichier temporaire
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fichierTemp);
            IOUtils.copy(templateInput, fos);
        } catch (IOException e) {
            throw new AtexoDocumentGenerationExcelException(ExceptionUtils.getFullStackTrace(e));
        } finally {
            IOUtils.closeQuietly(templateInput);
            IOUtils.closeQuietly(fos);
        }

        LOG.info("Copie du template XLS vers un fichier temporaire : " + fichierTemp.getAbsolutePath());

        FileWriter fileWriter = null;
        File xmlFile = FichierTemporaireUtil.creerFichierTemporaire("freemarker_output", ".xml");

        try {
            fileWriter = new FileWriter(xmlFile);
            // Ajout des champs fusion du documentDTO à Map freemarker
            TransformationService.ajouterChampFusionAFreeMarkerMap(documentDTO.getChampFusions(), freeMarkerMap);
            // transformation FreeMarker ( sortie dans un fichier )
            FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, documentDTO.getDocumentModeleDTO().getXml(), fileWriter);
            fileWriter.flush();
        } catch (Exception e) {
            throw new AtexoDocumentGenerationExcelException(ExceptionUtils.getFullStackTrace(e));
        } finally {
            IOUtils.closeQuietly(fileWriter);
        }

        TypeFormat modeleTypeFormat = documentDTO.getDocumentModeleDTO().getTypeFormat();
        String modeleFileName = documentDTO.getFileName();
        documentDTO = null;

        System.runFinalization();
        System.gc();

        FileReader xmlFileReader = null;
        XMLStreamReader xmlStreamReader = null;
        FileInputStream fichierExcelInputStream = null;

        ExcelManager excelManager = new ExcelManager();

        // Modification du fichier Excel
        try {
            xmlFileReader = new FileReader(xmlFile);

            Modifier modifier1 = new RegexModifier("<valeur>", 0, "<valeur><![CDATA[");
            Modifier modifier2 = new RegexModifier("</valeur>", 0, "]]></valeur>");

            Reader modifyingReader1 = new ModifyingReader(xmlFileReader, modifier1);
            Reader modifyingReader2 = new ModifyingReader(modifyingReader1, modifier2);

            xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(modifyingReader2);

            fichierExcelInputStream = new FileInputStream(fichierTemp);
            excelManager.read(fichierExcelInputStream);

            Map<CellStyleClef, CellStyle> styles = new HashMap<CellStyleClef, CellStyle>();

            // On fait un traitement unitaire par onglet pour réduire la taille de la mémoire utilisée
            while (xmlStreamReader.hasNext()) {
                if (xmlStreamReader.isStartElement()) {
                    String localName = xmlStreamReader.getLocalName();
                    Onglet onglet = null;

                    if (localName.equals("documentXLS")) {
                        String versionMoteurAsString = xmlStreamReader.getAttributeValue(null, "versionMoteur");
                        if (versionMoteurAsString != null) {
                            this.versionMoteur = Integer.parseInt(versionMoteurAsString);
                        }
                        xmlStreamReader.next();
                    } else if (xmlStreamReader.getLocalName().equals("onglet")) {
                        JAXBContext jc = JAXBContext.newInstance(Onglet.class);
                        Unmarshaller unmarshaller = jc.createUnmarshaller();
                        JAXBElement<Onglet> jb = unmarshaller.unmarshal(xmlStreamReader, Onglet.class);
                        onglet = jb.getValue();
                        modifierXLS(onglet, donneesTableaux, styles, excelManager);
                    } else {
                        xmlStreamReader.next();
                    }
                } else {
                    xmlStreamReader.next();
                }
            }

        } catch (Exception e) {
            throw new AtexoDocumentGenerationExcelException(ExceptionUtils.getFullStackTrace(e));
        } finally {
            IOUtils.closeQuietly(xmlFileReader);
            try {
                xmlStreamReader.close();
            } catch (XMLStreamException e) {
                throw new AtexoDocumentGenerationExcelException(ExceptionUtils.getFullStackTrace(e));
            }
            IOUtils.closeQuietly(fichierExcelInputStream);
            FileUtils.deleteQuietly(xmlFile);
        }

        // on copie le Workbbok dans le fichier d'orgine
        FileOutputStream fichierExcelOutputStream = null;
        try {
            fichierExcelOutputStream = new FileOutputStream(fichierTemp);
            IOUtils.copy(excelManager.getInputStream(), fichierExcelOutputStream);
        } catch (Exception e) {
            throw new AtexoDocumentGenerationExcelException(ExceptionUtils.getFullStackTrace(e));
        } finally {
            IOUtils.closeQuietly(fichierExcelOutputStream);
        }

        // conversion PDF
        try {
            if (TypeFormat.PDF == modeleTypeFormat) {
                String nomFichierTemp = GeneriqueUtil.isEmpty(modeleFileName) ? "sortiePdf" : modeleFileName;
                // Alors transformation XLS -> PDF
                fichierSortiePdfTemp = FichierTemporaireUtil.creerFichierTemporaire(nomFichierTemp, ".pdf");

                DefaultDocumentFormatRegistry defaultDocumentFormatRegistry = new DefaultDocumentFormatRegistry();
                DocumentFormat formatPdf = defaultDocumentFormatRegistry.getFormatByExtension("pdf");
                openOfficeDocumentConverteur.convertirDocument(fichierTemp, fichierSortiePdfTemp, formatPdf);
                return new FileInputStream(fichierSortiePdfTemp);
            }
        } catch (ServiceOpenOfficeException e) {
            throw e;
        } catch (Exception e) {
            throw new AtexoDocumentConversionPdfException(e.getMessage());
        }

        return new FileInputStream(fichierTemp);
    }

    private void modifierXLS(Onglet onglet, Map<String, List<LinkedHashMap<String, Object>>> mapDonneesTableaux, Map<CellStyleClef, CellStyle> styles, ExcelManager excelManager) throws Exception {

        HSSFWorkbook workbook = excelManager.getWorkbook();
        HSSFSheet sheet = null;
        if (onglet.getNom() != null) {
            sheet = workbook.getSheet(onglet.getNom());
        }
        if (onglet.getNumeroOnglet() != null) {
            sheet = workbook.getSheetAt(onglet.getNumeroOnglet());
        }
        if (sheet == null) {
            sheet = excelManager.getWorkbook().createSheet(onglet.getNom());
        }
        creerCellules(workbook, sheet, onglet.getCellule(), styles);

        for (Ligne ligne : onglet.getLigne()) {
            HSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
            sheet.shiftRows(ligne.getNumeroLigneInsertion(), sheet.getLastRowNum(), 1, false, false);
            creerCellules(workbook, sheet, ligne.getCellule(), styles);
            calculerRegionsFusionnees(sheet, ligne.getRegionFusionnee());
        }

        Tableau tableau = onglet.getTableau();
        if (tableau != null && tableau.getReference() != null && mapDonneesTableaux != null) {
            LOG.info("Recherche de la liste de données devant être associé au tableau ayant comme référence " + tableau.getReference() + " présent dans l'onglet numéro " + onglet.getNumeroOnglet());
            List<LinkedHashMap<String, Object>> donneesTableau = mapDonneesTableaux.get(tableau.getReference());

            if (donneesTableau != null) {
                // traitement des entêtes du tableau
                LinkedHashMap<String, Object> donneesLigneEntete = donneesTableau.get(0);
                sheet.createRow(sheet.getLastRowNum() + 1);
                sheet.shiftRows(tableau.getNumeroLigneInsertion(), sheet.getLastRowNum(), 1, false, false);
                traiterLigneTableau(workbook, sheet, donneesLigneEntete, styles, tableau.getNumeroLigneInsertion(), tableau.getNumeroColonneInsertion(), tableau.getStyleEntete(), true);

                // traitement des données du tableau
                int numeroLigneTableau = tableau.getNumeroLigneInsertion() + 1;
                for (LinkedHashMap<String, Object> donneesLigne : donneesTableau) {
                    sheet.createRow(sheet.getLastRowNum() + 1);
                    sheet.shiftRows(numeroLigneTableau, sheet.getLastRowNum(), 1, false, false);
                    traiterLigneTableau(workbook, sheet, donneesLigne, styles, numeroLigneTableau, tableau.getNumeroColonneInsertion(), tableau.getStyleCorp(), false);
                    numeroLigneTableau++;
                }

            } else {
                LOG.warn("Aucunes données n'a été trouvé pour le tableau ayant comme référence " + tableau.getReference());
            }
        }
        calculerRegionsFusionnees(sheet, onglet.getRegionFusionnee());
    }

    private void calculerRegionsFusionnees(HSSFSheet onglet, List<RegionFusionnee> regions) {
        StringBuffer valeurBuf = new StringBuffer();
        for (RegionFusionnee region : regions) {

            // concatenation des valeurs des cellules de la region fusionnee
            valeurBuf.setLength(0);
            for (int i = region.getLigneDebut(); i <= region.getLigneFin(); i++) {

                HSSFRow rowXLS = onglet.getRow(i);

                if (rowXLS != null) {
                    for (int j = region.getColonneDebut(); j <= region.getColonneFin(); j++) {
                        HSSFCell cellXLS = rowXLS.getCell((short) j);
                        if (cellXLS != null) {
                            valeurBuf.append(cellXLS.getRichStringCellValue().toString());
                        }
                    }
                }
            }

            // fusion
            onglet.addMergedRegion(new CellRangeAddress(region.getLigneDebut(), region.getLigneFin(), region.getColonneDebut(), region.getColonneFin()));

            // ajout du texte
            HSSFRow rowXLS = onglet.getRow(region.getLigneDebut());
            if (rowXLS != null) {
                HSSFCell cellXLS = rowXLS.getCell((short) region.getColonneDebut());
                if (cellXLS != null) {
                    cellXLS.setCellValue(new HSSFRichTextString(valeurBuf.toString()));
                }
            }
        }
    }

    private void traiterLigneTableau(HSSFWorkbook workbook, HSSFSheet sheet, LinkedHashMap<String, Object> donneesLigne, Map<CellStyleClef, CellStyle> styles, int numeroLigne, int numeroColonne, CelluleStyle celluleStyle, boolean entete) {

        for (String titreEntete : donneesLigne.keySet()) {
            Object valeur = donneesLigne.get(titreEntete);
            if (entete) {
                creerCellulle(workbook, sheet, styles, numeroLigne, numeroColonne, celluleStyle, titreEntete);
            } else {
                creerCellulle(workbook, sheet, styles, numeroLigne, numeroColonne, celluleStyle, valeur);
            }

            numeroColonne++;
        }

    }

    private HSSFCell creerCellulle(HSSFWorkbook workbook, HSSFSheet sheet, Map<CellStyleClef, CellStyle> styles, int numeroLigne, int numeroColonne, CelluleStyle celluleStyle, Object valeur) {
        CellStyleManager cellStyleManager = CellStyleManager.getInstance();
        HSSFRow row = sheet.getRow(numeroLigne);
        if (row == null) {
            row = sheet.createRow(numeroLigne);
        }
        HSSFCell cell = row.getCell(numeroColonne);
        if (cell == null) {
            cell = row.createCell(numeroColonne);
        }

        CellStyle cellStyle = cellStyleManager.creerOuRecupererStyle(styles, celluleStyle, workbook, row, numeroColonne);
        if (cellStyle != null) {
            cell.setCellStyle(cellStyle);
        }

        CelluleTypeTranslator ctt = new CelluleTypeTranslatorXls();

        if (valeur instanceof String) {
            cell.setCellType(CellType.STRING);
            cell.setCellValue(new HSSFRichTextString((String) valeur));
        } else if (valeur instanceof Date) {
            cell.setCellType(CellType.STRING);
            cell.setCellValue(GeneriqueUtil.formatSimpleDate((Date) valeur));
        } else if (valeur instanceof Integer) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue((Integer) valeur);
        } else if (valeur instanceof Double) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue((Double) valeur);
        } else if (valeur instanceof Long) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue((Long) valeur);
        } else if (valeur instanceof Float) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue((Float) valeur);
        } else if (valeur instanceof Boolean) {
            cell.setCellType(CellType.BOOLEAN);
            cell.setCellValue((Boolean) valeur);
        }

        return cell;
    }

    private void creerCellules(final HSSFWorkbook workbook, final HSSFSheet sheet, final List<Cellule> cellules, Map<CellStyleClef, CellStyle> styles) {

        CellStyleManager cellStyleManager = CellStyleManager.getInstance();

        for (Cellule cellule : cellules) {

            CelluleCoordonnee coordonnee = CelluleHelper.getCelluleCoordonnee(cellule);

            HSSFRow row = sheet.getRow(coordonnee.getLigne());
            sheet.createDrawingPatriarch();
            if (row == null) {
                row = sheet.createRow(coordonnee.getLigne());
            }
            HSSFCell cell = row.getCell(coordonnee.getColonne());
            if (cell == null) {
                cell = row.createCell(coordonnee.getColonne());
            }

            CellStyle cellStyle = cellStyleManager.creerOuRecupererStyle(styles, cellule, workbook, row);
            if (cellStyle != null) {
                cell.setCellStyle(cellStyle);
            }

            CelluleTypeTranslator ctt = new CelluleTypeTranslatorXls();

            if (cellule.getValeur() != null || cellule.getValeurNumerique() != null || cellule.isValeurBooleenne() != null || cellule.getValeurFormule() != null || cellule.getValeurDate() != null || cellule.getValeurUrlImage() != null) {

                if (cellule.getValeur() != null) {
                    cell.setCellType(CellType.STRING);
                    cell.setCellValue(new HSSFRichTextString(cellule.getValeur()));
                } else if (cellule.getValeurNumerique() != null) {
                    cell.setCellType(CellType.NUMERIC);

                    cell.setCellValue(CelluleHelper.getValeurNumeriqueDouble(cellule));
                } else if (cellule.isValeurBooleenne() != null) {
                    cell.setCellType(CellType.BOOLEAN);
                    cell.setCellValue(cellule.isValeurBooleenne());
                } else if (cellule.getValeurFormule() != null) {
                    cell.setCellType(CellType.FORMULA);
                    cell.setCellValue(cellule.getValeurFormule());
                } else if (cellule.getValeurDate() != null) {
                    cell.setCellType(CellType.STRING);
                    cell.setCellValue(cellule.getValeurDate());
                } else if (cellule.getValeurUrlImage() != null) {
                    insererImage(sheet, cellule.getValeurUrlImage(), CelluleHelper.getCelluleCoordonnee(cellule).getLigne(), CelluleHelper.getCelluleCoordonnee(cellule).getColonne());
                }
            }

            // si le type est précisé alors on le surcharge
            if (cellule.getType() != null && (cellule.getValeurFormule() == null)) {
                cell.setCellType(CellType.forInt((int) ctt.transale(cellule.getType())));
            }

            if (cell.getCellStyle() == null) {
                cell.setCellType(CellType.BLANK);
            }

            if (cellule.getHauteur() != null) {
                Float hauteur = new Float(cellule.getHauteur());
                Float hauteurActuelle = row.getHeightInPoints();
                if (hauteur > hauteurActuelle) {
                    row.setHeightInPoints(hauteur);
                }
            }

        }
    }

    private void insererImage(Sheet sheet, String url, int ligne, int colonne) {

        // téléchargement temporaire du contenu de l'image sur le disque
        File repertoireTemp = FichierTemporaireUtil.getRepertoireTemporaire();
        ImageUtil.GraphicInfo graphicInfo = null;
        InputStream fileInputStream = null;

        try {
            graphicInfo = ImageUtil.getGraphicInfo(url, repertoireTemp, false);

            if (graphicInfo != null && graphicInfo.getImage() != null && graphicInfo.getImage().exists()) {

                TypeImageTranslatorXls tit = new TypeImageTranslatorXls();

                EnumerationTypeImage enumerationTypeImage = tit.fromExtension(graphicInfo.getExtension());

                if (enumerationTypeImage != null) {

                    // ajoute l'image au contenu du workbook
                    fileInputStream = new FileInputStream(graphicInfo.getImage());
                    byte[] bytes = IOUtils.toByteArray(fileInputStream);

                    int pictureIdx = sheet.getWorkbook().addPicture(bytes, (Integer) tit.transale(enumerationTypeImage));

                    CreationHelper creationHelper = sheet.getWorkbook().getCreationHelper();

                    // crée le drawing patriarch. C'est le conteneur parent de
                    // toutes les nuances.
                    Drawing drawing = sheet.createDrawingPatriarch();

                    // ajoute une image
                    ClientAnchor anchor = creationHelper.createClientAnchor();
                    anchor.setCol1(colonne);
                    anchor.setRow1(ligne);
                    Picture picture = drawing.createPicture(anchor, pictureIdx);

                    // permet de faire un auto-size par rapport à son angle
                    // haut-gauche
                    picture.resize();

                } else {
                    LOG.error("Type d'image non supporté : " + graphicInfo.getExtension());
                }

            } else {
                LOG.error("Incapacité de récupérer l'image à l'url : " + url);
            }

        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(fileInputStream);
            if (graphicInfo != null) {
                FileUtils.deleteQuietly(graphicInfo.getImage());
            }
        }
    }

    /**
     * Méthode permettant de supprimer le fichier temporaire
     */
    @Override
    public void supprimerFichierTemporaire() {

        LOG.info("Supression du fichier temporaire");

        if (this.fichierTemp != null && this.fichierTemp.exists()) {
            this.fichierTemp.delete();
        }

        if (this.fichierSortiePdfTemp != null && this.fichierSortiePdfTemp.exists()) {
            this.fichierSortiePdfTemp.delete();
        }
    }

    @Override
    public File getFichierTemporaire() {
        return fichierTemp;
    }


    public File getFichierSortiePdfTemp() {
        return fichierSortiePdfTemp;
    }

    /**
     * Generation d'un template minimale dans le cas des requetes libre avec template null
     */
    private InputStream creerTemplateLibre(DocumentDTO documentDTO) {
        LOG.info("Génération d'un fichier Template");
        ExcelManager excelManager = new ExcelManager();

        List<ChampFusionDTO> listChampFusion = documentDTO.getChampFusions();
        if (!GeneriqueUtil.isEmpty(listChampFusion)) {

            String[] titles = new String[listChampFusion.size()];
            for (int i = 0; i < listChampFusion.size(); i++) {
                titles[i] = listChampFusion.get(i).getLibelle();
            }
            excelManager.createWorkbook("Requête Libre", titles);

            return excelManager.getInputStream();
        }
        return null;
    }

}
