/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.excel.ExcelManager;
import fr.atexo.commun.util.GeneriqueUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.util.List;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class AtexoDocumentService {

    // déclaration du log
    private static final Log LOG = LogFactory.getLog(AtexoDocumentService.class);


    /**
     * Méthode permetant d'exporter une liste de champs fusion sous forme de fichier Excel
     * @param champsFusion le champsFusion à exporter
     * @return InputStream le flux vers le fichier Excel généré
    * */
    public static InputStream generateFichierExcel(List<ChampFusionDTO> champsFusion) {
        LOG.info("Génération d'un fichier Excel recapitulatif des champFusion");

        ExcelManager excelManager = new ExcelManager();

        if (!GeneriqueUtil.isEmpty(champsFusion)) {

            String[] titles = new String[champsFusion.size()];
            for (int i = 0; i < champsFusion.size(); i++) {
                titles[i] = champsFusion.get(i).getNom();
            }
            excelManager.createWorkbook("Champs de fusion", titles);

            return excelManager.getInputStream();
        }

        LOG.warn("Le fichier Excel n'a pas pu être créé. La liste de champs fusion passée comme paramètre est vide");

        return null;
    }

}
