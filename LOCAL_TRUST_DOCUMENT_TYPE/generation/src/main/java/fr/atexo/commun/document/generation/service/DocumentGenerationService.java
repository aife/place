/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service;

import java.io.File;

/**
 * Commentaire
 * 
 * @version $Revision$ $Date$
 */
public interface DocumentGenerationService {

  File getFichierTemporaire();

  void supprimerFichierTemporaire();

}
