/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;
import fr.atexo.commun.document.generation.service.openoffice.OpenOfficeValeurs;
import fr.atexo.commun.document.generation.service.tableaudef.bean.DocumentXLS;
import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.commun.util.GeneriqueUtil;

import javax.xml.bind.JAXBElement;
import java.io.File;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

/**
 * Class recapitulant les différentes foncitonnalités de transformation
 * utilisées par le module generation
 *
 * @author JOR
 * @version $Revision$ $Date$
 */
public class TransformationService {

    /**
     * Méthode permetant d'obtenir les valeur des champsFusions. Il est utilisé
     * par la class
     * {@link fr.atexo.commun.document.generation.service.openoffice.OpenOfficeGenerateur}
     *
     * @param champsFusion  Les champsFusion à parcourir pour recuperer ses valeurs
     * @param freeMarkerMap La map par défaut à passer au service FreeMarker
     * @throws Exception
     */
    public static OpenOfficeValeurs getValeursDesChampsFusion(List<ChampFusionDTO> champsFusion, Map<String, Object> freeMarkerMap) throws Exception {

        freeMarkerMap = ajouterChampFusionAFreeMarkerMap(champsFusion, freeMarkerMap);

        Map<String, OpenOfficeValeurs.Valeur> valeursMap = new HashMap<String, OpenOfficeValeurs.Valeur>();
        Set<String> imagesSet = new HashSet<String>();
        Set<String> tableauxSet = new HashSet<String>();

        // on rajoute le cas où l'on doit générer la date de génération du
        // document
        valeursMap.put("date_jour", new OpenOfficeValeurs.Valeur(TypeChamp.OBJECT, GeneriqueUtil.formatSimpleDate(new Date())));
        valeursMap.put("date_jour_heure", new OpenOfficeValeurs.Valeur(TypeChamp.OBJECT,
                GeneriqueUtil.formatSimpleDate(new Date()) + " " + GeneriqueUtil.formatSimpleTime(new Time(new Date().getTime()))));

        for (ChampFusionDTO champFusion : champsFusion) {
            Object valeur = appliquerFormat(champFusion, freeMarkerMap);

            if (champFusion.getTypeChamp() == TypeChamp.URL_IMG) {
                imagesSet.add(champFusion.getNom());
            } else if (champFusion.getTypeChamp() == TypeChamp.SCRIPT_TABLEAU) {
                tableauxSet.add(champFusion.getNom());
            }
            valeursMap.put(champFusion.getNom(), new OpenOfficeValeurs.Valeur(champFusion.getTypeChamp(), valeur));
        }

        OpenOfficeValeurs openOfficeValeurs = new OpenOfficeValeurs();
        openOfficeValeurs.setValeurs(valeursMap);
        openOfficeValeurs.setImages(imagesSet);
        openOfficeValeurs.setTableaux(tableauxSet);

        return openOfficeValeurs;
    }

    /**
     * Retourne le fichier resultant de l'application de Freemarker sur le
     * modele nomFichierTemplate.
     *
     * @param parametres map d'association clef Freemarker -> valeur
     * @throws Exception
     */
    public static File executeTransformationFreemarker(final String cheminVersRepertoireTemplate, final String nomFichierTemplate, final Locale locale,
                                                       final Map<String, Object> parametres) throws Exception {
        return FreeMarkerUtil.executeTransformation(cheminVersRepertoireTemplate, nomFichierTemplate, locale, parametres);
    }

    /**
     * Méthode permetant d'ajouter une liste de champFusion à la map utilisée
     * par freeMarker. Il est utilisé par la méthode getValeursDesChampsFusion
     * et aussi pour la class
     * {@link fr.atexo.commun.document.generation.service.freemarker.FreeMarkerService}
     *
     * @param champsFusions Les champsFusions à parcourir pour recuperer ses valeurs
     * @param freeMarkerMap La map par défaut à passer au service FreeMarker
     * @throws Exception
     */
    public static Map<String, Object> ajouterChampFusionAFreeMarkerMap(List<ChampFusionDTO> champsFusions, Map<String, Object> freeMarkerMap) throws Exception {

        if (freeMarkerMap == null) {
            freeMarkerMap = new HashMap<String, Object>();
        }

        if (champsFusions == null) {
            // Il n'y a pas de champs fusion à ajouter
            return freeMarkerMap;
        }

        for (ChampFusionDTO champ : champsFusions) {
            Object valeur = appliquerFormat(champ, freeMarkerMap);
            freeMarkerMap.put(champ.getNom(), valeur);
        }

        return freeMarkerMap;
    }

    private static Object appliquerFormat(ChampFusionDTO champFusion, Map<String, Object> freeMarkerMap) throws Exception {

        if (champFusion.getTypeChamp() != null) {
            // TypeChamp est bien saisie
            if (champFusion.getTypeChamp() == TypeChamp.SCRIPT || champFusion.getTypeChamp() == TypeChamp.SCRIPT_OOO) {
                return FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, champFusion.getScript());
            } else if (TypeChamp.OBJECT == champFusion.getTypeChamp() || TypeChamp.URL_IMG == champFusion.getTypeChamp()) {
                return String.valueOf(champFusion.getValeur());
            } else if (TypeChamp.SCRIPT_TABLEAU == champFusion.getTypeChamp()) {
                String xmlApresFreeMarker = FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, champFusion.getScript());
                JAXBElement jaxbElement = (JAXBElement) JAXBService.instance().getAsObject(xmlApresFreeMarker, DocumentXLS.class.getPackage().getName());
                if (jaxbElement == null) {
                    throw new AtexoDocumentGenerationException(ExceptionLogMessages.XmlDefinitionTableauIncorrect.getLibelle());
                }
                return jaxbElement.getValue();
            }
        } else {
            // TypeChamp est null
            return convertValueToString(champFusion.getValeur());
        }
        return null;
    }

    private static String convertValueToString(Object object) {

        if (object instanceof String || object instanceof Boolean) {
            String resultat = "";
            if (object != null) {
                resultat = String.valueOf(object);
            }
            return resultat;
        } else if (object instanceof Time) {
            return GeneriqueUtil.formatSimpleTime((Time) object);
        } else if (object instanceof Date || object instanceof Timestamp) {
            return GeneriqueUtil.formatSimpleDate((Date) object);
        } else if (object instanceof Long) {
            return String.valueOf(object);
        } else if (object instanceof Double) {
            return GeneriqueUtil.format((Double) object);
        } else if (object instanceof Float) {
            return GeneriqueUtil.format(((Float) object).doubleValue());
        } else if (object instanceof Enum) {
            return ((Enum) object).name();
        } else if (object instanceof Number) {
            return String.valueOf(((Number) object).intValue());
        }

        return object == null ? " " : String.valueOf(object);
    }
}
