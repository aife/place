package fr.atexo.commun.document.generation.service.bean;

import java.io.Serializable;

/**
 * Resultat de la génération d'un courrier electronique avec Freemarker.
 *
 */
public class ResultatCourrierFreeMarkerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3207384848107625755L;

	/**
	 * Objet du courrier elecronique.
	 */
	private String objet;

	/**
	 * Message du courrier electronique.
	 */
	private String message;

	/**
	 * objet du courrier elecronique.
	 * 
	 * @return the objet du courrier elecronique
	 */
	public final String getObjet() {
		return objet;
	}

	/**
	 * Sets the objet du courrier elecronique.
	 * 
	 * @param objet
	 *            the new objet du courrier elecronique
	 */
	public final void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * message du courrier electronique.
	 * 
	 * @return the message du courrier electronique
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 * message du courrier electronique.
	 * 
	 * @param message
	 *            the new message du courrier electronique
	 */
	public final void setMessage(String message) {
		this.message = message;
	}

}
