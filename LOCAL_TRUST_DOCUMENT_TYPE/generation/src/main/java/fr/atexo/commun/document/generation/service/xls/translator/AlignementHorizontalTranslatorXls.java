package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import fr.atexo.commun.document.generation.service.tableaudef.AlignementHorizontalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationAlignementHorizontal;

public class AlignementHorizontalTranslatorXls implements AlignementHorizontalTranslator {

	@Override
	public HorizontalAlignment transale(EnumerationAlignementHorizontal alignementHorizontal) {

		HorizontalAlignment translation;

		switch (alignementHorizontal) {
		case ALIGN_GENERAL:
		case ALIGN_LEFT:
			translation = HorizontalAlignment.GENERAL;
			break;

		case ALIGN_CENTER:
			translation = HorizontalAlignment.CENTER;
			break;

		case ALIGN_RIGHT:
			translation = HorizontalAlignment.RIGHT;
			break;

		case ALIGN_FILL:
			translation = HorizontalAlignment.FILL;
			break;

		case ALIGN_JUSTIFY:
			translation = HorizontalAlignment.JUSTIFY;
			break;

		case ALIGN_CENTER_SELECTION:
			translation = HorizontalAlignment.CENTER_SELECTION;
			break;

		default:
			translation = HorizontalAlignment.GENERAL;
			break;
		}

		return translation;
	}

}
