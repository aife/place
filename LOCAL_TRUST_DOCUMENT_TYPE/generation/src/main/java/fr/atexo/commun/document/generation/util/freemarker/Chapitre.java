package fr.atexo.commun.document.generation.util.freemarker;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * classe gerant les chapitre dans le XML d'un document ODT
 * @author RME
 *
 */
@SuppressWarnings("deprecation")
public class Chapitre implements TemplateMethodModel {
    /**
     * Loggueur.
     */
    private final Log LOG = LogFactory.getLog(getClass());
    
    public TemplateModel exec(List args) throws TemplateModelException {
        if (args.size() < 3) {
            throw new TemplateModelException("Wrong arguments");
        }

        String titre = (String) args.get(0);
        int niveau = Integer.parseInt((String) args.get(1));
        String style = (String) args.get(2);
        Integer index = null;
        if (args.size() == 4) {
            index = Integer.parseInt((String) args.get(3));
        }
        LOG.info("titre --> " + titre);
        LOG.info("niveau --> " + niveau);
        LOG.info("style --> " + style);
        LOG.info("index --> " + index);
        StringBuilder builder = new StringBuilder("\n");
        for (int i = 0; i < niveau; i++) {
            if (i == 0) {
                builder.append("<text:list");
                if (index != null) {
                    builder.append(" xml:id=\"").append("list").append(index).append("\"");
                }
                if (index != null && index > 1) {
                    builder.append(" text:continue-list=\"list").append(index - 1).append("\"");
                }
                builder.append(" text:continue-numbering=\"true\" text:style-name=\"Numbering_20_1\">");
            } else {
                builder.append("<text:list>");   
            }
            builder.append("\t<text:list-item>").append("\n");
        }
        builder.append("<text:h text:style-name=\"");
        builder.append(style);
        builder.append("\" text:outline-level=\"");
        builder.append(niveau).append("\">");
        builder.append(titre);
        builder.append("</text:h>").append("\n");

        for (int i = 0; i < niveau; i++) {
            builder.append("</text:list-item>");
            builder.append("</text:list>").append("\n");
        }
        LOG.info(builder.toString());
        SimpleScalar resultat = new SimpleScalar(builder.toString());
        return resultat;
    }
}
