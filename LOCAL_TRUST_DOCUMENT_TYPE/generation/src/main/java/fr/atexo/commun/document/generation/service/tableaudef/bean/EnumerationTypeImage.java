package fr.atexo.commun.document.generation.service.tableaudef.bean;

/**
 * Commentaire
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public enum EnumerationTypeImage {

	EMF, WMF, PICT, JPG, PNG, DIB;

	public String value() {
		return name();
	}

	public static EnumerationTypeImage fromValue(String v) {
		return valueOf(v);
	}

}
