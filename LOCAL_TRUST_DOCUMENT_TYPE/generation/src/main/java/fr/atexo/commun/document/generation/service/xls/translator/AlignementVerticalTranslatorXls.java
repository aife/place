package fr.atexo.commun.document.generation.service.xls.translator;

import org.apache.poi.ss.usermodel.VerticalAlignment;

import fr.atexo.commun.document.generation.service.tableaudef.AlignementVerticalTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationAlignementVertical;

public class AlignementVerticalTranslatorXls implements AlignementVerticalTranslator {

	@Override
	public VerticalAlignment transale(EnumerationAlignementVertical alignementVertical) {

		VerticalAlignment translation = null;

		switch (alignementVertical) {
			case VERTICAL_TOP:
				translation = VerticalAlignment.TOP;
				break;

			case VERTICAL_CENTER:
				translation = VerticalAlignment.CENTER;
				break;

			case VERTICAL_BOTTOM:
				translation = VerticalAlignment.BOTTOM;
				break;

			case VERTICAL_JUSTIFY:
				translation = VerticalAlignment.JUSTIFY;
				break;

			default:
				translation = VerticalAlignment.TOP;
				break;
		}

		return translation;
	}

}
