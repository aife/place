package fr.atexo.commun.document.generation.service.openoffice.translator;

import java.awt.Color;

import org.apache.poi.hssf.util.HSSFColor;

import fr.atexo.commun.document.generation.service.tableaudef.CouleurTranslator;
import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCouleur;

public class CouleurTranslatorOdt implements CouleurTranslator {

	@Override
	public Object transale(EnumerationCouleur couleur) {

		int translation = 0;

		short[] colorTriplet = null;

		switch (couleur) {

			case BLACK:
				colorTriplet = HSSFColor.HSSFColorPredefined.BLACK.getTriplet();
				break;
			case BROWN:
				colorTriplet = HSSFColor.HSSFColorPredefined.BROWN.getTriplet();
				break;
			case OLIVE_GREEN:
				colorTriplet = HSSFColor.HSSFColorPredefined.OLIVE_GREEN.getTriplet();
				break;
			case DARK_GREEN:
				colorTriplet = HSSFColor.HSSFColorPredefined.DARK_GREEN.getTriplet();
				break;
			case DARK_TEAL:
				colorTriplet = HSSFColor.HSSFColorPredefined.DARK_TEAL.getTriplet();
				break;
			case DARK_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.DARK_BLUE.getTriplet();
				break;
			case INDIGO:
				colorTriplet = HSSFColor.HSSFColorPredefined.INDIGO.getTriplet();
				break;
			case GREY_80_PERCENT:
				colorTriplet = HSSFColor.HSSFColorPredefined.GREY_80_PERCENT.getTriplet();
				break;
			case ORANGE:
				colorTriplet = HSSFColor.HSSFColorPredefined.ORANGE.getTriplet();
				break;
			case DARK_YELLOW:
				colorTriplet = HSSFColor.HSSFColorPredefined.DARK_YELLOW.getTriplet();
				break;
			case GREEN:
				colorTriplet = HSSFColor.HSSFColorPredefined.GREEN.getTriplet();
				break;
			case TEAL:
				colorTriplet = HSSFColor.HSSFColorPredefined.TEAL.getTriplet();
				break;
			case BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.BLUE.getTriplet();
				break;
			case BLUE_GREY:
				colorTriplet = HSSFColor.HSSFColorPredefined.BLUE_GREY.getTriplet();
				break;
			case GREY_50_PERCENT:
				colorTriplet = HSSFColor.HSSFColorPredefined.GREY_50_PERCENT.getTriplet();
				break;
			case RED:
				colorTriplet = HSSFColor.HSSFColorPredefined.RED.getTriplet();
				break;
			case LIGHT_ORANGE:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIGHT_ORANGE.getTriplet();
				break;
			case LIME:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIME.getTriplet();
				break;
			case SEA_GREEN:
				colorTriplet = HSSFColor.HSSFColorPredefined.SEA_GREEN.getTriplet();
				break;
			case AQUA:
				colorTriplet = HSSFColor.HSSFColorPredefined.AQUA.getTriplet();
				break;
			case LIGHT_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIGHT_BLUE.getTriplet();
				break;
			case VIOLET:
				colorTriplet = HSSFColor.HSSFColorPredefined.VIOLET.getTriplet();
				break;
			case GREY_40_PERCENT:
				colorTriplet = HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getTriplet();
				break;
			case PINK:
				colorTriplet = HSSFColor.HSSFColorPredefined.PINK.getTriplet();
				break;
			case GOLD:
				colorTriplet = HSSFColor.HSSFColorPredefined.GOLD.getTriplet();
				break;
			case YELLOW:
				colorTriplet = HSSFColor.HSSFColorPredefined.YELLOW.getTriplet();
				break;
			case BRIGHT_GREEN:
				colorTriplet = HSSFColor.HSSFColorPredefined.BRIGHT_GREEN.getTriplet();
				break;
			case TURQUOISE:
				colorTriplet = HSSFColor.HSSFColorPredefined.TURQUOISE.getTriplet();
				break;
			case DARK_RED:
				colorTriplet = HSSFColor.HSSFColorPredefined.DARK_RED.getTriplet();
				break;
			case SKY_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.SKY_BLUE.getTriplet();
				break;
			case PLUM:
				colorTriplet = HSSFColor.HSSFColorPredefined.PLUM.getTriplet();
				break;
			case GREY_25_PERCENT:
				colorTriplet = HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getTriplet();
				break;
			case ROSE:
				colorTriplet = HSSFColor.HSSFColorPredefined.ROSE.getTriplet();
				break;
			case LIGHT_YELLOW:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIGHT_YELLOW.getTriplet();
				break;
			case LIGHT_GREEN:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIGHT_GREEN.getTriplet();
				break;
			case LIGHT_TURQUOISE:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIGHT_TURQUOISE.getTriplet();
				break;
			case PALE_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.PALE_BLUE.getTriplet();
				break;
			case LAVENDER:
				colorTriplet = HSSFColor.HSSFColorPredefined.LAVENDER.getTriplet();
				break;
			case WHITE:
				colorTriplet = HSSFColor.HSSFColorPredefined.WHITE.getTriplet();
				break;
			case CORNFLOWER_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.CORNFLOWER_BLUE.getTriplet();
				break;
			case LEMON_CHIFFON:
				colorTriplet = HSSFColor.HSSFColorPredefined.LEMON_CHIFFON.getTriplet();
				break;
			case MAROON:
				colorTriplet = HSSFColor.HSSFColorPredefined.MAROON.getTriplet();
				break;
			case ORCHID:
				colorTriplet = HSSFColor.HSSFColorPredefined.ORCHID.getTriplet();
				break;
			case CORAL:
				colorTriplet = HSSFColor.HSSFColorPredefined.CORAL.getTriplet();
				break;
			case ROYAL_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.ROYAL_BLUE.getTriplet();
				break;
			case LIGHT_CORNFLOWER_BLUE:
				colorTriplet = HSSFColor.HSSFColorPredefined.LIGHT_CORNFLOWER_BLUE.getTriplet();
				break;
			case TAN:
				colorTriplet = HSSFColor.HSSFColorPredefined.TAN.getTriplet();
				break;

			default:
				break;
		}

		if (colorTriplet != null && colorTriplet.length == 3) {
			short red = colorTriplet[0];
			short green = colorTriplet[1];
			short blue = colorTriplet[2];
			translation = new Color(red, green, blue, 0).getRGB();
		}

		return translation;
	}

}
