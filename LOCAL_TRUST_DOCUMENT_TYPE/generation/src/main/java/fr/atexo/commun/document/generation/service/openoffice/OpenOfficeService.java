package fr.atexo.commun.document.generation.service.openoffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.star.connection.NoConnectException;

import fr.atexo.commun.commande.CommandeConfiguration;
import fr.atexo.commun.commande.CommandeLanceur;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.generation.service.AtexoDocumentGenerationException;
import fr.atexo.commun.document.generation.service.AtexoDocumentService;
import fr.atexo.commun.document.generation.service.DocumentGenerationService;
import fr.atexo.commun.document.generation.service.ExceptionLogMessages;
import fr.atexo.commun.document.generation.util.freemarker.Chapitre;
import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeInjoignableException;
import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeRelanceException;


/**
 * Le point d'entrée à l'ensemble des service OpenOffice du module generation des Documents
 *
 * @author JOR
 * @version $Revision$ $Date$
 */
public class OpenOfficeService extends AtexoDocumentService implements DocumentGenerationService {

    // déclaration du log
    private static final Log LOG = LogFactory.getLog(OpenOfficeService.class);

    private OpenOfficeGenerationConnexion openOfficeGenerationConnexion;

    private String repertoireTravailPath;

    private OpenOfficeGenerateur openOfficeGenerateur;

    private Map<String, Object> defautFreeMarkerMap;

    private Boolean desactiverTransformations;

    /*
    * Ce fichier est utilisé pour enregistrer le template généré dynamiquement pour le cas du FreeMarker dans
    * le 'content.xml'
    *
    */
    private File templateTemporaire;


    /**
     * Constructeur spécifiant le host et le port pour créer une {@link OpenOfficeGenerationConnexion} et un répertoire
     * de travail pour le Service OpenOffice
     *
     * @param host                  le host du Serveur OpenOffice
     * @param port                  le port du Serveur OpenOffice
     * @param repertoireTravailPath le repertoire de travail du serveur OpenOffice
     * @throws AtexoDocumentGenerationException
     *          si repertoireTravailPath est invalide
     */
    public OpenOfficeService(String host, String port, String repertoireTravailPath) throws AtexoDocumentGenerationException, ServiceOpenOfficeInjoignableException, ServiceOpenOfficeRelanceException {
        this(host, port, repertoireTravailPath, false);
    }

    /**
     * Constructeur spécifiant le host et le port pour créer une {@link OpenOfficeGenerationConnexion} et un répertoire
     * de travail pour le Service OpenOffice
     *
     * @param host                  le host du Serveur OpenOffice
     * @param port                  le port du Serveur OpenOffice
     * @param repertoireTravailPath le repertoire de travail du serveur OpenOffice
     * @throws AtexoDocumentGenerationException
     *          si repertoireTravailPath est invalide
     */
    public OpenOfficeService(String host, String port, String repertoireTravailPath, boolean desactiverTransformations) throws AtexoDocumentGenerationException, ServiceOpenOfficeInjoignableException, ServiceOpenOfficeRelanceException {
        this(host, port, repertoireTravailPath, desactiverTransformations, null);
    }

    public OpenOfficeService(String host, String port, String repertoireTravailPath, boolean desactiverTransformations, String commandeRelanceService) throws AtexoDocumentGenerationException, ServiceOpenOfficeInjoignableException, ServiceOpenOfficeRelanceException {
        this.desactiverTransformations = desactiverTransformations;
        setRepertoireTravailPath(repertoireTravailPath);
        try {
            this.openOfficeGenerationConnexion = new OpenOfficeGenerationConnexion(host, port);
        } catch (NoConnectException connectException) {
            LOG.error("Impossible de se connecter au service Open Office...");
            if (commandeRelanceService != null && commandeRelanceService.trim().length() != 0) {
                LOG.error("Tentative de relance du service Open Office avec la commande : " + commandeRelanceService);
                CommandeConfiguration commandeConfiguration = new CommandeConfiguration(commandeRelanceService);
                try {
                    CommandeLanceur.lancer(commandeConfiguration, false);
                    LOG.error("Le service Open office a été relancé avec succès...");
                    throw new ServiceOpenOfficeRelanceException("Tentative de relance du Service Open Office via le scripts de relance : " + commandeRelanceService);
                }
                catch (IOException e) {
                    LOG.error("Le service Open office n'a pas pu être relancé...");
                    throw new ServiceOpenOfficeInjoignableException("Service Open Office impossible à relancer avec relance automatique configurée", e);
                }
            } else {
                throw new ServiceOpenOfficeInjoignableException("Service Open Office injoignable avec relance automatique non configurée", connectException);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new AtexoDocumentGenerationException(e.getMessage());
        }
    }

    /**
     * @param repertoirePath le repertoire de travail du serveur OpenOffice
     * @throws AtexoDocumentGenerationException
     *          si repertoirePath est invalide
     */
    private void setRepertoireTravailPath(String repertoirePath) throws AtexoDocumentGenerationException {

        if (repertoirePath == null || !(new File(repertoirePath).exists())) {
            throw new AtexoDocumentGenerationException(ExceptionLogMessages.repertoireDeTravailInvalide);
        }
        if (repertoirePath.charAt(repertoirePath.length() - 1) != File.separatorChar) {
            repertoirePath += File.separator;
        }
        repertoireTravailPath = repertoirePath;
    }

    /**
     * Méthode permettant de générer un Document OpenOffice à partir d'un documentDTO {@link DocumentDTO}.
     * L'appel de la méthode supprimerFichierTemporaire() est nécessaire une fois cette méthode fini son exécution
     *
     * @param documentDTO Le document à générer
     * @param template    Le fichier template
     * @return InputStream un InputStream vers un fichier temporaire contenant le Document OpenOffice généré
     * @throws Exception
     */
    public InputStream genererDocument(DocumentDTO documentDTO, FichierDTO template) throws Exception {
        return genererDocumentDansRepertoireSpecifique(documentDTO, template, repertoireTravailPath);
    }

    public InputStream genererDocumentDansRepertoireSpecifique(DocumentDTO documentDTO, FichierDTO template, String path) throws Exception {

        openOfficeGenerateur = creerOpenOfficeGenerateur(documentDTO, template, path);

        openOfficeGenerateur.generateDocumentFromTemplate(desactiverTransformations);

        return openOfficeGenerateur.getDocumentToInputSteam();
    }

    /**
     * Méthode permettant de générer un template OpenOffice valide à partir d'un documentDTO {@link DocumentDTO},
     * et d'un template OpenOffice invalide contenant des balises Freemarker a interpreter.
     * L'appel de la méthode supprimerFichierTemporaire() est nécessaire une fois cette méthode fini son exécution
     *
     * @param documentDTO Le document à générer
     * @param template    Le fichier template
     * @return InputStream un InputStream vers un fichier temporaire contenant le template OpenOffice généré
     * @throws Exception
     */
    public InputStream genererDocumentDepuisTemplateFreemarker(DocumentDTO documentDTO, FichierDTO template) throws Exception {

        String cheminFichierTemplateOoo = TemplateGenerateur.generateTemplateFromTemplate(documentDTO.getChampFusionsScriptsOoo(), template, defautFreeMarkerMap);
        File fichierTemplateOoo = new File(cheminFichierTemplateOoo);
        InputStream fichierTemplateOooInputStream = new FileInputStream(fichierTemplateOoo);

        // Renommage du fichier en odt en evitant l'interpretation des extensions par Truezip
        templateTemporaire = new File(repertoireTravailPath + documentDTO.getFileName() + ".odt");
        OutputStream fichierTemplateOooOutputStream = new FileOutputStream(templateTemporaire);

        IOUtils.copyLarge(fichierTemplateOooInputStream, fichierTemplateOooOutputStream);
        IOUtils.closeQuietly(fichierTemplateOooInputStream);
        IOUtils.closeQuietly(fichierTemplateOooOutputStream);

        if (fichierTemplateOoo.exists()) {
            //On supprime ce fichier qui n'est plus utilisé
            fichierTemplateOoo.delete();
        }

        return new FileInputStream(templateTemporaire);
    }

    /**
     * Méthode permettant de générer un Document OpenOffice à partir d'un documentDTO {@link DocumentDTO}
     * et spécifiant un repertoire temporaire pour enregistrer ce Document (utilisé pour la création par lot)
     * L'appel de la méthode supprimerFichierTemporaire() est nécessaire une fois cette méthode fini son exécution
     *
     * @param documentDTO    Le document à générer
     * @param template       Le fichier template
     * @param repertoirePath repertoire temporaire pour enregistrer le Document à générer
     * @return InputStream un InputStream vers un fichier temporaire contenant le Document OpenOffice généré
     * @throws Exception
     */
    public InputStream genererDocument(DocumentDTO documentDTO, FichierDTO template, String repertoirePath) throws Exception {

        openOfficeGenerateur = creerOpenOfficeGenerateur(documentDTO, template, repertoireTravailPath);

        openOfficeGenerateur.setLotDirectoryName(repertoirePath);

        openOfficeGenerateur.generateDocumentFromTemplate(desactiverTransformations);

        return openOfficeGenerateur.getDocumentToInputSteam();
    }


    private OpenOfficeGenerateur creerOpenOfficeGenerateur(DocumentDTO documentDTO, FichierDTO template, String repertoireTravailPath) throws Exception {

        if (documentDTO == null) {
            throw new AtexoDocumentGenerationException(ExceptionLogMessages.documentDTONonTrouve.getLibelle());
        }
        if (!desactiverTransformations && TypeDocumentModele.Document == documentDTO.getDocumentModeleDTO().getTypeDocument() && !documentDTO.getChampFusionsScriptsOoo().isEmpty()) {

            //Alors il s'agit d'un DocumentModeleDTO qui doit être modifié dynamiquement avec du FreeMarker
            InputStream input = genererDocumentDepuisTemplateFreemarker(documentDTO, template);

            if (templateTemporaire != null && input.available() > 0) {
                //Alors une nouveau template a ete genere
                template = new FichierDTO(templateTemporaire);
            }
            input.close();
        }


        OpenOfficeGenerateur openOfficeGenerateur = new OpenOfficeGenerateur(openOfficeGenerationConnexion, documentDTO, template, repertoireTravailPath);
        openOfficeGenerateur.setFreeMarkerMap(defautFreeMarkerMap);

        return openOfficeGenerateur;
    }

    /**
     * Méthode permettant de supprimer le fichier temporaire créer par le Service OpenOffice.
     * Il doit être appelé systematiquement après une génération d'un Document OpenOffice
     */
    public void supprimerFichierTemporaire() {
        openOfficeGenerateur.deleteTempFile();

        if (this.templateTemporaire != null && this.templateTemporaire.exists()) {
            LOG.info("Supression de template temporaire");
            this.templateTemporaire.delete();
        }
    }

    /**
     * Méthode permettant de récuperer le fichier Temporaire utilisé par {@link OpenOfficeGenerateur}
     *
     * @return InputStream un InputStream vers un fichier temporaire contenant le Document OpenOffice généré
     */
    public File getFichierTemporaire() {
        return openOfficeGenerateur.getTempFile();
    }

    /**
     * Méthode permettant de setter la map freeMarker à utiliser avec des entrées par défaut
     *
     * @param freeMarkerMap La map par défaut
     */
    public void setFreeMarkerMapPourGeneration(Map<String, Object> freeMarkerMap) {
        defautFreeMarkerMap = freeMarkerMap;
        defautFreeMarkerMap.put("chapitre", new Chapitre());
    }


    public Boolean isDesactiverTransformations() {
        return desactiverTransformations;
    }

    public void setDesactiverTransformations(Boolean desactiverTransformations) {
        this.desactiverTransformations = desactiverTransformations;
    }
}
