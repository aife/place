/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service;

/**
 * Enums contenant le différents messages de logs utilisés par {@link AtexoDocumentGenerationException}
 * @author JOR
 * @version $Revision$ $Date$
 */
public enum ExceptionLogMessages {

    serviceOpenOfficeInjoignable("Erreur dans la génération d'un document : Le Service Open Office est injoignable"),
    documentDTONonTrouve("Erreur dans la génération d'un document : Aucun DocumentDTO n'a été trouvé"),
    templateNonTrouvee("Erreur dans la génération d'un document : Aucune Template n'a été trouvée"),
    XmlDefinitionTableauIncorrect("Erreur dans la génération d'un document : La définition XML de la template est incorrecte"),
    repertoireDeTravailInvalide("Erreur dans la génération d'un document : Le repertoire de travail spécifié est invalide");

    private String libelle;

    ExceptionLogMessages(String libelle) {
        this.libelle = libelle;
    }

    public String getLibelle() {
        return libelle;
    }
}
