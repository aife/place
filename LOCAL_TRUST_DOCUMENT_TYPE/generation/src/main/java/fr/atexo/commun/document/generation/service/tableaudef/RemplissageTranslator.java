package fr.atexo.commun.document.generation.service.tableaudef;

import fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationRemplissage;

public interface RemplissageTranslator {

	public Object transale(EnumerationRemplissage remplissage);

}
