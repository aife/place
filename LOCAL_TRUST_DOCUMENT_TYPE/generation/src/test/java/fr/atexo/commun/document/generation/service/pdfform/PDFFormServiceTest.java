package fr.atexo.commun.document.generation.service.pdfform;

import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;

import de.schlichtherle.io.File;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.TypeDonnee;
import fr.atexo.commun.util.ClasspathUtil;

@Ignore
public class PDFFormServiceTest {

	private final static String CHAMPS_TEXT = "champs_Text";
	private final static String CHAMPS_CHECKBOX = "champs_Checkbox";
	private final static String CHAMPS_RADIO = "champs_Radio";
	private final static String CHAMPS_DROPDOWN = "champs_Dropdown";
	private final static String CHAMPS_LISTBOX = "champs_Listbox";
	private final static String CHAMPS_LISTBOX_MULTISELECT = "champs_Listbox_multiselect";

	private final static String TEMPLATE_PATH = "fr/atexo/commun/document/generation/service/pdfform/New_Blank_Document.pdf";
	private final static String TEMPLATE_PATH2 = "fr/atexo/commun/document/generation/service/pdfform/Attestation_pour_AT-ou-Maladie.pdf";
	private final static String TEMPLATE_PATH3 = "fr/atexo/commun/document/generation/service/pdfform/Attestation _pour_IJSS.pdf";

	@Test
	public void testFill() {
		try {
			InputStream templateInputStream = ClasspathUtil.searchResource(TEMPLATE_PATH);

			File fstFile = new File("E:/Config/out.pdf");

			PDFFormService pdfFormService = new PDFFormService();

			List<ChampFusionDTO> champsFusion = new ArrayList<ChampFusionDTO>();

			addChampsFusion(champsFusion, CHAMPS_TEXT, CHAMPS_TEXT, TypeDonnee.DATE, 12.5);

			addChampsFusion(champsFusion, CHAMPS_CHECKBOX, CHAMPS_CHECKBOX, TypeDonnee.BOOLEAN, Boolean.TRUE);

			addChampsFusion(champsFusion, CHAMPS_RADIO, CHAMPS_RADIO, TypeDonnee.TEXT, "value2");

			addChampsFusion(champsFusion, CHAMPS_DROPDOWN, CHAMPS_DROPDOWN, TypeDonnee.TEXT, "value2");

			addChampsFusion(champsFusion, CHAMPS_LISTBOX, CHAMPS_LISTBOX, TypeDonnee.TEXT, "value3");

			addChampsFusion(champsFusion, CHAMPS_LISTBOX_MULTISELECT, CHAMPS_LISTBOX_MULTISELECT, TypeDonnee.TEXT, Arrays.asList("value1", "value3"));

			pdfFormService.fillForm(champsFusion, templateInputStream, fstFile);

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void tesGetAndSetFields() {

		try {
			InputStream templateInputStream = ClasspathUtil.searchResource(TEMPLATE_PATH2);
			PDFFormService pdfFormService = new PDFFormService();
			List<PDFFormField> fields = pdfFormService.getFormFieldsNames(templateInputStream);

			List<ChampFusionDTO> champsFusion = new ArrayList<ChampFusionDTO>();

			for (PDFFormField field : fields) {
				if (field.getTypeDonnee() == TypeDonnee.BOOLEAN) {
					addChampsFusion(champsFusion, field.getNom(), field.getNom(), TypeDonnee.BOOLEAN, Boolean.TRUE);
				} else {
					String val = field.getValeursPossibles() == null ? "valtest" : field.getValeursPossibles().get(0);
					addChampsFusion(champsFusion, field.getNom(), field.getNom(), TypeDonnee.STRING, val);
				}
			}

			File fstFile = new File("E:/Config/out2.pdf");

			templateInputStream = ClasspathUtil.searchResource(TEMPLATE_PATH2);

			pdfFormService.fillForm(champsFusion, templateInputStream, fstFile);

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testGenerateDescription() {

		try {
			InputStream templateInputStream = ClasspathUtil.searchResource(TEMPLATE_PATH);
			PDFFormService pdfFormService = new PDFFormService();
			List<PDFFormField> fields = pdfFormService.getFormFieldsNames(templateInputStream);

			FileWriter fw = new FileWriter(new File("E:/Config/Description3.txt"));

			for (PDFFormField field : fields) {

				fw.write("nom : '");
				fw.write(field.getNom());
				fw.write("', type : ");
				fw.write(field.getTypeDonnee().name());

				boolean first = true;
				if (field.getValeursPossibles() != null) {
					fw.write(", valeurs possibles : [");

					for (String val : field.getValeursPossibles()) {

						if (!first) {
							fw.write(", ");
						} else if (first) {
							first = false;
						}

						fw.write("'" + val + "'");

					}
					fw.write("]\n");
				} else {
					fw.write("\n");
				}

			}

			fw.flush();
			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	private void addChampsFusion(List<ChampFusionDTO> champsFusion, String nom, String path, TypeDonnee typeDonnee, Object value) {
		ChampFusionDTO champs = new ChampFusionDTO();
		champs.setNom(nom);
		champs.setPath(path);
		champs.setValeur(value);
		champs.setTypeDonnee(typeDonnee);
		champsFusion.add(champs);
	}

}
