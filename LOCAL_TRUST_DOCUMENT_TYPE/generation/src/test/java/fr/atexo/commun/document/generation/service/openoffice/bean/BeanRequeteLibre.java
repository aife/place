package fr.atexo.commun.document.generation.service.openoffice.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class BeanRequeteLibre implements Serializable {

    private String nom;
    private Double montantDemande;
    private Double montantPropose;
    private Date dateDepot;
    private Boolean actif;

    public BeanRequeteLibre() {
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Date getDateDepot() {
        return dateDepot;
    }

    public void setDateDepot(Date dateDepot) {
        this.dateDepot = dateDepot;
    }

    public Double getMontantDemande() {
        return montantDemande;
    }

    public void setMontantDemande(Double montantDemande) {
        this.montantDemande = montantDemande;
    }

    public Double getMontantPropose() {
        return montantPropose;
    }

    public void setMontantPropose(Double montantPropose) {
        this.montantPropose = montantPropose;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
