/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.generation.service.openoffice.bean.BeanParent;
import fr.atexo.commun.document.generation.util.IntrospectionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class IntrospectionUtilTest {

    private static final Log LOG = LogFactory.getLog(IntrospectionUtilTest.class);


    public static List<ChampFusionDTO> getChampFusionTestGeneration() {
        ChampFusionDTO cf1 = new ChampFusionDTO();
        cf1.setGenerationAttributesSansValeur("champ1_NomParent", "nom", null, null);

        ChampFusionDTO cf2 = new ChampFusionDTO();
        cf2.setGenerationAttributesSansValeur("champ2_NomEnfant1", "enfants[0].nom", null, null);

        ChampFusionDTO cf3 = new ChampFusionDTO();
        cf3.setGenerationAttributesSansValeur("champ3_AgeEnfant2", "enfants[1].age", null, null);

        ChampFusionDTO cf4 = new ChampFusionDTO();
        cf4.setGenerationAttributesSansValeur("champ4_MontantEnfant3", "enfants[2].montant", null, null);

        ChampFusionDTO cf5 = new ChampFusionDTO();
        cf5.setGenerationAttributesSansValeur("champ5_NomParentEnfant4", "enfants[3].beanParent.nom", null, null);

        ChampFusionDTO[] champs = new ChampFusionDTO[]{cf1, cf2, cf3, cf4, cf5};

        return Arrays.asList(champs);
    }

    @Test
    public void genererValeursTest() {

        IntrospectionUtil introUtil = new IntrospectionUtil(BeanParent.getExempleInstance());

        List<ChampFusionDTO> champs = getChampFusionTestGeneration();

        introUtil.calculerValeurs(champs);

        for (ChampFusionDTO champ : champs) {
            LOG.info(champ.getNom() + " : " + champ.getValeur());
            assertTrue(champ.getValeur() != null);
        }
    }

}
