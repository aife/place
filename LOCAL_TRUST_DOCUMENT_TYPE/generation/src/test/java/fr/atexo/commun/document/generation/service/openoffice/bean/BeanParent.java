/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice.bean;

import fr.atexo.commun.document.generation.service.openoffice.OpenOfficeGenerateurTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class BeanParent implements Serializable {


    private static final long serialVersionUID = -3027502938576509326L;
    private static final Log LOG = LogFactory.getLog(OpenOfficeGenerateurTest.class);


    List<BeanEnfant> enfants;
    String nom;

    public List<BeanEnfant> getEnfants() {
        return enfants;
    }

    public void setEnfants(List<BeanEnfant> enfants) {
        this.enfants = enfants;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public static BeanParent getExempleInstance() {
        BeanParent parent = new BeanParent();
        parent.setNom("Nom du Bean Parent");
        List<BeanEnfant> enfants = new ArrayList<BeanEnfant>();
        parent.setEnfants(enfants);

        try {
            Date date1 = new Date();
            Thread.sleep(1000);
            Date date2 = new Date();
            Thread.sleep(1000);
            Date date3 = new Date();
            Thread.sleep(1000);
            Date date4 = new Date();
            Thread.sleep(1000);
            Date date5 = new Date();
            Thread.sleep(1000);

            BeanEnfant be= new BeanEnfant();
            be.setNom("zzz");
            enfants.add(BeanEnfant.getExempleInstance(parent, "zzz5555", date2, 12.2, be));
            enfants.add(BeanEnfant.getExempleInstance(parent, "ffff000", date1, 4.4d, be));
            enfants.add(BeanEnfant.getExempleInstance(parent, "t4444444", date5, 4d, enfants.get(0)));
            enfants.add(BeanEnfant.getExempleInstance(parent, "aaa111", date3, 2.3d, enfants.get(2)));
            enfants.add(BeanEnfant.getExempleInstance(parent, "h477777", date4, 1.5, enfants.get(3)));
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        return parent;
    }
}
