package fr.atexo.commun.document.generation.service.openoffice;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Ignore
public class ChapitreTest {

    private static final Log LOG = LogFactory.getLog(OpenOfficeGenerateurTest.class);


    private static String getTestScriptFreeMarker() {
        StringBuffer scriptFreemarker = new StringBuffer("");
        try {
            scriptFreemarker.append(FileUtils.readFileToString(new File("D:/Workspaces/rsemCalendrier/commun.document/generation/src/test/resources/exemple/documentOdt/content_a_modifier.xml")));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return scriptFreemarker.toString();
    }

    @Test
    public void testFreeMarker() {

        try {
            Configuration conf = new Configuration();
            //Modification du format par défaut de la date
            conf.setDateFormat("dd/MM/yyyy");

            Template template = new Template("template_freeMarker.ftl", new StringReader(getTestScriptFreeMarker()), conf);

            Writer out = new StringWriter();

            Map<String, Object> root = new HashMap<String, Object>();
            root.put("chapitre", new fr.atexo.commun.document.generation.util.freemarker.Chapitre());
            template.process(root, out);

            LOG.info(out.toString());
            FileUtils.writeStringToFile(new File("D:/Workspaces/rsemCalendrier/commun.document/generation/src/test/resources/exemple/documentOdt/content.xml"), out.toString());

        } catch (Exception e) {
            LOG.error(e.fillInStackTrace());
            assertTrue(false);
        }

    }

}
