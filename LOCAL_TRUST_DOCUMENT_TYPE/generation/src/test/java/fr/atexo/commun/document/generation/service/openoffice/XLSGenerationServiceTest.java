package fr.atexo.commun.document.generation.service.openoffice;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.artofsolving.jodconverter.document.DefaultDocumentFormatRegistry;
import org.artofsolving.jodconverter.document.DocumentFormat;
import org.artofsolving.jodconverter.office.OfficeConnectionProtocol;
import org.junit.Ignore;
import org.junit.Test;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.util.XMLUtil;
import fr.atexo.commun.document.generation.service.openoffice.bean.BeanParent;
import fr.atexo.commun.document.generation.service.openoffice.bean.BeanRequeteLibre;
import fr.atexo.commun.document.generation.service.xls.XLSGenerationService;
import fr.atexo.commun.document.generation.util.freemarker.FreemarkerUtil;
import fr.atexo.commun.document.openoffice.OpenOfficeDocumentConverteur;
import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import freemarker.template.utility.ObjectConstructor;

@Ignore
public class XLSGenerationServiceTest {

	private static final Log LOG = LogFactory.getLog(XLSGenerationServiceTest.class);

	private final String FICHIER_ANCIEN_TEMPLATE_XLS = "fr/atexo/commun/document/generation/service/xls/templates/templateXLS.xls";
	private final String FICHIER_ANCIEN_XML = "fr/atexo/commun/document/generation/service/xls/documentXLS.xml";

	private final String FICHIER_NOUVEAU_XML = "fr/atexo/commun/document/generation/service/xls/documentStyleXLS.xml";
	private final String FICHIER_NOUVEAU_TEMPLATE_XLS_VIDE = "fr/atexo/commun/document/generation/service/xls/templates/templateVideXLS.xls";

	private final String FICHIER_NOUVEAU_XML_EXEMPLE = "fr/atexo/commun/document/generation/service/xls/documentExempleXLS.xml";
	private final String FICHIER_NOUVEAU_TEMPLATE_XLS_EXEMPLE = "fr/atexo/commun/document/generation/service/xls/templates/templateVideExempleXLS.xls";

	private final String FICHIER_NOUVEAU_DATE_XML = "fr/atexo/commun/document/generation/service/xls/documentDateXLS.xml";
	private final String FICHIER_NOUVEAU_DATE_TEMPLATE_XLS = "fr/atexo/commun/document/generation/service/xls/templates/templateVideXLS.xls";

	private final String FICHIER_NOUVEAU_XML_LYCEE = "fr/atexo/commun/document/generation/service/xls/ficheLyceeIteration.xml";
	private final String FICHIER_NOUVEAU_TEMPLATE_XLS_LYCEE = "fr/atexo/commun/document/generation/service/xls/templates/ficheLyceeIteration.xls";

	private final String FICHIER_NOUVEAU_XML_ACOMPTE = "fr/atexo/commun/document/generation/service/xls/Acompte_intermediaire.xml";
	private final String FICHIER_NOUVEAU_TEMPLATE_XLS_ACOMPTE = "fr/atexo/commun/document/generation/service/xls/templates/Acompte_intermediaire.xls";

	private final String FICHIER_NOUVEAU_XML_ANNEXE = "fr/atexo/commun/document/generation/service/xls/annexe.xml";
	private final String FICHIER_NOUVEAU_TEMPLATE_XLS_ANNEXE = "fr/atexo/commun/document/generation/service/xls/templates/annexe.xls";

	private final String FICHIER_XML_TEST_MOTEUR_VERSION_1 = "fr/atexo/commun/document/generation/service/xls/testMoteurVersion1.xml";

	private final String NOM_TEMPLATE_TEMP = "templateTempXLS";
	private final String NOM_FICHIER_GENERE_TEMP = "templateTempXLS";

	private static String URL_IMAGE_1 = "http://www.google.fr/logos/2012/Rodin-2012-homepage.png";

	@Test
	public void testRequeteLibreCodeXML() throws Exception {

		List<ChampFusionDTO> champFusions = XMLUtilTest.getXMLCodeExemple();
		String codeXML = XMLUtil.getCodeFreeMarkerXML("Dossiers", 1, champFusions, true);

		List<BeanRequeteLibre> beanRequeteLibres = new ArrayList<BeanRequeteLibre>();
		BeanRequeteLibre beanRequeteLibre1 = new BeanRequeteLibre();
		beanRequeteLibre1.setNom("Dispositif désherbeur thermique");
		beanRequeteLibre1.setActif(true);
		beanRequeteLibre1.setDateDepot(new Date());
		beanRequeteLibre1.setMontantDemande(1000.8);
		beanRequeteLibre1.setMontantPropose(100.1);
		beanRequeteLibres.add(beanRequeteLibre1);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("listeDossiers", beanRequeteLibres);

		genererServiceDepuisContenu(FICHIER_NOUVEAU_TEMPLATE_XLS_VIDE, codeXML, map, new ArrayList<ChampFusionDTO>(), null);

	}

	private HSSFCell creerCellule(HSSFRow ligne, int numero, String texte) {

		HSSFCell cellule = ligne.getCell(numero);
		if (cellule == null) {
			cellule = ligne.createCell(numero);
		}
		cellule.setCellValue(texte);

		return cellule;
	}

	private CellStyle creerCelluleStyle(Workbook workbook) {
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setWrapText(true);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		return cellStyle;
	}

	@Test
	public void creationTest() throws IOException {

		InputStream templateInputStream = ClasspathUtil.searchResource(FICHIER_NOUVEAU_TEMPLATE_XLS_EXEMPLE);
		HSSFWorkbook workbook = new HSSFWorkbook(templateInputStream);

		HSSFSheet sheet = workbook.getSheetAt(0);

		CellStyle style = creerCelluleStyle(workbook);

		for (int i = 3; i < 8; i++) {

			// creation + intercallage

			HSSFRow rowTemp = sheet.createRow(sheet.getLastRowNum() + 1);
			Integer hauteur = new Integer(-1);
			rowTemp.setHeight(hauteur.shortValue());

			sheet.shiftRows(i, sheet.getLastRowNum(), 1, false, false);

			// traitement des lignes
			HSSFRow row = sheet.getRow(i);

			HSSFCell parcours = creerCellule(row, 0, "Parcours Parcours Parcours Parcours Parcours Parcours Parcours Parcours Parcours Parcours " + i);
			parcours.setCellStyle(style);

			HSSFCell nature = creerCellule(row, 1, "Nature " + i);
			nature.setCellStyle(style);

			HSSFCell intitule = creerCellule(row, 2, "Intitulé " + i);
			intitule.setCellStyle(style);

			HSSFCell groupement = creerCellule(row, 3, "Groupement " + i);
			groupement.setCellStyle(style);

			HSSFCell niveau = creerCellule(row, 4, "Niveau " + i);
			niveau.setCellStyle(style);
		}

		File excelFile = FichierTemporaireUtil.creerFichierTemporaire(NOM_TEMPLATE_TEMP, ".xls");

		OutputStream excelOutputStream = new FileOutputStream(excelFile);
		workbook.write(excelOutputStream);

		templateInputStream.close();
		excelOutputStream.close();

		boolean conversionPdf = false;

		if (conversionPdf) {
			OpenOfficeDocumentConverteur openOfficeDocumentConverteur = new OpenOfficeDocumentConverteur(OfficeConnectionProtocol.SOCKET, true, 8100);

			File pdfFile = FichierTemporaireUtil.creerFichierTemporaire(NOM_TEMPLATE_TEMP, ".pdf");

			DefaultDocumentFormatRegistry format = new DefaultDocumentFormatRegistry();
			DocumentFormat formatPdf = format.getFormatByExtension("pdf");

			try {
				openOfficeDocumentConverteur.convertirDocument(excelFile, pdfFile, formatPdf);
				assertTrue(pdfFile.exists() && pdfFile.length() > 0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void doubleTest() {
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00", new DecimalFormatSymbols(Locale.ENGLISH));
		Double doubleOrigine = 400D;
		String stringFormate = decimalFormat.format(doubleOrigine);
		Double doubleFormate = new Double(stringFormate);
		assertTrue(doubleFormate != null);
	}

	@Test
	public void dateTest() {
		Date date = new Date();
		Long timestamp = date.getTime();
		assertTrue(timestamp != null);
	}

	@Test
	public void xlsAncienneGenerationServiceTest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beanParent", BeanParent.getExempleInstance());
		genererServiceDepuisFichiers(FICHIER_ANCIEN_TEMPLATE_XLS, FICHIER_ANCIEN_XML, map, null, true);
	}

	@Test
	public void xlsNouvelleGenerationServiceDateTest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("testDate", new Date());
		map.put("objectConstructor", new ObjectConstructor());
		map.put("freemarkerUtil", new FreemarkerUtil());
		genererServiceDepuisFichiers(FICHIER_NOUVEAU_DATE_TEMPLATE_XLS, FICHIER_NOUVEAU_DATE_XML, map, null, false);
	}

	@Test
	public void xlsNouvelleGenerationServiceTest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beanParent", BeanParent.getExempleInstance());
		genererServiceDepuisFichiers(FICHIER_NOUVEAU_TEMPLATE_XLS_VIDE, FICHIER_NOUVEAU_XML, map, null, false);
	}

	@Test
	public void xlsNouvelleGenerationServiceExempleTest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beanParent", BeanParent.getExempleInstance());
		genererServiceDepuisFichiers(FICHIER_NOUVEAU_TEMPLATE_XLS_EXEMPLE, FICHIER_NOUVEAU_XML_EXEMPLE, map, null, false);
	}

	@Test
	public void xlsNouvelleGenerationServiceLyceeTest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LYCEE", genererLycee());
		genererServiceDepuisFichiers(FICHIER_NOUVEAU_TEMPLATE_XLS_LYCEE, FICHIER_NOUVEAU_XML_LYCEE, map, null, false);
	}

	@Test
	public void xlsNouvelleGenerationServiceAcompteIntermediaireTest() throws Exception {
		Map<String, Object> freemarkerMap = new HashMap<String, Object>();
		freemarkerMap.put("acompte", genererAcompte());
		freemarkerMap.put("image", genererImage(URL_IMAGE_1));

		List<LinkedHashMap<String, Object>> donneesTableau1 = genererParents(true);
		Map<String, List<LinkedHashMap<String, Object>>> donneesTableauMap = new HashMap<String, List<LinkedHashMap<String, Object>>>();
		donneesTableauMap.put("exemple", donneesTableau1);

		genererServiceDepuisFichiers(FICHIER_NOUVEAU_TEMPLATE_XLS_ACOMPTE, FICHIER_NOUVEAU_XML_ACOMPTE, freemarkerMap, donneesTableauMap, false);
	}

	@Test
	public void xlsNouvelleGenerationServiceAnnexeTest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("acompte", genererAcompte());
		genererServiceDepuisFichiers(FICHIER_NOUVEAU_TEMPLATE_XLS_ANNEXE, FICHIER_NOUVEAU_XML_ANNEXE, map, null, false);
	}

	@Test
	public void xlsTestGenerationMoteurVersion1() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beanParent", BeanParent.getExempleInstance());
		genererServiceDepuisFichiers(FICHIER_NOUVEAU_TEMPLATE_XLS_VIDE, FICHIER_XML_TEST_MOTEUR_VERSION_1, map, null, false);
	}

	private void genererServiceDepuisContenu(String cheminTemplate, String contenuXML, Map<String, Object> freemarkerMap, List<ChampFusionDTO> champFusions, Map<String, List<LinkedHashMap<String, Object>>> donneesTableaux) throws Exception {

		// Création d'un documentModele pour génération d'un fichier XLS
		DocumentModeleDTO docModele = new DocumentModeleDTO();
		docModele.setXml(contenuXML);
		DocumentDTO documentDTO = new DocumentDTO(docModele, null, champFusions);

		// Copie du template XLS vers le FileSystem autant que fichier
		// "temporaire"
		File templateXLSTemp = FichierTemporaireUtil.creerFichierTemporaire(NOM_TEMPLATE_TEMP, ".xls");
		OutputStream os = new FileOutputStream(templateXLSTemp);
		IOUtils.copy(ClasspathUtil.searchResource(cheminTemplate), os);

		// Appel au service génération du document XLS
		XLSGenerationService service = new XLSGenerationService();
		InputStream inputStreamFichierGenere = service.genererDocument(documentDTO, new FichierDTO(templateXLSTemp), freemarkerMap, donneesTableaux);

		// Vérification qu'un fichier a été créé
		assertTrue(inputStreamFichierGenere != null && inputStreamFichierGenere.available() > 0);

		// Copie du fichier généré vers le FileSystem autant que fichier
		// "temporaire"
		File fichierXLSGenereTemp = FichierTemporaireUtil.creerFichierTemporaire(NOM_FICHIER_GENERE_TEMP, ".xls");
		FileOutputStream file = new FileOutputStream(fichierXLSGenereTemp);
		IOUtils.copy(inputStreamFichierGenere, file);
		LOG.info("Fichier généré : " + fichierXLSGenereTemp.getAbsolutePath());

		IOUtils.closeQuietly(file);
		IOUtils.closeQuietly(os);

		templateXLSTemp.delete();
		assertTrue(!templateXLSTemp.exists());

		service.supprimerFichierTemporaire();

		// Suppression des fichiers créés
		fichierXLSGenereTemp.delete();
		assertTrue(!fichierXLSGenereTemp.exists());

		LOG.info("Les fichiers générés ont été supprimés");
	}

	private void genererServiceDepuisFichiers(String cheminTemplate, String cheminFichierXML, Map<String, Object> map, Map<String, List<LinkedHashMap<String, Object>>> donneesTableaux, boolean genererChampFusion) throws Exception {

		// Création d'un documentModele pour génération d'un fichier XLS
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		IOUtils.copy(ClasspathUtil.searchResource(cheminFichierXML), outputStream);
		String contenuXML = new String(outputStream.toByteArray());
		outputStream.close();

		List<ChampFusionDTO> champs = genererChampFusion ? OpenOfficeGenerateurTest.getChampFusionTestGeneration() : new ArrayList<ChampFusionDTO>();
		genererServiceDepuisContenu(cheminTemplate, contenuXML, map, champs, donneesTableaux);
	}

	public class Image {

		private String url;

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
	}

	public class Acompte {

		private String sousProgramme;

		private String campagne;

		private String numeroIris;

		private String date;

		private String periode;

		private List<VolumeMontant> volumeMontants;

		public String getSousProgramme() {
			return sousProgramme;
		}

		public void setSousProgramme(String sousProgramme) {
			this.sousProgramme = sousProgramme;
		}

		public String getCampagne() {
			return campagne;
		}

		public void setCampagne(String campagne) {
			this.campagne = campagne;
		}

		public String getNumeroIris() {
			return numeroIris;
		}

		public void setNumeroIris(String numeroIris) {
			this.numeroIris = numeroIris;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getPeriode() {
			return periode;
		}

		public void setPeriode(String periode) {
			this.periode = periode;
		}

		public List<VolumeMontant> getVolumeMontants() {
			return volumeMontants;
		}

		public void setVolumeMontants(List<VolumeMontant> volumeMontants) {
			this.volumeMontants = volumeMontants;
		}
	}

	public class VolumeMontant {

		private Double volume;

		private Double montant;

		public VolumeMontant() {
		}

		public VolumeMontant(Double volume, Double montant) {
			this.volume = volume;
			this.montant = montant;
		}

		public Double getVolume() {
			return volume;
		}

		public void setVolume(Double volume) {
			this.volume = volume;
		}

		public Double getMontant() {
			return montant;
		}

		public void setMontant(Double montant) {
			this.montant = montant;
		}
	}

	public class Lycee {

		private List<LyceeContact> contacts;

		public List<LyceeContact> getContacts() {
			return contacts;
		}

		public void setContacts(List<LyceeContact> contacts) {
			this.contacts = contacts;
		}
	}

	public class LyceeContact {

		private String nom;
		private String prenom;
		private String civilite;
		private String fonction;

		private LyceeContact() {
		}

		private LyceeContact(String nom, String prenom, String civilite, String fonction) {
			this.nom = nom;
			this.prenom = prenom;
			this.civilite = civilite;
			this.fonction = fonction;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public String getCivilite() {
			return civilite;
		}

		public void setCivilite(String civilite) {
			this.civilite = civilite;
		}

		public String getFonction() {
			return fonction;
		}

		public void setFonction(String fonction) {
			this.fonction = fonction;
		}
	}

	private Lycee genererLycee() {

		Lycee lycee = new Lycee();
		List<LyceeContact> contacts = new ArrayList<LyceeContact>();
		lycee.setContacts(contacts);

		for (int i = 0; i < 240; i++) {

			LyceeContact contact = new LyceeContact("Dupont", "Jean", "Mr", "DIRECTEUR");
			contacts.add(contact);
		}

		return lycee;
	}

	private Acompte genererAcompte() {

		Acompte acompte = new Acompte();
		acompte.setSousProgramme("Sous Programme de Test");
		acompte.setCampagne("Campagne de Test");
		acompte.setNumeroIris("45FL560DF");
		acompte.setDate("10/10/2011");
		acompte.setPeriode("Quatrième Trimestre 2011");

		List<VolumeMontant> volumeMontants = new ArrayList<VolumeMontant>();

		acompte.setVolumeMontants(volumeMontants);
		volumeMontants.add(new VolumeMontant(1D, 10D));
		volumeMontants.add(new VolumeMontant(2D, 20D));
		volumeMontants.add(new VolumeMontant(3D, 1011D));
		volumeMontants.add(new VolumeMontant(4D, 1345D));
		volumeMontants.add(new VolumeMontant(5D, 1D));
		volumeMontants.add(new VolumeMontant(6D, 1055D));
		volumeMontants.add(new VolumeMontant(7D, 145D));
		volumeMontants.add(new VolumeMontant(8D, 410D));

		return acompte;
	}

	private Image genererImage(String urlHttp) {

		Image image = new Image();
		image.setUrl(urlHttp);

		return image;
	}

	private List<LinkedHashMap<String, Object>> genererParents(boolean enfant) {

		List<LinkedHashMap<String, Object>> donneesTableaux = new ArrayList<LinkedHashMap<String, Object>>();

		LinkedHashMap<String, Object> donneesParent1 = new LinkedHashMap<String, Object>();
		donneesParent1.put("Prénom", "Marcel");
		donneesParent1.put("Nom", "Dupont");
		donneesParent1.put("Date de naissance", "23/04/1984");
		if (enfant) {
			donneesParent1.put("Enfant - Prénom", "");
			donneesParent1.put("Enfant - Nom", "");
			donneesParent1.put("Enfant - Date de naissance", "");
		}
		donneesTableaux.add(donneesParent1);

		LinkedHashMap<String, Object> donneesParent2 = new LinkedHashMap<String, Object>();
		donneesParent2.put("Prénom", "Agathe");
		donneesParent2.put("Nom", "Martin");
		donneesParent2.put("Date de naissance", "14/04/1973");
		if (enfant) {
			donneesParent2.put("Enfant - Prénom", "");
			donneesParent2.put("Enfant - Nom", "");
			donneesParent2.put("Enfant - Date de naissance", "");
		}
		donneesTableaux.add(donneesParent2);

		LinkedHashMap<String, Object> donneesParent3AvecEnfant1 = new LinkedHashMap<String, Object>();
		donneesParent3AvecEnfant1.put("Prénom", "Gustave");
		donneesParent3AvecEnfant1.put("Nom", "Fortier");
		donneesParent3AvecEnfant1.put("Date de naissance", "16/04/1980");
		donneesTableaux.add(donneesParent3AvecEnfant1);
		if (enfant) {
			donneesParent3AvecEnfant1.put("Enfant - Prénom", "Eric");
			donneesParent3AvecEnfant1.put("Enfant - Nom", "Fortier");
			donneesParent3AvecEnfant1.put("Enfant - Date de naissance", "23/02/2010");

			LinkedHashMap<String, Object> donneesParent3AvecEnfant2 = new LinkedHashMap<String, Object>();
			donneesParent3AvecEnfant2.put("Prénom", "Gustave");
			donneesParent3AvecEnfant2.put("Nom", "Fortier");
			donneesParent3AvecEnfant2.put("Date de naissance", "16/04/1980");
			donneesParent3AvecEnfant2.put("Enfant - Prénom", "Maxime");
			donneesParent3AvecEnfant2.put("Enfant - Nom", "Fortier");
			donneesParent3AvecEnfant2.put("Enfant - Date de naissance", "12/06/2011");
			donneesTableaux.add(donneesParent3AvecEnfant2);
		}

		return donneesTableaux;
	}
}
