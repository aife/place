package fr.atexo.commun.document.generation.service.openoffice;

import static org.junit.Assert.assertTrue;

import javax.xml.bind.JAXBElement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;

import fr.atexo.commun.document.generation.service.tableaudef.bean.Cellule;
import fr.atexo.commun.document.generation.service.tableaudef.bean.CelluleCoordonnee;
import fr.atexo.commun.document.generation.service.tableaudef.bean.DocumentXLS;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Onglet;
import fr.atexo.commun.jaxb.JAXBService;

@Ignore
public class JAXBTest {

	private static final Log LOG = LogFactory.getLog(JAXBTest.class);

	public static DocumentXLS createDocumentXLSTest() {

		DocumentXLS documentXLS = new DocumentXLS();

		CelluleCoordonnee valeur2 = new CelluleCoordonnee();
		valeur2.setColonne(1);
		valeur2.setLigne(4);

		CelluleCoordonnee valeur3 = new CelluleCoordonnee();
		valeur3.setColonne(5);
		valeur3.setLigne(2);

		CelluleCoordonnee valeur4 = new CelluleCoordonnee();
		valeur4.setColonne(7);
		valeur4.setLigne(5);

		documentXLS.getOnglet().add(new Onglet());
		documentXLS.getOnglet().get(0).setNom("test_onglet1");

		Cellule cell = new Cellule();
		cell.setCoordonnee(valeur2);
		cell.setValeur("valeur2");

		documentXLS.getOnglet().get(0).getCellule().add(cell);

		cell = new Cellule();
		cell.setCoordonnee(valeur3);
		cell.setValeur("valeur3");
		documentXLS.getOnglet().get(0).getCellule().add(cell);

		cell = new Cellule();
		cell.setCoordonnee(valeur4);
		cell.setValeur("valeur4");
		documentXLS.getOnglet().get(0).getCellule().add(cell);

		documentXLS.getOnglet().add(new Onglet());
		documentXLS.getOnglet().get(1).setNom("test_onglet2");

		cell = new Cellule();
		cell.setCoordonnee(valeur2);
		cell.setValeur("valeur2");
		documentXLS.getOnglet().get(1).getCellule().add(cell);

		cell = new Cellule();
		cell.setCoordonnee(valeur3);
		cell.setValeur("valeur3");
		documentXLS.getOnglet().get(1).getCellule().add(cell);

		cell = new Cellule();
		cell.setCoordonnee(valeur4);
		cell.setValeur("valeur4");
		documentXLS.getOnglet().get(1).getCellule().add(cell);

		return documentXLS;
	}

	@Test
	public void jaxbTest() {

		DocumentXLS documentXLS = createDocumentXLSTest();

		// Test du marshalling
		LOG.info("flux xml généré : " + JAXBService.instance().getAsString(documentXLS, documentXLS.getClass().getPackage().getName()));

		// Test du unmarshalling
		String xml = "<documentXLS><onglet><nom>test_onglet1</nom><cellule><coordonne colonne=\"1\" ligne=\"4\"/><valeur>valeur2</valeur></cellule><cellule><coordonne colonne=\"5\" ligne=\"2\"/><valeur>valeur3</valeur></cellule><cellule><coordonne colonne=\"7\" ligne=\"5\"/><valeur>valeur4</valeur></cellule></onglet><onglet><nom>test_onglet2</nom><cellule><coordonne colonne=\"1\" ligne=\"4\"/><valeur>valeur2</valeur></cellule><cellule><coordonne colonne=\"5\" ligne=\"2\"/><valeur>valeur3</valeur></cellule><cellule><coordonne colonne=\"7\" ligne=\"5\"/><valeur>valeur4</valeur></cellule></onglet></documentXLS>";

		DocumentXLS doc = (DocumentXLS) ((JAXBElement) JAXBService.instance().getAsObject(xml, documentXLS.getClass().getPackage().getName())).getValue();

		assertTrue(doc != null && doc.getOnglet().get(0).getCellule() != null && doc.getOnglet().get(0).getCellule().get(0) != null
				&& doc.getOnglet().get(0).getCellule().get(0).getCoordonne() != null
				&& doc.getOnglet().get(0).getCellule().get(0).getCoordonne().getColonne() == 1
				&& doc.getOnglet().get(0).getCellule().get(0).getCoordonne().getLigne() == 4 && doc.getOnglet().get(0).getCellule().get(0).getValeur() != null);

	}

}
