/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice;

import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.generation.service.bean.ResultatCourrierFreeMarkerBean;
import fr.atexo.commun.document.generation.service.freemarker.FreeMarkerService;
import fr.atexo.commun.document.generation.service.openoffice.bean.BeanParent;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class FreeMarkerMailGenerateurTest {

    private static final Log LOG = LogFactory.getLog(OpenOfficeGenerateurTest.class);

    @Test
    public void creerListes() throws Exception {

        StringBuffer buffer = new StringBuffer("<#list beanParent.enfants?sort_by(\"get('test')\") as e>${e.nom} | </#list>");
        DocumentDTO doc = new DocumentDTO(DocumentModeleDTO.getInstancePourGenerationMail(buffer.toString()), OpenOfficeGenerateurTest.getChampFusionTestGeneration());
        Map<String, Object> defaultMap = new HashMap<String, Object>();
        defaultMap.put("beanParent", BeanParent.getExempleInstance());
        ResultatCourrierFreeMarkerBean resultat = FreeMarkerService.generateMail(doc, defaultMap);

        LOG.info(resultat);

    }


    @Test
    public void creerContenuMailAvecFreeMarkerTest() {
        try {
            //Contenu pris du composant gwt :
            String entete = "<div style=\"text-align: center;\"><span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\"><span style=\"text-decoration: underline;\"></span></span></span><span style=\"font-style: italic;\"></span><span style=\"font-style: italic;\"></span>";
            String footer = "<span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\"><span style=\"background-color: white;\"><span style=\"background-color: black;\"><span style=\"background-color: red;\"></span></span></span>,</span></span><br><span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\"></span></span></div><span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\"><br></span></span><div style=\"margin-left: 40px;\">Voici un test de mail avec champ <span style=\"font-style: italic;\">italique</span>, <span style=\"text-decoration: underline;\">souligné</span> et <span style=\"font-weight: bold;\">bold</span><span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\">.</span></span><br><br><img src=\"http://ltsub-fc.local-trust.com/ltsub_fc/images/bandeau-right.jpg\"><br><span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\"></span></span></div><span style=\"text-decoration: underline;\"><span style=\"font-style: italic;\"><br></span></span><br>";
            StringBuffer buffer = new StringBuffer(entete);
            buffer.append("\n\n\nBonjour <#if (beanParent.enfants[0].age > 18)>${cf_monsieur}<#else>${cf_garcon}</#if> ${beanParent.enfants[0].nom}, \n\n\nIci un champFusion de type script :\n ${freemarker_script}");
            buffer.append(footer);

            DocumentDTO doc = new DocumentDTO(DocumentModeleDTO.getInstancePourGenerationMail(buffer.toString()), OpenOfficeGenerateurTest.getChampFusionTestGeneration());

            Map<String, Object> defaultMap = new HashMap<String, Object>();
            defaultMap.put("beanParent", BeanParent.getExempleInstance());

            ResultatCourrierFreeMarkerBean resultat = FreeMarkerService.generateMail(doc, defaultMap);

            LOG.info(resultat);

            //Verifier que rien a été mal modifié par FreeMarker
            assertTrue(resultat.getMessage().indexOf(entete) != -1);
            assertTrue(resultat.getMessage().indexOf(footer) != -1);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

    }

    public static String getTestScriptFreeMarker() {

        StringBuffer scriptFreemarker = new StringBuffer("\n\nBeanParen nom : ${beanParent.nom}\n");
        scriptFreemarker.append("BeanEnfant 1 nom : <#if beanParent.enfants[0].nom == 'zzz5555'>ca marche!</#if>\n");
        scriptFreemarker.append("BeanEnfant 2 age : ${beanParent.enfants[1].age}\n");
        scriptFreemarker.append("BeanEnfant 3 dateNaissance : ${beanParent.enfants[2].dateNaissance?date}\n");
        scriptFreemarker.append("BeanEnfant 4 montant : ${beanParent.enfants[3].montant}\n");
        scriptFreemarker.append("BeanEnfant 5 BeanParent nom : ${beanParent.enfants[4].beanParent.nom}\n");
        scriptFreemarker.append("Résultat d'une expression if-else : <#if (beanParent.enfants[4].age > 18)> L'enfant est un adult <#else> L'enfant est un garçon</#if>\n");
        scriptFreemarker.append("\nList des noms des enfants :\n<#list beanParent.enfants as enf>${enf_index+1}. ${enf.nom}\n </#list>");

        return scriptFreemarker.toString();
    }

    public static String getTestScriptFreeMarkerXmlOoo() {
        String freemarkerXmlOoo = "<text:list xml:id=\"list32233225\" text:style-name=\"Numbering_20_1\"><text:list-item><text:h text:style-name=\"Onglet\" text:outline-level=\"1\">${beanParent.nom}</text:h></text:list-item></text:list>";
        return freemarkerXmlOoo;
    }


    @Test
    public void testFreeMarker() {

        try {

            Configuration conf = new Configuration();
            //Modification du format par défaut de la date
            conf.setDateFormat("dd/MM/yyyy");

            Template template = new Template("template_freeMarker.ftl", new StringReader(getTestScriptFreeMarker()), conf);

            Writer out = new StringWriter();

            Map<String, BeanParent> map = new HashMap<String, BeanParent>();
            map.put("beanParent", BeanParent.getExempleInstance());

            template.process(map, out);

            LOG.info(out.toString());

        } catch (Exception e) {
            LOG.error(e.fillInStackTrace());
            assertTrue(false);
        }

    }

}
