/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class BeanEnfant implements Serializable {

    private static final long serialVersionUID = 6691476137122013515L;

    BeanParent beanParent;
    String nom;
    Date dateNaissance;
    Double montant;
    BeanEnfant frere;
    int age;

    public static BeanEnfant getExempleInstance(BeanParent beanParent, String nom, Date dateNaissance, double montant, BeanEnfant frere) {
        BeanEnfant enfant = new BeanEnfant();
        enfant.setAge(18);
        enfant.setNom(nom);
        enfant.setMontant(montant);
        enfant.setDateNaissance(dateNaissance);
        enfant.setBeanParent(beanParent);
        enfant.setFrere(frere);
        return enfant;
    }

    public BeanParent getBeanParent() {
        return beanParent;
    }

    public void setBeanParent(BeanParent beanParent) {
        this.beanParent = beanParent;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BeanEnfant getFrere() {
        return frere;
    }

    public void setFrere(BeanEnfant frere) {
        this.frere = frere;
    }

    public String get(String chaine){
        return nom;
    }
}
