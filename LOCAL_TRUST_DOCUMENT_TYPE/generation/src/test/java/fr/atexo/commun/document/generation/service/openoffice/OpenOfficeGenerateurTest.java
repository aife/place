/**
 * $Id$
 */
package fr.atexo.commun.document.generation.service.openoffice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.artofsolving.jodconverter.document.DefaultDocumentFormatRegistry;
import org.artofsolving.jodconverter.document.DocumentFormat;
import org.artofsolving.jodconverter.office.OfficeConnectionProtocol;
import org.junit.Ignore;
import org.junit.Test;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.administration.commun.TypeFormat;
import fr.atexo.commun.document.generation.service.openoffice.bean.BeanParent;
import fr.atexo.commun.document.openoffice.OpenOfficeDocumentConverteur;
import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;

/**
 * Commentaire
 * 
 * @version $Revision$ $Date$
 */
@Ignore
public class OpenOfficeGenerateurTest extends TestCase {

	private static final Log LOG = LogFactory.getLog(OpenOfficeGenerateurTest.class);

	String commandeServiceRelance = "/home/atexo/soffice.sh";

	private static String HOST = "localhost";
	private static String PORT = "8100";

	private static String URI_TEMPLATE_INSERTION_IMAGE_CLASSPATH = "fr/atexo/commun/document/generation/service/openoffice/templates/insertionImage.odt";

	private static String URL_IMAGE_1 = "http://www.google.fr/logos/2012/Rodin-2012-homepage.png";
	private static String URL_IMAGE_2 = "http://www.google.com/logos/2012/vets_day-12-hp.jpg";

	private static String NOM_CHAMPFUSION_IMAGE1 = "insertionImage1";
	private static String NOM_CHAMPFUSION_IMAGE2 = "insertionImage2";

	private static String URI_TEMPLATE_INSERTION_TABLEAU_CLASSPATH = "fr/atexo/commun/document/generation/service/openoffice/templates/insertionTableau.odt";
	private static String URI_XML_INSERTION_TABLEAU_CLASSPATH = "fr/atexo/commun/document/generation/service/openoffice/odt-insertion-tableau.xml";
	private static String NOM_CHAMPFUSION_TABLEAU1 = "insertionTableau1";

	private static String URI_TEMPLATE_CLASSPATH = "fr/atexo/commun/document/generation/service/openoffice/templates/creerConnexionOpenOffice.odt";
	private static String URI_EXCEL = "fr/atexo/commun/document/generation/service/openoffice/A2_PRC_Parcours_Agrement_04_04_2012.xls";
	private static String NOM_FICHIER_GENERE = "test";
	private static String NOM_TEMPLATE_TEMP = "test_unitaire_module_document_generation";
	private static String NOM_REPERTOIRE_TRAVAIL = "repertoireTravail";
	private static String NOM_VARIABLE_EXEMPLE = "exempleVariable";

	private static String URI_TEMPLATE_REMU_CLASSPATH = "fr/atexo/commun/document/generation/service/openoffice/templates/DCP_CRHN.odt";
	private static String NOM_VARIABLE_REMU_EXEMPLE = "TypeRemu";

	@Test
	public void testConversionPdf() throws IOException {

		OpenOfficeDocumentConverteur openOfficeDocumentConverteur = new OpenOfficeDocumentConverteur(OfficeConnectionProtocol.SOCKET, true, Integer.valueOf(PORT));

		URL urlDocumentSource = ClasspathUtil.searchURL(URI_EXCEL);
		File documentSource = new File(urlDocumentSource.getPath());

		File pdfFile = FichierTemporaireUtil.creerFichierTemporaire("A2_PRC_Parcours_Agrement_04_04_2012", ".pdf");

		DefaultDocumentFormatRegistry format = new DefaultDocumentFormatRegistry();
		DocumentFormat formatPdf = format.getFormatByExtension("pdf");

		try {
			openOfficeDocumentConverteur.convertirDocument(documentSource, pdfFile, formatPdf);
			assertTrue(pdfFile.exists() && pdfFile.length() > 0);
			pdfFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private File getTemplateODT(String uriTemplate) throws IOException {

		InputStream is = null;
		OutputStream os = null;

		try {
			// Récuperation de la template odt du classpath
			is = ClasspathUtil.searchResource(uriTemplate);

			// Copie de la template vers le FileSystem autant que fichier
			// "temporaire"
			File templateDestination = FichierTemporaireUtil.creerFichierTemporaire(NOM_TEMPLATE_TEMP, ".odt");
			os = new FileOutputStream(templateDestination);
			IOUtils.copy(is, os);

			return templateDestination;

		} finally {
			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
	}

	private void genererDocument(String uriTemplate, DocumentDTO documentDTO, Map<String, Object> map, boolean desactiverTransformations) {

		try {

			// Copie de la template vers le FileSystem autant que fichier
			// "temporaire"
			File templateDestination = getTemplateODT(uriTemplate);

			// Définition du repertoire de travail
			File repertoireTravail = FichierTemporaireUtil.getRepertoireTemporaire(NOM_REPERTOIRE_TRAVAIL, true);

			// Création d'une instance du service OpenOffice
			OpenOfficeService openOfficeService = new OpenOfficeService(HOST, PORT, repertoireTravail.getPath(), desactiverTransformations, commandeServiceRelance);

			if (map != null && !map.isEmpty()) {
				openOfficeService.setFreeMarkerMapPourGeneration(map);
			}

			// Appel de la méthode genererDocument pour récuperer l'inputStream
			// vers le fichier généré
			InputStream inputStream = openOfficeService.genererDocument(documentDTO, new FichierDTO(templateDestination));

			assertTrue(inputStream.available() > 0);

			if (templateDestination.canWrite()) {
				if (templateDestination.delete()) {
					assertTrue(!templateDestination.exists());
				}
			}

			openOfficeService.supprimerFichierTemporaire();

			if (repertoireTravail.delete()) {
				assertTrue(!repertoireTravail.exists());
			}

		} catch (Exception e) {
			LOG.error(e.fillInStackTrace());
			assertTrue(false);
		}
	}

	@Test
	public void testInsererTableauDocumentOpenOffice() {

		// Récuperation d'une instance du DocumentModeleDTO pour la template
		DocumentModeleDTO documentModele = DocumentModeleDTO.getInstancePourGenerationDocument(TypeFormat.PDF);
		documentModele.setTypeDocument(TypeDocumentModele.Document);

		List<ChampFusionDTO> champFusions = new ArrayList<ChampFusionDTO>();
		ChampFusionDTO champFusionTableau1 = new ChampFusionDTO();

		String codeXML = null;
		try {
			codeXML = IOUtils.toString(ClasspathUtil.searchResource(URI_XML_INSERTION_TABLEAU_CLASSPATH));
		} catch (IOException e) {
			fail();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("foo", "bar");

		champFusionTableau1.setGenerationAttributes(NOM_CHAMPFUSION_TABLEAU1, TypeChamp.SCRIPT_TABLEAU, null, codeXML);
		champFusions.add(champFusionTableau1);

		DocumentDTO documentDTO = new DocumentDTO(documentModele, NOM_FICHIER_GENERE + new Date().getTime(), champFusions);

		genererDocument(URI_TEMPLATE_INSERTION_TABLEAU_CLASSPATH, documentDTO, map, false);
	}

	@Test
	public void InsererImageDocumentOpenOffice() {

		// Récuperation d'une instance du DocumentModeleDTO pour la template
		DocumentModeleDTO documentModele = DocumentModeleDTO.getInstancePourGenerationDocument(TypeFormat.PDF);
		documentModele.setTypeDocument(TypeDocumentModele.Document);

		List<ChampFusionDTO> champFusions = new ArrayList<ChampFusionDTO>();
		ChampFusionDTO champFusionImage1 = new ChampFusionDTO();
		champFusionImage1.setGenerationAttributes(NOM_CHAMPFUSION_IMAGE1, TypeChamp.URL_IMG, URL_IMAGE_1, null);
		champFusions.add(champFusionImage1);

		ChampFusionDTO champFusionImage2 = new ChampFusionDTO();
		champFusionImage2.setGenerationAttributes(NOM_CHAMPFUSION_IMAGE2, TypeChamp.URL_IMG, URL_IMAGE_2, null);
		champFusions.add(champFusionImage2);

		// Création d'un documentDTO qui n'aura pas de champsFusion
		DocumentDTO documentDTO = new DocumentDTO(documentModele, NOM_FICHIER_GENERE + new Date().getTime(), champFusions);

		genererDocument(URI_TEMPLATE_INSERTION_IMAGE_CLASSPATH, documentDTO, null, false);
	}

	@Test
	public void testCreerDocumentOpenOffice() {

		// Récuperation d'une instance du DocumentModeleDTO pour la template
		DocumentModeleDTO documentModele = DocumentModeleDTO.getInstancePourGenerationDocument(TypeFormat.PDF);
		documentModele.setTypeDocument(TypeDocumentModele.Document);

		// Création d'un documentDTO qui n'aura pas de champsFusion
		DocumentDTO documentDTO = new DocumentDTO(documentModele, NOM_FICHIER_GENERE + new Date().getTime(), getChampFusionTestGeneration());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beanParent", BeanParent.getExempleInstance());

		genererDocument(URI_TEMPLATE_CLASSPATH, documentDTO, map, false);

	}

	@Test
	public void creerDocumentRemuRS1OpenOffice() {
		creerDocumentRemuOpenOffice("RS1");
	}

	@Test
	public void creerDocumentRemuP2SOpenOffice() {
		creerDocumentRemuOpenOffice("P2S");
	}

	private void creerDocumentRemuOpenOffice(String valeur) {

		// Récuperation d'une instance du DocumentModeleDTO pour la template
		DocumentModeleDTO documentModele = DocumentModeleDTO.getInstancePourGenerationDocument(TypeFormat.PDF);

		ChampFusionDTO cf1 = new ChampFusionDTO();
		cf1.setGenerationAttributes(NOM_VARIABLE_REMU_EXEMPLE, null, valeur, null);
		ChampFusionDTO[] champs = new ChampFusionDTO[] { cf1 };
		// Création d'un documentDTO qui n'aura pas de champsFusion
		DocumentDTO documentDTO = new DocumentDTO(documentModele, NOM_FICHIER_GENERE + new Date().getTime(), Arrays.asList(champs));

		genererDocument(URI_TEMPLATE_REMU_CLASSPATH, documentDTO, null, false);
	}

	public static List<ChampFusionDTO> getChampFusionTestGeneration() {

		// champs de fusion simple
		ChampFusionDTO cf1 = new ChampFusionDTO();
		cf1.setGenerationAttributes("string", null, "String", null);

		ChampFusionDTO cf2 = new ChampFusionDTO();
		cf2.setGenerationAttributes("time", null, new Time(5, 10, 30), null);

		ChampFusionDTO cf3 = new ChampFusionDTO();
		cf3.setGenerationAttributes("date", null, new Date(), null);

		ChampFusionDTO cf4 = new ChampFusionDTO();
		cf4.setGenerationAttributes("long", null, 5L, null);

		ChampFusionDTO cf5 = new ChampFusionDTO();
		cf5.setGenerationAttributes("double", null, 7D, null);

		ChampFusionDTO cf6 = new ChampFusionDTO();
		cf6.setGenerationAttributes("float", null, new Float(15), null);

		ChampFusionDTO cf7 = new ChampFusionDTO();
		cf7.setGenerationAttributes("enum", null, TypeFormat.PDF, null);

		ChampFusionDTO cf8 = new ChampFusionDTO();
		cf8.setGenerationAttributes("object", null, new Object(), null);

		ChampFusionDTO cf10 = new ChampFusionDTO();
		cf10.setGenerationAttributes("cf_monsieur", null, "Monsieur", null);

		ChampFusionDTO cf11 = new ChampFusionDTO();
		cf11.setGenerationAttributes("cf_garcon", null, "Garçon", null);

		ChampFusionDTO cf12 = new ChampFusionDTO();
		cf12.setGenerationAttributes(NOM_VARIABLE_EXEMPLE, null, "Valeur insérée test unitaire", null);

		// champs complexes
		ChampFusionDTO cf9 = new ChampFusionDTO();
		cf9.setGenerationAttributes("freemarker_script", TypeChamp.SCRIPT, null, FreeMarkerMailGenerateurTest.getTestScriptFreeMarker());

		ChampFusionDTO cf13 = new ChampFusionDTO();
		cf13.setGenerationAttributes("insertion_01_exemple_xml", TypeChamp.SCRIPT_OOO, null, FreeMarkerMailGenerateurTest.getTestScriptFreeMarkerXmlOoo());

		ChampFusionDTO cf14 = new ChampFusionDTO();
		cf14.setGenerationAttributes("insertion_02_exemple_xml", TypeChamp.SCRIPT_OOO, null, FreeMarkerMailGenerateurTest.getTestScriptFreeMarkerXmlOoo());

		ChampFusionDTO cf15 = new ChampFusionDTO();
		cf15.setGenerationAttributes("insertion_03_exemple_xml", TypeChamp.SCRIPT_OOO, null, FreeMarkerMailGenerateurTest.getTestScriptFreeMarkerXmlOoo());

		ChampFusionDTO cf16 = new ChampFusionDTO();
		cf16.setGenerationAttributes("insertionTableau_01", TypeChamp.SCRIPT_OOO, null, FreeMarkerMailGenerateurTest.getTestScriptFreeMarkerXmlOoo());

		ChampFusionDTO[] champs = new ChampFusionDTO[] { cf1, cf2, cf3, cf4, cf5, cf6, cf7, cf8, cf9, cf10, cf11, cf12, cf13, cf14, cf15, cf16 };

		return Arrays.asList(champs);
	}

	@Test
	public void testEscapeXML() {
		String xml = "<text:list xml:id=\"list32233225\" text:style-name=\"Numbering_20_1\"><text:list-item><text:h text:style-name=\"Onglet\" text:outline-level=\"1\">Insertion depuis XML</text:h></text:list-item></text:list>";
		String xmlEscape = StringEscapeUtils.escapeXml(xml);
		String xmlUnescape = StringEscapeUtils.unescapeXml(xmlEscape);
		assertTrue(xml.equals(xmlUnescape));
	}
}
