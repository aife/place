package fr.atexo.commun.document.generation.service.openoffice;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;
import fr.atexo.commun.document.administration.commun.TypeDonnee;
import fr.atexo.commun.document.administration.commun.util.XMLUtil;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class XMLUtilTest {

    @Test
    public void testGenerationXML() {
        List<ChampFusionDTO> champFusions = getXMLCodeExemple();
        String codeXML = XMLUtil.getCodeFreeMarkerXML("Dossiers", 1, champFusions, true);
        assert codeXML != null;
    }

    public static List<ChampFusionDTO> getXMLCodeExemple() {

        List<ChampFusionDTO> champFusions = new ArrayList<ChampFusionDTO>();

        long idDomaine = 1L;
        String valeurIdDomaine = String.valueOf(idDomaine);

        ChampFusionDTO champDispositif = new ChampFusionDTO();
        champDispositif.setAdministrationAttributes("nom",
                "Nom du dispositif",
                TypeChamp.OBJECT,
                null,
                "dossier",
                "dossier",
                "Domaine Dossier",
                1,
                valeurIdDomaine);
        champDispositif.setPath("nom");
        champDispositif.setTypeDonnee(TypeDonnee.STRING);
        champFusions.add(champDispositif);

        ChampFusionDTO champMontantDemande = new ChampFusionDTO();
        champMontantDemande.setAdministrationAttributes("montantDemande",
                "Montant demandé",
                TypeChamp.OBJECT,
                null,
                "dossier",
                "dossier",
                "Domaine Dossier",
                2,
                valeurIdDomaine);
        champMontantDemande.setPath("montantDemande");
        champMontantDemande.setTypeDonnee(TypeDonnee.DOUBLE);
        champFusions.add(champMontantDemande);

        ChampFusionDTO champMontantPropose = new ChampFusionDTO();
        champMontantPropose.setAdministrationAttributes("montantPropose",
                "Montant proposé",
                TypeChamp.OBJECT,
                null,
                "dossier",
                "dossier",
                "Domaine Dossier",
                3,
                valeurIdDomaine);
        champMontantPropose.setPath("montantPropose");
        champMontantPropose.setTypeDonnee(TypeDonnee.DOUBLE);
        champFusions.add(champMontantPropose);

        ChampFusionDTO champDateDepot = new ChampFusionDTO();
        champDateDepot.setAdministrationAttributes("dateDepot",
                "Date de dépôt",
                TypeChamp.OBJECT,
                null,
                "dossier",
                "dossier",
                "Domaine Dossier",
                4,
                valeurIdDomaine);
        champDateDepot.setPath("dateDepot");
        champDateDepot.setTypeDonnee(TypeDonnee.DATE);
        champFusions.add(champDateDepot);

        ChampFusionDTO champActif = new ChampFusionDTO();        
        champActif.setAdministrationAttributes("actif",
                "Actif",
                TypeChamp.OBJECT,
                null,
                "dossier",
                "dossier",
                "Domaine Dossier",
                5,
                valeurIdDomaine);
        champActif.setPath("actif");
        champActif.setTypeDonnee(TypeDonnee.BOOLEAN);
        champFusions.add(champActif);

        return champFusions;
    }
}
