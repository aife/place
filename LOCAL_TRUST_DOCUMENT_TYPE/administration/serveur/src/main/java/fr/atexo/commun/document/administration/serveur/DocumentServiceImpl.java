/**
 *
 */
package fr.atexo.commun.document.administration.serveur;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.atexo.commun.document.administration.client.interfaces.DocumentAdministrationService;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.ConstantesDocument;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO.TypeOperateur;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.DomaineDTO;
import fr.atexo.commun.document.administration.commun.EtatDTO;
import fr.atexo.commun.document.administration.commun.GenerationRequeteLibreAsyncStatus;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;
import fr.atexo.commun.document.administration.commun.ProfilDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.administration.commun.TypeDonnee;
import fr.atexo.commun.document.administration.commun.TypeFormat;
import fr.atexo.commun.document.administration.serveur.servlet.DownloadServletUtil;
import fr.atexo.gwt.ui.outils.commun.CleValeur;
import fr.atexo.gwt.ui.outils.commun.ICleValeur;

/**
 * @author Léon Barsamian
 */
public class DocumentServiceImpl extends RemoteServiceServlet implements DocumentAdministrationService {

  /**
   * Serialisation.
   */
  private static final long serialVersionUID = -8313478239227249507L;

  /**
   * Logger.
   */
  private static Log log = LogFactory.getLog(DocumentServiceImpl.class);

  /**
   * Initialisaton de l'écran.
   */
  public InitialisationDTO initDocumentModele(Map<String, List<String>> parameters) {
    InitialisationDTO donnees = new InitialisationDTO();
    donnees.setActiverGenerationDocumentRequeteLibre(true);
    donnees.setActiverMail(true);
    donnees.setActiverDocumentExcel(true);
    donnees.setActiverAssociation(true);
    donnees.setActiverDomaineChampFusionComplexe(true);
    List<TypeFormat> formatODT = new ArrayList<TypeFormat>();
    formatODT.add(TypeFormat.MSWORD);
    formatODT.add(TypeFormat.ODT);
    formatODT.add(TypeFormat.PDF);
    donnees.setListeFormatPourGenerationODT(formatODT);
    List<TypeFormat> formatXLS = new ArrayList<TypeFormat>();
    formatXLS.add(TypeFormat.EXCEL);
    formatXLS.add(TypeFormat.PDF);
    donnees.setListeFormatPourGenerationXLS(formatXLS);

    // creation du document
    DocumentModeleDTO documentModeleDTO = new DocumentModeleDTO();
    documentModeleDTO.setId(0L);
    documentModeleDTO.setNom("Lettre de refus");
    documentModeleDTO.setNomTemplate("nom du template");
    documentModeleDTO.setTypeDocument(TypeDocumentModele.Excel);
    documentModeleDTO.setContenuCourriel("&lt;# freemarker&gt;<br /><br />${azezaezae}<br />");
    documentModeleDTO.setChampFusionsSelectionnes(new ArrayList<ChampFusionDTO>());
    List<ProfilDTO> listeProfils = new ArrayList<ProfilDTO>();
    ProfilDTO profil = new ProfilDTO();
    profil.setCode("001");
    profil.setLibelle("Profil Gestionnaire");
    listeProfils.add(profil);
    profil = new ProfilDTO();
    profil.setCode("002");
    profil.setLibelle("Profil Intervenant");
    listeProfils.add(profil);
    documentModeleDTO.setProfilsDisponibles(listeProfils);

    List<ProfilDTO> profilsSelectionnes = new ArrayList<ProfilDTO>();
    profil = new ProfilDTO();
    profil.setCode("003");
    profil.setLibelle("Profil Vision");
    profilsSelectionnes.add(profil);
    documentModeleDTO.setProfilsSelectionnes(profilsSelectionnes);
    documentModeleDTO.setTypeFormat(TypeFormat.PDF);

    List<EtatDTO> etatsDisponible = new ArrayList<EtatDTO>();
    EtatDTO etat = new EtatDTO();
    etat.setCode("E1");
    etat.setLibelle("Etat Définition");
    etatsDisponible.add(etat);
    etat = new EtatDTO();
    etat.setCode("E2");
    etat.setLibelle("Etat Confirmation");
    etatsDisponible.add(etat);

    etat = new EtatDTO();
    etat.setCode("E2");
    etat.setLibelle("Etat Confirmation");
    etatsDisponible.add(etat);

    List<EtatDTO> etatsSelectionne = new ArrayList<EtatDTO>();
    etat = new EtatDTO();
    etat.setCode("E3");
    etat.setLibelle("Etat Validation");
    etatsSelectionne.add(etat);
    documentModeleDTO.setEtapesDisponibles(etatsDisponible);
    documentModeleDTO.setEtapesSelectionnees(etatsSelectionne);
    donnees.setDocumentModeleDTO(documentModeleDTO);

    // creation des domaines
    DomaineDTO domaineRacine = new DomaineDTO();

    domaineRacine.setNom("DomaineRacine");

    List<DomaineDTO> listeDomaine = new ArrayList<DomaineDTO>();
    for (int i = 0; i < 5; i++) {
      DomaineDTO domaine = new DomaineDTO();
      domaine.setId(i);
      domaine.setNom("Domaine " + i);
      domaine.setDataFusionDTOs(genereListeChampsFusionDTO(domaine.getNom(), 3, TypeChamp.OBJECT));
      listeDomaine.add(domaine);
      List<DomaineDTO> liste = genereListeSousDomaineDTO(domaine.getNom(), 3);
      listeDomaine.addAll(liste);

    }

    List<ChampFusionDTO> complexe = genereListeChampsFusionDTO("Domaine Test", 3, TypeChamp.SCRIPT);
    documentModeleDTO.getChampFusionsSelectionnes().addAll(complexe);
    List<ChampFusionDTO> simples = genereListeChampsFusionDTO("Test", 3, null);
    documentModeleDTO.getChampFusionsSelectionnes().addAll(simples);
    donnees.setDomainesDTO(listeDomaine);

    // commission sub
    // List<ItemSelectBox> listeSelectionExcel = new ArrayList<ItemSelectBox>();
    // ItemSelectBox commission1 = new ItemSelectBox();
    // commission1.setCleLibelle("commission1libelle");
    // commission1.setCode("01");
    // commission1.setOptionSelectionnee(true);
    // listeSelectionExcel.add(commission1);
    // ItemSelectBox commission2 = new ItemSelectBox();
    // commission2.setCleLibelle("commission2libelle");
    // commission2.setCode("02");
    // commission2.setOptionSelectionnee(false);
    // listeSelectionExcel.add(commission2);
    // ItemSelectBox commission3 = new ItemSelectBox();
    // commission3.setCleLibelle("commission3libelle");
    // commission3.setCode("03");
    // // listeSelectionExcel.add(commission3);
    // donnees.setListeSelectionExcel(listeSelectionExcel);
    //
    // List<EtatDTO> commissionDisponible = new ArrayList<EtatDTO>();
    // EtatDTO commission = new EtatDTO();
    // commission.setCode("C1");
    // commission.setLibelle("commission 1");
    // commissionDisponible.add(commission);
    // commission = new EtatDTO();
    // commission.setCode("C2");
    // commission.setLibelle("commission 2");
    // commissionDisponible.add(commission);
    //
    // commission = new EtatDTO();
    // commission.setCode("C3");
    // commission.setLibelle("commission 3");
    // commissionDisponible.add(commission);
    // documentModeleDTO.setCommissionsDisponibles(commissionDisponible);
    //
    // List<EtatDTO> commissionSelectionne = new ArrayList<EtatDTO>();
    // commission = new EtatDTO();
    // commission.setCode("C4");
    // commission.setLibelle("commission 4");
    // commissionSelectionne.add(commission);
    // documentModeleDTO.setCommissionsSelectionnees(commissionSelectionne);
    //
    //
    // Map<String, List<String>> mapDeversoire = new HashMap<String, List<String>>();
    // List<String> liste1 = new ArrayList<String>();
    // liste1.add(ConstantesDocument.DEVERSOIRE_COMMISSION);
    // mapDeversoire.put("01", liste1);
    // List<String> liste2 = new ArrayList<String>();
    // liste2.add(ConstantesDocument.DEVERSOIRE_DISPOSITIF);
    // mapDeversoire.put("02", liste2);
    // List<String> liste3 = new ArrayList<String>();
    // liste2.add(ConstantesDocument.DEVERSOIRE_PROFILS);
    // mapDeversoire.put("03", liste3);
    // donnees.setDeversoireAfficheParOption(mapDeversoire);
    List<String> listeDeversoireDocument = new ArrayList<String>();
    listeDeversoireDocument.add(ConstantesDocument.DEVERSOIRE_DISPOSITIF);
    listeDeversoireDocument.add(ConstantesDocument.DEVERSOIRE_PROFILS);
    donnees.setDeversoirePourDocument(listeDeversoireDocument);
    documentModeleDTO.setXml("XML");



    donnees.setActiverGestionCriteresAffichage(true);

    List<DocumentModeleCritereDTO> criteres = new ArrayList<DocumentModeleCritereDTO>();

    DocumentModeleCritereDTO critere = new DocumentModeleCritereDTO();
    critere.setNom("champ texte");
    critere.setTypeDonnee(TypeDonnee.TEXT);
    critere.setTypeOperateur(TypeOperateur.EGAL);
    critere.setValeurCritere("valeur texte");
    criteres.add(critere);

    critere = new DocumentModeleCritereDTO();
    critere.setNom("champ date");
    critere.setTypeDonnee(TypeDonnee.DATE);
    critere.setTypeOperateur(TypeOperateur.INFERIEUR);
    critere.setValeurCritere("12/12/2014");
    criteres.add(critere);


    critere = new DocumentModeleCritereDTO();
    critere.setNom("champ boolean");
    critere.setTypeDonnee(TypeDonnee.BOOLEAN);
    critere.setTypeOperateur(TypeOperateur.EGAL);
    critere.setValeurCritere("false");
    criteres.add(critere);

    critere = new DocumentModeleCritereDTO();
    critere.setNom("code1");
    critere.setTypeDonnee(TypeDonnee.REFERENTIEL);
    critere.setTypeOperateur(TypeOperateur.DIFFERENT);
    List<ICleValeur> refs = new ArrayList<ICleValeur>();
    refs.add(new CleValeur("1", "pomme"));
    refs.add(new CleValeur("2", "banane"));
    critere.setReferentielListeValeurs(refs);
    critere.setValeurCritere("2");
    criteres.add(critere);


    documentModeleDTO.setCriteres(criteres);

    return donnees;
  }

  private List<ChampFusionDTO> genereListeChampsFusionDTO(final String nomDomaine, final int nombre, final TypeChamp type) {
    List<ChampFusionDTO> resultat = new ArrayList<ChampFusionDTO>();
    for (int i = 0; i < nombre; i++) {
      ChampFusionDTO champ = new ChampFusionDTO();
      champ.setTypeDonnee(TypeDonnee.TEXT);
      champ.setAdministrationAttributes("Champ de fusion du domaine " + nombre + "_" + i, "description du champ", type, "", nomDomaine, nomDomaine, nomDomaine, 0, "0");
      resultat.add(champ);
    }

    return resultat;
  }

  private List<DomaineDTO> genereListeSousDomaineDTO(final String nomDomaine, final int nombre) {
    List<DomaineDTO> resultat = new ArrayList<DomaineDTO>();
    for (int i = 0; i < nombre; i++) {
      DomaineDTO domaine = new DomaineDTO();
      domaine.setId(i);
      domaine.setNom("Sous domaine n°" + i + "  de " + nomDomaine);
      domaine.setDataFusionDTOs(genereListeChampsFusionDTO(domaine.getNom(), 5, TypeChamp.OBJECT));
      resultat.add(domaine);
    }
    return resultat;
  }

  /**
   * Enregistrement d'un modéle de document avec le template
   * 
   * @param documentModeleDTO modele
   * @param tokenFichierSession token du template odt associé en session. peut être null dans le cas
   *        ou l'utilisateur modifie le modéle sans changer le template. Est null dans le cas d'un
   *        mail.
   */
  public void validerDocumentModele(DocumentModeleDTO documentModeleDTO, final String tokenFichierSession) {
    log.debug("contenu courriel : " + documentModeleDTO.getContenuCourriel());
    if (!documentModeleDTO.getTypeDocument().equals(TypeDocumentModele.Mail) && tokenFichierSession != null) {
      File fichier = (File) getThreadLocalRequest().getSession().getAttribute(tokenFichierSession);
      log.debug("Fichier : " + documentModeleDTO.getNomTemplate() + ", Taille : " + fichier.length());
      getThreadLocalRequest().getSession().removeAttribute(tokenFichierSession);
      fichier.delete();
    }
    log.debug(documentModeleDTO.toString());
  }


  public String genererDocumentRequeteLibre(DocumentModeleDTO documentModeleDTO, String retour) throws Exception {
    File tmp = File.createTempFile("test", ".xls");
    String nomFichier = "export.xls";
    getThreadLocalRequest().getSession().setAttribute(ConstantesDocument.CHEMIN_FICHIER_EXPORT, tmp.getPath());
    getThreadLocalRequest().getSession().setAttribute(ConstantesDocument.NOM_FICHIER_EXPORT, nomFichier);
    return "../recupererGenerationDocumentRequeteLibre.srv";

  }

  public String telechargerTemplate(Long idDocumentModele) throws Exception {
    File tmp = File.createTempFile("test", ".xls");
    String nomFichier = "template.xls";
    getThreadLocalRequest().getSession().setAttribute(ConstantesDocument.CHEMIN_FICHIER_DOWNLOAD, tmp.getPath());
    getThreadLocalRequest().getSession().setAttribute(ConstantesDocument.NOM_FICHIER_DOWNLOAD, nomFichier);
    getThreadLocalRequest().getSession().setAttribute(ConstantesDocument.TYPE_CONTENU_FICHIER_DOWNLOAD, DownloadServletUtil.CONTENT_TYPE_EXCEL);
    return "../recupererTemplate.srv";
  }

  @Override
  public String genererDocumentRequeteLibreAsync(DocumentModeleDTO documentModeleDTO, String tokenFichierSession) throws Exception {
    // TODO Auto-generated method stub
    return null;
  }


  @Override
  public GenerationRequeteLibreAsyncStatus recupererDocumentRequeteLibreAsync(String token) throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

}
