package fr.atexo.commun.document.administration.serveur.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;

/**
 * Permet de télécharger un fichier depuis des informations stocké dans la session HTTP.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class DownloadServletUtil {

    public final static String CONTENT_TYPE_EXCEL = "application/excel";

    public final static String CONTENT_TYPE_ODT = "application/vnd.oasis.opendocument.text";

    public static void downloadFichierExcel(HttpServletRequest request, HttpServletResponse response, String cheminFichierSessionConstante, String nomFichierSessionConstante) {
        downloadFichier(request, response, cheminFichierSessionConstante, nomFichierSessionConstante, CONTENT_TYPE_EXCEL);
    }

    public static void downloadFichierODT(HttpServletRequest request, HttpServletResponse response, String cheminFichierSessionConstante, String nomFichierSessionConstante) {
        downloadFichier(request, response, cheminFichierSessionConstante, nomFichierSessionConstante, CONTENT_TYPE_ODT);
    }

    public static void downloadFichier(HttpServletRequest request, HttpServletResponse response, String cheminFichierSessionConstante, String nomFichierSessionConstante, String typeContenu) {

        HttpSession session = request.getSession();
        String cheminFichier = (String) session.getAttribute(cheminFichierSessionConstante);
        String nomFichier = (String) session.getAttribute(nomFichierSessionConstante);

        if (cheminFichier != null && nomFichier != null && typeContenu != null) {
            // Outils
            response.setContentType(typeContenu);
            response.setHeader("Content-Disposition", "attachment; filename=\"" + nomFichier + "\"");

            FileInputStream inputStream;

            try {

                inputStream = new FileInputStream(cheminFichier);
                OutputStream fluxServlet = response.getOutputStream();
                IOUtils.copyLarge(inputStream, fluxServlet);
                IOUtils.closeQuietly(inputStream);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                session.removeAttribute(cheminFichierSessionConstante);
                session.removeAttribute(nomFichierSessionConstante);
                File fichier = new File(cheminFichier);
                if (fichier.exists() && fichier.isFile()) {
                    fichier.delete();
                }
            }
        }
    }

    public static void downloadFichierDepuisSession(HttpServletRequest request, HttpServletResponse response, String cheminFichierSessionConstante, String nomFichierSessionConstante, String typeContenuSessionConstante) {

        HttpSession session = request.getSession();
        String cheminFichier = (String) session.getAttribute(cheminFichierSessionConstante);
        String nomFichier = (String) session.getAttribute(nomFichierSessionConstante);
        String typeContenu = (String) session.getAttribute(typeContenuSessionConstante);

        if (cheminFichier != null && nomFichier != null && typeContenu != null) {
            // Outils
            response.setContentType(typeContenu);
            response.setHeader("Content-Disposition", "attachment; filename=\"" + nomFichier + "\"");

            FileInputStream inputStream;
            try {

                inputStream = new FileInputStream(cheminFichier);
                OutputStream fluxServlet = response.getOutputStream();
                IOUtils.copyLarge(inputStream, fluxServlet);
                IOUtils.closeQuietly(inputStream);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                session.removeAttribute(cheminFichierSessionConstante);
                session.removeAttribute(nomFichierSessionConstante);
                session.removeAttribute(typeContenuSessionConstante);
                File fichier = new File(cheminFichier);
                if (fichier.exists() && fichier.isFile()) {
                    fichier.delete();
                }
            }
        }
    }

}
