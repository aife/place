package fr.atexo.commun.document.administration.serveur.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.atexo.commun.document.administration.commun.ConstantesDocument;

/**
 * @author Léon Barsamian
 */
public class RecupereExportServlet extends HttpServlet {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 6104716716471482273L;


    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DownloadServletUtil.downloadFichierExcel(request, response, ConstantesDocument.CHEMIN_FICHIER_EXPORT, ConstantesDocument.NOM_FICHIER_EXPORT);
    }

}
