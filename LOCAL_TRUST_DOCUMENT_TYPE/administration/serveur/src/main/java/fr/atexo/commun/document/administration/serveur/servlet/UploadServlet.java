package fr.atexo.commun.document.administration.serveur.servlet;

import gwtupload.server.UploadAction;
import gwtupload.server.exceptions.UploadActionException;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

/**
 * Servlet pour recevoir les fichiers envoyé par la partie cliente de gwt.
 */
public class UploadServlet extends UploadAction {

    private static final long serialVersionUID = 1L;

    /**
     * Le chemin des fichiers sur le disque.
     */
    private static String cheminRacine;

    @Override
    protected FileItemFactory getFileItemFactory(int requestSize) {
        if (cheminRacine == null) {
            cheminRacine = System.getProperty("java.io.tmpdir");
        }
        DiskFileItemFactory diskFile = new DiskFileItemFactory();
        // on enregistre tous les fichiers sur le disque.
        diskFile.setSizeThreshold(0);

        diskFile.setRepository(new File(cheminRacine));
        return diskFile;
      }
    /**
     * Override executeAction to save the received files in a custom place and
     * delete this items from session.
     */
    @Override
    public String executeAction(HttpServletRequest request, List<FileItem> sessionFiles)
            throws UploadActionException {


        String timestamp = null;
        for (FileItem item : sessionFiles) {
            if (false == item.isFormField()) {

                try {
                    DiskFileItem diskFileItem = (DiskFileItem) item;
                    timestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
                    request.getSession().setAttribute(timestamp, diskFileItem.getStoreLocation());

                } catch (Exception e) {
                    throw new UploadActionException(e);
                }
            }
        }

        // / Send information of the received files to the client.
        return timestamp;
    }

    /**
     * @param cheminRacine the cheminRacine to set
     */
    public void setCheminRacine(final String cheminRacine) {
        UploadServlet.cheminRacine = cheminRacine;
    }
}
