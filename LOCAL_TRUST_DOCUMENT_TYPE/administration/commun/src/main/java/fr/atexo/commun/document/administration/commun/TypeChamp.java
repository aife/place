/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

/**
 * bean DTO contenant les informations d'un Profil qui sera lié à un
 * DocumentModele
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
public enum TypeChamp {

	OBJECT, SCRIPT, SCRIPT_OOO, URL_IMG, SCRIPT_TABLEAU;
}
