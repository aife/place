package fr.atexo.commun.document.administration.commun;

import java.util.EnumSet;

import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO.TypeOperateur;

/**
 * L'ensemble des types de données supporté par la fusion de données.
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public enum TypeDonnee {

	STRING("Texte", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	TEXT("Texte long", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	LIST("Liste d'éléments", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	DATE("Date", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE,
			TypeOperateur.INFERIEUR, TypeOperateur.INFERIEUR_OU_EGAL, TypeOperateur.SUPERIEUR, TypeOperateur.SUPERIEUR_OU_EGAL)),
	DATE_HEURE("Date_HEURE", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE,
			TypeOperateur.INFERIEUR, TypeOperateur.INFERIEUR_OU_EGAL, TypeOperateur.SUPERIEUR, TypeOperateur.SUPERIEUR_OU_EGAL)),
	BOOLEAN("Booléen", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	LONG("Entier", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE,
			TypeOperateur.INFERIEUR, TypeOperateur.INFERIEUR_OU_EGAL, TypeOperateur.SUPERIEUR, TypeOperateur.SUPERIEUR_OU_EGAL)),
	DOUBLE("Décimal", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE,
			TypeOperateur.INFERIEUR, TypeOperateur.INFERIEUR_OU_EGAL, TypeOperateur.SUPERIEUR, TypeOperateur.SUPERIEUR_OU_EGAL)),
	RIB("RIB", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	ADRESSE("Adresse", EnumSet.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	FICHIER("Fichier"),
	OBJECT("Objet"),
	ENUM("Enumeration"),
	REFERENTIEL("Référentiel", EnumSet
			.of(TypeOperateur.EGAL, TypeOperateur.DIFFERENT, TypeOperateur.VIDE, TypeOperateur.NON_VIDE)),
	URL_IMAGE("Url d'une image");

	private String label;

	EnumSet<TypeOperateur> operateursApplicables;

	public EnumSet<TypeOperateur> getOperateursApplicables() {
		return operateursApplicables;
	}

	private TypeDonnee(String label, EnumSet<TypeOperateur> operateursApplicables) {
		this.label = label;
		this.operateursApplicables = operateursApplicables;
	}

	TypeDonnee(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
