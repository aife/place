package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.atexo.commun.document.administration.commun.util.FormatageUtil;

/**
 * bean DTO utilisé pour l'initialisation de l'application GWT. Ce bean contient
 * l'ensemble des proprietés nécessaires à l'initialisation de l'application.
 * 
 * @author RME
 */
public class InitialisationDTO implements Serializable {

	private static final long serialVersionUID = -4537175257447554601L;

	private DocumentModeleDTO documentModeleDTO;

	/**
	 * Liste de domaines proposés par l'application GWT
	 */
	private List<DomaineDTO> DomainesDTO;

	/**
	 * Affiche ou cache le radiobouton de selection du document. Par defaut
	 * true.
	 */
	private boolean activerDocument = true;

	/**
	 * Affiche ou cache le radiobouton de selection du mail. Par defaut true.
	 */
	private boolean activerMail = true;

	/**
	 * Active ou non la gestion des requêtes libres (génération de fichier
	 * excel). Par defaut true.
	 */
	private boolean activerRequeteLibre = true;

	/**
	 * Active ou non la gestion des documents excel. Par defaut true.
	 */
	private boolean activerDocumentExcel = true;

	/**
	 * Affiche ou cache le bouton de génération excel depuis une requête libre.
	 * Par defaut true.
	 */
	private boolean activerGenerationDocumentRequeteLibre = false;

	/**
	 * Active la génération asynchrone des requêtes libres
	 */
	private boolean activerGenerationAsynchroneRequeteLibre = false;

	/**
	 * Active ou désactive la vérification de la nullité des champs lors de la
	 * génération du code XML lors de l'enregistrement d'une requête libre ou le
	 * passage d'une requête libre à une requête avancée. Par defaut true.
	 */
	private boolean activerVerificationChampsNulsGenerationCodeXML = true;

	/**
	 * Affiche ou cache le bouton de validation. Par defaut true.
	 */
	private boolean activerValidation = true;

	/**
	 * Active 2 radios bouttons retournant une valeur true / false. Par defaut
	 * false.
	 */
	private boolean activerAssociation = false;

	/**
	 * Active ou non la gestion des domaines des champs de fusion complexe. Par
	 * defaut false.
	 */
	private boolean activerDomaineChampFusionComplexe = false;

	/**
	 * Active ou non la gestion des types de domaines pour les champs de fusion
	 * simple. Par defaut true.
	 */
	private boolean activerTypeDomaineChampFusionSimple = true;

	/**
	 * Active ou non la gestion des types de domaines pour les champs de fusion
	 * complexe. Par defaut false.
	 */
	private boolean activerTypeDomaineChampFusionComplexe = false;

	/**
	 * Active ou non la possibilité de télécharger le template. Par defaut
	 * false.
	 */
	private boolean activerTelechargementTemplate = false;

	/**
	 * Active ou non la possibilité de définir un template d'un email avec un
	 * éditeur HTML.
	 */
	private boolean activerTemplateMailHTML = true;

	/**
   * 
   */
	private boolean activerGestionVisibliteService = true;

	/**
   * 
   */
	private boolean activerGestionCriteresAffichage = false;

	/**
	 * Liste des options à afficher dans le cas d'un fichier excel.
	 */
	private List<ItemSelectBox> listeSelectionExcel = null;

	/**
	 * Association entre option dans le cas d'un fichier excel et la liste de
	 * deversoires à afficher. Les deversoires sont identifés via les constantes
	 * du fichier ConstantesDocument.java.
	 */
	private Map<String, List<String>> deversoireAfficheParOption = null;

	/**
	 * Liste des déversoires associé à un document.
	 */
	private List<String> deversoirePourDocument = new ArrayList<String>();

	/**
	 * Liste des formats affiché pour la génération d'un document ODT.
	 */
	private List<TypeFormat> listeFormatPourGenerationODT;

	/**
	 * Liste des formats affiché pour la génération d'un document XLS.
	 */
	private List<TypeFormat> listeFormatPourGenerationXLS;

	/**
	 * Pattern par défaut à appliquer pour les champs de type date lors de la
	 * génération automatique du code XML pour les requêtes libres
	 */
	private String formatDateFreemarker = FormatageUtil.FORMAT_DATE_FREEMARKER;

	/**
	 * Pattern par défaut à appliquer pour les champs de type date lors de la
	 * génération automatique du code XML pour les requêtes libres
	 */
	private String formatDateTimeFreemarker = FormatageUtil.FORMAT_DATE_HEURE_FREEMARKER;

	/**
	 * Pattern par défaut à appliquer pour les champs de type valeur numérique
	 * lors de la génération automatique du code XML pour les requêtes libres
	 */
	private String formatNumeriqueCodeXML = FormatageUtil.FORMAT_NUMERIQUE_CODE_XML;

	/**
	 * Affiche ou cache le radiobouton de selection du mail.
	 * 
	 * @return true si affiché.
	 */
	public final boolean isActiverMail() {
		return activerMail;
	}

	/**
	 * Affiche ou cache le radiobouton de selection du mail.
	 * 
	 * @param valeur
	 *            si true, affiché.
	 */
	public final void setActiverMail(final boolean valeur) {
		this.activerMail = valeur;
	}

	/**
	 * Affiche ou cache le bouton de génération d'un fichier excel pour les
	 * requêtes libres.
	 * 
	 * @return true si affiché
	 */
	public final boolean isActiverGenerationDocumentRequeteLibre() {
		return activerGenerationDocumentRequeteLibre;
	}

	/**
	 * @return
	 */
	public boolean isActiverGenerationAsynchroneRequeteLibre() {
		return activerGenerationAsynchroneRequeteLibre;
	}

	/**
	 * @param activerGenerationAsynchroneRequeteLibre
	 */
	public void setActiverGenerationAsynchroneRequeteLibre(boolean activerGenerationAsynchroneRequeteLibre) {
		this.activerGenerationAsynchroneRequeteLibre = activerGenerationAsynchroneRequeteLibre;
	}

	/**
	 * Affiche ou cache le bouton de génération d'un fichier excel pour les
	 * requêtes libres.
	 * 
	 * @param valeur
	 *            si true, affiché
	 */
	public final void setActiverGenerationDocumentRequeteLibre(final boolean valeur) {
		this.activerGenerationDocumentRequeteLibre = valeur;
	}

	public DocumentModeleDTO getDocumentModeleDTO() {
		return documentModeleDTO;
	}

	public void setDocumentModeleDTO(DocumentModeleDTO documentModeleDTO) {
		this.documentModeleDTO = documentModeleDTO;
	}

	public List<DomaineDTO> getDomainesDTO() {
		return DomainesDTO;
	}

	public void setDomainesDTO(List<DomaineDTO> domainesDTO) {
		DomainesDTO = domainesDTO;
	}

	/**
	 * @return si true, Active 2 radios bouttons.
	 */
	public final boolean isActiverAssociation() {
		return activerAssociation;
	}

	/**
	 * @param activerAssociation
	 *            si true, Active 2 radios bouttons.
	 */
	public final void setActiverAssociation(final boolean activerAssociation) {
		this.activerAssociation = activerAssociation;
	}

	/**
	 * @return Active ou non la gestion des domaines des champs de fusion
	 *         complexe.
	 */
	public final boolean isActiverDomaineChampFusionComplexe() {
		return activerDomaineChampFusionComplexe;
	}

	/**
	 * @param valeur
	 *            Active ou non la gestion des domaines des champs de fusion
	 *            complexe.
	 */
	public final void setActiverDomaineChampFusionComplexe(boolean valeur) {
		this.activerDomaineChampFusionComplexe = valeur;
	}

	/**
	 * @return Active ou non la gestion des requetes libres. Par defaut true.
	 */
	public boolean isActiverRequeteLibre() {
		return activerRequeteLibre;
	}

	/**
	 * @param valeur
	 *            Active ou non la gestion des requetes libres. Par defaut true.
	 */
	public void setActiverRequeteLibre(boolean valeur) {
		this.activerRequeteLibre = valeur;
	}

	/**
	 * @return Active ou non la gestion des documents. Par defaut true.
	 */
	public final boolean isActiverDocument() {
		return activerDocument;
	}

	/**
	 * @param valeur
	 *            Active ou non la gestion des documents. Par defaut true.
	 */
	public final void setActiverDocument(boolean valeur) {
		this.activerDocument = valeur;
	}

	/**
	 * @return Active ou non la gestion des documents excel. Par defaut true.
	 */
	public final boolean isActiverDocumentExcel() {
		return activerDocumentExcel;
	}

	/**
	 * @param valeur
	 *            Active ou non la gestion des documents excel. Par defaut true.
	 */
	public final void setActiverDocumentExcel(boolean valeur) {
		this.activerDocumentExcel = valeur;
	}

	/**
	 * @return Active ou non le téléchargement du template. Par defaut false.
	 */
	public final boolean isActiverTelechargementTemplate() {
		return activerTelechargementTemplate;
	}

	/**
	 * @param valeur
	 *            Active ou non le téléchargement du template. Par defaut false.
	 */
	public final void setActiverTelechargementTemplate(boolean valeur) {
		this.activerTelechargementTemplate = valeur;
	}

	/**
	 * @return
	 */
	public boolean isActiverTemplateMailHTML() {
		return activerTemplateMailHTML;
	}

	/**
	 * Active ou non la possibilité de définir un template d'un email avec un
	 * éditeur HTML.
	 * 
	 * @param activerTemplateMailHTML
	 */
	public void setActiverTemplateMailHTML(boolean activerTemplateMailHTML) {
		this.activerTemplateMailHTML = activerTemplateMailHTML;
	}

	/**
	 * return Active ou non la gestion des types de domaines pour les champs de
	 * fusion simple. Par defaut true.
	 */
	public boolean isActiverTypeDomaineChampFusionSimple() {
		return activerTypeDomaineChampFusionSimple;
	}

	/**
	 * @param valeur
	 *            Active ou non la gestion des types de domaines pour les champs
	 *            de fusion simple. Par defaut true.
	 */
	public void setActiverTypeDomaineChampFusionSimple(boolean valeur) {
		this.activerTypeDomaineChampFusionSimple = valeur;
	}

	/**
	 * return Active ou non la gestion des types de domaines pour les champs de
	 * fusion complexe. Par defaut false.
	 */
	public boolean isActiverTypeDomaineChampFusionComplexe() {
		return activerTypeDomaineChampFusionComplexe;
	}

	/**
	 * @return
	 */
	public boolean isActiverGestionVisibliteService() {
		return activerGestionVisibliteService;
	}

	/**
	 * @param activerGestionVisibliteService
	 */
	public void setActiverGestionVisibliteService(boolean activerGestionVisibliteService) {
		this.activerGestionVisibliteService = activerGestionVisibliteService;
	}

	/**
	 * @return
	 */
	public boolean isActiverGestionCriteresAffichage() {
		return activerGestionCriteresAffichage;
	}

	/**
	 * @param activerGestionCriteresAffichage
	 */
	public void setActiverGestionCriteresAffichage(boolean activerGestionCriteresAffichage) {
		this.activerGestionCriteresAffichage = activerGestionCriteresAffichage;
	}

	/**
	 * @param valeur
	 *            Active ou non la gestion des types de domaines pour les champs
	 *            de fusion complexe. Par defaut false.
	 */
	public void setActiverTypeDomaineChampFusionComplexe(boolean valeur) {
		this.activerTypeDomaineChampFusionComplexe = valeur;
	}

	public boolean isActiverValidation() {
		return activerValidation;
	}

	public void setActiverValidation(boolean activerValidation) {
		this.activerValidation = activerValidation;
	}

	public boolean isActiverVerificationChampsNulsGenerationCodeXML() {
		return activerVerificationChampsNulsGenerationCodeXML;
	}

	public void setActiverVerificationChampsNulsGenerationCodeXML(boolean activerVerificationChampsNulsGenerationCodeXML) {
		this.activerVerificationChampsNulsGenerationCodeXML = activerVerificationChampsNulsGenerationCodeXML;
	}

	/**
	 * @return Liste des options à afficher dans le cas d'un fichier excel.
	 */
	public final List<ItemSelectBox> getListeSelectionExcel() {
		return listeSelectionExcel;
	}

	/**
	 * @param valeur
	 *            Liste des options à afficher dans le cas d'un fichier excel.
	 */
	public final void setListeSelectionExcel(final List<ItemSelectBox> valeur) {
		this.listeSelectionExcel = valeur;
	}

	/**
	 * @return Association entre option dans le cas d'un fichier excel et la
	 *         liste de deversoires à afficher. Les deversoires sont identifés
	 *         via les constantes du fichier ConstantesDocument.java.
	 */
	public final Map<String, List<String>> getDeversoireAfficheParOption() {
		return deversoireAfficheParOption;
	}

	/**
	 * @param valeur
	 *            Association entre option dans le cas d'un fichier excel et la
	 *            liste de deversoires à afficher. Les deversoires sont
	 *            identifés via les constantes du fichier
	 *            ConstantesDocument.java.
	 */
	public final void setDeversoireAfficheParOption(final Map<String, List<String>> valeur) {
		this.deversoireAfficheParOption = valeur;
	}

	/**
	 * @return the deversoirePourDocument
	 */
	public final List<String> getDeversoirePourDocument() {
		return deversoirePourDocument;
	}

	/**
	 * @param valeur
	 *            the deversoirePourDocument to set
	 */
	public final void setDeversoirePourDocument(final List<String> valeur) {
		this.deversoirePourDocument = valeur;
	}

	/**
	 * @return Liste des formats affiché pour la génération d'un document ODT.
	 */
	public final List<TypeFormat> getListeFormatPourGenerationODT() {
		return listeFormatPourGenerationODT;
	}

	/**
	 * @param valeur
	 *            Liste des formats affiché pour la génération d'un document
	 *            ODT.
	 */
	public final void setListeFormatPourGenerationODT(final List<TypeFormat> valeur) {
		this.listeFormatPourGenerationODT = valeur;
	}

	/**
	 * @return Liste des formats affiché pour la génération d'un document XLS.
	 */
	public final List<TypeFormat> getListeFormatPourGenerationXLS() {
		return listeFormatPourGenerationXLS;
	}

	/**
	 * @param valeur
	 *            Liste des formats affiché pour la génération d'un document
	 *            XLS.
	 */
	public final void setListeFormatPourGenerationXLS(final List<TypeFormat> valeur) {
		this.listeFormatPourGenerationXLS = valeur;
	}

	public String getFormatDateFreemarker() {
		return formatDateFreemarker;
	}

	public void setFormatDateFreemarker(String formatDateFreemarker) {
		this.formatDateFreemarker = formatDateFreemarker;
	}

	public String getFormatDateTimeFreemarker() {
		return formatDateTimeFreemarker;
	}

	public void setFormatDateTimeFreemarker(String formatDateTimeFreemarker) {
		this.formatDateTimeFreemarker = formatDateTimeFreemarker;
	}

	public String getFormatNumeriqueCodeXML() {
		return formatNumeriqueCodeXML;
	}

	public void setFormatNumeriqueCodeXML(String formatNumeriqueCodeXML) {
		this.formatNumeriqueCodeXML = formatNumeriqueCodeXML;
	}

	public final boolean isTypeGenerationDocumentUnique() {

		boolean generation = activerDocument && !activerDocumentExcel && !activerMail && !activerRequeteLibre;

		return generation;
	}

	public final boolean isTypeGenerationDocumentExcelUnique() {

		boolean generation = !activerDocument && activerDocumentExcel && !activerMail && !activerRequeteLibre;

		return generation;
	}

	public final boolean isTypeGenerationCourrierUnique() {

		boolean generation = !activerDocument && !activerDocumentExcel && activerMail && !activerRequeteLibre;

		return generation;
	}

	public final boolean isTypeGenerationRequeteLibreUnique() {

		boolean generation = !activerDocument && !activerDocumentExcel && !activerMail && activerRequeteLibre;

		return generation;
	}

	public final boolean isTypeGenerationUnique() {

		return isTypeGenerationDocumentUnique() || isTypeGenerationDocumentExcelUnique() || isTypeGenerationCourrierUnique()
				|| isTypeGenerationRequeteLibreUnique();
	}
}
