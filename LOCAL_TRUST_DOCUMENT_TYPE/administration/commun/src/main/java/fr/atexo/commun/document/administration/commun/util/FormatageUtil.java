package fr.atexo.commun.document.administration.commun.util;

import java.io.Serializable;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public abstract class FormatageUtil implements Serializable {

	public final static String FORMAT_DATE_FREEMARKER = "?date?string('dd/MM/yyyy')";

	public final static String FORMAT_DATE_HEURE_FREEMARKER = "?datetime?string('dd/MM/yyyy HH:mm')";

	public final static String FORMAT_BOOLEEN_FREEMARKER = "?string";

	public final static String FORMAT_NUMERIQUE_CODE_XML = "#,###0.00";
}
