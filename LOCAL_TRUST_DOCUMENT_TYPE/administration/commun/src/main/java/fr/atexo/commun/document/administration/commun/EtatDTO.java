/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;

/**
 * bean DTO contenant les informations d'un Domaine de ChampsFusions
 * @author JOR
 * @version $Revision$ $Date$
 */
public class EtatDTO extends SelectDTO implements Serializable {

    private static final long serialVersionUID = -8178202209794815151L;

}
