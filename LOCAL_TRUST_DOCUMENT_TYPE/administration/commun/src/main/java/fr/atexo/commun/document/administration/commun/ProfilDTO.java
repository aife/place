/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;

/**
 * bean DTO contenant les informations d'un Profil qui sera lié à un DocumentModele
 * @author JOR
 * @version $Revision$ $Date$
 */
public class ProfilDTO extends SelectDTO implements Serializable {

    private static final long serialVersionUID = 5538769556596639699L;

}
