/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

import java.io.File;
import java.io.Serializable;

/**
 * bean DTO representant un Fichier qui pointe sur le template 'odt' du DocumentModele
 * @author JOR
 * @version $Revision$ $Date$
 */
public class FichierDTO implements Serializable {

    private static final long serialVersionUID = 5025890265520922196L;

    private File fichier;

    public FichierDTO(File fichier) {
        this.fichier = fichier;
    }

    public File getFichier() {
        return fichier;
    }

    public void setFichier(File fichier) {
        this.fichier = fichier;
    }

}
