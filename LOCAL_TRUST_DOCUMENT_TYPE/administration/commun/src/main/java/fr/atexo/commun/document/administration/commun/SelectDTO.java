/**
 * 
 */
package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;

/**
 * Classe regroupant les champs commun code et libellé pour les objets affiché
 * dans des champs select.
 * @author Léon Barsamian
 */
public class SelectDTO implements Serializable {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 287961388729743089L;

    private String code;
    private String libelle;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
