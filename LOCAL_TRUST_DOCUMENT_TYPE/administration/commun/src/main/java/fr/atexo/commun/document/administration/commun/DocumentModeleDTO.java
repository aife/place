package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * bean DTO contenant l'ensemble des proprietés d'un modèle de document ou de mail à sauvegarder ou
 * à modifier
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
/**
 * @author MZO
 * 
 */
public class DocumentModeleDTO implements Serializable {

  private static final long serialVersionUID = -2491346953042608756L;

  // Champs utilisés pour les modules administration
  private Long id;
  private String nom;
  private String nomTemplate;
  private List<ChampFusionDTO> champFusionsSelectionnes;
  private List<? extends SelectDTO> etapesDisponibles;
  private List<? extends SelectDTO> etapesSelectionnees;
  private List<? extends SelectDTO> profilsDisponibles;
  private List<? extends SelectDTO> profilsSelectionnes;

  private List<? extends SelectDTO> commissionsDisponibles;
  private List<? extends SelectDTO> commissionsSelectionnees;


  /**
   * Contenu du courriel saisi dans l'interface graphique
   */
  private String contenuCourriel;

  // Champs utilisés pour les modules administration et génération
  private TypeFormat typeFormat;

  /**
   * Valeur liée aux radiobouton d'association.
   */
  private boolean associer;

  /**
   * Indique si il s'agit d'un mail ou d'un document.
   */
  private TypeDocumentModele typeDocument;

  /**
   * Objet du courriel.
   */
  private String objetCourriel;

  /**
   * Xml de paramétrage (contient du FreeMarker).
   */
  private String xml;

  /**
   * Code du choix de derversoire.
   */
  private String choixAffichageDeversoire;

  /**
   * Type visibilité ( transversale ou par service )
   */
  private TypeVisibiliteServiceModele typeVisibiliteService;

  /**
   * 
   */
  private List<DocumentModeleCritereDTO> criteres;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNomTemplate() {
    return nomTemplate;
  }

  public void setNomTemplate(String nomTemplate) {
    this.nomTemplate = nomTemplate;
  }

  public TypeFormat getTypeFormat() {
    return typeFormat;
  }

  public void setTypeFormat(TypeFormat typeFormat) {
    this.typeFormat = typeFormat;
  }

  public List<ChampFusionDTO> getChampFusionsSelectionnes() {
    if (champFusionsSelectionnes == null) {
      champFusionsSelectionnes = new ArrayList<ChampFusionDTO>();
    }
    return champFusionsSelectionnes;
  }

  public void setChampFusionsSelectionnes(List<ChampFusionDTO> champFusionsSelectionnes) {
    this.champFusionsSelectionnes = champFusionsSelectionnes;
  }

  public List<? extends SelectDTO> getEtapesDisponibles() {
    if (etapesDisponibles == null) {
      etapesDisponibles = new ArrayList<EtatDTO>();
    }
    return etapesDisponibles;
  }

  public void setEtapesDisponibles(List<? extends SelectDTO> etapesDisponibles) {
    this.etapesDisponibles = etapesDisponibles;
  }

  public List<? extends SelectDTO> getProfilsDisponibles() {
    if (profilsDisponibles == null) {
      profilsDisponibles = new ArrayList<ProfilDTO>();
    }
    return profilsDisponibles;
  }

  public void setProfilsDisponibles(List<? extends SelectDTO> profilsDisponibles) {
    this.profilsDisponibles = profilsDisponibles;
  }

  public List<? extends SelectDTO> getEtapesSelectionnees() {
    return etapesSelectionnees;
  }

  public void setEtapesSelectionnees(List<? extends SelectDTO> etapesSelectionnees) {
    this.etapesSelectionnees = etapesSelectionnees;
  }

  public List<? extends SelectDTO> getProfilsSelectionnes() {
    return profilsSelectionnes;
  }

  public void setProfilsSelectionnes(List<? extends SelectDTO> profilsSelectionnes) {
    this.profilsSelectionnes = profilsSelectionnes;
  }

  public static DocumentModeleDTO getInstancePourGenerationDocument(TypeFormat typeFormat) {
    DocumentModeleDTO documentModeleDTO = new DocumentModeleDTO();
    documentModeleDTO.setTypeFormat(typeFormat);
    return documentModeleDTO;
  }

  public static DocumentModeleDTO getInstancePourGenerationMail(String contenuCourriel) {
    DocumentModeleDTO documentModeleDTO = new DocumentModeleDTO();
    documentModeleDTO.setContenuCourriel(contenuCourriel);
    return documentModeleDTO;
  }

  public static DocumentModeleDTO getInstancePourGenerationTableau(String xmlExcel) {
    return getInstancePourGenerationTableau(TypeFormat.EXCEL, xmlExcel);
  }

  public static DocumentModeleDTO getInstancePourGenerationTableau(TypeFormat typeFormat, String xmlExcel) {
    DocumentModeleDTO documentModeleDTO = new DocumentModeleDTO();
    documentModeleDTO.setTypeFormat(typeFormat);
    documentModeleDTO.setXml(xmlExcel);
    return documentModeleDTO;
  }

  /**
   * @return Indique si il s'agit d'un mail ou d'un document.
   */
  public final TypeDocumentModele getTypeDocument() {
    return typeDocument;
  }

  /**
   * @param valeur Indique si il s'agit d'un mail ou d'un document.
   */
  public final void setTypeDocument(final TypeDocumentModele valeur) {
    this.typeDocument = valeur;
  }

  /**
   * @return Contenu du courriel saisi dans l'interface graphique
   */
  public final String getContenuCourriel() {
    return contenuCourriel;
  }

  /**
   * @param valeur Contenu du courriel saisi dans l'interface graphique
   */
  public final void setContenuCourriel(final String valeur) {
    this.contenuCourriel = valeur;
  }

  /**
   * @return Valeur liée aux radiobouton d'association.
   */
  public final boolean isAssocier() {
    return associer;
  }

  /**
   * @param valeur Valeur liée aux radiobouton d'association.
   */
  public final void setAssocier(boolean valeur) {
    this.associer = valeur;
  }

  /**
   * @return Objet du courriel.
   */
  public final String getObjetCourriel() {
    return objetCourriel;
  }

  /**
   * @param valeur Objet du courriel.
   */
  public final void setObjetCourriel(final String valeur) {
    this.objetCourriel = valeur;
  }


  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("Objet DocumentModele : ");
    sb.append("Nom : ");
    sb.append(nom);
    sb.append("\nNom template : ");
    sb.append(nomTemplate);
    sb.append("\nObjet courriel : ");
    sb.append(objetCourriel);
    sb.append("\nContenu courriel : ");
    sb.append(contenuCourriel);
    return sb.toString();
  }

  /**
   * @return the commissionSelectionne
   */
  public final List<? extends SelectDTO> getCommissionsSelectionnees() {
    return commissionsSelectionnees;
  }

  /**
   * @param valeur the commissionSelectionne to set
   */
  public final void setCommissionsSelectionnees(List<? extends SelectDTO> valeur) {
    this.commissionsSelectionnees = valeur;
  }

  /**
   * @return the commissionDisponible
   */
  public final List<? extends SelectDTO> getCommissionsDisponibles() {
    return commissionsDisponibles;
  }

  /**
   * @param valeur the commissionDisponible to set
   */
  public final void setCommissionsDisponibles(List<? extends SelectDTO> valeur) {
    this.commissionsDisponibles = valeur;
  }

  /**
   * @return Xml de paramétrage du fichier excel.
   */
  public final String getXml() {
    return xml;
  }

  /**
   * @param valeur Xml de paramétrage du fichier excel.
   */
  public final void setXml(final String valeur) {
    this.xml = valeur;
  }

  /**
   * @return the choixAffichageDeversoire
   */
  public final String getChoixAffichageDeversoire() {
    return choixAffichageDeversoire;
  }

  /**
   * @param valeur the choixAffichageDeversoire to set
   */
  public final void setChoixAffichageDeversoire(final String valeur) {
    this.choixAffichageDeversoire = valeur;
  }

  /**
   * @param typeVisibiliteService
   */
  public void setTypeVisibiliteService(TypeVisibiliteServiceModele typeVisibiliteService) {
    this.typeVisibiliteService = typeVisibiliteService;
  }

  /**
   * @return
   */
  public TypeVisibiliteServiceModele getTypeVisibiliteService() {
    return typeVisibiliteService;
  }

  /**
   * @return
   */
  public List<DocumentModeleCritereDTO> getCriteres() {
    return criteres;
  }

  /**
   * @param criteres
   */
  public void setCriteres(List<DocumentModeleCritereDTO> criteres) {
    this.criteres = criteres;
  }

}
