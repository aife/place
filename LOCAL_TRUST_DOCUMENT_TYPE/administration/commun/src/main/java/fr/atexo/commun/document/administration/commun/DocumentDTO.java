/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * bean DTO contenant les information d'un document à générer. Il est utilisé
 * par le module de génération de Documents et/ou Mails
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
public class DocumentDTO implements Serializable {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = -3084797641753498224L;
	private final DocumentModeleDTO documentModeleDTO;
	private final String fileName;
	private final List<ChampFusionDTO> champFusions;
	private final List<ChampFusionDTO> champFusionsScriptsOoo;

	/**
	 * Constructeur à utiliser lors de la génération d'un mail
	 * 
	 * @param documentModeleDTO
	 *            Utiliser méthode getInstancePourGenerationMail(...) de la
	 *            class{@DocumentModeleDTO}
	 * @param champFusions
	 *            Les ChampsFusions à utiliser por la génération du mail. Les
	 */
	public DocumentDTO(DocumentModeleDTO documentModeleDTO, List<ChampFusionDTO> champFusions) {
		this(documentModeleDTO, null, champFusions);
	}

	/**
	 * Constructeur à utiliser lors de la génération d'un document
	 * 
	 * @param documentModeleDTO
	 *            le dto avec ses champsDTO et le contenu du Mail.
	 * @param fileName
	 *            le nom du fichier
	 * @param champFusions
	 *            les champs de fusion
	 * @throws Exception
	 *             lancée par {@link fr.atexo.commun.freemarker.FreeMarkerUtil}
	 */
	public DocumentDTO(DocumentModeleDTO documentModeleDTO, String fileName, List<ChampFusionDTO> champFusions) {
		this.documentModeleDTO = documentModeleDTO;
		this.fileName = fileName;
		this.champFusions = new ArrayList<ChampFusionDTO>();
		this.champFusionsScriptsOoo = new ArrayList<ChampFusionDTO>();
		if (champFusions != null) {
			for (ChampFusionDTO champFusionDTO : champFusions) {

				if (champFusionDTO.getTypeChamp() == TypeChamp.SCRIPT_OOO) {
					this.champFusionsScriptsOoo.add(champFusionDTO);
				} else {
					this.champFusions.add(champFusionDTO);
				}
			}
		}
	}

	public DocumentModeleDTO getDocumentModeleDTO() {
		return documentModeleDTO;
	}

	public String getFileName() {
		return fileName;
	}

	public List<ChampFusionDTO> getChampFusions() {
		return champFusions;
	}

	public List<ChampFusionDTO> getChampFusionsScriptsOoo() {
		return champFusionsScriptsOoo;
	}

}
