/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;
import java.util.List;

/**
 * bean DTO contenant les informations d'un Domaine de ChampsFusions
 *
 * @author JOR
 * @version $Revision$ $Date$
 */
public class DomaineDTO implements Serializable {

    private static final long serialVersionUID = -2564287575811406761L;

    private String nom;
    private String pointDepartNom;
    private String pointDepartCode;
    private List<ChampFusionDTO> champFusionDTOs;
    
    /**
     * Identifiant du domaine.
     */
    private long id;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<ChampFusionDTO> getDataFusionDTOs() {
        return champFusionDTOs;
    }

    public void setDataFusionDTOs(List<ChampFusionDTO> champFusionDTOs) {
        this.champFusionDTOs = champFusionDTOs;
    }

	public final long getId() {
		return id;
	}

	public final void setId(final long valeur) {
		this.id = valeur;
	}

    public final String getPointDepartNom() {
        return pointDepartNom;
    }

    public final void setPointDepartNom(String pointDepartNom) {
        this.pointDepartNom = pointDepartNom;
    }

    public final String getPointDepartCode() {
        return pointDepartCode;
    }

    public final void setPointDepartCode(String pointDepartCode) {
        this.pointDepartCode = pointDepartCode;
    }
}
