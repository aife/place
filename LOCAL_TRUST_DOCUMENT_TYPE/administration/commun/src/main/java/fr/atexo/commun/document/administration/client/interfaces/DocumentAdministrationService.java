/**
 *
 */
package fr.atexo.commun.document.administration.client.interfaces;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.GenerationRequeteLibreAsyncStatus;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;

/**
 * @author Léon Barsamian
 */
@RemoteServiceRelativePath("documentGwt.srv")
public interface DocumentAdministrationService extends RemoteService {

  /**
   * Initialisaton de l'écran.
   */
  InitialisationDTO initDocumentModele(Map<String, List<String>> parameters) throws Exception;

  /**
   * Enregistrement d'un modéle de document avec le template
   * 
   * @param documentModeleDTO modele
   * @param tokenFichierSession token du template odt associé en session. peut être null dans le cas
   *        ou l'utilisateur modifie le modéle sans changer le template. Est null dans le cas d'un
   *        mail.
   */
  void validerDocumentModele(DocumentModeleDTO documentModeleDTO, String tokenFichierSession) throws Exception;

  /**
   * Génére un fichier excel à partir d'une document de type requête libre.
   * 
   * @param documentModeleDTO modele
   * @param tokenFichierSession token du template associé en session. peut être null dans le cas ou
   *        l'utilisateur ne joind pas de fichier.
   */
  String genererDocumentRequeteLibre(DocumentModeleDTO documentModeleDTO, String tokenFichierSession) throws Exception;


  /**
   * Lance la génération en asynchrone un fichier excel à partir d'un document de type requête
   * libre.
   * 
   * @param documentModeleDTO
   * @param tokenFichierSession
   * @return token de suivi
   * @throws Exception
   */
  String genererDocumentRequeteLibreAsync(DocumentModeleDTO documentModeleDTO, String tokenFichierSession) throws Exception;


  /**
   * Récupère le document généré à partir du token
   * 
   * @param token
   * @return
   * @throws Exception
   */
  GenerationRequeteLibreAsyncStatus recupererDocumentRequeteLibreAsync(String token) throws Exception;


  /**
   * Télécharge le template ODT / XLS à partir de l'id du modèle de document
   * 
   * @param idDocumentModele l'identifiant du modèle
   * @return l'url spécifique pour de la servlet pour la récupération des fichiers. Si null l'url
   *         appelée sera /recupererTemplate.srv
   */
  String telechargerTemplate(Long idDocumentModele) throws Exception;
}
