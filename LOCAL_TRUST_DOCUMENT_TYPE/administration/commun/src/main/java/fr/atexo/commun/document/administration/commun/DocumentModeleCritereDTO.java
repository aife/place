package fr.atexo.commun.document.administration.commun;


public class DocumentModeleCritereDTO extends ChampFusionDTO {

  private static final long serialVersionUID = 1498439618114487030L;

  public static enum TypeOperateur {

      EGAL("documentModeleCritere.typeOperateur.egal"),
      DIFFERENT("documentModeleCritere.typeOperateur.different"),

      VIDE("documentModeleCritere.typeOperateur.vide"),
      NON_VIDE("documentModeleCritere.typeOperateur.nonVide"),

      INFERIEUR("documentModeleCritere.typeOperateur.inferieur"),
      INFERIEUR_OU_EGAL("documentModeleCritere.typeOperateur.inferieurOuEgal"),

      SUPERIEUR("documentModeleCritere.typeOperateur.superieur"),
      SUPERIEUR_OU_EGAL("documentModeleCritere.typeOperateur.superieurOuEgal");

    String codeLabel;

    private TypeOperateur(String codeLabel) {
      this.codeLabel = codeLabel;
    }

    public String getCodeLabel() {
      return codeLabel;
    }
  }

  public DocumentModeleCritereDTO() {

  }

  public DocumentModeleCritereDTO(ChampFusionDTO champFusionDTO) {
    this.setNom(champFusionDTO.getNom());
    this.setTypeDonnee(champFusionDTO.getTypeDonnee());
    this.setId(champFusionDTO.getId());
    this.setPath(champFusionDTO.getPath());
    this.setReferentielListeValeurs(champFusionDTO.getReferentielListeValeurs());
    this.setDescription(champFusionDTO.getDescription());
    this.setLibelle(champFusionDTO.getLibelle());
    this.setNomDuDomaine(champFusionDTO.getNomDuDomaine());
    this.setIdDomaine(champFusionDTO.getIdDomaine());
    this.setPosition(champFusionDTO.getPosition());
    this.setPointDepartCodeDuDomaine(champFusionDTO.getPointDepartCodeDuDomaine());
    this.setPointDepartNomDuDomaine(champFusionDTO.getPointDepartNomDuDomaine());
  }

  TypeOperateur typeOperateur;

  String valeurCritere;

  public TypeOperateur getTypeOperateur() {
    return typeOperateur;
  }

  public void setTypeOperateur(TypeOperateur typeOperateur) {
    this.typeOperateur = typeOperateur;
  }

  public String getValeurCritere() {
    return valeurCritere;
  }

  public void setValeurCritere(String valeurCritere) {
    this.valeurCritere = valeurCritere;
  }

}
