package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;

/**
 * 
 * Objet de présentation pour afficher des donnée dans un select.
 * @author Léon BARSAMIAN
 *
 */
public class ItemSelectBox implements Serializable {
	
	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = -8592577400455233612L;

	/**
	 * Code (attribue value d'une option d'un selectBox).
	 */
	private String code;
	
	/**
	 * Clé du fichier de propriété servant à chercher le libellé (libellé de l'option).
	 */
	private String cleLibelle;
	
	/**
	 * Indique si l'option doit être selectionnée par défaut lors de l'affichage.
	 * false par défaut.
	 * Si aucune option n'est selectionnée par défaut, le premier choix de la liste sera selectionné.
	 */
	private boolean optionSelectionnee = false;

	/**
	 * @return Code (attribue value d'une option d'un selectBox).
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * @param valeur Code (attribue value d'une option d'un selectBox).
	 */
	public final void setCode(final String valeur) {
		this.code = valeur;
	}

	/**
	 * @return Clé du fichier de propriété servant à chercher le libellé (libellé de l'option).
	 */
	public final String getCleLibelle() {
		return cleLibelle;
	}

	/**
	 * @param valeur Clé du fichier de propriété servant à chercher le libellé (libellé de l'option).
	 */
	public final void setCleLibelle(final String valeur) {
		this.cleLibelle = valeur;
	}

	/**
	 * @return the optionSelectionnee
	 */
	public final boolean isOptionSelectionnee() {
		return optionSelectionnee;
	}

	/**
	 * @param valeur the optionSelectionnee to set
	 */
	public final void setOptionSelectionnee(final boolean valeur) {
		this.optionSelectionnee = valeur;
	}

}
