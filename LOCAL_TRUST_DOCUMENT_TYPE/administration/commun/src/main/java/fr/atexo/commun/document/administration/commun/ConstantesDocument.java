package fr.atexo.commun.document.administration.commun;

/**
 * @author Léon Barsamian
 */
public class ConstantesDocument {

    public final static String CHEMIN_FICHIER_EXPORT = "cheminFichierExportChampFusion";

    public final static String NOM_FICHIER_EXPORT = "nomFichierExportChampFusion";
          
    public final static String CHEMIN_FICHIER_DOWNLOAD = "cheminFichierDownload";

    public final static String NOM_FICHIER_DOWNLOAD = "nomFichierDownload";

    public final static String TYPE_CONTENU_FICHIER_DOWNLOAD = "typeContenyFichierDownload";

    public final static String DEVERSOIRE_DISPOSITIF = "DEVERSOIRE_DISPOSITIF";
    
    public final static String DEVERSOIRE_PROFILS = "DEVERSOIRE_PROFILS";
    
    public final static String DEVERSOIRE_COMMISSION = "DEVERSOIRE_COMMISSION";       
}
