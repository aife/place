/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

/**
 * Enum utilisé pour classifier les Documents Modèles
 * 
 * @version $Revision$ $Date$
 */
public enum TypeDocumentModele {
	Mail,
	Document,
	Excel,
	RequeteLibre,
	FormulairePDF;
}
