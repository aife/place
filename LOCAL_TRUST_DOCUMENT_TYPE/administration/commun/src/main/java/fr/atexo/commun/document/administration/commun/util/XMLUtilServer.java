package fr.atexo.commun.document.administration.commun.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLUtilServer extends XMLUtil {

	private static final long serialVersionUID = -8317615037991374703L;

	public static String duplicateOnglet(String freeMarkerXml, String srcOngletIndex, String dstOngletIndex, String srcNewPointDepart, String dstPointDepart) {

		Pattern p = Pattern.compile(getOngletXmlEntete(srcOngletIndex) + ".*?#list (.*?) as.*" + ONGLET_XML_PIED);
		Matcher m = p.matcher(freeMarkerXml);
		String src = null;
		String srcPointDepart;
		if (m.find()) {
			src = m.group();
			srcPointDepart = m.group(1);
		} else {
			return freeMarkerXml;
		}

		String srcNew = src.replace(srcPointDepart, srcNewPointDepart);
		String dst = src.replace(getOngletXmlEntete(srcOngletIndex), getOngletXmlEntete(dstOngletIndex));
		dst = dst.replace(srcPointDepart, dstPointDepart);

		return freeMarkerXml.replace(src, srcNew + dst);

	}

}
