package fr.atexo.commun.document.administration.commun.util;

import java.io.Serializable;
import java.util.List;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;

/**
 * Commentaire
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public abstract class XMLUtil implements Serializable {

	public final static String DOCUMENT_XML_ENTETE = "<documentXLS versionMoteur=\"2\"><#setting locale=\"fr\">";

	public final static String DEFAULT_ONGLET_INDEX = "0";

	public final static String ONGLET_XML_PIED = "</onglet>";

	public final static String DOCUMENT_XML_PIED = "</documentXLS>";

	public static String getCodeFreeMarkerXML(String pointDepart, int numeroLigneIndex, List<ChampFusionDTO> champs,
			boolean verifierChampsNuls) {
		return getCodeFreeMarkerXML(pointDepart, numeroLigneIndex, champs, verifierChampsNuls,
				FormatageUtil.FORMAT_NUMERIQUE_CODE_XML, FormatageUtil.FORMAT_DATE_FREEMARKER,
				FormatageUtil.FORMAT_DATE_HEURE_FREEMARKER);
	}

	public static String getCodeFreeMarkerXML(String pointDepart, int numeroLigneIndex, List<ChampFusionDTO> champs,
			boolean verifierChampsNuls, String formatNumeriqueCodeXML, String formatDateFreemarker,
			String formatDateHeureFreemarker) {

		StringBuilder documentBuilder = new StringBuilder();
		documentBuilder.append(DOCUMENT_XML_ENTETE);
		documentBuilder.append(getOngletXmlEntete(DEFAULT_ONGLET_INDEX));

		if (champs != null) {

			StringBuilder cellulesBuilder = new StringBuilder();
			String nomIndex = "index" + pointDepart;
			String nomIterationListe = "liste" + pointDepart;
			String valeurIterationListe = "item" + pointDepart;

			for (ChampFusionDTO champ : champs) {

				int colonne = champs.indexOf(champ);
				String ligne = "${" + nomIndex + "}";
				String valeur = null;
				String cellule = null;
				if (champ.getTypeDonnee() != null) {

					switch (champ.getTypeDonnee()) {
					case DATE:
						valeur = getEvaluationValeur(valeurIterationListe + "." + champ.getPath(), verifierChampsNuls,
								formatDateFreemarker);
						cellule = getCelluleChaineCaractere(colonne, ligne, valeur);
						break;

					case DATE_HEURE:
						valeur = getEvaluationValeur(valeurIterationListe + "." + champ.getPath(), verifierChampsNuls,
								formatDateHeureFreemarker);
						cellule = getCelluleChaineCaractere(colonne, ligne, valeur);
						break;

					case BOOLEAN:
						valeur = getEvaluationValeur(valeurIterationListe + "." + champ.getPath(), verifierChampsNuls,
								FormatageUtil.FORMAT_BOOLEEN_FREEMARKER);
						cellule = getCelluleChaineCaractere(colonne, ligne, valeur);
						break;
					case LONG:
					case DOUBLE:
						valeur = getEvaluationValeur(valeurIterationListe + "." + champ.getPath(), verifierChampsNuls);
						cellule = getCelluleNumerique(colonne, ligne, valeur, formatNumeriqueCodeXML);
						break;
					case STRING:
					case TEXT:
					case ENUM:
					default:
						valeur = getEvaluationValeur(valeurIterationListe + "." + champ.getPath(), verifierChampsNuls, null);
						cellule = getCelluleChaineCaractere(colonne, ligne, valeur);
						break;
					}

				} else {
					valeur = getEvaluationValeur(valeurIterationListe + "." + champ.getPath(), verifierChampsNuls, null);
					cellule = getCelluleChaineCaractere(colonne, ligne, valeur);
				}

				if (cellule != null) {
					cellulesBuilder.append(cellule);
				}

			}

			String cellules = cellulesBuilder.toString();

			if (cellules != null) {
				String listeIteration = getListeIteration(nomIndex, numeroLigneIndex, nomIterationListe, valeurIterationListe,
						cellules);
				documentBuilder.append(listeIteration);
			}

		}

		documentBuilder.append(ONGLET_XML_PIED);
		documentBuilder.append(DOCUMENT_XML_PIED);

		return documentBuilder.toString();
	}

	protected final static String getOngletXmlEntete(String index) {
		return "<onglet numeroOnglet=\"" + index + "\">";
	}

	private final static String getListeIteration(final String nomIndex, final int index, final String nomIterationListe,
			final String nomIterationValeur, final String cellules) {
		return "<#assign " + nomIndex + " = " + String.valueOf(index) + " /><#list " + nomIterationListe + " as "
				+ nomIterationValeur + ">" + cellules + "<#assign " + nomIndex + " = " + nomIndex + " + 1 /></#list>";
	}

	private final static String getCelluleChaineCaractere(final int colonne, final String ligne, final String cheminAcces) {
		return "<cellule>" + getCoordonnees(colonne, String.valueOf(ligne)) + getValeurChaineCaractere(cheminAcces)
				+ "</cellule>";
	}

	private final static String getCelluleNumerique(final int colonne, final String ligne, final String cheminAcces,
			final String format) {
		return format != null ? "<cellule format=\"" + format + "\">" + getCoordonnees(colonne, String.valueOf(ligne))
				+ getValeurNumerique(cheminAcces) + "</cellule>" : "<cellule>" + getCoordonnees(colonne, String.valueOf(ligne))
				+ getValeurNumerique(cheminAcces) + "</cellule>";
	}

	private final static String getCelluleBooleenne(final int colonne, final String ligne, final String cheminAcces) {
		return "<cellule>" + getCoordonnees(colonne, String.valueOf(ligne)) + getValeurBooleenne(cheminAcces) + "</cellule>";
	}

	private final static String getValeurChaineCaractere(final String cheminAcces) {
		return "<valeur>" + cheminAcces + "</valeur>";
	}

	private final static String getValeurNumerique(final String cheminAcces) {
		return "<valeurNumerique>" + cheminAcces + "</valeurNumerique>";
	}

	private final static String getValeurBooleenne(final String cheminAcces) {
		return "<valeurBooleenne>" + cheminAcces + "</valeurBooleenne>";
	}

	private final static String getCoordonnees(final int colonne, final String ligne) {
		return "<coordonnee colonne=\"" + String.valueOf(colonne) + "\" ligne=\"" + ligne + "\"/>";
	}

	private final static String getEvaluationValeur(String valeur, boolean verifierChampsNuls) {
		return getEvaluationValeur(valeur, verifierChampsNuls, null);
	}

	private final static String getEvaluationValeur(String valeur, boolean verifierChampsNuls, String formatageFreemarker) {

		String[] valeurs = valeur.split("[.]");

		StringBuilder valeurEvaluation = null;
		StringBuilder valeurTemporaire = null;
		if (verifierChampsNuls && valeurs.length > 2) {

			for (int i = 0; i < valeurs.length; i++) {

				if (valeurEvaluation != null) {
					valeurTemporaire.append("." + valeurs[i]);
					valeurEvaluation.append(valeurTemporaire.toString() + "??");
					if (i != valeurs.length - 1) {
						valeurEvaluation.append(" && ");
					}
				} else {
					valeurEvaluation = new StringBuilder();
					valeurTemporaire = new StringBuilder(valeurs[i]);
				}
			}

		} else {
			valeurEvaluation = new StringBuilder(valeur + "??");
		}

		return formatageFreemarker == null ? "<#if " + valeurEvaluation.toString() + ">${" + valeur + "}</#if>" : "<#if "
				+ valeurEvaluation.toString() + ">${" + valeur + formatageFreemarker + "}</#if>";
	}

}
