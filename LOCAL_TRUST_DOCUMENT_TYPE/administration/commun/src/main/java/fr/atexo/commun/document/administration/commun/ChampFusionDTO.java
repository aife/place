/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;
import java.util.List;

import fr.atexo.gwt.ui.outils.commun.ICleValeur;

/**
 * bean DTO contenant les informations des champFusions qui seront utilisés pour la génération de
 * documents et/ou mails
 * 
 * @author JOR
 * @version $Revision$ $Date$
 */
public class ChampFusionDTO implements Serializable {

  private static final long serialVersionUID = 1927145650450297794L;

  private long id;
  private String nom;
  private String path;
  private String description;
  private TypeChamp typeChamp;
  private TypeDonnee typeDonnee;

  /**
   * libelle qui sera utilisé dans les listes de sélection des champs de fusion
   */
  private String affichage;

  /**
   * libelle du champ utilisé dans l'entete du template du requete libre
   */
  private String libelle;

  /**
   * Nom du domaine du champ de fusion affiché dans le tableau.
   */
  private String nomDuDomaine;

  /**
   * Type du domaine du champ de fusion.
   */
  private String pointDepartCodeDuDomaine;

  private String pointDepartNomDuDomaine;

  /**
   * Contenu du script complexe.
   */
  private String script;

  private transient Object valeur;

  /**
   * Identifiant du domaine associé en base, 0 si pas d'assocition.
   */
  private long idDomaine = 0L;

  /**
   * Position dans la liste des autres champs
   */
  private int position = 0;

  /**
   * 
   */
  List<? extends ICleValeur> referentielListeValeurs;

  public String getNom() {
    return nom;
  }

  public String getDescription() {
    return description;
  }

  public TypeChamp getTypeChamp() {
    return typeChamp;
  }

  public Object getValeur() {
    return valeur;
  }

  public void setValeur(Object valeur) {
    this.valeur = valeur;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public String getAffichage() {
    return affichage;
  }

  public void setAffichage(String affichage) {
    this.affichage = affichage;
  }

  /**
   * Permet de remplir un ChampFusionDTO sans valeur (elle sera calculée après par la class
   * {@link fr.atexo.commun.document.generation.util.IntrospectionUtil}) qui sera utilisé pour le
   * module de génération de documents
   * 
   * @param nom Le nom (idéntifiant) du champ
   * @param path Le path pour retrouver la valeur si on veut passer par la class
   *        {@link fr.atexo.commun.document.generation.util.IntrospectionUtil}
   * @param typeChamp Le type de Champ {@link TypeChamp}
   * @param contenuScript le contenu du script du champ de fusion complexe
   */
  public void setGenerationAttributesSansValeur(String nom, String path, TypeChamp typeChamp, String contenuScript) {
    this.nom = nom;
    this.path = path;
    this.typeChamp = typeChamp;
    this.script = contenuScript;
  }

  /**
   * Permet de remplir un ChampFusionDTO qui sera utilisé pour le module de génération de documents
   * 
   * @param nom Le nom (idéntifiant) du champ
   * @param typeChamp Le type de Champ {@link TypeChamp}
   * @param valeur La valeur à fusioner dans les documents et/ou mails
   * @param contenuScript le contenu du script du champ de fusion complexe
   */
  public void setGenerationAttributes(String nom, TypeChamp typeChamp, Object valeur, final String contenuScript) {
    this.nom = nom;
    this.typeChamp = typeChamp;
    this.valeur = valeur;
    this.script = contenuScript;
  }

  /**
   * Permet de remplir un ChampFusionDTO qui sera utilisé pour le module d'administration de
   * Documents Modèles. Méthode obsolète, utiliser la méthode incluant le code et le nom du point de
   * départ pour un domaine.
   * 
   * @param nom Le nom (idéntifiant) du champ
   * @param description descriptione 'funcionnelle' du champFusion (elle sera affichée dans l'UI GWT
   * @param typeChamp Le type de Champ {@link TypeChamp}
   * @param contenuScript le contenu du script du champ de fusion complexe
   * @param domaine le nom du domaine auquel est associé le champ de fusion
   * @param id identifiant du champ de fusion en base
   * @param valeurIdDomaine identifiant du dommaine associé en base (0 si pas d'association).
   */
  @Deprecated
  public void setAdministrationAttributes(String nom, String description, TypeChamp typeChamp, final String contenuScript, final String domaine, final long id, final String valeurIdDomaine) {
    this.nom = nom;
    this.description = description;
    this.typeChamp = typeChamp;
    this.script = contenuScript;
    this.nomDuDomaine = domaine;
    this.id = id;
    if (valeurIdDomaine != null) {
      this.idDomaine = Long.parseLong(valeurIdDomaine);
    }
  }

  /**
   * Permet de remplir un ChampFusionDTO qui sera utilisé pour le module d'administration de
   * Documents Modèles
   * 
   * @param nom Le nom (idéntifiant) du champ
   * @param description descriptione 'funcionnelle' du champFusion (elle sera affichée dans l'UI GWT
   * @param typeChamp Le type de Champ {@link TypeChamp}
   * @param contenuScript le contenu du script du champ de fusion complexe
   * @param domaine le nom du domaine auquel est associé le champ de fusion
   * @param pointDepartCodeDomaine le code du point de départ du domaine auquel est associé le champ
   *        de fusion
   * @param pointDepartNomDomaine le nom du point de départ du domaine auquel est associé le champ
   *        de fusion
   * @param id identifiant du champ de fusion en base
   * @param valeurIdDomaine identifiant du dommaine associé en base (0 si pas d'association).
   */
  public void setAdministrationAttributes(String nom, String description, TypeChamp typeChamp, final String contenuScript, final String domaine, final String pointDepartCodeDomaine, final String pointDepartNomDomaine, final long id, final String valeurIdDomaine) {
    this.nom = nom;
    this.description = description;
    this.typeChamp = typeChamp;
    this.script = contenuScript;
    this.nomDuDomaine = domaine;
    this.pointDepartCodeDuDomaine = pointDepartCodeDomaine;
    this.pointDepartNomDuDomaine = pointDepartNomDomaine;
    this.id = id;
    if (valeurIdDomaine != null) {
      this.idDomaine = Long.parseLong(valeurIdDomaine);
    }
  }

  /**
   * Permet de remplir un ChampFusionDTO qui sera utilisé pour le module d'administration pour
   * exporter des Documents Modèles
   * 
   * @param nom Le nom (idéntifiant) du champ
   * @param description descriptione 'funcionnelle' du champFusion (elle sera affichée dans l'UI GWT
   */
  public void setExportAttributes(String nom, String description) {
    this.nom = nom;
    this.description = description;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof ChampFusionDTO) {
      ChampFusionDTO objet = (ChampFusionDTO) obj;
      if (this.id != 0 && objet.getId() != 0) {
        return Long.valueOf(this.id).equals(Long.valueOf(objet.getId()));
      }
      return this.nom.equals(objet.getNom());
    }
    return super.equals(obj);
  }

  public final String getScript() {
    return script;
  }

  public final boolean isComplexe() {
    return this.typeChamp == TypeChamp.SCRIPT || this.typeChamp == TypeChamp.SCRIPT_OOO || this.typeChamp == TypeChamp.SCRIPT_TABLEAU;
  }

  /**
   * @return Nom du domaine du champ de fusion affiché dans le tableau.
   */
  public final String getNomDuDomaine() {
    return nomDuDomaine;
  }

  /**
   * @param param Nom du domaine du champ de fusion affiché dans le tableau.
   */
  public final void setNomDuDomaine(final String param) {
    this.nomDuDomaine = param;
  }

  public long getId() {
    return id;
  }

  public final void setId(final long param) {
    this.id = param;
  }

  /**
   * @return identifiant du domaine associé. 0 si pas d'association.
   */
  public final long getIdDomaine() {
    return idDomaine;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public String getPointDepartCodeDuDomaine() {
    return pointDepartCodeDuDomaine;
  }

  public void setPointDepartCodeDuDomaine(String pointDepartCodeDuDomaine) {
    this.pointDepartCodeDuDomaine = pointDepartCodeDuDomaine;
  }

  public String getPointDepartNomDuDomaine() {
    return pointDepartNomDuDomaine;
  }

  public void setPointDepartNomDuDomaine(String pointDepartNomDuDomaine) {
    this.pointDepartNomDuDomaine = pointDepartNomDuDomaine;
  }

  public TypeDonnee getTypeDonnee() {
    return typeDonnee;
  }

  public void setTypeDonnee(TypeDonnee typeDonnee) {
    this.typeDonnee = typeDonnee;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setIdDomaine(long idDomaine) {
    this.idDomaine = idDomaine;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public void setScript(String script) {
    this.script = script;
  }

  public void setTypeChamp(TypeChamp typeChamp) {
    this.typeChamp = typeChamp;
  }

  public List<? extends ICleValeur> getReferentielListeValeurs() {
    return referentielListeValeurs;
  }

  public void setReferentielListeValeurs(List<? extends ICleValeur> referentielListeValeurs) {
    this.referentielListeValeurs = referentielListeValeurs;
  }

}
