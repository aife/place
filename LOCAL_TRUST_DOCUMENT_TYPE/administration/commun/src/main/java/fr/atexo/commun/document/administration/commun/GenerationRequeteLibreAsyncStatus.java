package fr.atexo.commun.document.administration.commun;

import java.io.Serializable;

public class GenerationRequeteLibreAsyncStatus implements Serializable {

  String url;

  StatusEnum status;



  public String getUrl() {
    return url;
  }



  public void setUrl(String url) {
    this.url = url;
  }



  public StatusEnum getStatus() {
    return status;
  }



  public void setStatus(StatusEnum status) {
    this.status = status;
  }



  public enum StatusEnum {

    EN_COURS, GENERE, ERREUR;

  }


}
