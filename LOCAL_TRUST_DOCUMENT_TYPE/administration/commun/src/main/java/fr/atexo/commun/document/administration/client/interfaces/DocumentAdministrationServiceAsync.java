/**
 *
 */
package fr.atexo.commun.document.administration.client.interfaces;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.GenerationRequeteLibreAsyncStatus;

/**
 * @author Léon Barsamian
 */
public interface DocumentAdministrationServiceAsync {

  void initDocumentModele(Map<String, List<String>> parametres, AsyncCallback callback);

  void validerDocumentModele(DocumentModeleDTO documentModeleDTO, String tokenFichierSession, AsyncCallback callback);

  void genererDocumentRequeteLibre(DocumentModeleDTO documentModeleDTO, String tokenFichierSession, AsyncCallback<String> callback);

  void genererDocumentRequeteLibreAsync(DocumentModeleDTO documentModeleDTO, String tokenFichierSession, AsyncCallback<String> callback);

  void recupererDocumentRequeteLibreAsync(String token, AsyncCallback<GenerationRequeteLibreAsyncStatus> callback);

  /**
   * Téléchargement du template ODT/EXCEL;
   * 
   * @param idDocumentModele
   * @param callback
   * @throws Exception
   */
  void telechargerTemplate(Long idDocumentModele, AsyncCallback<String> callback);


}
