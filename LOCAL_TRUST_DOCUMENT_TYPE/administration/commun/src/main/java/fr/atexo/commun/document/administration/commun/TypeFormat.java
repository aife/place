/**
 * $Id$
 */
package fr.atexo.commun.document.administration.commun;


/**
 * Enum utilisé pour classifier les différent types de fichiers générés par le module de génération
 * et avoir l'ensemble d'informations statiques liées à ces types de fichiers
 *
 * @version $Revision$ $Date$
 */
public enum TypeFormat {

	PDF("PDF", "writer_pdf_Export", ".pdf", true, "application/pdf"),
	MSWORD("MS-Word", "MS Word 97", ".doc", true, "application/msword"),
	ODT("ODT", null, ".odt", true, "application/vnd.oasis.opendocument.text", "application/octet-stream"),
	EXCEL("EXCEL", null, ".xls", false, "application/excel"),
	TEXTE("TEXTE", null, ".txt", false, "text/plain");

	public final static TypeFormat[] FORMATS_AUTHORISES_ODT = new TypeFormat[]{PDF, MSWORD, ODT};

	public final static TypeFormat[] FORMATS_AUTHORISES_EXCEL = new TypeFormat[]{PDF, EXCEL};

	public final static TypeFormat[] FORMATS_AUTHORISES_COURRIER = new TypeFormat[]{TEXTE};

	private String name;

	private String code;

	private String extension;

	/**
	 * Indique si le format de fichier sera affiché dans la selectbox des documents.
	 */
	private boolean affichageChoixFormat;

	private String[] contentTypes;

	private TypeFormat(String name, String code, String extension, boolean valeurAffichageChoixFormat, String... contentTypes) {
		this.name = name;
		this.code = code;
		this.extension = extension;
		this.contentTypes = contentTypes;
		this.affichageChoixFormat = valeurAffichageChoixFormat;
	}

	public String getName() {
		return name;
	}

	public String getFormatCode() {
		return code;
	}

	public String getExtension() {
		return extension;
	}

	public String getDefaultContentType() {
		return contentTypes[0];
	}

	public String getLabel() {
		return getName();
	}

	public boolean isContentType(String contentType) {
		for (String c : contentTypes) {
			if (c.equalsIgnoreCase(contentType)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return Indique si le format de fichier sera affiché dans la selectbox des documents.
	 */
	public final boolean isAffichageChoixFormat() {
		return affichageChoixFormat;
	}

	/**
	 * @param valeur Indique si le format de fichier sera affiché dans la selectbox des documents.
	 */
	public final void setAffichageChoixFormat(final boolean valeur) {
		this.affichageChoixFormat = valeur;
	}
}
