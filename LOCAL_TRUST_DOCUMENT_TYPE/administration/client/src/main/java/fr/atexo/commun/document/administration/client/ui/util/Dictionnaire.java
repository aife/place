package fr.atexo.commun.document.administration.client.ui.util;

import com.google.gwt.i18n.client.Dictionary;

/**
 * Singleton permettant de gèrer un ensemble de dictionnaires contenant des libélles.
 */
public class Dictionnaire {

    public static final String DICTIONNAIRE_DOCUMENT_TYPE = "DictionnaireAdministrationDocument";

    private static String nomDictionnaireDocumentType = DICTIONNAIRE_DOCUMENT_TYPE;

    private static Dictionnaire dictionnaire;

    private Dictionary dictionary;

    /**
     * Permet de gérer un cache de dictionnaires
     *
     * @param nomDictionnaire
     * @return
     */
    private static Dictionnaire getInstance(String nomDictionnaire) {
        if (dictionnaire == null) {
            dictionnaire = new Dictionnaire(nomDictionnaire);
        }
        return dictionnaire;
    }

    /**
     * @return une instance du dictionnaire du module document type.
     */
    public static Dictionnaire getInstanceDictionnaireDocumentType() {
        return getInstance(getNomDictionnaireDocumentType());
    }

    public static String getNomDictionnaireDocumentType() {
        return nomDictionnaireDocumentType;
    }

    public static void setNomDictionnaireDocumentType(String nomDictionnaireDocumentType) {
        Dictionnaire.nomDictionnaireDocumentType = nomDictionnaireDocumentType;
    }

    /**
     * Permet d'instancier un nouveau dictionnaire.
     *
     * @param nomDictionnaire le nom du dictionnaire
     */
    private Dictionnaire(final String nomDictionnaire) {
        dictionary = Dictionary.getDictionary(nomDictionnaire);
    }

    /**
     * Retourne une chaine de caractère du dictionnaire.
     * Si la resources n'est pas trouvé alors on retourne la clef.
     *
     * @param clef le nom de la propriété à charger
     * @return la chaine de caractère retourné
     */
    public String get(String clef) {

        if (clef != null) {

            try {
                return dictionary.get(clef);
            } catch (Exception e) {
                return clef;
            }
        } else {
            return null;
        }
    }

    /**
     * Retourne une chaine de caractère du dictionnaire.
     * Si la resources n'est pas trouvé alors on retourne la clef.
     *
     * @param clef le nom de la propriété à charger
     * @return la chaine de caractère retourné
     */
    public String getMultiple(String... clef) {
        String resultat = "";
        for (int i = 0; i < clef.length; i++) {
            resultat += get(clef[i]);
            if (i != clef.length) {
                resultat += " ";
            }
        }
        return resultat;
    }

    /**
     * Retourne une chaine de caractère du dictionnaire en remplaçant les paramètres.
     * Si la resources n'est pas trouvé alors on retourne la clef.
     *
     * @param clef       le nom de la propriété à charger
     * @param parametres la liste des paramétres à remplacer
     * @return la chaine de caractère retourné
     */
    public String get(String clef, String... parametres) {

        try {
            String resultat = dictionary.get(clef);
            if (parametres != null) {
                for (int i = 0; i < parametres.length; i++) {
                    if (parametres[i] != null) {
                        resultat = resultat.replace("{" + i + "}", parametres[i]);
                    }
                }
            }
            return resultat;
        } catch (Exception e) {
            return clef;
        }
    }

    public static String contruireNomDictionnaire(String nom, String locale) {
        return nom + "_" + locale;
    }
}
