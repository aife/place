package fr.atexo.commun.document.administration.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

import fr.atexo.commun.document.administration.client.interfaces.DocumentAdministrationService;
import fr.atexo.commun.document.administration.client.interfaces.DocumentAdministrationServiceAsync;
import fr.atexo.commun.document.administration.client.ui.page.Document;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;

/**
 * Le module GWT de l'application document.
 */
public class DocumentModule implements EntryPoint {

    private static final DocumentAdministrationServiceAsync service = GWT.create(DocumentAdministrationService.class);

    private Document doc;

    public void onModuleLoad() {
        Map<String, List<String>> parameters = Window.Location.getParameterMap();
        doc = new Document();
        service.initDocumentModele(getSerializableMap(parameters), initialisationCallBack());
    }

    private AsyncCallback<InitialisationDTO> initialisationCallBack() {

        AsyncCallback<InitialisationDTO> callback = new AsyncCallback<InitialisationDTO>() {

            public void onFailure(final Throwable erreur) {
                RootPanel.get("form-bloc").add(doc);
                doc.ajouterErreur("Une erreur est survenue lors du chargement de la page: " + erreur.getMessage());
                GWT.log(erreur.getMessage());
            }

            public void onSuccess(InitialisationDTO donnees) {
                RootPanel.get("form-bloc").add(doc);
                doc.initialiser(donnees);
            }
        };

        return callback;
    }

    public static final DocumentAdministrationServiceAsync getService() {
        return service;
    }

    /**
     * Solution pour le problème d'objets non serialisés (Map et List)
     */
    public Map<String, List<String>> getSerializableMap(Map<String, List<String>> parameters) {
        Map<String, List<String>> hashMap = new HashMap<String, List<String>>();
        Set<String> cles = parameters.keySet();
        for (String cle : cles) {
            List<String> values = parameters.get(cle);
            List<String> listValues = new ArrayList<String>();
            for (String value : values) {
                listValues.add(value);
            }
            hashMap.put(cle, listValues);
        }
        return hashMap;
    }

}
