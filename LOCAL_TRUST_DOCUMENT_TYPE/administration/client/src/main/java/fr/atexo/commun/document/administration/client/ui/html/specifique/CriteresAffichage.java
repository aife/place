package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;

public class CriteresAffichage extends SelectionChampsFusion {

  private static CriteresAffichageUiBinder uiBinder = GWT.create(CriteresAffichageUiBinder.class);

  interface CriteresAffichageUiBinder extends UiBinder<Widget, CriteresAffichage> {
  }

  @UiField
  LegendElement lblLegende;

  @UiField
  TableauCriteresAffichage tableauCriteresAffichage;

  Dictionnaire dictionnaire;

  @Override
  protected void gestionListeSousDomaines(ChangeEvent e) {
    String nomDomaine = listeDomaines.getValue(listeDomaines.getSelectedIndex());
    String nomChamp = listeSousDomaines.getValue(listeSousDomaines.getSelectedIndex());
    ChampFusionDTO champ = chercherChampDeFusion(nomDomaine, nomChamp);
    if (champ != null) {
      tableauCriteresAffichage.ajouterLigne(new DocumentModeleCritereDTO(champ));
    }
  }

  @Override
  public void nettoyerTableau() {

  }

  public CriteresAffichage() {
    initWidget(uiBinder.createAndBindUi(this));
  }

  public void initialisation(InitialisationDTO initialisation) {
    DocumentModeleDTO documentModeleDTO = initialisation.getDocumentModeleDTO();
    List<DocumentModeleCritereDTO> criteres = documentModeleDTO.getCriteres();

    if (criteres != null) {
      for (DocumentModeleCritereDTO critere : criteres) {
        tableauCriteresAffichage.ajouterLigne(critere);
      }
    }

    List<ChampFusionDTO> champs = initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes();
    super.initialiser(initialisation.getDocumentModeleDTO().getTypeDocument(), initialisation.getDomainesDTO(), champs, false);

    List<ChampFusionDTO> champsSimples = new ArrayList<ChampFusionDTO>();

    for (ChampFusionDTO champFusionDTO : champs) {
      if (!champFusionDTO.isComplexe()) {
        champsSimples.add(champFusionDTO);
      }
    }

    if (UIObject.isVisible(ligneTypeDomaine)) {
      UIObject.setVisible(ligneTypeDomaine, initialisation.isActiverRequeteLibre() || initialisation.isActiverTypeDomaineChampFusionSimple());
    }
  }

  public List<DocumentModeleCritereDTO> getEtat(List<String> erreurs) {
    List<DocumentModeleCritereDTO> etat = tableauCriteresAffichage.getEtat(erreurs);
    return etat;
  }

  public void initLabel(Dictionnaire dictionnaire) {
    lblLegende.setInnerText(dictionnaire.get("documentModeleCritere.label.titre"));
    this.dictionnaire = dictionnaire;
    tableauCriteresAffichage.initLabel(dictionnaire);
  }

}
