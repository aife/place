/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.html.erreur.Erreur;
import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO.TypeOperateur;
import fr.atexo.commun.document.administration.commun.TypeDonnee;
import fr.atexo.gwt.ui.outils.client.ui.html.widget.DateBox;
import fr.atexo.gwt.ui.outils.commun.ICleValeur;
import fr.atexo.gwt.ui.outils.commun.util.StringUtil;

/**
 * @author MZO
 * 
 */
public class CritereAffichageDetailsPopUp extends PopupPanel {

	private static CritereAffichageDetailsUiBinder uiBinder = GWT.create(CritereAffichageDetailsUiBinder.class);

	interface CritereAffichageDetailsUiBinder extends UiBinder<Widget, CritereAffichageDetailsPopUp> {
	}

	interface CritereAffichageDetailsListener {
		public void onValider(DocumentModeleCritereDTO documentModeleCritereDTO);
	}

	@UiField
	protected Erreur tableauErreur;

	@UiField
	protected DivElement divTitre;

	@UiField
	protected LegendElement titreFieldSet;

	@UiField
	protected DivElement divDomaine;

	@UiField
	protected SpanElement lblNom;

	@UiField
	protected SpanElement lblOperateur;

	@UiField
	protected SpanElement lblValeur;

	@UiField
	protected Label nom;

	@UiField
	protected ListBox listeOperateurs;

	@UiField
	DivElement valeurDiv;

	@UiField
	protected TextBox valeur;

	@UiField
	protected DateBox valeurDate;

	@UiField
	protected ListBox valeurReferentiel;

	@UiField
	protected DivElement valeurBooleanDiv;

	@UiField
	protected RadioButton valeurBooleanTrue;

	@UiField
	protected RadioButton valeurBooleanFalse;

	@UiField
	protected Anchor annuler;

	@UiField
	protected Anchor fermer;

	@UiField
	protected Anchor valider;

	protected Dictionnaire dictionnaire;

	protected CritereAffichageDetailsListener listener;

	protected DocumentModeleCritereDTO etatInitial;

	DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
	DateTimeFormat dateHeureFormat = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");

	@UiHandler("annuler")
	public void fermeturePopup(ClickEvent e) {
		this.hide();
	}

	@UiHandler("fermer")
	public void annulerPopup(ClickEvent e) {
		this.hide();
	}

	@UiHandler("valider")
	public void validerPopup(ClickEvent e) {
		tableauErreur.nettoyer();
		if (listener != null) {
			List<String> erreurs = new ArrayList<String>();
			DocumentModeleCritereDTO etat = getEtat(erreurs);
			if (erreurs.isEmpty()) {
				listener.onValider(etat);
			} else {
				for (String erreur : erreurs) {
					tableauErreur.ajouterErreur(erreur);
				}
				return;
			}
		}
		this.hide();
	}

	public CritereAffichageDetailsPopUp() {
		super(true);
		setWidget(uiBinder.createAndBindUi(this));

		listeOperateurs.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				onListeOperateursChange();
			}
		});

		listeOperateurs.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				onListeOperateursChange();
			}
		});
	}

	private void onListeOperateursChange() {
		int selectedIndex = listeOperateurs.getSelectedIndex();
		TypeOperateur typeOperateur = null;
		if (selectedIndex == 0) {
			updateTypeOperateurDependantElements(null);
		} else {
			typeOperateur = TypeOperateur.valueOf(listeOperateurs.getValue(selectedIndex));
			updateTypeOperateurDependantElements(typeOperateur);
		}
	}

	public void setTypeOperateur(TypeOperateur typeOperateur) {
		if (typeOperateur == null) {
			listeOperateurs.setSelectedIndex(0);
		} else {
			for (int i = 1; i < listeOperateurs.getItemCount(); i++) {
				TypeOperateur op = TypeOperateur.valueOf(listeOperateurs.getValue(i));
				if (op == typeOperateur) {
					listeOperateurs.setSelectedIndex(i);
					updateTypeOperateurDependantElements(typeOperateur);
					break;
				}
			}
		}
	}

	public void updateTypeOperateurDependantElements(TypeOperateur typeOperateur) {
		if (typeOperateur == null || typeOperateur == TypeOperateur.VIDE || typeOperateur == TypeOperateur.NON_VIDE) {
			UIObject.setVisible(valeurDiv, false);
		} else {
			UIObject.setVisible(valeurDiv, true);
		}
	}

	public void initLabel(final Dictionnaire dictionnaire) {
		this.dictionnaire = dictionnaire;
		divTitre.setInnerText(dictionnaire.get("documentModeleCritere.label.popinTitre"));
		titreFieldSet.setInnerText(dictionnaire.get("documentModeleCritere.label.titreCritere"));

		lblNom.setInnerText(dictionnaire.get("documentModeleCritere.label.nom"));
		lblOperateur.setInnerText(dictionnaire.get("documentModeleCritere.label.operateur"));
		lblValeur.setInnerText(dictionnaire.get("documentModeleCritere.label.valeur"));

		valeurBooleanTrue.setText(dictionnaire.get("documentModeleCritere.valeur.boolean.true"));
		valeurBooleanFalse.setText(dictionnaire.get("documentModeleCritere.valeur.boolean.false"));

		annuler.setText(dictionnaire.get("champsComplexePopUpAnnuler"));
		valider.setText(dictionnaire.get("champsComplexePopUpValider"));
	}

	public DocumentModeleCritereDTO getEtat(List<String> erreurs) {

		DocumentModeleCritereDTO etat = new DocumentModeleCritereDTO();

		etat.setNom(etatInitial.getNom());
		etat.setTypeDonnee(etatInitial.getTypeDonnee());
		etat.setId(etatInitial.getId());
		etat.setPath(etatInitial.getPath());
		etat.setReferentielListeValeurs(etatInitial.getReferentielListeValeurs());
		etat.setDescription(etatInitial.getDescription());
		etat.setLibelle(etatInitial.getLibelle());
		etat.setNomDuDomaine(etatInitial.getNomDuDomaine());
		etat.setIdDomaine(etatInitial.getIdDomaine());
		etat.setPosition(etatInitial.getPosition());
		etat.setPointDepartCodeDuDomaine(etatInitial.getPointDepartCodeDuDomaine());
		etat.setPointDepartNomDuDomaine(etatInitial.getPointDepartNomDuDomaine());
		etat.setReferentielListeValeurs(etatInitial.getReferentielListeValeurs());

		int selectedIndex = listeOperateurs.getSelectedIndex();

		TypeOperateur typeOperateur = null;
		if (selectedIndex != 0) {
			typeOperateur = TypeOperateur.valueOf(listeOperateurs.getValue(selectedIndex));
			etat.setTypeOperateur(typeOperateur);
		} else {
			erreurs.add(dictionnaire.get("documentModeleCritere.operateur.obligatoire"));
			return etat;
		}

		etat.setTypeOperateur(typeOperateur);
		if (typeOperateur != null && typeOperateur != TypeOperateur.VIDE && typeOperateur != TypeOperateur.NON_VIDE) {

			switch (etat.getTypeDonnee()) {
			case TEXT:
			case STRING: {
				String valeurAsString = valeur.getValue();

				if (StringUtil.estVide(valeurAsString)) {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				} else {
					etat.setValeurCritere(valeurAsString);
				}
			}
				break;

			case LONG: {
				String valeurAsString = valeur.getValue();
				if (StringUtil.estVide(valeurAsString)) {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				} else {
					Long valueAsLong = null;
					try {
						valueAsLong = Long.parseLong(valeurAsString);
					} catch (Exception e) {
						valueAsLong = null;
					}
					if (valueAsLong != null) {
						etat.setValeurCritere(valueAsLong.toString());
					} else {
						erreurs.add(dictionnaire.get("documentModeleCritere.valeur.invalide"));
					}
				}
			}
				break;

			case DOUBLE: {
				String valeurAsString = valeur.getValue();
				if (StringUtil.estVide(valeurAsString)) {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				} else {
					Double valueAsDouble = null;
					try {
						valueAsDouble = Double.parseDouble(valeurAsString);
					} catch (Exception e) {
						valueAsDouble = null;
					}
					if (valueAsDouble != null) {
						etat.setValeurCritere(valueAsDouble.toString());
					} else {
						erreurs.add(dictionnaire.get("documentModeleCritere.valeur.invalide"));
					}
				}
			}
				break;

			case BOOLEAN: {
				Boolean valueTrue = valeurBooleanTrue.getValue();
				Boolean valueFalse = valeurBooleanFalse.getValue();
				if (valueTrue != null || valueFalse != null) {
					if (valueTrue != null && valueTrue) {
						etat.setValeurCritere("true");
					} else if (valueFalse != null && valueFalse) {
						etat.setValeurCritere("false");
					}
				} else {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				}
			}
				break;

			case DATE: {
				Date date = valeurDate.getValue();
				if (date == null) {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				} else {
					etat.setValeurCritere(dateFormat.format(date));
				}
			}
				break;

			case DATE_HEURE: {
				Date date = valeurDate.getValue();
				if (date == null) {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				} else {
					etat.setValeurCritere(dateFormat.format(date));
				}
			}
				break;

			case REFERENTIEL: {
				int selectedIndexRef = valeurReferentiel.getSelectedIndex();
				if (selectedIndexRef != 0) {
					etat.setValeurCritere(valeurReferentiel.getValue(selectedIndexRef));
				} else {
					erreurs.add(dictionnaire.get("documentModeleCritere.valeur.obligatoire"));
				}
				break;
			}
			default:
			}
		}
		return etat;
	}

	public void afficher(DocumentModeleCritereDTO documentModeleCritereDTO) {

		this.etatInitial = documentModeleCritereDTO;
		nom.setText(documentModeleCritereDTO.getNom());
		TypeDonnee typeDonnee = documentModeleCritereDTO.getTypeDonnee();
		EnumSet<TypeOperateur> operateursApplicables = typeDonnee.getOperateursApplicables();

		listeOperateurs.addItem(dictionnaire.get(dictionnaire.get("selectionnez")), "0");
		for (TypeOperateur typeOperateur : operateursApplicables) {
			listeOperateurs.addItem(dictionnaire.get(typeOperateur.getCodeLabel()), typeOperateur.name());
		}

		setTypeOperateur(documentModeleCritereDTO.getTypeOperateur());

		switch (documentModeleCritereDTO.getTypeDonnee()) {
		case TEXT:
		case STRING:
		case LONG:
		case DOUBLE:
			valeur.setVisible(true);
			valeurDate.setVisible(false);
			valeurReferentiel.setVisible(false);
			UIObject.setVisible(valeurBooleanDiv, false);
			valeur.setValue(documentModeleCritereDTO.getValeurCritere());
			break;

		case BOOLEAN:
			valeur.setVisible(false);
			valeurDate.setVisible(false);
			valeurReferentiel.setVisible(false);
			UIObject.setVisible(valeurBooleanDiv, true);
			boolean valeurBoolean = Boolean.parseBoolean(documentModeleCritereDTO.getValeurCritere());
			valeurBooleanTrue.setValue(valeurBoolean);
			valeurBooleanFalse.setValue(!valeurBoolean);
			break;

		case DATE:
			valeur.setVisible(false);
			valeurDate.setVisible(true);
			valeurReferentiel.setVisible(false);
			UIObject.setVisible(valeurBooleanDiv, false);
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
			if (documentModeleCritereDTO.getValeurCritere() != null) {
				valeurDate.setValue(dateFormat.parse(documentModeleCritereDTO.getValeurCritere()));
			}
			break;

		case DATE_HEURE:
			valeur.setVisible(false);
			valeurDate.setVisible(true);
			valeurReferentiel.setVisible(false);
			UIObject.setVisible(valeurBooleanDiv, false);
			DateTimeFormat dateHeureFormat = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");
			if (documentModeleCritereDTO.getValeurCritere() != null) {
				valeurDate.setValue(dateHeureFormat.parse(documentModeleCritereDTO.getValeurCritere()));
			}
			break;

		case REFERENTIEL: {
			valeur.setVisible(false);
			valeurDate.setVisible(false);
			valeurReferentiel.setVisible(true);
			UIObject.setVisible(valeurBooleanDiv, false);

			if (documentModeleCritereDTO.getReferentielListeValeurs() != null) {
				int i = 1;
				valeurReferentiel.addItem(dictionnaire.get(dictionnaire.get("selectionnez")), "0");
				for (ICleValeur ref : documentModeleCritereDTO.getReferentielListeValeurs()) {
					valeurReferentiel.addItem(ref.getValeur(), ref.getCle());
					if (documentModeleCritereDTO.getValeurCritere() != null
							&& documentModeleCritereDTO.getValeurCritere().equals(ref.getCle())) {
						valeurReferentiel.setSelectedIndex(i);
					}
					i++;
				}
			}
		}
		default:
		}
	}

	public void setListener(CritereAffichageDetailsListener listener) {
		this.listener = listener;
	}
}
