/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;

/**
 * @author Léon Barsamian
 */
public class TableauChampFusionComplexe extends FlexTable {

	private List<ChampFusionDTO> listeChampsComplexe = new ArrayList<ChampFusionDTO>();

	private Map<Widget, List<Widget>> mapWidgetChampsComplexe = new HashMap<Widget, List<Widget>>();

	private Map<Button, ChampFusionDTO> mapLienChampComplexe = new HashMap<Button, ChampFusionDTO>();

	/**
	 * Element TH pour la description.
	 */
	private Element colonneDescription;

	/**
	 * Element TH pour le Champ.
	 */
	private Element colonneChamp;

	/**
	 * Element TH pour le Action.
	 */
	private Element colonneAction;

	private Dictionnaire dictionnaire;

	/**
	 * Element TH pour le domaine.
	 */
	private Element colonneDomaine;

	/**
	 * Indique si l'option de champ complexe par domaine est activé.
	 */
	private boolean champComplexesParDomaine = false;

	public TableauChampFusionComplexe() {
		super();
		this.setCellPadding(0);
		this.setCellSpacing(0);
		this.setBorderWidth(0);
		this.setStyleName("rich-table liste-docs");

		Element head = DOM.createTHead();
		head.setClassName("rich-table-thead");
		Element headerTr = DOM.createTR();
		headerTr.setClassName("rich-table-subheader");
		colonneDescription = DOM.createTH();
		colonneDescription.setInnerText("Description");
		colonneDescription.setClassName("col-430");
		headerTr.appendChild(colonneDescription);
		colonneDomaine = DOM.createTH();
		colonneDomaine.setInnerText("Domaine");
		colonneDomaine.setClassName("moyen-col");
		colonneDomaine.setAttribute("style", "display:none;");
		headerTr.appendChild(colonneDomaine);
		colonneChamp = DOM.createTH();
		colonneChamp.setInnerText("Champ");
		colonneChamp.setClassName("moyen-col");
		headerTr.appendChild(colonneChamp);
		colonneAction = DOM.createTH();
		colonneAction.setInnerText("Action");
		colonneAction.setClassName("actions");
		headerTr.appendChild(colonneAction);

		DOM.insertChild(this.getElement(), head, 0);
		DOM.insertChild(head, headerTr, 0);

	}

	public void ajouterLigne(final ChampFusionDTO script) {
		if (!mapLienChampComplexe.values().contains(script)) {
			int numRows = this.getRowCount();
			Label lblCol1 = new Label(script.getDescription());
			this.setWidget(numRows, 0, lblCol1);
			String styleLblDescription = "col-430";

			if (champComplexesParDomaine) {
				styleLblDescription = "moyen-col";

			}
			this.getCellFormatter().addStyleName(numRows, 0, styleLblDescription);

			Label lblDomaine = new Label(script.getNomDuDomaine());
			this.setWidget(numRows, 1, lblDomaine);
			this.getCellFormatter().addStyleName(numRows, 1, "moyen-col");
			boolean visible = false;
			if (champComplexesParDomaine) {
				visible = true;
			}
			this.getCellFormatter().setVisible(numRows, 1, visible);

			Label lblCol2 = new Label(script.getNom());
			this.setWidget(numRows, 2, lblCol2);
			this.getCellFormatter().addStyleName(numRows, 2, "moyen-col");

			Button supprimer = new Button();
			supprimer.setStyleName("supprimer");
			supprimer.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					Button a = (Button) event.getSource();
					supprimerLigne(a);

				}
			});

			Button editer = new Button();
			editer.setStyleName("modifier");
			editer.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					Button a = (Button) event.getSource();
					ChampsComplexePopUp popup = ChampsComplexePopUp.getInstance(dictionnaire);
					popup.setTableauScript(TableauChampFusionComplexe.this);
					if (champComplexesParDomaine) {
						popup.activerChampsComplexesParDomaine();
					}
					popup.remplirEtAfficher(mapLienChampComplexe.get(a));
					popup.setLien(a);

				}
			});

			String uid = HTMLPanel.createUniqueId();
			HTMLPanel panel = new HTMLPanel("<div id='" + uid + "'></div>");
			panel.add(editer, uid);
			panel.add(supprimer, uid);
			this.setWidget(numRows, 3, panel);
			this.getCellFormatter().addStyleName(numRows, 3, "actions actions-small");

			List<Widget> widgets = new ArrayList<Widget>();
			widgets.add(lblCol1);
			widgets.add(lblDomaine);
			widgets.add(lblCol2);
			widgets.add(panel);
			mapWidgetChampsComplexe.put(editer, widgets);
			mapWidgetChampsComplexe.put(supprimer, widgets);
			listeChampsComplexe.add(script);
			mapLienChampComplexe.put(editer, script);
			mapLienChampComplexe.put(supprimer, script);
		}
	}

	private void supprimerLigne(Button a) {
		Widget parent = a.getParent();
		int nbLigne = this.getRowCount();
		for (int i = 0; i < nbLigne; i++) {
			Widget w = this.getWidget(i, 3);
			if (w == parent) {
				this.removeRow(i);
				break;
			}
		}
		ChampFusionDTO script = mapLienChampComplexe.get(a);
		listeChampsComplexe.remove(script);
		mapLienChampComplexe.remove(a);
	}

	/**
	 * Modifie ou ajoute un script aprés la validation de la popup.
	 *
	 * @param script
	 *            le script {@link ChampFusionDTO} à modifier / supprimer.
	 */
	public void modifierOuAjouter(final ChampFusionDTO ancien, final ChampFusionDTO script, final Button lien) {
		if (!listeChampsComplexe.contains(ancien)) {
			ajouterLigne(script);
		} else if (lien != null) {
			List<Widget> widgets = mapWidgetChampsComplexe.get(lien);

			Label nom = (Label) widgets.get(2);
			nom.setText(script.getNom());

			Label description = (Label) widgets.get(0);
			description.setText(script.getDescription());
			if (champComplexesParDomaine) {
				Label domaine = (Label) widgets.get(1);
				domaine.setText(script.getNomDuDomaine());
			}
			script.setId(ancien.getId());
			listeChampsComplexe.remove(ancien);
			listeChampsComplexe.add(script);
			mapLienChampComplexe.remove(lien);
			mapLienChampComplexe.put(lien, script);
		}

	}

	public List<ChampFusionDTO> recupereListeChamps() {
		return listeChampsComplexe;
	}

	public boolean verifierNomChampExistant(final String nom) {
		boolean valide = true;
		for (ChampFusionDTO champ : listeChampsComplexe) {
			if (nom.equals(champ.getNom())) {
				return false;
			}
		}
		return valide;
	}

	public void initLabel(final Dictionnaire dictionnaireInitialisation) {
		dictionnaire = dictionnaireInitialisation;
		colonneDescription.setInnerText(dictionnaire.get("champsComplexesTitreColonneDescription"));
		colonneChamp.setInnerText(dictionnaire.get("champsComplexesTitreColonneChamp"));
		colonneAction.setInnerText(dictionnaire.get("champsComplexesTitreColonneAction"));
	}

	public final void setChampComplexesParDomaine(final boolean valeur) {
		this.champComplexesParDomaine = valeur;
		if (champComplexesParDomaine) {
			colonneDomaine.setAttribute("style", "display:block;");
			colonneDescription.setClassName("moyen-col");
		}
	}

	public void nettoyerTableau() {
		listeChampsComplexe.clear();
		mapLienChampComplexe.clear();
		mapWidgetChampsComplexe.clear();
		this.removeAllRows();
	}
}
