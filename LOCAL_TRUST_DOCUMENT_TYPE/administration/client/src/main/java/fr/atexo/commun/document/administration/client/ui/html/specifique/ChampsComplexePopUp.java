/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.html.erreur.Erreur;
import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DomaineDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;

/**
 * Popup de création / modification des champs de fusion complexe.
 * 
 * @author Léon Barsamian
 */
public class ChampsComplexePopUp extends PopupPanel {

	private static ChampsComplexePopUpUiBinder uiBinder = GWT.create(ChampsComplexePopUpUiBinder.class);

	interface ChampsComplexePopUpUiBinder extends UiBinder<Widget, ChampsComplexePopUp> {
	}

	@UiField
	protected Erreur tableauErreur;

	@UiField
	protected DivElement divTitre;

	@UiField
	protected LegendElement titreFieldSet;

	/**
	 * Div d'affichage de la selection du domaine si l'option de script complexe
	 * par domaine est activée.
	 */
	@UiField
	protected DivElement divDomaine;

	@UiField
	protected SpanElement lblDomaine;

	@UiField
	protected SpanElement lblNom;

	@UiField
	protected SpanElement lblDescription;

	@UiField
	protected SpanElement lblContenu;

	@UiField
	SpanElement lblTypeScript;

	/**
	 * Liste des domaines de données affiché à l'écran.
	 */
	@UiField
	protected ListBox listeDomaines;

	@UiField
	protected TextBox nom;

	@UiField
	protected TextBox description;

	@UiField
	protected TextArea contenu;

	@UiField
	protected RadioButton scriptTypeNormal;

	@UiField
	protected RadioButton scriptTypeOpenOffice;

	@UiField
	protected RadioButton scriptTypeTableau;

	@UiField
	protected Anchor annuler;

	@UiField
	protected Anchor fermer;

	@UiField
	protected Anchor valider;

	/**
	 * Popup pour la saisie des champs complexes.
	 */
	private static ChampsComplexePopUp popup;

	/**
	 * Champs de fusion dans le cas d'une modification.
	 */
	private ChampFusionDTO champFusionAncien;

	/**
	 * Tableau de script à mettre à jour en cas d'ajout / suppression
	 */
	private TableauChampFusionComplexe tableauScript;

	/**
	 * Indique si l'option de champ complexe par domaine est activé.
	 */
	private boolean champComplexesParDomaine = false;

	/**
	 * Liste des nom des champs complexes présent dans l'application pour éviter
	 * les doublons.
	 */
	private List<String> nomChampsComplexe;

	private Button lien;

	private static List<DomaineDTO> listeDomaineDTO = new ArrayList<DomaineDTO>();

	/**
	 * Message d'erreur sur l'unicité des nom de script.
	 */
	private String erreurNomChampUnique = "";

	/**
	 * Message d'erreur pour les champs obligatoires.
	 */
	private String erreurChampsObligatoires = "";

	@UiHandler("annuler")
	public void fermeturePopup(ClickEvent e) {
		this.hide();
		viderSaisie();
	}

	@UiHandler("fermer")
	public void annulerPopup(ClickEvent e) {
		this.hide();
		viderSaisie();
	}

	@UiHandler("valider")
	public void validerPopup(ClickEvent e) {
		if (valide()) {
			ChampFusionDTO resultat = creerScript();
			tableauScript.modifierOuAjouter(champFusionAncien, resultat, lien);
			viderSaisie();
			hide();
		}
	}

	public ChampsComplexePopUp() {
		super(true);
		setWidget(uiBinder.createAndBindUi(this));
		contenu.setVisibleLines(15);
	}

	private ChampFusionDTO creerScript() {
		ChampFusionDTO resultat = new ChampFusionDTO();
		String idDomaineAssocie = "0";
		String nomDomaine = "";
		if (champComplexesParDomaine) {
			int index = listeDomaines.getSelectedIndex();
			idDomaineAssocie = listeDomaines.getValue(index);
			nomDomaine = listeDomaines.getItemText(index);
		}

		TypeChamp typeChamp = null;

		if (scriptTypeTableau.getValue()) {
			typeChamp = TypeChamp.SCRIPT_TABLEAU;
		} else if (scriptTypeOpenOffice.getValue()) {
			typeChamp = TypeChamp.SCRIPT_OOO;
		} else {
			typeChamp = TypeChamp.SCRIPT;
		}

		resultat.setAdministrationAttributes(nom.getText(), description.getText(), typeChamp, contenu.getText(), nomDomaine, 0, idDomaineAssocie);
		return resultat;
	}

	/**
	 * Efface tout les champs de saisie.
	 */
	private void viderSaisie() {

		nom.setText("");
		description.setText("");
		contenu.setText("");

		scriptTypeNormal.setValue(true);

		nom.removeStyleName("error-border");
		description.removeStyleName("error-border");
		contenu.removeStyleName("error-border");
		if (champComplexesParDomaine) {
			listeDomaines.setSelectedIndex(0);
			listeDomaines.removeStyleName("error-border");
		}
		champFusionAncien = null;
	}

	/**
	 * Initialisation de l'affichage et affichage de la popup à partir d'un
	 * script.
	 * 
	 * @param script
	 */
	public void remplirEtAfficher(final ChampFusionDTO script) {
		viderSaisie();
		champFusionAncien = script;
		nom.setText(script.getNom());
		description.setText(script.getDescription());
		contenu.setText(script.getScript());

		if (script.getTypeChamp() == TypeChamp.SCRIPT_OOO) {
			scriptTypeOpenOffice.setValue(true);
		} else if (script.getTypeChamp() == TypeChamp.SCRIPT_TABLEAU) {
			scriptTypeTableau.setValue(true);
		} else {
			scriptTypeNormal.setValue(true);
		}

		if (champComplexesParDomaine) {
			if (script.getIdDomaine() == 0) {
				listeDomaines.setSelectedIndex(0);
			} else {
				int nombre = listeDomaines.getItemCount();
				for (int index = 0; index < nombre; index++) {
					String idDomaine = Long.toString(script.getIdDomaine());
					if (idDomaine.equals(listeDomaines.getValue(index))) {
						listeDomaines.setSelectedIndex(index);
						break;
					}
				}
			}

			listeDomaines.removeStyleName("error-border");
		}
		this.show();
	}

	/**
	 * @param dictionnaire
	 *            Dictionnaire utilisé pour l'externalisation des libellés.
	 * @return l'instance de la popup.
	 */
	public static ChampsComplexePopUp getInstance(final Dictionnaire dictionnaire) {
		if (popup == null) {
			popup = new ChampsComplexePopUp();
			popup.initLabel(dictionnaire);
		}
		popup.listeDomaines.clear();
		popup.viderSaisie();
		popup.champFusionAncien = null;
		return popup;
	}

	public final void setTableauScript(final TableauChampFusionComplexe valeur) {
		this.tableauScript = valeur;
	}

	public final void setLien(final Button valeur) {
		this.lien = valeur;
	}

	/**
	 * Validation de la saisie. vérifie les champs obligatoires, ainsi que
	 * l'unicité du nom.
	 * 
	 * @return true si valide, false sinon.
	 */
	private final boolean valide() {
		boolean valide = true;
		boolean champObligatoire = false;
		tableauErreur.nettoyer();
		nom.removeStyleName("error-border");
		if ("".equals(nom.getText().trim())) {
			nom.addStyleName("error-border");
			champObligatoire = true;
			valide = false;
		} else {
			boolean modification = false;
			if (champFusionAncien != null && champFusionAncien.getNom().equals(nom.getText())) {
				modification = true;
			}
			if ((!modification && !tableauScript.verifierNomChampExistant(nom.getText()))
					|| (!modification && champComplexesParDomaine && nomChampsComplexe.contains(nom.getText().trim()))) {

				nom.addStyleName("error-border");
				tableauErreur.ajouterErreur(erreurNomChampUnique);
				valide = false;
			}

		}
		if ("".equals(description.getText().trim())) {
			description.addStyleName("error-border");
			champObligatoire = true;
			valide = false;
		} else {
			description.removeStyleName("error-border");
		}
		if ("".equals(contenu.getText().trim())) {
			contenu.addStyleName("error-border");
			champObligatoire = true;
			valide = false;
		} else {
			contenu.removeStyleName("error-border");
		}

		if (champComplexesParDomaine && listeDomaines.getSelectedIndex() == 0) {
			listeDomaines.addStyleName("error-border");
			valide = false;
			champObligatoire = true;
		} else {
			listeDomaines.removeStyleName("error-border");
		}

		if (champObligatoire) {
			tableauErreur.ajouterErreur(erreurChampsObligatoires);
		}
		return valide;
	}

	public void initLabel(final Dictionnaire dictionnaire) {
		divTitre.setInnerText(dictionnaire.get("champsComplexePopUpTitre"));
		titreFieldSet.setInnerText(dictionnaire.get("champsComplexePopUpTitreFieldSet"));
		lblNom.setInnerText(dictionnaire.get("champsComplexePopUpNom"));
		lblDescription.setInnerText(dictionnaire.get("champsComplexePopUpDescription"));
		lblContenu.setInnerText(dictionnaire.get("champsComplexePopUpContenu"));
		lblTypeScript.setInnerText(dictionnaire.get("champsComplexePopUpTypeScript"));
		annuler.setText(dictionnaire.get("champsComplexePopUpAnnuler"));
		valider.setText(dictionnaire.get("champsComplexePopUpValider"));
		lblDomaine.setInnerText(dictionnaire.get("champsComplexePopUpDomaine"));
		erreurChampsObligatoires = dictionnaire.get("erreurChampsObligatoires");
		erreurNomChampUnique = dictionnaire.get("erreurNomChampUnique");
	}

	/**
	 * Active la gestion des domaines pour les champs de fusion complexes.
	 */
	public final void activerChampsComplexesParDomaine() {
		this.champComplexesParDomaine = true;
		UIObject.setVisible(divDomaine, true);
		listeDomaines.insertItem("Sélectionnez...", "Sélectionnez...", -1);
		nomChampsComplexe = new ArrayList<String>();
		for (DomaineDTO domaineDTO : listeDomaineDTO) {
			listeDomaines.insertItem(domaineDTO.getNom(), Long.toString(domaineDTO.getId()), -1);
			if (domaineDTO.getDataFusionDTOs() != null) {
				for (ChampFusionDTO champFusionDTO : domaineDTO.getDataFusionDTOs()) {
					if (champFusionDTO.isComplexe()) {
						nomChampsComplexe.add(champFusionDTO.getNom());
					}
				}
			}
		}
	}

	public static final void setListeDomaineDTO(List<DomaineDTO> valeur) {
		ChampsComplexePopUp.listeDomaineDTO = valeur;
	}
}
