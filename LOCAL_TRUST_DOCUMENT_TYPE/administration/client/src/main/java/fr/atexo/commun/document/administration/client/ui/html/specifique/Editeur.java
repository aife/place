package fr.atexo.commun.document.administration.client.ui.html.specifique;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.html.editeur.RichTextToolbar;
import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;

/**
 * Panneau de saisie pour les modéles de mail.
 *
 * @author Léon Barsamian
 */
public class Editeur extends Composite {

    private static EditeurUiBinder uiBinder = GWT.create(EditeurUiBinder.class);

    interface EditeurUiBinder extends UiBinder<Widget, Editeur> {
    }

    @UiField
    LegendElement lblLegende;

    @UiField
    protected Grid panneauEditeur;


    private RichTextArea area;

    private RichTextToolbar toolbar;

    public Editeur() {
        initWidget(uiBinder.createAndBindUi(this));

        area = new RichTextArea();
        area.ensureDebugId("cwRichText-area");
        area.setSize("100%", "14em");
        toolbar = new RichTextToolbar(area);
        toolbar.ensureDebugId("cwRichText-toolbar");
        toolbar.setWidth("100%");

        // Add the components to a panel
        panneauEditeur.resize(2, 1);
        panneauEditeur.setStyleName("cw-RichText");
        panneauEditeur.setSize("100%", "100%");
        panneauEditeur.setWidget(0, 0, toolbar);
        panneauEditeur.setWidget(1, 0, area);
        toolbar.setVisible(false);


    }

    public void initLabel(final Dictionnaire dictionnaire) {
        lblLegende.setInnerText(dictionnaire.get("editeurTitre"));
    }

    /**
     * Initialisation du contenu du message.
     *
     * @param document
     */
    public void initialisation(final DocumentModeleDTO document) {
        area.setHTML(document.getContenuCourriel());
    }

    /**
     * Mise à jour du courriel.
     *
     * @param document document à modifier.
     * @return le document modifié.
     */
    public DocumentModeleDTO modifier(final DocumentModeleDTO document) {
        if (this.isVisible()) {
            String courriel = area.getHTML();
            courriel = courriel.replaceAll("<br>", "<br />");
            document.setContenuCourriel(courriel);
        } else {
            document.setContenuCourriel("");
        }
        return document;
    }

}
