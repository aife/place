/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.DomaineDTO;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;

/**
 * Fieldset de gestion des champs de fusion complexe.
 *
 * @author Léon Barsamian
 */
public class SelectionChampsFusionComplexes extends SelectionChampsFusion {

    private static SelectionChampsFusionComplexesUiBinder uiBinder = GWT.create(SelectionChampsFusionComplexesUiBinder.class);

    interface SelectionChampsFusionComplexesUiBinder extends UiBinder<Widget, SelectionChampsFusionComplexes> {
    }

    /**
     * Tableau affichant la liste des champs de fusion complexe.
     */
    @UiField
    protected TableauChampFusionComplexe tableauChampFusionComplexe;

    /**
     * Popup d'ajout / modification d'un champ de fusion complexe.
     */
    private ChampsComplexePopUp popup;

    private List<DomaineDTO> listeDomaine = new ArrayList<DomaineDTO>();

    /**
     * Indique si l'option de champ complexe par domaine est activé.
     */
    private boolean champComplexesParDomaine = false;

    /**
     * Lien pour ajouter un champ de fusion complexe.
     */
    @UiField
    protected Anchor lienAjout;

    @UiHandler("lienAjout")
    protected void ouverturePopup(ClickEvent e) {

        if (popup == null) {
            popup = ChampsComplexePopUp.getInstance(getDictionnaire());
            popup.setTableauScript(tableauChampFusionComplexe);
            if (champComplexesParDomaine) {
                popup.activerChampsComplexesParDomaine();
            }
        }

        popup.show();

    }

    /**
     * Div contenant les selectBox pour la selection des champs de fusion
     * complexes par domaine.
     */
    @UiField
    protected DivElement specifiqueDomaineComplexe;

    public SelectionChampsFusionComplexes() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    /**
     * Listener exécuté lors de la selection d'un champ de fusion dans la
     * selectList. Le champ de fusion sera ajouté au tableau.
     */
    @Override
    protected void gestionListeSousDomaines(ChangeEvent e) {
        String nomDomaine = listeDomaines.getValue(listeDomaines.getSelectedIndex());
        String nomChamp = listeSousDomaines.getValue(listeSousDomaines.getSelectedIndex());
        ChampFusionDTO champ = chercherChampDeFusion(nomDomaine, nomChamp);
        if (champ != null) {
            tableauChampFusionComplexe.ajouterLigne(champ);
        }

    }

    @Override
    public void nettoyerTableau() {
        tableauChampFusionComplexe.nettoyerTableau();
    }


    /**
     * Initialisation de l'afficahge avec les champs de fusion complexes
     * associés au {@link DocumentModeleDTO}
     *
     * @param initialisation les informations de paramétrage
     */
    public void initialisation(final InitialisationDTO initialisation) {
        specifiqueDomaineComplexe.setAttribute("style", "display: none;");
        List<ChampFusionDTO> champs = initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes();

        if (initialisation.isActiverDomaineChampFusionComplexe()) {
            champComplexesParDomaine = true;

            specifiqueDomaineComplexe.setAttribute("style", "display: block;");

            listeDomaine = initialisation.getDomainesDTO();

            super.initialiser(initialisation.getDocumentModeleDTO().getTypeDocument(), listeDomaine, champs, true);
            tableauChampFusionComplexe.setChampComplexesParDomaine(initialisation.isActiverDomaineChampFusionComplexe());
            ChampsComplexePopUp.setListeDomaineDTO(listeDomaine);
        }

        for (ChampFusionDTO champFusionDTO : champs) {
            if (true == champFusionDTO.isComplexe()) {
                tableauChampFusionComplexe.ajouterLigne(champFusionDTO);
            }
        }

        if (UIObject.isVisible(ligneTypeDomaine)) {
            UIObject.setVisible(ligneTypeDomaine, initialisation.isActiverTypeDomaineChampFusionComplexe());
        }
    }

    private native void modifierStyleSpecifiqueDomaineComplexe(final String affichage) /*-{
        document.getElementById('specifiqueDomaineComplexe').style.display = affichage;
    }-*/;


    /**
     * Initialisation des libellés de l'application.
     *
     * @param dictionnaire dictionnaire utilisé pour les libellés.
     */
    public void initLabel(final Dictionnaire dictionnaire) {
        setDictionnaire(dictionnaire);
        lblLegende.setInnerText(dictionnaire.get("champsComplexesTitre"));
        lblLegende.setTitle(dictionnaire.get("champsComplexesTitre"));
        divTypeDomaine.setInnerText(dictionnaire.get("champsComplexesTypeDomaine"));
        divDomaine.setInnerText(dictionnaire.get("champsComplexesDomaine"));
        divSousDomaine.setInnerText(dictionnaire.get("champsComplexesSousDomaine"));
        lienAjout.setText(dictionnaire.get("champsComplexesAjoutScript"));
        tableauChampFusionComplexe.initLabel(dictionnaire);
    }

    /**
     * Mise à jour des champs de fusion complexes du {@link DocumentModeleDTO}
     *
     * @param document
     * @return document {@link DocumentModeleDTO} mise à jour
     */
    public DocumentModeleDTO modifier(final DocumentModeleDTO document) {
        document.getChampFusionsSelectionnes().addAll(tableauChampFusionComplexe.recupereListeChamps());
        return document;
    }

    /**
     * Retourne la liste des champs de fusion selectionné pour l'export excel.
     *
     * @return
     */
    public List<ChampFusionDTO> recupereChampCourrantPourExport() {
        return tableauChampFusionComplexe.recupereListeChamps();
    }

    public void initialiser(TypeDocumentModele typeDocumentModele, List<DomaineDTO> domaines, List<ChampFusionDTO> champs) {
        super.initialiser(typeDocumentModele, domaines, champs, true);
    }
}
