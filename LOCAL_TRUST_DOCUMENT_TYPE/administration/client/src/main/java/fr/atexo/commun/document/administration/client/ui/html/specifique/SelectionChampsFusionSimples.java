/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.DomaineDTO;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;

/**
 * Fieldset de gestion des champs de fusion simples
 *
 * @author Léon Barsamian
 */
public class SelectionChampsFusionSimples extends SelectionChampsFusion {

    private static SelectionChampsFusionSimplesUiBinder uiBinder = GWT.create(SelectionChampsFusionSimplesUiBinder.class);

    interface SelectionChampsFusionSimplesUiBinder extends UiBinder<Widget, SelectionChampsFusionSimples> {
    }

    @UiField
    protected TableauChampFusionSimple tableauChampFusionSimple;

    public SelectionChampsFusionSimples() {
        initWidget(uiBinder.createAndBindUi(this));

    }

    /**
     * Listener exécuté lors de la selection d'un champ de fusion dans la
     * selectList. Le champ de fusion sera ajouté au tableau.
     */
    @Override
    protected void gestionListeSousDomaines(ChangeEvent e) {
        String nomDomaine = listeDomaines.getValue(listeDomaines.getSelectedIndex());
        String nomChamp = listeSousDomaines.getValue(listeSousDomaines.getSelectedIndex());
        ChampFusionDTO champ = chercherChampDeFusion(nomDomaine, nomChamp);
        if (champ != null) {
            tableauChampFusionSimple.ajouterChampFusion(champ);
        }

    }

    @Override
    public void nettoyerTableau() {
        tableauChampFusionSimple.nettoyerTableau();
    }

    /**
     * Initialisation de l'afficahge avec les champs de fusion simples associés au {@link DocumentModeleDTO}
     *
     * @param initialisation les informations de paramétrage
     */
    public void initialisation(final InitialisationDTO initialisation) {
        List<ChampFusionDTO> champs = initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes();
        super.initialiser(initialisation.getDocumentModeleDTO().getTypeDocument(), initialisation.getDomainesDTO(), champs, false);
        tableauChampFusionSimple.setActiverTriParPosition(initialisation.getDocumentModeleDTO().getTypeDocument() == TypeDocumentModele.RequeteLibre);

        List<ChampFusionDTO> champsSimples = new ArrayList<ChampFusionDTO>();

        for (ChampFusionDTO champFusionDTO : champs) {
            if (!champFusionDTO.isComplexe()) {
                champsSimples.add(champFusionDTO);
            }
        }

        for (ChampFusionDTO champFusionDTO : champsSimples) {
            boolean premier = champFusionDTO == champsSimples.get(0);
            boolean dernier = champFusionDTO == champsSimples.get(champsSimples.size() - 1);
            tableauChampFusionSimple.ajouterLigne(champFusionDTO, premier, dernier);
        }

        if (UIObject.isVisible(ligneTypeDomaine)) {
            UIObject.setVisible(ligneTypeDomaine, initialisation.isActiverRequeteLibre() || initialisation.isActiverTypeDomaineChampFusionSimple());
        }
    }

    public void initLabel(final Dictionnaire dictionnaire) {
        setDictionnaire(dictionnaire);
        lblLegende.setTitle(dictionnaire.get("champsSimplesTitre"));
        lblLegende.setInnerText(dictionnaire.get("champsSimplesTitre"));
        divTypeDomaine.setInnerText(dictionnaire.get("champsSimplesTypeDomaine"));
        divDomaine.setInnerText(dictionnaire.get("champsSimplesDomaine"));
        divSousDomaine.setInnerText(dictionnaire.get("champsSimplesSousDomaine"));
        tableauChampFusionSimple.initLabel(dictionnaire);
    }

    /**
     * Mise à jour des champs de fusion simples du {@link DocumentModeleDTO}
     *
     * @param document
     * @return document {@link DocumentModeleDTO} mise à jour
     */
    public DocumentModeleDTO modifier(final DocumentModeleDTO document) {
        document.getChampFusionsSelectionnes().addAll(tableauChampFusionSimple.recupereListeChamps());
        return document;
    }

    /**
     * Retourne la liste des champs de fusion selectionné pour l'export excel.
     *
     * @return
     */
    public List<ChampFusionDTO> recupereListeChamps() {
        return tableauChampFusionSimple.recupereListeChamps();
    }


    public void setActiverTriParPosition(boolean activerTriParPosition) {
        tableauChampFusionSimple.setActiverTriParPosition(activerTriParPosition);
        tableauChampFusionSimple.raffraichirTableau();
    }

    public void initialiser(TypeDocumentModele typeDocumentModele, List<DomaineDTO> domaines, List<ChampFusionDTO> champs) {
        super.initialiser(typeDocumentModele, domaines, champs, false);
    }

}
