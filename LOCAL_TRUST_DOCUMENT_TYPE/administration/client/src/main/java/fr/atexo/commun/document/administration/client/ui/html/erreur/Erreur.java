/**
 * 
 */
package fr.atexo.commun.document.administration.client.ui.html.erreur;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Léon Barsamian
 *
 */
public class Erreur extends Composite {

    private static ErreurUiBinder uiBinder = GWT.create(ErreurUiBinder.class);

    interface ErreurUiBinder extends UiBinder<Widget, Erreur> {
    }

    @UiField
    protected DivElement divErreur;
    
    @UiField
    protected VerticalPanel listeErreurs;
    

    /**
     * Because this class has a default constructor, it can
     * be used as a binder template. In other words, it can be used in other
     * *.ui.xml files as follows:
     * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
     *   xmlns:g="urn:import:**user's package**">
     *  <g:**UserClassName**>Hello!</g:**UserClassName>
     * </ui:UiBinder>
     * Note that depending on the widget that is used, it may be necessary to
     * implement HasHTML instead of HasText.
     */
    public Erreur() {
        initWidget(uiBinder.createAndBindUi(this));
        this.setVisible(false);
    }
    
    public void ajouterErreur(final String message) {
        LigneErreur erreur = new LigneErreur();
        erreur.setMessage(message);
        listeErreurs.add(erreur);
        this.setVisible(true);
    }
    
    public void nettoyer() {
        listeErreurs.clear();
        this.setVisible(false);
    }

    

}
