/**
 * 
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.commun.SelectDTO;

/**
 * @author Léon Barsamian
 */
public class SelectionMultiple extends Composite {

    private static SelectionMultipleUiBinder uiBinder = GWT.create(SelectionMultipleUiBinder.class);

    interface SelectionMultipleUiBinder extends UiBinder<Widget, SelectionMultiple> {
    }

    private List<SelectDTO> etapes = new ArrayList();

    @UiField
    protected LegendElement lblLegende;

    @UiField
    protected DivElement divTitreListeSource;

    @UiField
    protected ListBox selectListeSource;

    @UiField
    protected ListBox selectListeCible;

    @UiField
    protected DivElement divTitreListeCible;

    @UiField
    protected Button deplaceUnElementVersCible;

    @UiHandler("deplaceUnElementVersCible")
    void gestionClickDeplaceUnElementVersCible(ClickEvent e) {
        deplacerUnElement(selectListeSource, selectListeCible);
    }

    @UiField
    protected Button deplaceTousElementsVersCible;

    @UiHandler("deplaceTousElementsVersCible")
    void gestionClickDeplaceTousElementsVersCible(ClickEvent e) {
        deplacerTousElements(selectListeSource, selectListeCible);
    }

    @UiField
    protected Button deplaceUnElementVersSource;

    @UiHandler("deplaceUnElementVersSource")
    void gestionClickDeplaceUnElementVersSource(ClickEvent e) {
        deplacerUnElement(selectListeCible, selectListeSource);
    }

    @UiField
    protected Button deplaceTousElementsVersSource;

    @UiHandler("deplaceTousElementsVersSource")
    void gestionClickDeplaceTousElementsVersSource(ClickEvent e) {
        deplacerTousElements(selectListeCible, selectListeSource);
    }

    /**
     * Consutructeur.
     */
    public SelectionMultiple() {
        initWidget(uiBinder.createAndBindUi(this));
    }


    /**
     * Initialisation des libellés.
     * @param titre Titre du fieldset
     * @param titreSource titre du select source
     * @param titreSelection titre du select cible.
     */
    public void initLabel(final String titre, final String titreSource, final String titreSelection) {
    	lblLegende.setTitle(titre);
    	lblLegende.setInnerText(titre);
    	divTitreListeSource.setInnerText(titreSource);
    	divTitreListeCible.setInnerText(titreSelection);
    }

    /**
     * Déplace le ou les item sélectionné de la liste source vers la liste
     * cible.
     * @param source liste source
     * @param cible liste cible
     */
    private void deplacerUnElement(final ListBox source, final ListBox cible) {
        if (source.getSelectedIndex() != -1) {
            for (int index = source.getItemCount()-1; index >= 0; index--) {
                if (source.isItemSelected(index)) {
                    cible.addItem(source.getItemText(index), source.getValue(index));
                    source.removeItem(index);
                }
            }
        }
    }

    /**
     * Déplace tous les items de la liste source vers la liste cible.
     * @param source liste source
     * @param cible liste cible
     */
    private void deplacerTousElements(final ListBox source, final ListBox cible) {
        for (int index = source.getItemCount()-1; index >= 0; index--) {
            cible.addItem(source.getItemText(index), source.getValue(index));
            source.removeItem(index);
        }
    }

    /**
     * Intialisation des select source et cible.
     * @param listeSource liste des données à inserer dans le select source
     * @param listeCible liste des données à inserer dans le select cible
     */
    public void initialiser(List<? extends SelectDTO> listeSource,
            List<? extends SelectDTO> listeCible) {
        remplirListe(selectListeSource, listeSource);
        remplirListe(selectListeCible, listeCible);

        if (etapes.isEmpty()) {
            this.setVisible(false);
        }

    }

    /**
     * Remplis une selectbox avec des données source.
     * @param listeARemplir {@link ListBox} selectbox à remplir
     * @param listeDonnees List<? extends SelectDTO> liste de données à remplir
     */
    private void remplirListe(final ListBox listeARemplir, List<? extends SelectDTO> listeDonnees) {
        if (listeDonnees != null) {
            for (SelectDTO selectDTO : listeDonnees) {
                etapes.add(selectDTO);
                listeARemplir.addItem(selectDTO.getLibelle(), selectDTO.getCode());
            }
        }
    }

    /**
     * @param documentResultat
     */
    public List<? extends SelectDTO> modifierDisponible() {
        List<SelectDTO> resultat = new ArrayList<SelectDTO>();
        if (this.isVisible()) {
            resultat = restournerListe(selectListeSource);
        }
        return resultat;
    }
    
    /**
     * @param documentResultat
     */
    public List<? extends SelectDTO> modifierSelection() {
        List<SelectDTO> resultat = new ArrayList<SelectDTO>();
        if (this.isVisible()) {
            resultat = restournerListe(selectListeCible);
        }
        return resultat;
    }
    
    private List<SelectDTO> restournerListe(final ListBox listeBox) {
        List<SelectDTO> resultat = new ArrayList<SelectDTO>();
        for (int index = 0; index < listeBox.getItemCount(); index++) {
            String code = listeBox.getValue(index);
            SelectDTO resultatDTO = retournerElementParCode(code);
            if (resultatDTO != null) {
                resultat.add(resultatDTO);
            }
        }
        
        return resultat;
    }
    
    private SelectDTO retournerElementParCode(final String code) {
        SelectDTO resultat = null;
        for (SelectDTO objet : etapes) {
            if (code.equals(objet.getCode())) {
                return objet;
            }
        }
        return resultat;
    }
    
    public void setVisible(final boolean visible) {
        if (visible) {
            if (!etapes.isEmpty()) {
                super.setVisible(true);
            }
        } else {
            super.setVisible(false);
        }
    }

}
