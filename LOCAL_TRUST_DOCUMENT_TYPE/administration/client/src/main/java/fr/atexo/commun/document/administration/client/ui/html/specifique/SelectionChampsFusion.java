/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.UIObject;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DomaineDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;

/**
 * @author Léon Barsamian
 */
public abstract class SelectionChampsFusion extends Composite {

    protected Map<String, List<String>> mapDomaineParPointDepart = new HashMap<String, List<String>>();

    protected Map<String, List<ChampFusionDTO>> mapChampFusionParDomaine = new HashMap<String, List<ChampFusionDTO>>();

    protected final static String SELECTIONNEZ_ITEM = "Sélectionnez...";

    private Dictionnaire dictionnaire;

    @UiField
    protected LegendElement lblLegende;

    @UiField
    protected DivElement ligneTypeDomaine;

    @UiField
    protected DivElement divTypeDomaine;

    /**
     * Liste des points de départ de domaines de données affiché à l'écran.
     */
    @UiField
    protected ListBox listePointDepartDomaine;

    private String pointDepartCodeDomaine;

    private boolean pointDepartCodeDomaineObligatoire;

    @UiHandler("listePointDepartDomaine")
    void gestionListeTypeDomaines(ChangeEvent e) {
        listeDomaines.clear();
        listeDomaines.insertItem(SELECTIONNEZ_ITEM, SELECTIONNEZ_ITEM, -1);
        listeSousDomaines.clear();
        String typeDomaineSelectionne = listePointDepartDomaine.getValue(listePointDepartDomaine.getSelectedIndex());

        if (this.pointDepartCodeDomaine == null) {
            this.pointDepartCodeDomaine = typeDomaineSelectionne;
        }

        if (pointDepartCodeDomaineObligatoire && !this.pointDepartCodeDomaine.equals(typeDomaineSelectionne)) {

            if (Window.confirm(dictionnaire.get("messageReinitialisationChamps"))) {
                nettoyerTableau();
                initialiserDomainesDepuisPointDepart(typeDomaineSelectionne);
            } else {
                selectionnner(listePointDepartDomaine, this.pointDepartCodeDomaine);
                listeDomaines.clear();
                listeDomaines.insertItem(SELECTIONNEZ_ITEM, SELECTIONNEZ_ITEM, -1);
                initialiserDomainesDepuisPointDepart(this.pointDepartCodeDomaine);
                listeSousDomaines.clear();
            }
        } else {
            initialiserDomainesDepuisPointDepart(typeDomaineSelectionne);
        }


    }

    private void selectionnner(ListBox listBox, String valeur) {
        int index = -1;
        for (int i = 0; i < listBox.getItemCount(); i++) {
            if (listBox.getValue(i).equals(valeur)) {
                index = i;
                break;
            }
        }
        listBox.setSelectedIndex(index);
    }

    private void initialiserDomainesDepuisPointDepart(String typeDomaineSelectionne) {
        this.pointDepartCodeDomaine = typeDomaineSelectionne;
        List<String> listeDomaine = mapDomaineParPointDepart.get(this.pointDepartCodeDomaine);
        if (listeDomaine != null) {
            for (String domaine : listeDomaine) {
                listeDomaines.insertItem(domaine, domaine, -1);
            }

        } else {
            listeDomaines.clear();
            listeSousDomaines.clear();
        }
    }

    @UiField
    protected DivElement divDomaine;

    /**
     * Liste des domaines de données affiché à l'écran.
     */
    @UiField
    protected ListBox listeDomaines;


    @UiHandler("listeDomaines")
    void gestionListeDomaines(ChangeEvent e) {
        listeSousDomaines.clear();
        listeSousDomaines.insertItem(SELECTIONNEZ_ITEM, SELECTIONNEZ_ITEM, -1);
        String nom = listeDomaines.getValue(listeDomaines.getSelectedIndex());
        List<ChampFusionDTO> listeChamp = mapChampFusionParDomaine.get(nom);
        if (listeChamp != null) {
            for (ChampFusionDTO champFusionDTO : listeChamp) {
                String affichage = champFusionDTO.getAffichage() == null ? champFusionDTO.getNom() : champFusionDTO.getAffichage();
                listeSousDomaines.insertItem(affichage, champFusionDTO.getNom(), -1);
            }

        }
    }

    @UiField
    protected DivElement divSousDomaine;

    /**
     * Liste des champs de fusion d'un domaine.
     */
    @UiField
    protected ListBox listeSousDomaines;

    protected abstract void nettoyerTableau();

    @UiHandler("listeSousDomaines")
    protected abstract void gestionListeSousDomaines(ChangeEvent e);

    /**
     * Initiation du select domaine.
     *
     * @param typeDocumentModele le type de modèle
     * @param domaines           la liste des domaines
     * @param champs             la liste des champs de fusion
     * @param champComplexe      true s'il s'agit d'un champ complexe, sinon false
     */
    protected void initialiser(TypeDocumentModele typeDocumentModele, List<DomaineDTO> domaines, List<ChampFusionDTO> champs, final boolean champComplexe) {
        mapChampFusionParDomaine.clear();
        mapDomaineParPointDepart.clear();
        listePointDepartDomaine.clear();
        listeDomaines.clear();
        listeSousDomaines.clear();
        this.pointDepartCodeDomaineObligatoire = typeDocumentModele == TypeDocumentModele.RequeteLibre;

        this.pointDepartCodeDomaine = getPointDepartCodeDomaine(champs, champComplexe);

        listePointDepartDomaine.insertItem(SELECTIONNEZ_ITEM, SELECTIONNEZ_ITEM, -1);

        listeDomaines.insertItem(SELECTIONNEZ_ITEM, SELECTIONNEZ_ITEM, -1);

        for (DomaineDTO domaineDTO : domaines) {

            // remplissage de la liste des types de domaines et de la map des domaines associé à un type de domaine
            List<String> listeNomDomaine = null;
            if (domaineDTO.getPointDepartNom() != null && domaineDTO.getPointDepartCode() != null) {
                listeNomDomaine = mapDomaineParPointDepart.get(domaineDTO.getPointDepartCode());
                if (listeNomDomaine == null) {
                    listeNomDomaine = new ArrayList<String>();
                    mapDomaineParPointDepart.put(domaineDTO.getPointDepartCode(), listeNomDomaine);
                    listePointDepartDomaine.insertItem(domaineDTO.getPointDepartNom(), domaineDTO.getPointDepartCode(), -1);
                }
                if (!listeNomDomaine.contains(domaineDTO.getNom())) {
                    listeNomDomaine.add(domaineDTO.getNom());
                }
            }

            // remplissage de la liste des domaines et la map des champs de fusions associés à un domaine
            List<ChampFusionDTO> listeChamp = new ArrayList<ChampFusionDTO>();
            if (domaineDTO.getDataFusionDTOs() != null) {

                for (ChampFusionDTO champFusionDTO : domaineDTO.getDataFusionDTOs()) {
                    if (champFusionDTO.isComplexe() == champComplexe) {
                        listeChamp.add(champFusionDTO);
                    }
                }
            }

            listeDomaines.insertItem(domaineDTO.getNom(), domaineDTO.getNom(), -1);

            mapChampFusionParDomaine.put(domaineDTO.getNom(), listeChamp);
        }

        // dans le cas où le point de départ est obligatoire alors on ne rempli pas la liste des domaines
        if (pointDepartCodeDomaineObligatoire) {
            listeDomaines.clear();
        }

        // dans le cas où aucun type de domaine n'est présent alors on n'affiche pas la liste des types de domaines
        if (mapDomaineParPointDepart.isEmpty()) {
            listePointDepartDomaine.clear();
            UIObject.setVisible(ligneTypeDomaine, false);
        }
    }

    protected ChampFusionDTO chercherChampDeFusion(final String nomDomaine, final String nomChamp) {
        List<ChampFusionDTO> listeChamp = mapChampFusionParDomaine.get(nomDomaine);
        if (listeChamp != null) {
            for (ChampFusionDTO champFusionDTO : listeChamp) {
                if (nomChamp.equals(champFusionDTO.getNom())) {
                    return champFusionDTO;
                }
            }

        }
        return null;
    }

    public String getPointDepartCodeDomaine(List<ChampFusionDTO> champs, final boolean champComplexe) {

        if (champs == null || champs.isEmpty()) {
            return null;
        } else {

            for (ChampFusionDTO champ : champs) {

                if (champComplexe && champ.isComplexe()) {
                    return champ.getPointDepartCodeDuDomaine();
                } else if (!champComplexe && !champ.isComplexe()) {
                    return champ.getPointDepartCodeDuDomaine();
                }
            }
        }

        return null;

    }

    public Dictionnaire getDictionnaire() {
        return dictionnaire;
    }

    public void setDictionnaire(Dictionnaire dictionnaire) {
        this.dictionnaire = dictionnaire;
    }
}
