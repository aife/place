package fr.atexo.commun.document.administration.client.ui.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.RadioButton;

import fr.atexo.commun.document.administration.client.DocumentModule;
import fr.atexo.commun.document.administration.client.ui.html.erreur.Erreur;
import fr.atexo.commun.document.administration.client.ui.html.specifique.CriteresAffichage;
import fr.atexo.commun.document.administration.client.ui.html.specifique.Denomination;
import fr.atexo.commun.document.administration.client.ui.html.specifique.Editeur;
import fr.atexo.commun.document.administration.client.ui.html.specifique.EditeurSimple;
import fr.atexo.commun.document.administration.client.ui.html.specifique.EditeurXml;
import fr.atexo.commun.document.administration.client.ui.html.specifique.SelectionChampsFusionComplexes;
import fr.atexo.commun.document.administration.client.ui.html.specifique.SelectionChampsFusionSimples;
import fr.atexo.commun.document.administration.client.ui.html.specifique.SelectionMultiple;
import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.ConstantesDocument;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.GenerationRequeteLibreAsyncStatus;
import fr.atexo.commun.document.administration.commun.GenerationRequeteLibreAsyncStatus.StatusEnum;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.administration.commun.util.XMLUtil;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader;
import gwtupload.client.IUploader.OnFinishUploaderHandler;

/**
 * @author RME
 */
public class Document extends Composite {

	interface DocumentBinder extends UiBinder<HTMLPanel, Document> {
	}

	private static DocumentBinder documentBinder = GWT.create(DocumentBinder.class);

	private InitialisationDTO initialisation;

	private LoadingCallBack loadingCallBack;

	private static Dictionnaire dictionnaire;

	@UiField
	protected Frame frameGenerationDocument;

	@UiField
	protected Erreur tableauErreur;

	@UiField
	protected EditeurSimple editeurSimple;

	@UiField
	protected Editeur editeur;

	@UiField
	protected EditeurXml editeurXml;

	@UiField
	protected Denomination denomination;

	@UiField
	protected SelectionMultiple dispositifEtape;

	@UiField
	protected SelectionMultiple profil;

	@UiField
	protected SelectionMultiple commission;

	@UiField
	protected SelectionChampsFusionSimples selectionChampsFusionSimples;

	@UiField
	protected SelectionChampsFusionComplexes selectionChampsFusionComplexes;

	@UiField
	protected CriteresAffichage criteresAffichage;

	@UiField
	protected Anchor btnAnnuler;

	@UiField
	protected Anchor btnValider;

	@UiField
	protected Anchor btnGenererDocumentRequeteLibre;

	public Document() {
		HTMLPanel rootPanel = documentBinder.createAndBindUi(this);
		this.initWidget(rootPanel);

		denomination.ajouterEcouteurCourriel(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				RadioButton bouton = (RadioButton) event.getSource();
				if (bouton.getValue()) {

					denomination.initialisationTypeCourriel();
					denomination.reinitiliaserTemplate();

					initAffichageCourriel(Document.this.initialisation.isActiverTemplateMailHTML());

					selectionChampsFusionSimples.nettoyerTableau();
					selectionChampsFusionSimples.initialiser(TypeDocumentModele.Mail, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
					selectionChampsFusionComplexes.nettoyerTableau();
					selectionChampsFusionComplexes.initialiser(TypeDocumentModele.Mail, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
				}
			}
		});

		denomination.ajouterEcouteurDocumentOdt(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				RadioButton bouton = (RadioButton) event.getSource();
				if (bouton.getValue()) {

					denomination.initialisationTypeDocument();
					denomination.reinitiliaserTemplate();
					initAffichageDocument();

					selectionChampsFusionSimples.nettoyerTableau();
					selectionChampsFusionSimples.initialiser(TypeDocumentModele.Document, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
					selectionChampsFusionComplexes.nettoyerTableau();
					selectionChampsFusionComplexes.initialiser(TypeDocumentModele.Document, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
				}
			}
		});

		denomination.ajouterEcouteurDocumentExcel(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				RadioButton bouton = (RadioButton) event.getSource();
				if (bouton.getValue()) {

					TypeDocumentModele type = denomination.getTypDocument();

					denomination.initialisationTypeExcel();
					initAffichageExcel();

					if (type == TypeDocumentModele.RequeteLibre) {

						List<ChampFusionDTO> champs = selectionChampsFusionSimples.recupereListeChamps();
						String pointDepartCodeDomaine = selectionChampsFusionComplexes.getPointDepartCodeDomaine(champs, false);
						if (pointDepartCodeDomaine != null) {
							String codeXML = XMLUtil.getCodeFreeMarkerXML(pointDepartCodeDomaine, 1, champs,
									initialisation.isActiverVerificationChampsNulsGenerationCodeXML(),
									initialisation.getFormatNumeriqueCodeXML(), initialisation.getFormatDateFreemarker(),
									initialisation.getFormatDateTimeFreemarker());

							editeurXml.setContenu(codeXML);
						}
						selectionChampsFusionComplexes.nettoyerTableau();
					} else {
						denomination.reinitiliaserTemplate();
						selectionChampsFusionSimples.nettoyerTableau();
						selectionChampsFusionComplexes.nettoyerTableau();
					}

					selectionChampsFusionSimples.initialiser(TypeDocumentModele.Excel, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
					selectionChampsFusionComplexes.initialiser(TypeDocumentModele.Excel, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
				}
			}
		});

		denomination.ajouterEcouteurRequeteLibre(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				RadioButton bouton = (RadioButton) event.getSource();
				if (bouton.getValue()) {

					denomination.initialisationTypeRequeteLibre();
					denomination.reinitiliaserTemplate();
					initAffichageRequeteLibre();

					selectionChampsFusionSimples.nettoyerTableau();
					selectionChampsFusionSimples.initialiser(TypeDocumentModele.RequeteLibre, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
					selectionChampsFusionComplexes.nettoyerTableau();
					selectionChampsFusionComplexes.initialiser(TypeDocumentModele.RequeteLibre, initialisation.getDomainesDTO(),
							initialisation.getDocumentModeleDTO().getChampFusionsSelectionnes());
				}
			}
		});

		Map<String, SelectionMultiple> mapDeversoire = new HashMap<String, SelectionMultiple>();
		mapDeversoire.put(ConstantesDocument.DEVERSOIRE_DISPOSITIF, dispositifEtape);
		mapDeversoire.put(ConstantesDocument.DEVERSOIRE_PROFILS, profil);
		mapDeversoire.put(ConstantesDocument.DEVERSOIRE_COMMISSION, commission);
		denomination.setMapDeversoire(mapDeversoire);
		frameGenerationDocument.setVisible(false);
		frameGenerationDocument.setPixelSize(0, 0);

		initialisationLibelle();
	}

	/**
	 * Initialisation des libellés de l'application.
	 */
	private void initialisationLibelle() {
		dictionnaire = Dictionnaire.getInstanceDictionnaireDocumentType();
		denomination.initLabel(dictionnaire, tableauErreur);
		editeurSimple.initLabel(dictionnaire);
		editeur.initLabel(dictionnaire);
		dispositifEtape.initLabel(dictionnaire.get("dispositifsTitre"), dictionnaire.get("dispositifsSelectSource"),
				dictionnaire.get("dispositifsSelectCible"));
		profil.initLabel(dictionnaire.get("profilTitre"), dictionnaire.get("profilSelectSource"),
				dictionnaire.get("profilSelectCible"));
		commission.initLabel(dictionnaire.get("commissionTitre"), dictionnaire.get("commissionSelectSource"),
				dictionnaire.get("commissionSelectCible"));
		editeurXml.initLabel(dictionnaire);
		selectionChampsFusionSimples.initLabel(dictionnaire);
		selectionChampsFusionComplexes.initLabel(dictionnaire);
		btnAnnuler.setText(dictionnaire.get("annuler"));
		btnValider.setText(dictionnaire.get("valider"));
		btnGenererDocumentRequeteLibre.setText(dictionnaire.get("generer"));
		criteresAffichage.initLabel(dictionnaire);
	}

	@UiHandler("btnAnnuler")
	void gestionClickAnnuler(ClickEvent e) {
		annulerJavascript();
	}

	@UiHandler("btnValider")
	void gestionClickValider(ClickEvent e) {
		sousMettreFormulaire(true);
	}

	@UiHandler("btnGenererDocumentRequeteLibre")
	void genererDocumentRequeteLibre(ClickEvent e) {
		sousMettreFormulaire(false);
	}

	private void sousMettreFormulaire(final boolean validation) {
		tableauErreur.nettoyer();
		if (validerFormulaire()) {
			if (denomination.isEnvoiFichier()) {

				if (denomination.isFormatFichierValide()) {

					denomination.getFichier().addOnFinishUploadHandler(new OnFinishUploaderHandler() {

						@Override
						public void onFinish(IUploader uploader) {
							if (uploader.getStatus() == Status.SUCCESS) {
								IUploader.UploadedInfo infos = uploader.getServerInfo();
								String token = infos.message;
								if (validation) {
									valider(token);
								} else {
									genererDocument(token);
								}

							} else {
								ajouterErreur(dictionnaire.get("erreurEnregistrementFichier"));
							}
						}
					});
					denomination.getFichier().submit();
				} else {
					ajouterErreur(dictionnaire.get("erreurEnregistrementFichierInvalide"));
				}
			} else {
				if (validation) {
					valider(null);
				} else {
					genererDocument(null);
				}
			}
		} else {
			Window.scrollTo(tableauErreur.getAbsoluteLeft(), tableauErreur.getAbsoluteTop());
		}
	}

	private boolean validerFormulaire() {
		boolean valide = false;
		valide = denomination.validerDenomination(tableauErreur);

		if (initialisation.isActiverGestionCriteresAffichage()) {
			List<String> erreursList = new ArrayList<String>();
			criteresAffichage.getEtat(erreursList);

			valide = erreursList.isEmpty();
			if (!erreursList.isEmpty()) {
				for (String erreur : erreursList) {
					tableauErreur.ajouterErreur(erreur);
				}
			}
		}
		return valide;
	}

	/**
	 * Validation du formulaire pour activer l'enregistrement du modèle de
	 * documents.
	 * 
	 * @param tokenFichier
	 *            le token en session du fichier généré
	 */
	private void valider(String tokenFichier) {
		tableauErreur.nettoyer();
		DocumentModeleDTO resultat = modifierDocument();
		DocumentModule.getService().validerDocumentModele(resultat, tokenFichier, validationCallBack());
	}

	/**
	 * Génération Excel pour l'execution de la requete libre.
	 * 
	 * @param tokenFichier
	 *            le token en session du fichier généré
	 */
	private void genererDocument(String tokenFichier) {

		tableauErreur.nettoyer();
		DocumentModeleDTO resultat = modifierDocument();

		if (!initialisation.isActiverGenerationAsynchroneRequeteLibre()) {
			AsyncCallback<String> callback = new AsyncCallback<String>() {
				@Override
				public void onFailure(final Throwable erreur) {
					ajouterErreur(dictionnaire.get("erreurGenerationDocumentRequeteLibre"));
					if (loadingCallBack != null) {
						loadingCallBack.onError();
					}
				}

				@Override
				public void onSuccess(final String url) {
					if (url != null) {
						frameGenerationDocument.setUrl(url);
					} else {
						frameGenerationDocument.setUrl("/recupererGenerationDocumentRequeteLibre.srv");
					}
					if (loadingCallBack != null) {
						loadingCallBack.onFinish();
					}
				}
			};

			if (loadingCallBack != null) {
				loadingCallBack.onStart();
			}
			DocumentModule.getService().genererDocumentRequeteLibre(resultat, tokenFichier, callback);
		} else {
			if (loadingCallBack != null) {
				loadingCallBack.onStart();
			}

			DocumentModule.getService().genererDocumentRequeteLibreAsync(resultat, tokenFichier, new AsyncCallback<String>() {
				@Override
				public void onSuccess(final String token) {
					Timer timer = new Timer() {
						@Override
						public void run() {

							DocumentModule.getService().recupererDocumentRequeteLibreAsync(token,
									new AsyncCallback<GenerationRequeteLibreAsyncStatus>() {
										@Override
										public void onSuccess(GenerationRequeteLibreAsyncStatus status) {
											if (status.getStatus() == StatusEnum.ERREUR) {
												cancel();
												if (loadingCallBack != null) {
													loadingCallBack.onError();
												}
												ajouterErreur(dictionnaire.get("erreurGenerationDocumentRequeteLibre"));
											} else if (status.getStatus() == StatusEnum.EN_COURS) {

												GWT.log("La requête libre ayant pour token : " + token
														+ " est en cours de traitement");
											} else {
												if (status.getUrl() != null) {
													frameGenerationDocument.setUrl(status.getUrl());
												} else {
													frameGenerationDocument
															.setUrl("/recupererGenerationDocumentRequeteLibre.srv");
												}
												cancel();
												if (loadingCallBack != null) {
													loadingCallBack.onFinish();
												}
											}
										}

										@Override
										public void onFailure(Throwable caught) {
											cancel();
											if (loadingCallBack != null) {
												loadingCallBack.onError();
											}
											ajouterErreur(dictionnaire.get("erreurGenerationDocumentRequeteLibre"));
										}
									});
						}
					};

					// Schedule the timer to run once in 5 seconds.
					timer.scheduleRepeating(5 * 1000);
				}

				@Override
				public void onFailure(Throwable caught) {
					if (loadingCallBack != null) {
						loadingCallBack.onError();
					}
					ajouterErreur(dictionnaire.get("erreurGenerationDocumentRequeteLibre"));
				}
			});
		}
	}

	private AsyncCallback<Void> validationCallBack() {
		AsyncCallback<Void> callback = new AsyncCallback<Void>() {
			@Override
			public void onFailure(final Throwable erreur) {
				GWT.log(erreur.getMessage());
				ajouterErreur(dictionnaire.get("erreurEnregistrement"));
			}

			@Override
			public void onSuccess(Void arg0) {
				validerJavascript();
			}

		};
		return callback;
	}

	private void initAffichageDocument() {
		editeur.setVisible(false);
		editeurSimple.setContenu(null);
		editeurSimple.setVisible(false);
		editeurXml.setContenu(null);
		editeurXml.setVisible(false);
		selectionChampsFusionSimples.setActiverTriParPosition(false);
		selectionChampsFusionComplexes.setVisible(initialisation.isActiverDomaineChampFusionComplexe());
	}

	private void initAffichageCourriel(boolean modeHtml) {
		if (modeHtml) {
			editeur.setVisible(true);
			editeurSimple.setVisible(false);
			editeurSimple.setContenu("");
		} else {
			editeurSimple.setVisible(true);
			editeur.setVisible(false);
		}
		editeurXml.setContenu(null);
		editeurXml.setVisible(false);
		selectionChampsFusionSimples.setActiverTriParPosition(false);
		selectionChampsFusionComplexes.setVisible(initialisation.isActiverDomaineChampFusionComplexe());
	}

	private void initAffichageExcel() {
		editeur.setVisible(false);
		editeurSimple.setContenu(null);
		editeurSimple.setVisible(false);
		editeurXml.setVisible(true);
		selectionChampsFusionSimples.setActiverTriParPosition(false);
		selectionChampsFusionComplexes.setVisible(initialisation.isActiverDomaineChampFusionComplexe());
	}

	private void initAffichageRequeteLibre() {
		editeur.setVisible(false);
		editeurSimple.setContenu(null);
		editeurSimple.setVisible(false);
		editeurXml.setVisible(false);
		editeurXml.setContenu(null);
		selectionChampsFusionSimples.setActiverTriParPosition(true);
		selectionChampsFusionComplexes.setVisible(false);
	}

	/**
	 * Initialisation de l'affichage à partir de {@link InitialisationDTO}
	 * 
	 * @param donnees
	 */
	public void initialiser(InitialisationDTO donnees) {

		initialisation = donnees;

		editeur.initialisation(initialisation.getDocumentModeleDTO());

		dispositifEtape.initialiser(initialisation.getDocumentModeleDTO().getEtapesDisponibles(), initialisation
				.getDocumentModeleDTO().getEtapesSelectionnees());
		profil.initialiser(initialisation.getDocumentModeleDTO().getProfilsDisponibles(), initialisation.getDocumentModeleDTO()
				.getProfilsSelectionnes());
		commission.initialiser(initialisation.getDocumentModeleDTO().getCommissionsDisponibles(), initialisation
				.getDocumentModeleDTO().getCommissionsSelectionnees());
		denomination.initialiser(donnees);

		if (TypeDocumentModele.Document == denomination.getTypDocument()) {
			initAffichageDocument();
			editeurXml.initialisation(initialisation.getDocumentModeleDTO());
		}

		if (TypeDocumentModele.Mail == denomination.getTypDocument()) {
			initAffichageCourriel(donnees.isActiverTemplateMailHTML());
			editeurSimple.initialisation(initialisation.getDocumentModeleDTO());
		}

		if (TypeDocumentModele.Excel == denomination.getTypDocument()) {
			initAffichageExcel();
			editeurXml.initialisation(initialisation.getDocumentModeleDTO());
		}

		if (TypeDocumentModele.RequeteLibre == denomination.getTypDocument()) {
			initAffichageRequeteLibre();
		}

		selectionChampsFusionSimples.initialisation(initialisation);
		selectionChampsFusionComplexes.initialisation(initialisation);

		btnGenererDocumentRequeteLibre.setVisible(donnees.isActiverGenerationDocumentRequeteLibre());
		btnValider.setVisible(donnees.isActiverValidation());

		criteresAffichage.setVisible(donnees.isActiverGestionCriteresAffichage());
		if (donnees.isActiverGestionCriteresAffichage()) {
			criteresAffichage.initialisation(initialisation);
		}
	}

	/**
	 * Met à jour le document utilisé lors de l'initialisation avec les
	 * nouvelles données.
	 * 
	 * @return
	 */
	public DocumentModeleDTO modifierDocument() {

		initialisation.setDocumentModeleDTO(denomination.modifier(initialisation.getDocumentModeleDTO()));

		if (editeur.isVisible()) {
			initialisation.setDocumentModeleDTO(editeur.modifier(initialisation.getDocumentModeleDTO()));
		}

		if (editeurSimple.isVisible()) {
			initialisation.setDocumentModeleDTO(editeurSimple.modifier(initialisation.getDocumentModeleDTO()));
		}

		initialisation.getDocumentModeleDTO().setEtapesDisponibles(dispositifEtape.modifierDisponible());
		initialisation.getDocumentModeleDTO().setEtapesSelectionnees(dispositifEtape.modifierSelection());
		initialisation.getDocumentModeleDTO().setProfilsDisponibles(profil.modifierDisponible());
		initialisation.getDocumentModeleDTO().setProfilsSelectionnes(profil.modifierSelection());

		initialisation.getDocumentModeleDTO().setCommissionsDisponibles(commission.modifierDisponible());
		initialisation.getDocumentModeleDTO().setCommissionsSelectionnees(commission.modifierSelection());

		initialisation.getDocumentModeleDTO().setChampFusionsSelectionnes(new ArrayList<ChampFusionDTO>());
		initialisation.setDocumentModeleDTO(selectionChampsFusionSimples.modifier(initialisation.getDocumentModeleDTO()));

		if (selectionChampsFusionComplexes.isVisible()) {
			initialisation.setDocumentModeleDTO(selectionChampsFusionComplexes.modifier(initialisation.getDocumentModeleDTO()));
		}

		if (editeurXml.isVisible()) {
			initialisation.setDocumentModeleDTO(editeurXml.modifier(initialisation.getDocumentModeleDTO()));
		} else {
			initialisation.getDocumentModeleDTO().setXml(null);
		}

		// dans le cas d'une requête libre alors on génére le code xml
		if (denomination.getTypDocument() == TypeDocumentModele.RequeteLibre) {
			List<ChampFusionDTO> champs = selectionChampsFusionSimples.recupereListeChamps();
			String pointDepartCodeDomaine = selectionChampsFusionComplexes.getPointDepartCodeDomaine(champs, false);
			if (pointDepartCodeDomaine != null) {
				String codeXML = XMLUtil.getCodeFreeMarkerXML(pointDepartCodeDomaine, 1, champs,
						initialisation.isActiverVerificationChampsNulsGenerationCodeXML());
				initialisation.getDocumentModeleDTO().setXml(codeXML);
			}
		}

		if (initialisation.isActiverGestionCriteresAffichage()) {
			List<String> erreurs = new ArrayList<String>();
			List<DocumentModeleCritereDTO> criteresList = criteresAffichage.getEtat(erreurs);
			initialisation.getDocumentModeleDTO().setCriteres(criteresList);
		}

		return initialisation.getDocumentModeleDTO();
	}

	public void ajouterErreur(final String message) {
		tableauErreur.ajouterErreur(message);
		Window.scrollTo(tableauErreur.getAbsoluteLeft(), tableauErreur.getAbsoluteTop());
	}

	/**
	 * Permet d'annuler l'ajout / modification du modèle de document.
	 */
	private static native void annulerJavascript()
	/*-{
	  $wnd.annulerDocumentService();
	}-*/;

	/**
	 * Permet de valider l'ajout / modification du modèle de document.
	 */
	private static native void validerJavascript()
	/*-{
	  $wnd.validerDocumentService();
	}-*/;

	/**
	 * @return the dictionnaire
	 */
	public static final Dictionnaire getDictionnaire() {
		return dictionnaire;
	}

	public void setLoadingCallBack(LoadingCallBack loadingCallBack) {
		this.loadingCallBack = loadingCallBack;
	}

}
