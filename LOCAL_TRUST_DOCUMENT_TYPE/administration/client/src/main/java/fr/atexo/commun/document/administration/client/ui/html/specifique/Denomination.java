/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.DocumentModule;
import fr.atexo.commun.document.administration.client.ui.html.erreur.Erreur;
import fr.atexo.commun.document.administration.client.ui.page.Document;
import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.InitialisationDTO;
import fr.atexo.commun.document.administration.commun.ItemSelectBox;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.administration.commun.TypeFormat;
import fr.atexo.commun.document.administration.commun.TypeVisibiliteServiceModele;
import gwtupload.client.Uploader;

/**
 * Représentation du fieldSet de dénomination.
 * 
 * @author Léon Barsamian
 */
public class Denomination extends Composite {

	private static DenominationUiBinder uiBinder = GWT.create(DenominationUiBinder.class);

	interface DenominationUiBinder extends UiBinder<Widget, Denomination> {
	}

	private Dictionnaire dictionnaire;

	protected Erreur tableauErreur;

	@UiField
	DivElement ligneTypeGeneration;

	@UiField
	protected SpanElement lblNom;

	@UiField
	protected SpanElement lblFormat;

	@UiField
	DivElement ligneNomTemplate;

	@UiField
	protected SpanElement lblNomTemplate;

	@UiField
	protected SpanElement lblTemplate;

	@UiField
	protected SpanElement lblTemplateObligatoire;

	@UiField
	protected LegendElement lblLegende;

	@UiField
	protected RadioButton btnDocument;

	ClickHandler btnDocumentClickHandler;

	@UiField
	protected RadioButton btnCourriel;

	ClickHandler btnCourrielClickHandler;

	@UiField
	protected RadioButton btnExcel;

	ClickHandler btnExcelClickHandler;

	@UiField
	RadioButton btnRequeteLibre;

	ClickHandler btnRequeteLibreClickHandler;

	@UiField
	protected Anchor nomTemplateUploadeLien;

	@UiField
	Label nomTemplateUploade;

	@UiField
	Frame frameTelechargementTemplate;

	@UiHandler("nomTemplateUploadeLien")
	protected void clickTelechargementTemplate(ClickEvent e) {
		telechargerTemplate();
	}

	/**
	 * Génération Excel de l'export des champs de fusion.
	 */
	private void telechargerTemplate() {

		if (idDocumentModele != null) {
			AsyncCallback<String> callback = new AsyncCallback<String>() {
				@Override
				public void onFailure(final Throwable erreur) {
					ajouterErreur(dictionnaire.get("erreurTelechargementTemplate"));
				}

				@Override
				public void onSuccess(final String url) {
					if (url != null) {
						frameTelechargementTemplate.setUrl(url);
					} else {
						frameTelechargementTemplate.setUrl("/recupererTemplate.srv");
					}
				}
			};
			DocumentModule.getService().telechargerTemplate(idDocumentModele, callback);
		}
	}

	public void ajouterErreur(final String message) {
		tableauErreur.ajouterErreur(message);
		Window.scrollTo(tableauErreur.getAbsoluteLeft(), tableauErreur.getAbsoluteTop());
	}

	@UiHandler("listeAffichageDeversoire")
	void gestionAffichageDeversoire(ChangeEvent e) {
		String code = listeAffichageDeversoire.getValue(listeAffichageDeversoire.getSelectedIndex());
		initialisationDeversoireExcel(code);
	}

	@UiField
	protected DivElement divAffichageDeversoire;

	@UiField
	protected DivElement divAffichageFormat;

	@UiField
	protected TextBox nomDocument;

	@UiField
	protected ListBox listeFormatDocumentType;

	@UiField
	protected DivElement divSpecifiqueDocument;

	@UiField
	protected Uploader fichier;

	@UiField
	protected DivElement lblAssociation;

	@UiField
	protected RadioButton btnAssociationOui;

	@UiField
	protected RadioButton btnAssociationNon;

	@UiField
	protected DivElement divSpecifiqueCourriel;

	@UiField
	protected SpanElement lblObjetCourriel;

	@UiField
	protected TextBox objetCourriel;

	@UiField
	protected ListBox listeAffichageDeversoire;

	@UiField
	protected SpanElement lblAffichageDeversoire;

	@UiField
	protected DivElement divVisibiliteService;

	@UiField
	protected RadioButton btnVisibiliteTransversal;

	@UiField
	protected RadioButton btnVisibiliteCertainsServices;

	private TypeDocumentModele typeDocument;

	private Map<String, TypeFormat> mapTypeFormatParLibelle = new HashMap<String, TypeFormat>();

	private boolean modification = false;

	/**
	 * Liste des déversoires associé à un document.
	 */
	private List<String> deversoirePourDocument = new ArrayList<String>();

	/**
	 * Association entre option dans le cas d'un fichier excel et la liste de
	 * deversoires à afficher. Les deversoires sont identifés via les constantes
	 * du fichier ConstantesDocument.java.
	 */
	private Map<String, List<String>> deversoireAfficheParOption = null;

	/**
	 * Message d'erreur pour les champs obligatoires.
	 */
	private String erreurChampsObligatoires = "";

	/**
	 * Message d'erreur pour les caractères qui peuvent pas être inclus dans le
	 * nom du document.
	 */
	private String erreurCaracteresInvalides = "";

	private Map<String, SelectionMultiple> mapDeversoire = null;

	/**
	 * Liste des formats affiché pour la génération d'un document ODT.
	 */
	private List<TypeFormat> listeFormatPourGenerationODT;

	/**
	 * Liste des formats affiché pour la génération d'un document XLS.
	 */
	private List<TypeFormat> listeFormatPourGenerationXLS;

	private Long idDocumentModele;

	/**
	 *
	 */
	public Denomination() {
		initWidget(uiBinder.createAndBindUi(this));
		btnDocument.setValue(true);
		UIObject.setVisible(ligneNomTemplate, false);
		fichier.setFileInputSize(103);

	}

	public void initLabel(final Dictionnaire dictionnaire, final Erreur tableauErreur) {

		this.dictionnaire = dictionnaire;
		this.tableauErreur = tableauErreur;

		btnDocument.setText(dictionnaire.get("denominationDocument"));
		btnCourriel.setText(dictionnaire.get("denominationCourriel"));
		btnExcel.setText(dictionnaire.get("denominationExcel"));
		btnRequeteLibre.setText(dictionnaire.get("denominationRequeteLibre"));
		lblLegende.setInnerText(dictionnaire.get("denominationTitre"));
		lblNom.setInnerText(dictionnaire.get("denominationNom"));
		lblFormat.setInnerText(dictionnaire.get("denominationFormat"));
		lblNomTemplate.setInnerText(dictionnaire.get("denominationNomTemplate"));
		lblTemplate.setInnerText(dictionnaire.get("denominationTemplateOdt"));
		lblAssociation.setInnerText(dictionnaire.get("denominationAssociation"));
		btnAssociationNon.setText(dictionnaire.get("denominationAssociationNon"));
		btnAssociationOui.setText(dictionnaire.get("denominationAssociationOui"));
		lblObjetCourriel.setInnerText(dictionnaire.get("denominationCourrielObjet"));
		lblAffichageDeversoire.setInnerHTML(dictionnaire.get("lblAffichageDeversoire"));
		btnVisibiliteTransversal.setText(dictionnaire.get("denominationVisibiliteTransversale"));
		btnVisibiliteCertainsServices.setText(dictionnaire.get("denominationVisibiliteParService"));
		erreurChampsObligatoires = dictionnaire.get("erreurChampsObligatoires");
		erreurCaracteresInvalides = dictionnaire.get("erreurCaracteresInvalides");
	}

	/**
	 * Affichage des informations du document.
	 */
	public void initialiser(final InitialisationDTO donnees) {

		nomTemplateUploadeLien.setVisible(donnees.isActiverTelechargementTemplate());
		nomTemplateUploade.setVisible(!donnees.isActiverTelechargementTemplate());

		idDocumentModele = donnees.getDocumentModeleDTO().getId();

		listeFormatPourGenerationODT = donnees.getListeFormatPourGenerationODT();
		listeFormatPourGenerationXLS = donnees.getListeFormatPourGenerationXLS();
		if (donnees.getDocumentModeleDTO().getId() != null && donnees.getDocumentModeleDTO().getId() != 0) {
			modification = true;
		}
		deversoirePourDocument = donnees.getDeversoirePourDocument();

		nomDocument.setText(donnees.getDocumentModeleDTO().getNom());

		if (donnees.getDocumentModeleDTO().getTypeDocument() != null) {
			typeDocument = donnees.getDocumentModeleDTO().getTypeDocument();
		}

		// template
		if (donnees.getDocumentModeleDTO().getNomTemplate() != null) {
			nomTemplateUploadeLien.setText(donnees.getDocumentModeleDTO().getNomTemplate());
			nomTemplateUploade.setText(donnees.getDocumentModeleDTO().getNomTemplate());
			UIObject.setVisible(ligneNomTemplate, true);

		}

		// association
		if (donnees.isActiverAssociation()) {

			UIObject.setVisible(lblAssociation.getParentElement(), true);

			if (donnees.getDocumentModeleDTO().isAssocier()) {
				btnAssociationOui.setValue(true);
			} else {
				btnAssociationNon.setValue(true);
			}
		}

		// déversoir
		String optionSelectionnee = "";
		int index = 0;
		if (donnees.getListeSelectionExcel() != null && !donnees.getListeSelectionExcel().isEmpty()
				&& donnees.getDeversoireAfficheParOption() != null) {
			deversoireAfficheParOption = donnees.getDeversoireAfficheParOption();
			for (int i = 0; i < donnees.getListeSelectionExcel().size(); i++) {
				ItemSelectBox item = donnees.getListeSelectionExcel().get(i);
				listeAffichageDeversoire.addItem(dictionnaire.get(item.getCleLibelle()), item.getCode());
				if (item.isOptionSelectionnee()) {
					optionSelectionnee = item.getCode();
					index = i;
				}
			}
		}

		// type de modèle Document
		if (typeDocument == TypeDocumentModele.Document) {
			btnDocument.setValue(true);
			initialisationTypeDocument();
		}

		// type de modèle Courrier
		if (typeDocument == TypeDocumentModele.Mail) {
			btnCourriel.setValue(true);
			initialisationTypeCourriel();
			objetCourriel.setText(donnees.getDocumentModeleDTO().getObjetCourriel());
		}

		// traitement specifique - type de modèle Excel
		if (typeDocument == TypeDocumentModele.Excel) {
			btnExcel.setValue(true);
			initialisationTypeExcel();
		}

		// type de modèle Requete Libre
		if (typeDocument == TypeDocumentModele.RequeteLibre) {
			btnRequeteLibre.setValue(true);
			initialisationTypeRequeteLibre();
		}

		// association gestion des déversoirs
		if (typeDocument == TypeDocumentModele.Excel || typeDocument == TypeDocumentModele.RequeteLibre) {
			if (deversoireAfficheParOption != null) {
				listeAffichageDeversoire.setSelectedIndex(index);
				if ("".equals(optionSelectionnee)) {
					optionSelectionnee = listeAffichageDeversoire.getValue(0);
				}
			}
			initialisationDeversoireExcel(optionSelectionnee);
		}

		UIObject.setVisible(ligneTypeGeneration, !donnees.isTypeGenerationUnique());

		if (!donnees.isActiverDocument()) {
			btnDocument.setVisible(false);
		}

		if (!donnees.isActiverMail()) {
			btnCourriel.setVisible(false);
		}
		if (!donnees.isActiverDocumentExcel()) {
			btnExcel.setVisible(false);
		}

		if (!donnees.isActiverRequeteLibre()) {
			btnRequeteLibre.setVisible(false);
		}

		// type de format (aprés l'initialisation faite de la liste des formats
		// en fonction du type de document)
		TypeFormat format = donnees.getDocumentModeleDTO().getTypeFormat();
		if (format != null) {
			int nbItem = listeFormatDocumentType.getItemCount();
			for (int indexFormat = 0; indexFormat < nbItem; indexFormat++) {
				if (format.getName().equals(listeFormatDocumentType.getItemText(indexFormat))) {
					listeFormatDocumentType.setSelectedIndex(indexFormat);
					break;
				}
			}
		}

		if (donnees.isActiverGestionVisibliteService()) {
			UIObject.setVisible(divVisibiliteService, true);
			if (donnees.getDocumentModeleDTO().getTypeVisibiliteService() != null) {
				switch (donnees.getDocumentModeleDTO().getTypeVisibiliteService()) {
				case Transversale:
					btnVisibiliteTransversal.setValue(true);
					break;

				case parService:
					btnVisibiliteCertainsServices.setValue(true);
					break;

				default:
					break;
				}
			} else {
				btnVisibiliteTransversal.setValue(true);
			}
		} else {
			UIObject.setVisible(divVisibiliteService, false);
		}
	}

	private void initialiserListeFormat(List<TypeFormat> formats) {
		listeFormatDocumentType.clear();
		mapTypeFormatParLibelle.clear();

		for (TypeFormat typeFormat : formats) {
			String nom = typeFormat.getName();
			listeFormatDocumentType.addItem(nom);
			mapTypeFormatParLibelle.put(nom, typeFormat);
		}
	}

	/**
	 * @param documentResultat
	 * @return le document modifié.
	 */
	public DocumentModeleDTO modifier(DocumentModeleDTO documentResultat) {
		documentResultat.setTypeDocument(typeDocument);
		documentResultat.setNom(nomDocument.getText());
		Integer indexFormatDocumentType = listeFormatDocumentType.getSelectedIndex();
		if (indexFormatDocumentType != -1) {
			String format = listeFormatDocumentType.getItemText(listeFormatDocumentType.getSelectedIndex());
			documentResultat.setTypeFormat(mapTypeFormatParLibelle.get(format));
		}
		if (documentResultat.getTypeDocument() == TypeDocumentModele.Mail) {
			documentResultat.setTypeFormat(TypeFormat.TEXTE);
		}

		if ((btnDocument.getValue() || btnExcel.getValue() || btnRequeteLibre.getValue()) && !"".equals(fichier.getFileName())) {
			String[] nomSplitte = fichier.getFileName().split("\\\\");
			String nomTemplate = fichier.getFileName();
			if (nomSplitte != null) {
				nomTemplate = nomSplitte[nomSplitte.length - 1];
			}
			documentResultat.setNomTemplate(nomTemplate);
		}
		if (btnCourriel.getValue()) {
			documentResultat.setObjetCourriel(objetCourriel.getText());
		}
		if (btnExcel.getValue() || btnRequeteLibre.getValue()) {
			if (deversoireAfficheParOption != null) {
				String code = listeAffichageDeversoire.getValue(listeAffichageDeversoire.getSelectedIndex());
				documentResultat.setChoixAffichageDeversoire(code);
			}
		}
		documentResultat.setAssocier(btnAssociationOui.getValue());

		if (btnVisibiliteTransversal.getValue()) {
			documentResultat.setTypeVisibiliteService(TypeVisibiliteServiceModele.Transversale);
		} else if (btnVisibiliteCertainsServices.getValue()) {
			documentResultat.setTypeVisibiliteService(TypeVisibiliteServiceModele.parService);
		}
		return documentResultat;
	}

	public final boolean validerDenomination(final Erreur erreur) {
		boolean valide = true;
		nomDocument.removeStyleName("error-border");
		fichier.getFileInput().getWidget().removeStyleName("error-border");
		objetCourriel.removeStyleName("error-border");

		// vérification sur les caractères compris dans le nom du document
		if (!isNomDocumentValide(nomDocument.getText())) {
			nomDocument.addStyleName("error-border");
			erreur.ajouterErreur(erreurCaracteresInvalides);
			valide = false;
		}

		if ("".equals(nomDocument.getText().trim())) {
			nomDocument.addStyleName("error-border");
			valide = false;
		}

		if ((btnDocument.getValue() || btnExcel.getValue())
				&& ("".equals(fichier.getFileName().trim()) && "".equals(nomTemplateUploade.getText().trim()))) {

			fichier.getFileInput().getWidget().addStyleName("error-border");
			valide = false;
		}

		if (btnCourriel.getValue()) {
			if ("".equals(objetCourriel.getText().trim())) {
				objetCourriel.addStyleName("error-border");
				valide = false;
			}
		}
		if (!valide) {
			erreur.ajouterErreur(erreurChampsObligatoires);
		}
		return valide;
	}

	/**
	 * Valide le nom du modèle du document s'il n'est pas vide ou s'il ne
	 * possède pas les caractères suivants / : * ? " < > |
	 * 
	 * @param nomDocument
	 * @return
	 */
	private boolean isNomDocumentValide(String nomDocument) {

		if (nomDocument == null || nomDocument.matches("([^\\/:*?\"<>|]*[\\/:*?\"<>|]+[^\\/:*?\"<>|]*)*")) {
			return false;
		}

		return true;

	}

	public void initialisationTypeDocument() {
		UIObject.setVisible(divSpecifiqueDocument, true);
		UIObject.setVisible(divSpecifiqueCourriel, false);
		UIObject.setVisible(lblTemplateObligatoire, true);
		affichageSpecifiqueDocument();
		initialisationDeversoireDocument();
		initialiserListeFormat(listeFormatPourGenerationODT);
		typeDocument = TypeDocumentModele.Document;
		lblTemplate.setInnerText(Document.getDictionnaire().get("denominationTemplateOdt"));
	}

	public void initialisationTypeCourriel() {
		UIObject.setVisible(divSpecifiqueDocument, false);
		UIObject.setVisible(divSpecifiqueCourriel, true);
		UIObject.setVisible(lblTemplateObligatoire, true);
		typeDocument = TypeDocumentModele.Mail;
		initialisationAffichageDeversoire(new ArrayList<String>());
	}

	public void initialisationTypeExcel() {
		UIObject.setVisible(lblTemplateObligatoire, true);
		initialisationTypeDocument();
		affichageSpecifiqueExcel();
		initialiserListeFormat(listeFormatPourGenerationXLS);
		typeDocument = TypeDocumentModele.Excel;
		String code = null;
		if (deversoireAfficheParOption != null) {
			code = listeAffichageDeversoire.getValue(listeAffichageDeversoire.getSelectedIndex());
		}
		initialisationDeversoireExcel(code);
		lblTemplate.setInnerText(Document.getDictionnaire().get("denominationTemplateXls"));
	}

	public void initialisationTypeRequeteLibre() {
		initialisationTypeExcel();
		typeDocument = TypeDocumentModele.RequeteLibre;
		UIObject.setVisible(lblTemplateObligatoire, false);
	}

	private void affichageSpecifiqueExcel() {
		if (deversoireAfficheParOption != null) {
			UIObject.setVisible(divAffichageDeversoire, true);
		}
	}

	private void affichageSpecifiqueDocument() {
		if (deversoireAfficheParOption != null) {
			UIObject.setVisible(divAffichageDeversoire, false);
		}
		UIObject.setVisible(divAffichageFormat, true);
	}

	private void initialisationDeversoireDocument() {
		initialisationAffichageDeversoire(deversoirePourDocument);
	}

	private void initialisationDeversoireExcel(final String code) {
		List<String> listeIdDeversoire = null;
		if (deversoireAfficheParOption != null) {
			listeIdDeversoire = deversoireAfficheParOption.get(code);
		}
		if (listeIdDeversoire == null) {
			listeIdDeversoire = deversoirePourDocument;
		}
		initialisationAffichageDeversoire(listeIdDeversoire);

	}

	public void reinitiliaserTemplate() {
		UIObject.setVisible(ligneNomTemplate, false);
		nomTemplateUploade.setText(null);
		nomTemplateUploadeLien.setText(null);
	}

	/**
	 * Rend visible des deveroire dont l'identifiant est dans la liste passée en
	 * paramétre. Les autres sont cachés.
	 * 
	 * @param listeIdDeversoire
	 */
	private void initialisationAffichageDeversoire(List<String> listeIdDeversoire) {
		for (String idDeversoire : mapDeversoire.keySet()) {
			boolean visible = false;
			if (listeIdDeversoire.contains(idDeversoire)) {
				visible = true;
			}
			mapDeversoire.get(idDeversoire).setVisible(visible);
		}
	}

	public void ajouterEcouteurDocumentOdt(ClickHandler handler) {
		btnDocument.addClickHandler(handler);
		btnDocumentClickHandler = handler;
	}

	public void selectionnerDocumentOdt() {
		ClickEvent clickEvent = new ClickEvent() {
			@Override
			public Object getSource() {
				return btnDocument;
			}
		};
		btnDocument.setValue(true);
		btnDocumentClickHandler.onClick(clickEvent);
	}

	public void ajouterEcouteurCourriel(ClickHandler handler) {
		btnCourriel.addClickHandler(handler);
		btnCourrielClickHandler = handler;
	}

	public void selectionnerCourriel() {
		ClickEvent clickEvent = new ClickEvent() {
			@Override
			public Object getSource() {
				return btnCourriel;
			}
		};
		btnCourriel.setValue(true);
		btnCourrielClickHandler.onClick(clickEvent);
	}

	public void ajouterEcouteurDocumentExcel(ClickHandler handler) {
		btnExcel.addClickHandler(handler);
		btnExcelClickHandler = handler;
	}

	public void selectionnerDocumentExcel() {
		ClickEvent clickEvent = new ClickEvent() {
			@Override
			public Object getSource() {
				return btnExcel;
			}
		};
		btnExcel.setValue(true);
		btnExcelClickHandler.onClick(clickEvent);
	}

	public void ajouterEcouteurRequeteLibre(ClickHandler handler) {
		btnRequeteLibre.addClickHandler(handler);
		btnRequeteLibreClickHandler = handler;
	}

	public void selectionnerRequeteLibre() {
		ClickEvent clickEvent = new ClickEvent() {
			@Override
			public Object getSource() {
				return btnRequeteLibre;
			}
		};
		btnRequeteLibre.setValue(true);
		btnRequeteLibreClickHandler.onClick(clickEvent);
	}

	public final TypeDocumentModele getTypDocument() {
		return typeDocument;
	}

	public final Uploader getFichier() {
		return fichier;
	}

	public final boolean isEnvoiFichier() {
		boolean envoi = false;
		if ((btnDocument.getValue() || btnExcel.getValue() || btnRequeteLibre.getValue())
				&& !"".equals(fichier.getFileName().trim())) {
			envoi = true;
		}
		return envoi;
	}

	public final boolean isFormatFichierValide() {
		boolean formatValide = false;

		String nomFichier = fichier.getFileName().trim();

		if (!"".equals(nomFichier)) {

			switch (typeDocument) {
			case Document:
				formatValide = validerExtension(new String[] { "odt" }, nomFichier);
				break;
			case Excel:
			case RequeteLibre:
				formatValide = validerExtension(new String[] { "xls", "xlsx" }, nomFichier);
				break;
			}

		}
		return formatValide;
	}

	private boolean validerExtension(String[] extensions, String nomFichier) {
		boolean valide = false;

		if (nomFichier != null) {

			for (int i = 0; i < extensions.length; i++) {

				if (nomFichier.toLowerCase().endsWith(extensions[i])) {
					return true;
				}
			}
		}
		return valide;
	}

	/**
	 * @param valeur
	 *            the mapDeversoire to set
	 */
	public final void setMapDeversoire(Map<String, SelectionMultiple> valeur) {
		this.mapDeversoire = valeur;
	}

	public static native void clickElement(Element elem) /*-{  elem.click(); }-*/;
}
