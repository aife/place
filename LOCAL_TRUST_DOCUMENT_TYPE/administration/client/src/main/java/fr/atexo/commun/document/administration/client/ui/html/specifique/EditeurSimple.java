package fr.atexo.commun.document.administration.client.ui.html.specifique;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;

public class EditeurSimple extends Composite {

	private static EditeurSimpleUiBinder uiBinder = GWT.create(EditeurSimpleUiBinder.class);

	interface EditeurSimpleUiBinder extends UiBinder<Widget, EditeurSimple> {
	}

	@UiField
	protected LegendElement lblLegende;

	@UiField
	protected TextArea content;

	public EditeurSimple() {
		initWidget(uiBinder.createAndBindUi(this));
		content.setVisibleLines(15);
	}

	/**
	 * Initialisation du contenu du message.
	 * 
	 * @param document
	 */
	public void initialisation(final DocumentModeleDTO document) {
		content.setText(document.getContenuCourriel());
	}

	/**
	 * Mise à jour du courriel.
	 * 
	 * @param document
	 *            document à modifier.
	 * @return le document modifié.
	 */
	public DocumentModeleDTO modifier(final DocumentModeleDTO document) {
		if (this.isVisible()) {
			document.setContenuCourriel(content.getText());
		} else {
			document.setContenuCourriel("");
		}
		return document;
	}

	public void initLabel(final Dictionnaire dictionnaire) {
		lblLegende.setInnerText(dictionnaire.get("editeurTitre"));
	}

	public void setContenu(String contenu) {
		content.setText(contenu);
	}
}
