/**
 *
 */
package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;

/**
 * Tableau d'affichage des champs de fusions simples.
 *
 * @author Léon Barsamian
 */
public class TableauChampFusionSimple extends FlexTable {

	private boolean activerTriParPosition;

	/**
	 * Map de stockage des champs de fusion par lien de suppression.
	 */
	private Map<Integer, ChampFusionDTO> mapChampFusion = new HashMap<Integer, ChampFusionDTO>();

	/**
	 * Element TH pour la description.
	 */
	private Element colonneDescription;

	/**
	 * Element TH pour le domaine.
	 */
	private Element colonneDomaine;

	/**
	 * Element TH pour le Champ.
	 */
	private Element colonneChamp;

	/**
	 * Element TH pour le Action.
	 */
	private Element colonneAction;

	public TableauChampFusionSimple() {
		super();
		this.setCellPadding(0);
		this.setCellSpacing(0);
		this.setBorderWidth(0);
		this.setStyleName("rich-table liste-docs");


		Element head = DOM.createTHead();
		head.setClassName("rich-table-thead");
		Element headerTr = DOM.createTR();
		headerTr.setClassName("rich-table-subheader");
		colonneDescription = DOM.createTH();
		colonneDescription.setInnerText("Description");
		colonneDescription.setClassName("moyen-col");
		headerTr.appendChild(colonneDescription);
		colonneDomaine = DOM.createTH();
		colonneDomaine.setInnerText("Domaine");
		colonneDomaine.setClassName("moyen-col");
		headerTr.appendChild(colonneDomaine);
		colonneChamp = DOM.createTH();
		colonneChamp.setInnerText("Champ");
		colonneChamp.setClassName("moyen-col");
		headerTr.appendChild(colonneChamp);
		colonneAction = DOM.createTH();
		colonneAction.setInnerText("Action");
		colonneAction.setClassName("actions");
		headerTr.appendChild(colonneAction);

		DOM.insertChild(this.getElement(), head, 0);
		DOM.insertChild(head, headerTr, 0);

	}

	/**
	 * Ajoute un champ de fusion au tableau. Ne fait rien si le champ de fusion est déjà ajouté.
	 *
	 * @param champFusion champ de fusion à ajouter.
	 */
	public void ajouterChampFusion(ChampFusionDTO champFusion) {
		if (!mapChampFusion.values().contains(champFusion)) {
			mapChampFusion.put(mapChampFusion.size(), champFusion);
			raffraichirTableau();
		}
	}

	/**
	 * Rajoute un ChampFusion dans la map et une ligne dans le tableau. Ne fait rien si le champ de fusion est déjà ajouté.
	 *
	 * @param champFusion champ de fusion à ajouter.
	 */
	public void ajouterLigne(ChampFusionDTO champFusion, boolean premier, boolean dernier) {
		if (!mapChampFusion.values().contains(champFusion)) {
			int numRows = this.getRowCount();

			// description
			this.setWidget(numRows, 0, new Label(champFusion.getDescription()));
			this.getCellFormatter().addStyleName(numRows, 0, "moyen-col");

			// domaine
			this.setWidget(numRows, 1, new Label(champFusion.getNomDuDomaine()));
			this.getCellFormatter().addStyleName(numRows, 1, "moyen-col");

			// champ
			this.setWidget(numRows, 2, new Label(champFusion.getNom()));
			this.getCellFormatter().addStyleName(numRows, 2, "moyen-col");


			int position = mapChampFusion.size();

			// action
			PositionButton supprimerBouton = new PositionButton(position);
			supprimerBouton.setStyleName("supprimer");
			supprimerBouton.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {
					PositionButton a = (PositionButton) event.getSource();
					supprimerLigne(a.getPosition());
				}
			});

			champFusion.setPosition(position);
			mapChampFusion.put(position, champFusion);

			if (activerTriParPosition) {

				HTMLPanel actions = new HTMLPanel("");
				actions.getElement().setId("champ_simple_ligne_action_" + position);
				actions.add(supprimerBouton, actions.getElement().getId());

				HTMLPanel tri = new HTMLPanel("");
				tri.getElement().setId("champ_simple_ligne_tri_" + position);
				actions.add(tri, actions.getElement().getId());
				tri.setStyleName("lien-deplacer");

				PositionButton triAscendantBouton = new PositionButton(position);
				triAscendantBouton.setStyleName(premier ? "monter-off" : "monter");
				triAscendantBouton.setEnabled(!premier);
				if (!premier) {
					triAscendantBouton.addClickHandler(new ClickHandler() {

						public void onClick(ClickEvent event) {
							PositionButton a = (PositionButton) event.getSource();
							ChampFusionDTO champCourant = mapChampFusion.get(a.getPosition());
							ChampFusionDTO champ = mapChampFusion.get(a.getPosition() - 1);
							champCourant.setPosition(champCourant.getPosition() - 1);
							champ.setPosition(champ.getPosition() + 1);
							mapChampFusion.put(champCourant.getPosition(), champCourant);
							mapChampFusion.put(champ.getPosition(), champ);
							raffraichirTableau();
						}
					});
				}
				tri.add(triAscendantBouton, tri.getElement().getId());

				PositionButton triDescendantBouton = new PositionButton(position);
				triDescendantBouton.setStyleName(dernier ? "descendre-off" : "descendre");
				triDescendantBouton.setEnabled(!dernier);
				if (!dernier) {
					triDescendantBouton.addClickHandler(new ClickHandler() {

						public void onClick(ClickEvent event) {
							PositionButton a = (PositionButton) event.getSource();
							ChampFusionDTO champCourant = mapChampFusion.get(a.getPosition());
							ChampFusionDTO champ = mapChampFusion.get(a.getPosition() + 1);
							champCourant.setPosition(champCourant.getPosition() + 1);
							champ.setPosition(champ.getPosition() - 1);
							mapChampFusion.put(champCourant.getPosition(), champCourant);
							mapChampFusion.put(champ.getPosition(), champ);
							raffraichirTableau();
						}
					});
				}
				tri.add(triDescendantBouton, tri.getElement().getId());

				this.setWidget(numRows, 3, actions);

			} else {
				this.setWidget(numRows, 3, supprimerBouton);
			}

			this.getCellFormatter().addStyleName(numRows, 3, "actions");
		}
	}

	public class PositionButton extends Button {

		private int position;

		public PositionButton() {
			super();
		}

		public PositionButton(int position) {
			super();
			this.position = position;
		}

		public int getPosition() {
			return position;
		}

		public void setPosition(int position) {
			this.position = position;
		}
	}

	/**
	 * Supprime une ligne du tableau à partir du bouton appelant.
	 *
	 * @param position la position de la ligne dans la liste.
	 */
	private void supprimerLigne(int position) {
		mapChampFusion.remove(position);
		raffraichirTableau();
	}

	public void nettoyerTableau() {
		mapChampFusion.clear();
		this.removeAllRows();
	}

	public void raffraichirTableau() {
		ChampFusionDTO[] champs = mapChampFusion.values().toArray(new ChampFusionDTO[mapChampFusion.values().size()]);
		nettoyerTableau();
		for (ChampFusionDTO champ : champs) {
			boolean premier = champ == champs[0];
			boolean dernier = champ == champs[champs.length - 1];
			ajouterLigne(champ, premier, dernier);
		}
	}

	/**
	 * retourne la liste des champs de fusions selectionnés dans le tableau.
	 *
	 * @return List<ChampFusionDTO>
	 */
	public List<ChampFusionDTO> recupereListeChamps() {
		List<ChampFusionDTO> resultat = new ArrayList<ChampFusionDTO>();
		resultat.addAll(mapChampFusion.values());
		return resultat;
	}

	public void initLabel(final Dictionnaire dictionnaire) {
		colonneDescription.setInnerText(dictionnaire.get("champsSimplesTitreColonneDescription"));
		colonneDomaine.setInnerText(dictionnaire.get("champsSimplesTitreColonneDomaine"));
		colonneChamp.setInnerText(dictionnaire.get("champsSimplesTitreColonneChamp"));
		colonneAction.setInnerText(dictionnaire.get("champsSimplesTitreColonneAction"));
	}

	public boolean isActiverTriParPosition() {
		return activerTriParPosition;
	}

	public void setActiverTriParPosition(boolean activerTriParPosition) {
		this.activerTriParPosition = activerTriParPosition;
	}
}
