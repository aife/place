package fr.atexo.commun.document.administration.client.ui.html.specifique;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.LegendElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;

public class EditeurXml extends Composite {

	private static EditeurXmlUiBinder uiBinder = GWT.create(EditeurXmlUiBinder.class);

	interface EditeurXmlUiBinder extends UiBinder<Widget, EditeurXml> {
	}
	
	@UiField 
    protected LegendElement lblLegende;
	
	@UiField
	protected TextArea codeXml;
	
	public EditeurXml() {
		initWidget(uiBinder.createAndBindUi(this));
		codeXml.setVisibleLines(15);
	}
	
	/**
	 * Initialisation du contenu du message.
	 * 
	 * @param document
	 */
	public void initialisation(final DocumentModeleDTO document) {
		codeXml.setText(document.getXml());
	}

	/**
	 * Mise à jour du courriel.
	 * 
	 * @param document
	 *            document à modifier.
	 * @return le document modifié.
	 */
	public DocumentModeleDTO modifier(final DocumentModeleDTO document) {
		if (this.isVisible()) {
			String xml = codeXml.getText();
			document.setXml(xml);
		} else {
			document.setXml("");
		}
		return document;
	}
	
	public void initLabel(final Dictionnaire dictionnaire) {
        lblLegende.setInnerText(dictionnaire.get("xmlTitre"));
    }
	
    public void setContenu(String contenu){
        codeXml.setText(contenu);
    }
}
