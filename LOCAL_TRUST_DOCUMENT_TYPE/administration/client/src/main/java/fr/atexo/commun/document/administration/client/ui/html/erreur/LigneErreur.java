/**
 * 
 */
package fr.atexo.commun.document.administration.client.ui.html.erreur;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Léon Barsamian
 *
 */
public class LigneErreur extends Composite {

    private static LigneErreurUiBinder uiBinder = GWT.create(LigneErreurUiBinder.class);

    interface LigneErreurUiBinder extends UiBinder<Widget, LigneErreur> {
    }


    @UiField
    protected LIElement ligne;
    
    public LigneErreur() {
        initWidget(uiBinder.createAndBindUi(this));
    }


    public final void setMessage(final String message) {
        ligne.setInnerText(message);
    }


}
