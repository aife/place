package fr.atexo.commun.document.administration.client.ui.html.specifique;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

import fr.atexo.commun.document.administration.client.ui.html.specifique.CritereAffichageDetailsPopUp.CritereAffichageDetailsListener;
import fr.atexo.commun.document.administration.client.ui.util.Dictionnaire;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleCritereDTO.TypeOperateur;
import fr.atexo.gwt.ui.outils.commun.ICleValeur;
import fr.atexo.gwt.ui.outils.commun.util.StringUtil;

public class TableauCriteresAffichage extends FlexTable {

	private Element colonneChamp;

	private Element colonneOperateur;

	private Element colonneValeur;

	private Element colonneAction;

	private Dictionnaire dictionnaire;

	LinkedHashMap<String, DocumentModeleCritereDTO> mapRowUuidModel = new LinkedHashMap<String, DocumentModeleCritereDTO>();

	public TableauCriteresAffichage() {
		super();
		this.setCellPadding(0);
		this.setCellSpacing(0);
		this.setBorderWidth(0);
		this.setStyleName("rich-table liste-docs");

		Element head = DOM.createTHead();
		head.setClassName("rich-table-thead");
		Element headerTr = DOM.createTR();
		headerTr.setClassName("rich-table-subheader");

		colonneChamp = DOM.createTH();
		colonneChamp.setClassName("col-430");
		headerTr.appendChild(colonneChamp);

		colonneOperateur = DOM.createTH();
		colonneOperateur.setClassName("moyen-col");
		headerTr.appendChild(colonneOperateur);

		colonneValeur = DOM.createTH();
		colonneValeur.setClassName("moyen-col");
		headerTr.appendChild(colonneValeur);

		colonneAction = DOM.createTH();
		colonneAction.setInnerText("Action");
		colonneAction.setClassName("actions");
		headerTr.appendChild(colonneAction);

		DOM.insertChild(this.getElement(), head, 0);
		DOM.insertChild(head, headerTr, 0);
	}

	public void ajouterLigne(final DocumentModeleCritereDTO documentModeleCritereDTO) {
		final int numRows = this.getRowCount();
		final String uid = HTMLPanel.createUniqueId();
		mapRowUuidModel.put(uid, documentModeleCritereDTO);

		Label nom = new Label(documentModeleCritereDTO.getNom());
		this.setWidget(numRows, 0, nom);
		this.getCellFormatter().addStyleName(numRows, 0, "moyen-col");
		final Label operateur = new Label();
		if (documentModeleCritereDTO.getTypeOperateur() != null) {
			operateur.setText(dictionnaire.get(documentModeleCritereDTO.getTypeOperateur().getCodeLabel()));
		}

		this.setWidget(numRows, 1, operateur);
		this.getCellFormatter().addStyleName(numRows, 1, "moyen-col");

		final Label valeur = new Label();
		if (!StringUtil.estVide(documentModeleCritereDTO.getValeurCritere())) {
			valeur.setText(getValeurFormattee(documentModeleCritereDTO));
		}
		this.setWidget(numRows, 2, valeur);
		this.getCellFormatter().addStyleName(numRows, 2, "moyen-col");

		Button editer = new Button();
		editer.setStyleName("modifier");
		editer.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				CritereAffichageDetailsPopUp popUp = new CritereAffichageDetailsPopUp();
				popUp.initLabel(dictionnaire);

				popUp.setListener(new CritereAffichageDetailsListener() {
					@Override
					public void onValider(DocumentModeleCritereDTO documentModeleCritereDTO) {
						if (documentModeleCritereDTO.getTypeOperateur() != null) {
							operateur.setText(dictionnaire.get(documentModeleCritereDTO.getTypeOperateur().getCodeLabel()));
							valeur.setText(getValeurFormattee(documentModeleCritereDTO));
						} else {
							operateur.setText(null);
							valeur.setText(null);
						}
						mapRowUuidModel.put(uid, documentModeleCritereDTO);
					}
				});

				popUp.afficher(mapRowUuidModel.get(uid));
				popUp.center();
				popUp.show();
			}
		});

		Button supprimer = new Button();
		supprimer.setStyleName("supprimer");
		supprimer.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TableauCriteresAffichage.this.removeRow(numRows);
				mapRowUuidModel.remove(uid);
			}
		});

		HTMLPanel panel = new HTMLPanel("<div id='" + uid + "'></div>");
		panel.add(editer, uid);
		panel.add(supprimer, uid);

		this.setWidget(numRows, 3, panel);
		this.getCellFormatter().addStyleName(numRows, 3, "actions actions-small");
	}

	private String getValeurFormattee(DocumentModeleCritereDTO documentModeleCritereDTO) {

		String valeurCritere = documentModeleCritereDTO.getValeurCritere();
		if (!StringUtil.estVide(valeurCritere)) {

			switch (documentModeleCritereDTO.getTypeDonnee()) {
			case TEXT:
			case STRING:
			case LONG:
			case DOUBLE:
			case BOOLEAN:
			case DATE:
			case DATE_HEURE:
				return valeurCritere;

			case REFERENTIEL: {
				if (documentModeleCritereDTO.getReferentielListeValeurs() != null) {
					for (ICleValeur ref : documentModeleCritereDTO.getReferentielListeValeurs()) {
						if (ref.getCle().equals(valeurCritere)) {
							return ref.getValeur();
						}
					}
				} else {
					return null;
				}
			}

			default:
				return null;
			}

		} else {
			return null;
		}
	}

	public void initLabel(final Dictionnaire dictionnaireInitialisation) {
		dictionnaire = dictionnaireInitialisation;
		colonneChamp.setInnerText(dictionnaire.get("documentModeleCritere.label.champ"));
		colonneOperateur.setInnerText(dictionnaire.get("documentModeleCritere.label.operateur"));
		colonneValeur.setInnerText(dictionnaire.get("documentModeleCritere.label.valeur"));
		colonneAction.setInnerText(dictionnaire.get("champsComplexesTitreColonneAction"));
	}

	public List<DocumentModeleCritereDTO> getEtat(List<String> erreurs) {
		Collection<DocumentModeleCritereDTO> values = mapRowUuidModel.values();
		int i = 1;
		for (DocumentModeleCritereDTO documentModeleCritereDTO : values) {
			boolean ligneIncomplete = false;
			if (documentModeleCritereDTO.getTypeOperateur() == null) {
				ligneIncomplete = true;
			} else {
				if (documentModeleCritereDTO.getTypeOperateur() != TypeOperateur.VIDE
						&& documentModeleCritereDTO.getTypeOperateur() != TypeOperateur.NON_VIDE
						&& StringUtil.estVide(documentModeleCritereDTO.getValeurCritere())) {
					ligneIncomplete = true;
				}
			}
			if (ligneIncomplete) {
				erreurs.add(dictionnaire.get("documentModeleCritere.ligneCritere.incomplete", String.valueOf(i)));
			}
			i++;
		}
		return new ArrayList<DocumentModeleCritereDTO>(values);
	}

}
