package fr.atexo.commun.document.administration.client.ui.page;

public interface LoadingCallBack {

  public void onStart();

  public void onFinish();

  public void onError();

}
