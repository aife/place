package fr.atexo.commun.document.generation.service.tableaudef;

import java.util.Date;

public class TimeStampFormatter {

	public static String marshal(Date v) {
		if (v == null) {
			return null;
		}
		return String.valueOf(v.getTime());
	}

	public static Date unmarshal(String v) {
		if (v == null) {
			return null;
		}
		return new Date(Long.parseLong(v));
	}
}
